import 'firebase/messaging';
import firebase from 'firebase/app';
import LocalStorageUtils from "./LocalStorageUtils";


const tokenKey = 'fcm_token'
const firebaseCloudMessaging = {
//checking whether token is available in indexed DB
    tokenInLocal: async () => {
        return LocalStorageUtils.get(tokenKey);
    },
//initializing firebase app
    init: async function () {
        if (!firebase.apps.length) {
            firebase.initializeApp({
                apiKey: process.env.REACT_APP_API_KEY,
                authDomain: process.env.REACT_APP_AUTH_DOMAIN,
                databaseURL: process.env.REACT_APP_DATABASE_URL,
                projectId: process.env.REACT_APP_FIREBASE_CONFIG_PROJECT_ID,
                storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
                messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
                appId: process.env.REACT_APP_MESSAGING_APP_ID,
                measurementId: process.env.REACT_APP_MEASUREMENT_ID,
            });
            try {
                const messaging = firebase.messaging();
                const tokenInLocal = await this.tokenInLocal();
                //if FCM token is already there just return the token
                if (tokenInLocal) {
                    return tokenInLocal;
                }
                //requesting notification permission from browser
                const status = await Notification.requestPermission();
                if (status && status === 'granted') {
                    //getting token from FCM
                    const fcm_token = await messaging.getToken();
                    if (fcm_token) {
                        //setting FCM token in indexed db using LocalStorage
                        LocalStorageUtils.set(tokenKey, fcm_token);
                        console.log(tokenKey, fcm_token);
                        //return the FCM token after saving it
                        return fcm_token;
                    }
                }
            } catch (error) {
                console.error(error);
                return null;
            }
        }
    },
};
export {firebaseCloudMessaging};
