import JDate from "./utils/JDate";
import LocalStorageUtils from './LocalStorageUtils'
import _ from "lodash";
import {siteOtherKey} from "./sv";
import cookies from "./cookies";
import {DEBUG_AUTH} from "./pages/_app";


const actions = {
    User: {
        main: ({ctx, defaultData} = {}) => LocalStorageUtils.get(siteOtherKey.user, defaultData) || {},
        login: ({user}) => {
            if (!user)
                return
            user.deadline = JDate.liveJalali().add(12, 'hour').unix();
            LocalStorageUtils.set(siteOtherKey.user, user);
            LocalStorageUtils.set(siteOtherKey.userIsLogin, userIsLogin(user));
        },
        logout: () => {
            LocalStorageUtils.set(siteOtherKey.user, {});
            LocalStorageUtils.set(siteOtherKey.userIsLogin, {});
            actions.Basket.reset(0);
        },
        isLogin: ({ctx, defaultData = undefined} = {}) => {
            const serverCockle = cookies.getServer({key: "is_login", ctx: ctx, defaultData: undefined});
            if (_.isString(serverCockle))
                return serverCockle === "True"
            return defaultData


            // return isServer() ? serverCockle ? serverCockle : defaultData : LocalStorageUtils.get(siteOtherKey.userIsLogin, defaultData) === "true"
        }
    },
    Basket: {
        basketCount:({ctx})=>{
            const serverCockle = cookies.getServer({key: "basket_count", ctx: ctx, defaultData: undefined});
            return _.toNumber(serverCockle)
        },
        main: ({ctx, defaultData} = {data: [], count: null}) => {
            try {
                let basket = actions.User.isLogin({ctx}) ? {
                    count: cookies.getServer({key: 'basket_count'})
                } : LocalStorageUtils.get(siteOtherKey.basket, defaultData);
                if (basket === undefined)
                    basket = {};
                if (!basket.data)
                    basket.data = [];
                return {
                    data: basket.data,
                    count: basket.count ? basket.count : 0
                }
            } catch (e) {
                return {
                    data: {},
                    count: 0
                }
            }
        },
        add: ({storage_id, count, features: storageFeatures, index}) => {
            let basketList = actions.Basket.main();
            try {
                basketList.data[index >= 0 ? index : basketList.data.length] = {
                    storage_id: storage_id,
                    count: count,
                    features: storageFeatures
                };
                basketList = {
                    ...basketList,
                    count: basketList.data.length
                };
                LocalStorageUtils.set(siteOtherKey.basket, basketList)
            } catch (e) {
            }
            return basketList;
        },
        remove: ({storage_id, features: storageFeatures}) => {
            let basketList = actions.Basket.main();
            try {
                const index = findStorageIndex({
                    basketList: basketList,
                    storage_id: storage_id,
                    features: storageFeatures
                });
                if (index === -1)
                    return basketList;
                basketList.data.splice(index, 1);
                basketList = {
                    ...basketList,
                    count: basketList.data.length
                };
                LocalStorageUtils.set(siteOtherKey.basket, basketList)
            } catch (e) {
            }
            return basketList
        },
        checkLoginUserBasketCount: (dispatch, {isLogin, localBasketCount} = {}) => {
            try {
                if (isLogin || (isLogin === undefined && userIsLogin())) {
                    const c = cookies.getServer({key: 'basket_count'})
                    if (!localBasketCount || _.toNumber(localBasketCount) !== _.toNumber(c)) {
                        // updateBasketCount(dispatch, {count: c})
                    }
                }
            } catch (e) {
            }
        },
        updateCount: ({count}) => {
            if (count === undefined) {
                return actions.Basket.main()
            }

            const basketList = {
                ...actions.Basket.main(),
                count: count
            };
            LocalStorageUtils.set({key: siteOtherKey.basket, value: basketList})
            return basketList;
        },
        reset: (defaultCount = null) => {
            LocalStorageUtils.set({key: siteOtherKey.basket, value: {data: [], count: defaultCount}})
        }
    },
    FireBase: {
        FcmToken: {
            get: () => {
                return LocalStorageUtils.get(siteOtherKey.fcmToken,undefined)
            },
            set: (fcmToken) => {
               return  LocalStorageUtils.set(siteOtherKey.fcmToken, fcmToken)
            }
        }
    }
};


export default actions;


//region HelperFunc
export function userIsLogin(user = null) {
    if (!user) {
        return DEBUG_AUTH || cookies.getServer({key: "is_login"}) === "True"
    }
    return !!(!_.isEmpty(user) && user.username);
}

function findStorageIndex({basketList = LocalStorageUtils.Basket.main(), storage_id, features: storageFeatures}) {
    try {
        if (!basketList.data || !_.isArray(basketList.data))
            return -1;

        const indexes = [];
        // find index of all same storage
        _.forEach(basketList.data, (o, i) => {
            if (o.storage_id === storage_id)
                indexes.push(i)
        });

        const features = [];
        const sFeatures = [];

        if (_.isEmpty(indexes))
            return -1;


        const f = storageFeatures;
        _.forEach(f, ({fid, fvid}) => {
            _.forEach(fvid, (id) => {
                features.push(`${fid}-${id}`)
            })
        });

        _.forEach(indexes, (i) => {
            sFeatures[i] = [];
            _.forEach(basketList.data[i].features, ({fid, fvid}) => {
                _.forEach(fvid, (id) => {
                    sFeatures[i].push(`${fid}-${id}`)
                })
            });
        });


        let index = -1;

        if (_.isEmpty(features)) {
            index = _.findIndex(sFeatures, (sf) => {
                return _.isEmpty(sf)
            })
        } else {
            _.forEach(sFeatures, (sF, i) => {
                if (!_.isArray(sF) || features.length !== sF.length) {
                    return
                }
                const checker = [];
                _.forEach(features, (f) => {
                    checker.push(_.findIndex(sF, (ss) => {
                        return ss === f
                    }))
                });
                const check = _.findIndex(checker, (c) => {
                    return c === -1;
                });
                if (check === -1) {
                    index = i;
                    return false;
                }
            });
        }

        return indexes[index];
    } catch (e) {
    }
    return -1
}

//endregion HelperFunc
