import React from "react";
import {Box} from "material-ui-helper";
import ContactAsItems from "./ContactAsItems";
import SocialMedia from "./SocialMedia";
import FooterMenu from "./FooterMenu";
import FooterContent from "./FooterContent";
import CertificateBox from "./CertificateBox";
import SiteTitle from "./SiteTitle";
import Copyright from "./Copyright";
import {SmHidden} from "../../header/Header";


export const footerLgPx = 10;

export default function FooterLgV2() {


    return (
        <SmHidden>
            <Box pt={9} width={1} boxShadow={2} flexDirection='column'>
                <Box width={1} px={footerLgPx} pb={7}>
                    <ContactAsItems/>
                    <SocialMedia/>
                </Box>
                <FooterMenu/>
                <Box px={footerLgPx} pt={7} flexDirection={'column'}>
                    <SiteTitle/>
                    <Box>
                        <FooterContent/>
                        <CertificateBox/>
                    </Box>
                </Box>
                <Copyright/>
            </Box>
        </SmHidden>
    )
}