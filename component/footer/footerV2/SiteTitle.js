import React from "react";
import Typography from "../../base/Typography";
import {colors, lang} from "../../../repository";
import {useRouter} from "next/router";
import rout from "../../../router";


export default function SiteTitle() {
    const {pathname} = useRouter();


    return (
        <Typography
            pt={1}
            color={colors.textColor.h000000}
            component={pathname === rout.Main.home ? "h1" : "span"}
            fontWeight={500}
            variant={"body1"}>
            {lang.get("mehr_takhfif_store")}
        </Typography>
    )
}