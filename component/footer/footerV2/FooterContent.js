import React from "react";
import {Box, Typography} from "material-ui-helper";
import {colors, lang} from "../../../repository";


export default function FooterContent() {

    return (
        <Box flex={1}>
            <Typography variant={"subtitle2"} color={colors.textColor.h404040} pt={2.5} lineHeight={2.22} pb={5}>
                {lang.get("footer_text")}
            </Typography>
        </Box>
    )
}