import React, {useMemo} from "react";
import {Box, Typography} from "material-ui-helper";
import {colors, lang} from "../../../repository";
import {globalProps} from "../../../pages/_app";
import Link from "../../base/link/Link";
import {useTheme} from "@material-ui/core";


const itemsGenerator = () => [
    {
        title: "support_number",
        value: "mehr_phone_number",
        link: `tel:${globalProps.phone}`
    },
    {
        title: "email_address",
        value: globalProps.email,
        link: `mailto:${globalProps.email}`
    }
]

export default function ContactAsItems() {

    const items = useMemo(()=>itemsGenerator(),[])

    return (
        <Box flexDirection='column'>
            {items.map((it,index) => (
                <Item key={it?.title} data={it} index={index}/>
            ))}
        </Box>
    )
}

function Item({data,index}) {
    const theme = useTheme()
    const {title, value, link} = data;

    return (
        <Box pt={index >0?2:0}>
            <Typography
                width={theme.spacing(18.75)}
                variant={"body2"}
                color={colors.textColor.h757575}>
                {lang.get(title)}
            </Typography>
            <Link  dir={'ltr'} href={link} variant={"subtitle2"} color={colors.textColor.h404040} fontWeight={500}>
                {lang.get(value)}
            </Link>
        </Box>
    )
}