import React from "react";
import {Box, Typography} from "material-ui-helper";
import {colors} from "../../../repository";
import {footerLgPx} from "./FooterLgV2";


export default function Copyright(){
    return(
        <Box dir={'ltr'} py={1.5} px={footerLgPx} bgcolor={colors.backgroundColor.hf8f8f8}>
            <Typography variant={"caption"} color={colors.textColor.h757575}>
                © Copyright 2021 MehrTakhfif.com
            </Typography>
        </Box>
    )
}