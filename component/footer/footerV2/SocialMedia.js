import React from "react";
import {Box, IconButton, Typography} from "material-ui-helper";
import {colors, lang} from "../../../repository";
import MtIcon from "../../MtIcon";
import Link from "../../base/link/Link";


const items = [
    {
        name: "telegram",
        icon: "mt-telegram",
        href: "https://t.me/MehrTakhfif"
    },
    {
        name: "instagram",
        icon: "mt-instagram-filled",
        href: "https://www.instagram.com/mehrtakhfif"
    },
    {
        name: "twitter",
        icon: "mt-twitter",
        href: "https://twitter/MehrTakhfif"
    },
]

export default function SocialMedia() {


    return (
        <Box flex={1} justifyContent={"flex-end"}>
            <Box flexDirection={'column'} pr={9.5}>
                <Typography pl={1} variant={'subtitle2'} color={colors.textColor.h757575} fontWeight={"normal"}>
                    {lang.get("get_in_touch_with_us")}
                </Typography>
                <Box dir={'ltr'} pt={2}>
                    {
                        items?.map(({name, icon, href},index) => (
                            <Box  key={name} pr={index!==0 ? 6:0}>
                                <IconButton size={'small'}>
                                    <Link
                                        href={href}
                                        target={"_blank"}
                                        color={colors.textColor.h404040}
                                        hoverColor={colors.textColor.h404040}>
                                        <MtIcon icon={icon}/>
                                    </Link>
                                </IconButton>
                            </Box>
                        ))
                    }
                </Box>
            </Box>
        </Box>
    )
}