import React, {useMemo} from "react";
import {colors, lang} from "../../../repository";
import {Box} from "material-ui-helper";
import rout from "../../../router";
import {footerLgPx} from "./FooterLgV2";
import Link from "../../base/link/Link";


const itemsGenerator = () => {
    return [
        {
            title: "about_us",
            href: rout.Main.AboutUs.create(),
        },
        {
            title: "contact_us",
            href: rout.Main.ContactUs.create(),
        },
        {
            title: "rules_and_privacy",
            href: rout.Main.PrivacyPolicy.create(),
        },
    ]
}

export default function FooterMenu() {

    const items = useMemo(() => itemsGenerator(), [])

    return (
        <Box
            px={footerLgPx}
            component={"ul"}
            width={1}
            bgcolor={colors.backgroundColor.hf8f8f8}>
            {
                items.map(({title,href}) => (
                    <Box key={title} pr={10} py={2} component={"li"}>
                        <Link py={1} href={href}  variant={"body2"} fontWeight={500}>
                            {lang.get(title)}
                        </Link>
                    </Box>
                ))
            }
        </Box>
    )
}