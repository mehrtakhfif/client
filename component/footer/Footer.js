import React from "react";
import FooterSm from "./FooterSm";
import {getSafe} from "material-ui-helper";
import {useHeaderAndFooterContext} from "../../context/HeaderAndFooterContext";
import FooterLgV2 from "./footerV2/FooterLgV2";


export default function Footer() {
    const [visibility] = useHeaderAndFooterContext();

    return (
        <React.Fragment>
            {
                getSafe(()=>visibility.footer.showLg,true) &&
                <FooterLgV2/>
            }
            {
                visibility?.footer?.showSm &&
                <FooterSm smFooter={visibility.footer.showSm}/>
            }
        </React.Fragment>
    )
}
