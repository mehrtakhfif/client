import React, {useEffect, useMemo, useState} from "react";
import {grey} from "@material-ui/core/colors";
import {SmHidden} from "../header/Header";
import {Card} from "@material-ui/core";
import Link from "../base/link/Link";
import {lang} from "../../repository";
import Icon from "../base/Icon";
import rout from "../../router";
import NLink from "../base/link/NLink";
import {Typography} from "material-ui-helper";
import Box from "@material-ui/core/Box";
import {useRouter} from "next/router";

export const footerMenu = () => {
    return [
        {
            label: "about_us",
            link: rout.Main.AboutUs.create(),
        },
        {
            label: "contact_us",
            link: rout.Main.ContactUs.create(),
        },
        {
            label: "privacy_policy",
            link: rout.Main.PrivacyPolicy.create(),
        },
    ]
}

export default function FooterLg({...props}) {
    const {pathname} = useRouter()
    const footerMenuItem = useMemo(() => footerMenu(), [])

    useEffect(() => {
        try {
            // gLog("askfjkasjkfjkasjfkj", router)
        } catch (e) {
        }
    }, [])

    return (
        <SmHidden>
            <Box p={2}>
                <Box width={1}
                     component={Card}
                     py={2}
                     flexDirection={'column'}>
                    <Box display={'flex'} px={2}>
                        <Box flex={1} maxWidth={2 / 3}>
                            <Typography pt={1} component={pathname === rout.Main.home ? "h1" : "span"}
                                        fontWeight={"normal"} variant={"body1"}>
                                فروشگاه اینترنتی مهرتخفیف
                            </Typography>
                            <FooterText/>
                        </Box>
                        <Box display={'flex'} minWidth={1 / 3} alignItems={'center'} justifyContent={'flex-end'}>
                            <Box
                                mx={0.5}
                                className={"certificationContainer"}
                                p={0.5}>
                                <a target="_blank"
                                   href="https://trustseal.enamad.ir/?id=131602&amp;Code=xUk7lkHP0S4Mrq3B8jrX">
                                    <img src="https://trustseal.enamad.ir/logo.aspx?id=131602&Code=xUk7lkHP0S4Mrq3B8jrX"
                                         alt=""
                                         style={{cursor: "pointer"}}
                                         id="xUk7lkHP0S4Mrq3B8jrX"/>
                                </a>
                            </Box>
                        </Box>
                    </Box>
                    <Box display={'flex'} py={1} px={2} my={2} style={{backgroundColor: grey[300]}}>
                        <Box display={'flex'} maxWidth={2 / 3} flex={1} px={5} alignItems={'center'}>
                            {footerMenuItem.map(it => (
                                <Box key={it.label} px={5} py={1}>
                                    <NLink variant={"body2"} fontWeight={"normal"} href={it.link}>
                                        {lang.get(it.label)}
                                    </NLink>
                                </Box>
                            ))}
                        </Box>
                        <Box minWidth={1 / 3} display={'flex'} justifyContent={'flex-end'} alignItems={'center'}>
                            <Box mx={1}>
                                <Link href={"https://t.me/MehrTakhfif"} target={'_blank'}>
                                    <Icon src={'/drawable/svg/socialMedia/telegram.svg'} width={30} color={false}
                                          alt={"telegram"}/>
                                </Link>
                            </Box>
                            <Box mx={1}>
                                <Link href={"https://instagram.com/mehrtakhfif"} target={'_blank'}>
                                    <Icon src={'/drawable/svg/socialMedia/instagram.svg'} width={30} color={false}
                                          alt={"instagram"}/>
                                </Link>
                            </Box>
                            <Typography variant={"body1"} pr={2}>
                                {lang.get("fallow_us")}
                            </Typography>
                        </Box>
                    </Box>
                    <Box py={1} px={2} display={'flex'} flexDirection={'column'}>
                        <Typography component={'p'} variant={"body1"} py={1}>
                            آدرس: {lang.get("mehrtakhfif-address")}
                        </Typography>
                        <Typography component={'p'} variant={"body1"} py={1}>
                            شماره تماس:
                            <Link px={1} dir={'ltr'} href={`tel:01391003033`}>
                                013 9100 3033
                            </Link>
                        </Typography>
                        <Typography component={'p'} variant={"body1"} py={1}>
                            کد پستی:
                            <Box component={'span'} dir={'ltr'} px={1}>
                                41799-36735
                            </Box>
                        </Typography>
                    </Box>
                </Box>
            </Box>
        </SmHidden>
    )
}


const footerTextProps = {
    display: "unset",
    component: "span",
    fontWeight: 200,
    variant: "body2",
    lineHeight: 2,
    color: "#000"
}

function FooterText() {
    const [show, setShow] = useState(false)


    return (
        <Box component={"p"} pt={1} flexDirection={"column"}>
            <Typography {...footerTextProps}>
                فروشگاه آنلاین مهرتخفیف شامل سایت و اپلیکیشن فروش و تخفیف گروهی که کالا و خدمات رو در رسته های مختلف با
                قیمت مناسب و ترجیحا با تخفیف به دلیل خرید مستقیم و بی‌واسطه از تامین‌کننده‌ها، در دسترس کاربران قرار
                میده و مدیریت هر رسته بر عهده‌ی کارشناس متخصص همان رسته‌ست. بنابراین در مورد خرید محصول مورد نظرتون
                میتونین مشاوره دریافت کنید.
            </Typography>
            {
                !show &&false &&
                <Typography
                    {...footerTextProps}
                    pl={1}
                    onClick={() => {
                        setShow(true)
                    }}
                    style={{
                        cursor: "pointer"
                    }}>
                    نمایش بیشتر
                </Typography>
            }
            <Typography
                {...footerTextProps}
                display={show ? "unset" : "none"}>
                <br/>
                فروشگاه اینترنتی مهرتخفیف شامل رسته های: لوازم ورزشی، ورزش و سرگرمی(بلیط مراکز ورزشی و تفریحی ثبت نام
                دورهای ورزشی و ...) ، گلکده(گل های آپارتمانی سبد گل باکس گل گلدان خاک-کود) عطاری (دمنوش ها و گیاهان
                دارویی و...) کافی شاپ و رستوران (بن تخفیف رستوران ها و کافی شاپها و...) صنایع دستی کودک و نوجوان
                مشاغل تحت حمایت(ارائه محصولات تولید شده توسط افراد تحت پوشش مراکز خیریه)
                هر خرید آنلاینی که از مهرتخفیف انجام میشه، نیم درصد از بهای محصول(کالا و خدمات) فروخته شده به صورت
                مستقیم به حساب موسسات خیریه‌ای که زنان سرپرست خانوار رو تحت پوشش قرار میدن، واریز می‌شه.
                بعد از اینکه محصول تولید شد، تیم مهرتخفیف از محصولات عکسبرداری کرده و در رسته‌ی «مشاغل تحت حمایت» در
                دسترس کاربران قرار داده می‌شه.
                محصولاتی که در رسته‌ی «مشاغل تحت حمایت» هستند، توسط زنان سرپرست خانوار تولید شده و پس از خرید این
                محصولات توسط کاربران، تمام وجه پرداختی بدون کسر کارمزد به حساب این افراد واریز میشه و مهرتخفیف هیچ سودی
                از این رسته دریافت نخواهد کرد.
            </Typography>
            <Typography
                fontWeight={400}
                {...footerTextProps}
                display={show ? "block" : "none"}
                textAlign={"center"}>
                *مهرتخفیف مثل یه دوسته مهربونه*
            </Typography>
        </Box>
    )
}
