import React, {useEffect} from "react";
import Hidden from "@material-ui/core/Hidden";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {useRouter} from "next/router";
import _ from "lodash"
import clsx from "clsx";
import Toolbar from "@material-ui/core/Toolbar";
import {lang} from "../../repository";
import rout, {routIsExact} from "../../router";
import Badge from "@material-ui/core/Badge/Badge";
import Box from "@material-ui/core/Box";
import Placeholder from "../base/Placeholder";
import Link from "../base/link/Link";
import {footerMenu} from "./FooterLg";
import {grey} from "@material-ui/core/colors";
import {useBasketCountContext} from "../../context/BasketCount";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useLoginDialogContext} from "../../context/LoginDialogContextContainer";
import {tryIt} from "material-ui-helper";
import {backdropType, useBackdropContext} from "../../context/BackdropContext";

export const smFooterHeight = 50;

const useFooterSmStyles = makeStyles(theme => ({
    footerSmRoot: {
        backgroundColor: 'rgba(255, 255, 255)',
        flexGrow: 1,
        position: 'fixed',
        bottom: 0,
        height: smFooterHeight,
        right: 0,
        left: 0,
        zIndex: 90,
        '&>div>div>span': {
            bottom: 'unset',
            top: 0
        }
    },
    footerSmTab: {
        fontSize: '0.55rem',
        minHeight: 40,
        paddingTop: theme.spacing(1),
        paddingBottom: 0,
    },
}));

export default function FooterSm() {
    const isLogin = useIsLoginContext();
    const classes = useFooterSmStyles();
    const router = useRouter();
    const [backdrop, _______,showBackDrop, hideBackdrop] = useBackdropContext();

    const [value, setValue] = React.useState(firstState());
    const basketCount = useBasketCountContext();
    const [__, ___, handleOpenLoginDialog] = useLoginDialogContext()

    useEffect(() => {
        router.prefetch(rout.Main.home)
        router.prefetch(rout.User.Basket.rout)
        const {rout: r, as} = rout.Main.BoxList.create({})
        router.prefetch(r, as)
        const {rout: r2, as: as2} = rout.User.Profile.Main.create()
        router.prefetch(r2, as2)
    }, [])


    function firstState() {
        if (routIsExact(router.pathname, rout.Main.BoxList.main)) {
            return 1
        }
        if (routIsExact(router.pathname, rout.User.Basket.rout)) {
            return 2
        }
        if (routIsExact(router.pathname, rout.User.Profile.Main.rout)) {
            return 3
        }
        if (routIsExact(router.pathname, rout.Main.home)) {
            return 0
        }
        return 0
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
        let routUrl;
        switch (newValue) {
            case 1: {
                routUrl = rout.Main.BoxList.create({});
                break
            }
            case 2: {
                routUrl = rout.User.Basket.rout;
                break
            }
            case 3: {
                if (!isLogin) {
                    handleOpenLoginDialog()
                    break;
                }
                routUrl = rout.User.Profile.Main.create();
                break
            }
            default: {
                routUrl = rout.Main.home;
            }
        }

        showBackDrop(backdropType.fillPageWithLoading)


        setTimeout(()=>{
            if (_.isObject(routUrl) && routUrl.rout && routUrl.as) {
                router.push(routUrl.rout, routUrl.as).finally(()=>hideBackdrop());
                return
            }
            router.push(routUrl).finally(()=>hideBackdrop());
        },800)
    };

    const iconStyle = {
        marginBottom: 2,
        width: 20
    };


    return (
        <Hidden lgUp={true}>
            <Toolbar style={{
                zIndex:1
            }}/>
            <Paper square
                   className={clsx(classes.footerSmRoot, 'headerRoot', 'paddingRightWhenOverflow')}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    variant="fullWidth"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="icon tabs example">
                    <Tab
                        disabled={backdrop}
                        className={classes.footerSmTab}
                        icon={<img alt={'HomeIcon'}
                                   src={'/drawable/svg/footer/HomeIcon.svg'}
                                   style={iconStyle}/>}
                        aria-label="home"
                        label={lang.get('home')}/>
                    <Tab
                        disabled={backdrop}
                        className={classes.footerSmTab}
                         icon={<img alt={'Dashboard Icon'}
                                    src={'/drawable/svg/footer/DashboardIcon.svg'}
                                    style={iconStyle}/>} aria-label="category"
                         label={lang.get('category')}/>
                    <Tab
                        disabled={backdrop}
                        className={classes.footerSmTab}
                         icon={
                             <Badge badgeContent={_.toNumber(basketCount)} color="primary"
                                    className={classes.shoppingCartIconBadge}
                                    anchorOrigin={{
                                        horizontal: 'right',
                                        vertical: 'bottom'
                                    }}>
                                 <img
                                     alt={'Basket Icon'}
                                     src={'/drawable/svg/footer/BasketIcon.svg'}
                                     style={iconStyle}/>
                             </Badge>
                         } aria-label="basket"
                         label={lang.get('basket')}/>
                    <Tab
                        disabled={backdrop}
                        className={classes.footerSmTab}
                        icon={<img
                            alt={'Profile Icon'}
                            src={'/drawable/svg/footer/ProfileIcon.svg'}
                            style={iconStyle}/>}
                        aria-label="profile"
                        label={lang.get('profile')}/>
                </Tabs>
            </Paper>
            <Placeholder/>
        </Hidden>
    );
}


export function ExtraMenu({...props}) {
    return (
        <Box display={'flex'} px={2} mt={2} flex={1} alignItems={'center'}
             {...props}
             style={{
                 backgroundColor: grey[200],
                 ...props.style
             }}>
            {footerMenu().map(it => (
                <Box key={it.label} justifyContent={'center'} px={1}>
                    <Link href={it.link} variant={"h6"}>
                        <Box px={1} py={1}>
                            {lang.get(it.label)}
                        </Box>
                    </Link>
                </Box>
            ))}
        </Box>
    )
}
