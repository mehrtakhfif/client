import React from "react";
import {HiddenMdUp, HiddenSmDown} from "material-ui-helper";
import LgBasketPanel from "./lgBasketPanel/LgBasketPanel";
import SmBasketPanel from "./SmBasketPanel";


export default function BasketPanel({summary}) {


    return (
        <React.Fragment>
            <HiddenSmDown>
                <LgBasketPanel summary={summary}/>
            </HiddenSmDown>
            <HiddenMdUp>
                <SmBasketPanel summary={summary}/>
            </HiddenMdUp>
        </React.Fragment>
    )
}


