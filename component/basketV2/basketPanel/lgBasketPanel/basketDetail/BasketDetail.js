import React from "react";
import {Box, HiddenSmDown, Typography} from "material-ui-helper";
import BasketDetailItem from "./BasketDetailItem";
import {colors, lang} from "../../../../../repository";
import {UtilsFormat} from "../../../../../utils/Utils";
import {Divider} from "@material-ui/core";
import {useBasketCountContext} from "../../../../../context/BasketCount";


export default function BasketDetail({summary}) {
    const basketCount = useBasketCountContext();

    const {
        discount_price,
        total_price
    } = summary || {};

    return (
        <React.Fragment>
            <BasketDetailItem
                pt={{
                    xs:0,
                    md:4,
                    lg:7,
                }}
                title={(
                    <React.Fragment>
                        {lang.get("cost_of_products")}
                        <Typography pl={0.7} variant={"body2"} fontWeight={300}
                                    color={colors.textColor.h757575}>
                            ( {basketCount} {lang.get("item")} )
                        </Typography>
                    </React.Fragment>
                )}
                value={UtilsFormat.numberToMoney(total_price)}/>
            <BasketDetailItem
                pt={{
                    xs: 1,
                    md: 2.5
                }}
                title={lang.get("discount")}
                value={UtilsFormat.numberToMoney(total_price - discount_price)}
                titleProps={{color: colors.textColor.h00aa5b}}
                valueProps={{color: colors.textColor.h00aa5b}}/>
            <HiddenSmDown>
                <Box py={2}>
                    <Divider style={{backgroundColor: colors.borderColor.heff0ef}}/>
                </Box>
            </HiddenSmDown>
            <BasketDetailItem
                pt={{
                    xs: 2,
                    md: 0
                }}
                title={lang.get("total")}
                titleProps={{
                    color: colors.textColor.h000000,
                    fontWeight: 500,
                    variant: {
                        xs:"body2",
                        md:"subtitle2",
                    },
                }}
                value={UtilsFormat.numberToMoney(discount_price)}
                valueProps={{
                    color: colors.textColor.h404040,
                    fontWeight: 500,
                    variant: {
                        xs:"subtitle1",
                        md:"h6"
                    },
                    component: 'div'
                }}/>
        </React.Fragment>
    )
}