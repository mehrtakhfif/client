import {Box, Typography} from "material-ui-helper";
import {colors, lang} from "../../../../../repository";
import React from "react";

export default function BasketDetailItem({title, titleProps = {}, value, valueProps = {}, ...props}) {

    return (
        <Box width={1} {...props}>
            <Typography flex={1} flexWrap={'wrap'} variant={"body2"} fontWeight={400} color={colors.textColor.h404040} {...titleProps}>
                {title}
            </Typography>
            <Typography
                variant={"subtitle2"}
                alignItems={'center'}
                color={colors.textColor.h404040}
                fontWeight={400} {...valueProps}>
                {value}
                <Typography pl={1.2} variant={"caption"}
                            fontWeight={400}>
                    {lang.get("toman")}
                </Typography>
            </Typography>
        </Box>
    )
}