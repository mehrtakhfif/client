import React from "react";
import StickyBox from "../../../base/StickyBox";
import {Box, HiddenSmDown, Typography} from "material-ui-helper";
import {colors, lang} from "../../../../repository";
import {UtilsStyle} from "../../../../utils/Utils";
import BasketDetail from "./basketDetail/BasketDetail";
import BasketSubmitButton from "../BasketSubmitButton";
import Link from "../../../base/link/Link";
import {globalProps} from "../../../../pages/_app";


export default function LgBasketPanel({summary}){



    return(
        <StickyBox
            offsetTop={130}
            width={0.27} pl={3.3}>
            <Box pt={6} justifyContent={'center'} flexDirection={'column'}>
                <Box
                    px={3}
                    pb={3}
                    width={1}
                    flexDirection={'column'}
                    alignItems={'center'}
                    style={{
                        position: 'relative',
                        border: `1px solid ${colors.borderColor.heff0ef}`,
                        ...UtilsStyle.borderRadius(4)
                    }}>
                    <Typography
                        variant={"subtitle1"}
                        fontWeight={500}
                        px={2}
                        style={{
                            position: 'absolute',
                            top: -20,
                            backgroundColor: colors.textColor.hffffff,
                            ...UtilsStyle.borderRadius(4)
                        }}>
                        {lang.get("order_information")}
                    </Typography>
                    <HiddenSmDown>
                        <BasketDetail summary={summary}/>
                    </HiddenSmDown>
                    <BasketSubmitButton summary={summary}/>
                </Box>
                <Box minWidth={1 / 2} pt={2} center={true}>
                    <Typography variant={"body2"} fontWeight={400} pr={2.5} color={colors.textColor.ha9a9a9}>
                        {lang.get("support_number")}
                    </Typography>
                    <Link
                        pl={2.5}
                        dir={'ltr'} href={`tel:${globalProps.phone}`}
                        color={colors.textColor.h404040} variant={"subtitle2"}
                        fontWeight={500}>
                        {lang.get("mehr_phone_number")}
                    </Link>
                </Box>
            </Box>
        </StickyBox>
    )
}