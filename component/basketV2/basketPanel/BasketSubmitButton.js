import React from "react";
import {colors, lang} from "../../../repository";
import {Button, useState} from "material-ui-helper";
import {useIsLoginContext} from "../../../context/IsLoginContextContainer";
import {useLoginDialogContext} from "../../../context/LoginDialogContextContainer";
import {useTheme} from "@material-ui/core";
import {useRouter} from "next/router";
import mainRout from "../../../router";


export default function BasketSubmitButton(props) {
    const theme = useTheme();
    const isLogin = useIsLoginContext();
    const router = useRouter();
    const [__, ___, onOpenLogin, onCloseLogin] = useLoginDialogContext()
    const [loading,setLoading] = useState(false)

    function handleSubmitClick() {
        if (!isLogin) {
            onOpenLogin()
            return
        }
        setLoading(true)
        setTimeout(()=>{
        const {rout, as} = mainRout.User.Basket.Shopping.Params.Details.create()
        router.push(rout, as).finally(()=>setLoading(false))

        },2000)
    }


    return (
        <Button
            pt={{
                xs: 0,
                md: 3
            }}
            loading={loading}
            fullWidth={true}
            onClick={handleSubmitClick}
            color={theme.palette.primary.main}
            typography={{
                variant: "subtitle1",
                color: colors.textColor.hffffff
            }}
            {...props}
        style={{
            zIndex:3,
            ...props.style
        }}>
            {lang.get("next_level")}
        </Button>
    )
}