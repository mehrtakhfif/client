import React from "react";
import {Box} from "material-ui-helper";
import Toolbar from "@material-ui/core/Toolbar";
import {smFooterHeight} from "../../footer/FooterSm";
import {colors} from "../../../repository";
import BasketSubmitButton from "./BasketSubmitButton";


export default function SmBasketPanel({summary}) {

    return (
        <React.Fragment>
            <Toolbar style={{
                zIndex:1
            }}/>
            <Box
                boxShadow={3}
                width={1}
                px={2}
                py={1.5}
                bgcolor={colors.backgroundColor.hffffff}
                style={{
                    position: 'fixed',
                    bottom: smFooterHeight-8,
                    zIndex:3
                }}>
                <BasketSubmitButton mb={"8px"}/>
            </Box>
        </React.Fragment>
    )
}