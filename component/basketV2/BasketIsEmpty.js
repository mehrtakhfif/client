import React from "react";
import {Box} from "material-ui-helper";
import EmptyBasket from "../basket/basketPopup/BasketIsEmpty";


export default function BasketIsEmpty(){

    return(
        <Box width={1} center={true}>
            <EmptyBasket showWishlist={true}/>
        </Box>
    )
}