import React from "react";
import {Box, UtilsStyle} from "material-ui-helper";
import {colors} from "../../../repository";
import BasketItem from "./BasketItem";
import {Divider} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";


const useBasketItemContainerStyles = makeStyles((theme) => ({
    root:{
        [theme.breakpoints.up('md')]: {
            border: `1px solid ${colors.borderColor.heff0ef}`,
            ...UtilsStyle.borderRadius(5)
        },
    },
}));
function BasketItemsContainer({data = [{}, {}, {}]}) {
const classes = useBasketItemContainerStyles();

    return (
        <Box
            pr={{
                xs:0,
                md:3.3
            }}
            flex={1}>
            {/*{deleted and changed basket items show}*/}
            <Box
                width={1}
                pt={{
                    xs:2,
                    md:6
                }}
                pl={{
                    xs:0,
                    md:2
                }}>
                <Box
                    className={classes.root}
                    px={{
                        xs:0,
                        md:4
                    }}
                    width={1}
                    flexDirection={'column'}>
                    {
                        data?.map((pr, index) => {
                            const isLastItem = data.length <= index + 1

                            return (
                                <React.Fragment key={pr?.id || index}>
                                    <BasketItem data={pr} index={index}/>
                                    {
                                        !isLastItem &&
                                        <Divider style={{backgroundColor: colors.borderColor.heff0ef}}/>
                                    }
                                </React.Fragment>
                            )
                        })
                    }
                </Box>
            </Box>
        </Box>
    )
}


export default React.memo(BasketItemsContainer)