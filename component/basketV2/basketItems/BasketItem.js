import React from "react";
import {Box, HiddenMdUp, HiddenSmDown, Random, Skeleton, Typography, UtilsStyle} from "material-ui-helper";
import Img from "../../base/Img";
import {colors, lang, media} from "../../../repository";
import MtIcon from "../../MtIcon";
import {Divider, useTheme} from "@material-ui/core";
import {DEBUG} from "../../../pages/_app";
import NumberSelector from "../../base/numberSelector/NumberSelector";
import {UtilsFormat} from "../../../utils/Utils";
import _ from "lodash"
import rout from "../../../router";
import {backdropType, useBackdropContext} from "../../../context/BackdropContext";
import Nlink from "../../base/link/NLink";
import ProductPrice from "../../product/ProductCardV2/Price"
import ControllerUser from "../../../controller/ControllerUser";

function BasketItem({data, index}) {

    const {
        id,
        product,
        final_price,
        discount_price,
        discount_percent,
        storage,
        count,
        features = []
    } = data || {};

    const {id: productId, name, permalink, thumbnail,default_storage} = product || {};
    const {id: storageId, max_count_for_sale,min_count_for_sale} = default_storage || {};
    const {imageTitle, image} = thumbnail || {};

    const isPlaceholder = !_.isNumber(id)
    const href = rout.Product.SingleV2.create(permalink || "placeholder")


    return (
        <Box py={3}
             flexDirection={'column'}
             px={{
                 xs: 2,
                 lg: 0
             }}>
            <Box width={1}>
                <Box
                    flexDirection={'column'}
                    width={{
                        xs: 0.4,
                        md: 0.25
                    }}
                    pr={{
                        xs: 1,
                        md: 5
                    }}>
                    <Nlink
                        href={href}
                        style={{width: "100%"}}>
                        <Img
                            alt={imageTitle}
                            src={image}
                            borderRadius={5}
                            imageWidth={media.thumbnail.width}
                            imageHeight={media.thumbnail.height}
                            imageProps={{
                                style: {
                                    ...UtilsStyle.borderRadius(5)
                                }
                            }}/>
                    </Nlink>
                    <HiddenMdUp>
                        <Box width={1} pt={2}>
                            <ProductCount
                                uniqKey={`sm-${id}`}
                                storageId={storageId}
                                placeholder={isPlaceholder}
                                count={count}
                                maxCountForSale={max_count_for_sale}
                                minCountForSale={min_count_for_sale}/>
                        </Box>
                    </HiddenMdUp>
                </Box>
                <Box
                    flexDirection={{
                        xs: 'column',
                        md: 'row'
                    }}
                    width={{
                        xs: 0.6,
                        md: 0.75
                    }}>
                    <Box
                        width={{
                            xs: 1,
                            md: 0.45
                        }}
                        flexDirection={'column'} justifyContent={"space-between"}>
                        <Box flexDirection={'column'}>
                            <Title href={href} placeholder={isPlaceholder} title={name}/>
                            <Features placeholder={isPlaceholder} data={features} index={index}/>
                        </Box>
                        <HiddenSmDown>
                            <Actions basketproductId={id} productId={productId} storageId={storageId} placeholder={isPlaceholder}/>
                        </HiddenSmDown>
                    </Box>
                    <HiddenSmDown>
                        <Box width={0.25} justifyContent={'center'} alignItems={'center'}>
                            <ProductCount
                                storageId={storageId}
                                center={true}
                                uniqKey={`lg-${id}`}
                                placeholder={isPlaceholder}
                                count={count}
                                minCountForSale={min_count_for_sale}
                                maxCountForSale={max_count_for_sale}/>
                        </Box>
                    </HiddenSmDown>
                    <Price finalPrice={final_price} discountPercent={discount_percent} discountPrice={discount_price}
                           placeholder={isPlaceholder}/>
                </Box>
            </Box>
            <HiddenMdUp>
                <Actions  basketproductId={id} productId={productId} storageId={storageId} placeholder={isPlaceholder}/>
            </HiddenMdUp>
        </Box>
    )
}

export default React.memo(BasketItem)


function Title({href, placeholder, title}) {

    if (placeholder)
        return <Skeleton width={"75%"} height={20}/>


    return (
        <Nlink
            href={href}
            variant={{
                xs: "body2",
                md: "subtitle2"
            }}
            fontWeight={"normal"}
            color={colors.textColor.h404040}
            style={{
                lineHeight: 1.9,
                ...UtilsStyle.limitTextLine(2)
            }}>
            {title}
            {(DEBUG && Random.randomBool()) ? "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است" : ""}
        </Nlink>
    )
}

function Features({placeholder, data = [], index}) {


    if (placeholder && index % 2 !== 0)
        data = [{}, {}]


    return (
        <React.Fragment>
            {data?.map((fe, index) => {
                const {id, feature, feature_value, feature_settings} = fe || {};

                return (
                    <Box
                        width={1}
                        key={id || index}
                        pt={1.5}>
                        <Typography
                            minWidth={'30%'}
                            color={colors.textColor.h757575}
                            variant={'subtitle2'}
                            pr={0.5}>
                            {
                                _.isEmpty(fe) &&
                                <Skeleton width={"100%"} height={20} style={{
                                    marginLeft:'5px'
                                }}/>
                            }
                            {feature || ""}:
                        </Typography>
                        <Typography
                            minWidth={'30%'}
                            variant={'subtitle2'}>
                            {
                                !_.isEmpty(fe) ?
                                    feature_value
                                    :
                                    <Skeleton width={"100%"} height={20}/>
                            }
                        </Typography>
                    </Box>
                )
            })}
        </React.Fragment>
    )
}

function Actions({basketproductId,productId, storageId, placeholder}) {
    const theme = useTheme();
    const [__, setShowBackDrop, ___, hideBackdrop] = useBackdropContext();

    function handleDelete() {
        setShowBackDrop(backdropType.fillPageWithLoading);
        ControllerUser.Basket.delete({basketproduct_id: basketproductId}).finally(() => {
            hideBackdrop()
        });
    }

    function handleAddWhitelist() {
        setShowBackDrop(backdropType.fillPageWithLoading)
        Promise.all([
            ControllerUser.User.Profile.Wishlist.update({productId: productId, wish: true}),
            ControllerUser.Basket.delete({storage_id: storageId}),
        ]).finally(() => {
            hideBackdrop()
        })
    }

    const textColor = placeholder ? colors.textColor.ha9a9a9 : colors.textColor.h757575;
    const cursor = placeholder ? 'progress' : 'pointer';

    return (
        <Box
            pt={{
                xs: 2,
                md: 3.25
            }}>
            <Typography
                py={0.5}
                variant={"subtitle2"}
                alignItems={'center'}
                color={textColor}
                onClick={handleDelete}
                style={{cursor: cursor, marginLeft: theme.spacing(3)}}>
                <MtIcon
                    icon={"mt-delete"}
                    fontSize={"small"}
                    color={textColor}
                    style={{marginLeft: theme.spacing(1)}}/>
                {lang.get("delete")}
            </Typography>
            <Divider orientation="vertical" flexItem
                     style={{
                         width: 2,
                         backgroundColor: colors.backgroundColor.heff0ef,
                         margin: theme.spacing(1, 0)
                     }}/>
            <Typography
                py={0.5}
                variant={"subtitle2"}
                alignItems={'center'}
                color={textColor}
                onClick={handleAddWhitelist}
                style={{cursor: cursor, marginRight: theme.spacing(3)}}>
                <MtIcon
                    icon={"mt-add-to-favorite"}
                    fontSize={"small"}
                    color={textColor}
                    style={{marginLeft: theme.spacing(1)}}/>
                {lang.get("transfer_to_whitelist")}
            </Typography>
        </Box>
    )
}

function Price({finalPrice, discountPrice, discountPercent, placeholder}) {

    const benefit = finalPrice - discountPrice;

    return (
        <React.Fragment>
            <HiddenSmDown>
                <Box
                    width={{
                        xs: 1,
                        md: 0.3
                    }}
                    flexDirection={'column'} center={true}>
                    {
                        benefit > 0 &&
                        <Typography variant={"subtitle2"} color={colors.textColor.he4254a}>
                            {`${lang.get("discount")} ${UtilsFormat.numberToMoney(benefit)} ${lang.get("toman")}`}
                        </Typography>
                    }
                    <Typography width={1} justifyContent={'center'} pt={2.5} variant={"body1"} fontWeight={500}
                                color={colors.textColor.h000000}
                                alignItems={'center'}>
                        {
                            placeholder ?
                                <Skeleton width={"30%"} height={20}/> :
                                UtilsFormat.numberToMoney(discountPrice)
                        }

                        <Typography pl={0.5} component={"span"} variant={"subtitle1"} fontWeight={400}
                                    color={placeholder ? colors.textColor.ha9a9a9 : colors.textColor.h757575}>
                            {lang.get("toman")}
                        </Typography>
                    </Typography>
                </Box>
            </HiddenSmDown>
            <HiddenMdUp>
                <ProductPrice pt={1} finalPrice={finalPrice} discountPrice={discountPrice}
                              discountPercent={discountPercent}/>
            </HiddenMdUp>
        </React.Fragment>
    )
}

function ProductCount({uniqKey, storageId, count, maxCountForSale,minCountForSale, placeholder, ...props}) {
    const [__, setBackdrop] = useBackdropContext()

    function handleChangeProductCount(newCount) {
        setBackdrop(true)
        ControllerUser.Basket.V2.add({
            storage_id: storageId,
            count: newCount
        }).finally(() => {
            setTimeout(() => {
                setBackdrop(false)
            }, 500)
        })
    }

    return (
        <NumberSelector
            loading={placeholder}
            uniqKey={`basket-item-${uniqKey}`}
            activeNumber={count}
            maxNumber={maxCountForSale}
            minNumber={minCountForSale}
            smButtonProps={{
                fullWidth: true
            }}
            onChange={handleChangeProductCount}
            {...props}/>
    )
}
