import React from "react";
import {Box, Typography, UtilsStyle} from "material-ui-helper";
import {sliderKey} from "./Slider";
import Slider from "../../base/slider/Slider";
import {useTheme} from "@material-ui/core";
import Img from "../../base/Img";
import {colors, lang, media} from "../../../repository";
import {DEBUG} from "../../../pages/_app";
import {makeStyles} from "@material-ui/core/styles";
import {UtilsFormat} from "../../../utils/Utils";
import Link from "../../base/link/Link";
import rout from "../../../router";


const useStyles = makeStyles(theme => ({
    root: {
        "& .swiper-slide": {
            backgroundColor: colors.backgroundColor.hffffff,
            height: 'auto',
            "&:first-child": {
                ...UtilsStyle.borderRadius("0 5px 5px 0"),
                "& .slider-item-holder": {
                    inset: "unset !important",
                }
            },
            "&:last-child": {
                ...UtilsStyle.borderRadius("5px 0 0 5px"),
                "& .slider-item-holder": {
                    inset: "unset !important",
                }
            },

            "& .slider-item-holder": {
                position: "absolute",
                zIndex: 1,
                background: colors.backgroundColor.hffffff,
                inset: "0 -1px  0 -1px",
            }


        }
    }
}));


function SliderSm({data}) {
    const theme = useTheme();
    const classes = useStyles()

    if (!data || !data?.[0])
        return <React.Fragment/>

    return (
        <Box width={1} className={classes.root}>
            <Slider
                scrollbar={true}
                sliderKey={sliderKey}
                sliderButton={false}
                items={data}
                navigation={true}
                slidesPerView={1.6}
                freeMode={true}
                loop={false}
                style={{
                    padding: theme.spacing(0, 2)
                }}
                render={(pr, index) => (
                    <Item
                        key={pr?.id}
                        index={index}
                        data={pr}/>
                )}/>
        </Box>
    )
}

export default React.memo(SliderSm)

const Item = React.memo(function ItemMemo({data}) {
    const theme = useTheme();
    const {id, name, thumbnail, default_storage, permalink} = data || {};
    const {discount_price, discount_percent, final_price} = default_storage || {}
    const {title: thumbnailTitle, image} = thumbnail || {};
    const href = rout.Product.SingleV2.create(permalink)

    return (
        <Link href={href} hasHoverBase={false} disable={!permalink} hasHover={false}>
            <Box
                width={"auto"}
                pt={3.5}
                pb={3}
                px={2}
                style={{position: 'relative'}}>
                <Box
                    className={"slider-item-holder"}/>
                <Box
                    width={1}
                    flexDirection={'column'}
                    zIndex={2}>
                    <Img
                        vw={100 / 1.6 - theme.factor(2)}
                        alt={thumbnailTitle}
                        src={DEBUG ? "/drawable/image/thumbnail.jpg" : image}
                        imageWidth={media.thumbnail.width}
                        imageHeight={media.thumbnail.height}/>
                    <Typography variant={"body2"} pt={1} minHeight={35}>
                        {name?.length <= 64 ? name : name?.slice(0, 61) + "..."}
                    </Typography>
                    <Box pt={1} display={'flex'} justifyContent={"flex-end"} flex={1} alignItems={'center'}
                         flexWrap={'wrap'}>

                        {
                            final_price > discount_price &&
                            <Typography color={'#9E9E9E'} pr={1} component={'del'} variant={'body2'}>
                                {UtilsFormat.numberToMoney(final_price)}
                            </Typography>
                        }
                        {
                            Boolean(discount_percent && discount_percent > 0) &&
                            <Typography
                                variant={'body2'}
                                px={0.2}
                                py={0.5}
                                color={'#fff'}
                                dir={'ltr'}
                                justifyContent={'center'}
                                style={{
                                    minWidth: 38,
                                    backgroundColor: theme.palette.primary.main,
                                    ...UtilsStyle.borderRadius(3)
                                }}>
                                % {discount_percent}
                            </Typography>
                        }
                    </Box>
                    <Typography variant={'subtitle1'} fontWeight={600} alignItems={'center'}
                                justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(discount_price)}
                        <Typography component={'span'} color={'#616161'} variant={'body2'} pl={0.5}>
                            {lang.get("toman")}
                        </Typography>
                    </Typography>
                </Box>
            </Box>
        </Link>
    )
})