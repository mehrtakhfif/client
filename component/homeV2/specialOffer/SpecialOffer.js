import React, {useEffect, useState} from "react";
import {Box, ButtonBase, gLog, HiddenLgUp, HiddenMdDown, Typography, UtilsStyle} from "material-ui-helper";
import {colors, lang} from "../../../repository";
import _ from "lodash"
import moment from 'moment'
import Link from "../../base/link/Link";
import rout from "../../../router";
import Slider from "./Slider";
import {makeStyles, useTheme} from "@material-ui/core";
import MtIcon from "../../MtIcon";
import useSWR from "swr";
import ControllerSite from "../../../controller/ControllerSite";
import {Skeleton} from "@material-ui/lab";


const useSpecialOfferStyles = makeStyles(theme => ({
    rootSpecialOffer: {
        background: `url(/drawable/image/specialOffer.jpg)`,
        backgroundRepeat: "no-repeat !important",
        [theme.breakpoints.down('md')]: {
            background: `url(/drawable/image/specialOfferXs.jpg)`,
            WebkitBackgroundSize: "cover",
            MozBackgroundSize: "cover",
            OBackgroundSize: "cover",
            backgroundSize: "cover",
        }
    }
}));


function SpecialOffer({initialData, seeMoreLink}) {
    const theme = useTheme();
    const classes = useSpecialOfferStyles();
    const [key, req] = ControllerSite.limitedSpecialProduct();
    const {data} = useSWR(key, req, {
        initialData: initialData
    });

    const {products} = data?.data || {};


    if (_.isObject(data) && (_.isEmpty(data) || _.isEmpty(products)))
        return <React.Fragment/>

    const isPlaceholder = _.isEmpty(products)

    return (
        <React.Fragment>
            <Box
                mb={6}
                className={classes.rootSpecialOffer}
                mt={{
                    xs: 2,
                    md: 10
                }}
                display={'flex'}
                flexDirection={'column'}
                minHeight={400}
                alignItems={'center'}>
                <Box
                    display={'flex'}
                    width={1}
                    px={{
                        xs: 2,
                        md: 16
                    }}
                    pt={{
                        xs: 3,
                        md: 11
                    }}
                    flexDirection={{
                        xs: "column",
                        md: 'row'
                    }}
                    alignItems={{
                        xs: "unset",
                        md: 'center'
                    }}>
                    <Box
                        width={{
                            xs: 1,
                            md: 1 / 3
                        }}
                        flexDirection={'column'}>
                        {
                            isPlaceholder ?
                                <Skeleton
                                    variant={"rectangular"}
                                    width={300}
                                    height={40}/>
                                :
                                <Link
                                    href={seeMoreLink}
                                    hoverColor={'#fff'}
                                    fontWeight={500}
                                    pb={3}
                                    component={"h4"}
                                    variant={{
                                        xs: "h6",
                                        md: "h2"
                                    }}
                                    color={'#fff'}
                                    style={{
                                        ...UtilsStyle.widthFitContent()
                                    }}>
                                    {lang.get("special_discounts_today")}
                                </Link>
                        }
                        {seeMoreLink &&
                        <HiddenMdDown>
                            <Box pt={2}>
                                {
                                    isPlaceholder ?
                                        <Skeleton
                                            variant={"rectangular"}
                                            width={200}
                                            height={30}/> :
                                        <Link
                                            href={rout.Product.SingleV2.create(1)}
                                            hasHover={false}
                                            hasHoverBase={false}>
                                            <ButtonBase>
                                                <Typography
                                                    py={1}
                                                    px={2}
                                                    variant={"body2"}
                                                    color={"#fff"}
                                                    style={{
                                                        border: `1px solid #fff`,
                                                        ...UtilsStyle.borderRadius(5)
                                                    }}>
                                                    {lang.get("see_all")}
                                                </Typography>
                                            </ButtonBase>
                                        </Link>
                                }

                            </Box>
                        </HiddenMdDown>}
                    </Box>
                    <Box
                        width={{
                            xs: 1,
                            md: 2 / 3
                        }}
                        pl={{
                            xs: 0,
                            md: 8
                        }}>
                        <Counter/>
                    </Box>
                    {seeMoreLink &&
                    <HiddenLgUp>
                        <Box pt={1}>
                            {
                                isPlaceholder ?
                                    <Skeleton
                                        variant={"rectangular"}
                                        width={200}
                                        height={30}/>
                                    :
                                    <Link
                                        href={rout.Product.SingleV2.create(1)}
                                        hasHover={false}
                                        hasHoverBase={false}>
                                        <Typography
                                            py={1}
                                            color={colors.textColor.hffffff}
                                            alignItems={'center'}
                                            variant={"h6"}>
                                            {lang.get("see_all")}
                                            <MtIcon
                                                fontSize={"small"}
                                                icon={"mt-chevron-left"}
                                                style={{
                                                    marginRight: theme.spacing(1),
                                                    color: colors.textColor.hffffff, fontSize: 16
                                                }}/>
                                        </Typography>
                                    </Link>
                            }
                        </Box>
                    </HiddenLgUp>}
                </Box>
                <Slider products={products || [{}, {}, {}, {}, {}]}/>
            </Box>
        </React.Fragment>
    )
}

export default React.memo(SpecialOffer)

export const containerSpace = '4.16vw';
const counterVariant = {
    xl: "h1",
    lg: "h1",
    md: "h1",
    sm: "h2",
    xs: "h2",
};

const useCounterStyles = makeStyles(theme => ({
    rootTypographyCounter: {
        minWidth: 80,
        [theme.breakpoints.down('md')]: {
            minWidth: 30
        }
    }
}));

function Counter({...props}) {
    const classes = useCounterStyles()

    const [timer, setTimer] = useState({
        hours: undefined,
        minutes: undefined,
        seconds: undefined
    })


    function serializeTime(val) {
        const v = _.toString(val);
        const a = v.length === 1 ? '0' + v : v;
        return a > 0 ? a : '00'
    }

    useEffect(() => {
        const interval = setInterval(() => {
            const du = moment.duration((1731880668 - moment().unix()) * 1000 - interval, 'milliseconds')
            setTimer({
                hours: du.hours(),
                minutes: du.minutes(),
                seconds: du.seconds()
            })
        }, 1000)
        return () => {
            clearInterval(interval)
        }
    }, [])

    return (
        <Box display={'flex'} justifyContent={'flex-end'} dir={'ltr'}>
            {
                !_.isUndefined(timer.hours) &&
                <React.Fragment>
                    <Typography
                        className={classes.rootTypographyCounter}
                        display={'flex'}
                        component={"span"}
                        justifyContent={'center'}
                        variant={counterVariant} fontWeight={500}
                        color={'#fff'}>
                        {serializeTime(timer.hours)}
                    </Typography>
                    <Typography px={1}
                                component={"span"}
                                variant={counterVariant}
                                fontWeight={500} color={'#fff'}>
                        :
                    </Typography>
                    <Typography display={'flex'}
                                className={classes.rootTypographyCounter}
                                component={"span"} justifyContent={'center'}
                                variant={counterVariant} fontWeight={500}
                                color={'#fff'}>
                        {serializeTime(timer.minutes)}
                    </Typography>
                    <Typography px={1}
                                component={"span"}
                                variant={counterVariant} fontWeight={500} color={'#fff'}>
                        :
                    </Typography>
                    <Typography
                        display={'flex'}
                        className={classes.rootTypographyCounter}
                        component={"span"}
                        justifyContent={'center'}
                        fontWeight={500}
                        variant={counterVariant}
                        color={'#fff'}>
                        {serializeTime(timer.seconds)}
                    </Typography>
                </React.Fragment>
            }
        </Box>
    )
}