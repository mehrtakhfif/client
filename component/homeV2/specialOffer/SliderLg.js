import React from "react";
import {Box} from "material-ui-helper";
import {Card, useTheme} from "@material-ui/core";
import Slider, {SliderButton} from "../../base/slider/Slider";
import {sliderKey} from "./Slider";
import ProductCard from "../ProductCard";


const iconButtonTransform = "-30px";

export default function SliderLg({data}) {
    const theme = useTheme()

    if (!data)
        return <React.Fragment/>

    return (
        <Box width={0.8} style={{position: 'relative'}}>
            <Box width={1} component={Card}>
                <Slider
                    scrollbar={true}
                    sliderKey={sliderKey}
                    sliderButton={false}
                    items={data}
                    navigation={true}
                    breakpoints={{
                        0: {
                            slidesPerView: 2,
                            freeMode: true,
                        },
                        678: {
                            slidesPerView: 3,
                        },
                        1280: {
                            slidesPerView: 3.5,
                            slidesPerGroup: 3,
                        },
                    }}
                    loop={false}
                    containerProps={{
                        pb: 6
                    }}
                    style={{
                        padding: theme.spacing(8, 0)
                    }}
                    render={(pr, index) => {
                        return(
                            <ProductCard
                                key={pr?.id}
                                soldOut={true}
                                product={pr}/>
                        )
                    }}/>
            </Box>
            <SliderButton
                isNextButton={true}
                sliderKey={sliderKey}
                buttonRootProps={{
                    style: {
                        left: iconButtonTransform
                    }
                }}/>
            <SliderButton
                isNextButton={false}
                sliderKey={sliderKey}
                buttonRootProps={{
                    style: {
                        right: iconButtonTransform
                    }
                }}/>
        </Box>

    )
}


// function Item({data: pr, i}) {
//     const iPlus = i + 1;
//     return (
//         <Link href={`/${pr.id}`} hasHoverBase={false} hasHover={false}>
//             <ProductCard
//                 item={pr}
//                 style={{
//                     borderLeft: (iPlus % 4 !== 0 && iPlus < 2) ? `1px solid #0000` : undefined
//                 }}/>
//         </Link>
//     )
// }

// function ProductCard({item: it, ...props}) {
//     const theme = useTheme();
//
//     return (
//         <Box display={'flex'}
//              flexDirection={'column'}
//              width={1}
//              px={2}
//              py={2}
//              {...props}>
//             <Box>
//                 <Img
//                     imageWidth={media.thumbnail.width}
//                     imageHeight={media.thumbnail.height}
//                     alt={it.thumbnail.title}
//                     src={DEBUG ? "/drawable/image/thumbnail.jpg" : it.thumbnail.image}/>
//             </Box>
//             <Box px={2} display={'flex'} flexDirection={'column'}>
//                 <Typography component={'h6'} fontWeight={400}
//                             variant={'subtitle2'}
//                             pb={1.25}
//                             minHeight={55}>
//                     {it.title.length <= 68 ? it.title : it.title.slice(0, 66) + "..."}
//                 </Typography>
//                 <Box
//                     alignItems={'center'}
//                     display={'flex'}>
//                     {
//                         Boolean(it.discount_percent && it.discount_percent > 0) &&
//                         <Typography
//                             variant={'body1'}
//                             px='0.729vw'
//                             py={0.5}
//                             color={'#fff'}
//                             dir={'ltr'}
//                             justifyContent={'center'}
//                             style={{
//                                 minWidth: 38,
//                                 backgroundColor: theme.palette.primary.main,
//                                 ...UtilsStyle.borderRadius(3)
//                             }}>
//                             % {it.discount_percent}
//                         </Typography>
//                     }
//                     <Box display={'flex'} flex={1} pl={0.1} dir={'ltr'} alignItems={'center'} flexWrap={'wrap'}>
//                         <Typography variant={'subtitle1'} fontWeight={600} direction={'ltr'} alignItems={'center'}>
//                             <Typography component={'span'} color={'#616161'} variant={'body2'} pl={0.5}>
//                                 {lang.get("toman")}
//                             </Typography>
//                             {UtilsFormat.numberToMoney(it.discount_price)}
//                         </Typography>
//                         {
//                             it.final_price > it.discount_price &&
//                             <Typography color={'#9E9E9E'} pr={1} component={'del'} variant={'body2'} direction={'ltr'}>
//                                 {UtilsFormat.numberToMoney(it.final_price)}
//                             </Typography>
//                         }
//                     </Box>
//                 </Box>
//             </Box>
//         </Box>
//     )
// }