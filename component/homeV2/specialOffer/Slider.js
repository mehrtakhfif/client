import React from "react";
import {Box, HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import SliderLg from "./SliderLg";
import SliderSm from "./SliderSm";


export const sliderKey = "special-offer-slider";
export default function SpecialOfferSlider({products}) {

    return (
        <Box
            width={1}
            mt={{
                xs: 2,
                md: 7.5
            }}
            mb={{
                xs: 3,
                md: 0
            }}
            justifyContent={'center'}>
            <HiddenLgUp>
                <SliderSm data={products}/>
            </HiddenLgUp>
            <HiddenMdDown>
                <SliderLg data={products}/>
            </HiddenMdDown>
        </Box>
    )
}

