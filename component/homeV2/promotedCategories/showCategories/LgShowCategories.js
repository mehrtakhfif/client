import React from "react";
import {Box} from "material-ui-helper";
import CategoryItem from "./CategoryItem";


export default function LgShowCategories({list,...props}){



    return (
        <Box width={1} flexWrap={'wrap'} justifyContent={'center'} {...props}>
            {list?.map((item,index) => {
                return (
                    <CategoryItem isLg={true} key={item?.id} data={item}/>
                )
            })}
        </Box>
    )
}