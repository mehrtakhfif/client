import React from "react";
import Link from "../../../base/link/Link";
import rout from "../../../../router";
import {Box, Typography} from "material-ui-helper";
import Img from "../../../base/Img";
import {
    colors, media
        as siteMedia
} from "../../../../repository";
import {DEBUG} from "../../../../pages/_app";
import {useTheme} from "@material-ui/core";
import {lgPx} from "../PromotedCategories";
import _ from "lodash"
import {Skeleton} from "@material-ui/lab";

export default function CategoryItem({isLg, data}) {
    const theme = useTheme();
    const {id, permalink, name, media} = data;

    const isPlaceholder = _.isEmpty(data)

    return (
        <Box
            width={{
                xs: 1,
                md: 1 / 7
            }}>
            <Box
                width={1}
                justifyContent={'center'}
                pt={{
                    xs: 2,
                    md: 3
                }}
                px={{
                    xs: 1,
                    md: 4
                }}>
                <Link
                    hasHoverBase={false}
                    hasHover={false}
                    disable={isPlaceholder}
                    href={rout.Product.Search.create({category: permalink})}
                    flexDirection={'column'}
                    style={{
                        width: "100%"
                    }}>
                    <Box
                        justifyContent={'center'}
                        width={1}
                        mb={1}>
                        <Img
                            borderRadius={"fill"}
                            imageHeight={siteMedia.promotedCategories.height}
                            imageWidth={siteMedia.promotedCategories.width}
                            alt={media?.alt}
                            src={media?.image}/>
                    </Box>
                    <Typography variant={'body2'} pt={1} center={true} fontWeight={'normal'}
                                style={{textAlign: 'center'}}>
                        {
                            name ?
                                name :
                                <Skeleton
                                    variant={"text"}
                                    width={80}
                                    height={40}/>
                        }
                    </Typography>
                </Link>
            </Box>
        </Box>
    )
}