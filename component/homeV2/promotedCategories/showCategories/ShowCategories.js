import React from "react";
import {HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import LgShowCategories from "./LgShowCategories";
import SmShowCategories from "./SmShowCategories";


//TODO: what is cat image Width and Height
export default function ShowCategories({list,...props}) {



    return (
        <React.Fragment>
            <HiddenLgUp>
                <SmShowCategories list={list} {...props}/>
            </HiddenLgUp>
            <HiddenMdDown>
                <LgShowCategories list={list} {...props}/>
            </HiddenMdDown>
        </React.Fragment>
    )

}