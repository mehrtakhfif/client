import React from "react";
import {Box} from "material-ui-helper";
import Slider from "../../../base/slider/Slider";
import {sliderKey} from "../../specialOffer/Slider";
import CategoryItem from "./CategoryItem";
import {useTheme} from "@material-ui/core";
import _ from 'lodash'


export const slidesPerView = 3.6
export const pxSm = 2
export default function SmShowCategories({list, ...props}) {
    const theme = useTheme();

    if (_.isEmpty(list) || _.isEmpty(list?.[0]))
        return <React.Fragment/>


    return (
        <Box width={1} {...props}>
            <Slider
                scrollbar={false}
                sliderKey={sliderKey}
                sliderButton={false}
                items={list}
                navigation={true}
                slidesPerView={slidesPerView}
                freeMode={true}
                loop={false}
                style={{
                    padding: theme.spacing(0, pxSm)
                }}
                render={(item, index) => (
                    <CategoryItem key={item?.id} data={item}/>
                )}/>
        </Box>
    )
}