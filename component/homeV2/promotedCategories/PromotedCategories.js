import React from "react";
import {Box, gLog, Typography} from "material-ui-helper";
import ControllerSite from "../../../controller/ControllerSite";
import useSWR from "swr";
import {lang} from "../../../repository";
import ShowCategories from "./showCategories/ShowCategories";
import _ from "lodash"

export const smPx = 0;
export const lgPx = 16;
function PromotedCategories({initialData}) {


    const [key, req] = ControllerSite.promotedCategories()
    const {data, error} = useSWR(key, req, {
        initialData: initialData
    });
    const {product, service} = data || {}

    const showProduct = !(_.isObject(data) && _.isEmpty(product))
    const showService = !(_.isObject(data) && _.isEmpty(service))


    if (!(showProduct || showService))
        return <React.Fragment/>

    return (
        <Box
            pt={{
                xs: 2,
                md: 6
            }}
            px={{
                xs: smPx,
                md: lgPx
            }}
            flexDirection={'column'}
            alignItems={'center'}>
            {
                showProduct &&
                <React.Fragment>
                    <Title title={"product_group"}/>
                    <ShowCategories list={product || [{}, {}, {}, {}, {}]}/>
                </React.Fragment>
            }
            {
                showService &&
                <React.Fragment>
                    <Title
                        pt={{
                            xs: 7,
                            md: 9
                        }}
                        title={"service_group"}/>
                    <ShowCategories list={service || [{}, {}, {}, {}, {}]}/>
                </React.Fragment>
            }
        </Box>
    )
}


export default React.memo(PromotedCategories)

function Title({title, ...props}) {

    return (
        <Typography width={1}
                    px={2}
                    justifyContent={{
                        xs: 'flex-start',
                        md: 'center'
                    }}
                    varinat={'h5'} fontWeight={600} pb={2} {...props}>
            {lang.get(title)}
        </Typography>
    )
}

