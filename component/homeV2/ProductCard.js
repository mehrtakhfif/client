import React from "react";
import Link from "../base/link/Link";
import rout from "../../router";
import {Box, HiddenLgUp, HiddenMdDown, Typography, UtilsStyle} from "material-ui-helper";
import Img from "../base/Img";
import {lang, media} from "../../repository";
import {DEBUG} from "../../pages/_app";
import {UtilsFormat} from "../../utils/Utils";
import {useTheme} from "@material-ui/core";
import {Skeleton} from "@material-ui/lab";


function ProductCard({soldOut, product, linkProps = {}, ...props}) {
    const theme = useTheme();


    if (!product)
        return <React.Fragment/>

    const {thumbnail, name, permalink, default_storage} = product || {}
    const {discount_percent, discount_price, final_price} = default_storage || {}
    const href = rout.Product.SingleV2.create(permalink)

    return (
        <Link href={href} hasHoverBase={false} disable={!permalink} hasHover={false} {...linkProps}>
            <Box display={'flex'}
                 flexDirection={'column'}
                 justifyContent={'center'}
                 width={1}
                 px={2}
                 py={2}
                 {...props}>
                <Box justifyContent={'center'} pb={2}>
                    <Img
                        imageWidth={media.thumbnail.width}
                        imageHeight={media.thumbnail.height}
                        alt={thumbnail?.title}
                        src={thumbnail?.image}/>
                </Box>
                <Box
                    px={{
                        xs: 0,
                        md: 2
                    }}
                    display={'flex'} flexDirection={'column'}>
                            <Typography component={'h6'} fontWeight={400}
                                        variant={'subtitle2'}
                                        mb={1.25}
                                        minHeight={55}
                                        style={{
                                            ...UtilsStyle.limitTextLine(2)
                                        }}>
                    {
                        name?
                                name:
                            <Skeleton
                                variant={"text"}
                                height={50}
                                style={{
                                    width:"100%",
                                    marginBottom:theme.spacing(1.25)
                                }}/>
                    }
                            </Typography>
                    <ShowPrice
                        soldOut={soldOut}
                        finalPrice={final_price} discountPrice={discount_price}
                        discountPercent={discount_percent}/>
                </Box>
            </Box>
        </Link>
    )
}

export default React.memo(ProductCard)

function ShowPrice({soldOut, discountPercent, discountPrice, finalPrice}) {
    const theme = useTheme()


    if (discountPrice <= 0){
    if (soldOut)
        return (
            <Typography variant={"subtitle2"} fontWeight={500} color={theme.palette.error.light}>
                اتمام موجودی
            </Typography>
        )
        return <React.Fragment/>
    }


    return (
        <React.Fragment>
            <HiddenLgUp>
                <Box dir={'ltr'} flexDirection={'column'}>
                    <Box alignItems={'center'}>
                        <DiscountPercent minWidth={25} discountPercent={discountPercent}/>
                        <FinalPrice finalPrice={finalPrice}/>
                    </Box>
                    <DiscountPrice discountPrice={discountPrice}/>
                </Box>
            </HiddenLgUp>
            <HiddenMdDown>
                <Box
                    alignItems={'center'}
                    display={'flex'}>
                    <DiscountPercent minWidth={38} discountPercent={discountPercent}/>
                    <Box display={'flex'} flex={1} pl={0.1} dir={'ltr'} alignItems={'center'} flexWrap={'wrap'}>
                        <DiscountPrice discountPrice={discountPrice}/>
                        <FinalPrice discountPrice={discountPrice} finalPrice={finalPrice}/>
                    </Box>
                </Box>
            </HiddenMdDown>
        </React.Fragment>
    )
}

function DiscountPercent({minWidth, discountPercent}) {
    const theme = useTheme()

    if (!Boolean(discountPercent && discountPercent > 0))
        return <React.Fragment/>


    return (
        <Typography
            variant={'body1'}
            px={{
                xs: 0.5,
                md: 1.2
            }}
            py={{
                xs: 0.3,
                md: 0.5
            }}
            color={'#fff'}
            dir={'ltr'}
            justifyContent={'center'}
            style={{
                minWidth: minWidth,
                backgroundColor: theme.palette.primary.main,
                ...UtilsStyle.borderRadius(3)
            }}>
            {discountPercent}%
        </Typography>
    )
}


function DiscountPrice({discountPrice}) {
    if (discountPrice <= 0)
        return <React.Fragment/>

    return (
        <Typography variant={'subtitle1'} fontWeight={600} direction={'ltr'} alignItems={'center'}>
            <Typography component={'span'} color={'#616161'} variant={'body2'} pl={0.5}>
                {lang.get("toman")}
            </Typography>

            {
                discountPrice !== undefined?
                UtilsFormat.numberToMoney(discountPrice):
                    <Skeleton
                        variant={"text"}
                        width={75}
                        height={30}/>

            }
        </Typography>
    )
}

function FinalPrice({discountPrice, finalPrice}) {

    if (finalPrice <= discountPrice)
        return <React.Fragment/>


    return (
        <Typography color={'#9E9E9E'} pr={1} component={'del'} variant={'body2'}
                    direction={'ltr'}>
            {UtilsFormat.numberToMoney(finalPrice)}
        </Typography>
    )
}