import React from "react";
import {Box} from "material-ui-helper";
import useSWR from "swr";
import ControllerSite from "../../controller/ControllerSite";
import SwiperCore, {Navigation, Thumbs} from "swiper";
import {useTheme} from "@material-ui/core";
import FullScreenSlider, {createSliderItem} from "../base/slider/FullScreenSlider";
import {DEBUG} from "../../pages/_app";
import {createImage} from "../base/Img";
import {media} from "../../repository";


SwiperCore.use([Navigation, Thumbs]);


// const useStyle = makeStyles(theme => ({
//     homeSlider:{
//         '& .swiper-slide>div>a>span':{
//             width:'100% !important'
//         }
//     }
// }));


export default function HomeSlider({initialData}) {
    const theme = useTheme();
    // const classes = useStyle()
    const [key, req] = ControllerSite.Slider.getHomeSlider()
    const {data} = useSWR(key, req, {
        initialData: initialData
    })

    const {slider} = data || {slider: [{},{},{},{}]}

    return (
        <Box
            className={"homeSlider"}
            width={1}
            pt={{
                xs: 1,
                md: 3,
                xl: 5,
            }}
            display={'block'}>
            <FullScreenSlider
                sliderKey={"home"}
                loop={true}
                items={slider.map((item, index) => createSliderItem({
                    id: item?.id,
                    imageSrc: item?.image,
                    imageAlt: item?.media?.title || `src-placeholder-${index}`,
                    url: item?.url,
                    disable: item?.disable
                }))}/>
        </Box>
    )
}