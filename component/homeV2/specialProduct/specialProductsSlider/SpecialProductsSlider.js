import React, {useEffect, useState} from "react";
import {Box, gLog, HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import Link from "../../../base/link/Link";
import {lang} from "../../../../repository";
import rout from "../../../../router";
import MtIcon from "../../../MtIcon";
import {useTheme} from "@material-ui/core";
import Slider, {SliderButton} from "../../../base/slider/Slider";
import ProductCard from "../../ProductCard";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Skeleton} from "@material-ui/lab";
import useInView from "react-cool-inview";

const iconButtonTransform = "30px";


export const pySpacing = {
    xs: 2.5,
    md: 3
}


const useStyle = makeStyles(theme => ({
    sectionSlider: {},
}));


function SpecialProductsSlider({data}) {
    const theme = useTheme();
    const [init, setInit] = useState(false)

    useEffect(() => {
        // if (isClient()){
        //     setInit(true)
        //     return
        // }
        setTimeout(() => {
            setInit(true)
        }, 500)
    }, [])


    const {id, name, permalink, special_products} = data;
    const href = rout.Product.Search.create({category: permalink});

    const sliderKey = `SpecialProductsSlider-${id}`


    return (
        <React.Fragment>
            <Box
                width={1}
                px={{
                    xs: 2,
                    md: 10
                }}
                pt={{
                    xs: 2.5,
                    md: 3
                }}>
                <Box flex={1}>
                    <Link
                        disable={!href}
                        href={href}
                        variant={'h5'}
                        fontWeight={500}
                        hasHoverBase={false}
                        hasHover={false}>
                        {
                            name ? name :
                                <Skeleton width={180} height={50}/>
                        }
                    </Link>
                </Box>
                <Link
                    variant={"subtitle2"}
                    fontWeight={400}
                    disable={!href}
                    href={href}
                    hasHoverBase={false}
                    hasHover={false}
                    alignItems={'center'}>
                    <HiddenMdDown>
                        {lang.get("see_all")}
                    </HiddenMdDown>
                    <MtIcon
                        icon={"mt-chevron-left"}
                        style={{
                            marginRight: theme.spacing(1),
                            fontSize: theme.spacing(2.5)
                        }}/>
                </Link>
            </Box>
            <HiddenLgUp>
                <SpecialSlider sliderKey={sliderKey} specialProducts={special_products}/>
            </HiddenLgUp>
            <HiddenMdDown>
                <SpecialSlider
                    sliderKey={sliderKey}
                    specialProducts={special_products}/>
            </HiddenMdDown>
        </React.Fragment>
    )
}

export default React.memo(SpecialProductsSlider)

function SpecialSlider({sliderKey, specialProducts, ...props}) {

    const {observe, inView} = useInView({
        onEnter: ({unobserve}) => unobserve(),
    });

    return (
        <Box
            width={1}
            pb={pySpacing}
            pt={3}
            {...props}
            style={{
                position: 'relative',
                ...props?.style
            }}>
            <Slider
                scrollbar={false}
                sliderKey={sliderKey}
                sliderButton={false}
                items={specialProducts}
                navigation={true}
                slidesPerView={5}
                slidesPerGroup={5}
                breakpoints={{
                    0: {
                        slidesPerView: 2.1,
                        freeMode: true,
                    },
                    678: {
                        slidesPerView: 3.2,
                        freeMode: true,
                    },
                    1280: {
                        slidesPerView: 5,
                        slidesPerGroup: 5,
                    },
                }}
                loop={false}
                render={(pr) => (
                    <React.Fragment>
                        <HiddenLgUp>
                            <ProductCard
                                product={pr}/>
                        </HiddenLgUp>
                        <HiddenMdDown>
                            <ProductCard
                                product={pr}/>
                        </HiddenMdDown>
                    </React.Fragment>
                )}/>
            <HiddenMdDown>
                <SliderButton
                    isNextButton={true}
                    sliderKey={sliderKey}
                    buttonRootProps={{
                        style: {
                            left: iconButtonTransform
                        }
                    }}/>
                <SliderButton
                    isNextButton={false}
                    sliderKey={sliderKey}
                    buttonRootProps={{
                        style: {
                            right: iconButtonTransform
                        }
                    }}/>
            </HiddenMdDown>
        </Box>
    )
}