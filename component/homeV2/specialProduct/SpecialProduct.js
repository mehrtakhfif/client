import React, {useEffect} from "react";
import {Box, gLog, useState} from "material-ui-helper";
import ControllerSite from "../../../controller/ControllerSite";
import useSWR from "swr";
import _ from 'lodash';
import Ads from "./Ads";
import SpecialProductsSlider from "./specialProductsSlider/SpecialProductsSlider";


export default function SpecialProduct({initialAdsData, initialSpecialProductData}) {
    const [ads, setAds] = useState();
    const [adsKey, adsReq] = ControllerSite.ADS.getHome()
    const [specialProductKey, specialProductReq] = ControllerSite.specialProduct()
    const {data: adsD} = useSWR(adsKey, adsReq, {
        initialData: initialAdsData
    });
    const {data: specialD} = useSWR(specialProductKey, specialProductReq, {
        initialData: initialSpecialProductData
    });

    const {products: special} = specialD || {};


    useEffect(() => {
        const {ads} = adsD || (_.isEmpty(specialD) ? {ads:[{settings: {size: 1}}, {settings:{size: 2}}, {settings:{size: 2}}]} : []);
        if (_.isEmpty(ads))
            return

        const adsR = []

        function add(item, part) {
            const index = _.findIndex(adsR, ({data = [], size}) => {
                return size === part && data.length < part;
            })
            if (index === -1) {
                adsR.push({
                    data: [item],
                    size: part
                })
                return
            }
            adsR[index].data.push(item)
        }

        _.forEach(ads, it => {
            if (it?.settings?.size === 4) {
                add(it, 4)
                return
            }
            if (it?.settings?.size === 2) {
                add(it, 2)
                return
            }
            add(it, 1)
        })

        setAds(adsR)
    }, [adsD, specialD])

    if (!special)
        return <React.Fragment/>

    return (
        <Box
            width={1}
            flexDirection={'column'}
            pt={{
                xs: 5,
                md: 6
            }}>
            {
                special?.map((spItem, index) => {
                    const adsItem = ads?.[index]

                    return (
                        <React.Fragment key={spItem?.id || index}>
                            <Ads item={adsItem}/>
                            <SpecialProductsSlider
                                data={spItem}/>
                        </React.Fragment>
                    )
                })
            }
        </Box>
    )
}