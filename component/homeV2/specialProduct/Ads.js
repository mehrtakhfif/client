import React from "react";
import {Box, gLog, HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import Link from "../../base/link/Link";
import {media as baseMedia} from "../../../repository"
import Img from "../../base/Img";
import {pySpacing} from "./specialProductsSlider/SpecialProductsSlider";


const px = {
    4: {
        root: {
            xs: 1,
            md: 8.5
        },
        item: {
            xs: 1,
            sm: 1.5
        },
        size: {
            xs: 1 / 2,
            sm: 1 / 4
        },
        py: {
            xs: 1,
            md: 0
        },
        vw:{
            xs:44,
            md:22
        }
    },
    2: {
        root: {
            xs: 1,
            sm: 8
        },
        item: {
            xs: 1,
            sm: 2
        },
        size: 1 / 2,
        vw:{
            xs:42.5,
            md:44
        }
    },
    1: {
        root: {
            xs: 2,
            sm: 10
        },
        item: 0,
        size: 1,
        vw:{
            xs:91,
            md:92
        }
    }
}

export default function Ads({item}) {

    const {data, size} = item || {};

    return (
        <Box px={px[size]?.root} py={pySpacing} flexWrap={'wrap'}>
            {
                data?.map(({id, title,image, url, settings}) => {
                    return (
                        <React.Fragment key={id}>
                            <HiddenLgUp>
                                <AdItem
                                    id={id}
                                    url={url}
                                    title={title}
                                    src={image}
                                    size={size}
                                    isSm={true}/>
                            </HiddenLgUp>
                            <HiddenMdDown>
                                <AdItem
                                    id={id}
                                    url={url}
                                    title={title}
                                    src={image}
                                    size={size}
                                    isSm={false}/>
                            </HiddenMdDown>
                        </React.Fragment>

                    )
                })
            }
        </Box>
    )
}

function AdItem({id, url, src, title, size, isSm}) {
    const {width: imageWidth, height: imageHeight} = !isSm ? baseMedia.ads[size] : baseMedia.ads[size]?.sm;


    return (
        <Box
            px={px[size]?.item}
            py={px[size]?.py}
            id={"item-" + id}
            width={px[size]?.size}>
            <Link
                disable={!url}
                href={url}
                hasHover={false}
                hasHoverBase={false}
                style={{
                    width: "100%",
                    display:'flex',
                    justifyContent:'center'
                }}>
                <Img
                    src={src}
                    alt={title}
                    imageWidth={imageWidth}
                    imageHeight={imageHeight}
                    borderRadius={isSm ? 12 : 20}/>
            </Link>
        </Box>
    )
}