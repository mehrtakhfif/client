import React from "react";
import {Box, Typography, UtilsStyle} from "material-ui-helper";
import MtIcon from "../../../MtIcon";
import {colors, lang} from "../../../../repository";
import {useTheme} from "@material-ui/core";


export default function AddressItem({address: ad, active, onRequestChangeAddress, onClick}) {
    const theme = useTheme()
    const {id, address, name, phone, postal_code, state, city} = ad || {};
    const {name: stateName} = state;
    const {name: cityName} = city;


    return (
        <Box>
            <Box pr={2} alignItems={'center'}>
                <MtIcon
                    color={theme.palette.secondary.main}
                    icon={"mt-radio-button-checked"}/>
            </Box>
            <Box width={1} flexDirection={'column'}>
                <Box
                    width={1}
                    py={2}
                    px={4.5}
                    flexDirection={'column'}
                    style={{
                        border: `1px solid ${theme.palette.secondary.main}`,
                        ...UtilsStyle.borderRadius(10)
                    }}>
                    <Typography variant={'body1'} fontWeight={400}>
                        {`${stateName}، ${cityName}، ${address}`}
                    </Typography>
                    <Item label={lang.get("phone_number")} value={phone}/>
                    <Item label={lang.get("recipient_name")} value={name}/>
                </Box>
                {
                    onRequestChangeAddress &&
                    <Typography
                        pt={2}
                        alignItems={'center'}
                        variant={'subtitle2'}
                        color={theme.palette.secondary.main}
                        fontWeight={500}
                        onClick={onRequestChangeAddress}
                        style={{
                            cursor: 'pointer'
                        }}>
                        {lang.get("change_or_edit_the_address")}
                        <MtIcon
                            icon={"mt-chevron-left"}
                            color={theme.palette.secondary.main}
                            style={{
                                marginRight: theme.spacing(1),
                                fontSize: theme.spacing(2.5)
                            }}/>
                    </Typography>
                }
            </Box>
        </Box>
    )
}


function Item({label, value}) {
    const theme = useTheme();
    return (
        <Box pt={1.5}>
            <Typography
                pr={4}
                variant={"body2"}
                color={colors.textColor.h757575}
                style={{
                    minWidth: theme.spacing(16)
                }}>
                {label}
            </Typography>
            <Typography variant={"body2"} color={colors.textColor.h404040} fontWeight={400}>
                {value}
            </Typography>
        </Box>
    )
}