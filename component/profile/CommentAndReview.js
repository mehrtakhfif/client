import React, {Fragment, useEffect, useMemo, useState} from "react";
import Box from "@material-ui/core/Box";
import {useSnackbar} from "notistack";
import _ from "lodash";
import ControllerUser, {wishlistDefaultStep} from "../../controller/ControllerUser";
import Fade from "@material-ui/core/Fade";
import {Card} from "@material-ui/core";
import Img from "../base/oldImg/Img";
import {Utils, UtilsStyle} from "../../utils/Utils";
import Rating from "@material-ui/lab/Rating";
import {colors, GA, lang, theme} from "../../repository";
import rout from "../../router";
import BaseButton from "../base/button/BaseButton";
import {amber, cyan, green} from "@material-ui/core/colors";
import Typography from "../base/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import Button from "@material-ui/core/Button";
import ButtonLink from "../base/link/ButtonLink";
import Head from "next/head";
import {getPageTitle} from "../../headUtils";
import useSWRInfinite from "../../lib/useSWRInfinite";

const rowsPerPage = wishlistDefaultStep;
export default function CommentAndReview({...props}) {
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.Main.Params.CommentsAndReview.param});
    }, []);
    let undo = false;
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        newRemovedItem: null,
    });


    // const d = ControllerUser.User.Profile.Comment.get();
    // const {
    //     pages,
    //     isLoadingMore,
    //     isReachingEnd,
    //     loadMore,
    //     pageSWRs,
    //     ...f
    // } = useSWRPages(
    //     'profile-comment-page',
    //     ({offset, withSWR}) => {
    //         const {data, error} = withSWR(
    //             // eslint-disable-next-line react-hooks/rules-of-hooks
    //             useSWR(d[0] + (offset || 0) + 1, () => {
    //                 return d[1]({page: (offset || 0) + 1, step: rowsPerPage})
    //             })
    //         );
    //
    //         if (error) {
    //             return <ComponentError tryAgainFun={() => mutate(d[0])}/>
    //         }
    //
    //         if (!data) {
    //             return (
    //                 <React.Fragment>
    //                     {placeHolder(1)}
    //                 </React.Fragment>
    //             )
    //         }
    //
    //
    //         if (_.isEmpty(data.data)) {
    //             return (
    //                 <Box px={2} py={5} width={1} display={'flex'} alignItems={'center'}
    //                      justifyContent={'center'}>
    //                     <Typography variant={'h6'}>
    //                         {lang.get('list_is_empty')}.
    //                     </Typography>
    //                 </Box>
    //             )
    //         }
    //
    //
    //         return data.data.map((c, i) => (
    //             <CommentItem index={c} key={c.id}
    //                          text={c.text}
    //                          product={c.product}
    //                          type={c.type}
    //                          rate={c.rate}
    //                          approved={c.approved}
    //                          approvedLabel={c.approvedLabel}
    //                          removeDisable={state.removedItem}
    //                          onRemoveCommentItem={() => onRemoveCommentItem(i, c)}
    //                          notify={c.notify} {...props}/>
    //         ))
    //     },
    //     // get next page's offset from the index of current page
    //     (SWR, index) => {
    //         if (SWR.data.pagination && SWR.data.pagination.lastPage <= index + 1) return null;
    //         return (index + 1)
    //     },
    //     []
    // );


    const {getKey, fetcher} = ControllerUser.User.Profile.Comment.V2.Get;
    const api = useMemo(() => getKey(), []);

    //region swr
    const {
        data,
        error,
        page,
        maxPage,
        allCount,
        step,
        mutate,
        setNextPage,
        onClear,
        isEmpty,
        isLoadingMore,
        isReachingEnd,
        isRefreshing
    } = useSWRInfinite(api, fetcher, {
        revalidateOnFocus: false,
    })
    //endregion swr



    function onRemoveCommentItem(index, itemIndex, item) {
        if (state.removedItem) {
            return
        }
        const data = state.data[index];
        _.remove(data, {
            id: item.id
        });
        const newList = state.data;
        newList[index] = data;
        setState({
            ...state,
            data: newList,
            removedItem: item
        });
        enqueueSnackbar(lang.get("me_product_is_removed"),
            {
                variant: "success",
                action: (key) => (
                    <Fragment>
                        <Button
                            onClick={() => {
                                setState({
                                    ...state,
                                    removedItem: null
                                });
                                closeSnackbar(key);
                            }}
                            style={{
                                marginLeft: theme.spacing(1),
                                color: '#fff'
                            }}>
                            {lang.get('close')}
                        </Button>
                        <Button
                            onClick={() => {
                                undoCommentItem(index, itemIndex, item);
                                undo = true;
                                closeSnackbar(key);
                            }}
                            style={{
                                backgroundColor: green[700],
                                color: '#fff'
                            }}>
                            {lang.get('undo')}
                        </Button>
                    </Fragment>
                ),
                onExited: () => {
                    if (undo)
                        setState({
                            ...state,
                            removedItem: null
                        });
                    else
                        removeCommentItemRequest(index, itemIndex, item);
                    undo = false;
                },
                resumeHideDuration: 1500
            });
    }

    function undoCommentItem(index, itemIndex, item) {
        const newItemList = Utils.insert(state.data[index], itemIndex, item);
        const newList = state.data;
        newList[index] = newItemList;
        setState({
            ...state,
            data: newList,
            newRemovedItem: null
        })
    }

    function removeCommentItemRequest(index, itemIndex, item) {
        setState({
            ...state,
            newRemovedItem: null
        });
        ControllerUser.User.Profile.Comment.remove({wishlist_id: item.id}).then(() => {
        }).catch(() => {
            undoCommentItem(index, itemIndex, item)
        })
    }

    const placeHolder = (index) => {
        index = _.isNumber(index) ? index : 1;
        return (
            <Box width={1} display={'flex'} flexWrap={'wrap'} key={index}>
                {[1, 2, 3, 4].map((i) => (
                    <Box key={index + '-' + i}
                         width={{
                             xs: 1,
                             lg: 1 / 2
                         }}
                         display={'flex'} px={3} py={2}>
                        <Box component={Card}
                             py={3}
                             width={1}
                             display={'flex'}
                             alignItems={'center'}
                             justifyContent={'center'}>
                            <Skeleton variant="rect" height={150}
                                      style={{
                                          width: '100%',
                                          maxWidth: '95%'
                                      }}/>
                        </Box>
                    </Box>
                ))}
            </Box>
        )
    };

    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Head>
                <title>{getPageTitle(lang.get("comments_and_review"))}</title>
            </Head>
            <Box width={1} display={'flex'}
                 flexWrap={'wrap'}>
                {
                        isEmpty ?
                            <Box px={2} py={5} width={1} display={'flex'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <Typography variant={'h6'}>
                                    {lang.get('list_is_empty')}.
                                </Typography>
                            </Box> :
                            <React.Fragment>
                                {data.map((c, i) => (
                                    <CommentItem index={c} key={c.id}
                                                 text={c.text}
                                                 product={c.product}
                                                 type={c.type}
                                                 rate={c.rate}
                                                 approved={c.approved}
                                                 approvedLabel={c.approvedLabel}
                                                 removeDisable={state.removedItem}
                                                 onRemoveCommentItem={() => onRemoveCommentItem(i, c)}
                                                 notify={c.notify} {...props}/>
                                ))}
                                {isLoadingMore && placeHolder(1)}
                            </React.Fragment>

                }
            </Box>
            {(!isReachingEnd && !isLoadingMore) &&
            <Box flex={1} py={2} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                <BaseButton
                    variant={'outlined'}
                    onClick={setNextPage}>
                    نمایش بیشتر
                </BaseButton>
            </Box>
            }
        </Box>
    )
}

function CommentItem({
                         index,
                         text,
                         type,
                         rate,
                         approved,
                         approvedLabel,
                         product,
                         removeDisable = false,
                         onRemoveCommentItem,
                         ...props
                     }) {
    const approvedColor = useMemo(() => approved === null ? amber[700] : approved ? colors.success.main : colors.danger.main, [approved]);
    return (
        <Fade in={true}
              style={{transformOrigin: '0 0 0'}}
              timeout={1000}>
            <Box display={'flex'}
                 elevation={0}
                 width={{
                     xs: 1 / 2,
                     lg: 1 / 3
                 }} py={1} px={1.5}>
                <Box component={Card} width={1}
                     display={'flex'}
                     alignItems={'center'}
                     justifyContent={'center'}
                     flexWrap={'wrap'} py={2} px={1}>
                    <Box mr={2} display={'flex'} alignItems={'center'}
                         flexDirection={'column'}>
                        <Img src={product.thumbnail}
                             alt={product.name}
                             maxWidth={{
                                 xs: 80,
                                 md: 150,
                                 lg: 100,
                             }}
                             imgStyle={{
                                 width: '100%',
                                 ...UtilsStyle.borderRadius(5)
                             }}/>
                        {rate ?
                            <Box display={'flex'} flexDirection={'column'} justifyContent={'center'}
                                 alignItems={'center'}
                                 px={1} py={2}>
                                <Typography variant={'body1'} pb={1}
                                            style={{
                                                ...UtilsStyle.disableTextSelection()
                                            }}>
                                    {lang.get("your_rate")}
                                </Typography>
                                <Box dir={'ltr'}>
                                    <Rating name="read-only" value={rate / 2} precision={0.5} readOnly/>
                                </Box>
                            </Box> : null}
                    </Box>
                    <Box display={'flex'} flex={1} minWidth={300} flexDirection={'column'} pr={2}>
                        <Box display={'flex'}>
                            <Typography width={1}
                                        display={'flex'}
                                        alignItems={'center'}
                                        variant={"body1"}
                                        color={approvedColor}
                                        textStyle={{
                                            border: `1px solid ${approvedColor}`,
                                            paddingTop: theme.spacing(0.2),
                                            paddingBottom: theme.spacing(0.2),
                                            paddingRight: theme.spacing(0.5),
                                            paddingLeft: theme.spacing(0.5),
                                            ...UtilsStyle.borderRadius(5),
                                            ...UtilsStyle.disableTextSelection(),
                                        }}>
                                {approvedLabel}
                            </Typography>
                            {false &&
                            <Box width={1 / 2} display={'flex'} ml={1}
                                 alignItems={'center'} justifyContent={'flex-end'}>
                                <BaseButton
                                    variant="text"
                                    size={'small'}
                                    disabled={removeDisable ? true : false}
                                    onClick={() => {
                                        onRemoveCommentItem()
                                    }}
                                    style={{
                                        marginRight: theme.spacing(1),
                                        color: colors.danger.main
                                    }}>
                                    {lang.get('delete')}
                                </BaseButton>
                            </Box>}
                        </Box>
                        <Typography variant={'body1'} pt={1} pl={1} fontWeight={200}>
                            {text}
                        </Typography>
                        <Box pt={2} display={'flex'} flexWrap={'wrap'}>
                            <ButtonLink
                                href={rout.Product.SingleV2.create(product.permalink)}
                                color={'#fff'}
                                backgroundColor={cyan[300]}>
                                {lang.get('see_product')}
                            </ButtonLink>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Fade>
    )
}
