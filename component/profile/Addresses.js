import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerUser from "../../controller/ControllerUser";
import {useSnackbar} from "notistack";
import _ from "lodash";
import {Card} from "@material-ui/core";
import Fade from "@material-ui/core/Fade";
import Skeleton from "@material-ui/lab/Skeleton";
import {cyan, grey} from "@material-ui/core/colors";
import Typography from "../base/Typography";
import {GA, lang, theme} from "../../repository";
import rout from "../../router";
import BaseButton from "../base/button/BaseButton";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ComponentError from "../base/ComponentError";
import useSWR, {mutate} from "swr";
import {getPageTitle} from "../../headUtils";
import Head from "next/head";
import ButtonBase from "@material-ui/core/ButtonBase";
import AddAddressDialog from "../addAddress/AddAddressDialog";


export default function Addresses({deletable, onSelect, ...props}) {
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.AddAddress.rout});
    }, []);
    const [state, setState] = useState({
        data: [],
        defaultAddress: null,
        newRemovedItem: null,
        pagination: {
            hasMoreItems: true,
            lastPage: 0,
            items: 0,
            page: 0,
        }
    });
    const [removeAddressState, setRemoveAddressState] = React.useState({
        state: false,
        address_id: null
    });
    const [editAddressState, setEditAddressState] = React.useState({
        state: false,
        address: null
    });

    const d = ControllerUser.User.Profile.Addresses.get({all: true});
    const {data, error} = useSWR(...d);

    let undo = false;
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    useEffect(() => {
        requestNewData(state.pagination.page)
    }, [state.pagination.page]);

    function resetData() {
        mutate(d[0]);
    }

    function requestNewData(page = 0) {
        if (_.isArray(state.data[page]))
            return;
        ControllerUser.User.Profile.Addresses.get({all: true, page: page + 1})[1]().then((res) => {
            const newData = state;
            newData.data[page] = res.data;
            setState({
                ...newData,
                defaultAddress: res.defaultAddress,
                pagination: {
                    ...state.pagination,
                    lastPage: res.pagination.lastPage,
                    items: res.pagination.items,
                    hasMoreItems: page + 1 < res.pagination.lastPage,
                    page: page
                }
            });
        }).catch(() => {
            const newData = state;
            newData.data[page] = [];
            setState({...newData})
        })
    }

    function removeAddress() {
        const address_id = removeAddressState.address_id;
        const index = 0;
        ControllerUser.User.Profile.Addresses.remove({address_id: address_id}).then((res => {
            resetData()
        })).catch(() => {
        }).finally(() => {
            setRemoveAddressState({
                ...removeAddressState,
                state: false,
                address_id: null
            })
        })
    }

    const items = [];
    if (data && data.data)
        data.data.map((ad, index) => {
            items.push(
                <AddressesItem
                    key={ad.id}
                    id={ad.id}
                    name={ad.name}
                    deletable={deletable}
                    location={ad.location}
                    address={ad.fullAddress}
                    city={ad.city}
                    phone={ad.phone}
                    postalCode={ad.postalCode}
                    state={ad.state}
                    onSelect={onSelect ? () => {
                        onSelect(ad)
                    } : undefined}
                    onEditClick={(id) => {
                        setEditAddressState({
                            ...editAddressState,
                            state: true,
                            address: ad,
                        })
                    }}
                    onRemoveClick={(id) => {
                        setRemoveAddressState({
                            ...removeAddressState,
                            state: true,
                            address_id: id,
                        })
                    }}
                    {...props}/>
            );
        });

    return (
        <Box display={'flex'} flexWrap={'wrap'}>
            <Head>
                <title>{getPageTitle(lang.get("add_address"))}</title>
            </Head>
            {
                error ?
                    <Box component={Card} width={1}>
                        <ComponentError tryAgainFun={resetData}/>
                    </Box> :
                    (data !== undefined && _.isEmpty(data)) ?
                        <Box width={1} display={'flex'} flexWrap={'wrap'}>
                            {[1, 2, 3, 4].map((i) => (
                                <Box key={i}
                                     width={{
                                         xs: 1,
                                         lg: 1 / 2
                                     }}
                                     display={'flex'} px={3} py={2}>
                                    <Box component={Card}
                                         py={3}
                                         width={1}
                                         display={'flex'}
                                         alignItems={'center'}
                                         justifyContent={'center'}>
                                        <Skeleton variant="rect" height={150}
                                                  style={{
                                                      width: '100%',
                                                      maxWidth: '95%'
                                                  }}/>
                                    </Box>
                                </Box>
                            ))}
                        </Box> :
                        <>
                            <Fade in={true}
                                  style={{transformOrigin: '0 0 0'}}
                                  timeout={1000}>
                                <Box display={'flex'}
                                     elevation={0}
                                     width={{
                                         xs: 1,
                                         lg: 1 / 2
                                     }} py={2} px={2}>
                                    <Box component={Card} width={1}
                                         display={'flex'}
                                         alignItems={'center'}
                                         justifyContent={'center'}
                                         flexDirection={'column'}
                                         flexWrap={'wrap'} py={2} px={1}
                                         onClick={() => {
                                             setEditAddressState({
                                                 ...editAddressState,
                                                 state: true,
                                                 address: null
                                             })
                                         }}
                                         style={{
                                             backgroundColor: grey[300],
                                             cursor: 'pointer'
                                         }}>
                                        <img
                                            alt={'homeAddress icon'}
                                            src={'/drawable/svg/homeAddress.svg'}
                                            style={{
                                                width: 100,
                                                fill: grey[600],
                                                height: 'auto'
                                            }}/>
                                        <Typography variant={"h6"} pt={1.5} color={grey[600]} fontWeight={500}>
                                            {lang.get("add_address")}
                                        </Typography>
                                    </Box>
                                </Box>
                            </Fade>
                            {items}
                        </>
            }
            <>
                <Backdrop
                    open={removeAddressState.address_id !== null && removeAddressState.state === false}
                    style={{
                        zIndex: 999
                    }}>
                    <CircularProgress color="inherit"
                                      style={{
                                          width: 100,
                                          height: 100
                                      }}/>
                </Backdrop>
                <Dialog
                    open={removeAddressState.state}
                    onClose={() => {
                        setRemoveAddressState({
                            ...removeAddressState,
                            state: false,
                            address_id: null
                        })
                    }}
                    aria-labelledby="address_remove_alert-dialog-title"
                    aria-describedby="address_remove_alert-dialog-description">
                    <DialogTitle
                        style={{
                            paddingRight: theme.spacing(3),
                            paddingLeft: theme.spacing(3),
                            paddingBottom: theme.spacing(3)
                        }}>{lang.get("me_remove_address")}</DialogTitle>
                    <DialogActions>
                        <BaseButton
                            onClick={() => {
                                setRemoveAddressState({
                                    ...removeAddressState,
                                    state: false,
                                    address_id: null
                                })
                            }}
                            autoFocus
                            style={{
                                backgroundColor: cyan[400],
                                color: '#fff',
                                marginLeft: theme.spacing(1.5)
                            }}>
                            {lang.get("no")}
                        </BaseButton>
                        <BaseButton
                            size={"small"}
                            variant={"text"}
                            onClick={() => {
                                setRemoveAddressState({
                                    ...removeAddressState,
                                    state: false,
                                });
                                removeAddress(removeAddressState.address_id)
                            }}
                            style={{
                                color: grey[800]
                            }}>
                            {lang.get("yes")}
                        </BaseButton>
                    </DialogActions>
                </Dialog>
            </>
            <AddAddressDialog
                address={editAddressState.address}
                open={editAddressState.state}
                onFinish={(address)=>{
                    resetData();
                    setEditAddressState({
                        ...setEditAddressState,
                        state: false,
                        address: null
                    })
                }}
            onClose={()=>{
                setEditAddressState({
                    ...editAddressState,
                    state: false,
                    address_id: null
                })
            }}/>
        </Box>
    )
}

function AddressesItem({id, name, address, phone, city, state, postalCode, location, deletable, onSelect, onEditClick, onRemoveClick, ...props}) {

    const el = (<Box component={Card} width={1}
                     display={'flex'}
                     flexDirection={'column'}
                     flexWrap={'wrap'} py={2} px={2}>
        <Typography variant={'h6'} fontWeight={400}>
            {name}
        </Typography>
        <Typography pt={1.2} variant={'body1'}>
            {address}
        </Typography>
        <Box display={'flex'} mt={4} alignItems={'flex-end'}>
            <Box display={'flex'} width={'50%'} flexDirection={'column'}>
                <Typography variant={'body1'}>
                    {lang.get("postal_code")}: {postalCode}
                </Typography>
                <Typography mt={1.5} variant={'body1'}>
                    {lang.get("phone")}: {phone}
                </Typography>
            </Box>
            <Box display={'flex'} width={'50%'} flexWrap={'wrap'} justifyContent={'flex-end'}>
                <BaseButton
                    onClick={(e) => {
                        e.stopPropagation();
                        onEditClick(id)
                    }}
                    style={{
                        backgroundColor: cyan[400],
                        color: '#fff',
                        marginLeft: theme.spacing(0.5)
                    }}>
                    {lang.get("edit")}
                </BaseButton>
                {
                    deletable &&
                    <BaseButton
                        size={"small"}
                        variant={"text"}
                        onClick={(e) => {
                            e.stopPropagation();
                            onRemoveClick(id)
                        }}
                        style={{
                            color: grey[800]
                        }}>
                        {lang.get("delete")}
                    </BaseButton>
                }
            </Box>
        </Box>
    </Box>)

    return (
        <Fade in={true}
              style={{transformOrigin: '0 0 0'}}
              timeout={1000}>
            <Box display={'flex'}
                 elevation={0}
                 width={{
                     xs: 1,
                     lg: 1 / 2
                 }} py={2} px={2}>
                {onSelect ?
                    <ButtonBase onClick={onSelect} style={{width: '100%'}}>
                        {el}
                    </ButtonBase> :
                    <React.Fragment>
                        {el}
                    </React.Fragment>}
            </Box>
        </Fade>
    )
}
