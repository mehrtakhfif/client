import React, {Fragment, useEffect, useMemo, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerUser, {wishlistDefaultStep} from "../../controller/ControllerUser";
import Skeleton from "@material-ui/lab/Skeleton";
import {Card} from "@material-ui/core";
import _ from 'lodash'
import Typography from "../base/Typography";
import {GA, lang, theme} from "../../repository";
import rout from "../../router";
import {Delete} from "@material-ui/icons"
import IconButton from "@material-ui/core/IconButton";
import BaseButton from "../base/button/BaseButton";
import {green} from "@material-ui/core/colors";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import Fade from "@material-ui/core/Fade";
import Head from "next/head";
import {getPageTitle} from "../../headUtils";
import Nlink from "../base/link/NLink";
import ProductCard from "../product/ProductCard";
import useSWRInfinite from "../../lib/useSWRInfinite";


const rowsPerPage = wishlistDefaultStep;

export default function Wishlist({...props}) {
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.Main.Params.Wishlist.param});
    }, []);
    const [state, setState] = useState({
        removedItem: null
    });
    // const d = ControllerUser.User.Profile.Wishlist.get();
    //
    // const {
    //     pages,
    //     isLoadingMore,
    //     isReachingEnd,
    //     loadMore,
    //     pageSWRs,
    //     ...f
    // } = useSWRPages(
    //     'wishlist-page',
    //     ({offset, withSWR}) => {
    //         const {data, error} = withSWR(
    //             // eslint-disable-next-line react-hooks/rules-of-hooks
    //             useSWR(d[0] + (offset || 0) + 1, () => {
    //                 return d[1]({page: (offset || 0) + 1, step: rowsPerPage})
    //             })
    //         );
    //
    //         if (error) {
    //             return <ComponentError tryAgainFun={() => mutate(d[0])}/>
    //         }
    //
    //         if (!data) {
    //             return (
    //                 <React.Fragment>
    //                     {placeHolder(1)}
    //                 </React.Fragment>
    //             )
    //         }
    //
    //
    //         if (_.isEmpty(data.data)) {
    //             return (
    //                 <Box px={2} py={5} width={1} display={'flex'} alignItems={'center'}
    //                      justifyContent={'center'}>
    //                     <Typography variant={'h6'}>
    //                         {lang.get('list_is_empty')}.
    //                     </Typography>
    //                 </Box>
    //             )
    //         }
    //
    //
    //         return data.data.map((w, i) => (
    //             (w && w.product) ?
    //                 <WishlistItem index={i} key={w.id}
    //                               id={w.id}
    //                               product={w.product}
    //                               type={w.type}
    //                               removedItem={state.removedItem}
    //                               onRemoveWishlistItem={() => removeWishlistItem(i, w)}
    //                               notify={w.notify} {...props}/> :
    //                 <React.Fragment key={i}/>
    //         ))
    //     },
    //     // get next page's offset from the index of current page
    //     (SWR, index) => {
    //         if (SWR.data.pagination && SWR.data.pagination.lastPage <= index + 1) return null;
    //         return (index + 1)
    //     }, []);


    const {getKey, fetcher} = ControllerUser.User.Profile.Wishlist.V2.Get;
    const api = useMemo(() => getKey(), []);

    //region swr
    const {
        data,
        error,
        page,
        maxPage,
        allCount,
        step,
        mutate,
        setNextPage,
        onClear,
        isEmpty,
        isLoadingMore,
        isReachingEnd,
        isRefreshing
    } = useSWRInfinite(api, fetcher, {
        revalidateOnFocus: false,
    })
    //endregion swr


    let undo = false;
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    function requestNewData(page = 0) {
        if (_.isArray(state.data[page]))
            return;
        ControllerUser.User().Wishlist().get({page: page + 1}).then((res) => {
            const newData = state;
            newData.data[page] = res.data;
            setState({
                ...newData,
                pagination: {
                    ...state.pagination,
                    lastPage: res.pagination.lastPage,
                    items: res.pagination.items,
                    hasMoreItems: page + 1 < res.pagination.lastPage,
                    page: page
                }
            });
        }).catch(() => {
            const newData = state;
            newData.data[page] = [];
            setState({...newData})
        })
    }

    function removeWishlistItem(itemIndex, item) {

        if (state.removedItem) {
            return
        }

        setState({
            ...state,
            removedItem: item
        });

        enqueueSnackbar(lang.get("me_product_is_removed"),
            {
                variant: "success",
                action: (key) => (
                    <Fragment>
                        <Button
                            onClick={() => {
                                setState({
                                    ...state,
                                    removedItem: null
                                });
                                closeSnackbar(key);
                            }}
                            style={{
                                marginLeft: theme.spacing(1),
                                color: '#fff'
                            }}>
                            {lang.get('close')}
                        </Button>
                        <Button
                            onClick={() => {
                                undoWishlistItem(index, itemIndex, item);
                                undo = true;
                                closeSnackbar(key);
                            }}
                            style={{
                                backgroundColor: green[700],
                                color: '#fff'
                            }}>
                            {lang.get('undo')}
                        </Button>
                    </Fragment>
                ),
                onExited: () => {
                    if (undo)
                        setState({
                            ...state,
                            removedItem: null
                        });
                    else
                        removeWishlistItemRequest(itemIndex, item);
                    undo = false;
                },
                resumeHideDuration: 1500
            });
    }

    function undoWishlistItem(itemIndex, item) {
        setState({
            ...state,
            removedItem: null
        })
    }

    function removeWishlistItemRequest(itemIndex, item) {
        // setState({
        //         //     ...state,
        //         //     newRemovedItem: null
        //         // });
        //         // ControllerUser.User().Wishlist().remove({wishlist_id: item.id}).then(() => {
        //         // }).catch(() => {
        //         //     undoWishlistItem(index, itemIndex, item)
        //         // })
    }


    const placeHolder = (index) => {
        index = _.isNumber(index) ? index : 1;
        return (
            <Box width={1} display={'flex'} flexWrap={'wrap'} key={index}>
                {
                    [1, 2, 3, 4].map((i) => (
                        <Box key={i + '-' + index}
                             width={{
                                 xs: 1,
                                 lg: 1 / 2
                             }}
                             display={'flex'} px={3} py={2}>
                            <Box component={Card}
                                 py={3}
                                 width={1}
                                 display={'flex'}
                                 alignItems={'center'}
                                 justifyContent={'center'}>
                                <Skeleton variant="rect" height={150}
                                          style={{
                                              width: '100%',
                                              maxWidth: '95%'
                                          }}/>
                            </Box>
                        </Box>
                    ))
                }
            </Box>
        )
    };


    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Head>
                <title>{getPageTitle(lang.get("wishlist"))}</title>
            </Head>
            <Box width={1} display={'flex'}
                 flexWrap={'wrap'}>
                {
                        isEmpty ?
                            <Box px={2} py={5} width={1} display={'flex'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <Typography variant={'h6'}>
                                    {lang.get('list_is_empty')}.
                                </Typography>
                            </Box> :
                            <React.Fragment>
                                {data?.map((w, i) => (
                                    (w && w.product) ?
                                        <WishlistItem index={i} key={w.id}
                                                      id={w.id}
                                                      product={w.product}
                                                      type={w.type}
                                                      removedItem={state.removedItem}
                                                      onRemoveWishlistItem={() => removeWishlistItem(i, w)}
                                                      notify={w.notify} {...props}/> :
                                        <React.Fragment key={i}/>
                                ))}
                                {isLoadingMore && placeHolder(1)}
                            </React.Fragment>
                }
            </Box>
            {(!isReachingEnd && !isLoadingMore) &&
            <Box flex={1} py={2} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                <BaseButton onClick={setNextPage}>
                    نمایش بیشتر
                </BaseButton>
            </Box>
            }
        </Box>
    )
}

function WishlistItem({index, id, type, product, notify, removedItem = null, onRemoveWishlistItem, ...props}) {


    return (
        <Fade in={true}
              style={{transformOrigin: '0 0 0'}}
              timeout={1000}>
            <Box display={'flex'}
                 elevation={0}
                 justifyContent={'center'}
                 width={{
                     xs: 1,
                     lg: 1 / 4
                 }} py={1}
                 style={{position: 'relative'}}>
                <Nlink
                    style={{width: '100%'}}
                    href={rout.Product.SingleV2.create(product?.permalink)}>
                    {/*
                <Box width={1}
                     display={'flex'}
                     alignItems={'center'}
                     flexWrap={'wrap'}
                     justifyContent={'center'}>*/}
                    <ProductCard
                        id={product.id}
                        imageSrc={product.thumbnail?.image}
                        name={product.name}
                        discountPrice={product.discountPrice}
                        finalPrice={product.finalPrice}
                        discountPercent={product.discountPercent}
                        deadline={product.deadline}
                        availableCountForSale={product.maxCountForSale}
                        permalink={product.permalink}/>
                    {/*             <Box
                        mr={{
                            xs: 2,
                            lg: 0
                        }} display={'flex'} alignItems={'center'}>
                        <Img src={product.thumbnail.image}
                             alt={product.thumbnail.title}
                             maxWidth={{
                                 xs: 120,
                                 md: 220,
                                 lg: 200,
                             }}
                             imgStyle={{
                                 width: '100%',
                                 ...UtilsStyle.borderRadius(5)
                             }}/>
                    </Box>
                    <Box display={'flex'} flex={1} width={'100%'} flexDirection={'column'} p={1}>
                        <Typography variant={'h6'} pt={2} fontWeight={400}>
                            {product.name}
                        </Typography>
                        <Box mt={2} mb={1}
                             ml={2}
                             display={'flex'}
                             flexDirection={'row-reverse'}
                             alignItems={'center'}
                             flexWrap={'wrap'}>
                            <Box display={'flex'} flex={1} mr={1} alignItems={'center'}>
                                <Typography variant={'h5'} fontWeight={400} color={colors.primary.light}>
                                    {UtilsFormat.numberToMoney(product.finalPrice)}
                                </Typography>
                                <Typography variant={'caption'} mr={0.3} color={colors.primary.light}>
                                    {lang.get('toman')}
                                </Typography>
                            </Box>
                        </Box>

                        <Box pt={2} display={'flex'} flexWrap={'wrap'}>
                            {false &&
                            <BaseButton
                                variant="text"
                                size={'small'}
                                disabled={!!removedItem}
                                onClick={() => {
                                    onRemoveWishlistItem()
                                }}
                                style={{
                                    marginRight: theme.spacing(1),
                                    color: colors.danger.main
                                }}>
                                {lang.get('delete')}
                            </BaseButton>
                            }
                        </Box>
                    </Box>*/}
                    {/*</Box>*/}
                </Nlink>
                <IconButton style={{
                    position: 'absolute',
                    zIndex: theme.zIndex.appBar + 200,
                    right: '8px',
                    marginTop: '8px',
                    padding: '8px',
                }} onClick={(e) => {
                    e.stopPropagation();
                    onRemoveWishlistItem();
                }}><Delete/></IconButton>
            </Box>
        </Fade>
    )
}
