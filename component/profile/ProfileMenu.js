import React from "react";
import {UtilsStyle} from "../../utils/Utils";
import {cyan, grey} from "@material-ui/core/colors";
import {colors, lang, theme} from "../../repository";
import rout from "../../router";
import Box from "@material-ui/core/Box";
import clsx from "clsx";

import {
    AccountBoxOutlined,
    ChatBubbleOutlineOutlined,
    FavoriteBorder,
    LocationOnOutlined,
    PersonOutlineOutlined,
    ShoppingCartOutlined
} from "@material-ui/icons";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "../base/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import {SmHidden, SmShow} from "../header/Header";
import BaseButton from "../base/button/BaseButton";
import {useRouter} from "next/router";


export const profileList = () => {
    return [
        {
            label: 'profile',
            profileKey: rout.User.Profile.Main.Params.Profile,
            icon: (props) => <PersonOutlineOutlined {...props}/>,
            background: cyan[300]
        },
        {
            label: 'all_order',
            profileKey: rout.User.Profile.Main.Params.AllOrder,
            icon: (props) => <ShoppingCartOutlined {...props}/>,
            background: cyan[300]
        },
        {
            label: 'wishlist',
            profileKey: rout.User.Profile.Main.Params.Wishlist,
            icon: (props) => <FavoriteBorder {...props}/>,
            background: cyan[300]
        },
        {
            label: 'comments_and_review',
            profileKey: rout.User.Profile.Main.Params.CommentsAndReview,
            icon: (props) => <ChatBubbleOutlineOutlined {...props}/>,
            background: cyan[300]
        },
        {
            label: 'addresses',
            profileKey: rout.User.Profile.Main.Params.Addresses,
            icon: (props) => <LocationOnOutlined {...props}/>,
            background: cyan[300]
        },
        {
            label: 'user_info',
            profileKey: rout.User.Profile.Main.Params.UserInfo,
            icon: (props) => <AccountBoxOutlined {...props}/>,
            background: cyan[300]
        },
    ];
}

const useStyle = makeStyles(theme => (
    {
        profileListItem: {
            cursor: 'pointer',
            borderTopColor: grey[400],
            borderTopStyle: 'solid',
            ...UtilsStyle.transition('300'),
            ...UtilsStyle.disableTextSelection()
        },
        active: {
            cursor: 'default',
            '& *': {
                color: colors.primary.main,
            }
        },
        deActive: {
            '&:hover *':
                {
                    color: colors.primary.light,
                    ...UtilsStyle.transition('300'),
                }
        }
    }
));
export default function ProfileMenu({user, activeProfileKey, ...props}) {

    return (
        <Box
            mr={3}
            boxShadow={2}
            style={{
                backgroundColor: '#fff',
                ...UtilsStyle.borderRadius(5),
                ...UtilsStyle.widthFitContent(),
            }}>
            {user ?
                <React.Fragment>
                    <Typography py={1.5}
                                variant={'h6'}
                                display={'flex'}
                                justifyContent={'center'}
                                color={grey[100]}
                                px={5}
                                boxShadow={2}
                                style={{
                                    backgroundColor: theme.palette.primary.dark,
                                    cursor: 'default',
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                        {(user&&user.fullName )? user.fullName : user.username}
                    </Typography>
                    <ProfileMenuItems activeProfileKey={activeProfileKey}/>
                </React.Fragment>
                : <Skeleton variant="rect"/>}
        </Box>
    )
}


export function ProfileMenuItems({activeProfileKey, ...props}) {
    const classes = useStyle(props);
    const router = useRouter();


    function onItemClick(routItem) {
        try {
            const {rout, as} = routItem.create();
            router.push(rout, as)
        } catch (e) {

        }
    }

    return (
        <React.Fragment>
            {
                profileList().map((item, index) => (
                    <React.Fragment>
                        <SmHidden>
                            <Box key={index}
                                 display={'flex'}
                                 alignItems={'center'}
                                 onClick={() => onItemClick(item.profileKey)}
                                 py={2}
                                 pr={3}
                                 pl={5}
                                 className={clsx(classes.profileListItem, activeProfileKey === item.profileKey.param ? classes.active : classes.deActive)}
                                 style={{
                                     borderTopWidth: index !== 0 ? 1 : 0,
                                 }}>
                                {item.icon()}
                                <Typography
                                    pr={1.5}
                                    fontWeight={300}
                                    variant={'subtitle1'}>
                                    {lang.get(item.label)}
                                </Typography>
                            </Box>
                        </SmHidden>
                        <SmShow>
                            <Box py={1} px={0.5}>
                                <BaseButton
                                    onClick={() => onItemClick(item.profileKey)}
                                    disabled={activeProfileKey === item.profileKey.param}
                                    style={{
                                        backgroundColor: activeProfileKey === item.profileKey.param ? undefined : item.background
                                    }}>
                                    <Box display={'flex'} alignItems={'center'}>
                                        {item.icon({style: {color:  activeProfileKey === item.profileKey.param ? undefined :"#fff"}})}
                                        <Typography
                                            color={"#fff"}
                                            pr={1.5}
                                            fontWeight={300}
                                            variant={'subtitle1'}>
                                            {lang.get(item.label)}
                                        </Typography>
                                    </Box>
                                </BaseButton>
                            </Box>
                        </SmShow>
                    </React.Fragment>
                ))
            }
        </React.Fragment>
    )

}
