import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import BaseButton from "../base/button/BaseButton";
import {colors, GA, lang, theme} from "../../repository";
import rout from "../../router";
import {cyan, grey} from "@material-ui/core/colors";
import Img from "../base/oldImg/Img";
import {UtilsStyle} from "../../utils/Utils";
import {CircularProgress, Hidden, makeStyles} from "@material-ui/core";
import ControllerUser from "../../controller/ControllerUser";
import _ from 'lodash'
import MeliCodeTextField from "../base/textField/MeliCodeTextField";
import ShabaTextField from "../base/textField/ShabaTextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import moment from 'moment-jalaali';
import momentJalaali from 'moment-jalaali';
import JDate from "../../utils/JDate";
import FormController from '../base/formController/NewFormController'
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import {update} from "../../middleware/auth";
import Head from "next/head";
import {getPageTitle} from "../../headUtils";
import TextField from "../base/textField/TextField";
import TextFieldContainer, {errorList} from "../base/textField/TextFieldContainer";
import PhoneTextField from "../base/textField/PhoneTextField";
import {DEBUG} from "../../pages/_app";
import {useRouter} from "next/router";
import {tryIt} from "material-ui-helper";
import {useUserContext} from "../../context/UserContext";

const useUserDetailsStyles = makeStyles(theme => ({
    textField: {
        width: '100%',
        '& input': {
            paddingLeft: theme.spacing(0.5),
            paddingRight: theme.spacing(0.5),
        }
    },
    gender: {
        width: '100%',
    }
}));


export default function UserDetails({user = {}, onUserSubmitted, ...props}) {

    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.Main.Params.UserInfo.param});
    }, []);
    const classes = useUserDetailsStyles(props);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const formRef = useRef();
    const router = useRouter();
    const [state, setState] = useState({
        disable: false,
        data: {
            firstName: user.first_name,
            lastName: user.last_name,
            email: user.email,
            meliCode: user.meli_code,
            gender: user?.gender?.value || '',
            birthday: user.jBirthday,
            shaba: user.shaba,
            avatar: user.avatar,
        }
    });
    const [___,refreshUser] = useUserContext()

    useEffect(() => {
        setState({
            ...state,
            data: {
                firstName: user.first_name,
                lastName: user.last_name,
                email: user.email,
                meliCode: user.meli_code,
                gender: user?.gender?.value || '',
                birthday: user.jBirthday,
                shaba: user.shaba,
                avatar: user.avatar,
            }
        })
    }, [user]);


    const data = state.data;

    function handleSubmitClick() {
        if (formRef.current.hasError()) {
            enqueueSnackbar(lang.get("er_fill_all_inputs"),
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return
        }
        setState({
            ...state,
            disable: true
        });

        const pr = formRef.current.serialize();

        const br = momentJalaali(data.birthday, "jYYYY-jM-jD").add(1, "days").unix();
        update({
            firstName: pr.firstName,
            lastName: pr.lastName,
            birthday: br < 1000 ? undefined : br,
            gender: data.gender,
            email: pr.email,
            meliCode: pr.meliCode,
            shaba: pr.shaba,
        }).then(res => {
            tryIt(()=>refreshUser())
            const r = rout.User.Profile.Main.create()
            router.push(r.rout, r.as)
            // setTimeout(() => {
            //     setState({
            //         ...state,
            //         disable: false
            //     });
            //     enqueueSnackbar(lang.get("me_details_successfully_update"),
            //         {
            //             variant: "success",
            //             action: (key) => (
            //                 <Fragment>
            //                     <Button onClick={() => {
            //                         closeSnackbar(key)
            //                     }}>
            //                         {lang.get('close')}
            //                     </Button>
            //                 </Fragment>
            //             )
            //         });
            //     if (onUserSubmitted)
            //         onUserSubmitted();
            // }, 800)
        }).catch(() => {
            setTimeout(() => {
                setState({
                    ...state,
                    disable: false
                });
            }, 800)
        })
    }

    return (
        <Box display={'flex'} width={1}>
            <Head>
                <title>{getPageTitle(lang.get("user_info"))}</title>
            </Head>
            <Box component={Card} display={'flex'} width={1} p={2} flexWrap={'wrap'}
                 flexDirection={{
                     xs: 'column',
                     md: 'row'
                 }}>
                <Box display={'flex'} flexDirection={'column'} alignItems={'center'}
                     width={{
                         xs: 1,
                         md: 'unset'
                     }}>
                    {DEBUG &&
                    <AvatarComponent
                        data={data}
                        disable={state.disable}
                        onChangeAvatar={(avatarUrl) => {
                            setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    avatar: avatarUrl
                                }
                            })
                        }}/>}

                    <Hidden smDown>
                        <ActionComponent disable={state.disable} onSubmitClick={handleSubmitClick}/>
                    </Hidden>
                </Box>
                <FormController
                    innerref={formRef}
                    display={'flex'} flex={1} pl={2}
                    pr={{
                        xs: 2,
                        md: 5
                    }}
                    alignItems={'flex-end'}
                    flexWrap={'wrap'}
                    onChange={() => {

                    }}>
                    <TextFieldItem>
                        <TextFieldContainer
                            name={"firstName"}
                            defaultValue={data.firstName}
                            render={(ref, {
                                name: inputName,
                                initialize,
                                valid,
                                errorIndex,
                                setValue,
                                inputProps,
                                style,
                                props
                            }) => {
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        name={inputName}
                                        helperText={errorList[errorIndex]}
                                        inputRef={ref}
                                        fullWidth
                                        label={lang.get('first_name')}
                                        style={{
                                            ...style,
                                            minWidth: '90%'
                                        }}/>
                                )
                            }}/>
                    </TextFieldItem>
                    <TextFieldItem>
                        <TextFieldContainer
                            name={"lastName"}
                            defaultValue={data.lastName}
                            render={(ref, {
                                name: inputName,
                                initialize,
                                valid,
                                errorIndex,
                                setValue,
                                inputProps,
                                style,
                                props
                            }) => {
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        name={inputName}
                                        helperText={errorList[errorIndex]}
                                        inputRef={ref}
                                        fullWidth
                                        label={lang.get('last_name')}
                                        style={{
                                            ...style,
                                            minWidth: '90%'
                                        }}/>
                                )
                            }}/>
                    </TextFieldItem>
                    <TextFieldItem
                        width={1}>
                        <TextFieldContainer
                            name={"email"}
                            errorPatterns={['$^|^(([^<>()\\[\\]\\.,;:\\s@\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})$']}
                            defaultValue={data.email}
                            render={(ref, {
                                name: inputName,
                                initialize,
                                valid,
                                errorIndex,
                                setValue,
                                inputProps,
                                style,
                                props
                            }) => {
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        name={inputName}
                                        helperText={[[lang.get("er_verify_email")]][errorIndex]}
                                        inputRef={ref}
                                        fullWidth
                                        label={lang.get('email')}
                                        style={{
                                            ...style,
                                            minWidth: '90%'
                                        }}/>
                                )
                            }}/>
                    </TextFieldItem>
                    <TextFieldItem>
                        <PhoneTextField
                            variant={"standard"}
                            required={false}
                            disabled={true}
                            defaultValue={user.username}/>
                    </TextFieldItem>
                    <TextFieldItem>
                        <MeliCodeTextField
                            name={"meliCode"}
                            defaultValue={data.meliCode}/>
                    </TextFieldItem>
                    <TextFieldItem>
                        <FormControl className={classes.gender}
                                     disabled={state.disable}>
                            <InputLabel
                                id="gender-select-label"
                                style={{
                                    left: 'auto',
                                    transform: 'translate(0, 0) scale(0.75)',
                                    transformOrigin: 'top right'
                                }}>{lang.get("gender")}</InputLabel>
                            <Select
                                labelId="gender-select"
                                value={state.data.gender}
                                onChange={(e) => {
                                    setState({
                                        ...state,
                                        data: {
                                            ...state.data,
                                            gender: e.target.value
                                        }
                                    })
                                }}
                                className={classes.selectEmpty}>
                                <MenuItem value={null}>
                                    <em>{lang.get("gender_none")}</em>
                                </MenuItem>
                                <MenuItem value={true}>{lang.get('gender_man')}</MenuItem>
                                <MenuItem value={false}>{lang.get('gender_woman')}</MenuItem>
                            </Select>
                        </FormControl>
                    </TextFieldItem>
                    <TextFieldItem>
                        <Birthday birthday={state.data.birthday}
                                  disabled={state.disable}
                                  onDateChange={(birthday) => {
                                      setState({
                                          ...state,
                                          data: {
                                              ...state.data,
                                              birthday: birthday ? JDate.format(birthday, 'jYYYY-jM-jD HH:mm') : undefined
                                          }
                                      })
                                  }}/>
                    </TextFieldItem>
                    <TextFieldItem
                        width={1}>
                        <ShabaTextField
                            variant={"standard"}
                            name={"shaba"}
                            defaultValue={state.data.shaba}
                            disabled={state.disable}/>
                    </TextFieldItem>
                </FormController>
                <Hidden mdUp>
                    <ActionComponent disable={state.disable} onSubmitClick={handleSubmitClick}/>
                </Hidden>
            </Box>
        </Box>
    )
}

function ActionComponent({disable, onSubmitClick, ...props}) {
    return (
        <Box display={'flex'} justifyContent={'center'}
             flex={1} alignItems={'flex-end'}
             mt={{
                 xs: 4,
                 md: 2
             }}
             mb={2}>
            <BaseButton
                onClick={onSubmitClick}
                loading={disable}
                style={{
                    marginLeft: theme.spacing(1),
                    backgroundColor: cyan[400],
                    color: grey[100]
                }}
                loadingStyle={{
                    color: grey[600]
                }}>
                {lang.get("submit_details")}
            </BaseButton>
            {false ?
                <BaseButton
                    variant={'text'}
                    style={{
                        marginRight: theme.spacing(1),
                        color: grey[800]
                    }}>
                    {lang.get("dissuasion")}
                </BaseButton> :
                null}
        </Box>
    )
}

function TextFieldItem({full = false, ...props}) {
    return (
        <Box
            height={'max-content'}
            pb={3}
            pr={{
                xs: 2.5,
                md: 0
            }}
            pl={{
                xs: 2.5,
                md: 4
            }}
            width={{
                xs: 1,
                md: 1 / 2
            }}
            {...props}>
            {props.children}
        </Box>
    )
}

function AvatarComponent({data, disable, onChangeAvatar, ...props}) {
    const avatarSize = 150;
    const [state, setState] = useState({
        newAvatarLoading: false
    });

    const avatarFileUploaderRef = useRef(null);
    const avatarFileUploaderRef2 = useRef(null);


    function onUploadImage(e) {
        if (disable)
            return;
        try {
            const file = e.target.files[0];
            const imageType = /image.*/;
            if (state.newAvatarLoading || !file.type.match(imageType)) {
                resetAvatarInput();
                return;
            }
            setState({
                ...state,
                newAvatarLoading: true
            });

            let formData = new FormData();
            formData.append('file', file);
            ControllerUser.User.Profile.uploadAvatar({formData}).then(res => {
                setState({
                    ...state,
                    newAvatarLoading: false,
                });
                onChangeAvatar(res.data.avatarUrl);
                resetAvatarInput();
            }).catch(e => {
                resetAvatarInput();
                setState({
                    ...state,
                    newAvatarLoading: false
                })
            });
        } catch (e) {
            resetAvatarInput();
        }
    }

    function removeAvatar() {
        if (disable)
            return;
        setState({
            ...state,
            newAvatarLoading: true
        });
        ControllerUser.User.Profile.removeAvatar().then((res) => {
            setState({
                    ...state,
                    newAvatarLoading: false,
                }
            );
            onChangeAvatar('');
        }).catch((e) => {
            setState({
                ...state,
                newAvatarLoading: false
            })
        })
    }


    function resetAvatarInput() {
        avatarFileUploaderRef.current.value = '';
        avatarFileUploaderRef2.current.value = '';
    }

    return (
        <Box display={'flex'} justifyContent={'center'} mt={2}
             alignItems={'center'} flexDirection={'column'}>
            <input
                ref={avatarFileUploaderRef}
                accept="image/*"
                alt={'user avatar input'}
                id="avatar-file-uploader"
                type="file"
                disabled={disable}
                onChange={onUploadImage}
                style={{
                    display: 'none',
                }}/>
            <label htmlFor="avatar-file-uploader">
                <Box width={avatarSize} height={avatarSize}
                     display={'flex'}
                     flexDirection={'column'}
                     style={{
                         cursor: disable ? 'default' : 'pointer',
                         overflow: 'hidden',
                         position: 'relative',
                         border: data.avatar ? `1px solid ${grey[500]}` : null,
                         ...UtilsStyle.borderRadius('50%')
                     }}>
                    {!_.isEmpty(data.avatar) ?
                        <Img
                            onError={() => {
                                onChangeAvatar('');
                            }}
                            src={data.avatar}
                            alt={'user image'}
                            imgStyle={{
                                width: avatarSize,
                                height: avatarSize,
                            }}/>
                        :
                        <img
                            src={'/drawable/svg/userAvatar.svg'}
                            style={{
                                width: '100%',
                                height: 'auto'
                            }} alt={'avatar placeholder'}/>
                    }
                    {state.newAvatarLoading &&
                    <Box width={avatarSize} height={avatarSize} style={{
                        backgroundColor: 'rgba(0, 0, 0, 0.65)',
                        position: 'absolute',
                    }}>
                        <CircularProgress
                            size={10}
                            style={{
                                width: avatarSize,
                                height: avatarSize,
                                color: grey[300],
                            }}/>
                    </Box>
                    }
                </Box>
            </label>
            <Box display={'flex'} mt={0.75} alignItems={'center'} justifyContent={'center'}>
                <input
                    ref={avatarFileUploaderRef2}
                    alt={'file uploader avatar'}
                    accept="image/*"
                    id="avatar-file-uploader2"
                    type="file"
                    onChange={onUploadImage}
                    style={{
                        display: 'none'
                    }}/>
                <label htmlFor="avatar-file-uploader2">
                    <BaseButton variant={'text'}
                                component="span"
                                disabled={state.newAvatarLoading || disable}
                                style={{
                                    color: cyan[400]
                                }}>
                        {lang.get(data.avatar ? 'edit' : 'add')}
                    </BaseButton>
                </label>
                {data.avatar ?
                    <>
                        <Box px={0.5}>
                            |
                        </Box>
                        <BaseButton variant={'text'}
                                    component="span"
                                    size={'small'}
                                    onClick={removeAvatar}
                                    disabled={state.newAvatarLoading || disable}
                                    style={{
                                        color: colors.danger.main
                                    }}>
                            {lang.get('delete')}
                        </BaseButton>
                    </> : null}
            </Box>
        </Box>
    )
}

function Birthday({birthday, onDateChange, disabled, ...props}) {
    const classes = useUserDetailsStyles(props);
    const br = birthday ? moment(birthday, 'jYYYY-jM-jD HH:mm') : null;
    const live = br ? br.clone() : JDate.liveJalali();
    live.jDate(1);

    const days = [];
    for (let i = 1; i <= live.daysInMonth(); i++) {
        days.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
    }

    const years = [];
    for (let i = JDate.liveJalali().jYear() - 17; i > 1300; i--) {
        years.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
    }

    function dateChangeHandler({year = null, month = null, day = null}) {
        if (year) {
            month = br ? br.jMonth() + 1 : 1;
            day = br ? br.jDate() : 1;
            const d = moment(`${year}-${month}-1`, 'jYYYY-jM-jD HH:mm').daysInMonth();
            if (d < day) {
                day = d
            }
        } else if (month) {
            year = br ? br.jYear() : JDate.liveJalali().jYear() - 17;
            month = month + 1;
            day = br ? br.jDate() : 1;
            const d = moment(`${year}-${month}-1`, 'jYYYY-jM-jD HH:mm').daysInMonth();
            if (d < day) {
                day = d
            }
        } else if (day) {
            year = br ? br.jYear() : JDate.liveJalali().jYear() - 17;
            month = br ? br.jMonth() + 1 : 1;
            const d = moment(`${year}-${month}-1`, 'jYYYY-jM-jD HH:mm').daysInMonth();
            if (d < day) {
                day = d
            }
        }
        if (year && month && day) {
            onDateChange(moment(`${year}-${month}-${day} 12:00`, 'jYYYY-jM-jD HH:mm'));
            return
        }
        onDateChange(null);
    }


    return (
        <>
            <InputLabel id="birthday-select-label"
                        className={disabled ? 'Mui-disabled' : ''}
                        style={{
                            left: 'auto',
                            transform: 'translate(0, 0) scale(0.75)',
                            transformOrigin: 'top right'
                        }}>
                {lang.get("birthday")}
            </InputLabel>
            <Box display={'flex'}>
                <Box width={1 / 5} pl={1}>
                    <FormControl className={classes.gender}
                                 disabled={disabled}>
                        <InputLabel id="birthday-day-select-label"
                                    style={{
                                        left: 'auto'
                                    }}>{lang.get("day")}</InputLabel>
                        <Select
                            labelId="birthday-day-select"
                            value={br ? br.jDate() : ''}
                            onChange={(e) => dateChangeHandler({day: e.target.value})}
                            className={classes.selectEmpty}>
                            <MenuItem value={''}>
                                <em>{lang.get("not_selected")}</em>
                            </MenuItem>
                            {days}
                        </Select>
                    </FormControl>
                </Box>
                <Box width={2 / 5} px={1}>
                    <FormControl className={classes.gender}
                                 disabled={disabled}>
                        <InputLabel id="birthday-month-select-label"
                                    style={{
                                        left: 'auto'
                                    }}>{lang.get("month")}</InputLabel>
                        <Select
                            labelId="birthday-month-select"
                            value={br ? br.jMonth() : ''}
                            renderValue={(m) => {
                                return monthsName[m]
                            }}
                            onChange={(e) => dateChangeHandler({month: e.target.value})}
                            className={classes.selectEmpty}>
                            <MenuItem value={''}>
                                <em>{lang.get("not_selected")}</em>
                            </MenuItem>
                            {
                                monthsName.map((m, index) => (
                                    <MenuItem key={index} value={index}>{m}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                </Box>
                <Box width={2 / 5} px={1}>
                    <FormControl className={classes.gender}
                                 disabled={disabled}>
                        <InputLabel id="birthday-year-select-label"
                                    style={{
                                        left: 'auto'
                                    }}>{lang.get("year")}</InputLabel>
                        <Select
                            labelId="birthday-year-select"
                            value={br ? br.jYear() : ''}
                            onChange={(e) => dateChangeHandler({year: e.target.value})}
                            className={classes.selectEmpty}>
                            <MenuItem value={null}>
                                <em>{lang.get("not_selected")}</em>
                            </MenuItem>
                            {years}
                        </Select>
                    </FormControl>
                </Box>
            </Box>
        </>
    )
}


const monthsName = [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند',
];
