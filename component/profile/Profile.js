import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import {Card} from "@material-ui/core";
import Typography from "../base/Typography";
import {GA, lang} from "../../repository";
import rout from '../../router';
import {cyan, grey} from "@material-ui/core/colors";
import _ from 'lodash'
import {Edit} from "@material-ui/icons";
import Link from "../base/link/Link";
import {makeStyles} from "@material-ui/styles";
import {DEBUG} from "../../pages/_app";
import {SmHidden, SmShow} from "../header/Header";
import {logout} from "../../middleware/auth";
import Head from "next/head";
import {getPageTitle} from "../../headUtils";
import {ProfileMenuItems} from "./ProfileMenu";
import {gender} from "../../controller/converter";


const useProfileStyles = makeStyles(theme => ({
    profileODDItem: {
        [theme.breakpoints.up('sm')]: {
            borderRightWidth: '1px !important',
        },
    },
}));

//TODO:  "حذف دریافت خبرناه"
export default function Profile({user, ...props}) {
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.Main.Params.Profile.param});
    }, []);
    const classes = useProfileStyles(props);

    function editPersonalInformation(width = 0.5) {
        return (
            <Link
                display={'flex'}
                width={width}
                href={rout.User.Profile.Main.Params.UserInfo.create()}
                hoverColor={cyan[700]}
                py={2}
                justifyContent={'center'}
                color={cyan[400]}
                linkStyle={{
                    display: 'flex',
                    minWidth: width,
                    cursor: 'pointer',
                    justifyContent: 'center',
                }}>
                <Box display={'flex'}
                     justifyContent={'center'}
                     alignItems={'center'}>
                    <Edit fontSize={"small"}/>
                    <Typography mx={0.8} variant={'subtitle1'} fontWeight={400}>
                        {lang.get('edit_personal_information')}
                    </Typography>
                </Box>
            </Link>
        )
    }

    return (
        <Box display={'flex'} flexWrap={'wrap'} width={1}>
            <Head>
                <title>{getPageTitle(lang.get("profile"))}</title>
            </Head>
            {DEBUG &&
            "حذف دریافت خبرناه"
            }
            <SmShow>
                <Box display={'flex'} pb={4} flexWrap={'wrap'}>
                    <ProfileMenuItems activeProfileKey={rout.User.Profile.Main.Params.Profile.param}/>
                </Box>
            </SmShow>
            <Box component={Card}
                 width={1}
                 display={'flex'}
                 alignItems={'center'}
                 flexDirection={'column'}>
                <Box display={'flex'} width={1}
                     flexWrap={'wrap'}>
                    <Item label={lang.get("full_name")}
                          value={user.first_name + " " + user.last_name}
                          className={classes.profileODDItem}/>
                    <Item label={lang.get("email")}
                          value={user.email}/>
                    <Item label={lang.get("phone")}
                          value={user.phone || user.username}
                          className={classes.profileODDItem}/>
                    <Item label={lang.get("meli_code")}
                          value={user.meli_code}/>
                    <Item label={lang.get("birthday")}
                          value={ user?.jBirthday }
                          className={classes.profileODDItem}/>
                    <Item label={lang.get("gender")}
                          value={user?.gender?.label}/>
                </Box>
                <Box
                    display={'flex'} alignItems={'center'}
                    justifyContent={'center'} width={1}
                    flexWrap={'wrap'}>
                    <SmHidden>
                        {editPersonalInformation('100%')}
                    </SmHidden>
                    <SmShow>
                        <Box width={0.5}>
                            {editPersonalInformation('100%')}
                        </Box>
                        <Link
                            hoverColor={cyan[700]}
                            py={2}
                            color={cyan[400]}
                            mr={1} variant={'subtitle1'} fontWeight={400}
                            onClick={() => logout()}
                            linkStyle={{
                                display: 'flex',
                                width: '50%',
                                justifyContent: 'center',
                            }}>
                            {lang.get('logout_from_account')}
                        </Link>
                    </SmShow>
                </Box>
            </Box>
        </Box>
    )
}

function Item({label, value, ...props}) {
    return (
        <Box pr={2} pl={3} pt={1.5} pb={1}
             display={'flex'}
             flexDirection={'column'}
             width={{
                 xs: 1,
                 sm: 1 / 2
             }}
             style={{
                 borderColor: grey[400],
                 borderWidth: 0,
                 borderStyle: 'solid',
                 borderBottomWidth: 1,
                 ...props.style
             }}
             {...props}>
            <Typography variant={'body1'} color={grey[600]}>
                {label}
            </Typography>
            <Typography pt={1} variant={'h6'}
                        color={grey[800]}>
                {!_.isEmpty(value) ? value : '——————————'}
            </Typography>
        </Box>
    )
}

