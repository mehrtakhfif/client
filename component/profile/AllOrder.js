import React, {useEffect, useMemo, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerUser, {ordersDefaultStep} from "../../controller/ControllerUser";
import {convertInvoiceStatus} from "../../controller/converter";
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Card, Tooltip} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import {colors, GA, lang, theme} from "../../repository";
import rout from "../../router";
import JDate from "../../utils/JDate";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {cyan, grey} from "@material-ui/core/colors";
import TableContainer from '@material-ui/core/TableContainer';
import {
    ChevronLeft,
    FiberManualRecordTwoTone,
    FirstPage,
    KeyboardArrowLeft,
    KeyboardArrowRight,
    LastPage
} from "@material-ui/icons";
import Typography from "../base/Typography";
import IconButton from "@material-ui/core/IconButton";
import Link from "../base/link/Link";
import BaseButton from "../base/button/BaseButton";
import Head from "next/head";
import {getPageTitle} from "../../headUtils";
import useSWRInfinite from "../../lib/useSWRInfinite";

const useAllOrderStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    tableItem: {
        borderRight: `1px solid ${grey[400]}`,
        paddingRight: theme.spacing(1),
        paddingLeft: theme.spacing(1),
        paddingTop: theme.spacing(1.5),
        paddingBottom: theme.spacing(1.5),
        [theme.breakpoints.up('md')]: {
            paddingRight: theme.spacing(3),
            paddingLeft: theme.spacing(3),
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
        },
    }
}));

const rowsPerPage = ordersDefaultStep;

export default function AllOrder({...props}) {
    const classes = useAllOrderStyles();
    const [state, setState] = useState({
        data: [],
        activePage: 0,
    });

    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.Main.Params.AllOrder.param});
        GA.User.main({action: "check orders"})
    }, []);


    // const d = ControllerUser.User.Profile.Orders.get();
    // const {
    //     pages,
    //     isLoadingMore,
    //     isReachingEnd,
    //     loadMore,
    //     pageSWRs,
    //     ...f
    // } = useSWRPages(
    //     'all-order-page',
    //     ({offset, withSWR}) => {
    //         const apiKey = d[0] + (offset || 0) + 1;
    //         const {data, error} = withSWR(
    //             // eslint-disable-next-line react-hooks/rules-of-hooks
    //             useSWR(apiKey, () => {
    //                 return d[1]({page: (offset || 0) + 1, step: rowsPerPage})
    //             })
    //         );
    //
    //         if (error) {
    //             return <ComponentError tryAgainFun={() => mutate(apiKey)}/>
    //         }
    //
    //         if (!data) {
    //             return (
    //                 <TableRow>
    //                     <TableCell component="th" scope="row" align="center"
    //                                className={classes.tableItem}
    //                                style={{...UtilsStyle.disableTextSelection()}}>
    //                         <Typography pr={0.5} variant={'subtitle1'}>
    //                             -
    //                         </Typography></TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}>
    //                         <Skeleton variant={'rect'}/>
    //                     </TableCell>
    //                     <TableCell
    //                         align="center"
    //                         className={classes.tableItem}>
    //                         <Skeleton variant={'rect'}/>
    //                     </TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}>
    //                         <Skeleton variant={'rect'}/>
    //                     </TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}>
    //                         <Skeleton variant={'rect'}/>
    //                     </TableCell>
    //                     <TableCell align="right"
    //                                className={classes.tableItem}
    //                                style={{...UtilsStyle.disableTextSelection()}}>
    //                         <Box display={'flex'} alignItems={'center'}
    //                              justifyContent={'center'}>
    //                             <Skeleton variant={'rect'}/>
    //                         </Box>
    //                     </TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}
    //                                style={{
    //                                    cursor: 'pointer'
    //                                }}>
    //                         <Tooltip title={lang.get("show_details")}>
    //                             <IconButton>
    //                                 <ChevronLeft fontSize={"large"}/>
    //                             </IconButton>
    //                         </Tooltip>
    //                     </TableCell>
    //                 </TableRow>
    //             )
    //         }
    //
    //
    //         if (_.isEmpty(data.data)) {
    //             return (
    //                 <Box px={2} py={5} width={1} display={'flex'} alignItems={'center'}
    //                      justifyContent={'center'}>
    //                     <Typography variant={'h6'}>
    //                         {lang.get('list_is_empty')}.
    //                     </Typography>
    //                 </Box>
    //             )
    //         }
    //
    //
    //         return data.data.map(({
    //                                   id,
    //                                   order_id,
    //                                   jalaliCreatedAt,
    //                                   finalPrice,
    //                                   amount,
    //                                   statusLabel,
    //                                   status,
    //                                   isCanceled,
    //                                   isPayed,
    //                                   isRejected,
    //                                   isPending,
    //                               }, index) => {
    //             const color = invoiceStatusColor(status);
    //             return (
    //                 <TableRow key={id}>
    //                     <TableCell component="th" scope="row" align="center"
    //                                className={classes.tableItem}
    //                                style={{...UtilsStyle.disableTextSelection()}}>
    //                         <Typography pr={0.5} variant={'subtitle1'}>
    //                             {(index) + (rowsPerPage * (data.pagination.page - 1) + 1)}
    //                         </Typography></TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}>
    //                         <Typography variant={'h6'}>
    //                             {order_id}
    //                         </Typography>
    //                     </TableCell>
    //                     <TableCell
    //                         align="center"
    //                         className={classes.tableItem}>
    //                         <Typography variant={'h6'}>
    //                             {jalaliCreatedAt ? JDate.format(jalaliCreatedAt, 'jD jMMMM jYYYY') : '-----'}
    //                         </Typography>
    //                     </TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}>
    //                         <Typography variant={'h6'}>
    //                             {UtilsFormat.numberToMoney(finalPrice)}
    //                         </Typography>
    //                     </TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}>
    //                         <Typography variant={'h6'}>
    //                             {UtilsFormat.numberToMoney(amount)}
    //                         </Typography>
    //                     </TableCell>
    //                     <TableCell align="right"
    //                                className={classes.tableItem}
    //                                style={{...UtilsStyle.disableTextSelection()}}>
    //                         <Box display={'flex'} alignItems={'center'}
    //                              justifyContent={'center'}>
    //                             <FiberManualRecordTwoTone
    //                                 style={{
    //                                     fill: color
    //                                 }}/>
    //                             <Typography pr={0.5} variant={'body1'} color={color}>
    //                                 {statusLabel}
    //                             </Typography>
    //                         </Box>
    //                     </TableCell>
    //                     <TableCell align="center"
    //                                className={classes.tableItem}
    //                                style={{
    //                                    cursor: 'pointer'
    //                                }}>
    //                         <Tooltip title={lang.get("show_details")}>
    //                             <IconButton>
    //                                 <Link
    //                                     display={'flex'}
    //                                     color={grey[700]}
    //                                     href={rout.User.Profile.Invoice.Single.create({invoice_id: id})}
    //                                     hoverColor={cyan[300]}
    //                                     linkStyle={{
    //                                         display: 'flex'
    //                                     }}>
    //                                     <ChevronLeft fontSize={"large"}/>
    //                                 </Link>
    //                             </IconButton>
    //                         </Tooltip>
    //                     </TableCell>
    //                 </TableRow>
    //             )
    //         })
    //     },
    //     // get next page's offset from the index of current page
    //     (SWR, index) => {
    //         if (SWR.data.pagination && SWR.data.pagination.lastPage <= index + 1) return null;
    //         return (index + 1)
    //     });


    const {getKey, fetcher} = ControllerUser.User.Profile.Orders.V2.Get;
    const api = useMemo(() => getKey(), []);

    //region swr
    const {
        data,
        error,
        page,
        maxPage,
        allCount,
        step,
        mutate,
        setNextPage,
        onClear,
        isEmpty,
        isLoadingMore,
        isReachingEnd,
        isRefreshing
    } = useSWRInfinite(api, fetcher, {
        revalidateOnFocus: false,
    })
    //endregion swr


    return (
        <React.Fragment>
            <Head>
                <title>{getPageTitle(lang.get("all_order"))}</title>
            </Head>
            <Box component={Card}>
                <TableContainer>
                    <Table className={classes.table} aria-label="orders"
                           style={{
                               overflow: 'scroll'
                           }}>
                        <TableHead>
                            <TableRow>
                                <TableCell align="center"
                                           className={classes.tableItem}
                                           style={{...UtilsStyle.disableTextSelection()}}>#</TableCell>
                                <TableCell align="center"
                                           className={classes.tableItem}>{lang.get("order_id")}</TableCell>
                                <TableCell align="center"
                                           className={classes.tableItem}>{lang.get("order_registration_date")}</TableCell>
                                <TableCell align="center"
                                           className={classes.tableItem}>{lang.get("the_amount_payable")}</TableCell>
                                <TableCell align="center"
                                           className={classes.tableItem}>{lang.get("total_amount")}</TableCell>
                                <TableCell align="center"
                                           className={classes.tableItem}>{lang.get("payment_status")}</TableCell>
                                <TableCell align="center"
                                           className={classes.tableItem}>{lang.get("details")}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                data?.map(({
                                               id,
                                               order_id,
                                               jalaliCreatedAt,
                                               finalPrice,
                                               amount,
                                               statusLabel,
                                               status,
                                               isCanceled,
                                               isPayed,
                                               isRejected,
                                               isPending,
                                           }, index) => {
                                    const color = invoiceStatusColor(status);
                                    return (
                                        <TableRow key={id}>
                                            <TableCell component="th" scope="row" align="center"
                                                       className={classes.tableItem}
                                                       style={{...UtilsStyle.disableTextSelection()}}>
                                                <Typography pr={0.5} variant={'subtitle1'}>
                                                    {(index) + 1}
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="center"
                                                       className={classes.tableItem}>
                                                <Typography variant={'h6'}>
                                                    {order_id}
                                                </Typography>
                                            </TableCell>
                                            <TableCell
                                                align="center"
                                                className={classes.tableItem}>
                                                <Typography variant={'h6'}>
                                                    {jalaliCreatedAt ? JDate.format(jalaliCreatedAt, 'jD jMMMM jYYYY') : '-----'}
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="center"
                                                       className={classes.tableItem}>
                                                <Typography variant={'h6'}>
                                                    {UtilsFormat.numberToMoney(finalPrice)}
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="center"
                                                       className={classes.tableItem}>
                                                <Typography variant={'h6'}>
                                                    {UtilsFormat.numberToMoney(amount)}
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="right"
                                                       className={classes.tableItem}
                                                       style={{...UtilsStyle.disableTextSelection()}}>
                                                <Box display={'flex'} alignItems={'center'}
                                                     justifyContent={'center'}>
                                                    <FiberManualRecordTwoTone
                                                        style={{
                                                            fill: color
                                                        }}/>
                                                    <Typography pr={0.5} variant={'body1'} color={color}>
                                                        {statusLabel}
                                                    </Typography>
                                                </Box>
                                            </TableCell>
                                            <TableCell align="center"
                                                       className={classes.tableItem}
                                                       style={{
                                                           cursor: 'pointer'
                                                       }}>
                                                <Tooltip title={lang.get("show_details")}>
                                                    <IconButton>
                                                        <Link
                                                            display={'flex'}
                                                            color={grey[700]}
                                                            href={rout.User.Profile.Invoice.Single.create({invoice_id: id})}
                                                            hoverColor={cyan[300]}
                                                            linkStyle={{
                                                                display: 'flex'
                                                            }}>
                                                            <ChevronLeft fontSize={"large"}/>
                                                        </Link>
                                                    </IconButton>
                                                </Tooltip>
                                            </TableCell>
                                        </TableRow>
                                    )
                                })
                            }

                            {
                                isLoadingMore &&
                                <TableRow>
                                    <TableCell component="th" scope="row" align="center"
                                               className={classes.tableItem}
                                               style={{...UtilsStyle.disableTextSelection()}}>
                                        <Typography pr={0.5} variant={'subtitle1'}>
                                            -
                                        </Typography></TableCell>
                                    <TableCell align="center"
                                               className={classes.tableItem}>
                                        <Skeleton variant={'rect'}/>
                                    </TableCell>
                                    <TableCell
                                        align="center"
                                        className={classes.tableItem}>
                                        <Skeleton variant={'rect'}/>
                                    </TableCell>
                                    <TableCell align="center"
                                               className={classes.tableItem}>
                                        <Skeleton variant={'rect'}/>
                                    </TableCell>
                                    <TableCell align="center"
                                               className={classes.tableItem}>
                                        <Skeleton variant={'rect'}/>
                                    </TableCell>
                                    <TableCell align="right"
                                               className={classes.tableItem}
                                               style={{...UtilsStyle.disableTextSelection()}}>
                                        <Box display={'flex'} alignItems={'center'}
                                             justifyContent={'center'}>
                                            <Skeleton variant={'rect'}/>
                                        </Box>
                                    </TableCell>
                                    <TableCell align="center"
                                               className={classes.tableItem}
                                               style={{
                                                   cursor: 'pointer'
                                               }}>
                                        <Tooltip title={lang.get("show_details")}>
                                            <IconButton>
                                                <ChevronLeft fontSize={"large"}/>
                                            </IconButton>
                                        </Tooltip>
                                    </TableCell>
                                </TableRow>
                            }
                        </TableBody>
                    </Table>
                </TableContainer>


                {data &&
                <Box display={'flex'}>
                    <Box display={'flex'} alignItems={'center'} mx={2}>
                        <Typography variant={'body1'}>
                            {lang.get('total_number')}:
                        </Typography>
                        <Typography variant={'h6'} mr={1}>
                            {allCount}
                        </Typography>
                    </Box>
                    {(!isReachingEnd && !isLoadingMore) &&
                    <Box flex={1} py={2} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                        <BaseButton variant={"outlined"} onClick={setNextPage}>
                            نمایش بیشتر
                        </BaseButton>
                    </Box>
                    }
                </Box>
                }
            </Box>
        </React.Fragment>
    );
}

export function invoiceStatusColor(status) {
    const p = convertInvoiceStatus(status);
    const color = p.isCanceled ?
        colors.danger.main :
        (p.isPayed || p.isSent) ? colors.success.main :
            p.isPending ? cyan[500] : grey[400];
    return color
}

const useTablePaginationActionsStyles = makeStyles(theme => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
}));

function TablePaginationActions({count, page, rowsPerPage, onChangePage}) {
    const classes = useTablePaginationActionsStyles();

    const handleFirstPageButtonClick = event => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = event => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = event => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = event => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box display={'flex'} alignItems={'center'}>
            <Box className={classes.root}>
                <IconButton
                    onClick={handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="first page">
                    {theme.direction === 'rtl' ? <LastPage/> : <FirstPage/>}
                </IconButton>
                <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                    {theme.direction === 'rtl' ? <KeyboardArrowRight/> : <KeyboardArrowLeft/>}
                </IconButton>
                <IconButton
                    onClick={handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="next page">
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft/> : <KeyboardArrowRight/>}
                </IconButton>
                <IconButton
                    onClick={handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="last page">
                    {theme.direction === 'rtl' ? <FirstPage/> : <LastPage/>}
                </IconButton>
            </Box>
            <Box display={'flex'} alignItems={'center'}>
                <Typography variant={'body1'}>
                    {lang.get('total_number')}:
                </Typography>
                <Typography variant={'h6'} mr={1}>
                    {count}
                </Typography>
            </Box>
        </Box>
    );
}

