import React from "react"
import ExternalDrawable from "../../../public/drawable/ExternalDrawable";
import {Box, Dialog, isClient, useOpenWithBrowserHistory} from "material-ui-helper";
import {UtilsStyle} from "../../../utils/Utils";
import Img from "../../base/Img";
import {makeStyles} from "@material-ui/styles";
import MtIcon from "../../MtIcon";
import {DEBUG} from "../../../pages/_app";


const useStyle = makeStyles(theme => ({
    playIconBg: {
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: "rgb(0 0 0 / 40%)",
        transition: "all 500ms ease",
        '&:hover': {
            backgroundColor: "rgb(0 0 0 / 50%)",
        }
    }
}));


export default function AboutUsVideo({isSmall}) {
    const classes = useStyle()
    const [show, __, setShowDialog, setCloseDialog] = useOpenWithBrowserHistory("about-us-video-dialog")

    return (
        <Box
            width={{
                xs: '100%',
                md: '50%'
            }}
            pb={{
                xs: 2,
                md: 0
            }}
            pl={{
                xs: 0,
                md: 8
            }}>
            <div
                onClick={() => {
                    if (DEBUG)
                        setShowDialog()
                }}
                style={{
                    position: 'relative',
                    overflow: "hidden",
                    cursor: 'pointer',
                    ...UtilsStyle.borderRadius(!isSmall ? "0 30px 30px 0" : undefined)
                }}>
                <img src={'/drawable/image/aboutUs.png'} alt={'video thumbnail'}
                     style={{
                         width: '100%',
                         height: 'auto',
                     }}/>
                <div className={classes.playIconBg}/>
                <img
                    src={"/drawable/svg/playCircle.svg"}
                    alt={"playCircle"}
                    style={{
                        width: "20%",
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: 'translate(-50%, -50%)',
                        transition: "all 500ms ease"
                    }}/>
            </div>
            <Dialog
                open={show}
                onClose={setCloseDialog}
                fullWidth={true}
                onBackdropClick={setCloseDialog}
                maxWidth={"lg"}
                closeElement={(
                    <MtIcon icon={"mt-close"} style={{color: "#fff"}}/>
                )}>
                <Box height={"80vh"} width={1}>
                    {
                        isClient() &&
                        <iframe src="https://www.aparat.com/video/video/embed/videohash/djL1V/vt/frame"
                                title="مسافران قسمت 43" allowFullScreen="true" webkitallowfullscreen="true"
                                mozallowfullscreen="true"
                                style={{width: '100%'}}/>
                    }
                </Box>
            </Dialog>
        </Box>
    )
}