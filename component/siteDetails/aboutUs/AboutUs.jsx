import {Collapse, useTheme} from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React, {useState} from "react";
import {TabPanel} from "../SiteDetails";
import {Box} from "material-ui-helper"
import Typography from "../../base/Typography";
import {ExpandLess, ExpandMore, ZoomIn, ZoomOut} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Utils, {UtilsParser, UtilsStyle} from "../../../utils/Utils";
import Img from "../../base/oldImg/Img";
import {cyan, grey} from "@material-ui/core/colors";
import ExternalDrawable from "../../../public/drawable/ExternalDrawable";
import AboutUsVideo from "./AboutUsVideo";


const content = [
    {
        id: 1,
        title: "ارائه کالا و خدمات با قیمت مناسب",
        content: "ارائه محصولات با قیمت‌ مناسب و ترجیحاً با تخفیف به دلیل خرید مستقیم و بی‌واسطه از تامین‌کننده‌ها. علاوه بر کالا، خدمات رو هم با تخفیف در اختیار کاربران قرار می‌دیم."
    },
    {
        id: 2,
        title: "کارشناس‌های متخصص",
        content: "مدیریت هر بخش بر عهده‌ کارشناس متخصص همان بخشه؛ بنابراین اگر سؤالی در مورد کالای مورد نظر یا نیاز به مشاوره داشتید، موجب خرسندی ماست که بتونیم کمکی کنیم."
    },
    {
        id: 3,
        title: "اقدامات نیکوکارانه",
        content: "هر خریدی که از مهرتخفیف انجام میشه، نیم درصد از بهای محصول (کالا یا خدمات) فروخته شده به صورت مستقیم به حساب مؤسسات خیریه‌ای که زنان سرپرست خانوار رو تحت پوشش قرار میدن، واریز می‌شه. <br/>این مؤسسات از تولیدات افراد تحت پوشش‌شون حمایت می‌کنن. و بعد از اینکه این محصولات آماده فروش شدند، تیم مهرتخفیف از محصولات عکسبرداری می‌کنه و در دسته‌بندی «مشاغل تحت حمایت» در دسترس کاربران قرار میده."
    },
    {
        id: 4,
        title: "رسته مشاغل تحت حمایت",
        content: "محصولاتی که در دسته «مشاغل تحت حمایت» هستند، توسط زنان سرپرست خانوار تولید شده و پس از فروش این محصولات، تمام وجه پرداختی بدون کسر کارمزد به حساب این افراد واریز میشه و مهرتخفیف هیچ سودی از این بخش دریافت نمی‌کنه."
    }
]


export default function AboutUs({value, ...props}) {

    const [openStore, setOpenStore] = useState(false)
    const theme = useTheme();
    const isSmall = !useMediaQuery(theme => theme.breakpoints.up('md'))


    return (
        <TabPanel value={value} index={0} p={0} my={{xs: 1, md: 3}}>
            <Box display={'flex'} flexDirection={'column'}>
                <Box
                    width={1}
                    minHeight={[100, 300]}
                    display={'flex'}
                    alignItems={'center'}
                    justifyContent={'center'}
                    style={{
                        backgroundImage: `url(/drawable/image/aboutUsHead.jpg)`,
                        backgroundRepeat: 'no-repeat',
                        backgroundSize: 'auto'
                    }}>
                    <img
                        src={ExternalDrawable.mtTypo}
                        alt={'typo White'}
                        style={{
                            filter: "invert(100%) sepia(100%) saturate(0%) hue-rotate(188deg) brightness(103%) contrast(101%)",
                            width: "40%",
                        }}/>
                </Box>
                <Box
                    display={'flex'}
                    alignItems={"flex-start"}
                    pt={{
                        xs: 10,
                        md: 15
                    }}
                    flexDirection={{
                        xs: 'column',
                        md: 'row !important'
                    }}>
                    <Box
                        flex={1}
                        px={{
                            xs: 2,
                            md: 3,
                            lg: 4
                        }}
                        py={1} display={'flex'}
                        flexDirection={'column'}>
                        <Typography variant={"h4"} justifyContent={'center'} fontWeight={500} pb={3}>
                            داستان ما
                        </Typography>
                        {isSmall && <AboutUsVideo isSmall={isSmall}/>}
                        <Typography
                            component={"p"}
                            display={"flex"}
                            flexDirection={"column"}
                            variant={isSmall ? "h5" : "subtitle2"}
                            style={{
                                lineHeight: 1.8,
                            }}>
                            عضو داوطلب هلال احمر بودم و هر روز می‌دیدم که افراد نیازمند زیادی به این نهاد مراجعه می‌کنن
                            و بعد از کلی رفت‌وآمد مبالغی رو به عنوان کمک معیشتی دریافت می‌کردن.
                            <br/>
                            به عنوان یک ناظر، خیلی زود فهمیدم این روش جواب نمیده! با توجه به تورمی که به لطف آقایان در
                            این مملکت وجود داره (و خواهد داشت) تا وقتی که اقشار نیازمند شغلی نداشته باشن روزبه‌روز
                            نیازمندتر میشن و هر بار درخواست کمک بیشتری خواهند داشت.
                            <br/>
                            بنابراین شروع به تشویقشون برای شاغل شدن و خودکفا شدن کردیم. با این جواب که: «مواد اولیه
                            نداریم تا تولیدی داشته باشیم» مواجه شدیم.
                            <br/>
                            دیدیم راست میگن! حتی تهیه مواد اولیه برای شروع کار هم براشون بسیار دشواره.
                            <br/>
                            سعی کردیم مواد اولیه مورد نیازشون رو تهیه کنیم و در اختیارشون قرار بدیم؛ مثلاً چرم مورد نیاز
                            برای ساخت چند کیف چرمی رو در اختیارشون قرار دادیم و خوشبختانه تونستن تعدادی کیف چرمی مردانه
                            و زنانه در انواع مختلف تولید کنن که انصافاً کیفیت خوبی هم داشتن.
                            <br/>
                            شاد و سرخوش از کاری که انجام دادیم؛ تصور می‌کردیم یک کارآفرینی بزرگ کردیم و بقیه چقدر خنگ
                            بودن که تا حالا این کار ما رو انجام ندادن!
                            <br/>
                            همه چی خیلی خوب بود تا وقتی یکی از همکاران این سؤال رو پرسید: حالا چطوری اینارو بفروشیم؟!
                            مهمترین مشکلی که در این پروسه وجود داشت و ما ازش غافل بودیم؛ یعنی عدم توانایی در بازاریابی و
                            فروش.
                            <br/>
                            اینجا بود که فهمیدیم بقیه نه تنها خنگ نبودن بلکه خیلی باهوش‌تر از ما بودن که این کارو نکردن!
                            ولی قطعاً به کلّه‌شقی ما هم نبودن چون ما اصلاً ناامید نشدیم و تمام تلاشمون رو کردیم که این
                            تولیدات رو بفروشیم.
                            <br/>
                            احتمالاً اینجای داستان انتظار دارید که بگیم بالاخره تونستیم اون جنس‌ها رو بفروشیم و با
                            مبالغی که جمع شد تونستن کم‌کم کارشون رو گسترش بدن و پیشرفت کنن. الان هم هرکدومشون یک کارگاه
                            کوچک دارن و به تولید مشغولند. دیگه به کمک‌ هیچ مؤسسه‌ خیریه‌ای هم نیاز ندارن و حتی خودشون هم
                            الان می‌تونن دست چندتا نیازمند رو بگیرن.
                            <br/>
                            ولی نشد.
                            <br/>
                            نتونستیم اون کیف‌ها رو بفروشیم، چون جایی برای عرضه و بازاریابی نبود.
                            <br/>
                            بنابراین دست از پا درازتر برگشتیم سرخونه اول.
                            <br/>
                            ولی با خودمون یک عهدی بستیم؛ که اگر کسب و کاری رو راه انداختیم یه بخشی رو به بازاریابی و
                            فروش این محصولات اختصاص بدیم؛
                            <br/>
                            <span style={{padding: "10px 0", fontWeight: 'bold', textAlign: "center"}}>
                                و این جرقه‌ای شد برای استارت "مهرتخفیف".
                            </span>
                            فروشگاهی که کالا و خدمات را با تخفیف در دسترس کاربران قرار میده و از هر خرید، بخشی از بهای
                            محصول رو مستقیماً به حساب مؤسسات حامی زنان سرپرست خانوار، واریز می‌کنه.
                            <br/>
                            مؤسسات حامی زنان سرپرست خانوار از تولیدات افرادی که تحت پوشش دارن، حمایت می‌کنن و بعد از
                            اینکه محصولاتشون آماده فروش شد، تیم مهرتخفیف از این محصولات عکسبرداری می‌کنه و در دسته‌بندی
                            «مشاغل تحت حمایت» در دسترس کاربران قرار میده.
                            <br/>
                            بنابراین محصولاتی که در دسته «مشاغل تحت حمایت» هستن، توسط زنان سرپرست خانوار تولید میشه و پس
                            از خرید این محصولات توسط کاربران، تمام وجه پرداختی بدون کسر کارمزد و مستقیماً به حساب این
                            افراد واریز میشه و مهرتخفیف هیچ سودی از این بخش دریافت نمی‌کنه
                        </Typography>
                    </Box>
                    {
                        !isSmall &&
                        <AboutUsVideo isSmall={isSmall}/>
                    }
                </Box>
                <Box
                    display={'flex'}
                    px={2}
                    flexDirection={'column'}
                    pt={{
                        xs: 2,
                        md: 5
                    }}>
                    <Box
                        pb={{
                            xs: 1,
                            md: 3
                        }}>
                        <Typography variant={"h4"}
                                    justifyContent={'center'} fontWeight={500}
                                    style={{
                                        textAlign: 'center',
                                        lineHeight: 1.9
                                    }}>
                            ویژگی‌هایی که مارو متفاوت می‌کنه
                        </Typography>
                    </Box>
                    {
                        content.map(({id, title, content}) => (
                            <Item key={id} title={title} content={content}/>
                        ))
                    }
                </Box>
                <Cycle/>
            </Box>
        </TabPanel>
    )
}

function Cycle({...props}) {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const isSm = !useMediaQuery(theme => theme.breakpoints.up('md'))

    function handleOpenClick() {
        setOpen(!open)
    }

    return (
        <Box display={'flex'} pt={4} flexDirection={'column'} alignItems={'center'} justifyContent={'center'}>
            <Box display={'flex'} alignItems={'center'} justifyContent={'center'} pb={4}>
                <Typography variant={"h4"} fontWeight={600}>
                    چرخه کار مشاغل خانگی تحت حمایت مهرتخفیف
                </Typography>
            </Box>
            <Box
                display={'flex'}
                width={{
                    xs: '100%',
                    md: "70%",
                    lg: "50%"
                }}
                flexDirection={'column'}>
                <Box
                    display={'flex'}
                    flexDirection={'column'} alignItems={'center'}
                    justifyContent={'center'}
                    onClick={handleOpenClick}
                    pb={1}
                    style={{
                        border: !isSm ? `1px solid ${grey[200]}` : undefined,
                        cursor: 'pointer',
                        ...UtilsStyle.borderRadius(5)
                    }}>
                    <Img src={'/drawable/svg/cycleTop.svg'}/>
                    <Collapse in={open}
                              style={{
                                  width: '100%'
                              }}>
                        <Img src={'/drawable/svg/cycleBottom.svg'}/>
                    </Collapse>
                </Box>
                <Box
                    display={'flex'}
                    justifyContent={{
                        xs: "center",
                        md: 'unset'
                    }}>
                    <Typography variant={isSm ? "subtitle1" : "body2"} pt={2}
                                color={cyan[700]}
                                alignItems={'center'}
                                onClick={handleOpenClick}
                                style={{
                                    cursor: 'pointer'
                                }}>
                        {open ?
                            <React.Fragment>
                                <ZoomOut
                                    style={{
                                        marginLeft: theme.spacing(0.5)
                                    }}/>
                                بستن
                            </React.Fragment> :
                            <React.Fragment>
                                <ZoomIn
                                    style={{
                                        marginLeft: theme.spacing(0.5)
                                    }}/>
                                برای مشاهده کامل تصویر روی آن کلیک کنید
                            </React.Fragment>}
                    </Typography>
                </Box>
            </Box>
        </Box>
    )
}


function Item({title, content, ...props}) {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const isSmall = !useMediaQuery(theme => theme.breakpoints.up('md'))

    return (
        <Box px={3}
             width={1}
             py={{
                 xs: 0.75,
                 md: 1.5
             }}>
            <Box
                width={1}
                py={{
                    xs: 1,
                    md: 2
                }}
                px={{
                    xs: 2,
                    md: 4
                }}
                display={'flex'}
                flexDirection={'column'}
                style={{
                    backgroundColor: 'rgb(248, 248, 248)',
                    ...UtilsStyle.borderRadius(5)
                }}>
                <Box display={'flex'}
                     onClick={() => {
                         setOpen(!open)
                     }}
                     style={{
                         cursor: 'pointer'
                     }}>
                    <IconButton
                        onClick={(e) => {
                            setOpen(!open)
                            e.stopPropagation()
                        }}>
                        {
                            open ?
                                <ExpandLess fontSize={isSmall ? "small" : "large"}/> :
                                <ExpandMore fontSize={isSmall ? "small" : "large"}/>
                        }
                    </IconButton>
                    <Typography variant={isSmall ? "h5" : "h6"} alignItems={"center"} pr={1} component={"h5"}
                                fontWeight={500}
                                style={{
                                    flex: 1
                                }}>
                        {title}
                    </Typography>
                </Box>
                <Collapse
                    in={open}
                    addEndListener={() => {
                    }}>
                    <Typography component={"p"}
                                pt={2}
                                variant={isSmall ? "h5" : "subtitle1"}
                                style={{
                                    lineHeight: 2,
                                }}>
                        {UtilsParser.html(content)}
                    </Typography>
                </Collapse>
            </Box>
        </Box>
    )
}
