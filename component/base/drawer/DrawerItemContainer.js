import {makeStyles} from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import React from "react";
import {Box} from "material-ui-helper";


const useDrawerContainerStyles = makeStyles((theme) => ({
    rootDrawerContainer: {
        padding: 0
    }
}));


export default function DrawerItemContainer(pr) {
    const classes = useDrawerContainerStyles();
    const {children, ...props} = pr
    return (
        <Box component={ListItem} flexDirection={'column'} className={classes.rootDrawerContainer} {...props}>
            {children}
        </Box>
    )
}