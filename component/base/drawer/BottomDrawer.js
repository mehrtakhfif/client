import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import {Box, IconButton} from "material-ui-helper";
import {colors} from "../../../repository";
import DrawerItemContainer from "./DrawerItemContainer";
import MtIcon from "../../MtIcon";

export const defaultDrawerBleeding = 5;


//
// const useStyles = makeStyles(theme => (
//     {
//         customBadge: props => ({
//             color: props.color,
//             backgroundColor: props.backgroundColor,
//         })
//     }));
const useStyles = makeStyles((theme) => ({
    fullList: {
        width: "auto"
    },
    paper: props => ({
        maxHeight: props?.spaceOnTop ? `calc(100% - ${theme.spacing(props?.drawerBleeding || defaultDrawerBleeding)} - ${theme.spacing(props?.drawerBleeding  || defaultDrawerBleeding)})`:undefined,
        overflow: "visible",
        borderTopLeftRadius: theme.spacing(2),
        borderTopRightRadius: theme.spacing(2),
    }),
    wrapper: {
        position: "relative",
        borderTopLeftRadius: theme.spacing(2),
        borderTopRightRadius: theme.spacing(2),
        backgroundColor: theme.palette.common.white,
        visibility: "visible !important "
    },
    content: props => ({
        overflow: "auto",
        width: "100%",
        position: "relative",
        maxHeight: props?.spaceOnTop ?`calc(100vh - ${theme.spacing(props?.drawerBleeding || defaultDrawerBleeding)} - ${theme.spacing(props?.drawerBleeding || defaultDrawerBleeding)})`:undefined
    }),
    drawerBorder: {
        width: 38,
        height: 3,
        backgroundColor: colors.borderColor.h9e9e9e,
        borderRadius: 1.5,
    }
}));

export default function BottomDrawer({open, spaceOnTop=true,drawerBleeding = defaultDrawerBleeding, disableSwipeToOpen = true, keepMounted=false,showBack, onBackClick, onClose, ...props}) {
    const classes = useStyles({spaceOnTop,drawerBleeding});

    return (
        <React.Fragment>
            <CssBaseline/>
            <SwipeableDrawer
                anchor="bottom"
                open={open}
                onClose={onClose}
                disableSwipeToOpen={disableSwipeToOpen}
                onOpen={() => {
                }}
                ModalProps={{
                    keepMounted: keepMounted
                }}
                classes={{
                    paper: classes.paper
                }}>
                <Box
                    flexDirectionColumn={true}
                    className={classes.wrapper}
                    alignCenter={true}>
                    <Box
                        my={3}
                        className={classes.drawerBorder}/>
                    <Box
                        className={classes.content}
                        flexDirection={"column"}
                        {...props}>
                        {
                            showBack &&
                            <DrawerItemContainer>
                                <Box px={2} width={1} alignItems={"flex-start"}>
                                    <IconButton size={"small"} onClick={onBackClick || onClose}>
                                        <MtIcon icon={"mt-arrow-right"}/>
                                    </IconButton>
                                </Box>
                            </DrawerItemContainer>
                        }
                        {props?.children}
                    </Box>
                </Box>
            </SwipeableDrawer>
        </React.Fragment>
    );
}

