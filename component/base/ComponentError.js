import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import {lang} from "../../repository";
import BaseButton from "../base/button/BaseButton";
import Typography from "./Typography";
import {tryIt} from "material-ui-helper";

export default function ComponentError({
                                           message = lang.get("er_render_check_internet_connection"),
                                           tryAgainLabel = lang.get("try_again"),
                                           errorStatus,
                                           tryAgainFun,
                                           ...props
                                       }) {
    const [timer, setTimer] = useState(Date.now())


    function handleClick() {
        tryIt(() => {
            if (timer > Date.now()) {
                return
            }
            const t = new Date();
            t.setSeconds(t.getSeconds() + 45);
            setTimer(t.getTime())
            tryAgainFun()
        })
    }

    return (
        <Box width={1} display='flex' px={2} py={2}
             flexDirection='column'
             justifyContent='center'
             alignItems={'center'}>
            <Typography pt={1} pb={2} variant={'h6'}
                        style={{
                            lineHeight: 1.7,
                            textAlign: 'center',
                            display: 'flex',
                            justifyContent: 'center'
                        }}>
                {message}
            </Typography>
            <Box display={'flex'}>
                <BaseButton
                    onClick={handleClick}>
                    {tryAgainLabel}
                </BaseButton>
            </Box>
        </Box>
    )
}
