import React from "react";
import {Backdrop as MaterialBackdrop, Box} from "material-ui-helper";
import PleaseWait from "./loading/PleaseWait";


export default function Backdrop({open, transparent, zIndex, timeout, showPleaseWait, onClose, onTimedOut, ...props}) {
return <React.Fragment/>
    return (
        <MaterialBackdrop open={open} zIndex={zIndex} transparent={transparent} timeout={timeout}
                          onTimedOut={onTimedOut} onClose={onClose}>
            {
                (showPleaseWait &&open)&&
                <Box
                    px={5} py={2}
                    style={{
                        backgroundColor: "#fff",
                    }}>
                    <PleaseWait/>
                </Box>
            }
        </MaterialBackdrop>)
}
