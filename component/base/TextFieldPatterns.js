import React from "react";
import ReactNumberFormat from "react-number-format";


const mobileFormat = "#### ### ####";
const mobileFormatMask = " ";

export function MobileFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            onChange={onChange}
            error={true}
            format={mobileFormat} mask={mobileFormatMask}
            style={{
                direction: 'ltr',
                ...other.style,
            }}
        />
    );
}

export function MobileFormatCenter(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            onChange={onChange}
            format={mobileFormat}
            mask={mobileFormatMask}
            style={{
                direction: 'ltr',
                textAlign: 'center',
                ...other.style,
            }}
        />
    );
}
