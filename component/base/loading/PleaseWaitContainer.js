import React, {useMemo} from "react";
import {Box} from "material-ui-helper";
import CircularProgress from "@material-ui/core/CircularProgress";


const PleaseWaitContainerStyle = {
    position: "absolute",
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
}
const progressStyle = {
    width: "25%"
}

export default function PleaseWaitContainer({loading, circularProgressProps = {}, ...props}) {

    const style = useMemo(() => {
        return {
            ...props.style,
            position: "relative"
        }
    }, [props.style])


    return (
        <Box {...props} style={style}>
            {props.children}
            {
                loading &&
                <Box style={PleaseWaitContainerStyle}>
                    <Box width={1} height={1} center={true}>
                        <CircularProgress
                            {...circularProgressProps}
                            style={{
                                ...progressStyle,
                                ...circularProgressProps.style
                            }}/>
                    </Box>
                </Box>
            }
        </Box>
    )
}
