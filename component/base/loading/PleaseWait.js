import React from "react";
import {grey} from "@material-ui/core/colors";
import {isClient, lang} from "../../../repository";
import ThreeDots from "./treeDots/ThreeDots";
import Skeleton from "@material-ui/lab/Skeleton";
import Typography from "../Typography";
import {Box} from "material-ui-helper";

export default function PleaseWait({fullPage = true, variant = 'h3', fontWeight = 400, color = grey[700], textRootStyle, textStyle, skeletonStyle, ...props}) {


    return (
        <Box width={1} minHeight={1} center={true} p={[1, 4]} flexDirectionColumn={true}>
            <Typography variant={"h6"} pb={2} fontWeight={400}>
                {lang.get("please_wait")}
            </Typography>
            <ThreeDots mr={0.5} dotSize={8}/>
        </Box>
    )
}


function PleaseWait2({fullPage = true, variant = 'h3', fontWeight = 400, color = grey[700], textRootStyle, textStyle, skeletonStyle, ...props}) {
    let style = {};
    if (fullPage && isClient())
        style = {
            height: window.innerHeight - 200,
        };
    return (
        <Box
            px={{
                xs: 0.2,
                md: 2,
                lg: 4
            }}
            py={{
                xs: 1,
                md: 2,
                lg: 4
            }}
            width={1}
            display={'flex'}
            flexDirection={'column'}
            alignItems={'center'}
            justifyContent={'center'}
            {...props}
            style={{
                position: "relative",
                ...style,
                ...props.style,
            }}>
            <Typography variant={variant} fontWeight={fontWeight} color={color}
                        style={{
                            position: 'absolute',
                            right: 0,
                            left: 0,
                            textAlign: 'center',
                            display: 'flex',
                            justifyContent: 'center',
                            ...textStyle,
                            ...textRootStyle
                        }}>
                {lang.get("please_wait")}
                <ThreeDots mr={0.5} dotSize={8}/>
            </Typography>
            <Skeleton variant={"rect"} style={{
                width: '100%',
                height: '100%',
                ...skeletonStyle
            }}/>
        </Box>
    )
}
