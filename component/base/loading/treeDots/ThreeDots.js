import React from 'react';
import Box from "@material-ui/core/Box";

export default function Loading({dotSize,...props}) {
    return (
        <Box aria-hidden="true" className="loadingDots" {...props}>
            <span className="css-1ojr9sz-LoadingDot"/>
            <span className="css-12a6fd1-LoadingDot"/>
            <span className="css-19d8ryt-LoadingDot"/>
        </Box>
    )
}
