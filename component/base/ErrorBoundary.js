import React, {useState} from "react";
import {DEBUG} from "../../pages/_app";
import Sentry from "../../lib/Sentry"
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useUserContext} from "../../context/UserContext";

export default function ErrorBoundary({elKey, sendToSentry = true, errorComponent, onError, ...props}) {
    const isLogin = useIsLoginContext();
    const [user] = useUserContext();
    const [error, setError] = useState(false)

    function onEr(e) {
        if (error)
            return;
        setError(true)
        try {
            onError(e)
        } catch (e) {
        }
        try {
            if (sendToSentry)
                Sentry.error(`Something broke => ${(elKey ? `${elKey}      WINDOW LOCATION --> ` : "") + window?.location?.href}`)
        } catch (e) {
        }
    }

    return (
        <ErrorBoundaryBase
            elKey={elKey}
            errorComponent={errorComponent}
            isAdmin={(isLogin && user && user.isStaff)}
            onError={onEr} {...props}>

            {props.children}
        </ErrorBoundaryBase>)
}

class ErrorBoundaryBase extends React.Component {
    state = {error: null, errorInfo: null};

    componentDidCatch(error, errorInfo) {
        this.setState({
            error: error,
            errorInfo: errorInfo
        });
        try {
            this.props.onError({
                error: error,
                errorInfo: errorInfo
            })
        } catch (e) {
        }
    }


    render() {
        if (this.state.errorInfo) {
            return (
               !(DEBUG || this.props.isAdmin) ?
                    this.props.errorComponent ?
                        <React.Fragment>
                            {this.props.errorComponent}
                        </React.Fragment> :
                        <React.Fragment/>
                    :
                    <div>
                        <h2>Something went wrong{this.props.elKey ? ` => ${this.props.elKey}.` : "."}</h2>
                        <details style={{margin: 10, whiteSpace: "pre-wrap"}}>
                            {this.state.error && this.state.error.toString()}
                            <br/>
                            {this.state.errorInfo.componentStack}
                        </details>
                    </div>
            );
        }

        return (
            <React.Fragment>
                {this.props.children}
                <React.Fragment/>
            </React.Fragment>
        );
    }
}

