import React from "react";
import {Calendar} from "react-modern-calendar-datepicker";
import Popover from "@material-ui/core/Popover";
import JDate from "../../utils/JDate";

export default function DatePicker({
                             renderComponent,
                             date,
                             minimumDate,
                             maximumDate,
                             onChange,
                             transformOrigin = {
                                 vertical: 'top',
                                 horizontal: 'center',
                             },
                             anchorOrigin = {
                                 vertical: 'bottom',
                                 horizontal: 'center',
                             },
                             ...props
                         }) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDateChange = (date) => {
        handleClose()
        try {
            onChange(date, JDate.timeStampToJalaliByFormat(`${date.year}-${date.month}-${date.day}`, "jYYYY-jM-jD"))
        } catch (e) {
        }
    }

    const open = Boolean(anchorEl);

    return (
        <React.Fragment>
            {renderComponent({onClick: handleClick})}
            <Popover
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={anchorOrigin}
                transformOrigin={transformOrigin}>
                <Calendar
                    value={date|| ((minimumDate && (_.toNumber(JDate.format(undefined,"jM")) !== minimumDate.month)) ? minimumDate : undefined)}
                    minimumDate={minimumDate}
                    maximumDate={maximumDate}
                    onChange={handleDateChange}
                    shouldHighlightWeekends
                    locale={"fa"}/>
            </Popover>
        </React.Fragment>
    )
}


export function convertDate(year,month,day) {

    return{
        year: year,
        month: month,
        day: day
    }
}
