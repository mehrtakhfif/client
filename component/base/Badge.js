import React from "react";
import Badge from "@material-ui/core/Badge";
import {makeStyles} from "@material-ui/core/styles";


const useStyles = makeStyles(theme => (
    {
        customBadge: props => ({
            color: props?.color,
            backgroundColor: props?.backgroundColor,
            ...props?.badgeStyle
        })
    }));


function BadgeComponent({color, backgroundColor, badgeContent,badgeStyle={}, ...props}) {
    const classes = useStyles({color: color, backgroundColor: backgroundColor,badgeStyle});
    return (
        <Badge
            classes={{badge: classes.customBadge}}
            badgeContent={badgeContent} {...props}>
            {props.children}
        </Badge>
    )
}

export default (BadgeComponent);
