import React, {useMemo, useState} from "react";
import {Img as SmartImg} from "smart-image-lazyload"
import {FullWidthSkeleton, gLog, tryIt, UtilsStyle} from "material-ui-helper";
import {GA, media} from "../../repository";
import {DEBUG} from "../../pages/_app";
import Image from "next/image"
import clsx from "clsx";


const shimmer = (w, h) => `
<svg width="${w}" height="${h}"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <linearGradient id="g">
      <stop stop-color="#e6e6e6" offset="20%" />
      <stop stop-color="#dedede" offset="50%" />
      <stop stop-color="#e6e6e6" offset="70%" />
    </linearGradient>
  </defs>
  <rect width="${w}" height="${h}" fill="#e6e6e6" />
  <rect id="r" width="${w}" height="${h}" fill="url(#g)" />
  <animate xlink:href="#r" attributeName="x" from="-${w}" to="${w}" dur="1s" repeatCount="indefinite"  />
</svg>`

const toBase64 = (str) =>
    typeof window === 'undefined'
        ? Buffer.from(str).toString('base64')
        : window.btoa(str)


export const createImage = (width, height) => `data:image/svg+xml;base64,${toBase64(shimmer(width, height))}`


export default function Img({
                                className,
                                src,
                                placeholderSrc,
                                convertPh,
                                alt,
                                placeholderWidth,
                                imageWidth,
                                imageHeight,
                                imageProps,
                                borderRadius,
                                ...props
                            }) {
    const [error, setError] = useState(false)
    const ph = useMemo(() => {
        let phWidth = tryIt(() => Math.round(imageWidth / 3), 30)
        phWidth = phWidth > 30 ? phWidth : 30

        if (placeholderSrc === undefined)
            return media.convertor(src, {
                width: phWidth,
                height: Math.round((phWidth * imageHeight) / imageWidth),
                quality: 30
            })
        return placeholderSrc
    }, [src, placeholderSrc, imageWidth, imageHeight])

    const imgPl = createImage(imageWidth, imageHeight)
    return (
        <Image
            className={clsx(className, borderRadius ? `border-radius-${borderRadius}` : undefined)}
            alt={alt}
            src={!error ? (src || imgPl ): imgPl}
            placeholder={"blur"}
            blurDataURL={ph || `data:image/svg+xml;base64,${toBase64(shimmer(imageWidth, imageHeight))}`}
            width={imageWidth}
            height={imageHeight}
            onError={() => {
                setError(true)
            }}
            {...props}/>
    )
}