import React, {useMemo} from "react";
import {tryIt, Typography} from "material-ui-helper";
import NextLink from "next/link";
import _ from "lodash";
import PropTypes from 'prop-types'


function Nlink({
                   prefetch = false,
                   href: hr,
                   as: asHref,
                   hoverStyle = false,
                   onClick,
                   variant,
                   color = "#000",
                   disabled = false,
                   replace,
                   scroll,
                   shallow,
                   local,
                   passHref = true,
                   ...props
               }) {

    const {href, as} = useMemo(() => {
        return tryIt(() => {
            if (_.isArray(hr)) {
                return {
                    href: hr[0],
                    as: hr[1]
                }
            }
            if (_.isObject(hr))
                return {
                    href: hr.rout || "/",
                    as: hr.as
                }
            throw ""
        }, {
            href: hr || "/",
            as: asHref
        })
    }, [hr, asHref, disabled])


    return (
        <NextLink
            href={href || '/'}
            as={as}
            prefetch={prefetch}
            replace={replace}
            scroll={scroll}
            shallow={shallow}
            passHref={passHref}
            local={local}>
            <Typography
                component={"a"}
                variant={variant}
                color={color}
                prefetch_data={prefetch ? "true" : undefined}
                no_hover={!hoverStyle ? "true" : undefined}
                onClick={(e) => {
                    e.stopPropagation();
                    if (disabled)
                        return false
                    tryIt(() => {
                        onClick(e)
                    })
                }}
                {...props}>
                {props.children}
            </Typography>
        </NextLink>
    )
}

Nlink.propTypes = {
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object,PropTypes.array]),
    as: PropTypes.string,
    disabled: PropTypes.bool,
    prefetch: PropTypes.bool,
    replace: PropTypes.bool,
    scroll: PropTypes.bool,
    passHref: PropTypes.bool,
    shallow: PropTypes.any,
    local: PropTypes.any
}

export default Nlink;

function Test({...props}) {

    return (
        <a {...props}>
            safasfasfasf
        </a>
    )
}

