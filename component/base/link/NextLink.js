import React from "react";
import {tryIt, Typography} from "material-ui-helper";
import NextLinkBase from "next/link";


export default function NextLink({
                   prefetch = false,
                   href,
                   hoverStyle = true,
                   onClick,
                   variant,
                   color = "#000",
                   disabled = false,
                   replace,
                   scroll,
                   shallow,
                   local,
                   passHref = true,
                   ...props
               }) {

    const el = (
        <Typography
            component={"a"}
            variant={variant}
            color={color}
            prefetch_data={prefetch ? "true" : undefined}
            no_hover={!hoverStyle ? "true" : undefined}
            onClick={(e) => {
                e.stopPropagation();
                if (disabled)
                    return false
                tryIt(() => {
                    onClick(e)
                })
            }}
            {...props}>
            {props.children}
        </Typography>
    )



    return (
        <NextLinkBase
            href={href || '/'}
            prefetch={prefetch}
            replace={replace}
            scroll={scroll}
            shallow={shallow}
            passHref={passHref}
            local={local}>
            {el}
        </NextLinkBase>
    )
}
