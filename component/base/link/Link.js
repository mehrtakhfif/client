import React from 'react'
import {makeStyles} from "@material-ui/core";
import NLink from "next/link";
import {colors} from "../../../repository";
import {grey} from "@material-ui/core/colors";
import _ from 'lodash';
import rout from "../../../router";
import {Typography} from "material-ui-helper";
import clsx from "clsx";

const useStyles = makeStyles(theme => ({
    linkRoot: props => {
        return ({
            color: props.color,
            '&>*': {
                color: props.color,
            },
            "&:hover": {
                color: props.hoverColor,
                '& *': {
                    color: props.hoverColor,
                }
            },
        });
    },
    clearLink: {
        color: 'unset',
        '&:hover': {
            opacity: 1,
            color: 'unset',
        },
        '&:hover *': {
            opacity: 1,
            color: 'unset',
        }
    }
}));

export default function Link({
                                 prefetch = false,
                                 variant = 'body1',
                                 fontWeight,
                                 color = grey[800],
                                 hoverColor = colors.primary.main,
                                 openNewTab = false,
                                 hasHover = true,
                                 hasHoverBase = true,
                                 className,
                                 cursorIsPointer = true,
                                 href = "#",
                                 as,
                                 component = 'span',
                                 isText = true,
                                 rel,
                                 linkStyle,
                                 target,
    disable,
                                 ...props
                             }) {

    if (!hasHover)
        hoverColor = undefined;

    const classes = useStyles({
        color: color,
        hoverColor: hoverColor,
        hasHover: hasHover,
        ...props
    });


    if (_.isArray(href)) {
        as = href?.[1];
        href = href?.[0];
    } else if (_.isObject(href)) {
        as = href.as;
        href = href.rout;
    }

    if (href === undefined) {
        href = rout.Main.home
    }


    return (
        <NLink href={href || '/'} as={as} prefetch={prefetch} passHref>
            {isText ?
                    <Typography
                        component={'a'}
                        no_hover={!(hasHoverBase) ? 'true' : undefined}
                        rel={rel}
                        target={target}
                        fontWeight={fontWeight} variant={variant} color={color}
                        hoverProps={{
                            color: hoverColor,
                        }}
                        className={clsx([classes.linkRoot,disable ? "avoidClicks":'', className || ""])}
                        {...props}
                        style={{
                            ...props?.style,
                            ...linkStyle
                        }}>
                        {props.children}
                    </Typography>
                : props.children}
        </NLink>
    )
}
