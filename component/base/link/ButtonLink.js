import React from "react";
import {UtilsStyle} from "../../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import Link from "./Link";

export default function ButtonLink({href, border, backgroundColor, color, fontWeight, variant = 'body1', hoverable = false,hoverColor,target, textRootStyle, textStyle, ...props}) {
    return (
        <ButtonBase
            {...props}
            style={{
                backgroundColor: backgroundColor,
                ...UtilsStyle.borderRadius(5),
                ...props.style,
            }}>
            <Link
                href={href}
                hasHoverBase={hoverable || hoverColor}
                hasHover={hoverable || hoverColor}
                hoverColor={hoverColor}
                variant={variant}
                color={color}
                target={target}
                fontWeight={fontWeight}
                p={1}
                style={{
                    color: color,
                    ...textStyle,
                    ...textRootStyle
                }}>
                {props.children}
            </Link>
        </ButtonBase>
    )
}
