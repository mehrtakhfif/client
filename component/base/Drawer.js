import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {lang, theme as siteTheme} from "../../repository";
import rout from '../../router'
import Box from "@material-ui/core/Box";
import Typography from "./Typography";
import Link from "./link/Link";
import {Home, Menu, ShoppingCart} from "@material-ui/icons";
import Divider from "@material-ui/core/Divider";
import {grey} from "@material-ui/core/colors";
import {UtilsStyle} from "../../utils/Utils";
import {useBasketCountContext} from "../../context/BasketCount";

const useStyles = makeStyles({
    list: {},
    fullList: {
        width: 'auto',
    },
});


export default function SwipeableTemporaryDrawer(props) {
    const basketCount =useBasketCountContext()

    const classes = useStyles();
    const [state, setState] = React.useState({
        left: false,
    });
    const menuItem = [
        [
            {
                icon: <Home/>,
                name: lang.get("home"),
                link: rout.Main.home
            },
            {
                icon: <Menu/>,
                name: lang.get("product_group_list"),
                link: rout.Main.BoxList.create({})
            }
        ], [
        ],
    ];

    const toggleDrawer = (open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        if (open === false) {
            props.onClose()
        }
        setState({...state, ['left']: open});
    };

    const sideList = (
        <Box
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
            minWidth={{
                xs: 150,
                md: 300
            }}>
            <Box display={'flex'}
                 position={'relative'}
                 pr={1}
                 minHeight={{
                     xs: 70,
                     md: 150
                 }}>
                {/*<Link*/}
                {/*    href={rout.User.Auth.Login.rout}*/}
                {/*    style={{*/}
                {/*        position: 'absolute',*/}
                {/*        bottom: 5*/}
                {/*    }}>*/}
                {/*    <Box display={'flex'} alignItems={'center'} pr={1}>*/}
                {/*        {(isLogin) ?*/}
                {/*            <>*/}
                {/*                <Person/>*/}
                {/*                <Typography mr={0.5} variant={'body2'}>*/}
                {/*                    {(user.full_name )? user.full_name : user.username}*/}
                {/*                </Typography>*/}
                {/*            </> :*/}
                {/*            <>*/}
                {/*                <Lock/>*/}
                {/*                <Typography mr={0.5} variant={'body2'}>*/}
                {/*                    {lang.get("signup_or_login")}*/}
                {/*                </Typography>*/}
                {/*            </>}*/}
                {/*    </Box>*/}
                {/*</Link>*/}
            </Box>
            <List>
                {menuItem[0].map(({name, icon, link, ...item}, index) => (
                    <Item key={index} icon={icon} name={name} link={link} {...item}/>
                ))}
                <Divider/>
                <Item icon={<ShoppingCart/>} name={lang.get("basket")} badge={basketCount}
                      link={rout.User.Basket.rout}/>
                <Divider/>
                {menuItem[1].map(({name, icon, link, ...item}, index) => (
                    <Item key={index} icon={icon} name={name} link={link} {...item}/>
                ))}
            </List>
        </Box>
    );

    return (
        <div>
            {props.children}
            <ThemeProvider theme={{
                ...siteTheme,
                direction: 'ltr',
            }}>
                <SwipeableDrawer
                    anchor="right"
                    open={state.left || props.open}
                    onClose={toggleDrawer(false)}
                    onOpen={toggleDrawer(true)}>
                    {sideList}
                </SwipeableDrawer>
            </ThemeProvider>
        </div>
    );
}

function Item({icon = null, name, link, badge = null}) {
    return (
        <Link href={link}>
            <ListItem button style={{
                textAlign: 'right'
            }}>
                <Box display={'flex'}
                     alignItems={'center'}
                     width={1}
                     py={0.5}>
                    <Box display={'flex'}
                         alignItems={'center'}
                         flex={1}>
                        {icon ? icon : <Home style={{opacity: 0}}/>}
                        <Typography
                            variant={'body1'}
                            pr={1}>
                            {name}
                        </Typography>
                    </Box>
                    {badge &&
                    <Box style={{
                        background: grey[300],
                        width: 25,
                        height: 25,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: grey[900],
                        ...UtilsStyle.borderRadius('50%'),
                    }}>
                        {badge}
                    </Box>
                    }
                </Box>
            </ListItem>
        </Link>
    )
}
