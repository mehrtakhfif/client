import {useTheme} from "@material-ui/core";
import React, {useState} from "react";
import IconButton from "@material-ui/core/IconButton";
import {grey} from "@material-ui/core/colors";
import {Box} from "material-ui-helper";
import ControllerUser from "../../controller/ControllerUser";
import ErrorBoundary from "./ErrorBoundary";
import Tooltip from "./Tooltip";
import MtIcon from "../MtIcon";


export default function Wish({wish: wh, productId, permalink,iconButtonSize="small", ...props}) {
    const theme = useTheme();
    const [backupWish, setBackupWish] = useState(Boolean(wh))
    const [wish, setWish] = useState(Boolean(wh))
    const [timer, setTimer] = useState()


    function handleWishChange() {
        setWish(!wish)
        clearTimeout(timer);
        setTimer(setTimeout(() => {
            update(wish)
        }, 2500));
    }

    function update(wish) {
        ControllerUser.User.Profile.Wishlist.update({productId: productId, wish: !wish}).then(() => {
            setBackupWish(wish)
        }).catch(() => {
            setWish(backupWish)
        })
    }

    return (
        <ErrorBoundary elKey={"Wish::(component->productV2->storagePanel->Wish.js)"}>
            <Box
                pr={{
                    xs: 0,
                    lg: 2
                }} {...props}>
                <Tooltip title={wish ? "حذف علاقه مندی‌ها" : "افزودن به علاقه مندی‌ها"}>
                    <IconButton
                        size={iconButtonSize}
                        onClick={handleWishChange}>
                        {!wish ?
                            <MtIcon icon={"mt-heart"} color={grey[500]}/> :
                            <MtIcon icon={"mt-heart-filled"} color={theme.palette.primary.main}/>
                        }
                    </IconButton>
                </Tooltip>
            </Box>
        </ErrorBoundary>
    )
}

