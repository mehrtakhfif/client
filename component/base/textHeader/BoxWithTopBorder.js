import React from "react";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core";
import {UtilsConverter, UtilsStyle} from "../../../utils/Utils";
import {colors, theme} from "../../../repository";
import Divider from "@material-ui/core/Divider";

const style = makeStyles(theme => ({
    root: props => ({
        ...UtilsStyle.transition(500),
        "&:before": {
            content: "''",
            width: '100%',
            height: props.borderStork,
            backgroundColor: props.borderColor,
            ...UtilsStyle.transition(500),
        }
    }),
}));
export default function BoxWithTopBorder({title, borderStork = 4, borderColor = colors.primary.main, ...props}) {
    const classes = style({
        borderColor: borderColor,
        borderStork:borderStork,
        ...props,
    });
    return (
        <Box display={'flex'}
             flexDirection={'column'}
             className={classes.root}
             {...props}>
            {title &&
            <>
                <Box py={1.5} px={1} component={'h6'} fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 1)}>
                    {title}
                </Box>
                <Divider/>
            </>}
            {props.children}
        </Box>
    )

}
