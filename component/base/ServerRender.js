import React, {useEffect, useState} from 'react'
import {isServer} from "../../repository";
import {DEBUG} from "../../pages/_app";


export default function ServerRender({...props}) {
    const [state, setState] = useState(isServer())
    useEffect(() => {
        setState(DEBUG)
    }, [])
    return (
        state ?
            <React.Fragment>
                    {props.children}
            </React.Fragment>:
            <React.Fragment/>
    )
}
