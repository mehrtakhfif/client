import React, {useEffect, useState} from "react";
import {lang} from "../../../repository";
import {Box, Button, HiddenLgUp, HiddenMdDown, tryIt, useOpenWithBrowserHistory} from "material-ui-helper";
import ErrorBoundary from "../ErrorBoundary";
import MtIcon from "../../MtIcon";
import {useTheme} from "@material-ui/core";
import LgNumberItem from "./lgMenu/LgNumberItem";
import LgMenu from "./lgMenu/LgMenu";
import SmMenu from "./smMenu/SmMenu";


export default function NumberSelector({
                                           uniqKey,
                                           minNumber = 1,
                                           maxNumber,
                                           activeNumber,
                                           loading,
                                           text = lang.get("number"),
                                           onChange,
                                           onClose,
                                           smButtonProps = {},
                                           lgButtonProps = {}
                                       },
) {
    const theme = useTheme();
    const [open, setOpen] = useState(undefined)
    const [openNumberSelector, __, onOpenNumberSelector, onCloseNumberSelector] = useOpenWithBrowserHistory(`numberSelector-${uniqKey}`)


    useEffect(() => {
        if (activeNumber < minNumber) {
            onChange(minNumber)
            return
        }
        if (activeNumber > maxNumber) {
            onChange(maxNumber)
        }
    }, [minNumber, maxNumber])


    function handleItemSelect(num) {
        tryIt(() => onChange(num))
        handleClose()
    }

    function handleButtonClick(event) {
        if (loading)
            return
        if (event) {
            setOpen(event.currentTarget)
            return;
        }
        onOpenNumberSelector()
    }

    function handleClose() {

        tryIt(() => {
            if (open)
                setOpen(undefined)
        })
        tryIt(() => {
            if (openNumberSelector)
                onCloseNumberSelector()
        })
        tryIt(() => onClose())
    }


    if (maxNumber <= 1 || minNumber === maxNumber)
        return <React.Fragment/>


    return (
        <ErrorBoundary elKey={"NumberSelector"}>
            <HiddenLgUp>
                <Button variant={"outlined"}
                        disabled={loading}
                        onClick={() => handleButtonClick()}
                        {...smButtonProps}
                        buttonProps={{
                            ...smButtonProps?.buttonProps,
                            style: {padding: 0, ...smButtonProps?.buttonProps?.style}
                        }}>
                    <Box py={{
                        xs:1,
                        md:1.6
                    }} px={1.5} alignCenter={true}>
                        <LgNumberItem number={activeNumber || minNumber}/>
                        <Box pl={1}>
                            <MtIcon
                                icon={"mt-chevron-down"}
                                style={{
                                    fontSize: theme.spacing(2.5)
                                }}/>
                        </Box>
                    </Box>
                </Button>
                <SmMenu
                    open={openNumberSelector}
                    minNumber={minNumber}
                    maxNumber={maxNumber}
                    onItemSelect={handleItemSelect}
                    onClose={handleClose}/>
            </HiddenLgUp>
            <HiddenMdDown>
                <Button variant={"outlined"} disabled={loading} onClick={handleButtonClick}
                        {...lgButtonProps}
                        buttonProps={{
                            ...lgButtonProps?.buttonProps,
                            style: {padding: 0, ...lgButtonProps?.buttonProps?.style}
                        }}>
                    <Box py={1.6} px={1.5} alignCenter={true}>
                        <LgNumberItem number={activeNumber || minNumber}/>
                        <Box pl={1}>
                            <MtIcon
                                icon={"mt-chevron-down"}
                                style={{
                                    fontSize: theme.spacing(2.5)
                                }}/>
                        </Box>
                    </Box>
                </Button>
                <LgMenu
                    uniqKey={`numberSelector-${uniqKey}`}
                    open={open}
                    minNumber={minNumber}
                    maxNumber={maxNumber}
                    onItemSelect={handleItemSelect}
                    onClose={handleClose}/>
            </HiddenMdDown>
        </ErrorBoundary>
    )
}
