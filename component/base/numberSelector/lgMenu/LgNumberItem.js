import {useTheme} from "@material-ui/core";
import {Typography} from "material-ui-helper";
import {lang} from "../../../../repository";
import React from "react";

export default function LgNumberItem({number, isItems = false, ...props}) {
    const theme = useTheme();
    return (
        <Typography
            {...props}
            fontWeight={"normal"}
            alignItems={'center'}
            variant={{
                xs: "caption",
                md: "body2"
            }}>
            <Typography
                variant={{
                    xs: "body2",
                    md: "h6"
                }}
                fontWeight={"normal"}
                pr={{
                    xs: 0.1,
                    md: 0.5
                }}
                style={{
                    justifyContent: isItems ? 'flex-end' : 'center',
                    minWidth: theme.spacing(2.8)
                }}>
                {number}
            </Typography>
            {lang.get("number")}
        </Typography>
    )
}