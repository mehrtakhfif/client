import React from "react";
import {Box, ButtonBase} from "material-ui-helper";
import LgNumberItem from "./LgNumberItem";
import {Menu} from "@material-ui/core";


export default function LgMenu({uniqKey,open,maxNumber,minNumber,onItemSelect,onClose}){




    return(
        <Menu
            id={uniqKey}
            anchorEl={open}
            keepMounted
            open={Boolean(open)}
            onClose={onClose}>
            {
                [...Array(maxNumber - minNumber + 1).keys()].map(n => {
                    const num = n + minNumber;

                    const handleClick = () => {
                        onItemSelect(num)
                    }

                    return (
                        <Box key={num}>
                            <ButtonBase onClick={handleClick}>
                                <LgNumberItem
                                    px={2}
                                    py={1}
                                    isItems={true} number={num}/>
                            </ButtonBase>
                        </Box>
                    )
                })
            }
        </Menu>
    )
}