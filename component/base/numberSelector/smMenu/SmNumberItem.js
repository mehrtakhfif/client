import React from "react";
import {useTheme} from "@material-ui/core";
import DrawerItemContainer from "../../drawer/DrawerItemContainer";
import {Box, ButtonBase, Typography, UtilsStyle} from "material-ui-helper";
import {colors} from "../../../../repository";

export default function SmNumberItem({variant = "h5", onClick, ...props}) {
    const theme = useTheme();
    return (
        <DrawerItemContainer>
            <Box px={1}>
                <ButtonBase onClick={onClick}>
                    <Box
                        center={true}
                        minWidth={theme.spacing(8)}
                        minHeight={theme.spacing(8)}
                        style={{
                            border: `1px solid ${colors.borderColor.he5e5e5}`,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Typography
                            variant={variant}
                            fontWeight={"normal"}
                            style={{
                                justifyContent: 'center',
                                minWidth: theme.spacing(2.8)
                            }}>
                            {props?.children}
                        </Typography>
                    </Box>
                </ButtonBase>
            </Box>
        </DrawerItemContainer>
    )
}