import React from "react";
import BottomDrawer from "../../drawer/BottomDrawer";
import {Box} from "material-ui-helper";
import SmNumberItem from "./SmNumberItem";


export default function SmMenu({open, maxNumber, minNumber = 1, onItemSelect, onClose}) {

    return (
        <BottomDrawer open={open} onClose={onClose}>
            <Box component={'ul'} px={2} pb={4}>
                {
                    [...Array(maxNumber - minNumber + 1).keys()].map(n => {
                        const num = n + minNumber;

                        const handleClick = () => {
                            onItemSelect(num)
                        }
                        return (
                            <SmNumberItem key={n} isItems={true} number={num} onClick={handleClick}>
                                {num}
                            </SmNumberItem>
                        )
                    })
                }
            </Box>
        </BottomDrawer>
    )
}