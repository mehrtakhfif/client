import React from "react";
import Box from "@material-ui/core/Box";
import Link from "../link/Link";
import {HiddenLgUp, HiddenMdDown, Typography} from "material-ui-helper";
import Img from "../Img";
import {media, theme} from "../../../repository";
import Slider from "./Slider";
import {useTheme} from "@material-ui/core";
import MtIcon from "../../MtIcon";
import {DEBUG} from "../../../pages/_app";
import {makeStyles} from "@material-ui/styles";


const useStyle = makeStyles(theme => ({
    homeSliderSm: {
        '&>span': {
            width: '100% !important'
        },
        '&>span>span>img': {
            width: '100% !important'
        }
    }
}));

function FullScreenSlider({
                                             items,
                                             sliderKey,
                                             scrollbar,
                                             autoPlaySpeed,
                                             loop,
                                             pagination = true,
                                             prevIcon = "mt-chevron-right",
                                             nextIcon = "mt-chevron-left",
                                             iconProps = {},
                                             nextIconProps = {},
                                             prevIconProps = {},
                                             buttonRootProps = {},
                                             nextButtonRootProps = {},
                                             prevButtonRootProps = {},
                                             ...props
                                         }) {
    const classes = useStyle()
    const theme = useTheme();
    return (
        <Slider
            sliderKey={`full-screen-${sliderKey}`}
            slidesPerView={1}
            loop={loop}
            sliderButton={true}
            items={items}
            centeredSlides={false}
            scrollbar={scrollbar}
            autoPlaySpeed={autoPlaySpeed}
            prevIcon={prevIcon}
            nextIcon={nextIcon}
            iconProps={iconProps}
            nextIconProps={nextIconProps}
            prevIconProps={prevIconProps}
            buttonRootProps={buttonRootProps}
            nextButtonRootProps={nextButtonRootProps}
            prevButtonRootProps={prevButtonRootProps}
            breakpoints={{
                0: {
                    slidesPerView: 1.1,
                    spaceBetween: 0,
                    pagination: true
                },
                // when window width is >= 640px
                640: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    pagination: false
                }
            }}
            render={({id, imageSrc, imageAlt, url, target, disable}, index) => (
                <React.Fragment>
                    <HiddenLgUp>
                        <Box
                            pl={1.5}
                            pr={1.5}
                            width={'auto'}
                            style={{
                                position: 'relative'
                            }}>
                            {
                                disable &&
                                <Disable/>
                            }
                            <Link href={url} disable={!url}
                                  className={classes.homeSliderSm} target={target} hasHoverBase={false}
                                  hasHover={false}>
                                <Img
                                    imageWidth={media.slider.XS.width}
                                    imageHeight={media.slider.XS.height}
                                    borderRadius={5}
                                    alt={imageAlt}
                                    src={DEBUG ? "https://api.mehrtakhfif.com/media/categories/None/2021-10-31/mobile_slider/17-29-34-24-has-ph.jpg" : imageSrc}/>
                            </Link>
                        </Box>
                    </HiddenLgUp>
                    <HiddenMdDown>
                        <Box
                            width={1}
                            style={{
                                position: 'relative'
                            }}>
                            {
                                disable &&
                                <Disable/>
                            }
                            <Link href={url} disable={!url} target={target} hasHoverBase={false} hasHover={false}>
                                <Img
                                    imageWidth={media.slider.width}
                                    imageHeight={media.slider.height}
                                    alt={imageAlt}
                                    src={imageSrc}/>
                            </Link>
                        </Box>
                    </HiddenMdDown>
                </React.Fragment>
            )}
            {...props}
            style={{
                paddingBottom: scrollbar || pagination ? theme.spacing(2.5) : 0,
                ...props?.style
            }}
        />
    )
}

export default React.memo(FullScreenSlider)

function Disable() {
    const theme = useTheme()

    return (
        <Typography
            py={1}
            px={2}
            color={"#fff"}
            variant={"h6"}
            alignItems={'center'}
            style={{
                zIndex: 1,
                backgroundColor: theme.palette.error.light,
                position: 'absolute'
            }}>
            <MtIcon
                icon={"mt-exclamation-circle"}
                color={"#fff"}
                style={{
                    marginLeft: 10
                }}/>
            پیشنمایش
        </Typography>
    )
}


export const createSliderItem = ({id, imageSrc, imageAlt, url, target, disable}) => {
    return {id, imageSrc, imageAlt, url, target, disable}
}


