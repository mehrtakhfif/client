import React, {useMemo} from "react";
import _ from "lodash"
import Box from "@material-ui/core/Box";
import {ButtonBase, getSafe, UtilsStyle} from "material-ui-helper";
import MtIcon from "../../MtIcon";
import {Swiper, SwiperSlide} from 'swiper/react';
import {colors} from "../../../repository";
import {useTheme} from "@material-ui/core";
import clsx from "clsx";


function Slider({
                                   sliderKey,
                                   className,
                                   items,
                                   slidesPerView,
                                   breakpoints: br,
                                   navigation = true,
                                   scrollbar = false,
                                   pagination = false,
                                   autoPlaySpeed = false,
                                   centeredSlides,
                                   loop = false,
                                   freeMode = false,
                                   sliderButton = false,
                                   swiperSlide = true,
                                   render,
                                   prevIcon = "mt-chevron-right",
                                   nextIcon = "mt-chevron-left",
                                   iconProps = {},
                                   nextIconProps = {},
                                   prevIconProps = {},
                                   buttonRootProps = {},
                                   nextButtonRootProps = {},
                                   prevButtonRootProps = {},
                                   containerProps = {},
                                   ...props
                               }) {

    const breakpoints = useMemo(() => {
        let b = br || {};
        if (navigation) {
            b[1280] = b[1280] || {navigation: {}};
            b[1280].navigation = {
                nextEl: `.slider-next-${sliderKey}`,
                prevEl: `.slider-prev-${sliderKey}`,
            }
        }

        return b
    }, [navigation, br])


    const renderedData = useMemo(() => {
        return getSafe(() => items?.map((it, index) => {
            if (swiperSlide)
                return (
                    <SwiperSlide key={it?.id || index}>
                        {render(it, index)}
                    </SwiperSlide>
                )
            return (
                render(it, index)
            )
        }), [])
    }, [items, render])


    return (
        <Box display={'flex'}
             flexDirection={'column'} position={'relative'}
             width={1}
             className={clsx([
                 className,
                 `base-swiper-slider slider-${sliderKey}`
             ])}
             {...containerProps}>
            <Box>
                <Swiper
                    scrollbar={(_.isBoolean(scrollbar) && scrollbar) ? {draggable: false} : scrollbar}
                    breakpoints={breakpoints}
                    loop={loop}
                    centeredSlides={centeredSlides}
                    freeMode={freeMode}
                    pagination={pagination}
                    slidesPerView={slidesPerView}
                    autoplay={autoPlaySpeed ? {
                        delay: autoPlaySpeed,
                        disableOnInteraction: true,
                    } : undefined}
                    {...props}>
                    {renderedData}
                </Swiper>
                {
                    sliderButton &&
                    <React.Fragment>
                        <SliderButton
                            isNextButton={true}
                            sliderKey={sliderKey}
                            icon={nextIcon}
                            buttonRootProps={{
                                ...buttonRootProps,
                                ...prevButtonRootProps,
                                style: {
                                    ...buttonRootProps?.style,
                                    ...prevButtonRootProps?.style,
                                }
                            }}
                            iconProps={{
                                ...iconProps,
                                ...prevIconProps,
                            }}/>
                        <SliderButton
                            isNextButton={false}
                            sliderKey={sliderKey}
                            icon={prevIcon}
                            buttonRootProps={{
                                ...buttonRootProps,
                                ...nextButtonRootProps,
                                style: {
                                    left: 0,
                                    ...buttonRootProps?.style,
                                    ...nextButtonRootProps?.style,
                                }
                            }}
                            iconProps={{
                                ...iconProps,
                                ...nextIconProps,
                            }}/>
                    </React.Fragment>
                }
            </Box>
        </Box>
    )
}

export default React.memo(Slider)


export function SliderButton({
                                 buttonRootProps,
                                 sliderKey,
                                 icon,
                                 iconProps,
                                 isNextButton,
                                 ...props
                             }) {
    const theme = useTheme();

    return (
        <ButtonBase
            buttonProps={{
                elevation: 2,
                ...buttonRootProps,
                className: `${(isNextButton ? `slider-next-${sliderKey}` : `slider-prev-${sliderKey}`) || ""} ${isNextButton ? "slider-next" : " slider-prev"} slider-arrows transition400 ${buttonRootProps?.className || ""}`,
                style: {
                    position: 'absolute',
                    maxHeight: "70px",
                    maxWidth: "70px",
                    overflow:'hidden',
                    left: !isNextButton ? undefined : "5%",
                    right: !isNextButton ? "5%" : undefined,
                    ...buttonRootProps?.style
                }
            }}
            {...props}>
            <Box
                boxShadow={1}
                style={{
                    maxHeight: "70px",
                    maxWidth: "70px",
                    ...UtilsStyle.borderRadius(5),
                    background: colors.backgroundColor.hffffff,
                }}>
                <MtIcon
                    icon={icon || (isNextButton ? "mt-chevron-left" : "mt-chevron-right")} {...iconProps}
                    style={{
                        display: "inline-table",
                        color: colors.textColor.h404040,
                        padding: theme.spacing(2),
                        fontSize: theme.spacing(4.5),
                        ...iconProps?.style,
                    }}/>
            </Box>
        </ButtonBase>
    )
}

