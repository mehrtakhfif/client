import React from "react";
import {Box} from "material-ui-helper";
import {headerOffset} from "../header/HeaderLg";
import StickyBoxBase from "react-sticky-box";


export default function StickyBox({offsetTop=headerOffset,children,...props}){
    return(
        <Box flexDirectionColumn={true} {...props}>
            <StickyBoxBase offsetTop={offsetTop} offsetBottom={10}>
                <Box width={1} flexDirectionColumn={true}>
                    {children}
                </Box>
            </StickyBoxBase>
        </Box>
    )
}