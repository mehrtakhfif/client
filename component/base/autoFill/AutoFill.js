import React from 'react';
import {makeStyles} from "@material-ui/styles";
import {createTheme, emphasize} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import {isLtr} from "../../../utils/Checker";
import {colors} from "../../../repository";
import Select from "react-select";
import Paper from "@material-ui/core/Paper";
import clsx from 'clsx'


const autoFillStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        height: 250,
        minWidth: 290,
    },
    input: {
        display: 'flex',
        padding: 0,
        height: 'auto',
        '& div[class*="loadingIndicator"] span:nth-child(1)': {
            marginLeft: '1em'
        }

    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden',
    },
    chip: {
        margin: theme.spacing(0.5, 0.25),
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
            0.08,
        ),
    },
    noOptionsMessage: {
        padding: theme.spacing(1, 2),
    },
    singleValue: {
        fontSize: 16,
    },
    placeholder: {
        position: 'absolute',
        left: 2,
        bottom: 6,
        fontSize: 16,
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
        '& *': {
            backgroundColor: '#fff !important',
            color: '#000 !important'
        }
    },
    divider: {
        height: theme.spacing(2),
    },
}));

function inputComponent({inputRef, ...props}) {
    return <div ref={inputRef} {...props} />;
}

inputComponent.propTypes = {
    inputRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({
            current: PropTypes.any.isRequired,
        }),
    ]),
};

function Control(props) {
    const {
        children,
        innerProps,
        innerRef,
        selectProps: {classes, TextFieldProps},
    } = props;

    return (
        <TextField
            fullWidth
            InputProps={{
                inputComponent,
                inputProps: {
                    className: classes.input,
                    ref: innerRef,
                    children,
                    ...innerProps,
                },
            }}
            {...TextFieldProps}
        />
    );
}

Control.propTypes = {
    /**
     * Children to render.
     */
    children: PropTypes.node,
    /**
     * The mouse down event and the innerRef to pass down to the controller element.
     */
    innerProps: PropTypes.shape({
        onMouseDown: PropTypes.func.isRequired,
    }).isRequired,
    innerRef: PropTypes.oneOfType([
        PropTypes.oneOf([null]),
        PropTypes.func,
        PropTypes.shape({
            current: PropTypes.any.isRequired,
        }),
    ]).isRequired,
    selectProps: PropTypes.object.isRequired,
};

function Menu(props) {
    return (
        <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
            {props.children}
        </Paper>
    );
}

Menu.propTypes = {
    /**
     * The children to be rendered.
     */
    children: PropTypes.element.isRequired,
    /**
     * Props to be passed to the menu wrapper.
     */
    innerProps: PropTypes.object.isRequired,
    selectProps: PropTypes.object.isRequired,
};

const components = {
    Control,
    Menu,
};


const useStyles = makeStyles(theme => ({
    input: ({hintIsLtr,...props}) => ({
        '& p[class*=makeStyles-placeholder]': {
            right: hintIsLtr ? 'auto' : '0',
            left: hintIsLtr ? 0 : 'auto',
        },
        '& [class*=MuiPaper-root]': {
            top: 50,
        },
        '&>div>label': {
            color: props.activeItem ? colors.success.main : '',
            left:'auto'
        },
        '& div[class*=MuiInput-underline]::before': {
            borderColor: props.activeItem ? colors.success.main : '',
        },
        '& div[class*=MuiInput-underline]::after': {
            borderColor: props.activeItem ? colors.success.main : '',
        },
    })
}));

export default function AutoFill(props) {
    const {
        inputId,
        title,
        placeholder,
        className,
        items,
        activeItem,
        onActiveItemChanged,
        disabled,
        validatable,
        loading
    } = props;
    const theme = createTheme();
    const hintIsLtr = isLtr(placeholder);
    const classes = useStyles({hintIsLtr,...props});
    const autoFillClasses = autoFillStyles();


    const selectStyles = {
        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
    };

    const componentClass = [classes.input];
    if (componentClass) {
        componentClass.push(className)
    }
    return (
        <Select
            classes={autoFillClasses}
            styles={selectStyles}
            className={clsx(componentClass)}
            inputId={inputId + '-select'}
            TextFieldProps={{
                label: title,
                InputLabelProps: {
                    htmlFor: inputId + '-select-single',
                    shrink: true,
                },
            }}
            placeholder={placeholder}
            options={items}
            inputProps={{autoComplete: 'off', autoCorrect: 'off', spellCheck: 'off'}}
            components={components}
            value={activeItem}
            onChange={onActiveItemChanged}
            isDisabled={disabled}
            isLoading={loading}
        />
    )
}

AutoFill.propTypes = {
    inputId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    items: PropTypes.array.isRequired,
    activeItem: PropTypes.object,
    onActiveItemChanged: PropTypes.func.isRequired,
    display: PropTypes.bool,
    loading: PropTypes.bool,
    validatable: PropTypes.bool,
};
