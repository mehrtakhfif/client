import React, {Fragment} from 'react'
import {UtilsConverter} from "../../utils/Utils";
import Popover from "@material-ui/core/Popover";
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import { withWidth} from "@material-ui/core";
import { createTheme } from '@material-ui/core/styles'
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import {useSnackbar} from "notistack";
import {lang} from "../../repository";
import Button from "@material-ui/core/Button";

function HelpIcon(props) {
    const {message, show_snackbar, icon_style, show_snackbar_in_upmd, show_snackbar_in_downmd, snackbar_duration} = props;
    const theme = createTheme();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = event => {
        let isLargeScreen = null;
        if (show_snackbar_in_downmd || show_snackbar_in_upmd) {
            isLargeScreen = (props.width) === 'md' || (props.width) === 'lg' || (props.width) === 'xl'
        }

        if (show_snackbar || (isLargeScreen !== null && ((isLargeScreen && show_snackbar_in_upmd) || (!isLargeScreen && show_snackbar_in_downmd)))) {
            enqueueSnackbar(message,
                {
                    variant: "info",
                    autoHideDuration: snackbar_duration,
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return
        }
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <>
            <HelpOutlineIcon
                onClick={handleClick}
                style={{
                    paddingRight: theme.spacing(0.5),
                    fontSize: UtilsConverter.addRem(theme.typography.body1.fontSize, 1),
                    position: 'relative',
                    cursor: 'pointer',
                    ...icon_style
                }}/>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Box
                    fontSize={UtilsConverter.addRem(theme.typography.body2.fontSize, 1)}
                    style={{
                        padding: theme.spacing(1),
                        direction: 'rtl',
                        maxWidth: 500
                    }}>
                    {message}
                </Box>
            </Popover>
        </>
    );
}

export default withWidth()(HelpIcon);
HelpIcon.defaultProps = {
    snackbar_duration: 6000
};
HelpIcon.propTypes = {
    message: PropTypes.string.isRequired,
    show_snackbar: PropTypes.bool,
    show_snackbar_in_upmd: PropTypes.bool,
    show_snackbar_in_downmd: PropTypes.bool,
    snackbar_duration: PropTypes.number,
    icon_style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
