import React from "react";
import MTooltip from "@material-ui/core/Tooltip";

export default function Tooltip({title, disable, ...props}) {
    return (
        (!disable && title) ?
            <MTooltip title={title}>
                {props.children}
            </MTooltip> :
            <React.Fragment>
                {props.children}
            </React.Fragment>
    )
}
