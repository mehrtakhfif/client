import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import {BugReportOutlined} from "@material-ui/icons";
import {red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../utils/Utils";
import Tooltip from "./Tooltip";
import {DEBUG} from "../../pages/_app";


export default function DebugBox({debug, ...props}) {

    return (
        <Box {...props}
             style={{position: "relative", ...props.style}}>
            {props.children}
            {
                debug &&
                <DebugCm debug={debug}/>
            }
        </Box>
    )
}

function DebugCm() {
    const [show, setShow] = useState(true)

    function handleClicked(e) {
        e.stopPropagation()
        setShow(false)
        setTimeout(() => {
            setShow(true)
        }, DEBUG ? 5000 : 10000)
    }

    return (
        (show) ?
            <Tooltip title={"پیش‌نمایش"}>
                <BugReportOutlined
                    onClick={handleClicked}
                    style={{
                        position: 'absolute',
                        top: 5,
                        left: 5,
                        cursor: "pointer",
                        color: red[400],
                        zIndex: 999,
                        background: 'rgba(255, 255, 255, 0.83)',
                        ...UtilsStyle.borderRadius(5)
                    }}/>
            </Tooltip> :
            <React.Fragment/>
    )
}
