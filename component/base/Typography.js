import React from "react";
import {isWidthDown, Typography as MaterialTypography} from "@material-ui/core";
import {theme} from "../../repository";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import useTheme from "@material-ui/core/styles/useTheme";
import withWidth from "@material-ui/core/withWidth";


const useTypographyStyles = makeStyles(theme => ({
    typographyRoot: (props => ({
        '&:hover': {
            color: props.hoverColor
        }
    }))
}));

function Typography({
    width,
                        component = 'div',
                        fontSize,
                        variant,
                        smVariant,
                        fontWeight,
                        children,
                        isText,
                        color,
                        hoverColor = null,
                        display = 'flex',
                        alignItems,
                        justifyContent,
                        flexDirection,
                        textAlign,
                        ...props
                    }) {
    const theme = useTheme();

    const classes = useTypographyStyles({hoverColor: hoverColor});
    props = generateStyle(props);
    const isSmall = isWidthDown('sm', props.width)


    return (
        <MaterialTypography
            component={component}
            className={classes.typographyRoot}
            variant={isSmall ? smVariant || variant : variant}
            {...props}
            style={{
                width:width === 1 ? '100%' : width,
                color: color ? color : theme.palette.text.primary,
                fontWeight: fontWeight,
                fontSize: fontSize,
                display: display,
                alignItems: alignItems,
                justifyContent: justifyContent,
                flexDirection: flexDirection,
                textAlign:textAlign,
                ...props.style
            }}>
            {children}
        </MaterialTypography>
    )
}

export default withWidth()(Typography)

function generateStyle(props) {
    const {p, px, py, pl, pr, pt, pb, m, mx, my, ml, mr, mt, mb, ...newProps} = props;
    const s = props.style ? props.style : {};
    //padding
    s.paddingLeft = getValue(pl || px || p);
    s.paddingRight = getValue(pr || px || p);
    s.paddingTop = getValue(pt || py || p);
    s.paddingBottom = getValue(pb || py || p);
    //margin
    s.marginLeft = getValue(ml || mx || m);
    s.marginRight = getValue(mr || mx || m);
    s.marginTop = getValue(mt || my || m);
    s.marginBottom = getValue(mb || my || m);


    return {
        ...newProps,
        style: {
            ...s
        }
    }
}

function getValue(v) {
    if (_.isNumber(v))
        return theme.spacing(v);
    return v
}
