import React from "react";
import Popover from "@material-ui/core/Popover";
import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "./Typography";
import {UtilsStyle} from "../../utils/Utils";
import {grey} from "@material-ui/core/colors";

const hours = (() => {
    const list = [];
    for (let i = 8; i <= 20; i++) {
        const start = {
            value: i,
            label: `${i}:00`
        };
        const end = {
            value: i + 1,
            label: `${i + 1}:00`
        };
        list.push({
            value: i,
            component: ({...props}) => (
                <Box display={'flex'}
                     py={1}
                     width={1}
                     alignItems={'center'}
                     justifyContent={'center'}
                     {...props}>
                    <Typography variant={"h6"} fontWeight={500}>
                        {start.label}
                    </Typography>
                    <Typography px={2} variant={"body1"}>
                        تا
                    </Typography>
                    <Typography variant={"h6"} fontWeight={500}>
                        {end.label}
                    </Typography>
                </Box>
            ),
            start: start,
            end: end
        })
    }
    return list;
})()

export default function TimePicker({
                             renderComponent,
                             date,
                             onChange,
                             transformOrigin = {
                                 vertical: 'center',
                                 horizontal: 'left',
                             },
                             anchorOrigin = {
                                 vertical: 'center',
                                 horizontal: 'right',
                             },
                             ...props
                         }) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleTimeChange = (date) => {
        handleClose()
        try {
            onChange(date)
        } catch (e) {
        }
    }

    const open = Boolean(anchorEl);

    return (
        <React.Fragment>
            {renderComponent({onClick: handleClick})}
            <Popover
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={anchorOrigin}
                transformOrigin={transformOrigin}>
                <Box px={0.5} py={1} display={'flex'} flexDirection={"column"} maxHeight={400}>
                    {
                        hours.map((hr) => (
                            <Box py={1} minWidth={260} px={2}>
                                <ButtonBase
                                    onClick={()=>{
                                        handleTimeChange(hr)
                                    }}
                                    style={{
                                    width: "100%",
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                                    {
                                        hr.component({
                                            style: {
                                                border: `1px solid ${grey[400]}`,
                                                ...UtilsStyle.borderRadius(5)
                                            }
                                        })
                                    }
                                </ButtonBase>
                            </Box>
                        ))
                    }
                </Box>
            </Popover>
        </React.Fragment>
    )
}


export function convertDate(year, month, day) {

    return {
        year: year,
        month: month,
        day: day
    }
}
