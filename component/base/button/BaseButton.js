import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {green} from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
}));

export default function BaseButton({id, loading, loadingStyle = {}, disabled, children, ...props}) {
    const classes = useStyles();
    return (
        <Button
            id={id}
            disabled={(loading || disabled)}
            {...props}>
            {children}
            {loading && <CircularProgress size={24} className={classes.buttonProgress}
                                          style={{
                                              ...loadingStyle
                                          }}/>}
        </Button>
    )
}
BaseButton.defaultProps = {
    size: 'medium',
    variant: "contained"
};

BaseButton.propTypes = {
    id: PropTypes.string,
    onClick: PropTypes.func,
    variant: PropTypes.oneOf(['text', 'contained', 'outlined']),
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool
};


