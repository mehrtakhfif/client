import React from 'react';
import Green from '@material-ui/core/colors/green'
import BaseButton from "../BaseButton";
import PropTypes from "prop-types";
import {UtilsStyle} from "../../../../utils/Utils";

export default function SuccessButton({style, ...props}) {
    return (
        <BaseButton
            {...props}
            style={{
                ...style,
                color: Green[50],
                backgroundColor: Green["A700"],
                ...UtilsStyle.widthFitContent(),
                '&:hover': {
                    backgroundColor: Green[500],
                },
            }}/>
    )
}
SuccessButton.propTypes = {
    id: PropTypes.string,
    onClick: PropTypes.func,
    variant: PropTypes.oneOf(['contained', 'outlined']),
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool
};





