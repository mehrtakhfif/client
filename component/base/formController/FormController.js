import React, {useEffect} from 'react'
import _ from "lodash";
import Box from "@material-ui/core/Box";


export default function FormControl({innerref, ...props}) {
    const [hasError, setHasError] = React.useState(false);

    useEffect(() => {
        setHasError(checkError(innerref.current.children))
    }, [checkError, innerref]);

    //region Props
    // const newProps = {};
    // _.forEach(props, function (value, key) {
    //     if (key === 'innerref')
    //         return;
    //     newProps[key] = value
    // });


    //endregion Props

    //region Functions
    function checkError(elements) {
        let hasError = false;
        try {
            _.forEach(elements, function (value) {
                if (value.attributes.haserror) {
                    //
                    //
                    //
                    //
                    hasError = true;
                    return false
                }
                if (!value.attributes.formcontrol && checkError(value.children)) {
                    hasError = true;
                    return false
                }
            });
        } catch (e) {
        }
        // if (hasError)
        //
        return hasError
    }

    //endregion Functions


    return (
        <Box ref={innerref}
             {...props}
             component="form"
             {...((hasError) ? {haserror: ""} : null)}
             formcontrol="true">
            {props.children}
        </Box>
    )
}
