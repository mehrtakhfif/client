import React, {useEffect} from "react";
import {tryIt} from "material-ui-helper";
import {useHeaderAndFooterContext} from "../../context/HeaderAndFooterContext";
import {GA} from "../../repository";
import {useRouter} from "next/router";


export default function BasePage({name,setting = {}, children}) {
    const {asPath} = useRouter();
    const [_, setVisibility] = useHeaderAndFooterContext()

    useEffect(() => {
        GA.initializePage({pageRout: asPath});
    }, [])



    useEffect(() => {
        tryIt(() => {
            setVisibility(setting)
        })
    }, [setting])



    return (
        <React.Fragment>
            {children}
        </React.Fragment>
    )
}


// BasePage.propTypes = {
//     setting: PropTypes.shape({
//         header: {
//             showLg: PropTypes.bool,
//             showSm: PropTypes.bool
//         },
//         footer: {
//             showLg: PropTypes.bool,
//             showSm: PropTypes.bool
//         },
//     })
// }

