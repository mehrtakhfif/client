import React from "react";
import Box from "@material-ui/core/Box";
import Search from "../../search/Search";
import {Dialog} from "material-ui-helper";


function SearchDialog({open,onClose}){

    return(
        <Dialog open={open} fullScreen={true}>
            <Box px={1} py={2}>
                <Search onClose={onClose} isMobile={true}/>
            </Box>
        </Dialog>
    )
}
export default React.memo(SearchDialog)