import React from "react";
import Img from "./oldImg/Img";
import PropTypes from "prop-types";
import {useTheme} from "@material-ui/core";

function getFilterId(color, isDark) {
    if (!color || color ==="default"){
        return isDark ? "svgWhite" : "svgBlack"
    }
    if (color ==="cyan"){
        return "svgCyan"
    }
    if (color ==="orange"){
        return "svgOrange"
    }
}

function Icon({width, height, src, alt, showSkeleton=false, color="default", ...props}) {
    const theme = useTheme();
    const isDarkMode = theme.palette.type === "dark";
    return (
        <Img width={width}
             height={height}
             src={src}
             alt={alt}
             minHeight={1}
             showSkeleton={showSkeleton}
             {...props}
             imgStyle={{
                 ...props.imgStyle,
                 filter: color ? `url(#${getFilterId(color, isDarkMode)})` : undefined
             }}/>
    )
}

export default Icon;

Icon.prototype = {
    color: PropTypes.oneOf(["default", "white", "black","cyan","orange"]),
};
