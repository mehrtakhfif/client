import React from "react";
import ServerRender from "./ServerRender";
import Box from "@material-ui/core/Box";


export default function Placeholder({...props}) {
    return (
        <ServerRender>
            <Box
                className={'mt-place-holder place'}
                {...props}
                style={{
                    position: 'absolute !important',
                    bottom: 0,
                    display: 'none !important',
                    opacity: 0,
                    ...props.style,
                }}>
                {
                    props.children ? props.children : "holder"
                }
            </Box>
        </ServerRender>
    )
}
