import {MuiThemeProvider, withStyles} from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import React, {useState} from "react";
import {colors, dir, lang, theme} from "../../repository";
import {DEBUG} from "../../pages/_app";
import {grey} from "@material-ui/core/colors";
import {Box, Typography} from "material-ui-helper";
import ErrorBoundary from "./ErrorBoundary";
import MtIcon from "../MtIcon";

const StyledRating = withStyles(theme => ({
    iconEmpty:{
        color: colors.backgroundColor.heff0ef
    },
    iconFilled: {
        color: theme.palette.primary.main,
    },
    iconHover: {
        color: theme.palette.primary.main,
    },
}))(Rating);

export default function RatingComponent({
                                    name = "product rate",
                                    rate,
                                    showNoRateSubmit = true,
                                    onRatingChange,
                                    readOnly = false,
                                    size = 'medium',
                                    ...props
                                }) {
    const [state, setState] = useState({
        onHover: false
    });

    return (
        <ErrorBoundary elKey={"RatingComponent::(component->productV2->storagePanel->RatingComponent.js)"}>
            {(rate && rate > 0) ?
                <MuiThemeProvider
                    theme={{
                        ...theme,
                        direction: 'ltr'
                    }}>
                    <Box display={'flex'} flex={1} alignItems={'center'} style={{direction: 'ltr'}}>
                        <StyledRating
                            name={name}
                            readOnly={readOnly}
                            size={size}
                            color={"primary"}
                            icon={<MtIcon icon={"mt-star-filled"} fontSize="small" />}
                            onMouseEnter={(e) => {
                                if (!readOnly)
                                    setState({
                                        ...state,
                                        onHover: true
                                    })
                            }}
                            onMouseLeave={(e) => {
                                if (!readOnly)
                                    setState({
                                        ...state,
                                        onHover: false
                                    })
                            }}
                            onChange={(e, val) => {
                                if (!readOnly)
                                    onRatingChange(val)
                            }}
                            precision={state.onHover ? 1 : 0.5}
                            value={state.onHover ? 0 : rate}/>
                        {(showNoRateSubmit && !rate) ?
                            <Typography pt={1} variant={'subtitle2'} style={{direction: dir}}>
                                {lang.get("no_rate")}
                            </Typography> :
                            <React.Fragment>
                                <Typography variant={"body2"} fontWeight={500} pl={0.8}>
                                    {rate}
                                </Typography>
                                {DEBUG &&
                                <Typography pl={0.5} variant={"body2"} fontWeight={500} color={grey[500]}>
                                    ({53})
                                </Typography>
                                }
                            </React.Fragment>}
                    </Box>
                </MuiThemeProvider> :
                <React.Fragment/>}
        </ErrorBoundary>
    )
}
