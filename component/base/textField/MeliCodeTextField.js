import TextFieldContainer, {createName, errorList} from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import {lang} from "../../../repository";
import ReactNumberFormat from "react-number-format";
import _ from "lodash";


const meliCodeErrorList = [(value) => {
    let input = value.replaceAll(/_/g, "").trimAll()
    if (!/^\d{10}$/.test(input))
        return false;

    let check = parseInt(input[9]);
    let sum = 0;
    let i;
    for (i = 0; i < 9; ++i) {
        sum += parseInt(input[i]) * (10 - i);
    }
    sum %= 11;
    return !((sum < 2 && check === sum) || (sum >= 2 && (check + sum) === 11));
}];

export default function MeliCodeField({group, name, defaultValue, required, disabled, label = lang.get("meli_code"), variant = "standard", onChange, ...props}) {
    let codeRef = null;

    return (
        <React.Fragment>
            <TextFieldContainer
                defaultValue={_.toString(defaultValue)}
                name={createName({group: group, name: name})}
                type={'text'}
                onChangeDelay={undefined}
                errorPatterns={meliCodeErrorList}
                returnValue={(value) => {
                    return value.replaceAll(/_/g, "").trimAll()
                }}
                render={(ref, {name, initialize, valid, errorIndex, errorText, inputProps, setValue, props}) => {
                    codeRef = ref;
                    return (
                        <TextField
                            {...props}
                            error={!valid}
                            variant={variant}
                            helperText={[...(required ? errorList : []), "کدملی را درست وارد کنید."][errorIndex]}
                            name={name}
                            inputRef={ref}
                            disabled={disabled}
                            fullWidth
                            placeholder={lang.get("meli_code")}
                            required={required}
                            defaultValue={props.defaultValue}
                            label={label}
                            onChange={(e) => {

                            }}
                            InputLabelProps={{
                                shrink: (ref && ref.current && ref.current.value) ? true : undefined,
                            }}
                            InputProps={{
                                inputComponent: MeliCodeFormat,
                            }}

                        />
                    )
                }}/>
        </React.Fragment>
    )
}


function MeliCodeFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            format="##########" mask=" _"
            style={{
                direction: 'ltr'
            }}
        />
    );
}
