import _ from "lodash";
import TextFieldContainer, {createName, errorList} from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";
import {isMobileNumber} from "../../../utils/Checker";

const phoneErrorList = [(value) => {
    return !isMobileNumber(value.replaceAll(/_/g, "").trimAll())
}];
export default function PhoneTextField({group, name, defaultValue, required, disabled, label = "شماره موبایل ", variant = "outlined", onChange, ...props}) {
    let mobileRef = null;

    return (
        <TextFieldContainer
            defaultValue={_.toString(defaultValue)}
            name={createName({group: group, name: name})}
            type={'text'}
            onChangeDelay={undefined}
            errorPatterns={phoneErrorList}
            returnValue={(value) => {
                return value.trimAll()
            }}
            render={(ref, {name, initialize, valid, errorIndex, errorText, inputProps, setValue, props}) => {
                mobileRef = ref;
                return (
                    <TextField
                        error={!valid}
                        variant={variant}
                        helperText={[...(required ? errorList : []), "شماره موبایل را درست وارد کنید."][errorIndex]}
                        name={name}
                        inputRef={ref}
                        disabled={disabled}
                        fullWidth
                        placeholder={"0900 000 0000"}
                        required={required}
                        defaultValue={props.defaultValue}
                        label={label}
                        onChange={(e) => {

                        }}
                        InputLabelProps={{
                            shrink: (ref && ref.current && ref.current.value) ? true : undefined,
                        }}
                        InputProps={{
                            inputComponent: MobileFormat,
                        }}

                    />
                )
            }}/>
    )
}

function PhoneFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            format="### ########" mask="_"
            style={{
                direction: 'ltr'
            }}
        />
    );
}

function MobileFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            format="#### ### ####" mask=" _"
            style={{
                direction: 'ltr'
            }}
        />
    );
}
