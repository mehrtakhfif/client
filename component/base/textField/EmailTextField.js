import TextField from "./TextField";
import {lang} from "../../../repository";
import TextFieldContainer, {createName} from "./TextFieldContainer";
import React from "react";


export default function EmailTextField({group, name, defaultValue, required, disabled, label = lang.get("meli_code"), variant = "standard", onChange, ...props}) {

    return (
        <TextFieldContainer
            name={createName({group: group, name: name})}
            errorPatterns={['$^|^(([^<>()\\[\\]\\.,;:\\s@\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})$']}
            defaultValue={defaultValue}
            render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                return (
                    <TextField
                        {...props}
                        error={!valid}
                        name={inputName}
                        variant={variant}
                        helperText={[[lang.get("er_verify_email")]][errorIndex]}
                        inputRef={ref}
                        fullWidth
                        label={lang.get('email')}
                        style={{
                            ...style,
                            minWidth: '90%'
                        }}/>
                )
            }}/>
    )
}
