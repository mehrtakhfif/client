import _ from "lodash";
import TextFieldContainer, {createName} from "./TextFieldContainer";
import React, {useState} from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";

const phoneErrorList = [];
export default function PostalCodeTextField({group, name, defaultValue, required, label="کدپستی ",variant="outlined", onChange, ...props}) {
    const [inRef,setInRef] = useState();
    return (
            <TextFieldContainer
                defaultValue={_.toString(defaultValue)}
                name={createName({group: group, name: name})}
                type={'text'}
                onChangeDelay={undefined}
                errorPatterns={phoneErrorList}
                returnValue={(value) => {
                    return value.replaceAll(/-/g,"").trimAll()
                }}
                render={(ref, {name, initialize, valid, errorText, inputProps, setValue, props}) => {
                    setInRef(ref);
                    return (
                        <TextField
                            error={!valid}
                            variant={variant}
                            helperText={errorText}
                            name={name}
                            inputRef={ref}
                            fullWidth
                            placeholder={"00000 - 00000"}
                            required={required}
                            defaultValue={props.defaultValue}
                            label={label}
                            onChange={(e) => {
                            }}
                            InputLabelProps={{
                                shrink: (ref && ref.current && ref.current.value) ? true : undefined,
                            }}
                            InputProps={{
                                inputComponent: PostalCodeFormat,
                            }}

                        />
                    )
                }}/>
    )
}

function PostalCodeFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            format="##### - #####" mask=" "
            style={{
                direction: 'ltr'
            }}
        />
    );
}
