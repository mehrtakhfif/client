import _ from "lodash";
import {createName} from "./TextFieldMultiLanguageContainer";
import {UtilsFormat} from "../../../utils/Utils";
import TextFieldContainer, {errorList} from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";

const priceErrorList = ["$^|^T 0$|^T [1-9][0-9,]*"];
const priceErrorTextList = ["ساختار قیمت اشتباه میباشد"];
export default function PriceTextField({group, array, name, value, required, label, onChange, textFieldProps = {}, ...props}) {
    let inputRef = null;

    return (
        <TextFieldContainer
            defaultValue={_.toString(value)}
            name={createName({group: group, array: array, name: name})}
            type={'number'}
            onChangeDelay={undefined}
            errorPatterns={priceErrorList}
            onChange={(d, el) => {
                try {
                    if (el.value.match("^T 0.+")) {
                        el.setValue(el.value.replace("0", ""));
                        return
                    }
                    onChange(d, el)
                } catch (e) {
                }

            }}
            errorList={[...(required ? errorList : []), ...priceErrorTextList]}
            returnValue={(value) => {
                return UtilsFormat.moneyToNumber(value.replace("T ", "")) || 0;
            }}
            render={(ref, {name, initialize, valid, errorText, inputProps, setValue, props}) => {
                inputRef = ref;
                return (
                    <TextField
                        error={!valid}
                        variant="outlined"
                        helperText={errorText}
                        name={name}
                        inputRef={ref}
                        fullWidth
                        required={required}
                        defaultValue={props.defaultValue}
                        label={label}
                        {...textFieldProps}
                        InputProps={{
                            ...textFieldProps.InputProps,
                            inputComponent: NumberFormat,
                        }}
                    />
                )
            }}/>
    )
}


function NumberFormat(props) {
    const {inputRef, onChange, ...other} = props;

    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            isAllowed={(values) => {
                const {value, floatValue} = values;

                if (typeof floatValue === 'undefined' || typeof value === 'undefined') {
                    return true;
                }
                if (value.match('\\.')) {
                    return false
                }
                return value.charAt(0) !== '-';
            }}
            thousandSeparator
            isNumericString
            prefix="T "
            style={{
                direction: 'ltr'
            }}
        />
    );
}
