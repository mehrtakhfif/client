import React, {useState} from "react";
import TextFieldContainer, {createName, errorList} from "./TextFieldContainer";
import NewTextField from "./TextField";
import {lang} from "../../../repository";
import IconButton from "@material-ui/core/IconButton";
import {VisibilityOffOutlined, VisibilityOutlined} from "@material-ui/icons";


export default function PasswordTextField({group, name, defaultValue, required, label=lang.get("password"),variant="outlined",
                             placeholder=lang.get("password_hint"), onChange, ...props}) {

    const [showPassword, setShowPassword] = useState(false);


    return (
        <TextFieldContainer
            defaultValue={defaultValue}
            name={createName({group: group, name: name})}
            onChangeDelay={undefined}
            errorPatterns={['.{6,}']}
            dir={"ltr"}
            render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                return (
                    <NewTextField
                        {...props}
                        error={!valid}
                        variant={variant}
                        name={inputName}
                        inputRef={ref}
                        type={showPassword ? "text" : "password"}
                        fullWidth
                        placeholder={placeholder}
                        required={required}
                        defaultValue={props.defaultValue}
                        label={label}
                        helperText={[...errorList, lang.get('er_password')][errorIndex]}
                        startAdornment={(
                            <IconButton
                                size={"small"}
                                onFocus={ ()=>{
                                    try {
                                    ref.current.focus()
                                    }catch (e) {
                                    }
                                }}
                                onClick={() => {
                                   setShowPassword(!showPassword)
                                }}>
                                {showPassword ?
                                    <VisibilityOutlined fontSize={"small"}/> :
                                    <VisibilityOffOutlined fontSize={"small"}/>
                                }
                            </IconButton>
                        )}/>
                )
            }}/>

    )
}
