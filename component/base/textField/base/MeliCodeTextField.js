import React from 'react';
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function MeliCodeTextField(props) {
    if (_.isEmpty(props.label)) {
        props = {
            ...props,
            label: lang.get("meli_code")
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        props = {
            ...props,
            errorMessage: lang.get('er_meli_code_structure')
        }
    }

    return (
        <BaseTextField
            {...props}
            dir='ltr'
            errorPattern={(text) => {
                return true
            }}
            label={props.label}/>
    )
}

MeliCodeTextField.defaultProps = {
    label: '',
    placeholder: '12********',
    icon: {},
    type: 'number',
    pattern: '',
    validatable: false,
    maxLength: 10
};

MeliCodeTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
