import React from 'react';
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function ShabaTextField(props) {
    if (_.isEmpty(props.label)) {
        props = {
            ...props,
            label: lang.get("shaba_number")
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        props = {
            ...props,
            errorMessage: lang.get('er_shaba_number')
        }
    }

    return (
        <BaseTextField
            {...props}
            dir='ltr'
            maxLength={26}
            onTextFocusChange={(e) => {
                if (props.onTextFocusChange)
                    props.onTextFocusChange(e)
            }}
            onTextChange={(text) => {
                if (text && !(text.length === 1 && text[0] === 'I') && (text[0] + text[1] !== 'IR')) {
                    text = 'IR' + text
                }
                if (RegExp('^IRI').test(text)){
                    text =text.substr(3);
                    text = "IR"+text
                }
                if (!RegExp('^I?R?[0-9]*$').test(text)) {
                    return
                }
                if (props.onTextChange) {
                    props.onTextChange(text)
                }
            }}
            errorPattern='^IR$|(?:IR)(?=.{24}$)[0-9]*$'
            label={props.label}/>
    )
}

ShabaTextField.defaultProps = {
    label: '',
    placeholder: 'IR5505676***********298001',
    icon: {},
    pattern: '.|^IR.*',
};

ShabaTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
