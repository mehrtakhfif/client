import React from 'react';
import {isPassword} from "../../../../utils/Checker";
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import BaseTextField from "./BaseTextField";

export default function PasswordTextField({
                                              label = lang.get("password"),
                                              errorMessage,
                                              errorPattern,
                                              placeholder = lang.get("password_hint"),
                                              ...props
                                          }) {

    const errorMessageList = [lang.get('er_password')];
    if (errorMessage)
        errorMessageList.push(errorMessage);
    const errorPatternList = [isPassword];
    if (errorPattern)
        errorPatternList.push(errorPattern);
    return (
        <BaseTextField
            {...props}
            dir='ltr'
            errorPattern={errorPatternList}
            label={label}
            type="password"
            errorMessage={errorMessageList}
            placeholder={placeholder}/>
    )
}

PasswordTextField.defaultProps = {
    icon: {},
    validatable: false,
};

PasswordTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    errorPattern: PropTypes.func,
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
