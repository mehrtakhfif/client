import React, {createRef, useEffect} from 'react';
import {createTheme} from "@material-ui/core";
import {checkHasErrorPattern, checkPattern as checkerCheckPattern, isLtr} from "../../../../utils/Checker";
import makeStyles from "@material-ui/core/styles/makeStyles";
import PropTypes from 'prop-types';
import {colors, dir as SiteDir, lang} from "../../../../repository";
import clsx from 'clsx'
import _ from "lodash"
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import ThreeDots from "../../loading/treeDots/ThreeDots";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import {UtilsKeyboardKey} from "../../../../utils/Utils";

const useStyles = makeStyles(theme => (
    {
        myInput: props => ({
            marginTop: props.isOutlineV2 ? theme.spacing(6) : 0,
            "&>label": {
                transformOrigin: props.isLabelRtl ? 'top right !important' : 'top left !important',
                right: props.isLabelRtl ? 0 : 'auto',
                left: props.isLabelRtl ? 'auto' : 0,
                paddingRight: (props.isLabelRtl && (props.isOutline || props.isOutlineV2)) ? 5 : 0,
                paddingLeft: (!props.isLabelRtl && (props.isOutline || props.isOutlineV2)) ? 5 : 0,
                transform: (props.isOutline || props.isOutlineV2) ? "translate(0px, 21px) scale(1)" : "",
                "&[data-shrink=true]": {
                    transform: (props.isOutline) ? "translate(-14px, -0.5px) scale(0.75)" : props.isOutlineV2 ? "translate(0, -22.5px) scale(1)" : "",
                    fontWeight: (props.isOutlineV2) ? "400" : "200"
                },
            },
            "& .Mui-disabled": {
                backgroundColor: 'transparent !important'
            },
            '& div[class*="MuiOutlinedInput-root"]': {
                "& legend": {
                    textAlign: 'right'
                }
            },
            '& div[class*="MuiInput-formControl"]:before': {
                display: props.isStandardWhitOutButtonLine ? 'none' : 'block'
            },
            '& div[class*="MuiInput-formControl"]:after': {
                display: props.isStandardWhitOutButtonLine ? 'none' : 'block'
            }
        }),
        validateStyle: props => ({
            '&[isvalide*="true"]': {
                '&>label': {
                    color: colors.success.main,
                },
                '&>div::before': {
                    borderColor: colors.success.main,
                },
                "& fieldset": {
                    borderColor: (props.isOutline || props.isOutlineV2) ? colors.success.main + " !important" : 'unset'
                },
            },
        }),
    }
));

export default function BaseTextField({
                                          onTextChange,
                                          onTextFocusChange,
                                          onTextChangeFinished,
                                          value,
                                          label,
                                          isLabelRtl,
                                          isRequiredErrorMessage,
                                          placeholder,
                                          isRequired,
                                          hasValid,
                                          validatable,
                                          pattern,
                                          errorMessage,
                                          hasError,
                                          typingWaitInterval = 1000,
                                          variant = "standard",
                                          optionalLabel,
                                          errorPattern,
                                          maxLength,
                                          loading,
                                          icon,
                                          multiline,
                                          rows,
                                          type,
                                          inputName,
                                          disabled,
                                          dir,
                                          style,
                                          className,
                                          ...props
                                      }) {
    const isStandard = variant === "standard" || variant === 'standardWhitOutButtonLine';
    const isOutline = variant === "outline";
    const isOutlineV2 = variant === "outlineV2";
    useEffect(() => {
        if (isOutline || isOutlineV2)
            setLabelWidth(labelRef.current.offsetWidth + (labelRef.current.offsetWidth * 0.35));
    }, []);

    const inputRef = createRef();
    const [labelWidth, setLabelWidth] = React.useState(0);
    const [initialize, setInitialize] = React.useState(false);
    const labelRef = React.useRef(null);
    React.useEffect(() => {
        if (!initialize) {
            setInitialize(true);
            return
        }
        // onFocusChangeHandler(value, false);
        onChangeHandler(value, false);
    }, [value]);

    //region Props
    if (isLabelRtl === undefined) {
        // props = {
        //     ...props,
        // }
        isLabelRtl = SiteDir === "rtl"
    }
    if (isRequiredErrorMessage === undefined) {
        // props = {
        //     ...props,
        // }
        isRequiredErrorMessage = lang.get("er_fill_input")
    }
    //endregion Props

    //region Variable

    let theme = createTheme();
    const classes = useStyles({
        isLabelRtl: isLabelRtl,
        isStandard: isStandard,
        isStandardWhitOutButtonLine: variant === 'standardWhitOutButtonLine',
        isOutline: isOutline,
        isOutlineV2: isOutlineV2,
        ...props
    });

    const [state, setState] = React.useState({
        inputDir: getDefaultDir(),
        hasError: null,
        hasErrorAttr: (_.isEmpty(value) && isRequired),
        hasValid: null,
        timer: null
    });
    useEffect(() => {
    }, [state, value]);

    function getDefaultDir() {
        if (!_.isEmpty(value)) {
            return isLtr(value) ? "ltr" : "rtl";
        }
        if (!_.isEmpty(placeholder)) {
            return isLtr(placeholder) ? "ltr" : "rtl";
        }
        return SiteDir;
    }

    //endregion Variable

    //region handler
    const onChange = event => {
        const text = event.target.value;
        //region patternCheck

        if (pattern && !_.isEmpty(text) && !checkPattern(text))
            return;

        //endregion patternCheck
        let newState = {
            ...state,
            hasError: null,
            hasValid: null
        };
        //region maxLengthCheck
        if (maxLength) {
            if (event.target.value.length > event.target.maxLength) {
                event.target.value = event.target.value.slice(0, event.target.maxLength);
                return
            }
        }
        //endregion maxLengthCheck
        //region LtrOrRtl
        const ltr = (!_.isEmpty(text)) ? isLtr(text) : (!_.isEmpty(placeholder) ? isLtr(placeholder) : SiteDir === 'ltr');
        newState = {
            ...newState,
            inputDir: ltr ? "ltr" : 'rtl'
        };
        //endregion LtrOrRtl
        //region CheckError Only For Attr
        newState = {
            ...newState,
            hasErrorAttr: checkError(errorPattern, text)
        };
        //endregion CheckError Only For Attr
        if (onTextChange !== undefined)
            onTextChange(text);
        if (onTextChangeFinished !== undefined) {
            clearTimeout(state.timer);
            // requestNewListDebounce(value);
            newState = {
                ...newState,
                timer: setTimeout(() => onTextChangeFinished(value), typingWaitInterval)
            }
        }
        setState(newState);
    };

    function onChangeHandler(text, callParent = true) {
        //region patternCheck
        if (pattern && !_.isEmpty(text) && !checkPattern(text))
            return;
        //endregion patternCheck
        let newState = {
            ...state,
            hasError: null,
            hasValid: null
        };
        //region maxLengthCheck
        if (maxLength) {
            if (text.length > maxLength) {
                text = text.slice(0, maxLength);
                return
            }
        }
        //endregion maxLengthCheck
        //region LtrOrRtl
        const ltr = (!_.isEmpty(text)) ? isLtr(text) : (!_.isEmpty(placeholder) ? isLtr(placeholder) : SiteDir === 'ltr');
        newState = {
            ...newState,
            inputDir: ltr ? "ltr" : 'rtl'
        };
        //endregion LtrOrRtl
        //region CheckError Only For Attr
        newState = {
            ...newState,
            hasErrorAttr: checkError(errorPattern, text)
        };
        //endregion CheckError Only For Attr
        setState(newState);
        if (callParent && undefined !== onTextChange)
            onTextChange(text);
    }

    const onFocusChange = event => {
        try {
            const text = event.target.value;
            let newState = {...state};
            //region ErrorCheck
            const checkErrorCo = checkError(errorPattern, text);
            newState = {
                ...newState,
                hasError: checkErrorCo,
                hasValid: !checkErrorCo
            };
            //endregion ErrorCheck
            setState(newState);
            if (onTextFocusChange !== undefined)
                onTextFocusChange(event.target.value)
        } catch (e) {
        }
    };

    function handleKeyDown(e) {
        if (onTextChangeFinished && e.keyCode === UtilsKeyboardKey.ENTER_KEY) {
            clearTimeout(state.timer);
            onTextChangeFinished(value)
        }
    }

    function onFocusChangeHandler(text, callParent = true) {
        try {
            let newState = {...state};
            //region ErrorCheck
            const checkErrorCo = checkError(errorPattern, text);
            newState = {
                ...newState,
                hasError: checkErrorCo,
                hasValid: !checkErrorCo
            };
            //endregion ErrorCheck
            setState(newState);
            if (callParent && onTextFocusChange !== undefined)
                onTextFocusChange(text)
        } catch (e) {
        }
    }

    //region handlerHelper

    /**
     *  Check Error
     *
     * pattern can regex or function or list of regex and functions
     * text for check pattern
     *
     *
     *
     * NOTE: Text Pattern first check pattern has available then check if pattern is regex check or pattern has function run function
     *
     */
    function checkError(pattern, text) {
        if (_.isEmpty(text)) {
            if (isRequired)
                return -1;
            return state.hasError
        }
        if (hasError === undefined && pattern) {
            const check = checkHasErrorPattern(pattern, text);
            return check !== -1 ? check : null
        }
        return state.hasError
    }


    /**
     *  Check Pattern
     *
     * text for check pattern
     *
     *
     * NOTE: Text Pattern first check pattern has available then check if pattern is regex check or pattern has function run function
     *
     */
    function checkPattern(text) {
        try {
            return pattern && checkerCheckPattern(pattern, text)
        } catch (e) {
        }
        return true
    }

    //endregion handlerHelper

    //endregion handler

    //region ClassName
    const classesName = [classes.myInput];
    if (className) {
        classesName.push(className)
    }
    if (hasValid || (!_.isEmpty(value) && validatable && !hasErrorFun())) {
        classesName.push(classes.validateStyle)
    }
    let elemntProp = {};
    if (value !== undefined) {
        elemntProp = {
            ...elemntProp,
            value: value,
        }
    }
    if (maxLength) {
        elemntProp = {
            ...elemntProp,
            inputProps: {
                ...elemntProp.inputProps,
                maxLength: maxLength,
            }
        }
    }
    //endregion ClassName

    //region Functions
    function hasErrorFun() {
        return !(disabled || loading) && (hasError || (hasError === undefined && state.hasError !== null))
    }

    function hasValidFun() {


        return (disabled || loading) && (hasValid || (hasValid === undefined && state.hasValid !== null))
    }

    function getErrorMessage() {
        if (state.hasError !== -1) {
            if (_.isString(errorMessage)) {
                return errorMessage
            }
            if (_.isArray(errorMessage)) {
                return errorMessage[state.hasError]
            }
        }
        return isRequiredErrorMessage
    }

    function getInputDir() {
        if (state.inputDir === "ltr" || type === "number" || type === "password" || type === "email") {
            return 'ltr'
        }
        return "rtl"
    }

    //endregion Functions

    return (
        <FormControl className={clsx(classesName, classes.root)}
                     error={hasErrorFun()}
                     isvalide={state.hasValid ? 'true' : 'false'}
                     {...((state.hasErrorAttr) ? {haserror: ""} : null)}
                     disabled={disabled || loading}
                     variant={isStandard ? "standard" : "outlined"}
                     style={{
                         ...style,
                         paddingTop: 5
                     }}>
            {
                <InputLabel ref={labelRef}
                            htmlFor={isStandard ? "component-standard" : "component-outlined"}>{label}{(optionalLabel !== "" && !isRequired) &&
                <span style={{
                    fontSize: '0.7rem',
                    top: -4
                }}>
                {(optionalLabel) ? optionalLabel : ' (' + lang.get('optional') + ')'}</span>}
                </InputLabel>
            }
            {
                isStandard ? (
                    <Input
                        name={inputName ? inputName : label}
                        {...elemntProp}
                        dir={getInputDir()}
                        onChange={onChange}
                        onKeyDown={handleKeyDown}
                        type={type}
                        value={value}
                        variant="outlined"
                        inputRef={inputRef}
                        onBlur={onFocusChange}
                        placeholder={placeholder}
                        multiline={multiline}
                        rows={rows}
                        startAdornment={
                            loading ? (
                                <InputAdornment position="start">
                                    <ThreeDots/>
                                </InputAdornment>
                            ) : null
                        }
                        error={hasErrorFun()}/>
                ) : (isOutline || isOutlineV2) ? (
                    <OutlinedInput
                        name={inputName ? inputName : label}
                        {...elemntProp}
                        dir={getInputDir()}
                        onChange={onChange}
                        onKeyDown={handleKeyDown}
                        type={type}
                        value={value}
                        variant="outlined"
                        inputRef={inputRef}
                        onBlur={onFocusChange}
                        placeholder={placeholder}
                        multiline={multiline}
                        labelWidth={isOutline ? labelWidth : 0}
                        rows={rows}
                        startAdornment={
                            loading ? (
                                <InputAdornment position="start">
                                    <ThreeDots/>
                                </InputAdornment>
                            ) : null
                        }
                        error={hasErrorFun()}/>
                ) : null
            }
            {
                (hasErrorFun()) &&
                <FormHelperText>{getErrorMessage()}</FormHelperText>
            }
        </FormControl>
    )
}

BaseTextField.defaultProps = {
    label: '',
    icon: {},
    type: 'text',
};

BaseTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    pattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    optionalLabel: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    errorPattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    multiline: PropTypes.bool,
    rows: PropTypes.number,
    type: PropTypes.oneOf(['text', 'password', 'number', 'date', 'email', 'file', 'search']),
    variant: PropTypes.oneOf(['standard', 'standardWhitOutButtonLine', 'outline', 'outlineV2']),
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
