import React from 'react';
import {isFirstAndLastName} from "../../../../utils/Checker";
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function NameTextField(props) {
    let newProps = props;
    if (_.isEmpty(props.label)) {
        newProps = {
            ...newProps,
            label: lang.get('full_name_transferee')
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        newProps = {
            ...newProps,
            errorMessage: lang.get('er_name_structure')
        }
    }

    return (
        <BaseTextField
            {...newProps}
            pattern={'^([A-zآ-ی]+[ ]?)*$'}
            errorPattern={(text) => isFirstAndLastName(text)}
        />
    )
}

NameTextField.defaultProps = {
    label: '',
    placeholder: 'احمد پورمخبر',
    icon: {},
    type: 'text',
    pattern: '^([A-zآ-ی]+[ ]?)*$',
    validatable: false,
};

NameTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
