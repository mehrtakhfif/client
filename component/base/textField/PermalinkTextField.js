import React from "react";
import SyncableTextField from "./SyncableTextField";
import {lang} from "../../../repository";
import TextFieldContainer from "./TextFieldContainer";


export default function permalinkTextField({syncedRef,name,defaultValue,inputProps={}}) {
    let inputRef = null;

    return(
        <TextFieldContainer
            name={name}
            actived={true}
            errorPatterns={['^[A-Za-z\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC][A-Za-z0-9-_ \u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC]*$']}
            onChangeDelay={500}
            onChange={(value)=>{
                inputRef.current.setValue(generatePermalink(value))
            }}
            render={(ref, {name, initialize, valid, errorIndex, setValue}) => {
                inputRef = ref;
                return(
                    <SyncableTextField
                        name={name}
                        variant='outlined'
                        fullWidth
                        syncedRef={syncedRef}
                        inputRef={ref}
                        required
                        label={lang.get('permalink')}
                        renderSynced={(text)=>{
                            return generatePermalink(text)
                        }}
                        defaultValue={defaultValue}
                        {...inputProps}/>
                )
            }}
        />
    )
}


function generatePermalink(text) {
    text = text.replaceAll(/[ _]+/gm,'-').toLowerCase();
    text = text.replaceAll(/[^-0-9a-zA-zاآبپتثجچحخدذرزژسشصضطظعغفکگلمنوهی]|[\/\\'\^_`]/gm,"");
    return text.replaceAll(/[ _]+/gm,'-').toLowerCase()
}
