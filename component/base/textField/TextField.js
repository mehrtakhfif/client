import React, {useEffect, useLayoutEffect} from "react";
import {MuiThemeProvider, TextField as MTextField, useTheme} from "@material-ui/core";
import _ from 'lodash'
import {InfoOutlined} from "@material-ui/icons";
import Typography from "../Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import Box from "@material-ui/core/Box";
import {grey} from "@material-ui/core/colors";
import {ThemeProvider} from 'styled-components'
import createPalette from "@material-ui/core/styles/createPalette";
import {notValidErrorTextField} from "./TextFieldContainer";


export default function TextField({
                             inputRef,
                             helperText,
                             helperTextIcon,
                             defaultValue,
                             requestFocus,
                             disabled,
                             startAction,
                             startAdornment,
                             endAction,
                             endAdornment,
                             containerProps,
                             ...props
                         }) {
    const theme = useTheme();


    useEffect(() => {
        try {
            if (props.error) {
                inputRef.current.setAttribute(notValidErrorTextField, helperText)
            } else {
                inputRef.current.removeAttribute(notValidErrorTextField)
            }
        } catch (e) {

        }
    }, [props.error])

    useLayoutEffect(() => {
        try {
            if (requestFocus && inputRef)
                inputRef.current.focus()
        } catch (e) {
        }
    }, []);

    useEffect(() => {
        try {
            inputRef.current.setValue(_.trim(defaultValue))
        } catch (e) {
            try {
                inputRef.current.value = defaultValue
            } catch (e) {
            }
        }
    }, [defaultValue]);


    return (
        <MuiThemeProvider theme={{
            ...theme,
            palette: createPalette({
                primary: {
                    light: grey[300],
                    main: grey[500],
                    dark: grey[700]
                },
                secondary: {
                    light: grey[500],
                    main: grey["A700"],
                    dark: grey[900]
                }
            })
        }}>
            <ThemeProvider theme={{
                ...theme,
                palette: createPalette({
                    primary: {
                        light: grey[300],
                        main: grey[500],
                        dark: grey[700]
                    },
                    secondary: {
                        light: grey[500],
                        main: grey["A700"],
                        dark: grey[900]
                    }
                })
            }}>
                <Box display={'flex'} width={props.fullWidth ? 1 : null} alignItems="flex-end" {...containerProps}>
                    <Box flex={props.fullWidth ? 1 : null}>
                        <MTextField
                            inputRef={inputRef}
                            defaultValue={defaultValue}
                            disabled={disabled}
                            {...props}
                            helperText={(
                                helperText &&
                                <Typography component={'span'} variant={'caption'}
                                            color={props.error ? theme.palette.error.main : null}
                                            display={'flex'} alignItems={'center'}>
                                    {
                                        !helperTextIcon ? null :
                                            helperTextIcon === true ?
                                                <InfoOutlined fontSize={"small"}
                                                              style={{
                                                                  fontSize: 17,
                                                                  color: !props.error ? theme.palette.text.secondary : theme.palette.error.main,
                                                                  marginLeft: theme.spacing(0.5)
                                                              }}/>
                                                : helperTextIcon}
                                    {helperText}
                                </Typography>
                            )}
                            InputProps={{
                                ...props.InputProps,
                                startAdornment: startAdornment && (
                                    <InputAdornment position="start">
                                        {startAdornment}
                                    </InputAdornment>
                                ),
                                endAdornment: endAdornment && (
                                    <InputAdornment position="end">
                                        {endAdornment}
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </Box>
                </Box>
            </ThemeProvider>
        </MuiThemeProvider>
    )

}
