import React, {useState} from 'react';
import BaseTextField from "./BaseTextField";


export default function TextField({
                                      onTextChange = null,
                                      onTextFocusChange = null,
                                      defaultText = "",
                                      title = "",
                                      hint = "",
                                      errorMessage = null,
                                      type = "text",
                                      dir = null,
                                      hasError = false,
                                      inputName = null,
                                      disabled = false,
                                      style = {}
                                  }) {
    const [text, setText] = useState(defaultText);

    const onChange = text => {
        setText(text);
        if (null !== onTextChange)
            onTextChange(text);
    };

    const onFocusChange = text => {
        if (null !== onTextFocusChange)
            onTextFocusChange(text)
    };

    return (
        <BaseTextField text={text}
                       onTextChange={onChange}
                       onTextFocusChange={onFocusChange}
                       hasError={hasError}
                       title={title}
                       type={type}
                       errorMessage={errorMessage}
                       dir={dir}
                       inputName={inputName}
                       disabled={disabled}
                       hint={hint}
                       style={style}/>
    )
}
