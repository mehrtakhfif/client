import _ from "lodash";
import TextFieldContainer, {createName, errorList} from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";
import {lang} from "../../../repository";

const shabaErrorList = [(value) => {
    const l = value.replaceAll(/[^IR0-9]*/g, "").length;
    return l !== 26 && l !== 0;
}];
export default function ShabaTextField({group, name, defaultValue, variant = "outlined", required, label = "شماره شبا", isMobile: isM, onChange, ...props}) {
    let mobileRef = null;

    return (
        <TextFieldContainer
            defaultValue={defaultValue ? _.split(defaultValue, "IR")[1] : ''}
            name={createName({group: group, name: name})}
            type={'text'}
            onChangeDelay={undefined}
            errorPatterns={shabaErrorList}
            returnValue={(value) => {
                return value.replaceAll(/[^IR0-9]*/g, "")
            }}
            render={(ref, {name, initialize, valid, errorIndex, inputProps, setValue, props}) => {
                mobileRef = ref;
                return (
                    <TextField
                        {...props}
                        error={!valid}
                        variant={variant}
                        name={name}
                        inputRef={ref}
                        fullWidth
                        helperText={[...(required ? errorList : []), lang.get('er_shaba_number')][errorIndex]}
                        required={required}
                        label={label}
                        placeholder={'IR 5505676***** **** **2 98001'}
                        InputLabelProps={{
                            shrink: (ref && ref.current && ref.current.value) ? true : undefined,
                        }}
                        InputProps={{
                            inputComponent: MobileFormat,
                        }}

                    />
                )
            }}/>
    )
}

function MobileFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            prefix="IR "
            format="IR ############ #### ### #####" mask=" _"
            style={{
                direction: 'ltr'
            }}
        />
    );
}
