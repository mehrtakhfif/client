import React from "react";
import TextField from "./TextField";
import TextFieldContainer, {errorList} from "./TextFieldContainer";


export default function DefualtTextField({name, defaultValue,placeholder,variant,label,required,multiline,rows,containerProps = {}, textFieldProps = {},...props}) {

    return (
        <TextFieldContainer
            name={name}
            defaultValue={defaultValue}
            render={(ref, {errorIndex,props}) => (
                <TextField
                    {...props}
                    variant={variant}
                    inputRef={ref}
                    name={name}
                    required={required}
                    fullWidth={true}
                    multiline={multiline}
                    rows={rows}
                    placeholder={placeholder}
                    error={errorList[errorIndex]}
                    label={label}
                    {...textFieldProps}
                    style={{
                        ...textFieldProps.style
                    }}/>
            )}
            {...containerProps}
        />
    )
}
