import React, {Fragment, useEffect, useLayoutEffect, useState} from "react";
import TextField from "./TextField";
import Tooltip from "../Tooltip";
import {Sync, SyncDisabled} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import {useSnackbar} from "notistack";
import {lang} from "../../../repository";
import Button from "@material-ui/core/Button";


export default function SyncableTextField({
                             inputRef,
                             syncedRef,
                             defaultValue,
                             renderSynced,
                             syncedLabel = 'غیرفعالسازی',
                             syncedIcon = <SyncDisabled/>,
                             unSyncedLabel = 'فعال‌سازی',
                             unSyncedIcon = <Sync/>,
                             ...props
                         }) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [sync, setSync] = useState(false);
    const [value, setValue] = useState(defaultValue);
    const [lastValue, setLastValue] = useState(defaultValue);

    useEffect(() => {
        setValue(defaultValue);
    }, [defaultValue]);


    useLayoutEffect(() => {
        try {
            setSync(syncedRef.current.value === value);
        } catch (e) {
        }
    }, [syncedRef]);


    useEffect(() => {
        try {
            if (sync) {
                syncedRef.current.addEventListener("keyup", handleKeyUp);
                handleKeyUp();
            } else {
                syncedRef.current.removeEventListener("keyup", handleKeyUp);
            }
        } catch (e) {
            if (sync) {
                setSync(!sync);
                enqueueSnackbar(`لطفا دوباره تلاش کنید.`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            }
        }

        return () => {
            try {
                syncedRef.current.removeEventListener("keyup", handleKeyUp);
            } catch (e) {
            }
        }
    }, [sync]);

    function handleSyncClick() {
        if (!sync)
            setLastValue(inputRef.current.value);
        else
            setValue(lastValue);
        setSync(!sync)
    }


    function handleKeyUp() {
        try {
            setValue(renderSynced ? renderSynced(syncedRef.current.value) : syncedRef.current.value)
        } catch (e) {
        }
    }

    return (
        <TextField
            inputRef={inputRef}
            defaultValue={value}
            {...props}
            disabled={sync}
            InputLabelProps={{
                shrink: sync === true ? true : undefined,
            }}
            endAdornment={(
                <Tooltip title={sync ? syncedLabel : unSyncedLabel}>
                    <IconButton onClick={handleSyncClick}>
                        {sync ?
                            syncedIcon :
                            unSyncedIcon
                        }
                    </IconButton>
                </Tooltip>
            )}/>
    )
}
