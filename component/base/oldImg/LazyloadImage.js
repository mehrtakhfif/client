import * as React from "react";
import {useEffect} from "react";
import {isClient} from "../../../repository";


// Only initialize it one time for the entire application



function LazyImage({alt, src, placeholderSrc, srcset, sizes, width='100%', height,...props}) {

    useEffect(() => {
        try {
        if (isClient())
            document.lazyLoadInstance.update();
        }catch {

        }
    });

    return (
        <img
            alt={alt}
            className="lazy"
            src={placeholderSrc ? placeholderSrc : src}
            data-src={src}
            data-srcset={srcset}
            data-sizes={sizes}
            width={width}
            height={height}
            {...props}
        />
    )
}

export default LazyImage;
