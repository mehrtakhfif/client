import React from "react";
import PropTypes from "prop-types";
import Box from "@material-ui/core/Box";
import LazyImage from "./LazyloadImage";
import _ from "lodash";
import Skeleton from "@material-ui/lab/Skeleton";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import {Cancel} from "@material-ui/icons";
import {useTheme} from "@material-ui/core";
import {UtilsStyle} from "../../../utils/Utils";

function Image({
                   src,
                   alt,
                   width = '100%',
                   height = 'auto',
                   placeholder = true,
                   placeholderSrc,
                   minWidth,
                   minHeight,
                   maxWidth,
                   maxHeight,
                   open = false,
                   showSkeleton = false,
                   onClose,
                   imgStyle,
                   style,
                   ...props
               }) {
    const theme = useTheme();
    const sr = _.isObject(src) ? src.image : src;
    let al = (alt ? alt : (_.isObject(src) && src.title) ? src : '');
    al = al ? "مهرتخفیف | " + al : al

    try {
        if (placeholder && !placeholderSrc && src.match('(http|https).*\\-has\\-ph\\.(jpg|png)')) {
            placeholderSrc = src.replace("-has-ph", "-ph");
            showSkeleton = false;
        }
    } catch (e) {
    }

    return (
        <React.Fragment>
            <Box
                width={1}
                display={'flex'}
                style={{
                    width: width,
                    height: height,
                    minWidth: minWidth,
                    minHeight: minHeight ? minHeight : 10,
                    maxWidth: maxWidth,
                    maxHeight: maxHeight,
                    position: 'relative',
                    overflow: 'hidden',
                    ...style
                }} {...props}>
                {showSkeleton &&
                <Skeleton variant="rect"
                          style={{
                              position: 'absolute',
                              height: '100%',
                              width: '100%',
                              top: 0,
                              bottom: 0,
                              right: 0,
                              left: 0
                          }}/>
                }
                <LazyImage
                    src={sr}
                    placeholder={_.isBoolean(placeholder) ? undefined : placeholder}
                    placeholderSrc={placeholderSrc}
                    alt={al}
                    {...props}
                    style={{
                        width: width,
                        height: height,
                        maxWidth: maxWidth,
                        maxHeight: maxHeight,
                        zIndex: 2,
                        ...imgStyle
                    }}
                />
                {(open && onClose) &&
                <Dialog fullWidth={true} maxWidth={'md'} open={open} onClose={onClose}>
                    <Box p={2} style={{position: 'relative'}}>
                        <IconButton onClick={onClose} style={{
                            zIndex: 99999,
                            backgroundColor: theme.palette.background.default,
                            position: 'absolute', top: 5, left: 5
                        }}>
                            <Cancel/>
                        </IconButton>
                        <Image src={src} style={{...UtilsStyle.borderRadius(5)}}/>
                    </Box>
                </Dialog>}
            </Box>
        </React.Fragment>
    )
}

export default Image;

Image.prototype = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    onLoad: PropTypes.func,
    onError: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    imgStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

