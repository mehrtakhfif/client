import React, {useMemo} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import clsx from "clsx";
import {
    AppBar,
    Box,
    Dialog,
    getSafe,
    HiddenLgUp,
    HiddenMdDown,
    IconButton,
    tryIt,
    useEffectWithoutInit,
    useInit,
    useLimit,
    useOpenWithBrowserHistory,
    useState,
    useWindowSize
} from "material-ui-helper";
import {colors, lang} from "../../repository";
import Typography from "./Typography";
import {Collapse, CssBaseline, Toolbar, useTheme} from "@material-ui/core";
import MtIcon from "../MtIcon";

const duration = 1000;

const useShowMoreStyles = makeStyles((theme) => ({
    showMoreRoot: {
        position: 'relative',
        "&.active::after": {
            content: "''",
            width: "100%",
            height: "100%",
            background: "linear-gradient(360deg, rgb(255 255 255) 0%, rgba(255,255,255,1) 28%, rgba(255,255,255,0) 100%)",
            position: "absolute",
            bottom: 0,
        }
    },
    showMoreShadow: {
        width: "100%",
        height: 145,
        "&::after": {
            content: "''",
            width: "100%",
            height: "100%",
            background: "linear-gradient(360deg, rgb(255 255 255) 0%, rgba(255,255,255,1) 36%, rgba(255,255,255,0) 100%)",
            position: "absolute",
            bottom: 0,
        }
    },
    showMoreButton: {
        position: 'absolute',
        bottom: 0,
        zIndex: theme.zIndex.appBar - 11,
        justifyContent: "flex-start",
        width: '100%',
        [theme.breakpoints.down('md')]: {
            bottom: 20,
        },
    }
}));


export default function ShowMore({

                                     className,
                                     name,
                                     acceptableLine,
                                     acceptableHeight,
                                     watcher,
                                     children,
                                     ...props
                                 }) {
    const init = useInit()
    const theme = useTheme()
    const [width] = useWindowSize()

    const [ref, show, setShow, {
        maxHeight,
        canHide,
        lineHeight
    }] = (acceptableLine || acceptableHeight) ? useLimit(acceptableLine || acceptableHeight, {
        isTextLine: Boolean(acceptableLine),
        defaultShow: false,
        watcher: watcher
    }) : [undefined, undefined, undefined, {}];

    const [openLg, setOpenLg] = useState(show)
    const [openSm, setOpenSm] = useOpenWithBrowserHistory(`show-more-component-${name}`)

    const collapsedHeight = useMemo(() => {
        return getSafe(() => {
            const height = acceptableHeight || ((acceptableLine * lineHeight) + 10);
            if (height > maxHeight) {
                return maxHeight
            }
            return height
        })
    }, [acceptableHeight, acceptableLine, lineHeight])

    useEffectWithoutInit(() => {
        const func = () => {
            const isMd = theme.breakpoints.width('md') >= width;
            const isSm = theme.breakpoints.width('sm') >= width;
            const isXs = theme.breakpoints.width('xs') >= width;
            const checkSize = isSm || isXs

            if (!checkSize || !show) {
                if (show !== openLg)
                    setOpenLg(show)
            }
            if (checkSize) {
                if (show !== openSm)
                    setOpenSm(show)
            }
        }
        if (!init()) {
            setTimeout(func, 500)
            return
        }
        func()
    }, [show, width])

    useEffectWithoutInit(() => {
        tryIt(() => {
            if (!openSm)
                setShow(false)
        })
    }, [openSm])

    return (
        <ShowMoreBase
            innerRef={ref}
            className={className}
            name={name}
            show={show}
            canHide={canHide}
            maxHeight={maxHeight}
            onShowMore={setShow}
            {...props}>
            {
                maxHeight ?
                    <React.Fragment>
                        <Collapse
                            timeout={duration}
                            in={openLg}
                            collapsedHeight={(maxHeight && maxHeight < collapsedHeight) ? maxHeight + 10 : collapsedHeight}>
                            {children}
                        </Collapse>
                    </React.Fragment> :
                    children
            }
            <Dialog
                open={openSm}
                fullScreen={true}
                fullWidth={true}>
                <CssBaseline/>
                <AppBar
                    appBarProps={{
                        elevation: 0
                    }}>
                    <IconButton
                        onClick={(e) => {
                            e.stopPropagation()
                            setOpenSm(false)
                        }}>
                        <MtIcon icon={"mt-arrow-right"}/>
                    </IconButton>
                </AppBar>
                <Toolbar/>
                <Box px={1} width={1} flexDirectionColumn={true}>
                    {children}
                </Box>
            </Dialog>
        </ShowMoreBase>
    )
}


export function ShowMoreBase(
    {
        innerRef,
        className,
        name,
        show,
        canHide,
        maxHeight,
        onShowMore,
        children,
        ...
            props
    }
) {
    const classes = useShowMoreStyles()

    function handleButtonClick() {
        onShowMore(!show)
    }



    return (
        <Box
            ref={innerRef}
            flexDirectionColumn={true}
            className={clsx([className, classes.showMoreRoot])}
            position={"relative"} {...props}>
            {children}
            {
                (maxHeight && canHide) ?
                    <React.Fragment>
                        <Box
                            pt={{
                                xs: 0,
                                md: 2
                            }}
                            className={!show ? classes.showMoreButton : undefined}
                            style={{
                                backgroundColor: "rgba(255,255,255,0.49)",
                                cursor: "pointer"
                            }}>
                            <Box pr={2} alignCenter={true} onClick={handleButtonClick}>
                                <Typography py={1} pl={1} variant={"body1"} color={colors.textColor.h404040}>
                                    {show ? lang.get("see_less") : name ? lang.get("see_more_x", {
                                        args: {
                                            title: name,
                                        }
                                    }) : lang.get("see_more")}
                                </Typography>
                                <HiddenLgUp>
                                    <MtIcon fontSize={"small"} icon={"mt-chevron-left"}/>
                                </HiddenLgUp>
                                <HiddenMdDown>
                                    <MtIcon fontSize={"small"} icon={show ? "mt-chevron-up" : "mt-chevron-down"}/>
                                </HiddenMdDown>
                            </Box>
                        </Box>
                        <Box style={{
                            position: "absolute",
                            bottom: 0,
                            left: 0,
                            right: 0
                        }}>
                            <Collapse timeout={(duration / 2) + 100} in={!show}>
                                <Box className={classes.showMoreShadow}/>
                            </Collapse>
                        </Box>
                    </React.Fragment> :
                    <React.Fragment/>
            }
        </Box>
    )
}

