import React from "react";
import {Box, Typography} from "material-ui-helper";
import {ArrowForward} from "@material-ui/icons";
import {colors, lang} from "../../repository";
import {useTheme} from "@material-ui/core";

export const BackEl = ({onBackClick})=>{
    const theme = useTheme()
    return(

        <Box
            onClick={onBackClick}
            textSelectable={false}
            center={true}
            style={{
                cursor: "pointer",
                position: 'absolute',
                top: theme.spacing(4),
                right: theme.spacing(4)
            }}>
            <ArrowForward style={{
                color: colors.textColor.h757575
            }}/>
            <Typography px={1} color={colors.textColor.h757575} variant={"caption"}>
                {lang.get("return")}
            </Typography>
        </Box>
    )
}
