import React, {useCallback, useMemo, useRef, useState} from "react";
import {Box, Button, DefaultTextField, FormController, IconButton, tryIt, Typography} from "material-ui-helper";
import {colors, lang} from "../../repository";
import {useTheme} from "@material-ui/core";
import {MobileFormatCenter} from "../base/TextFieldPatterns";
import {Close} from "@material-ui/icons";
import _ from "lodash"
import {isMobileNumber} from "../../utils/Checker";
import ControllerUser from "../../controller/ControllerUser";
import rout from "../../router";
import NLink from "../base/link/NLink";
import {BackEl} from "./helper";


export default function FirstStep({onSubmitted, ...props}) {
    const theme = useTheme();
    const ref = useRef()
    const [username, setUsername] = useState("")
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)
    const formHasError = useMemo(() => error || username.length !== 11, [error, username]);
    const hasErrorInput = useMemo(() => !_.isEmpty(username) && error, [username, error])


    const handleSubmitClick = useCallback(() => {
        if (formHasError || loading) {
            return
        }
        setLoading(true)
        ControllerUser.Auth.firstStep({username: username}).then(res => {
            tryIt(() => onSubmitted({
                status: res.data.status,
                username: username
            }))
        }).finally(() => {
            setLoading(false)
        })
    }, [username, loading, formHasError])


    const handleUsernameChange = useCallback((e, v) => {
        const val = v.replace(/ /g, '');
        setUsername(val)
        if (val.length === 11) {
            setError(!isMobileNumber(val))
            return
        }
        if (error)
            setError(false)
    }, [error])

    const handleFocusOut = useCallback(() => {
        checkError()
    }, [username])

    const checkError = useCallback(() => {
        setError(_.isEmpty(username) ? false : !isMobileNumber(username));
    }, [username])


    return (
        <Box
            {...props}>
            <BackEl
                onBackClick={onSubmitted}/>
            <Typography
                color={colors.textColor.h000000}
                variant={'h3'}
                pb={[4.6]}
                component={"h3"}>
                {lang.get("signup_or_login")}
            </Typography>
            <Typography
                color={colors.textColor.h757575}
                variant={'subtitle2'}
                pb={[2, 4]}
                component={"p"}>
                {lang.get("add_phone_number")}
            </Typography>
            <FormController
                innerRef={ref}
                onSubmit={handleSubmitClick}
                minWidth={"100%"}>
                <Box width={1} flexDirectionColumn={true}>
                    <DefaultTextField
                        value={username}
                        name={"username"}
                        variant={"outlined"}
                        disabled={Boolean(loading)}
                        type={"tel"}
                        autoFocus={true}
                        placeholder={lang.get("phone_number")}
                        error={Boolean(hasErrorInput)}
                        onChangeTextField={handleUsernameChange}
                        onFocusOut={handleFocusOut}
                        onFocusIn={() => {
                            setError(false)
                        }}
                        autoComplete={"tel"}
                        inputStyle={{
                            textAlign: 'center'
                        }}
                        startAdornment={(
                            !_.isEmpty(username) ?
                                <IconButton
                                    disabled={Boolean(loading)}
                                    title={lang.get("clear")}
                                    onClick={() => {
                                        setUsername("")
                                        tryIt(() => {
                                            document.getElementsByName("username")[0].focus()
                                        })
                                    }}
                                    style={{
                                        position: 'absolute'
                                    }}>
                                    <Close/>
                                </IconButton> :
                                <React.Fragment/>
                        )}
                        InputProps={{
                            inputComponent: MobileFormatCenter,
                        }}
                    />
                    {
                        hasErrorInput ?
                        <Typography pt={1} variant={"caption"}
                                    color={theme.palette.error.main}>
                            {lang.get("invalid_phone_number")}
                        </Typography>:<React.Fragment/>
                    }
                </Box>
            </FormController>
            <Button
                pt={[2, 5]}
                id={"login_first_step_submit_button"}
                fullWidth={true}
                color={theme.palette.primary.main}
                loading={loading}
                disableElevation={true}
                onClick={handleSubmitClick}
                disabled={formHasError}
                typography={{
                    py: "8px",
                    variant: "body1",
                    color: "#fff"
                }}>
                {lang.get("continue")}
            </Button>
            <Typography
                variant={"caption"}
                pt={[3, 4]}
                display={"block"}
                color={colors.textColor.h757575}>
                با ورود و یا ثبت نام در سایت مهرتخفیف شما
                <Box
                    mx={1}
                    display={"inline-block"}>
                    <NLink
                        href={rout.Main.PrivacyPolicy.create()}
                        color={colors.textColor.h757575}
                        variant={"caption"}
                        textDecorationBottom={{
                            width: 1,
                            color: colors.textColor.h757575
                        }}>
                        شرایط و قوانین
                    </NLink>
                </Box>
                استفاده از سرویس‌های سایت را می‌پذیرید.
            </Typography>
        </Box>
    )
}
