import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {BackEl} from "./helper";
import {colors, lang} from "../../repository";
import {Box, Button, tryIt, Typography} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import {UtilsFormat} from "../../utils/Utils";
import {AuthStatus} from "../../pages/login";
import ReactCodeInput from "react-code-input";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Timer from "react-compound-timer";
import ControllerUser from "../../controller/ControllerUser";


const useMobileFilterStyles = makeStyles(theme => ({
        rootVerifyCode: {
            '& input': {
                maxWidth: 50,
                padding: '5px 0 5px 0',
                textAlign: 'center',
                fontFamily: "inherit !important",
                margin: '0 5px',
                borderWidth: "0",
                borderBottom: "solid 2px #0000006b",
                [theme.breakpoints.up('md')]: {
                    fontSize: "2rem !important",
                },
                [theme.breakpoints.up('sm')]: {
                    fontSize: "1.5rem",
                },
                fontSize: "1.1rem",
                '&:focus': {
                    borderBottom: 'solid 2px',
                    borderBottomColor: theme.palette.secondary.light,
                    outlineWidth: '0'
                },
                '&:not([value=""])': {
                    borderBottomColor: theme.palette.secondary.light
                },
            }
        }
    }
))


let timerInterval = undefined
export default function VerifyCode({
                                       status,
                                       username,
                                       response,
                                       onBackClick,
                                       onSubmitted,
                                       ...props
                                   }) {
    const theme = useTheme()
    const codeRef = useRef(undefined)
    const isGustUser = useMemo(() => status === AuthStatus.GUEST_USER, [status])

    const [loading, setLoading] = useState(false)
    const classes = useMobileFilterStyles()
    const [code, setCode] = useState("")
    const [error, setError] = useState(false)
    const [disable, setDisable] = useState(true)
    const [resendTimeout, setResendTimeout] = useState()
    const [activationExpire, setActivationExpire] = useState()


    useEffect(() => {
        if (!(activationExpire - Date.now() / 1000 <= 0)) {
            setDisable(false)
            setTimeout(() => {
                focusCode()
            }, 300)
        }
        timerInterval = setInterval(() => {
            if (activationExpire - Date.now() / 1000 <= 0) {
                setDisable(true)
                clearInterval(timerInterval)
            }
        }, 1000)

        return () => {
            clearInterval(timerInterval)
        }
    }, [activationExpire])

    useEffect(() => {
        tryIt(() => {
            setResendTimeout(response.data.resend_timeout)
            setActivationExpire(response.data.activation_expire)
            setDisable(false)
        }, () => sendCode())
    }, [response])

    useEffect(() => {
        if (disable)
            clearCode()
    }, [disable])

    const codeLengthValid = useMemo(() => code.length === 5, [code])


    const clearCode = useCallback(() => {
        setTimeout(() => {
            tryIt(() => {
                _.forEach(codeRef.current.textInput, el => el.value = "")
            })
            tryIt(() => {
                codeRef.current.state.input = ["", "", "", "", ""]
            })
            setCode("")
            focusCode()
        }, 300)
    }, [codeRef])

    const focusCode = useCallback(() => {
        tryIt(() => {
            if (codeRef.current.textInput[0]) codeRef.current.textInput[0].focus()
        })
    }, [codeRef])

    const sendCode = useCallback(() => {
        if (loading)
            return
        setLoading(true)
        ControllerUser.Auth.sendCode({username}).then(res => {
            tryIt(() => {
                if (!res.data.resend_timeout)
                    return
                setResendTimeout(res.data.resend_timeout)
                setActivationExpire(res.data.activation_expire)
                setDisable(false)
            })
        }).finally(() => {
            setLoading(false)
        })
    }, [username, loading, code, codeLengthValid])

    const handleSubmitClick = useCallback((co = code) => {
        if (loading || co.length !== 5) {
            return
        }
        setLoading(true)
        ControllerUser.Auth.login({username: username, code: co}).then((res) => {
            if (status === AuthStatus.FORGOT_PASSWORD) {
                onSubmitted({res, status: AuthStatus.SET_NEW_PASSWORD})
                return
            }
            onSubmitted({res: res})
        }).catch(() => {
            clearCode();
            setError(true);
        }).finally(() => {
            setLoading(false);
        })
    }, [loading, username, code, status, onSubmitted])


    return (
        <Box {...props}>
            <BackEl onBackClick={onBackClick}/>
            <Typography
                color={colors.textColor.h000000}
                variant={'h3'}
                pb={4.6}
                component={"h3"}>
                {lang.get(isGustUser ? "sign_up" : "login")}
            </Typography>
            <Typography
                color={colors.textColor.h000000}
                variant={'subtitle2'}
                component={"p"}>
                {lang.get("otp_code_sent")}
            </Typography>
            <Box width={1} center={true} py={1}>
                <Typography variant={"body2"} pr={2} py={1} minWidth={0.5}
                            dir={"ltr"}>
                    {UtilsFormat.phoneNumberWithSpace(username)}
                </Typography>
                <Typography
                    variant={"caption"}
                    pl={2} py={1} minWidth={0.5}
                    color={theme.palette.secondary.main}
                    onClick={() => {
                        onSubmitted({status: AuthStatus.FIRST_STEP})
                    }}
                    style={{
                        cursor: "pointer"
                    }}>
                    {lang.get("edit_phone_number")}
                </Typography>
            </Box>
            <Box pt={2} dir={"ltr"}>
                <ReactCodeInput
                    ref={codeRef}
                    autoFocus={true}
                    inputMode={"numeric"}
                    name={"verifyCode"}
                    value={""}
                    disabled={loading || disable}
                    type="number"
                    className={classes.rootVerifyCode}
                    fields={5}
                    onChange={(e) => {
                        setCode(e)
                        if (e.length === 5)
                            handleSubmitClick(e)
                    }}/>
            </Box>
            {error &&
            <Typography pt={1.5} variant={"caption"} color={theme.palette.error.main}>
                {lang.get("otp_invalid")}
            </Typography>
            }
            <Button
                pt={5}
                fullWidth={true}
                disabled={!codeLengthValid || disable || loading}
                loading={loading && codeLengthValid}
                color={theme.palette.primary.main}
                disableElevation={true}
                onClick={handleSubmitClick}
                typography={{
                    py: 1,
                    variant: "body1",
                    color: "#fff"
                }}>
                {lang.get("login")}
            </Button>
            {
                resendTimeout &&
                <SendAgain loading={loading} resendTimeout={resendTimeout} activationExpire={activationExpire}
                           onSendAgainClick={sendCode}/>
            }
        </Box>
    )
}


function SendAgain({loading, resendTimeout, activationExpire, onSendAgainClick, ...props}) {
    const theme = useTheme();
    const [visible, setVisible] = useState(false)


    useEffect(() => {
        tryIt(() => {
            if (_.toInteger(resendTimeout * 1000 - Date.now()) > 0) {
                setVisible(false)
                document.getElementById("timerResetBtn").click()

            }
        })
    }, [resendTimeout])

    const handleTimerFinish = () => {
        setVisible(true)
    }


    const resendVisible = resendTimeout && visible && !loading

    return (
        <Typography variant={"caption"}
                    pt={2.5}
                    textSelectable={false}
                    color={resendVisible ? theme.palette.secondary.main : colors.textColor.ha9a9a9}
                    onClick={onSendAgainClick}
                    style={{
                        cursor: resendVisible ? "pointer" : undefined
                    }}>
            {lang.get("send_again")}
            {!visible &&
            <Timer
                initialTime={_.toInteger(resendTimeout * 1000 - Date.now())}
                formatValue={(value) => `${(value < 10 ? `0${value}` : value)}`}
                direction="backward"
                checkpoints={[
                    {
                        time: 0,
                        callback: handleTimerFinish,
                    },
                ]}>
                {({start, reset}) => (
                    <Box dir={"ltr"} px={1}>
                        <Typography variant={"caption"} minWidth={25} center={true}>
                            <Timer.Minutes/>
                        </Typography>
                        :
                        <Typography variant={"caption"} minWidth={25} center={true}>
                            <Timer.Seconds/>
                        </Typography>
                        <div
                            style={{display: 'none'}}
                            id="timerResetBtn"
                            onClick={(e) => {
                                reset();
                                start();
                            }}>
                        </div>
                    </Box>
                )}
            </Timer>}
        </Typography>
    )
}
