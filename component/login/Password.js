import React, {useCallback, useMemo, useRef, useState} from "react";
import {
    Box,
    Button,
    DefaultTextField,
    FormController,
    IconButton,
    NoneTextField,
    tryIt,
    Typography
} from "material-ui-helper";
import {colors, lang} from "../../repository";
import {useTheme} from "@material-ui/core";
import {CheckCircleOutline, ChevronLeft, InfoOutlined, Visibility, VisibilityOff} from "@material-ui/icons";
import _ from "lodash"
import ControllerUser from "../../controller/ControllerUser";
import {DEBUG} from "../../pages/_app";
import {AuthStatus} from "../../pages/login";
import {BackEl} from "./helper";
import {UtilsFormat} from "../../utils/Utils";


export default function Password({username, status, onSubmitted, onBackClick, ...props}) {
    const theme = useTheme();
    const ref = useRef()
    const isNewPassword = useMemo(() => status === AuthStatus.SET_NEW_PASSWORD || status === AuthStatus.SET_PASSWORD_REQUIRED, [status])
    const isNewUserPassword = useMemo(() => status === AuthStatus.SET_NEW_USER_PASSWORD, [status])


    const [password, setPassword] = useState("")
    const [showPassword, setShowPassword] = useState(false)
    const [passwordMinCharacterShow, setPasswordMinCharacterShow] = useState(isNewPassword || isNewUserPassword)
    const [passwordIncorrect, setPasswordIncorrect] = useState(0)
    const [inputFocus, setInputFocus] = useState(false)
    const [loading, setLoading] = useState(false)


    //region props
    const passwordLengthCheck = useMemo(() => password.length >= 6, [password])
    const formHasError = !passwordLengthCheck;
    const hasErrorInput = useMemo(() => !_.isEmpty(password) && !inputFocus && !passwordLengthCheck, [password, inputFocus, passwordLengthCheck])

    //endregion props

    const handleSubmitClick = useCallback(() => {
        if (formHasError || loading) {
            return
        }
        setLoading(true)
        if (isNewPassword || isNewUserPassword) {
            ControllerUser.Auth.setPassword({username: username, password: password}).then((res) => {
                tryIt(() => onSubmitted({
                    res: res,
                }))
            }).finally(() => {
                setLoading(false)
            })
            return;
        }
        ControllerUser.Auth.login({username: username, password: password}).then(res => {
            tryIt(() => onSubmitted({
                res: res
            }))
        }).catch((res) => {
            if (DEBUG || res.status === 401) {
                setPasswordIncorrect(v => v + 1)
                setPassword("")
                setTimeout(() => {
                    tryIt(() => document.getElementsByName("password")[0].focus())
                }, 500)
            }
        }).finally(() => {
            setLoading(false)
        })
    }, [loading, formHasError, username, password, isNewPassword, isNewUserPassword])

    const handlePasswordChange = useCallback((e, value) => {
        setPassword(value)
    }, [])

    const handleFocusIn = useCallback((e) => {
        setInputFocus(true)
    }, [])

    const handleFocusOut = useCallback((e) => {
        setInputFocus(false)
        if (!_.isEmpty(password) && !passwordLengthCheck) {
            setPasswordMinCharacterShow(true)
        }
    }, [password, passwordLengthCheck])


    return (
        <Box {...props}>
            <BackEl onBackClick={onBackClick}/>
            <Typography
                color={colors.textColor.h000000}
                variant={'h3'}
                pb={4.6}
                component={"h3"}>
                {lang.get(isNewUserPassword ? "create_password" : isNewPassword ? "new_password" : "login")}
            </Typography>
            <Typography
                color={colors.textColor.h757575}
                variant={'subtitle2'}
                pb={4}
                component={"p"}>
                {lang.get("enter_password")}
            </Typography>
            <FormController
                innerRef={ref}
                onSubmit={handleSubmitClick}
                minWidth={"100%"}>
                <Box width={1} flexDirectionColumn={true}>
                    <NoneTextField name={"username"} defaultValue={username}/>
                    <DefaultTextField
                        value={password}
                        name={"password"}
                        variant={"outlined"}
                        disabled={loading}
                        type={showPassword ? "text" : "password"}
                        autoFocus={true}
                        placeholder={lang.get("password")}
                        autoComplete={(isNewUserPassword || isNewPassword) ? "new-password" : "current-password"}
                        error={hasErrorInput}
                        onChangeTextField={handlePasswordChange}
                        onFocusOut={handleFocusOut}
                        onFocusIn={handleFocusIn}
                        inputStyle={{
                            textAlign: 'center'
                        }}
                        endAdornment={(
                            <IconButton
                                disabled={loading}
                                title={lang.get(showPassword ? "hide" : "show")}
                                onClick={() => {
                                    setShowPassword(val => !val)
                                }}
                                style={{
                                    position: 'absolute',
                                    left: 2
                                }}>
                                {
                                    showPassword ?
                                        <VisibilityOff/> :
                                        <Visibility/>
                                }
                            </IconButton>
                        )}
                    />
                    {
                        (!isNewPassword && !isNewUserPassword && passwordIncorrect > 0 && !loading) ?
                            <Typography pt={1} variant={"caption"}
                                        color={theme.palette.error.main}>
                                {lang.get("password_incorrect")}
                            </Typography> :
                            (passwordMinCharacterShow) ?
                                <PasswordLengthChecker
                                    passwordLengthCheck={passwordLengthCheck}
                                    password={password}
                                    inputFocus={inputFocus}/> :
                                <React.Fragment/>
                    }
                    {
                        (!isNewPassword && !isNewUserPassword) &&
                        <Box pt={1}>
                            <Typography
                                py={1}
                                variant={"caption"}
                                color={passwordIncorrect >= 1 ? theme.palette.secondary.main : colors.textColor.he9e9e9}
                                onClick={() => {
                                    onSubmitted({status: AuthStatus.FORGOT_PASSWORD})
                                }}
                                style={{cursor: "pointer"}}>
                                {lang.get("forgot_password")}
                            </Typography>
                        </Box>
                    }
                </Box>
            </FormController>
            <Button
                pt={5}
                id={"login_first_step_submit_button"}
                fullWidth={true}
                color={theme.palette.primary.main}
                loading={loading}
                disableElevation={true}
                onClick={handleSubmitClick}
                disabled={formHasError}
                typography={{
                    py: 1,
                    variant: "body1",
                    color: "#fff"
                }}>
                {lang.get(isNewUserPassword ? "sign_up" : isNewPassword ? "change_password" : "continue")}
            </Button>
            {
                !(isNewPassword || isNewUserPassword || status === AuthStatus.PASSWORD_REQUIRED) &&
                <Box
                    pt={1}
                    center={true}
                    onClick={() => {
                        onSubmitted({status: AuthStatus.LOGIN_WITH_OTP})
                    }}
                    style={{
                        flexWrap: 'wrap',
                        cursor: 'pointer'
                    }}>
                    <Typography
                        py={1}
                        color={theme.palette.secondary.main}
                        variant={"body1"}
                        mt={1}>
                        {lang.get("send_otp_code_to_number")}
                    </Typography>
                    <Box pb={1} center={"true"}>
                        <Typography
                            color={theme.palette.secondary.main}
                            pl={1} variant={"body1"} dir={"ltr"}>
                            {UtilsFormat.phoneNumberWithSpace(username)}
                        </Typography>
                        <ChevronLeft style={{color: theme.palette.secondary.main}}/>
                    </Box>
                </Box>}
        </Box>
    )
}


function PasswordLengthChecker({password, inputFocus, passwordLengthCheck}) {
    const theme = useTheme()
    const len = passwordLengthCheck;
    const success = theme.palette.success.main;
    const error = password.length !== 0 && !len
    const color = (inputFocus) ? ((len) ? success : colors.textColor.ha9a9a9) : (len) ? success : error ? theme.palette.error.main : colors.textColor.ha9a9a9;
    return (
        <Box alignCenter={true} pt={1}>
            {
                !error ?
                    <CheckCircleOutline style={{color: color}}/> :
                    <InfoOutlined style={{color: color}}/>
            }

            <Typography px={1} variant={"caption"}
                        color={color}>
                {lang.get("password_min_character")}
            </Typography>
        </Box>
    )
}
