import React from 'react';
import ReactCodeInput from "react-code-input";
import Box from "@material-ui/core/Box";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    inputs: {
        '& input': {
            maxWidth:50,
            padding: '5px 0 5px 0',
            textAlign: 'center',
            fontFamily: "inherit !important",
            margin: '0 5px',
            borderWidth: "0",
            borderBottom: "solid 2px #0000006b",
            [theme.breakpoints.up('md')]: {
                fontSize: "2rem !important",
            },
            [theme.breakpoints.up('sm')]: {
                fontSize: "1.5rem",
            },
            fontSize: "1.1rem",
            '&:focus': {
                borderBottom: 'solid 2px',
                borderColor: theme.palette.primary.main,
                outlineWidth: '0'
            }
        }
    }
});

function BaseTextField({onChange, disable, ...props}) {
    const {classes} = props;

    return (
        <Box dir="ltr" display="flex" my={1} flexGrow={1} justifyContent={"center"} alignItems="center">
            <ReactCodeInput
                disabled={disable}
                type="number"
                fields={5}
                className={classes.inputs}
                onChange={(e) => {
                    onChange(e)
                }}/>
        </Box>
    )
}

export default withStyles(styles)(BaseTextField)

