import React, {useEffect, useState} from 'react';
import {createTheme, withStyles} from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import VerifyMobileCode from './VerifyMobileCode'
import Typography from "@material-ui/core/Typography";
import {lang} from "../../../repository"
import Timer from 'react-compound-timer'
import Link from "@material-ui/core/Link";
import SuccessButton from "../../base/button/buttonVariant/SuccessButton";
import {UtilsConverter, UtilsStyle} from "../../../utils/Utils";
import {grey} from "@material-ui/core/colors";

const styles = theme => ({
    paper: {
        [theme.breakpoints.up('md')]: {
            width: "40% !important",
        },
        [theme.breakpoints.up('sm')]: {
            width: "85%",
        },
        width: "98%"
    }
});

function VerifyMobile({phone, code, onCodeChange, timeout, resendTimeout, onResendClick, onChangePhoneClick, onSubmitClick, disable, ...props}) {
    let theme = createTheme();

    useEffect(() => {
        resetResendTimeout()
    }, []);

    const [state, setState] = useState({
        timeIsEnded: false,
        resendTimeIsEnd: false,
    });

    function onTimeEnded() {
        setState({
            ...state,
            timeIsEnded: true,
            resendTimeIsEnd: true
        })
    }

    function resetResendTimeout(time) {
        setTimeout(() => {
            setState({
                ...state,
                resendTimeIsEnd: false,
            })
        }, time ? time : resendTimeout)
    }

    return (
        <Box display="flex" flexDirection="column" my={6} justifyContent={"center"}>
            <Typography variant="h5"
                        component="h1"
                        align="center"
                        style={{
                            marginTop: theme.spacing(5),
                            marginBottom: theme.spacing(5),
                            fontWeight: "bold"
                        }}>
                {lang.get("phone_number_verification")}
            </Typography>
            <VerifyMobileCode
                disable={disable || state.timeIsEnded}
                onChange={(e) => {
                    onCodeChange(e);
                    // setVerifyMobile(e);
                    if (e.length === 5) {
                        try {
                            document.getElementById('verifyCodeBtn').focus()
                        } catch (e) {

                        }
                    }
                }}/>
            <Typography variant="subtitle1"
                        component="h3"
                        align="center"
                        style={{
                            marginTop: theme.spacing(5),
                            marginBottom: theme.spacing(2),
                            paddingLeft: theme.spacing(2),
                            paddingRight: theme.spacing(2)
                        }}>
                {lang.get("phone_number_verification_message_withparam_mobile", {
                    args: {
                        mobile: phone
                    }
                }) + " "}
                <Link disabled={disable}
                      onClick={onChangePhoneClick}
                      style={{
                          fontSize: UtilsConverter.addRem(theme.typography.subtitle2.fontSize, 0.8)
                      }}>
                    {lang.get("edit_number")}
                </Link>
            </Typography>
            <Box
                display="flex"
                flexGrow={4}
                px={2}
                justifyContent={"center"}
                alignItems="center">
                <Box
                    my={1}
                    mx={2}
                    py={1}
                    px={2}
                    style={{
                        minWidth: "65px",
                        border: "solid 2px",
                        borderColor: theme.palette.primary.main,
                        textAlign: "center",
                        ...UtilsStyle.borderRadius(5),
                    }}>
                    <Timer
                        initialTime={timeout}
                        direction="backward"
                        checkpoints={[
                            {
                                time: 0,
                                callback: () => {
                                    onTimeEnded()
                                },
                            },
                        ]}>
                        {({start, reset}) => (
                            <div>
                                <Timer.Minutes/>
                                :
                                <Timer.Seconds/>
                                <div
                                    style={{display: 'none'}}
                                    id="timerResetBtn"
                                    onClick={(e) => {
                                        reset();
                                        start();
                                    }}>
                                </div>
                            </div>
                        )}
                    </Timer>
                </Box>
                <Link
                    variant={"body1"}
                    onClick={() => {
                        if (!state.resendTimeIsEnd || disable)
                            return
                        resetResendTimeout(resendTimeout / 2);
                        setState({
                            ...state,
                            resendTimeout: false
                        });
                        onResendClick()
                    }}
                    style={{
                        color: !state.resendTimeIsEnd || disable ? grey[300] : undefined,
                        cursor: !state.resendTimeIsEnd || disable ? "default" : undefined
                    }}>
                    {lang.get("resend_code_message")}
                </Link>
            </Box>
            <SuccessButton
                id="verifyCodeBtn"
                variant="contained"
                disabled={disable || state.timeIsEnded}
                style={{
                    width: "fit-content",
                    marginLeft: "auto",
                    marginRight: "auto",
                    marginTop: theme.spacing(2),
                    marginBottom: theme.spacing(1)
                }}
                size="large"
                loading={disable}
                onClick={onSubmitClick}>
                {lang.get("sign_up_finalization")}
            </SuccessButton>
        </Box>
    )
}

export default withStyles(styles)(VerifyMobile)

