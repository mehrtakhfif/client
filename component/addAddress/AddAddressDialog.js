import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";
import {grey} from "@material-ui/core/colors";
import Typography from "../base/Typography";
import {lang} from "../../repository";
import AddAddress from "../../pages/add-address";
import Dialog from "@material-ui/core/Dialog";
import {tryIt} from "material-ui-helper";


export default function AddAddressDialog({open,address, onClose, onFinish}) {

    return (
        <Dialog
            open={open}
            onClose={() => {
                tryIt(() => onClose())
            }}
            fullScreen
            aria-labelledby="address_edit_alert-dialog-title"
            aria-describedby="address_edit_alert-dialog-description">
            <AppBar style={{position: 'relative'}}>
                <Toolbar
                    style={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                        backgroundColor: "#fff"
                    }}>
                    <IconButton edge="start"
                                onClick={() => {
                                    onClose()
                                }}
                                aria-label="close">
                        <Close style={{color: grey[800]}}/>
                    </IconButton>
                    <Typography variant="h6" ml={2} color={grey[800]}>
                        {lang.get(address ? "edit_address" : "add_address")}
                    </Typography>
                </Toolbar>
            </AppBar>
            <AddAddress
                address={address}
                directBack={false}
                onFinish={(address) => {
                    try {
                        if (onFinish) {
                            onFinish(address)
                            return
                        }
                        onClose()
                    } catch {
                    }
                }}/>
        </Dialog>
    )
}