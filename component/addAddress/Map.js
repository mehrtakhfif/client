import React, {useState} from "react";
import {makeStyles} from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import {Map as LeafletMap, TileLayer} from "react-leaflet-universal";
import PropTypes from "prop-types";


const useStyles = makeStyles(theme => ({
    mapRoot: {
        minHeight: 300,
        height: '100%',
    }
}));
export default function Map(props) {

    const {targetLocation, onMoveEnd} = props;
    const classes = useStyles(props);
    const [state, setState] = useState({
        zoom: 14,
    });

    //region Function
    function onZoomEnd(e) {
        setState({
            ...state,
            zoom: e.target._zoom
        })
    }


    //endregion Function

    //region Marker
    // L.Marker.prototype.options.icon = L.icon({
    //     iconUrl: '',
    // });
    // L.Control.prototype.options.icon = L.icon({
    //     iconUrl: '/drawable/mapImage/marker-icon.png',
    //     shadowUrl: '/drawable/mapImage/marker-shadow.png',
    // });
    //endregion Marker

    return (
        <Box width="100%"
             className={classes.mapRoot}
             dir={'ltr'}
             style={{
                 direction: 'ltr',
                 overflow: 'hidden',
                 position: 'relative',
                 zIndex:9999999,
                 ...props.style
             }}>
            <LeafletMap
                animate={true}
                className={classes.root}
                center={targetLocation}
                doubleClickZoom={true}
                boxZoom={true}
                zoomControl={true}
                minZoom={7}
                maxZoom={19}
                onMoveEnd={(e) => {
                    onMoveEnd(e.target.getCenter())
                }}
                onZoomend={onZoomEnd}
                length={4}
                zoom={state.zoom}
                style={{
                    height: '100%',
                }}>
                {
                    () => {
                        return (

                            <TileLayer
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'/>
                        )
                    }
                }
            </LeafletMap>


            <img src={'/drawable/mapImage/marker-icon.png'}
                 alt='Map Marker'
                 style={{
                     position: 'absolute',
                     top: '50%',
                     zIndex: '9999',
                     left: '50%',
                     width: 25,
                     marginLeft: -12.5,
                     marginTop: -41
                 }}/>
        </Box>
    )
}

Map.propTypes = {
    centerLocation: PropTypes.object,
    targetLocation: PropTypes.object.isRequired,
    onMoveEnd: PropTypes.func.isRequired,
};
