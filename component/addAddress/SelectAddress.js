import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {lang} from "../../repository";
import BaseButton from "../base/button/BaseButton";
import Box from "@material-ui/core/Box";
import {useTheme} from "@material-ui/styles";
import Addresses from "../profile/Addresses";


const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function SelectAddress({open, addable = true,deletable=true, onSelect, ...props}) {
    const theme = useTheme();

    return (
        <Dialog fullScreen open={open} onClose={() => onSelect()} TransitionComponent={Transition}>
            <AppBar style={{position: 'relative'}}>
                <Toolbar>
                    <Box display={'flex'}>
                        <IconButton edge="start" onClick={() => onSelect()} aria-label="close"
                                    style={{color: theme.palette.text.primary}}>
                            <CloseIcon/>
                        </IconButton>
                        <Box mr={1}>
                            <BaseButton variant={"text"} onClick={() => onSelect()}>
                                {lang.get("close")}
                            </BaseButton>
                        </Box>
                    </Box>
                    <Box flex={1}>
                        <BaseButton variant={"text"}>
                            <Typography variant="h6">
                                {lang.get("add_new_address")}
                            </Typography>
                        </BaseButton>
                    </Box>
                </Toolbar>
            </AppBar>
            <Box py={2} width={1} display={'flex'} justifyContent={'center'}>
                <Box width={"80%"}>
                    <Addresses
                        deletable={deletable}
                        onSelect={(address) => {
                            onSelect(address)
                        }}/>
                </Box>
            </Box>
        </Dialog>
    );
}
