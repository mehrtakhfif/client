import React, {useContext} from "react";
import {NextSeo, ProductJsonLd} from "next-seo";
import {lang, media, siteRout} from "../../../repository";
import rout from "../../../router";
import {getSafe, tryIt} from "material-ui-helper";
import _ from "lodash";
import ProductDataContext from "../context/ProductDataContext";
import StorageDataContext from "../context/StorageDataContext";
import Head from "next/head";


export default function Seo() {
    const [product,__,productError] = useContext(ProductDataContext);
    const [storageData] = useContext(StorageDataContext);
    const {is_service, storage} = storageData || {};



    const productName = product?.name ||lang.get(productError ? "product_not_found" : "product")

    return (
        <React.Fragment>
            <Head>
                <meta property="product:price:currency" content="IRR"/>
                <title>{productName}</title>
                {
                    (() => {
                        return tryIt(() => {
                            const st = is_service ? getSafe(() => _.find(storage, st => st?.discount_price > 0) || storage?.[0]) : storage;
                            const maxCountForSale = getSafe(() => st.max_count_for_sale, 0)
                            const discountPrice = getSafe(() => maxCountForSale === 0 ? 0 : st.discount_price, 0);
                            return (
                                <React.Fragment>
                                    <meta property="product:availability"
                                          content={maxCountForSale > 0 ? "in stock" : "out of stock"}/>
                                    <meta property="product:price:amount"
                                          content={discountPrice}/>
                                </React.Fragment>
                            )
                        }, <React.Fragment/>)
                    })()
                }
            </Head>
            <ProductJsonLd
                productName={productName}
                images={product?.media?.map((m) => m?.image)}
                description={product?.short_description}
                brand={product?.brand?.name}
                aggregateRating={product?.rate > 0 ? {
                    ratingValue: product?.rate,
                    reviewCount: product?.rate > 0 ? 1 : 0, //TODO: reviewCount not received
                } : undefined}
                offers={(() => {
                    return tryIt(() => {
                        if (is_service) {
                            const storages = []
                            _.forEach(storage, s => {
                                storages.push({
                                    price: s?.discount_price * 10,
                                    priceCurrency: "IRR",
                                    availability: s?.max_count_for_sale ? "https://schema.org/InStock" : `https://schema.org/${is_service ? "SoldOut" : "OutOfStock"}`,
                                    url: siteRout + rout.Product.SingleV2.as(permalink),
                                    seller: {
                                        type: 'Organization',
                                        name: lang.get("mehr_takhfif")
                                    }
                                })
                            })
                            return storages
                        }

                        return [{
                            price: storage?.discount_price * 10,
                            priceCurrency: "IRR",
                            availability: storage?.max_count_for_sale ? "https://schema.org/InStock" : `https://schema.org/${is_service ? "SoldOut" : "OutOfStock"}`,
                            url: siteRout + rout.Product.SingleV2.as(product?.permalink),
                            seller: {
                                type: 'Organization',
                                name: lang.get("mehr_takhfif")
                            }
                        }]


                    })
                })()}
                reviews={[]}/>
            <NextSeo
                title={productName}
                description={product?.short_description}
                canonical={siteRout + rout.Product.SingleV2.as(product?.permalink)}
                noindex={Boolean(productError)}
                nofollow={Boolean(productError)}
                image={getSafe(() => product?.thumbnail?.image)}
                openGraph={{
                    type: "product",
                    title: productName,
                    images: [{
                        url: getSafe(() => product?.thumbnail?.image),
                        width: media.thumbnail.width,
                        height: media.thumbnail.height,
                        alt: product?.thumbnail?.title,
                    }],
                    url: rout.Product.SingleV2.as(product?.permalink),
                }}/>

            {/*{*/}
            {/*    DEBUG &&*/}
            {/*    <BreadcrumbJsonLd*/}
            {/*        itemListElements={[*/}
            {/*            {*/}
            {/*                position: 1,*/}
            {/*                name: 'Books',*/}
            {/*                item: 'https://example.com/books',*/}
            {/*            },*/}
            {/*            {*/}
            {/*                position: 2,*/}
            {/*                name: 'Authors',*/}
            {/*                item: 'https://example.com/books/authors',*/}
            {/*            },*/}
            {/*            {*/}
            {/*                position: 3,*/}
            {/*                name: 'Ann Leckie',*/}
            {/*                item: 'https://example.com/books/authors/annleckie',*/}
            {/*            },*/}
            {/*            {*/}
            {/*                position: 4,*/}
            {/*                name: 'Ancillary Justice',*/}
            {/*                item: 'https://example.com/books/authors/ancillaryjustice',*/}
            {/*            },*/}
            {/*        ]}*/}
            {/*    />*/}
            {/*}*/}
        </React.Fragment>
    )
}