import React, {useContext} from "react";
import {Box, Button, tryIt, Typography, useState} from "material-ui-helper";
import {makeStyles} from "@material-ui/styles";
import ProductDataContext from "../context/ProductDataContext";
import {Map as LeafletMap, Marker, TileLayer} from "react-leaflet-universal";
import clsx from "clsx";
import {lang} from "../../../repository";
import MtIcon from "../../MtIcon";


const useStyles = makeStyles(theme => ({
    mapRootContainer: {
        minHeight: 300,
        height: '100%',
        width: '100%'
    },
    placeholder: {
        backgroundImage: `url(/drawable/image/map.jpg)`,
    },
    mapRoot: {
        '& .leaflet-marker-pane img': {
            top: -41,
            right: -12.5,
            width: 25,
            height: 41
        },
        '& .leaflet-popup': {
            bottom: '22px !important',
        }
    }
}));

export default function LocationOnMap(props) {
    const [product] = useContext(ProductDataContext);
    const {location} = product || {};

    return (
        location ?
            <Base location={location} {...props}/> :
            <React.Fragment/>
    )
}

function Base({location, ...props}) {
    const classes = useStyles()
    const [show, setShow] = useState(false)
    const [scrollWheelZoom, setScrollWheelZoom] = useState(false)
    const [zoom, setZoom] = useState(14)


    function onClick(e) {
        tryIt(() => {
            e.stopPropagation()
        })
        if (!show) {
            setShow(true)
            return;
        }
        if (!scrollWheelZoom)
            setScrollWheelZoom(true)
    }

    return (
        <Box
            component={'li'}
            pt={{
                xs: 3,
                md: 8
            }}
            width={1}
            {...props}
            style={{
                position: "relative",
                ...props.style
            }}>
            {
                !show ?
                    <Box center={true}
                         className={clsx([classes.mapRootContainer, classes.placeholder])}>
                        <Button
                            variant={"outlined"}
                            disableElevation={true}
                            color={"#fff"}
                            onClick={onClick}>
                            <Box center={true} py={1}>
                                <MtIcon icon={"mt-map-marker-alt"} color={"#fff"}/>
                                <Typography pl={1} color={"#fff"} variant={'body1'}>
                                    {lang.get("show_address_on_map")}
                                </Typography>
                            </Box>
                        </Button>
                    </Box> :
                    <React.Fragment>
                        <LeafletMap
                            onClick={onClick}
                            animate={true}
                            className={clsx([classes.mapRootContainer, classes.mapRoot])}
                            center={location}
                            doubleClickZoom={true}
                            boxZoom={true}
                            zoomControl={true}
                            scrollWheelZoom={scrollWheelZoom}
                            minZoom={7}
                            maxZoom={19}
                            length={4}
                            zoom={zoom}
                            style={{
                                width: '100%',
                                height: '100%',
                            }}>
                            <TileLayer
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'/>
                            <Marker position={location}/>
                        </LeafletMap>
                    </React.Fragment>
            }
        </Box>
    )
}