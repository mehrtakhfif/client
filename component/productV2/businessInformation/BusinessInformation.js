import React, {useContext} from "react";
import {Box, Button, ButtonBase, HiddenLgUp, HiddenMdDown, Typography, useState} from "material-ui-helper";
import ContentContainer from "../StickyContent/elements/helper/ContentContainer";
import ProductDataContext from "../context/ProductDataContext";
import {colors, lang} from "../../../repository";
import MtIcon from "../../MtIcon";
import {useTheme} from "@material-ui/core";
import LocationOnMap from "./LocationOnMap";
import {UtilsParser} from "../../../utils/Utils";


export default function BusinessInformation({name, showTitle, divider, ...props}) {
    const [product] = useContext(ProductDataContext);
    const {address, location, details, cities} = product || {};
    const {days, hours, phone} = details || {}


    return (
        <ContentContainer
            name={name}
            showTitle={showTitle}
            divider={divider}
            showMoreProps={{
                className: "lineHeight32"
            }}>
            <Box pl={1} pt={1} flexDirectionColumn={true}>
                <Box component={'ul'} flexDirectionColumn={true} width={1}>
                    <Item
                        icon={"mt-clock"}
                        title={lang.get("service_time")}
                        value={UtilsParser.html(hours)}/>
                    <Item
                        icon={"mt-calendar"}
                        title={lang.get("service_days")}
                        value={days}/>
                    <PhoneCm phone={phone}/>
                    <Item
                        icon={"mt-map-marker"}
                        title={lang.get("address")}
                        value={UtilsParser.html(address)}/>
                    <LocationOnMap/>
                </Box>
            </Box>
        </ContentContainer>
    )
}

//region Phone

function PhoneCm({phone}) {

    return (
        phone ?
            <Item
                icon={"mt-phone"}
                title={lang.get("call_number")}
                value={(
                    <Box flexDirectionColumn={true}>
                        {
                            phone.map((it, index) => <PhoneItem phone={it} key={it} pt={index !== 0 ? 2 : 0}/>)
                        }
                    </Box>
                )}/> :
            <React.Fragment/>
    )
}

function PhoneItem({phone, ...props}) {
    const theme = useTheme()
    const [show, setShow] = useState(false)

    return (
        <Box {...props}>
            <Box
                pr={{
                    xs: 1,
                    md: 0
                }}>
                <Typography
                    dir={'ltr'}
                    component={show ? 'a' : 'div'}
                    href={show ? `tel:${phone}` : undefined}
                    variant={{
                        xs:"subtitle1",
                        md:"subtitle2"
                    }}
                    fontWeight={"normal"} color={colors.textColor.h757575}>
                    {show ? phone.spaceWithPattern("#### ### ####") : phone.replaceAtTo(4, 7, " *** ")}
                </Typography>
            </Box>
            <HiddenLgUp>
                <Button
                    color={theme.palette.secondary.main}
                    size={"small"}
                    variant={"outlined"}
                    onClick={() => {
                        setShow(true)
                        window.open(`tel:${phone}`, '_self')
                    }}>
                    <Typography
                        px={1}
                        variant={"subtitle2"}
                        color={theme.palette.secondary.main}>
                        {lang.get("contact_the_business")}
                    </Typography>
                </Button>
            </HiddenLgUp>
            <HiddenMdDown>
                {
                    !show &&
                    <Box pl={2}>
                        <ButtonBase
                            onClick={() => setShow(true)}>
                            <Typography
                                px={1}
                                variant={"subtitle2"}
                                color={theme.palette.secondary.main}>
                                {lang.get("show_full_number")}
                            </Typography>
                        </ButtonBase>
                    </Box>
                }
            </HiddenMdDown>

        </Box>
    )
}

//endregion Phone

function Item({icon, title, value}) {

    return (
        value ?
            <Box py={1} component={"li"}>
                <Box pt={"2px"} alignItems={{
                    xs: 'unset',
                    md: "center"
                }}>
                    <MtIcon icon={icon}/>
                </Box>
                <Box
                    width={1}
                    pl={{
                        xs: 1,
                        md: 2
                    }}
                    flexDirection={{
                        xs: "column",
                        md: "row"
                    }}>
                    <Box
                        width={{
                            xs: "100%",
                            md: "30%"
                        }}
                        pb={{
                            xs: 1,
                            md: 0
                        }}
                        alignItems={'center'}>
                        <Box>
                            <Typography
                                variant={{
                                    xs: "subtitle1",
                                    md: "subtitle2"
                                }}
                                fontWeight={"normal"}>
                                {title}
                            </Typography>
                        </Box>
                    </Box>
                    <Typography
                        width={{
                            xs: "100%",
                            md: "70%"
                        }}
                        variant={{
                            xs: "subtitle1",
                            md: "subtitle2"
                        }}
                        style={{whiteSpace: "pre-wrap"}}
                        fontWeight={"normal"} color={colors.textColor.h757575}>
                        {value}
                    </Typography>
                </Box>
            </Box> :
            <React.Fragment/>
    )
}