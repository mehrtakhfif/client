import React, {useContext} from "react";
import ProductDataContext from "../context/ProductDataContext";
import {Box} from "material-ui-helper";
import RatingComponent from "../../base/RatingComponent";
import {DEBUG} from "../../../pages/_app";


export default function RatingProduct({readOnly=true,...props}) {
    const [product] = useContext(ProductDataContext);
    const {rate} = product || {}


    function handleRatingChange() {
        //Todo: do something
    }

    return (
        <Box {...props}>
            <RatingComponent
                readOnly={readOnly}
                rate={(DEBUG && !rate) ? 3.5 : rate}
                size={"small"}
                onRatingChange={handleRatingChange}/>
        </Box>
    )
}