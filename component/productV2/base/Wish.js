import {Tooltip, useTheme} from "@material-ui/core";
import React, {useContext, useState} from "react";
import ControllerUser from "../../../controller/ControllerUser";
import MaterialIconButton from "@material-ui/core/IconButton";
import {grey} from "@material-ui/core/colors";
import {Box, HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import ErrorBoundary from "../../base/ErrorBoundary";
import MtIcon from "../../MtIcon";
import ProductDataContext from "../context/ProductDataContext";


export default function Wish(props) {

    const theme = useTheme();
    const [product] = useContext(ProductDataContext);
    const {id: productId, wish: wh, permalink} = product || {}


    const [backupWish, setBackupWish] = useState(Boolean(wh))
    const [wish, setWish] = useState(Boolean(wh))
    const [timer, setTimer] = useState()


    function handleWishChange() {
        setWish(!wish)
        clearTimeout(timer);
        setTimer(setTimeout(() => {
            update(wish)
        }, 2500));
    }

    function update(wish) {
        ControllerUser.User.Profile.Wishlist.update({productId: productId, wish: !wish}).then(() => {
            setBackupWish(wish)
        }).catch(() => {
            setWish(backupWish)
        })
    }

    return (
        <ErrorBoundary elKey={"Wish::(component->productV2->storagePanel->Wish.js)"}>
            <Box
                pr={{
                    xs: 0,
                    lg: 2
                }}
                {...props}>
                <IconButton
                    wish={wish}
                    onClick={handleWishChange}>
                    {!wish ?
                        <MtIcon icon={"mt-heart"} color={grey[500]}/> :
                        <MtIcon icon={"mt-heart-filled"} color={theme.palette.primary.main}/>
                    }
                </IconButton>
            </Box>
        </ErrorBoundary>
    )
}

function IconButton({wish, onClick, ...props}) {
    return (
        <React.Fragment>
            <HiddenLgUp>
                <Tooltip title={wish ? "حذف علاقه مندی‌ها" : "افزودن به علاقه مندی‌ها"}>
                    <MaterialIconButton onClick={onClick}>
                        {props.children}
                    </MaterialIconButton>
                </Tooltip>
            </HiddenLgUp>
            <HiddenMdDown>
                <Tooltip title={wish ? "حذف علاقه مندی‌ها" : "افزودن به علاقه مندی‌ها"}>
                    <MaterialIconButton size={"small"} onClick={onClick}>
                        {props.children}
                    </MaterialIconButton>
                </Tooltip>
            </HiddenMdDown>
        </React.Fragment>
    )
}