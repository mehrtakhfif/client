import StickyBox from "../base/StickyBox";
import StoragePanel from "./storagePanel/StoragePanel";
import {HiddenMdDown} from "material-ui-helper";
import React from "react";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import {headerOffset, headerOffsetPlus} from "../header/HeaderLg";

export default function LeftStickyPanel() {
    const t = useScrollTrigger();

    return (
        <HiddenMdDown>
            <StickyBox
                offsetTop={t?headerOffsetPlus:headerOffset}
                width={"32%"}>
                <StoragePanel/>
            </StickyBox>
        </HiddenMdDown>
    )
}
