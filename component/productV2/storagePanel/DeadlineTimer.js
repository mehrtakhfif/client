import React, {useEffect, useState} from "react";
import {useTheme} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Typography from "../../base/Typography";
import moment from "moment";
import _ from "lodash";
import ErrorBoundary from "../../base/ErrorBoundary";


export default function DeadlineTimer({deadline, ...props}) {
    const [state, setState] = useState({show: deadline > new Date().getTime() / 1000})
    const theme = useTheme();

    return (
        <React.Fragment>
            {
                (deadline && state.show) &&
                <ErrorBoundary elKey={"DeadlineTimer::(component->productV2->storagePanel->DeadlineTimer.js)"}>
                    <Box position='relative' pt={2} pb={0.5} px={1}
                         display={'flex'}
                         alignItems={'center'}
                         {...props}
                         style={{
                             borderBottom: `1px solid ${theme.palette.primary.main}`
                         }}>
                        <Typography variant={"subtitle1"} color={theme.palette.primary.main}>
                            تخفیف ویژه
                        </Typography>
                        <TimerComponent deadline={deadline}/>
                    </Box>
                </ErrorBoundary>}
        </React.Fragment>
    )
}

export function TimerComponent({deadline, ...props}) {
    const theme = useTheme();

    const [state, setState] = useState({
        duration: moment.duration((deadline - moment().unix()) * 1000, 'milliseconds')
    });
    useEffect(() => {
        const interval = setInterval(() => {
            setState({
                ...state,
                duration: moment.duration((deadline - moment().unix()) * 1000 - interval, 'milliseconds')
            });
        }, 1000);
        return () => clearInterval(interval);
    }, []);


    function serializeTime(val) {
        const v = _.toString(val);
        return v.length === 1 ? '0' + v : v;
    }

    const du = state.duration;
    const hours = serializeTime((du.days() * 24) + du.hours());
    const minutes = serializeTime(du.minutes());
    const seconds = serializeTime(du.seconds());
    return (
        <Box dir='ltr' flex={1} display='flex' alignItems={'center'} {...props}>
            <TimerItem>
                {hours}
            </TimerItem>
            <Typography fontWeight={400} variant={"h6"} color={theme.palette.primary.main}>
                :
            </Typography>
            <TimerItem>
                {minutes}
            </TimerItem>
            <Typography fontWeight={500} variant={"h6"} color={theme.palette.primary.main}>
                :
            </Typography>
            <TimerItem>
                {seconds}
            </TimerItem>
        </Box>
    )
}

function TimerItem({...props}) {
    const theme = useTheme()
    return (
        <Typography variant={"h6"} color={theme.palette.primary.main} fontWeight={400} alignItems={"center"}
                    justifyContent={"center"}
                    style={{letterSpacing: 2, minWidth: 30}}>{props.children}</Typography>
    )
}