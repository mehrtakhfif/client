import React, {useContext, useEffect} from "react";
import {Box, HiddenMdDown, tryIt, useState} from "material-ui-helper";
import {DEBUG} from "../../../pages/_app";
import ErrorBoundary from "../../base/ErrorBoundary";
import DeadlineTimer from "./DeadlineTimer";
import Wish from "../base/Wish";
import Share from "./Share";
import ActiveStorageContext from "../context/ActiveStorageContext";
import ProductDataContext from "../context/ProductDataContext";
import Name from "./Name";
import Storages from "./storages/Storages";
import RatingProduct from "../base/RatingProduct";
import ProductDisable from "../../product/ProductDisable";


function StoragePanel() {
    const [activeStorage] = useContext(ActiveStorageContext);
    const [productData] = useContext(ProductDataContext);
    const {id: productId, permalink, rate, wish, name, short_address} = productData || {};
    const [deadline, setDeadline] = useState()

    useEffect(() => {
        tryIt(() => {
            setDeadline(activeStorage?.deadline)
        })
    }, [activeStorage])


    return (
        <ErrorBoundary elKey={"StoragePanel::(component->productV2->storagePanel->StoragePanel.js)"}>
            <HiddenMdDown>
                <Box width={1} pl={10} pr={3} flexDirectionColumn={true}>
                    <ProductDisable/>
                    {
                        !productId ?
                            <StoragePanelPlaceholder/> :
                            <React.Fragment>
                                <Box width={1} display={'flex'} px={3} flexDirection={'column'}>
                                    <DeadlineTimer deadline={DEBUG ? (new Date().getTime() * 1.1 / 1000) : deadline}/>
                                    <Box pr={1} display={'flex'} alignCenter={true} pt={1}>
                                        <Share pr={1}/>
                                        <Wish mr={2}/>
                                        <RatingProduct/>
                                    </Box>
                                </Box>
                                <Name
                                    pt={{
                                        xs: 1,
                                        md: 2.5
                                    }}
                                    pb={4}
                                    px={3}/>
                                <Storages/>
                            </React.Fragment>
                    }
                    <ProductDisable/>
                </Box>
            </HiddenMdDown>
        </ErrorBoundary>
    )
}


function StoragePanelPlaceholder() {
    return <React.Fragment/>
}


export default React.memo(StoragePanel)