import React from "react";
import {Box, Typography} from "material-ui-helper";
import {colors, lang} from "../../../../../repository";


export default function Unavailable() {

    return (
        <Box>
            <Typography variant={'h4'} color={colors.textColor.he4254a}>
                {lang.get("unavailable")}
            </Typography>
        </Box>
    )
}