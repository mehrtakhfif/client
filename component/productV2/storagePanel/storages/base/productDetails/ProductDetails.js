import React from "react";
import {Box, HiddenLgUp} from "material-ui-helper";
import RatingProduct from "../../../../base/RatingProduct";
import StorageDetails from "./StorageDetials";
import Name from "../../../Name";
import {usActiveStorageContext} from "../../../../context/ActiveStorageContext";


export default function ProductDetails() {
    const [activeStorage] = usActiveStorageContext()

    return (
        <HiddenLgUp>
            <Box px={2} flexDirection={'column'}>
                <RatingProduct
                    pt={4}
                    readOnly={true}
                    dir={'ltr'}/>
                <Name mt={2}/>
                <StorageDetails data={activeStorage}/>
            </Box>
        </HiddenLgUp>
    )
}
