import React from "react";
import {Box, Typography} from "material-ui-helper";
import {colors, lang} from "../../../../../../repository";
import {useTheme} from "@material-ui/core";
import {UtilsFormat} from "../../../../../../utils/Utils";
import DiscountPercent from "../DiscountPercent";


export default function StorageDetails({data, ...props}) {
    const theme = useTheme()
    const {discount_percent, discount_price, final_price, max_count_for_sale} = data || {}

    return (
        max_count_for_sale > 0 &&
        <Box flexDirection={'column'}  {...props}>
            {
                discount_percent > 0 &&
                <Box
                    dir={"ltr"}
                    pt={{
                        xs: 0,
                        md: 3
                    }}>
                    <DiscountPercent
                        discountPercent={discount_percent}/>
                    <Box
                        pr={{
                            xs: 0.5,
                            md: 1.5
                        }}>
                        <Typography
                            alignItems={"center"}
                            component={"del"}
                            variant={{
                                xs: "h6",
                                md: "body1"
                            }}
                            fontWeight={"normal"} color={colors.textColor.h757575}>
                            {UtilsFormat.numberToMoney(final_price)}
                        </Typography>
                    </Box>
                </Box>
            }
            <Box dir={'ltr'}
                 pt={{
                     xs: 1,
                     md: 1.5
                 }} alignItems={"flex-end"}>
                <Box
                    pb={{
                        xs: 0.2,
                        md: 0.7
                    }}>
                    <Typography variant={"body2"} color={colors.textColor.h757575}>
                        {lang.get("toman")}
                    </Typography>
                </Box>
                <Box
                    pr={{
                        xs: 0.5,
                        md: 1
                    }}>
                    <Typography
                        variant={"h3"} fontWeight={"bold"}
                        alignItems={'flex-end'}>
                        {UtilsFormat.numberToMoney(discount_price)}
                    </Typography>
                </Box>
            </Box>
        </Box>
    )
}