import React, {useContext} from "react";
import {BottomAppBar, Box} from "material-ui-helper";
import StorageDataContext from "../../../../../context/StorageDataContext";
import BottomDrawer from "../../../../../../base/drawer/BottomDrawer";
import DrawerItemContainer from "../../../../../../base/drawer/DrawerItemContainer";
import {Toolbar} from "@material-ui/core";
import StorageItem from "../../../serviceStorage/storageItem/StorageItem";
import {Submit} from "../SubmitPanel";


export default function StoragesDrawable({open,onSubmit, onClose}) {
    const [storageData] = useContext(StorageDataContext);
    const {is_service, storage} = storageData || {}

    return (
        <BottomDrawer open={open} showBack={true} onClose={onClose}>
            {storage?.map((st, index) => (
                <DrawerItemContainer key={st?.id}>
                    <Box
                        width={1}
                        pl={{
                            xs: 2,
                            md: 2
                        }}
                        pr={2}
                        flexDirction={'column'}>
                        <StorageItem data={st} index={index}/>
                    </Box>
                </DrawerItemContainer>
            ))}
            <DrawerItemContainer>
                <Toolbar/>
                <Toolbar/>
                <Toolbar/>
            </DrawerItemContainer>
            <BottomAppBar py={1.5} px={2} elevation={1}>
                <Submit onSubmit={onSubmit}/>
            </BottomAppBar>
        </BottomDrawer>
    )
}