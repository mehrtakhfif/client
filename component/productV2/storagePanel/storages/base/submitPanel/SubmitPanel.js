import React, {useCallback, useContext, useMemo, useState} from "react";
import {Box, Button, HiddenLgUp, HiddenXsDown, tryIt, Typography, useOpenWithBrowserHistory} from "material-ui-helper";
import OrderCount from "./orderCount/OrderCount";
import {colors, lang} from "../../../../../../repository";
import {useTheme} from "@material-ui/core";
import CallbackProductContext from "../../../../context/ProductParamsContext";
import ProductParamsContext from "../../../../context/ProductParamsContext";
import _ from "lodash";
import ActiveStorageContext from "../../../../context/ActiveStorageContext";
import StorageDataContext from "../../../../context/StorageDataContext";
import StoragesDrawable from "./storagesDrawable/StoragesDrawable";
import ControllerUser from "../../../../../../controller/ControllerUser";
import {useIsLoginContext} from "../../../../../../context/IsLoginContextContainer";
import ProductDataContext from "../../../../context/ProductDataContext";
import {useLoginDialogContext} from "../../../../../../context/LoginDialogContextContainer";
import {DEBUG} from "../../../../../../pages/_app";
import ProductDisable from "../../../../../product/ProductDisable";

export default function SubmitPanel(props) {
    const [storageData] = useContext(StorageDataContext);
    const {is_service, storage} = storageData || {}
    const haveOneStorage = useMemo(() => {
        if (!is_service)
            return true
        let storagesLe = 0
        _.forEach(storage, (st) => {
            if (st?.max_count_for_sale > 0) {
                storagesLe = storagesLe + 1
            }
        })
        return storagesLe <= 1
    }, [storage])
    const [openStoragesDrawer, __, onOpenStoragesDrawer, onCloseStoragesDrawer] = useOpenWithBrowserHistory("submitPanel")

    const handleOnSubmit = useCallback(() => {
        // onCloseStoragesDrawer()
    }, [openStoragesDrawer])


    return (
        <Box width={1} flexDirection={'column'} {...props}>
            <HiddenXsDown>
                <Submit px={{xs: 2, md: 0}} onSubmit={handleOnSubmit}/>
            </HiddenXsDown>
            <HiddenLgUp>
                <ProductDisable/>
                {
                    (is_service && !haveOneStorage) ?
                        <BaseButton onClick={onOpenStoragesDrawer}>
                            {lang.get("view_and_buy")}
                        </BaseButton> :
                        <Submit px={{xs: 2, md: 0}} onSubmit={handleOnSubmit}/>
                }
            </HiddenLgUp>
            {
                is_service &&
                <StoragesDrawable
                    onSubmit={handleOnSubmit}
                    open={openStoragesDrawer}
                    onClose={onCloseStoragesDrawer}/>
            }
        </Box>
    )
}

export function Submit({onSubmit: onSub, ...props}) {
    const isLogin = useIsLoginContext()
    const [__, ___, handleOpenLoginDialog] = useLoginDialogContext()


    const [loading, setLoading] = useState(false)
    const {onSubmit} = useContext(CallbackProductContext);
    const [product] = useContext(ProductDataContext);
    const [activeStorage] = useContext(ActiveStorageContext);
    const isAvailable = activeStorage?.max_count_for_sale > 0
    const {discount_price} = activeStorage || {}

    function handleOnClick() {
        if (!isAvailable)
            return
        onSubmit()
        tryIt(() => {
            onSub()
        })
    }


    return (
        isAvailable ?
            <Box width={1} {...props}>
                <OrderCount/>
                <Box flex={1}>
                    <SubmitButton onClick={handleOnClick}>
                        {lang.get("add_to_cart")}
                    </SubmitButton>
                </Box>
            </Box> :
            DEBUG ?
                <Box
                    width={1}
                    justifyContent={{
                        xs: 'center',
                        md: 'flex-start'
                    }}>
                    <Button
                        disabled={loading}
                        disableElevation={true}
                        color={colors.backgroundColor.ha9a9a9}
                        typography={{
                            variant: "subtitle1",
                            color: colors.textColor.hffffff
                        }}
                        onClick={() => {
                            if (!isLogin) {
                                handleOpenLoginDialog()
                                return
                            }
                            setLoading(true)
                            ControllerUser.User.Profile.Wishlist.update({
                                productId: product?.id,
                                wish: true,
                                notify: true
                            }).finally(() => {
                                setLoading(false)
                            })
                        }}>
                        {lang.get("let_me_know")}
                    </Button>
                </Box> :
                <React.Fragment/>
    )
}

function SubmitButton({onClick, ...props}) {
    const [activeStorage] = useContext(ActiveStorageContext)
    const isAvailable = activeStorage?.max_count_for_sale !== 0

    return (
        <BaseButton disabled={!isAvailable} onClick={onClick}>
            {props?.children}
        </BaseButton>
    )
}

function BaseButton({disabled, onClick, ...props}) {
    const theme = useTheme();
    const {loading} = useContext(ProductParamsContext)

    return (
        <Button
            loading={loading}
            disableElevation={true}
            disabled={disabled} fullWidth={true}
            color={theme.palette.primary.main} onClick={onClick}>
            <Box py={1}>
                <Typography
                    variant={{
                        xs: 'h6',
                        md: "subtitle1"
                    }}
                    color={"#fff"}>
                    {props.children}
                </Typography>
            </Box>
        </Button>
    )
}

