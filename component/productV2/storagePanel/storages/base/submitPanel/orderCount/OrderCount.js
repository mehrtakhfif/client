import React, {useContext, useEffect} from "react";
import {
    Box,
    Button,
    ButtonBase,
    HiddenLgUp,
    HiddenMdDown,
    tryIt,
    Typography,
    useOpenWithBrowserHistory,
    useState
} from "material-ui-helper";
import {Menu, useTheme} from "@material-ui/core";
import {lang} from "../../../../../../../repository";
import MtIcon from "../../../../../../MtIcon";
import SmOrderCount from "./SmOrderCount";
import ActiveStorageContext from "../../../../../context/ActiveStorageContext";
import OrderCountContext from "../../../../../context/OrderCountContext";
import ProductParamsContext from "../../../../../context/ProductParamsContext";


export default function OrderCount() {

    const [orderCount, setOrderCount] = useContext(OrderCountContext)
    const [activeStorage] = useContext(ActiveStorageContext)
    const {max_count_for_sale,min_count_for_sale} = activeStorage || {};
    const theme = useTheme()
    const {loading} = useContext(ProductParamsContext)
    const [open, setOpen] = useState(false)
    const [openOrderCount, __, onOpenOrderCount, onCloseOrderCount] = useOpenWithBrowserHistory("OrderCount")



    useEffect(() => {
        if (max_count_for_sale < orderCount) {
            setOrderCount(max_count_for_sale || 1)
        }
    }, [activeStorage])

    function handleButtonClick(event) {
        tryIt(() => {
            if (loading)
                return
            if (event) {
                setOpen(event.currentTarget)
                return;
            }
            onOpenOrderCount()
        })
    }

    function handleItemSelect(count) {
        setOrderCount(count)
        handleClose()
    }

    function handleClose() {
        tryIt(() => {
            if (open)
                setOpen(false)
            if (openOrderCount)
                onCloseOrderCount()
        })
    }


    // if (max_count_for_sale <= 1)
    //     return <React.Fragment/>




    return (
        <Box pr={1}>
            <HiddenLgUp>
                <Button variant={"outlined"} onClick={() => handleButtonClick()}
                        buttonProps={{style: {padding: 0}}}>
                    <Box py={1.6} px={1.5} alignCenter={true}>
                        <NumberItem number={orderCount}/>
                        <Box pl={1}>
                            <MtIcon
                                icon={"mt-chevron-down"}
                                style={{
                                    fontSize: theme.spacing(2.5)
                                }}/>
                        </Box>
                    </Box>
                </Button>
                <SmOrderCount
                    open={openOrderCount}
                    onItemSelect={handleItemSelect}
                    onClose={handleClose}/>
            </HiddenLgUp>
            <HiddenMdDown>
                <Button variant={"outlined"}
                        onClick={(e) => handleButtonClick(e)}
                        buttonProps={{style: {padding: 0}}}>
                    <Box py={1.6} px={1.5} alignCenter={true}>
                        <NumberItem number={orderCount}/>
                        <Box pl={1}>
                            <MtIcon
                                icon={"mt-chevron-down"}
                                style={{
                                    fontSize: theme.spacing(2.5)
                                }}/>
                        </Box>
                    </Box>
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={open}
                    keepMounted
                    open={Boolean(open)}
                    onClose={handleClose}>
                    {
                        [...Array(max_count_for_sale-min_count_for_sale+1).keys()].map(n => {
                            const num = n + min_count_for_sale;
                            const handleClick = () => {
                                handleItemSelect(num)
                            }

                            return (
                                <Box key={num}>
                                    <ButtonBase onClick={handleClick}>
                                        <NumberItem
                                            px={2}
                                            py={1}
                                            isItems={true} number={num}/>
                                    </ButtonBase>
                                </Box>
                            )
                        })
                    }
                </Menu>
            </HiddenMdDown>
        </Box>
    )
}

function NumberItem({number, isItems = false, ...props}) {
    const theme = useTheme();
    return (
        <Typography
            {...props}
            fontWeight={"normal"}
            alignItems={'center'}
            variant={{
                xs: "body1",
                md: "body2"
            }}>
            <Typography
                variant={"h6"}
                fontWeight={"normal"}
                pr={0.5}
                style={{
                    justifyContent: isItems ? 'flex-end' : 'center',
                    minWidth: theme.spacing(2.8)
                }}>
                {number}
            </Typography>
            {lang.get("number")}
        </Typography>
    )
}