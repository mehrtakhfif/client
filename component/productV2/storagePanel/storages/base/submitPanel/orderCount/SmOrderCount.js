import React, {useContext} from "react";
import ActiveStorageContext from "../../../../../context/ActiveStorageContext";
import {Box, ButtonBase, gLog, Typography, UtilsStyle} from "material-ui-helper";
import BottomDrawer from "../../../../../../base/drawer/BottomDrawer";
import {useTheme} from "@material-ui/core";
import {colors} from "../../../../../../../repository";
import DrawerItemContainer from "../../../../../../base/drawer/DrawerItemContainer";


export default function SmOrderCount({open, removable, onRemove, onItemSelect, onClose}) {
    const [activeStorage] = useContext(ActiveStorageContext)
    const {min_count_for_sale, max_count_for_sale} = activeStorage || {};



    return (
        <BottomDrawer open={open} onClose={onClose}>
            <Box component={'ul'} px={2} pb={4}>
                {
                    removable &&
                    <Item variant={"body1"}>
                        حذف
                    </Item>
                }

                {
                    [...Array(max_count_for_sale - min_count_for_sale + 1).keys()].map(n => {
                        const num = n + min_count_for_sale;
                        const handleClick = () => {
                            onItemSelect(num)
                        }

                        return (
                            <Item key={n} isItems={true} number={num} onClick={handleClick}>
                                {num}
                            </Item>
                        )
                    })
                }
            </Box>
        </BottomDrawer>
    )
}


function Item({variant = "h5", onClick, ...props}) {
    const theme = useTheme();
    return (
        <DrawerItemContainer>
            <Box px={1}>
                <ButtonBase onClick={onClick}>
                    <Box
                        center={true}
                        minWidth={theme.spacing(8)}
                        minHeight={theme.spacing(8)}
                        style={{
                            border: `1px solid ${colors.borderColor.he5e5e5}`,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Typography
                            variant={variant}
                            fontWeight={"normal"}
                            style={{
                                justifyContent: 'center',
                                minWidth: theme.spacing(2.8)
                            }}>
                            {props?.children}
                        </Typography>
                    </Box>
                </ButtonBase>
            </Box>
        </DrawerItemContainer>
    )
}