import React from "react";
import {Box, Typography, UtilsStyle} from "material-ui-helper";
import {useTheme} from "@material-ui/core";


export default function DiscountPercent({discountPercent,...props}){
    const theme = useTheme();

    return(
        <Box
            py={{
                xs: 0,
                md: 0.5
            }}
            px={{
                xs: 1,
                md: 1.75
            }}
            {...props}
            style={{
                backgroundColor: theme.palette.primary.main,
                ...UtilsStyle.borderRadius(5),
                ...props?.style
            }}>
            <Typography
                variant={{
                    xs: "h6",
                    md: "body1"
                }}
                alignItems={'center'}
                fontWeight={"normal"}
                color={"#fff"}>
                {discountPercent}%
            </Typography>
        </Box>
    )
}