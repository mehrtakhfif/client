import React, {useContext} from "react";
import ErrorBoundary from "../../../../../base/ErrorBoundary";
import {Box, ButtonBase, IconButton, Typography, UtilsStyle} from "material-ui-helper";
import {RadioButtonChecked, RadioButtonUnchecked} from "@material-ui/icons";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {colors, lang} from "../../../../../../repository";
import clsx from "clsx";
import {useTheme} from "@material-ui/core";
import MtIcon from "../../../../../MtIcon";
import StorageDetails from "../../base/productDetails/StorageDetials";
import ActiveStorageContext from "../../../../context/ActiveStorageContext";


const useStyles = makeStyles((theme) => ({
    serviceStorageItemRoot: {
        border: `1px solid ${colors.borderColor.ha9a9a9}`,
        ...UtilsStyle.borderRadius(5)
    },
    serviceStorageItemActive: {
        borderColor: theme.palette.secondary.main
    },
    serviceStorageItemUnAvailable: {
        backgroundColor: colors.backgroundColor.hf8f8f8
    }
}));

function StorageItem({data, index}) {
    const classes = useStyles()
    const theme = useTheme()
    const {id, title, discount_percent, final_price, discount_price, max_count_for_sale} = data
    const isAvailable = max_count_for_sale !== 0
    const [activeStorage, setActiveStorage] = useContext(ActiveStorageContext)
    const isActive = activeStorage?.id === data?.id && isAvailable

    function handleClick() {
        if (!isAvailable || !data?.id)
            return
        setActiveStorage(data)
    }

    return (
        <React.Fragment>
            <ErrorBoundary
                elKey={"StorageItemLg::(component->productV2->storagePanel->storages->serviceStorage->storageItem->StorageItemLg.js)"}>
                <Box width={1} mt={3} display={'flex'} alignItems={"center"} onClick={handleClick}>
                    <Box pr={1}>
                        <IconButton
                            size="small"
                            disabled={!isAvailable}
                            onClick={(e) => {
                                handleClick()
                                e.stopPropagation()
                            }}>
                            {
                                isActive ?
                                    <RadioButtonChecked color={"secondary"}/> :
                                    <RadioButtonUnchecked
                                        style={{
                                            color: isAvailable? colors.borderColor.ha9a9a9:colors.borderColor.hc9c9c9
                                        }}/>
                            }
                        </IconButton>
                    </Box>
                    <ButtonBase
                        disableRipple={!isAvailable}
                        onClick={(e) => {
                            handleClick()
                            e.stopPropagation()
                        }}
                        width={1}
                        style={{
                            cursor: !isAvailable ? "default" : undefined
                        }}>
                        <Box
                            className={clsx([
                                classes.serviceStorageItemRoot,
                                isActive ? classes.serviceStorageItemActive : '',
                                !isAvailable ? classes.serviceStorageItemUnAvailable : ''
                            ])}
                            py={2}
                            px={{
                                xs: 2,
                                md: 3
                            }}
                            flex={1}
                            flexDirectionColumn={true}>
                            <Typography
                                component={"h4"}
                                variant={{
                                    xs: "subtitle1",
                                    md: "subtitle2"
                                }}>
                                {title}
                            </Typography>
                            {
                                isAvailable ?
                                    <React.Fragment>
                                        <StorageDetails data={data}/>
                                    </React.Fragment> :
                                    <React.Fragment>
                                        <Box py={{
                                            xs: 1,
                                            md: 3
                                        }}>
                                            <Typography variant={"subtitle1"} color={colors.textColor.h757575}>
                                                {lang.get("the_end")}
                                            </Typography>
                                        </Box>
                                        <Box
                                            alignItems={'center'}
                                            onClick={() => {
                                                //Todo let me know dialog if logout first login
                                            }}
                                            style={{
                                                cursor: "pointer"
                                            }}>
                                            <Typography
                                                alignItems={'center'}
                                                variant={"subtitle2"}
                                                color={theme.palette.secondary.main}>
                                                {lang.get("let_me_know")}
                                            </Typography>
                                            <MtIcon
                                                color={theme.palette.secondary.main}
                                                icon={"mt-chevron-left"}
                                                style={{
                                                    marginRight: theme.spacing(1),
                                                    fontSize: theme.spacing(3)
                                                }}/>
                                        </Box>
                                    </React.Fragment>
                            }
                        </Box>
                    </ButtonBase>
                </Box>
            </ErrorBoundary>
        </React.Fragment>
    )
}

export default React.memo(StorageItem)