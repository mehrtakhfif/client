import React from "react";
import ErrorBoundary from "../../../../../base/ErrorBoundary";
import StorageItemLg from "./StorageItemLg";


function StorageItem({data, index}) {
    return (
        <React.Fragment>
            <ErrorBoundary
                elKey={"StorageItem::(component->productV2->storagePanel->storages->serviceStorage->storageItem->StorageItem.js)"}>
                <StorageItemLg data={data} index={index}/>
            </ErrorBoundary>
        </React.Fragment>
    )
}

export default React.memo(StorageItem)