import React from "react";
import ErrorBoundary from "../../../../base/ErrorBoundary";
import {HiddenLgUp, HiddenXsDown} from "material-ui-helper";
import ServiceStorageLg from "./ServiceStorageLg";
import ServiceStorageSm from "./ServiceStorageSm";

function ServiceStorage() {

    return (
        <React.Fragment>
            <ErrorBoundary
                elKey={"ServiceStorage::(component->productV2->storagePanel->storages->serviceStorage->ServiceStorage.js)"}>
                <HiddenLgUp>
                    <ServiceStorageSm/>
                </HiddenLgUp>
                <HiddenXsDown>
                    <ServiceStorageLg/>
                </HiddenXsDown>
            </ErrorBoundary>
        </React.Fragment>
    )
}

export default React.memo(ServiceStorage)