import React from "react";
import {BottomAppBar, HiddenLgUp} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import SubmitPanel from "../base/submitPanel/SubmitPanel";


export default function ServiceStorageSm() {
    const theme = useTheme()

    return (
        <React.Fragment>
            <HiddenLgUp>
                <Toolbar style={{marginTop: theme.spacing(3)}}/>
                <BottomAppBar>
                    <SubmitPanel py={1}/>
                </BottomAppBar>
            </HiddenLgUp>
        </React.Fragment>
    )
}