import React, {useContext, useEffect, useMemo} from "react";
import ErrorBoundary from "../../../../base/ErrorBoundary";
import {Box, getSafe, tryIt, Typography, useState} from "material-ui-helper";
import _ from "lodash"
import StorageItem from "./storageItem/StorageItem";
import {lang} from "../../../../../repository";
import {Collapse, Divider, useTheme} from "@material-ui/core";
import SubmitPanel from "../base/submitPanel/SubmitPanel";
import StorageDataContext from "../../../context/StorageDataContext";
import ActiveStorageContext from "../../../context/ActiveStorageContext";
import Unavailable from "../base/Unavailable";

function ServiceStorage() {
    const theme = useTheme();

    const [active] = useContext(ActiveStorageContext);
    const [storageData] = useContext(StorageDataContext);
    const {storage: da} = storageData
    const [showAll, setShowAll] = useState(isBigList());
    const activeStorage = useMemo(() => {
        if (!active)
            return {}
        return getSafe(() => {
            const item = _.find(da, st => st.id === active)
            return item || {}
        }, {})
    }, [da, active]);
    const {max_count_for_sale} = activeStorage || {};
    const [data1, data2] = useMemo(() => {
        const isBL = isBigList()
        return [
            isBL ? da.slice(0, 3) : da,
            isBL ? da.slice(3, da.length) : []
        ]
    }, [da, showAll])

    if (_.isEmpty(da))
        return <React.Fragment/>

    function isBigList() {
        return da?.length - 4 >= 0
    }

    useEffect(() => {
        tryIt(() => {
            setShowAll(da.length - 4 <= 0)
        })
    }, [da]);


    return (
        <React.Fragment>
            <ErrorBoundary
                elKey={"ServiceStorage::(component->productV2->storagePanel->storages->serviceStorage->ServiceStorageLg.js)"}>
                <Box pr={3} flexDirectionColumn={true}>
                    {
                        data1.map((st, index) => (
                            <StorageItem
                                key={st.id}
                                index={index}
                                data={st}/>
                        ))
                    }
                    <Collapse in={showAll}>
                        {
                            data2.map((st, index) => (
                                <StorageItem
                                    key={st.id}
                                    index={index}
                                    data={st}/>
                            ))
                        }
                    </Collapse>
                    {
                        !showAll &&
                        <Box width={1} pt={3} justifyContent={'center'}>
                            <Typography
                                color={theme.palette.secondary.main}
                                variant={"body1"} py={1} px={2}
                                textAlign={"center"}
                                fontWeight={"normal"}
                                onClick={() => {
                                    setShowAll(true)
                                }}
                                style={{
                                    cursor: 'pointer'
                                }}>
                                {lang.get("see_all_items_item", {
                                    args: {
                                        item: _.toString(da?.length - 3)
                                    }
                                })}
                            </Typography>
                        </Box>
                    }
                    {
                        !showAll &&
                        <Box flexDirectionColumn={true} width={1} pt={2.5}>
                            <Divider/>
                        </Box>
                    }

                    {
                        _.isEmpty(data1)&&
                            <Unavailable/>
                    }

                    <Box width={1}>
                        <SubmitPanel pt={2.5}/>
                    </Box>
                </Box>
            </ErrorBoundary>
        </React.Fragment>
    )
}

export default React.memo(ServiceStorage)