import React from "react";
import ErrorBoundary from "../../../../base/ErrorBoundary";
import {HiddenLgUp, HiddenXsDown} from "material-ui-helper";
import ProductStorageLg from "./ProductStorageLg";
import ProductStorageSm from "./ProductStorageSm";


export default function ProductStorage() {

    return (
        <React.Fragment>
            <ErrorBoundary
                elKey={"ProductStorage::(component->productV2->storagePanel->storages->productStorage->ProductStorage.js)"}>
                <HiddenLgUp>
                    <ProductStorageSm/>
                </HiddenLgUp>
                <HiddenXsDown>
                    <ProductStorageLg/>
                </HiddenXsDown>
            </ErrorBoundary>
        </React.Fragment>
    )
}