import React, {useContext} from "react";
import {Box, FullWidthSkeleton, Typography} from "material-ui-helper";
import {colors, lang} from "../../../../../repository";
import {UtilsFormat} from "../../../../../utils/Utils";
import DiscountPercent from "../base/DiscountPercent";
import FeatureItem from "./FeatureItem";
import SubmitPanel from "../base/submitPanel/SubmitPanel";
import StorageDataContext from "../../../context/StorageDataContext";
import MaxShippingTime from "../../MaxShippingTime";


export default function () {
    const [st, ___, loading] = useContext(StorageDataContext);
    const {features, storage} = st;

    const {final_price, discount_percent, discount_price,max_shipping_time, max_count_for_sale} = storage;

    return (
        <Box
            flexDirection={'column'}
            px={3}>
            {
                final_price ?
                    <Box
                        p={1}
                        style={{position: "relative"}}>
                        {
                            discount_percent > 0 &&
                            <React.Fragment>
                                <Typography
                                    color={colors.textColor.h757575}
                                    component={'del'}
                                    variant={"subtitle1"}
                                    fontWeight={400}>
                                    {UtilsFormat.numberToMoney(final_price)}
                                    {"  " + lang.get("toman")}
                                </Typography>
                                <DiscountPercent ml={1} discountPercent={discount_percent}/>
                            </React.Fragment>
                        }
                        {
                            loading &&
                            <FullWidthSkeleton
                                variant={"rect"}
                                style={{}}/>
                        }
                    </Box>:
                    <React.Fragment/>
            }
            {
                discount_price !== undefined &&
                    <Box
                        p={1}
                        style={{position: "relative"}}>
                        <Typography variant={"h2"} fontWeight={'normal'}>
                            {UtilsFormat.numberToMoney(discount_price)}
                        </Typography>
                        <Typography variant={"subtitle2"} fontWeight={'normal'} alignItems={'flex-end'} pb={0.5} pl={1}>
                            {lang.get("toman")}
                        </Typography>
                        {
                            loading &&
                            <FullWidthSkeleton
                                variant={"rect"}
                                style={{}}/>
                        }
                    </Box>
            }

            {
                features?.map(fe => {
                    return (
                        <FeatureItem
                            key={fe?.id}
                            data={fe}/>
                    )
                })
            }
            <MaxShippingTime value={max_shipping_time}/>
            <Box width={1}>
                <SubmitPanel pt={2.5}/>
            </Box>
        </Box>
    )
}


