import React, {useContext} from "react";
import {Box, ButtonBase, Typography} from "material-ui-helper";
import ErrorBoundary from "../../../../base/ErrorBoundary";
import {useTheme} from "@material-ui/core";
import {colors} from "../../../../../repository";
import {UtilsStyle} from "../../../../../utils/Utils";
import ActiveFeatureContext from "../../../context/ActiveFeatureContext";

export default function FeatureItem({data}) {

    const theme = useTheme()
    const [activeFeature, onActiveFeatureChange] = useContext(ActiveFeatureContext);

    const {id: featureId, name, values, settings, selected} = data || {}


    function handleClick(item) {
        onActiveFeatureChange(item, data)
    }

    if (!data)
        return <React.Fragment/>


    return (
        <ErrorBoundary elKey={"FeatureItem::(component->productV2->context->FeatureItem.js)"}>
            <Box pt={4} flexDirectionColumn={true}>
                <Typography pb={1} fontWeight={400} variant={'subtitle1'}>
                    {name}
                </Typography>
                <Box width={1} flexWrap={'wrap'}>
                    {
                        values?.map((item, index) => {
                            const {id, name, selectable, available} = item
                            const first = index % 3 === 0;
                            const last = !first && (index + 1) % 3 === 0;
                            const isSelected = activeFeature[featureId] === id;

                            return (
                                <Box width={0.3} pb={2} pr={last ? 0 : first ? 2 : 1} pl={first ? 0 : last ? 2 : 1}>
                                    <ButtonBase
                                        width={1}
                                        disabled={!available}
                                        onClick={() => handleClick(item)}>
                                        <Box
                                            py={1}
                                            width={1}
                                            center={true}
                                            style={{
                                                backgroundColor: !available ? colors.backgroundColor.heff0ef : undefined,
                                                border: `${isSelected ? 2:1}px solid ${available ?(isSelected ? theme.palette.secondary.main : colors.borderColor.ha9a9a9):'transparent'}`,
                                                ...UtilsStyle.borderRadius(5)
                                            }}>
                                            <Typography
                                                variant={"subtitle1"}
                                                fontWeight={'normal'}
                                                color={!available ? colors.textColor.h9e9e9e : colors.textColor.h000000}>
                                                {name}
                                            </Typography>
                                        </Box>
                                    </ButtonBase>
                                </Box>
                            )
                        })
                    }
                </Box>
            </Box>
        </ErrorBoundary>
    )
}
