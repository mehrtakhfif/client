import React, {useContext} from "react";
import ErrorBoundary from "../../../base/ErrorBoundary";
import _ from "lodash"
import ProductStorage from "./productStorage/ProductStorage";
import ServiceStorage from "./serviceStorage/ServiceStorage";
import StorageDataContext from "../../context/StorageDataContext";

function Storages() {
    const [storageData] = useContext(StorageDataContext);

    if (_.isEmpty(storageData))
        return <React.Fragment/>

    const {is_service} = storageData || {};

    return (
        <React.Fragment>
            <ErrorBoundary elKey={"Storages::(component->productV2->storagePanel->storages->Storages.js)"}>
                {
                    !is_service ?
                        <ProductStorage/> :
                        <ServiceStorage/>
                }
            </ErrorBoundary>
        </React.Fragment>
    )
}


export default React.memo(Storages)