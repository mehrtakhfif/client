import React, {useContext} from "react";
import ErrorBoundary from "../../base/ErrorBoundary";
import Typography from "../../base/Typography";
import {Box} from "material-ui-helper";
import MtIcon from "../../MtIcon";
import {colors} from "../../../repository";
import ProductDataContext from "../context/ProductDataContext";


export default function Name(props) {

    const [productData] = useContext(ProductDataContext);
    const {name, short_address} = productData || {};

    return (
        <ErrorBoundary elKey={"Name::(component->productV2->stragePanel->Name.js)"}>
            <Box display={'flex'} flexDirection={'column'}   {...props}>
                <Typography component={"h1"} px={1} variant={"h6"} fontWeight={500}>
                    {name}
                </Typography>
                {
                    short_address &&
                    <Typography pt={0.5} pl={"12px"} color={colors.textColor.h757575} alignItems={"center"}
                                variant={"body2"} fontWeight={400}>
                        <MtIcon color={colors.textColor.h757575} icon={"mt-map-marker-alt"}/>
                        {short_address}
                    </Typography>
                }
            </Box>
        </ErrorBoundary>
    )
}