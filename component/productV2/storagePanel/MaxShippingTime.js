import React from "react";
import {Box, Typography} from "material-ui-helper";
import {UtilsStyle} from "../../../utils/Utils";
import Img from "../../base/oldImg/Img";



export default function MaxShippingTime({value, ...props}) {

    const time = (() => {
        if (value < 24) {
            return value
        }
        return Math.ceil(value / 24)
    })()

    return (
        value && value > 1 ?
            <Box mt={1} display={'flex'} py={1} alignItems={'center'} justifyContent={'center'} width={1} {...props}>
                <Typography
                    px={2}
                    py={2.5}
                    alignItems={'center'}
                    variant={"body1"}
                    fontWeight={500}
                    style={{
                        width: '100%',
                        backgroundColor: "#F2F2F2",
                        ...UtilsStyle.borderRadius(5)
                    }}>
                    <Img width={35} mr={1.5} src={'/drawable/svg/truck-icon.svg'}
                         alt={'Truck Icon'}/>
                    ارسال
                    ({
                    value < 24 ?
                        `پس از ` + time + " ساعت" :
                        'از ' + time + ' روز کاری'
                })
                </Typography>
            </Box> :
            <React.Fragment/>
    )
}