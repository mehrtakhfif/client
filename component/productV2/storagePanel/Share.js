import React, {useContext, useMemo, useState} from "react";
import Tooltip from "../../base/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {grey} from "@material-ui/core/colors";
import {Box, ButtonBase, HiddenLgUp, HiddenMdDown, Typography, Utils} from "material-ui-helper";
import ErrorBoundary from "../../base/ErrorBoundary";
import MtIcon from "../../MtIcon";
import {lang} from "../../../repository";
import ProductDataContext from "../context/ProductDataContext";
import rout from "../../../router";
import Sentry from "../../../lib/Sentry"
import {Popover} from "@material-ui/core";


export default function Share(props) {
    const [open, setOpen] = useState(undefined)
    const [product] = useContext(ProductDataContext);
    const {id} = product || {};

    const shareRout = useMemo(()=>{
        return rout.Product.SingleV2.as(id, {isShortLink: true})
    },[id])


    function handleOnClick(e) {
        setOpen(e.currentTarget);
    }

    async function handleOnClickSm() {
        try {
            if (!navigator?.share)
                throw "navigator not found"
            await navigator.share({
                title: lang.get("mehr_takhfif"),
                text: '',
                url: shareRout,
            })
        } catch (err) {
            Sentry.error(`error on share product in mobile. id => ${id} || error => ${err}`)
        }
    }


    return (
        <ErrorBoundary elKey={"Share::(component->productV2->storagePanel->Share.js)"}>
            <Box {...props}>
                <HiddenLgUp>
                    <Button onClick={handleOnClickSm}/>
                </HiddenLgUp>
                <HiddenMdDown>
                    <Button onClick={handleOnClick}/>
                    <Popover
                        anchorEl={open}
                        open={Boolean(open)}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'center',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'center',
                        }}
                        onClose={()=>setOpen(undefined)}
                    style={{
                        marginTop:5
                    }}>
                        <Box flexDirectionColumn={true}>
                            <CustomMenuItem
                                icon={"mt-telegram"}
                                title={lang.get("telegram")}
                                onClick={() => {
                                    window.open(`tg://msg_url?url=${shareRout}&text=${lang.get('mehr_takhfif')}`)
                                }}/>
                            <CustomMenuItem
                                icon={"mt-whatsapp"}
                                title={lang.get("whatsapp")}
                                onClick={() => {
                                        window.open(`https://wa.me/?text=${lang.get('mehr_takhfif')} \n${shareRout}`)

                                }}/>
                            <CustomMenuItem
                                icon={"mt-twitter"}
                                title={lang.get("twitter")}
                                onClick={() => {
                                        window.open( `https://twitter.com/intent/tweet?url=${lang.get('mehr_takhfif')} \n${shareRout}`)
                                }}/>
                            <CustomMenuItem
                                icon={"mt-copy"}
                                title={lang.get("copy_to_clipboard")}
                                onClick={() => {
                                    setOpen(false)
                                    setTimeout(()=>{
                                        Utils.copyToClipboard(shareRout)
                                    },1000)
                                }}/>
                        </Box>
                    </Popover>
                </HiddenMdDown>
            </Box>
        </ErrorBoundary>
    )
}

function Button({onClick}) {
    return (
        <Tooltip title={lang.get("share")}>
            <IconButton onClick={onClick} size={"small"}>
                <MtIcon icon={"mt-share"} color={grey[500]}/>
            </IconButton>
        </Tooltip>
    )
}

function CustomMenuItem({icon, title, onClick}) {
    return (
        <ButtonBase width={1} onClick={onClick}>
            <Box width={1} px={3} py={2}>
                <MtIcon icon={icon}/>
                <Typography pl={2} variant={'body1'}>
                    {title}
                </Typography>
            </Box>
        </ButtonBase>
    )
}
