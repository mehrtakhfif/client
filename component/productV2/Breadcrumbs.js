import React from "react";
import {Box, HiddenMdDown, Typography} from "material-ui-helper";
import {DEBUG} from "../../pages/_app";


export default function Breadcrumbs() {

    if (true ||!DEBUG)
        return <React.Fragment/>

    return (
        <HiddenMdDown>
            <Box px={10} pb={4}>
                <Typography variant={"h6"}>
                    مهرتخفیف > پوشاک ورزشی مردانه > تیشرت باشگاهی
                </Typography>
            </Box>
        </HiddenMdDown>
    )
}