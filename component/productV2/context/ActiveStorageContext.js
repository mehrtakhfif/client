import React, {useContext} from "react";

export const defaultActiveStorageContext = undefined;

const ActiveStorageContext =  React.createContext(defaultActiveStorageContext)
export default ActiveStorageContext;



export function usActiveStorageContext(){
    return useContext(ActiveStorageContext)
}
