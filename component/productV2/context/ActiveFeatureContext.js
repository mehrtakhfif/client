import React from "react";

export const defaultActiveFeatureContext = {};

export default React.createContext(defaultActiveFeatureContext)

