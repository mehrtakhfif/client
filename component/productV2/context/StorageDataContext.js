import React from "react";

export const defaultStorageDataContext = [undefined,undefined];

export default React.createContext(defaultStorageDataContext)

