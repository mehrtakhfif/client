import {Box} from "material-ui-helper";
import React from "react";

export default function GallerySkeleton() {
    return (
        <Box width={1} pl={3} pr={10}>
            <Box width={"79%"}>

            </Box>
            <Box width={"21%"}>

            </Box>
        </Box>
    )
}