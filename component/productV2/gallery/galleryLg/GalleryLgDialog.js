import React, {useEffect, useState} from "react";
import MaterialBox from "@material-ui/core/Box"
import {Box, Dialog, IconButton, tryIt, UtilsStyle} from "material-ui-helper";
import _ from "lodash"
import {colors, icons, media as mediaSize} from "../../../../repository";
import MtIcon from "../../../MtIcon";
import {Swiper, SwiperSlide} from 'swiper/react';
import {useTheme} from "@material-ui/core";
import SwiperCore, {Navigation, Thumbs} from "swiper/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

SwiperCore.use([Navigation, Thumbs]);


const useStyle = makeStyles(theme => ({
    sectionSlider: {
        "& .swiper-button-prev": {
            left: "10%"
        },
        "& .swiper-button-next": {
            right: "10%"
        },
        "& .swiper-button-next::after , & .swiper-button-prev::after": {
            fontFamily: "'mt' !important",
            background: colors.backgroundColor.heff0ef,
            padding: theme.spacing(2),
            color: colors.textColor.h404040,
            fontSize: "2.083vw"
        },
        "& .swiper-button-prev::after": {
            content: icons.mtChevronRight,
        },
        "& .swiper-button-next::after": {
            content: icons.mtChevronLeft,
        },
        "& .swiper-button-disabled::after": {
            color: colors.textColor.h757575
        }
    },
}));


let lastSwiperIndex = null
export default function GalleryLgDialog({open, activeImage, data, onClose}) {
    const classes = useStyle()
    const theme = useTheme();
    const [init, setInit] = useState(open)
    const [active, setActive] = useState()
    const [swiper, setSwiper] = useState(null);
    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    const slideTo = (index) => tryIt(() => swiper.slideTo(index));

    useEffect(() => {
        setTimeout(() => {
            const index = _.findIndex(data, d => d.id === activeImage)
            if (index !== -1) {
                setActive(index)
                slideTo(index)
            }
        }, 300)
    }, [activeImage])

    useEffect(() => {
        lastSwiperIndex = active
        if (!lastSwiperIndex)
            return
        if (lastSwiperIndex < active) {
            thumbsSwiper?.slideTo(active)
            return;
        }
        thumbsSwiper?.slideTo(active === 0 ? active : active - 1)
    }, [active])

    function handleNext() {
        // try {
        //     gLog("sklfjkajskfja", {firstSwiper,secondSwiper})
        //     // swiperClass.activeIndex = 1
        // } catch (e) {
        //     gError("sklfjkajskfja", e)
        // }


        tryIt(() => {
            // Swiper.slideNext()
        })
        tryIt(() => {
            // ref?.current?.slideNext()
        })
    }

    function handlePrev() {
        // ref?.current?.slidePrev()
    }

    useEffect(() => {
        if (open && !init)
            setInit(true)
    }, [open])


    return (
        <Dialog
            open={Boolean(open)}
            fullScreen={true}
            onBackdropClick={onClose}
            onClose={onClose}
            closeElement={(onClick) => (
                <IconButton onClick={onClick}>
                    <MtIcon icon={"mt-close"}/>
                </IconButton>
            )}
            headerProps={{
                style: {
                    top: 50,
                    left: 20,
                    zIndex:999
                }
            }}>
            {init &&
            <Box width={1} flexDirection={'column'}>
                <Box width={1} pt={8} center={true}>
                    <Box className={classes.sectionSlider} width={1} center={true}>
                        <Swiper
                            tag="section"
                            freeMode={false}
                            thumbs={{swiper: thumbsSwiper}}
                            wrapperTag="ul"
                            spaceBetween={0}
                            slidesPerView={1}
                            navigation
                            onSwiper={setSwiper}
                            onSlideChange={(swiper) => {
                                console.log('Slide index changed to: ', swiper.activeIndex);
                            }}
                            onReachEnd={() => console.log('Swiper end reached')}
                            onActiveIndexChange={(pr) => {
                                tryIt(() => {
                                    setActive(pr.activeIndex)
                                })
                            }}>
                            {
                                data?.map(media => (
                                    <SwiperSlide key={media.id}>
                                        <Box justifyContent={'center'}>
                                            <img
                                                alt={media?.title}
                                                src={media?.image}
                                                style={{
                                                    width: "auto",
                                                    height: '65vh'
                                                }}/>
                                        </Box>
                                    </SwiperSlide>
                                ))
                            }
                        </Swiper>
                    </Box>
                </Box>
                <Box pt={5} pb={2} boxShadow={4} style={{zIndex: theme.zIndex.appBar}}>
                    <Swiper
                        id="thumbs"
                        slidesPerView={5.6}
                        onSwiper={setThumbsSwiper}
                        style={{
                            width: "100%",
                            margin: 0
                        }}>
                        {data?.map((media, index) => {
                            const isActive = index === active
                            return (
                                <SwiperSlide key={media.id}>
                                    <Box
                                        width={1}
                                        pl={1}
                                        style={{
                                            height: "20vh",
                                        }}>
                                        <img
                                            alt={media?.title}
                                            src={mediaSize.convertor(media?.image, {
                                                width: 300,
                                                quality: 50
                                            })}
                                            onClick={() => setActive(index)}
                                            style={{
                                                border: `2px solid ${isActive ? theme.palette.secondary.main : "transparent"}`,
                                                ...UtilsStyle.borderRadius(5)
                                            }}/>
                                    </Box>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </Box>
            </Box>
            }
        </Dialog>
    )
}

function ArrowItem({innerRef, icon, onClick, ...props}) {
    return (
        <MaterialBox ref={innerRef} width={1 / 5} justifyContent={"center"} alignItems={'center'} {...props}>
            <Box
                p={2}
                style={{
                    cursor: "pointer",
                    backgroundColor: colors.backgroundColor.heff0ef
                }}>
                <MtIcon icon={icon} fontSize={"large"}/>
            </Box>
        </MaterialBox>
    )
}