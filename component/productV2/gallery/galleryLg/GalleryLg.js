import React from "react";
import {Box, Typography, useOpenWithBrowserHistory, useState, UtilsStyle, zIndexComponent} from "material-ui-helper";
import {media as mediaSize} from "../../../../repository";
import {useTheme} from "@material-ui/core";
import GalleryLgDialog from "./GalleryLgDialog";
import Img from "../../../base/Img";


const moreBoxStyle = {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,0.30)",
    zIndex: zIndexComponent.img + 1,
    cursor: 'pointer'
}

const thumbnailWidth = 200

export default function GalleryLg({data}) {
    const theme = useTheme()
    const [open, setOpen] = useOpenWithBrowserHistory("product-galleryLg")
    const [activeItem, setActiveItem] = useState()
    const canOpenDialog =data?.length >1;

    function handleClose() {
        setActiveItem(undefined)
        setOpen(false)
    }

    function handleClick(id) {
        if (!canOpenDialog)
            return
        setActiveItem(id)
        setOpen(true)
    }

    return (
        <Box width={1} pr={3} pl={10} pb={5} center={true}>
            <Box width={"14%"} flexDirectionColumn={true} pr={3}>
                {
                    data.slice(0, 4)?.map((media, index) => (
                        <Box key={media?.image} width={1} pt={index === 0 ? 0 : 4}>
                            <Box
                                width={1}
                                p={"2px"}
                                style={{
                                    position: "relative",
                                    border: `2px solid ${index !== 0 ? "transparent" : theme.palette.secondary.main}`,
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                                <Img
                                    imageHeight={thumbnailWidth}
                                    imageWidth={thumbnailWidth}
                                    src={mediaSize.convertor(media?.image, {
                                        width: thumbnailWidth,
                                        cropHeight: thumbnailWidth,
                                        cropWidth: thumbnailWidth,
                                        quality: 50
                                    })}
                                    onClick={() => {
                                        if (media?.id)
                                            handleClick(media?.id)
                                    }}
                                    alt={media?.title}
                                    style={{
                                        cursor: canOpenDialog ? "pointer":undefined
                                    }}/>
                                {
                                    data.length > 4 && index === 3 &&
                                    <Box
                                        dir={"ltr"}
                                        onClick={() => {
                                            if (media?.id)
                                                handleClick(media?.id)
                                        }}
                                        center={true} style={moreBoxStyle}>
                                        <Typography color={"#fff"} variant={"h3"} component={"span"}>
                                            +{data.length - 3}
                                        </Typography>
                                    </Box>
                                }
                            </Box>
                        </Box>
                    ))
                }
            </Box>
            <Box width={"86%"} center={true}>
                <Img
                    src={data?.[0].image}
                    alt={data?.[0].title}
                    imageWidth={mediaSize.media.width}
                    imageHeight={mediaSize.media.height}
                    onClick={() => {
                        handleClick(data?.[0]?.id)
                    }}
                    style={{
                        cursor: canOpenDialog ?  "pointer":undefined
                    }}/>
            </Box>
            <GalleryLgDialog
                open={open}
                activeImage={activeItem}
                data={data}
                onClose={handleClose}/>
        </Box>
    )
}