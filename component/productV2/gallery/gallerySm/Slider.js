import makeStyles from "@material-ui/core/styles/makeStyles";
import {Box, Img, isServer} from "material-ui-helper";
import React from "react";
import MaterialBox from "@material-ui/core/Box";
import clsx from "clsx";
import {Swiper, SwiperSlide} from 'swiper/react';
import {media} from "../../../../repository";


const useSliderProductStyle = makeStyles(theme => ({
    productSwiperSliderRoot: {
        '& .active>div:after': {
            content: "''",
            background: theme.palette.secondary.main,
            position: "absolute",
            width: "100%",
            height: 4,
            left: 0,
            bottom: 0
        }
    },
}));

export default function Slider({data, active, onChange}) {
    const classes = useSliderProductStyle();
    return (
        isServer() ?
            <React.Fragment/> :
            <MaterialBox width={1} pt={1} className={clsx([classes.productSwiperSliderRoot, 'base-swiper-slider'])}>
                <Swiper
                    slidesPerView={3.3}
                    freeMode={true}>
                    {data.map((img, i) => {
                        const isActive = img?.id === active
                        return (
                            <SwiperSlide key={img.id}>
                                <Box className={isActive ? 'active' : undefined} px={0.5}
                                     onClick={() => onChange(img?.id)}>
                                    <Img
                                        imageWidth={media.media.width}
                                        imageHeight={media.media.height}
                                        renderTimeout={2000}
                                        src={img.image}
                                        alt={img.title}/>
                                </Box>
                            </SwiperSlide>
                        )
                    })}
                </Swiper>
            </MaterialBox>
    )
}