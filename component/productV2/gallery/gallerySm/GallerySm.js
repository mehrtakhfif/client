import React, {useEffect, useMemo} from "react";
import {Box, Img, tryIt, useOpenWithBrowserHistory, useState} from "material-ui-helper";
import {media} from "../../../../repository";
import _ from "lodash"
import Slider from "./Slider"
import GallerySmDialog from "./GallerySmDialog";

export default function GallerySm({data}) {
    const [active, setActive] = useState();
    const [open, setOpen, __, setClose] = useOpenWithBrowserHistory("product-galleryLg")
    const image = useMemo(() => {
        return tryIt(() => {
            return _.find(data, da => da.id === active)
        })
    }, [data, active])

    useEffect(() => {
        try {
            if (!data)
                return
            if (!active || _.findIndex(data, d => d.id === active) !== -1) {
                setActive(data[0].id)
            }
        } catch (e) {
        }
    }, [data])

    function handleClick() {
        setOpen(true)
    }

    return (
        <Box flexDirection={'column'}>
            <Img
                imageWidth={media.media.width}
                imageHeight={media.media.height}
                src={image?.image}
                alt={image?.title}
                onClick={handleClick}/>
            <Slider data={data} active={active} onChange={setActive}/>
            <GallerySmDialog
                open={open}
                active={active}
                data={data}
                onClose={setClose}/>
        </Box>
    )
}
