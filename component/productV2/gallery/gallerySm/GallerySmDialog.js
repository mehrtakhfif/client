import React, {useEffect, useMemo, useRef} from "react";
import {BottomAppBar, Box, DelayComponent, Dialog, tryIt, useState} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import _ from "lodash";
import CustomHeader from "../../../header/customHeader/CustomHeader";
import {lang} from "../../../../repository";
import {TransformComponent, TransformWrapper} from "react-zoom-pan-pinch";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Swiper, SwiperSlide} from 'swiper/react';
import Button from "@material-ui/core/Button";


const useGallerySmDialog = makeStyles(theme => ({
    smGalleryRoot: {
        "& .swiper-container": {
            width: '100%'
        }
    },
    imageZoomable: {
        '& div': {
            width: '100%',
            height: '100%'
        },
        '& img': {
            width: '100%'
        }
    }
}));

export default function GallerySmDialog({open, active: activeImageId, data, onClose}) {
    const resetRef = useRef()
    const theme = useTheme();
    const classes = useGallerySmDialog()
    const [active, setActive] = useState()
    const [init,setInit] = useState(open)

    useEffect(() => {
        const index = _.findIndex(data, d => d.id === activeImageId)
        if (index !== -1)
            setActive(activeImageId)
    }, [activeImageId])

    const activeImage = useMemo(() => {
        return _.find(data, (d) => d?.id === active)
    }, [active])

    useEffect(()=>{
        tryIt(()=>resetRef.current.click())
    },[active])

    useEffect(() => {
        if (open && !init)
            setInit(true)
    }, [open])


    return (
        <Dialog open={open} fullScreen={true}>
            {
                init&&
                <DelayComponent refresh={open} delay={400}>
                    <Box className={classes.smGalleryRoot} flexDirection={'column'}>
                        <CustomHeader
                            showToolbar={false}
                            title={lang.get("view_photos")}
                            backable={true}
                            onDismissClick={onClose}/>
                        <Box
                            height={"100vh"}
                            className={classes.imageZoomable}>
                            <TransformWrapper
                                defaultScale={1}
                                defaultPositionX={200}
                                defaultPositionY={100}>
                                {
                                    ({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                                        <TransformComponent>
                                            <Button ref={resetRef} onClick={resetTransform} style={{display:'none'}}>
                                                reset
                                            </Button>
                                            <Box component={"span"} flexDirection={'column'} center={true}>
                                                <img
                                                    src={activeImage?.image}
                                                    alt={activeImage?.title}/>
                                            </Box>
                                        </TransformComponent>
                                    )
                                }
                            </TransformWrapper>
                        </Box>
                        <BottomAppBar>
                            <DelayComponent>
                                <Swiper
                                    slidesPerView={3.6}
                                    freeMode={true}>
                                    {
                                        data?.map(media =>{
                                            const isActive = active === media.id
                                            return (
                                                <SwiperSlide key={media.id}>
                                                    <Box
                                                        px={1}
                                                        minHeight={"13vh"}
                                                        alignItems={'center'}
                                                        justifyContent={'center'}>
                                                        <img
                                                            alt={media?.title}
                                                            src={media?.image}
                                                            onClick={() => {
                                                                setActive(media.id)
                                                            }}
                                                            style={{
                                                                width: "100%",
                                                                height: 'auto',
                                                                borderBottom:`2px solid ${isActive?theme.palette.secondary.main:"transparent"}`
                                                            }}/>
                                                    </Box>
                                                </SwiperSlide>
                                            )

                                        })
                                    }
                                </Swiper>
                            </DelayComponent>
                        </BottomAppBar>
                    </Box>
                </DelayComponent>
            }
        </Dialog>
    )
}