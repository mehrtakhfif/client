import React, {useContext} from "react";
import {HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import _ from "lodash"
import GallerySkeleton from "./GallerySkeleton";
import GalleryLg from "./galleryLg/GalleryLg";
import ErrorBoundary from "../../base/ErrorBoundary";
import GallerySm from "./gallerySm/GallerySm";
import ProductDataContext from "../context/ProductDataContext";


function Gallery() {
    const [productData] = useContext(ProductDataContext);
    const {media} = productData || {};
    if (_.isEmpty(media))
        return <GallerySkeleton/>

    return (
        <React.Fragment>
            <ErrorBoundary elKey={"GalleryLg::(component->productV2->gallery->Gallery.js)"}>
                <HiddenLgUp>
                    <GallerySm data={media}/>
                </HiddenLgUp>
                <HiddenMdDown>
                    <GalleryLg data={media}/>
                </HiddenMdDown>
            </ErrorBoundary>
        </React.Fragment>
    )
}

export default React.memo(Gallery)