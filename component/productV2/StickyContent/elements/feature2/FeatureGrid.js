import React from "react";
import {Box, Typography} from "material-ui-helper";
import _ from "lodash"
import ShowMore from "../../../../base/ShowMore";


//Todo: Skeleton note created

const acceptableHeight = 220;

export default function FeatureGrid({data}) {


    const {feature_value} = data || {};
    if (_.isEmpty(data) || _.isEmpty(feature_value))
        return <React.Fragment/>


    return (
        <ShowMore
            acceptableHeight={acceptableHeight}
            watcher={[data]}>
            <Box
                pl={1}
                width={1}
                pt={1.45}
                flexDirection={{
                    xs: "column",
                    md: "row"
                }}
                flexWrap={{
                    xs: "nowrap",
                    md: "wrap"
                }}>
                {
                    feature_value?.map(({id, name, settings}, index) => (
                        <Box key={id}
                             py={1.8}
                             pl={{
                                 xs:0,
                                 md:4
                             }}
                             fontWeight={"normal"}
                             width={{
                                 xs: 1,
                                 md: 1 / 3
                             }}>
                            <Typography fontWeight={"normal"}
                                        variant={{
                                            xs: "subtitle1",
                                            md: "subtitle2"
                                        }}>
                                {name}
                            </Typography>
                        </Box>
                    ))
                }
            </Box>
        </ShowMore>
    )
}