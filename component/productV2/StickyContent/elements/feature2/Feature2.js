import React from "react";
import ContentContainer from "../helper/ContentContainer";
import _ from "lodash";
import {featureType} from "../../StickyContent";
import FeatureGrid from "./FeatureGrid";
import Feature from "./Feature";


function Feature2({showTitle, name, data, divider}) {


    const {feature_value} = data


    if (_.isEmpty(feature_value))
        return <React.Fragment/>

    return (
        <ContentContainer name={name}
                          showTitle={showTitle}
                          divider={divider}
                          watcher={[data]}>

            {feature_value?.map(({feature, hideName, type}) => {
                const isGrid = featureType.grid === type
                const isDefault = featureType.default === type

                return (
                    <React.Fragment>
                        {
                            isGrid ?
                                <FeatureGrid data={feature}/> :
                                isDefault ?
                                    <Feature data={feature}/> :
                                    <React.Fragment/>}
                    </React.Fragment>
                )
            })}

        </ContentContainer>
    )
}

export default React.memo(Feature2)