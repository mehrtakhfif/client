import React from "react";
import {Box, Typography, useWindowSize} from "material-ui-helper";
import _ from "lodash";
import {colors} from "../../../../../repository";
import ShowMore from "../../../../base/ShowMore";


const acceptableHeight = 220;
const smAcceptableHeight = 300;

export default function Feature({data}) {
    const [__, ___, {
        smDown,
        ...pr
    }] = useWindowSize()



    if (_.isEmpty(data))
        return <React.Fragment/>

    return (
        <ShowMore
            acceptableHeight={smDown ? smAcceptableHeight : acceptableHeight}
            watcher={[data]}>
            <Box
                flexDirectionColumn={true}
                pl={1}
                pt={{
                    xs: 2
                }}>
                {
                    data?.map(it => {
                        const {id, feature: name, feature_value, feature_settings} = it
                        const {hideName} = feature_settings || {};
                        return (
                            <Box
                                key={id}
                                py={{
                                    xs: 1
                                }}>
                                {
                                    !hideName ?
                                        <React.Fragment>
                                            <Box
                                                pr={1}
                                                width={{
                                                    xs: "40%",
                                                    md: "30%",
                                                }}>
                                                <Typography
                                                    color={colors.textColor.h757575}
                                                    variant={{
                                                        xs: "subtitle1",
                                                        md: "subtitle2"
                                                    }}>
                                                    {name}
                                                </Typography>
                                            </Box>
                                            <FeatureValue
                                                flex={1}
                                                pl={2}
                                                pr={2}
                                                value={feature_value}/>
                                        </React.Fragment> :
                                        <FeatureValue
                                            width={1}
                                            value={feature_value}/>
                                }
                            </Box>
                        )
                    })
                }
            </Box>
        </ShowMore>
    )
}


function FeatureValue({value, ...props}) {
    return (
        <Box
            flexWrap={"wrap"}>
            {
                value?.map((it, index) => (
                    <Typography
                        key={it?.id}
                        style={{whiteSpace:'pre-wrap'}}
                        variant={{
                            xs: "subtitle1",
                            md: "subtitle2"
                        }}>
                        {index !== 0 && ", "}
                        {it?.name}
                    </Typography>
                ))
            }
        </Box>
    )
}