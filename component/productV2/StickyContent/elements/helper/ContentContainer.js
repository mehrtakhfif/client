import React from "react";
import {Box, HiddenLgUp, HiddenMdDown, Typography} from "material-ui-helper";
import {colors} from "../../../../../repository";
import {Divider} from "@material-ui/core";
import ShowMore from "../../../../base/ShowMore";


export default function ContentContainer({
                                             name,
                                             showTitle,
                                             divider,
                                             acceptableLine,
                                             acceptableHeight,
                                             watcher = [],
                                             showMoreProps = {},
                                             children,
                                             ...props
                                         }) {

    return (
        <Box pr={{xs: 2, md: 4}} pl={{xs: 2, md: 10}} flexDirectionColumn={true} {...props}>
            <Box py={{xs: 3, md: 5}} pr={{xs: 0, md: 2}} flexDirectionColumn={true}>
                <HiddenLgUp>
                    <Name name={name}/>
                </HiddenLgUp>
                <HiddenMdDown>
                    {
                        showTitle ?
                            <Name name={name}/> :
                            <React.Fragment/>
                    }
                </HiddenMdDown>
                {
                    (acceptableHeight || acceptableLine) ?
                        <ShowMore
                            name={name}
                            acceptableLine={acceptableLine}
                            acceptableHeight={acceptableHeight}
                            watcher={watcher}
                            {...showMoreProps}>
                            {children}
                        </ShowMore> :
                        children
                }
            </Box>
            {
                divider &&
                <Divider/>
            }
        </Box>
    )
}

function Name({name}) {
    return (
        <Typography
            component={"h4"}
            color={colors.textColor.h404040}
            fontWeight={700}
            variant={{
                xs: "h5",
                md: "subtitle1"
            }}>
            {name}
        </Typography>
    )
}