import React, {useCallback, useEffect, useRef} from "react";
import {gLog, Typography} from "material-ui-helper";
import _ from "lodash"
import {UtilsParser} from "../../../../utils/Utils";
import ContentContainer from "./helper/ContentContainer";
import {useRouter} from "next/router";
import Box from "@material-ui/core/Box";


const acceptableLine = 15;
export default function Description({uniqKey, name, showTitle, data, divider,productId}) {
    const router = useRouter()




    const {value} = data || {};



    function rebuild(){

    }


    useEffect(() => {

        const interval = setInterval(()=>{
            try {
                const section = document.getElementById(uniqKey);
                const initial = section.getAttribute("initial");
                if (initial){
                    clearInterval(interval)
                    return
                }
                const aList = section.getElementsByTagName("a")
                _.forEach(aList, el => {
                    el.onclick = (e) => {
                        e.preventDefault()
                        const href = el.getAttribute("href");
                        router.push(href)
                    }
                })
                section.setAttribute("initial",'true')
            } catch (e) {
            }
        },1000)


        return()=>{
            clearInterval(interval)
        }
    }, []);

    if (_.isEmpty(value))
        return (<React.Fragment/>);

    return (
        <ContentContainer
            name={name}
            showTitle={showTitle}
            divider={divider}
            acceptableLine={acceptableLine}
            watcher={[value]}
            showMoreProps={{
                className: "lineHeight32"
            }}>
            <Typography
                id={uniqKey}
                pl={1}
                pt={1}
                display={'block'}
                className={"description-content"}
                variant={"subtitle2"}
                style={{}}>

                {
                    productId?.productId &&  productId.productId === 2158 &&  <iframe  width={'100%'} height={'350px'} src="https://www.aparat.com/video/video/embed/videohash/pEUG9/vt/frame" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"/>

                }
                {UtilsParser.html(value)}
            </Typography>
        </ContentContainer>
    )
}



function useHookWithRefCallback() {
    const ref = useRef(null)
    const setRef = useCallback(node => {
        if (ref.current) {
            gLog("sakfjkasjkfaksj ref.current",ref.current)
        }

        if (node) {
            gLog("sakfjkasjkfaksj node",node)
        }
            gLog("sakfjkasjkfaksj pars")

        // Save a reference to the node
        ref.current = node
    }, [])

    return [setRef]
}
