import React from "react";
import ContentContainer from "./helper/ContentContainer";
import {Box, Typography} from "material-ui-helper";
import _ from "lodash"


//Todo: Skeleton note created

const acceptableHeight = 220;

export default function FeatureGrid({showTitle, name, data, divider}) {

    const {feature_value} = data || {};
    if (_.isEmpty(data) || _.isEmpty(feature_value))
        return <React.Fragment/>


    return (
        <ContentContainer name={name}
                          showTitle={showTitle}
                          divider={divider}
                          acceptableHeight={acceptableHeight}
                          watcher={[data]}>
            <Box
                pl={1}
                width={1}
                pt={1.45}
                flexDirection={{
                    sm:"column",
                    md:"row"
                }}
                flexWrap={{
                    sm:"nowrap",
                    md:"wrap"
                }}>
                {
                    feature_value?.map(({id, name, settings}, index) => (
                        <Box key={id}
                             py={1.8}
                             pl={{
                            xs:0,
                            md:4.8
                        }}
                             fontWeight={"normal"} width={1 / 4}>
                            <Typography fontWeight={"normal"}
                                        variant={{
                                            xs: "subtitle1",
                                            md: "subtitle2"
                                        }}>
                                {name}
                            </Typography>
                        </Box>
                    ))
                }
            </Box>
        </ContentContainer>
    )
}