import React from "react";
import {Box, Typography, useWindowSize} from "material-ui-helper";
import ContentContainer from "./helper/ContentContainer";
import _ from "lodash";
import {colors} from "../../../../repository";


const acceptableHeight = 220;
const smAcceptableHeight = 300;

export default function Feature({name, data, divider, showTitle, ...props}) {
    const [__, ___, {
        smDown,
        ...pr
    }] = useWindowSize()


    if (_.isEmpty(data))
        return <React.Fragment/>

    return (
        <ContentContainer
            name={name}
            watcher={[data]}
            acceptableHeight={smDown ? smAcceptableHeight : acceptableHeight}
            divider={divider}
            showTitle={showTitle}>
            <Box
                flexDirectionColumn={true}
                pl={1}
                pt={{
                    xs: 2
                }}>
                {
                    data?.map(it => {
                        const {id, feature: name, feature_value, feature_settings} = it
                        const {hideName} = feature_settings || {};

                        return (
                            <Box
                                key={id}
                                py={{
                                    xs: 1
                                }}>
                                {
                                    !hideName ?
                                        <React.Fragment>
                                            <Box width={"25%"}>
                                                <Typography
                                                    color={colors.textColor.h757575}
                                                    variant={{
                                                        xs: "subtitle1",
                                                        md: "subtitle2"
                                                    }}>
                                                    {name}
                                                </Typography>
                                            </Box>
                                            <FeatureValue
                                                width={"60%"}
                                                pl={2}
                                                pr={{
                                                    xs: 2,
                                                }}
                                                value={feature_value}/>
                                        </React.Fragment> :
                                        <FeatureValue
                                            width={1}
                                            value={feature_value}/>
                                }
                            </Box>
                        )
                    })
                }
            </Box>
        </ContentContainer>)

}


function FeatureValue({value, ...props}) {
    return (
        <Box
            width={'75%'}
            flexWrap={"wrap"}>
            {
                value?.map((it, index) => (
                    <Typography
                        key={it?.id}
                        variant={{
                            xs: "subtitle1",
                            md: "subtitle2"
                        }}>
                        {index !== 0 && ", "}
                        {it?.name}
                    </Typography>
                ))
            }
        </Box>
    )
}