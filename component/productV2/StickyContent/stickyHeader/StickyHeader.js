import React, {useEffect} from "react";
import {Box, tryIt} from "material-ui-helper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import _ from "lodash"
import StickyHeaderSkeleton from "../StickyHeaderSkeleton";
import StickyHeaderItem from "../StickyHeaderItem";
import {colors} from "../../../../repository";
import {hidableHeaderHeight, maxAppBarHeight} from "../../../header/HeaderLg";

const useStickyHeaderStyle = makeStyles(theme => ({
    rootSticky: {
        position: "sticky",
        top: maxAppBarHeight,
        background: "#fff",
        paddingTop: hidableHeaderHeight + 10,
        borderBottom: `1px solid ${colors.borderColor.hc9c9c9}`,
        overflowX: "scroll",
        overflowY: "hidden",
        whiteSpace: "nowrap",
        MsOverflowStyle: "none",
        scrollbarWidth: "none",
        zIndex: theme.zIndex.appBar - 10,
        "&::-webkit-scrollbar": {
            display: "none"
        }
    },

}));

export default function StickyHeader({active, list, onCLick}) {

    if (_.isEmpty(list))
        return <StickyHeaderSkeleton/>

    const classes = useStickyHeaderStyle()


    useEffect(() => {
        tryIt(() => {
            const container = document.getElementById("sticky-content-header")
            const item = document.getElementById(`sticky-content-header-item-${active}`)
            // container.scrollLeft = item.offsetLeft - (container.offsetWidth / 2)
            container.scroll({
                left: item.offsetLeft - (container.offsetWidth / 2.5),
                behavior: 'smooth'
            })
        })
    }, [active])

    return (
        <Box id={"sticky-content-header"} className={classes.rootSticky}>
            <Box component={"ul"} ml={10}>
                {
                    list?.map(({name, key}, index) => (
                        <StickyHeaderItem
                            key={key}
                            to={key}
                            index={index}
                            active={key === active}
                            name={name}
                            onClick={onCLick}/>
                    ))
                }
            </Box>
        </Box>
    )
}



