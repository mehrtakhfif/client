import React from "react";
import {Box, Typography} from "material-ui-helper";
import {colors} from "../../../repository";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-scroll";
import {headerOffset} from "../../header/HeaderLg";


const useStickyHeaderItemStyle = makeStyles(theme => ({
    rootStickyItem: {
        position: "relative",
        cursor: "pointer",
        "&>span": {
            position: "absolute",
            width: '125%',
            height: 3,
            bottom: 0,
            right: "50%",
            transform: "translate(50%)",
            backgroundColor: theme.palette.secondary.main
        }
    },
}));


export default function StickyHeaderItem({index, active, to, name, onClick}) {
    const classes = useStickyHeaderItemStyle()

    return (
        <Box
            component={"li"}
            id={`sticky-content-header-item-${to}`}
            ml={index === 0 ? 0 : 4} mr={4}>
            <Link
                no_hover="true"
                to={to}
                spy={true}
                smooth={true}
                offset={-1 * (headerOffset + 20)}
                className={classes.rootStickyItem}
                duration={500}
                onSetActive={onClick}
                onClick={() => {
                    onClick(to)
                }}>
                <Box display={"inline-block"} aria-label={to} pb={1.5}>
                    <Typography
                        display={"inline-block"}
                        variant={"body1"} fontWeight={active ? "bold" : "normal"}
                        color={active ? colors.textColor.h000000 : colors.textColor.h757575}>
                        {name}
                    </Typography>
                </Box>
                {
                    active &&
                    <Box component={"span"}/>
                }
            </Link>
        </Box>
    )


    return (
        <Link
            id={`sticky-content-header-item-${to}`}
            no_hover="true"
            to={to}
            spy={true}
            smooth={true}
            offset={-1 * (headerOffset + 80)}
            className={classes.rootStickyItem}
            duration={500}
            onSetActive={onClick}>
            <Box display={"inline-block"} aria-label={to} pb={2.5} ml={index === 0 ? 0 : 8}>
                <Typography
                    display={"inline-block"}
                    variant={"body1"} fontWeight={active ? "bold" : "normal"}
                    color={active ? colors.textColor.h000000 : colors.textColor.h757575}>
                    {name}
                </Typography>
            </Box>
            {
                active &&
                <Box component={"span"}/>
            }
        </Link>
    )
}