import React, {useContext, useEffect, useMemo} from "react";
import {Box, HiddenMdDown, tryIt, useState} from "material-ui-helper";
import StickyHeader from "./stickyHeader/StickyHeader";
import _ from "lodash";
import {isServer, lang} from "../../../repository";
import {Element} from "react-scroll";
import Description from "./elements/Description";
import {DEBUG} from "../../../pages/_app";
import FeatureGrid from "./elements/FeatureGrid";
import Feature from "./elements/feature2/Feature";
import ProductDataContext from "../context/ProductDataContext";
import BusinessInformation from "../businessInformation/BusinessInformation";
import Feature2 from "./elements/feature2/Feature2";


const contentTypes = {
    description: "description",
    feature: "feature",
    feature2: "feature2",
    featureGrid: "feature_grid",
    userComments: "user_comments",
    questionReplay: "question_replay",
    businessInformation: "business_information",
}

export const featureType = {
    default: 'default',
    grid: 'grid'
}

export default function StickyContent(productId) {


    //region data
    const [data, __, productError] = useContext(ProductDataContext);

    const {description, features} = data || {};
    const [active, setActive] = useState()
    const list = useMemo(getList, [data])


    function getList() {
        let list = [];

        tryIt(() => {
            _.forEach(description?.items, ({key, value}) => {
                if (value.length <= 50)
                    return

                list.push({
                    key: key,
                    name: lang.get(key),
                    type: contentTypes.description,
                })
            })
        })

        tryIt(() => {
            const {group_features} = features
            const newGpFeature = [];

            let concatFeature = features?.features || [];

            _.forEach(group_features, (f) => {
                const {settings, features} = f
                tryIt(() => {
                    const {show_title} = settings || {};
                    if (!show_title) {
                        concatFeature = concatFeature.concat(features)
                        return
                    }
                    newGpFeature.push(f)
                })
            })

            const generatorFeature = (id, feature, type, hideName = false) => {
                return {
                    id,
                    feature,
                    type,
                    hideName
                }
            }

            const reList2 = [
                {
                    id: 0,
                    value: [],
                    name: lang.get("specifications")
                }
            ];


            const featureGenerator = (features, {
                id = 0,
                name = lang.get("specifications"),
                settings = {}
            } = {}) => {

                if (_.isEmpty(features))
                    return


                const showFeatureTitle = settings?.show_title;
                const list2 = [];

                try {
                    _.forEach(features, (it) => {
                        const {id, feature_settings} = it;
                        const {showGrid, hideName} = feature_settings;

                        const type = showGrid ? featureType.grid : featureType.default;
                        if (type === featureType.grid) {
                            const fe = generatorFeature(id, it, type, hideName)
                            list2.push(fe)
                        } else {
                            const index = _.findIndex(list2, it => {
                                return it.type === featureType.default
                            })
                            if (index === -1) {
                                const fe = generatorFeature(id, [it], type, hideName)
                                list2.push(fe)
                            } else {
                                list2[index].feature.push(it)
                            }
                        }

                    })
                    const index = showFeatureTitle ? _.findIndex(reList2, (it) => {
                        return it.id === id
                    }) : 0

                    if (index >= 0) {
                        if (reList2[index].value) {
                            reList2[index].value = [...reList2[index].value, ...list2]
                        } else {
                            reList2[index].value = list2
                        }
                    } else {
                        reList2.push({
                            id,
                            value: list2,
                            name: name
                        })
                    }
                } catch {
                }


                // const featureList = [];
                // const hideName = []
                // const grid = []
                // const gridHideName = [];
                //
                // _.forEach(features, it => {
                //     //showGrid
                //     if (it?.feature_settings?.showGrid) {
                //         const item = {
                //             ...it,
                //             name: it?.feature,
                //             key: contentTypes.featureGrid + `${id}` + it?.id,
                //             type: contentTypes.featureGrid,
                //         }
                //
                //         //if showGrid and hideName
                //         if (it?.feature_settings?.hideName) {
                //             gridHideName.push(item)
                //             return;
                //         }
                //
                //         //showGrid
                //         grid.push(item)
                //         return
                //     }
                //
                //     //only hideName
                //     if (it?.feature_settings?.hideName) {
                //         hideName.push(it)
                //         return;
                //     }
                //
                //     //default feature
                //     featureList.push(it)
                // })
                //
                //
                // //hideName feature
                //
                //
                // const newF = featureList.concat(hideName)
                // if (!_.isEmpty(newF)) {
                //     list = list.concat({
                //         name: name,
                //         key: contentTypes.feature + id,
                //         type: contentTypes.feature,
                //         data: newF
                //     })
                // }
                //
                // //gridHideNameFeature
                // if (!_.isEmpty(gridHideName)) {
                //     let feature_value = [];
                //     _.forEach(gridHideName, f => feature_value = feature_value.concat(f?.feature_value))
                //     list = list.concat({
                //         name: name,
                //         key: contentTypes.featureGrid + id,
                //         type: contentTypes.featureGrid,
                //         feature_value: feature_value
                //     })
                // }
                //
                // //all grid feature
                // if (grid)
                //     list = list.concat(grid)
            }


            featureGenerator(concatFeature)
            _.forEach(newGpFeature, ({id, name, settings, features}) => {
                featureGenerator(features, {id, name, settings})
            })


            try {
                _.forEach(reList2, ({id, value, name}) => {
                    if (_.isEmpty(value))
                        return
                    list.push({
                        name: name,
                        key: contentTypes.feature2 + id,
                        type: contentTypes.feature2,
                        feature_value: _.sortBy(value, [function (it) {
                            return it.type !== featureType.grid
                        }])
                    })
                })
            } catch (e) {
            }
        })

        const {address, location, details, cities} = data || {};
        const {days, hours, phone} = details || {}

        if (Boolean(address || location || days || hours || !(_.isEmpty(phone) || _.isEmpty(cities))))
            list.push({
                key: "business_information",
                name: lang.get("business_information"),
                type: contentTypes.businessInformation
            })

        if (false && DEBUG) {
            list.push({
                key: "user_comments",
                name: lang.get("user_comments"),
                type: contentTypes.userComments,
            })

            list.push({
                key: "question_replay",
                name: lang.get("question_replay"),
                type: contentTypes.questionReplay
            })
        }

        return list
    }

    //endregion data

    useEffect(() => {
        tryIt(() => {
            if (!data)
                return
            if (!active && list) {
                setActive(list[0].key)
            }
        })
    }, [list, data, active])

    if (_.isEmpty(data))
        return (
            <Box>
                <StickyHeader/>
            </Box>
        )

    const renderList = isServer() ? getList() : list

    return (
        <Box flexDirectionColumn={true}>
            <HiddenMdDown>
                <StickyHeader
                    active={active}
                    list={renderList}
                    onCLick={setActive}/>
            </HiddenMdDown>
            {
                renderList.map((it, index) => (
                    <ComponentSelector
                        key={it?.key}
                        listLength={renderList.length}
                        index={index}
                        productId={productId}
                        data={data} item={it}/>))
            }
        </Box>
    )
}

function ComponentSelector({index, data, item, listLength,productId}) {
    const {key, name, type} = item;
    const showTitle = index !== 0
    const divider = index + 1 !== listLength;
    const props = {
        name,
        showTitle,
        divider
    }


    return (
        <React.Fragment>
            <Element key={key} name={key} className="element">
                {
                    type === contentTypes.description ?
                        <Description
                            {...props}
                            productId={productId}
                            uniqKey={key}
                            data={data?.description?.items.find(it => it.key === key)}/> :
                        type === contentTypes.feature ?
                            <Feature
                                {...props}
                                data={item?.data}/> :
                            type === contentTypes.featureGrid ?
                                <FeatureGrid
                                    {...props}
                                    data={item}/> :
                                type === contentTypes.businessInformation ?
                                    <BusinessInformation {...props}/> :
                                    type === contentTypes.feature2 ?
                                        <Feature2
                                            {...props}
                                            data={item}/> :
                                        <React.Fragment/>
                }
            </Element>
        </React.Fragment>
    )
}