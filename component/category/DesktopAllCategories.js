import React, {useEffect, useState} from 'react';
import {ButtonBase, makeStyles, useTheme} from '@material-ui/core';
import {Box, gLog, HiddenLgUp, HoverWatcher, tryIt, Typography} from 'material-ui-helper';
import clsx from "clsx";
import {useRouter} from "next/router";
import _ from "lodash"
import MtIcon from "../MtIcon";
import rout from "../../router";
import PleaseWait from "../base/loading/PleaseWait";
import ControllerSite from "../../controller/ControllerSite";
import {grey} from "@material-ui/core/colors";
import Nlink from "../base/link/NLink";
import {lang} from "../../repository";
import {typeItems} from "../../pages/search/[[...cat]]";

const useStyles = makeStyles((theme) => ({
    CategoryItem: {
        '&.active, &:hover': {
            backgroundColor: '#F8F8F8'
        }
    }
}));


export default function DesktopAllCategories({open = false, showProduct = true, onClick, ...props}) {
    const router = useRouter()
    const classes = useStyles();
    const theme = useTheme();
    const [data, setData] = useState()
    const [state, setState] = useState([])
    const [error, setError] = useState()

    useEffect(() => {
        ControllerSite.getAllCategories()[1]().then(res => {
            setError(false);
            const {product, service} = res.data;

            setData([
                {
                    permalink: rout.generate(rout.Product.Search.as({type: typeItems.service})),
                    id: -1,
                    children: service,
                    name: lang.get("grouping_services"),
                    type: "service"
                },
                {
                    permalink: rout.generate(rout.Product.Search.as({type: typeItems.product})),
                    id: 0,
                    children: product,
                    name: lang.get("grouping_products"),
                    type: "product"
                },
            ])

        }).catch((e) => setError(e));
    }, []);


    if (data === undefined) return open ? <PleaseWait/> : <React.Fragment/>
    //todo error component
    if (error) return open ? <Typography>Error</Typography> : <React.Fragment/>


    function generateLink(permalink) {
        return rout.Product.Search.create({category: permalink})?.[1].pathname
        router.push(r?.[0], r?.[1])
        tryIt(() => onClick(permalink))
    }

    function onRootHover(i, index) {
        if (_.isEmpty(i.children)) {
            setState([])
            return
        }
        setState([index])
    }

    function onChildHover(parentIndex, child, index) {
        // if (state[index + 1] === index) {
        //     return
        // }
        if (_.isEmpty(child.children)) {
            return
        }

        setState(s => {
            const newS = s.slice(0, parentIndex + 1)
            if (!_.isEmpty(child.children)) {
                newS.push(index)
            }
            return newS
        })
    }

    return (
        <Box bgcolor={"#fff"} width={'fit-content'} height={'100%'}
             pl={4}
             display={open ? 'flex' : 'none'} flexDirection={'row'}
             style={{
                 border: `1px solid ${grey[200]}`,
                 zIndex: theme.zIndex.appBar - 1
             }}>
            <Box bgcolor={'#fff'} pt={3} pb={7} display={'flex'} flexDirection={'column'}>
                {data?.map((i, index) => {
                    const href = generateLink(i?.permalink)
                    return (
                        <CategoryItem
                            key={i?.id || index}
                            isActive={state[0] === index}
                            isRoot={true}
                            href={href}
                            onHover={() => onRootHover(i, index)}
                            onClick={() => onClick(i?.permalink)}
                            showArrow={!_.isEmpty(i.children)}
                            name={i.name}/>
                    )
                })}
            </Box>
            {state?.map((stateIndex, parentIndex) => {
                let parent = data
                for (let i = 0; i <= parentIndex; i++) {
                    parent = _.cloneDeep(_.isArray(parent) ? parent[state[i]] : parent?.children[state[i]])
                }

                // if (parentIndex === 1)
                //     gcLog("asfasfasfasf log parent", parent)

                return (
                    <Box pt={3} pb={7} bgcolor={'#fff'} display={'flex'} flexDirection={'column'}>
                        <HiddenLgUp>
                            {
                                !_.isEmpty(parent?.children) &&
                                <CategoryItem
                                    href={generateLink(parent?.permalink)}
                                    onClick={(e) => onClick(parent?.permalink)}
                                    name={'مشاهده همه محصولات'}
                                    showArrow={false}/>
                            }
                        </HiddenLgUp>
                        {parent?.children.map((child, index) => {
                            const href = generateLink(child.permalink)
                            return (
                                <CategoryItem
                                    key={child?.id || index}
                                    href={href}
                                    isActive={state[parentIndex + 1] === index}
                                    onClick={(e) => onClick(child.permalink)}
                                    onHover={() => onChildHover(parentIndex, child, index)}
                                    showArrow={!_.isEmpty(child.children)}
                                    name={child.name}
                                />
                            )
                        })
                        }
                    </Box>

                )
            })}
        </Box>)


    function CategoryItem(props) {
        const {onClick, onHover, href, name, isRoot, isActive, showArrow = true} = props
        return (
            <HoverWatcher
                component={ButtonBase}
                timeout={0}
                onHover={(hover) => {
                    try {
                        if (hover)
                            onHover()
                    } catch {
                    }
                }}>
                <Nlink href={href} hoverStyle={false}>
                    <Box
                        className={clsx([classes.CategoryItem, isActive ? 'active' : ''])} display={'flex'}
                        alignItems={'center'} flexDirection={'row'} width={'15.5vw'}
                        bgcolor={'#fff'} py={2} pr={4} pl={isRoot ? 4 : 4}
                        onClick={onClick}>
                        <Typography flex={1} variant={'body2'} textAlign={'right'}>{name}</Typography>
                        <Box pl={2} alignItems={'center'}>{showArrow &&
                        <MtIcon icon={'mt-chevron-left'} fontSize={'small'}/>}</Box>
                    </Box>
                </Nlink>
            </HoverWatcher>
        )
    }
}