import React, {useEffect, useState} from 'react';
import {AppBar, Box, makeStyles, Tab, Tabs, Toolbar, useTheme} from '@material-ui/core';
import {gLog, SquareImg, Typography} from 'material-ui-helper';
import ControllerSite from '../../controller/ControllerSite';
import Paper from '@material-ui/core/Paper';
import MtIcon from "../MtIcon";
import useScrollTrigger from "@material-ui/core/useScrollTrigger/useScrollTrigger";
import clsx from "clsx";
import {useRouter} from "next/router";
import {lang, media} from '../../repository';
import BasePage from "../base/BasePage";
import {UtilsStyle} from "../../utils/Utils";
import PleaseWait from "../base/loading/PleaseWait";
import rout from "../../router";
import _ from "lodash"

export const smHeaderHeight = 60;
export const smHeaderZIndex = 99;
const baseHeaderSmStyle = makeStyles(theme => ({
    headerSmRoot: {
        height: smHeaderHeight,
    },
    headerSmToolbar: {
        width: '100%',
        padding: 0,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        zIndex: smHeaderZIndex,
        height: smHeaderHeight,
        paddingRight: theme.spacing(1.5),
        paddingLeft: theme.spacing(1.5)
    },
}));
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    firstIcon: {
        paddingLeft: 70,
    },
    labelContainer: {
        width: 'auto',
        padding: 0,
    },
    iconLabelWrapper: {
        flexDirection: 'row',
    },
    iconLabelWrapper2: {
        flexDirection: 'row-reverse',
    },
}));

function MobileAllCategories(props) {
    const router = useRouter();
    const classes = baseHeaderSmStyle(props);
    const {window} = props;
    let trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 10,
        target: window ? window() : undefined,
    });

    const [showHeader, setShowHeader] = useState(false);
    const [data, setData] = useState();
    const [error, setError] = useState();
    const [backState, setBackState] = useState([]);

    useEffect(() => {
        ControllerSite.getAllCategories()[1]().then(res => {
            setError(false);
            setData(res?.data);
        }).catch((e) => setError(e));
    }, []);

    useEffect(() => {
        if (backState.length === 0) {
            toggleShowHeader(true)
        } else {
            toggleShowHeader(false)
        }
    }, [backState]);

    function toggleShowHeader(s) {
        setShowHeader(s)
    }

    if (data === undefined) return <PleaseWait/>
    //todo error component
    if (error) return <Typography>Error</Typography>


    function onChangeCategory(param) {
        //todo check go to link
        if (_.isEmpty(param?.children)) {
            setData(undefined)
            const r = rout.Product.Search.create({category: param.permalink})
            router.push(r?.[0], r?.[1])
            return
        }

        setData(s => {
            setBackState(j => {
                return j.concat([s])
            })
            try {
                param.children[0].parent = param.name
                param.children[0].parentId = param.id
                param.children[0].parentPermalink = param.permalink
            } catch {
            }
            return param.children
        })

    }

    function onBack(e) {
        const lastState = backState[backState.length - 1]
        setData(lastState)
        setBackState(s => s.filter(i => i !== lastState))
    }


    function onSearchClicked(e) {
        //todo open search component
    }


    return (
        <BasePage
            name="mobileAllCategories"
            setting={{
                header: {
                    showLg: true,
                    showSm: showHeader
                },
            }}>
            {!showHeader && (
                <AppBar
                    className={classes.headerSmRoot}
                    elevation={trigger ? 6 : 0}
                    style={{
                        backgroundColor: '#fff',
                    }}>
                    <Toolbar className={clsx(classes.headerSmToolbar, 'headerRoot')}
                             style={{
                                 transform: trigger ? 'scale(1)' : 'scale(0.96)',
                                 ...UtilsStyle.transition(650)
                             }}>
                        <Box width={1}
                             p={1} display={'flex'} flexDirection={'row'} color={"#000"}
                             alignItems={"center"}>
                            <Box onClick={onBack}>
                                <MtIcon icon={'mt-arrow-right'}/>
                            </Box>
                            <Box m={1} flex={1}>
                                <Typography
                                    variant={'h5'}
                                    fontWeight={"bold"}>
                                    {data[0]?.parent}
                                </Typography>
                            </Box>
                            {/*<Box onClick={onSearchClicked}>*/}
                            {/*    <MtIcon icon={'mt-search'}/>*/}
                            {/*</Box>*/}
                        </Box>
                    </Toolbar>
                </AppBar>
            )}
            <Box mt={!showHeader ? smHeaderHeight / 8 : 0}>
                <MobileCategory data={data} changeShowHeader={toggleShowHeader} onChangeCategory={onChangeCategory}/>
            </Box>
        </BasePage>
    );
}

function MobileCategory(props) {
    const {onChangeCategory, changeShowHeader, data} = props
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    function a11yProps(index) {
        return {
            id: `scrollable-force-tab-${index}`,
            'aria-controls': `scrollable-force-tabpanel-${index}`,
        };
    }

    function TabPanel(props) {
        const {children, value, index, ...other} = props;
        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}>
                {value === index && (
                    <Box p={3}>
                        {children}
                    </Box>
                )}
            </div>
        );
    }

    return (
        <div className={classes.root}>
            {
                _.isArray(data) ?
                    <CategoryContainer
                        onChangeCategory={onChangeCategory}
                        data={data}
                        changeShowHeader={changeShowHeader}/> :
                    <React.Fragment>
                        <Paper square className={classes.root}>
                            <Tabs
                                value={value}
                                onChange={handleChange}
                                variant="fullWidth"
                                indicatorColor="secondary"
                                textColor="secondary"
                            >
                                <Tab
                                    classes={{
                                        wrapper: classes.iconLabelWrapper,
                                        labelContainer: classes.labelContainer,
                                    }}
                                    icon={<MtIcon icon={"mt-shipping-bag"}/>}
                                    label={<Typography variant={'h5'} fontWeight={'bold'} p={1}>کالا</Typography>}
                                    {...a11yProps(0)}
                                />
                                <Tab
                                    classes={{
                                        wrapper: classes.iconLabelWrapper,
                                        labelContainer: classes.labelContainer,
                                    }}
                                    icon={<MtIcon icon={"mt-ticket"}/>}
                                    label={<Typography variant={'h5'} fontWeight={'bold'} p={1}>خدمات</Typography>}
                                    {...a11yProps(1)}
                                />
                            </Tabs>
                        </Paper>
                        <TabPanel value={value} index={0}>
                            <CategoryContainer
                                onChangeCategory={onChangeCategory}
                                data={data?.product}
                                changeShowHeader={changeShowHeader}/>
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <CategoryContainer
                                onChangeCategory={onChangeCategory}
                                data={data?.service}
                                changeShowHeader={changeShowHeader}/>
                        </TabPanel>
                    </React.Fragment>
            }

        </div>
    );
}


function CategoryContainer(props) {
    const {data, onChangeCategory} = props;

    return (
        <Box>
            {data?.map(category => (
                <CategoryItem
                    key={category.id}
                    data={category}
                    onChangeCategory={onChangeCategory}/>
            ))}
        </Box>);
}

function CategoryItem(props) {
    const {data, onChangeCategory} = props;
    const {parent,parentId,parentPermalink} = data || {}

    return (
        <React.Fragment>
            {
                parentId &&
                <CategoryItem data={{
                    id:parentId,
                    name:lang.get("show_all"),
                    permalink:parentPermalink
                }}
                onChangeCategory={onChangeCategory}/>
            }
            <Box onClick={(e) => onChangeCategory(data)} p={1} mb={1} display={'flex'} alignItems={'center'}
                 flexDirection={'row'} borderRadius={'5px'} style={{borderBottom: '1px solid #e9e9e9'}}>
                {
                    data.media &&
                    <SquareImg
                        imageWidth={media.category.width}
                        imageHeight={media.category.height}
                        width={theme.spacing(7)}
                        alt={data.media?.title} src={data.media?.image}/>
                }
                <Typography
                    p={1}
                    width={'-webkit-fill-available'} variant={'body1'}
                    fontWeight={'noraml'}>
                    {data.name}
                </Typography>
                {
                    !_.isEmpty(data?.children) &&
                    <Box>
                        <MtIcon fontSize={"small"} icon={"mt-chevron-left"}/>
                    </Box>
                }
            </Box>
        </React.Fragment>
    )
}

export default React.memo(MobileAllCategories)