import React from "react";
import {ButtonBase, Typography, UtilsStyle} from "material-ui-helper";
import {lang} from "../../../../../repository";
import MtIcon from "../../../../MtIcon";


export default function NoneAddress({onClick}) {

    return (
        <ButtonBase
            display={'flex'}
            width={1}
            center={true}
            onClick={onClick}
            style={{
                backgroundImage: 'url("data:image/svg+xml,%3csvg width=\'100%25\' height=\'100%25\' xmlns=\'http://www.w3.org/2000/svg\'%3e%3crect width=\'100%25\' height=\'100%25\' fill=\'none\' rx=\'8\' ry=\'8\' stroke=\'%23333\' stroke-width=\'3\' stroke-dasharray=\'15%2c15\' stroke-dashoffset=\'0\' stroke-linecap=\'square\'/%3e%3c/svg%3e")',
                ...UtilsStyle.borderRadius(8),
            }}>
            <MtIcon icon={"mt-map-marker-plus"} fontSize={"small"}/>
            <Typography variant={"body1"} py={4} pl={2} fontWeight={400}>
                {lang.get("create_an_address")}
            </Typography>
        </ButtonBase>
    )
}