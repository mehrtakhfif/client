import React from "react";
import useSWR from "swr";
import ControllerUser from "../../../../../controller/ControllerUser";
import {Box, Typography} from "material-ui-helper";
import NoneAddress from "./NoneAddress";
import Placeholder from "./Placeholder";
import {colors, lang} from "../../../../../repository";
import AddressItem from "../../../../profileV2/address/addressItem/AddressItem";


export default function Address() {
    const {data, error} = useSWR(...ControllerUser.User.Profile.Address.get())
    const {address: ad} = data || {};


    function handleChangeAddress(){

    }


    if (error)
        return <React.Fragment/>

    if (!data)
        return <Placeholder/>

    return (
        <Box width={1} flexDirection={'column'}>
            <Typography variant={'body1'} pb={5} fontWeight={400} color={colors.textColor.h757575}>
                {lang.get("select_the_order_delivery_address")}
            </Typography>
            {
                ad?.id ?
                    <AddressItem address={ad} onRequestChangeAddress={handleChangeAddress}/> :
                    <NoneAddress onClick={handleChangeAddress}/>
            }
        </Box>
    )
}


