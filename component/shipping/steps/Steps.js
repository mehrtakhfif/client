import React, {useMemo} from "react";
import {useRouter} from "next/router";
import rout from "../../../router";
import {Box, Typography} from "material-ui-helper";
import {Tab as MaterialTab, Tabs} from "@material-ui/core";
import {colors, lang} from "../../../repository";
import withStyles from "@material-ui/core/styles/withStyles";


export default function Steps() {
    const router = useRouter();
    const step = router?.query?.step?.[0] || "";


    const isDetails = step === rout.User.Basket.Shopping.Params.Details.param;
    const isPayment = step === rout.User.Basket.Shopping.Params.Payment.param;
    const isInvoice = step === rout.User.Basket.Shopping.Params.Invoice.param;
    const isFail = step === rout.User.Basket.Shopping.Params.Fail.param;

    const handleChange = (event, newValue) => {

    };


    if (isFail)
        return <React.Fragment/>


    const activeStep = useMemo(() => {
        if (isPayment)
            return 1
        if (isInvoice)
            return 2
        return 0
    }, [step])

    return (
        <Box justifyContent={'center'} width={'75%'} alignSelf={'center'}>
                <Tabs
                    value={activeStep}
                    indicatorColor="secondary"
                    textColor="secondary"
                    onChange={handleChange}
                    variant="fullWidth"
                    centered
                    aria-label="shipping tabs"
                    style={{width: '100%'}}>
                    <Tab step={0} isActive={isDetails} label={"sending_details"}/>
                    <Tab step={1} isActive={isPayment} label={"payment_information"}/>
                    <Tab step={2} isActive={isInvoice} label={"payment_confirmation"}/>
                </Tabs>
        </Box>
    )
}


const MyTabs = withStyles({
    root: {
        opacity: `1 !important`,
        width: "33%",
        borderBottom: `2px solid ${colors.borderColor.heff0ef}`
    },
})(MaterialTab);


function Tab({step, isActive, label,}) {

    return (
        <MyTabs
            disabled={true}
            disableRipple
            label={(
                <Typography variant={"body2"} fontWeight={400}
                            color={isActive ? colors.textColor.h3278C9 : colors.textColor.h8d8d8d}>
                    {lang.get(label)}
                </Typography>
            )}/>
    )
}