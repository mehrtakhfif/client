import React from "react";
import {AppBar, Box} from "material-ui-helper";
import {Toolbar, useTheme} from "@material-ui/core";
import Logo from "../header/headerLgV2/headerV2/Logo";


export default function Header() {
const theme = useTheme()

    return (
        <React.Fragment>
            <AppBar>
                <Box center={true} width={1}>
                    <Logo
                    minHeight={theme.spacing(7)}
                        justifyContent={'center'}
                          imageRootProps={{ml:0}}/>
                </Box>
            </AppBar>
            <Toolbar/>
        </React.Fragment>
    )
}