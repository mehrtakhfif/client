import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import React from "react";
import Hidden from "@material-ui/core/Hidden";
import {PlaceHolder as CheckoutSummaryPlaceHolder} from "./CheckoutSummary";
import {makeStyles} from "@material-ui/core";
import PropTypes from "prop-types";
import Loadable from "react-loadable";
import {UtilsStyle} from "../../utils/Utils";
import {theme} from "../../repository";
import clsx from "clsx";
import {useBasketCountContext} from "../../context/BasketCount";


const useStyles = makeStyles(theme => (
    {
        root: (props => ({
            transition: 'transform .3s,-webkit-transform .3s',
            position: 'sticky',
            overflow: 'scroll',
            top: !props.showDefaultHeader ? 80 : 140,
            ...UtilsStyle.transition(200),
        }))
    }));

export default function ShoppingDetailsCard(
    {
        amountPayable,
        totalAmount,
        totalProfit,
        shippingCost,
        buttonText,
        onButtonClick,
        checkoutSummary_style,
        count = null,
        max_shipping_time,
        disable,
        style,
        ...props
    }) {
    const classes = useStyles();
    const basketCount = useBasketCountContext();


    return (
        <Hidden smDown>
            <Box width='30%' ml={2} style={style}
                 className={clsx(classes.root, 'removeScroll')}>
                <Card
                    style={{
                        transition: 'transform .3s,-webkit-transform .3s',
                        top: 20,
                        marginBottom: theme.spacing(1)
                    }}>
                    <CheckoutSummary
                        amountPayable={amountPayable}
                        totalAmount={totalAmount}
                        totalProfit={totalProfit}
                        shippingCost={shippingCost}
                        max_shipping_time={max_shipping_time}
                        count={count ? count : basketCount}
                        disable={disable}
                        buttonText={buttonText}
                        onButtonClick={onButtonClick}
                        style={{
                            ...checkoutSummary_style,
                            padding: theme.spacing(2),
                        }}/>
                </Card>
            </Box>
        </Hidden>
    )
}

ShoppingDetailsCard.propTypes = {
    totalAmount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    totalProfit: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    shippingCost: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    amountPayable: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    buttonText: PropTypes.string,
    onButtonClick: PropTypes.func.isRequired,
    checkoutSummary_style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};


const CheckoutSummary = Loadable.Map({
    loader: {
        Component: () => import('./CheckoutSummary'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <Box width='95%'>Error! <button onClick={props.retry}>Retry</button></Box>;
        }
        return <CheckoutSummaryPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component {...props}/>;
    },
});
