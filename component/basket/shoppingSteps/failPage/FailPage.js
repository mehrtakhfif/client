import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import {GA, lang} from "../../../../repository";
import rout from "../../../../router";
import {getPageTitle} from "../../../../headUtils";
import Head from "next/head";


export default function FailPage({...props}) {
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Basket.Shopping.Params.Fail.rout});
    },[]);
    return(
        <Box>
            <Head>
                <title>{getPageTitle(lang.get("fail"))}</title>
            </Head>
            fail
        </Box>
    )
}
