import React from 'react'
import {makeStyles, useTheme} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import {colors, lang, media} from "../../../repository";
import Divider from "@material-ui/core/Divider";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "../../base/Typography";
import {FiberManualRecord} from "@material-ui/icons";
import {grey} from "@material-ui/core/colors";
import Img from "../../base/Img";
import {UtilsStyle} from "material-ui-helper";

//region BaseClass
const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        width: '100%',
        "&:before": {
            content: "''",
            width: '100%',
            height: 4,
            backgroundColor: colors.primary.main,
            display: 'block'
        }
    },
    boxRoot: {},
    scrollHorizontal: {
        overflow: 'auto',
        whiteSpace: 'nowrap',
        '&::-webkit-scrollbar': {
            height: 7,
        },
        '&::-webkit-scrollbar-track': {
            backgroundColor: '#f1f1f1',
            ...UtilsStyle.borderRadius(5),
        },
        '&::-webkit-scrollbar-thumb': {
            background: '#888',
            ...UtilsStyle.borderRadius(5),
        },
        '&::-webkit-scrollbar-thumb:hover': {
            background: '#555'
        }
    }
}));
export default function BasketSummery({products, ...props}) {
    //region Variable
    const theme = useTheme();
    const classes = useStyles(props);

    //endregion Variable
    return (
        <Card className={classes.root}>
            <Box
                className={classes.boxRoot}
                display='flex'
                flexDirection='column'>
                <Typography py={1}
                            px={2}
                            width={1}
                            variant={"subtitle2"}
                            component="h6"
                            fontWeight={400}>
                    {lang.get("submitted_product")}
                </Typography>
                <Divider/>
                <Box className={classes.scrollHorizontal} pt={2} pb={1} px={2}>
                    {
                        products.map(({count, product}, index) => (
                            <Item key={index} features={product?.features} title={product?.storageTitle}
                                  imgSrc={product?.thumbnail}/>
                        ))
                    }
                </Box>
            </Box>
        </Card>
    )
}
//endregion BaseClass

//region Component
//region Item

// text-align: right;
// overflow: hidden;
// white-space: nowrap;
// text-overflow: ellipsis;
// display: block;\

const useItemStyles = makeStyles(theme => ({
    root: {},
    title: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        cursor: 'default'
    },
    titleTooltip: {}
}));

function Item({title = "", features, imgSrc, ...props}) {
    //region Variable
    const theme = useTheme();
    const classes = useItemStyles(props);
    //endregion Variable

    const TitleElement = () => (
        <Typography
            px={1} my={1}
            variant={'body1'}
            component='span'
            style={{
                display: 'block',
                ...UtilsStyle.limitTextLine(2)
            }}>
            {title}
        </Typography>
    );

    return (
        <Box display='flex'
             flexDirection='column'
             boxShadow={1}
             width={{
                 xs: 0.8,
                 md: 0.3
             }}
             component={Card}
             style={{
                 display: 'inline-block',
                 textAlign: 'center',
                 textDecoration: 'none',
                 marginLeft: theme.spacing(2)
             }}>
            <Img
                src={imgSrc?.image || imgSrc}
                imageWidth={media.thumbnail.width}
                imageHeight={media.thumbnail.height}
                alt={title}/>
            {title?.length > 42 ?
                <Tooltip title={title}
                         placement="bottom"
                         className={classes.titleTooltip}>
                    <Box>
                        <TitleElement/>
                    </Box>
                </Tooltip> :
                <TitleElement/>
            }
            {
                _.isArray(features) &&
                <Box display={'flex'} flexWrap={'wrap'}>
                    {
                        features.map((f, i) => (
                            <React.Fragment key={f.id}>
                                {i > 0 && lang.get("comma")}
                                <Box display={'flex'} pr={0.3} pl={0.5} pb={0.5}>
                                    {f?.feature}
                                    <Box pl={0.5} display={'flex'} alignItems={'center'}>
                                        {"( "}{
                                        f?.settings?.color &&
                                        <FiberManualRecord
                                            fontSize={"small"}
                                            style={{
                                                color: f.settings.color,
                                                border: `1px solid ${grey[300]}`,
                                                marginLeft: theme.spacing(0.4),
                                                ...UtilsStyle.borderRadius('100%')
                                            }}/>
                                    }
                                        {f?.feature_value}
                                        {" )"}
                                    </Box>
                                </Box>
                            </React.Fragment>
                        ))
                    }
                </Box>
            }
        </Box>
    )
}

//endregion Component
//endregion Item
