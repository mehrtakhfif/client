import React, {useEffect, useRef} from 'react'
import Box from "@material-ui/core/Box";
import {makeStyles, useTheme} from "@material-ui/core";
import {colors, GA, lang, setPageTitle, theme} from "../../../../repository";
import rout from "../../../../router";
import Card from "@material-ui/core/Card";
import Divider from "@material-ui/core/Divider";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import {cyan, grey} from "@material-ui/core/colors";
import {UtilsConverter, UtilsStyle} from "../../../../utils/Utils";
import BaseButton from "../../../base/button/BaseButton";
import {Melat} from "../../../IPGImage";
import _ from 'lodash'
import MtFormControl from "@material-ui/core/FormControl";
import FormControl from '../../../base/formController/NewFormController'
import {getPageTitle} from "../../../../headUtils";
import Head from "next/head";
import Typography from "../../../base/Typography";
import TextField from "../../../base/textField/TextField";
import TextFieldContainer, {errorList} from "../../../base/textField/TextFieldContainer";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import NewMeliCodeTextField from "../../../base/textField/MeliCodeTextField";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ControllerUser from "../../../../controller/ControllerUser";

const useStyles = makeStyles(theme => ({
    card: {
        position: 'relative',
        "&:before": {
            content: "''",
            width: '100%',
            height: 4,
            backgroundColor: colors.primary.main,
            display: 'block'
        },
        [theme.breakpoints.down('md')]: {
            marginRight: '0 !important',
            marginLeft: '0 !important'
        }
    }
}));
export default function PaymentPage({
                                        firstName,
                                        lastName,
                                        meliCode,
                                        email,
                                        personalDataIsActive,
                                        hasError,
                                        onChangePersonalData,
                                        personalDataRef,
                                        ipgs,
                                        disable,
                                        activeIpg,
                                        onDiscountCodeSet,
                                        onChangeIpg,
                                        ...props
                                    }) {

    useEffect(() => {
        setPageTitle({title: lang.get("payment")});
        GA.initializePage({pageRout: rout.User.Basket.Shopping.Params.Payment.rout});
    }, []);
    //region Variable
    const classes = useStyles(props);

    //endregion Variable
    return (
        <Box display='flex' width={1}
             flexWrap='wrap'>
            <Head>
                <title>{getPageTitle(lang.get("payment"))}</title>
            </Head>
            <UserRequireDetails
                firstName={firstName}
                lastName={lastName}
                meliCode={meliCode}
                email={email}
                disable={disable}
                personalDataIsActive={personalDataIsActive}
                onChangePersonalData={onChangePersonalData}
                formControlRef={personalDataRef}/>
            <Box width={{
                lg: '60% !important',
                xs: '100%'
            }} mb={2}>
                <Card
                    elevation={3}
                    className={classes.card}
                    style={{
                        marginLeft: theme.spacing(1),
                        height: '100%'
                    }}>
                    <OnlinePayment
                        ipgs={ipgs}
                        activeIpg={activeIpg}
                        onChangeIpg={event => onChangeIpg(event.target.value)}/>
                </Card>
            </Box>
            <Box width={{
                lg: '40% !important',
                xs: '100%'
            }} mb={2}>
                <Card
                    className={classes.card}
                    style={{
                        marginRight: theme.spacing(1)
                    }}>
                    <DiscountCode onSet={onDiscountCodeSet}/>
                </Card>
            </Box>
        </Box>
    )
}


//region Component
function UserRequireDetails({
                                firstName,
                                lastName,
                                meliCode,
                                email,
                                personalDataIsActive,
                                formControlRef,
                                disable,
                                onChangePersonalData,
                                ...props
                            }) {

    const classes = useStyles(props);
    return (
        !personalDataIsActive ? (
                <Box width={1} mb={2}>
                    <Card
                        elevation={3}
                        className={classes.card}>
                        <Box width={1}>
                            <Box py={1}
                                 px={2}
                                 component="h6"
                                 fontWeight={400}
                                 fontSize="body1.fontSize">
                                {lang.get("profile_complete")}
                            </Box>
                            <Divider/>
                            <FormControl
                                display='flex'
                                flexWrap='wrap'
                                width='100%'
                                innerref={formControlRef}
                                onChange={() => {

                                }}>
                                <Box
                                    width={1}
                                    display='flex'
                                    flexWrap='wrap'
                                    px={2}
                                    py={2}>
                                    <Box width='50%' my={1}>
                                        <Box width={"95%"}>
                                            <TextFieldContainer
                                                name={"firstName"}
                                                defaultValue={firstName}
                                                render={(ref, {
                                                    name: inputName,
                                                    initialize,
                                                    valid,
                                                    errorIndex,
                                                    setValue,
                                                    inputProps,
                                                    style,
                                                    props
                                                }) => {
                                                    return (
                                                        <TextField
                                                            {...props}
                                                            error={!valid}
                                                            name={inputName}
                                                            required={true}
                                                            helperText={errorList[errorIndex]}
                                                            inputRef={ref}
                                                            fullWidth
                                                            label={lang.get('first_name')}
                                                            style={{
                                                                ...style,
                                                            }}/>
                                                    )
                                                }}/>
                                        </Box>
                                    </Box>
                                    <Box width='50%' my={1}>
                                        <Box width={"95%"}>
                                            <TextFieldContainer
                                                name={"lastName"}
                                                defaultValue={lastName}
                                                render={(ref, {
                                                    name: inputName,
                                                    initialize,
                                                    valid,
                                                    errorIndex,
                                                    setValue,
                                                    inputProps,
                                                    style,
                                                    props
                                                }) => {
                                                    return (
                                                        <TextField
                                                            {...props}
                                                            error={!valid}
                                                            name={inputName}
                                                            helperText={errorList[errorIndex]}
                                                            inputRef={ref}
                                                            fullWidth
                                                            required={true}
                                                            label={lang.get('last_name')}
                                                            style={{
                                                                ...style,
                                                                minWidth: '90%'
                                                            }}/>
                                                    )
                                                }}/>
                                        </Box>
                                    </Box>
                                    <Box width='50%' my={1}>
                                        <Box width={"95%"}>
                                            <NewMeliCodeTextField
                                                required={true}
                                                defaultValue={meliCode}
                                                name={"meliCode"}/>
                                        </Box>
                                    </Box>
                                    <Box width='50%' my={1}>
                                        <Box width={"95%"}>
                                            <TextFieldContainer
                                                name={"email"}
                                                errorPatterns={['$^|^(([^<>()\\[\\]\\.,;:\\s@\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})$']}
                                                defaultValue={email}
                                                render={(ref, {
                                                    name: inputName,
                                                    initialize,
                                                    valid,
                                                    errorIndex,
                                                    setValue,
                                                    inputProps,
                                                    style,
                                                    props
                                                }) => {
                                                    return (
                                                        <TextField
                                                            {...props}
                                                            error={!valid}
                                                            name={inputName}
                                                            helperText={[[lang.get("er_verify_email")]][errorIndex]}
                                                            inputRef={ref}
                                                            fullWidth
                                                            label={lang.get('email')}
                                                            style={{
                                                                ...style,
                                                                minWidth: '90%'
                                                            }}/>
                                                    )
                                                }}/>
                                        </Box>
                                    </Box>
                                </Box>
                            </FormControl>
                        </Box>
                    </Card>
                </Box>
            )
            :
            <></>
    )

}

//region OnlinePayment
function OnlinePayment({ipgs, activeIpg, onChangeIpg, ...props}) {
    //region Variable
    const theme = useTheme();
    //endregion Variable
    return (
        <Box
            display="flex"
            flexDirection='column'
            style={{
                height: '100%'
            }}>
            <Typography py={1}
                 px={2}
                 component="h6"
                 fontWeight={400}
            variant={"body2"}>
                {lang.get("online_payment")}
            </Typography>
            <Divider/>
            <Typography
                px={2}
                pt={2}
                pb={1}
                variant={"body1"}
                fontWeight={400}>
                {lang.get('me_select_ipg')}
            </Typography>
            <Box display='flex'
                 width={1}
                 py={1}
                 flexWrap='wrap'
                 alignItems="center"
                 style={{
                     height: '100%'
                 }}>
                <MtFormControl component="fieldset">
                    <RadioGroup aria-label="position" name="position" value={_.toString(activeIpg)}
                                onChange={onChangeIpg} row>
                        {
                            ipgs.map((ipg, index) => {
                                return (
                                    <OnlinePaymentItem
                                        key={index}
                                        item={ipg}/>)
                            })
                        }
                    </RadioGroup>
                </MtFormControl>
            </Box>
        </Box>
    )
}

//region OnlinePaymentItem
function OnlinePaymentItem({item, ...props}) {
    //region Variable
    const {id, disable, hide, name, ...itemProps} = item;
    //endregion Variable
    return (
        (!hide) &&
        <Box
            my={1}
            mx={1}
            style={{
                border: '1px solid',
                borderColor: grey[700],
                ...UtilsStyle.borderRadius(25),
            }}>
            <FormControlLabel
                value={_.toString(id)}
                disabled={disable}
                control={
                    <Radio color="primary"
                           style={{
                               padding: theme.spacing(0.5)
                           }}/>}
                label={
                    (
                        <Box display='flex' alignItems='center' pr={1}>
                            <Box pr={1}>
                                {name}
                            </Box>
                            <Melat/>
                        </Box>
                    )
                }
                labelPlacement="end"
                style={{
                    margin: 0,
                    paddingLeft: theme.spacing(1)
                }}
            />
        </Box>
    )
}

//endregion OnlinePaymentItem
//endregion OnlinePayment

//region DiscountCode
const useDiscountCodeStyles = makeStyles(theme => ({
    button: {
        [theme.breakpoints.down('xs')]: {
            width: '95%',
        },
    }
}));

function DiscountCode({onSet, ...props}) {
    //region Variable
    const ref = useRef()
    const theme = useTheme();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const classes = useDiscountCodeStyles(props);
    const [loading, setLoading] = React.useState(false);

    //endregion Variable

    function handleSet() {
        const {discountCode} = ref.current.serialize();

        if (!discountCode) {
            reqCancel("کدتخفیف را وارد کنید.")
            return
        }
        setLoading(true)
        ControllerUser.Basket.setDiscountCode({code: discountCode}).then(res => {
            onSet()
        }).finally(() => {
            reqCancel()
        })
    }

    function reqCancel(text) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <React.Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </React.Fragment>
                    )
                });
        setLoading(false)
    }

    return (
        <Box
            display="flex"
            flexDirection='column'
            width={1}>
            <Typography py={1}
                 px={2}
                 component="h6"
                 fontWeight={400}
            variant={"body2"}>
                {lang.get("use_discount_code")}
            </Typography>
            <Divider/>
            <Box display='flex'
                 flexDirection='column'
                 py={1}>
                <Box
                    px={2}
                    pb={1}
                    fontSize={UtilsConverter.addRem(theme.typography.body2.fontSize, 0.9)}
                    fontWeight={300}>
                    با ثبت کد تخفیف، مبلغ کد تخفیف از "از مبلغ قابل پرداخت" کسر میشود.
                </Box>
                <FormControl
                    innerref={ref}
                    name={"discount_code"}
                    display='flex'
                    flexWrap='wrap'
                    px={2}>
                    <Box width={{
                        sm: "60% !important",
                        xs: '100%'
                    }}
                         py={1}
                         display='flex'
                         justifyContent='center'
                         alignItems="flex-end">
                        <TextFieldContainer
                            name={"discountCode"}
                            defaultValue={""}
                            render={(ref, {
                                name: inputName,
                                initialize,
                                valid,
                                errorIndex,
                                setValue,
                                inputProps,
                                style,
                                props
                            }) => {
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        name={inputName}
                                        helperText={errorList[errorIndex]}
                                        inputRef={ref}
                                        fullWidth
                                        label={lang.get("discount_code")}
                                        style={{
                                            ...style,
                                            minWidth: '90%'
                                        }}/>
                                )
                            }}/>
                    </Box>
                    <Box width={{
                        sm: "40% !important",
                        xs: '100%'
                    }}
                         py={1}
                         display='flex'
                         justifyContent='center'
                         alignItems="flex-end">
                        <BaseButton size='small' variant={"outlined"}
                                    className={classes.button}
                                    onClick={handleSet}
                                    style={{
                                        borderColor: cyan[300]
                                    }}>
                            ثبت کد تخفیف
                        </BaseButton>
                    </Box>
                </FormControl>
            </Box>
            <Backdrop open={loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: theme.palette.primary.main,
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Box>
    )
}

//endregion DiscountCode
//endregion Component
