import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepConnector from '@material-ui/core/StepConnector';
import Hidden from "@material-ui/core/Hidden";
import {lang} from "../../../repository";
import {UtilsStyle} from "../../../utils/Utils";

const ColorLibConnector = withStyles({
    alternativeLabel: {
        top: 22,
    },
    active: {
        '& $line': {
            backgroundImage:
                'linear-gradient( 95deg,rgb(237, 160, 160) 0%,rgb(243, 109, 109) 50%,rgb(255, 0, 19) 100%);',
        },
    },
    completed: {
        '& $line': {
            backgroundImage:
                'linear-gradient( 95deg,rgb(237, 160, 160) 0%,rgb(243, 109, 109) 50%,rgb(255, 0, 19) 100%)',
        },
    },
    line: {
        height: 3,
        border: 0,
        backgroundColor: '#eaeaf0',
        ...UtilsStyle.borderRadius(1),
    },
})(StepConnector);

const useColorLibStepIconStyles = makeStyles({
    root: {
        backgroundColor: '#ccc',
        zIndex: 1,
        color: '#fff',
        width: 50,
        height: 50,
        display: 'flex',
        ...UtilsStyle.borderRadius("50%"),
        justifyContent: 'center',
        alignItems: 'center',
    },
    active: {
        // backgroundImage:
        //     'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
        backgroundColor: '#ebebeb',
        boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    },
    completed: {
        // backgroundImage:
        //     'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
        backgroundColor: '#ebebeb',
    },

});

function ColorLibStepIcon(props) {
    const classes = useColorLibStepIconStyles();
    const {active, completed} = props;
    const style = {
        width: 32,
    };
    const icons = {
        1: <img alt={'shopping_details'} src={'/drawable/svg/shopping/shopping_details.svg'} style={style}/>,
        2: <img alt={'shopping_payment'} src={'/drawable/svg/shopping/shopping_payment.svg'} style={style}/>,
        3: <img alt={'shopping_factor'} src={'/drawable/svg/shopping/shopping_factor.svg'} style={style}/>,
    };

    return (
        <div
            className={clsx(classes.root, {
                [classes.active]: active,
                [classes.completed]: completed,
            })}>
            {icons[String(props.icon)]}
        </div>
    );
}

ColorLibStepIcon.propTypes = {
    active: PropTypes.bool,
    completed: PropTypes.bool,
    icon: PropTypes.node,
};

const useStyles = makeStyles(theme => ({
    stepperRoot: {
        width: '90%',
        '& [class*="MuiStepLabel-root"]': {
            backgroundColor: '#fff0 !important',
            '&.Mui-disabled [class*="MuiStepLabel-iconContainer"] *': {
                fill: 'rgba(0, 0, 0, 0.54)'
            }
        }
    },
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

function getSteps(addressRequired) {
    const array = [];
    if (addressRequired)
        array.push(lang.get('sending_details'));
    array.push(lang.get('payment'));
    array.push(lang.get('shipping_end_and_show_factor'));
    return array;
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return 'Select campaign settings...';
        case 1:
            return 'What is an ad group anyways?';
        case 2:
            return 'This is the bit I really care about!';
        default:
            return 'Unknown step';
    }
}

export default function StepperCom({activeStep, addressRequired, ...props}) {
    const classes = useStyles();
    // const [state, setState] = React.useState({
    //     activeStep: activeStep
    // });
    const steps = getSteps(addressRequired);

    return (
        <div className={classes.stepperRoot}>
            <Stepper
                alternativeLabel
                activeStep={activeStep}
                connector={<ColorLibConnector/>}
                style={{
                    backgroundColor: '#fff0'
                }}>
                {steps.map(label => (
                    <Step key={label}>
                        <StepLabel StepIconComponent={ColorLibStepIcon}>
                            <Hidden xsDown>{label}</Hidden>
                        </StepLabel>
                    </Step>
                ))}
            </Stepper>
        </div>
    );
}
StepperCom.propTypes = {
    activeStep: PropTypes.number.isRequired,
};
