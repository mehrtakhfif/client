import React, {createRef, Fragment, useEffect, useRef, useState} from 'react'
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import {createTheme, makeStyles, withStyles} from "@material-ui/core";
import {colors, lang} from '../../../../repository'
import Divider from "@material-ui/core/Divider";
import {blue, cyan, grey, lightGreen} from "@material-ui/core/colors";
import {ScrollToRef, Utils, UtilsConverter, UtilsStyle} from "../../../../utils/Utils";
import BaseButton from "../../../base/button/BaseButton";
import {Call, Clear, Edit, MarkunreadMailbox, Place, Save} from '@material-ui/icons'
import Hidden from "@material-ui/core/Hidden";
import PropTypes from "prop-types";
import NameTextField from "../../../base/textField/base/NameTextField";
import FormControl from "../../../base/formController/FormController";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import Tooltip from "@material-ui/core/Tooltip";
import {FullScreenMap, SelectAddress} from '../../../../pages/add-address'
import {states as stateItems} from '../../../../controller/converter'
import MobileTextField from "../../../base/textField/base/MobileTextField";
import PostalCodeTextField from "../../../base/textField/base/PostalCodeTextField";
import AddressTextField from "../../../base/textField/base/AddressTextField";
import Radio from "@material-ui/core/Radio";
import _ from 'lodash'
import Skeleton from "@material-ui/lab/Skeleton";
import BoxWithTopBorder from "../../../base/textHeader/BoxWithTopBorder";
import Typography from "../../../base/Typography";
import ControllerUser from "../../../../controller/ControllerUser";
import Img from "../../../base/oldImg/Img";

//region BaseClass
const useStyles = makeStyles(theme => ({
    root: {},
}));

export default function AddressItems({addresses, defaultAddress, onChangeAddress, ...props}) {
    //region Variable
    const theme = createTheme();
    const [state, setState] = useState({
        addresses: addresses,
        defaultAddress: defaultAddress,
        selectAddress: false,
        editAddress: false,
        loading: false
    });
    useEffect(() => {
        // onChangeAddress(defaultAddress)
    });
    const [editAddress, setEditAddress] = useState({
        data: {},
    });
    const formAddress = useRef();


    //endregion Variable

    function onStateChange(props) {

        try {
            if (!(Object.keys(props).length === 1 && Object.keys(props)[0] === ' loading'))
                ScrollToRef(formAddress);
        } catch (e) {
        }
        if (props.loading) {
            setState({
                ...state,
                loading: true
            });
            return;
        }
        const newState = {
            ...state,
            loading: false
        };
        if (props.addNewAddress) {
            const newList = newState.addresses;

            setState({
                ...newState,
                addresses: Utils.insert(newList, 0, props.addNewAddress),
                selectAddress: false,
                editAddress: false,
            });
            // onChangeAddress(props.addNewAddress);
            return
        }
        if (props.changeDefaultAddress) {
            setState({
                ...newState,
                selectAddress: false,
                editAddress: false,
                defaultAddress: props.changeDefaultAddress
            });
            return
        }
        if (props.toEditAddress) {
            setEditAddress({
                ...editAddress,
                data: props.toEditAddress,
            })
        }
        setState({
            ...newState,
            ...props,
        });
    }

    return (
        <Box
            ref={formAddress}
            style={{
                position: 'relative'
            }}>
            {
                (state.selectAddress && !state.editAddress) &&
                <Box
                    fontSize='h6.texSize'
                    display='flex'
                    px={3}
                    py={1}
                    component='a'
                    alignItems='center'
                    onClick={() => {
                        onStateChange({
                            ...state,
                            editAddress: true,
                            toEditAddress: {
                                id: -1,
                                name: '',
                                state: null,
                                city: null,
                                phone: '',
                                postalCode: '',
                                address: '',
                                location: null
                            },
                        })
                    }}
                    style={{
                        position: 'absolute',
                        left: 0,
                        top: -45,
                        backgroundColor: lightGreen[400],
                        ...UtilsStyle.borderRadius(26),
                    }}>
                    <Box
                        pl={2}
                        style={{
                            color: '#fff'
                        }}>
                        {lang.get('add_new_address')}
                    </Box>
                    <img alt={'homeAddress'}
                         src={'/drawable/svg/homeAddress.svg'}
                         style={{
                             width: 25,
                             height: 25
                         }}/>
                </Box>
            }
            <>
                {
                    (!(state.selectAddress || state.editAddress)) ?
                        (<Item
                            address={state.defaultAddress}
                            selectAddress={state.selectAddress}
                            editAddress={state.editAddress}
                            loading={state.loading}
                            onStateChange={(val) => {
                                onStateChange({
                                    ...state,
                                    ...val
                                })
                            }}/>)
                        :
                        (!state.editAddress) ?
                            (
                                state.addresses.map(address => (
                                    <Item
                                        key={address.id}
                                        address={address}
                                        selectAddress={state.selectAddress}
                                        editAddress={state.editAddress}
                                        loading={state.loading}
                                        checked={address.id === state.defaultAddress.id}
                                        onStateChange={onStateChange}/>
                                ))
                            )
                            :
                            (
                                <Item
                                    address={editAddress.data}
                                    selectAddress={state.selectAddress}
                                    editAddress={state.editAddress}
                                    loading={state.loading}
                                    onStateChange={onStateChange}/>
                            )
                }
            </>
        </Box>
    )
};

AddressItems.propTypes = {
    addresses: PropTypes.array,
    defaultAddress: PropTypes.object,
    onChangeAddress: PropTypes.func
};

//endregion BaseClass
//region Component
//region AddressItems
const useItemAddressStyles = makeStyles(theme => ({
    boxRoot: {
        position: 'relative',
        ...UtilsStyle.transition(500),
        '&[selectable]:hover': {
            backgroundColor: '#00000012',
            ...UtilsStyle.transition(500),
            "&>div:first-child:before": {
                backgroundColor: cyan[400]
            }
        }
    },
    buttonContainer: {
        position: 'absolute',
        bottom: theme.spacing(1.5),
        left: theme.spacing(1.5),
    },
    button: {
        color: grey[600],
        fontWeight: 400,
        fontSize: UtilsConverter.addRem(theme.typography.body2.fontSize, 0.78),
    },
}));

function Item({address, checked, selectAddress, editAddress, loading = false, onStateChange, ...props}) {
    const theme = createTheme();
    const classes = useItemAddressStyles(props);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [addressDetails, setAddressDetails] = useState({
        data: address,
        backupData: address
    });
    const formRef = createRef();

    function handleCardClickHandler() {
        if (!selectAddress || editAddress)
            return;
        onStateChange({
            loading: true
        });
        ControllerUser.User.Profile.Addresses.setDefault({address_id: address.id}).then(() => {
            onStateChange({
                selectAddress: false,
                editAddress: false,
                defaultAddress: address
            })
        }).catch(() => {
            onStateChange({
                loading: false
            });
            enqueueSnackbar(lang.get("er_error"),
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        })
    }

    function getState() {
        if (_.isEmpty(addressDetails.data.state))
            return null
        return stateItems[_.findLastIndex(stateItems, function (state) {
            return state.id === addressDetails.data.state.id
        })]
    }


    return (
        <Card
            style={{
                width: '100%',
                marginBottom: theme.spacing(2),
                position: 'relative'
            }}>
            {
                loading &&
                <Skeleton variant="rect" width={"100%"} height={"100%"}
                          style={{
                              position: 'absolute',
                              top: 0,
                              bottom: 0,
                              backgroundColor: '#0000007a'
                          }}/>
            }
            <FormControl
                innerref={formRef}
                className={classes.boxRoot}
                {...((selectAddress && !editAddress) ? {selectable: ""} : null)}
                display='flex'
                flexDirection='column'
                onClick={handleCardClickHandler}
                style={{
                    cursor: (selectAddress && !editAddress) ? 'pointer' : 'default',
                    '&:hover': {
                        backgroundColor: '#000'
                    }
                }}>
                <CardHeader
                    username={addressDetails.data.name}
                    onUserNameChange={(value) => {
                        setAddressDetails({
                            ...addressDetails,
                            data: {
                                ...addressDetails.data,
                                name: value
                            }
                        })
                    }}
                    selectAddress={selectAddress}
                    editAddress={editAddress}
                    onChangeAddress={(e) => {
                        e.stopPropagation();
                        onStateChange({
                            selectAddress: true
                        })
                    }}/>
                <Divider/>
                <Box display='flex' alignItems='center'>
                    {
                        (selectAddress && !editAddress) &&
                        <RadioButton checked={checked}/>
                    }
                    <CardBody
                        selectAddress={selectAddress}
                        editAddress={editAddress}
                        state={getState()}
                        city={addressDetails.data.city}
                        phone={addressDetails.data.phone}
                        address={addressDetails.data.address}
                        postalCode={addressDetails.data.postalCode}
                        location={addressDetails.data.location}
                        onChange={(val) => {
                            setAddressDetails({
                                ...addressDetails,
                                data: {
                                    ...addressDetails.data,
                                    address: val.address,
                                    state: val.state ? val.state : null,
                                    city: val.city,
                                    phone: val.phone,
                                    postalCode: val.postalCode,
                                    location: val.location
                                }
                            });
                        }}/>
                </Box>
                <ActionButton
                    disable={loading}
                    selectAddress={selectAddress}
                    editAddress={editAddress}
                    handleEditAddressClick={(e) => {
                        e.stopPropagation();
                        onStateChange({
                            editAddress: true,
                            toEditAddress: address,
                        })
                    }}
                    handleSubmitAddressClick={(e) => {
                        e.stopPropagation();
                        if (formRef.current.attributes.haserror) {
                            enqueueSnackbar(lang.get("er_fill_all_inputs"),
                                {
                                    variant: "error",
                                    action: (key) => (
                                        <Fragment>
                                            <Button onClick={() => {
                                                closeSnackbar(key)
                                            }}>
                                                {lang.get('close')}
                                            </Button>
                                        </Fragment>
                                    )
                                });
                            return
                        }
                        try {
                            const data = addressDetails.data;
                            const props = {
                                name: data.name,
                                address: data.address,
                                stateId: data.state.id,
                                cityId: data.city.id,
                                phone: data.phone,
                                postalCode: data.postalCode,
                                location: data.location
                            };
                            onStateChange({loading: true});
                            if (data.id === -1) {
                                ControllerUser.User().Addresses().add({
                                    ...props,
                                    setDefault: true
                                }).then(res => {


                                    onStateChange({
                                        editAddress: false,
                                        selectAddress: false,
                                        addNewAddress: res.data,
                                        loading: false
                                    })
                                }).catch(() => {
                                    onStateChange({loading: false});
                                });
                                return
                            }
                            ControllerUser.User.Profile.Addresses.update({
                                id: data.id,
                                ...props
                            }).then(res => {
                                onStateChange({
                                    editAddress: false,
                                    selectAddress: false,
                                    changeDefaultAddress: data,
                                    loading: false,
                                })
                            }).catch(() => {
                                onStateChange({loading: false});
                            });
                            return;
                        } catch (e) {
                            onStateChange({loading: false});
                            enqueueSnackbar(lang.get("er_server_error"),
                                {
                                    variant: "error",
                                    action: (key) => (
                                        <Fragment>
                                            <Button onClick={() => {
                                                closeSnackbar(key)
                                            }}>
                                                {lang.get('close')}
                                            </Button>
                                        </Fragment>
                                    )
                                });
                        }

                    }}
                    handleCancelClick={(e) => {
                        e.stopPropagation();
                        setAddressDetails({
                            ...addressDetails,
                            data: addressDetails.backupData
                        });
                        onStateChange({
                            editAddress: false
                        })
                    }}/>
            </FormControl>
        </Card>
    )
}

Item.propTypes = {
    address: PropTypes.object.isRequired,
    checked: PropTypes.bool,
    selectAddress: PropTypes.bool.isRequired,
    editAddress: PropTypes.bool,
    onStateChange: PropTypes.func.isRequired,
};
//endregion AddressItems
//region CardHeader

function CardHeader(props) {

    //region Variable
    const {username, onUserNameChange, selectAddress, editAddress, onChangeAddress} = props;
    const theme = createTheme();
    //endregion Variable

    return (
        <BoxWithTopBorder
            alignItems="center">
            <Box
                display="flex"
                width={1}
                flexWrap='wrap'
                flexDirection={{
                    sm: 'row !important',
                    xs: 'column-reverse'
                }}>
                <Box py={1}
                     px={2}

                     flexGrow={{
                         sm: 1
                     }}>
                    {
                        editAddress ?
                            <NameTextField
                                label={lang.get("receiver")}
                                value={username}
                                onTextChange={onUserNameChange}
                                validatable={true}
                                style={{
                                    minWidth: '30%'
                                }}
                                isRequired/>
                            :
                            <Typography variant={"h6"} component={'h4'} py={0.5}>
                                {lang.get("receiver_withparam_receiver", {
                                    args: {
                                        receiver: username
                                    }
                                }) + " "}
                            </Typography>
                    }
                </Box>
                {!selectAddress &&
                <Box
                    px={3}
                    py={2}
                    variant='text'
                    fontWeight={400}
                    component='a'
                    fontSize={UtilsConverter.addRem(theme.typography.body1.fontSize, 0.8)}
                    onClick={onChangeAddress}
                    style={{
                        color: cyan[400],
                    }}>
                    {lang.get('change_the_sending_address')}
                </Box>
                }
                {editAddress &&
                <Tooltip title={lang.get('me_address_editing')}
                         placement="bottom">
                    <Box
                        px={2}
                        py={1}
                        flexShrink={0}
                        fontWeight={400}
                        fontSize={UtilsConverter.addRem(theme.typography.body2.fontSize, 0.8)}
                        display='flex'
                        justifyContent='center'
                        alignItems='center'>
                        <Box
                            style={{
                                width: 7,
                                height: 7,
                                backgroundColor: cyan[400],
                                ...UtilsStyle.borderRadius("50%"),
                                marginLeft: theme.spacing(0.7)
                            }}/>
                        {lang.get('editing')}
                    </Box>
                </Tooltip>
                }
            </Box>
        </BoxWithTopBorder>
    )
}

CardHeader.propTypes = {
    username: PropTypes.string,
    onUserNameChange: PropTypes.func,
    selectAddress: PropTypes.bool,
    editAddress: PropTypes.bool,
    onChangeAddress: PropTypes.func.isRequired,
};
//endregion CardHeader
//region CardBody
const useCardBodyStyles = makeStyles(theme => ({
    root: {}
}));

function CardBody(props) {
    const {selectAddress, editAddress, state, city, address, phone, postalCode, location, onChange} = props;
    //region Variable
    const theme = createTheme();
    const classes = useCardBodyStyles(props);
    let callBackVal = props;
    const [map, setMap] = useState({
        showMap: false,
    });
    //endregion Variable
    //region Functions
    function callBack() {
        onChange(callBackVal)
    }

    //endregion Functions
    return (
        <Box
            display="flex"
            flexDirection='column'
            my={[4, 2, 2]}
            mx={2}
            width={1}
            className={classes.root}>
            {
                !editAddress ?
                    <Box
                        mt={1}
                        fontSize='body2.fontSize'
                        fontWeight={300}
                        display='flex'
                        alignItems='center'>
                        <Place style={{paddingLeft: theme.spacing(0.5)}}/>
                        <Typography variant={'subtitle1'} pr={0.5} fontWeight={300} component='p'>
                            آدرس:
                            <b style={{paddingRight: theme.spacing(0.7)}}>{" " + state.label + " -"}</b>
                            <b style={{
                                paddingRight: theme.spacing(0.7),
                                paddingLeft: theme.spacing(0.7),
                            }}>{" " + city.label + " - "}</b>
                            {address}
                        </Typography>
                    </Box>
                    :
                    <>
                        <SelectAddress
                            defaultState={state}
                            defaultCity={city}
                            onChange={(val) => {
                                callBackVal = {
                                    ...callBackVal,
                                    ...val
                                };
                                callBack()
                            }}
                            style={{
                                marginTop: theme.spacing(1),
                                marginBottom: theme.spacing(1)
                            }}/>
                        <AddressTextField
                            value={address}
                            validatable={true}
                            onTextChange={(val) => {
                                callBackVal = {
                                    ...callBackVal,
                                    address: val
                                };
                                callBack()
                            }}
                            style={{
                                marginTop: theme.spacing(1),
                                marginBottom: theme.spacing(1)
                            }}
                            isRequired/>
                    </>
            }
            <Box
                mt={2}
                fontSize='body2.fontSize'
                fontWeight={300}
                component='span'>
                <Box>
                    {
                        editAddress ?
                            <MobileTextField
                                value={phone}
                                validatable={true}
                                onTextChange={(val) => {
                                    callBackVal = {
                                        ...callBackVal,
                                        phone: val
                                    };
                                    callBack()
                                }}
                                style={{
                                    marginTop: theme.spacing(1),
                                    marginBottom: theme.spacing(1),
                                    minWidth: '30%'
                                }}
                                isRequired/>
                            :
                            <Box display='flex' alignItems='center' py={1}>
                                <Call style={{paddingLeft: theme.spacing(0.5)}}/>
                                <Typography variant={'subtitle1'} fontWeight={300} component='span'>
                                    شماره تماس:
                                    <b style={{paddingRight: theme.spacing(0.7)}}>{" " + phone + " "}</b>
                                </Typography>
                            </Box>
                    }
                </Box>

                {
                    <>
                        <Box mt={0.5}>
                            {
                                editAddress ?
                                    <PostalCodeTextField
                                        value={postalCode}
                                        validatable={true}
                                        onTextChange={(val) => {
                                            callBackVal = {
                                                ...callBackVal,
                                                postalCode: val
                                            };
                                            callBack()
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                            marginBottom: theme.spacing(2),
                                            minWidth: '30%'
                                        }}
                                        isRequired/>
                                    :
                                    <Box display='flex' alignItems='center' py={1}>
                                        <MarkunreadMailbox style={{paddingLeft: theme.spacing(0.5)}}/>
                                        <Typography variant={'subtitle1'} fontWeight={300} component='span'>
                                            کدپستی:
                                            <b style={{paddingRight: theme.spacing(0.7)}}>{" " + postalCode + " "}</b>
                                        </Typography>
                                    </Box>
                            }
                        </Box>
                        {editAddress &&
                        <Box mt={1} mb={[3, 2, 2]}>
                            <BaseButton
                                onClick={() => setMap({
                                    ...map,
                                    showMap: true
                                })}
                                style={{
                                    backgroundColor: blue[500],
                                    color: "#fff",
                                }}>
                                <Img
                                    alt={"address-item"}
                                    src={'/drawable/svg/homeAddress.svg'}
                                    style={{
                                        width: 25,
                                        height: 25,
                                        paddingLeft: theme.spacing(1)
                                    }}/>
                                {lang.get('select_location_on_map')}
                            </BaseButton>
                        </Box>}
                        {
                            map.showMap &&
                            <FullScreenMap
                                firstLocation={location}
                                onSubmitClick={(val) => {
                                    callBackVal = {
                                        ...callBackVal,
                                        location: val
                                    };
                                    callBack();
                                    setMap({
                                        ...map,
                                        showMap: false,
                                    })
                                }}
                                onReturnClick={() => {
                                    setMap({
                                        ...map,
                                        showMap: false
                                    })
                                }}/>}
                    </>
                }
            </Box>
        </Box>
    )
}

CardBody.propTypes = {
    selectAddress: PropTypes.bool,
    editAddress: PropTypes.bool,
    state: PropTypes.object,
    city: PropTypes.object,
    address: PropTypes.string,
    phone: PropTypes.string,
    postalCode: PropTypes.string,
    location: PropTypes.object,
    onChange: PropTypes.func.isRequired,
};
//endregion CardBody
//region ActionButton
const useActionButtonStyles = makeStyles(theme => ({
    root: {
        position: 'absolute',
        bottom: theme.spacing(1.5),
        left: theme.spacing(1.5),
    },
    button: {
        color: grey[600],
        fontWeight: 400,
    },
    icon: {
        width: '0.9rem',
        marginLeft: 3
    }
}));

function ActionButton({selectAddress, editAddress, disable = false, handleEditAddressClick, handleSubmitAddressClick, handleCancelClick, ...props}) {
    //region Variable
    const theme = createTheme();
    const classes = useActionButtonStyles(props);
    //endregion Variable
    return (
        <Box className={classes.root}
             display='flex'
             flexDirection='row-reverse'>
            {
                selectAddress &&
                <BaseButton
                    disabled={disable}
                    className={classes.button}
                    onClick={(e) => {
                        if (editAddress) {
                            handleSubmitAddressClick(e);
                            return
                        }
                        handleEditAddressClick(e);
                    }}
                    style={{
                        backgroundColor: editAddress ? colors.success.main : grey[300],
                        color: editAddress ? grey[100] : grey[700],
                    }}>
                    <Hidden xsDown>
                        {editAddress ?
                            <Save className={classes.icon}/> :
                            <Edit className={classes.icon}/>
                        }
                    </Hidden>
                    {editAddress ? lang.get('submit_address') : lang.get("edit_address")}
                </BaseButton>
            }
            {
                editAddress &&
                <BaseButton
                    disabled={disable}
                    className={classes.button}
                    onClick={handleCancelClick}
                    style={{
                        backgroundColor: colors.danger.main,
                        color: grey[100],
                        marginLeft: theme.spacing(1)
                    }}>
                    <Hidden xsDown>
                        <Clear className={classes.icon}/>
                    </Hidden>
                    {lang.get("cancel")}
                </BaseButton>
            }
        </Box>
    )
}

ActionButton.propTypes = {
    selectAddress: PropTypes.bool.isRequired,
    editAddress: PropTypes.bool,
    handleEditAddressClick: PropTypes.func.isRequired,
    handleSubmitAddressClick: PropTypes.func.isRequired,
    handleCancelClick: PropTypes.func.isRequired
};
//endregion ActionButton

const RadioButton = withStyles({
    root: {
        color: cyan[400],
        '&$checked': {
            color: cyan[600],
        },
    },
    checked: {},
})(props => <Radio color="default" {...props} />);
//endregion Component
