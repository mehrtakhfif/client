import React, {useEffect} from 'react'
import Box from "@material-ui/core/Box";
import _ from 'lodash'
import PropTypes from "prop-types";
import {GA, lang} from "../../../../repository";
import rout from '../../../../router';
import {useRouter} from "next/router";
import AddressItems from './AddressItems';
import ControllerUser from "../../../../controller/ControllerUser";
import useSWR from "swr";
import ComponentError from "../../../base/ComponentError";
import PleaseWait from "../../../base/loading/PleaseWait";
import Head from "next/head";
import {getPageTitle} from "../../../../headUtils";

export default function AddressPage({onChangeAddress, ...props}) {
    const router = useRouter()
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Basket.Shopping.Params.Details.rout});
    }, []);

    const d = ControllerUser.User.Profile.Addresses.get({all: true});
    const {data, error} = useSWR(...d, {
        onSuccess: (res) => {
            if (_.isEmpty(res.data)) {
                router.push(rout.User.Profile.AddAddress.create({redirectRout: rout.User.Basket.Shopping.as()}));
            }
        }
    });


    return (
        <React.Fragment>
            <Head>
                <title>{getPageTitle(lang.get("details"))}</title>
            </Head>
            {error ?
                <ComponentError/>
                :
                data ?
                    <Box
                        width={1}
                        display='flex'
                        flexDirection='column'>
                        <AddressItems
                            {...props}
                            addresses={data.data}
                            defaultAddress={data.data[0]}
                            onChangeAddress={props.onChangeAddress}/>
                    </Box> :
                    <PleaseWait/>}
        </React.Fragment>


    )
}

AddressPage.propTypes = {
    defaultAddress: PropTypes.object,
    onChangeAddress: PropTypes.func.isRequired,
};

