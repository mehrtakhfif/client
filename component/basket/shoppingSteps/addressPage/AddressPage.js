import React, {useEffect, useState} from 'react'
import Box from "@material-ui/core/Box";
import _ from 'lodash'
import PropTypes from "prop-types";
import {colors, GA, lang, theme} from "../../../../repository";
import rout from '../../../../router';
import {useRouter} from "next/router";
import ControllerUser from "../../../../controller/ControllerUser";
import useSWR, {mutate} from "swr";
import ComponentError from "../../../base/ComponentError";
import PleaseWait from "../../../base/loading/PleaseWait";
import Head from "next/head";
import {getPageTitle} from "../../../../headUtils";
import {cyan} from "@material-ui/core/colors";
import Divider from "@material-ui/core/Divider";
import {Card, makeStyles, useTheme} from "@material-ui/core";
import {Call, LocationOn, MarkunreadMailbox, Person} from "@material-ui/icons";
import {SmHidden} from "../../../header/Header";
import ProductsContainerHeader from "../../../home/product/productsContainer/ProductsContainerHeader";
import ButtonBase from "@material-ui/core/ButtonBase";
import SelectAddress from "../../../addAddress/SelectAddress";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {Typography, useOpenWithBrowserHistory} from "material-ui-helper";
import AddAddressDialog from "../../../addAddress/AddAddressDialog";
import {redirectTo} from "../../../../middleware/base";


export default function AddressPage({onChangeAddress, ...props}) {

    const router = useRouter();
    const [open, __, handleOpenClick, handleCloseClick] = useOpenWithBrowserHistory("add-address-in-address-page")
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Basket.Shopping.Params.Details.rout});
    }, []);

    const [state, setState] = useState({
        newDefaultId: false
    });
    const [select, setSelect] = useState(false);

    const d = ControllerUser.User.Profile.Addresses.get({all: true});
    const {data: da, error} = useSWR(...d, {
        onSuccess: (res) => {
            // if (_.isEmpty(res.data)) {
            //     router.push(rout.User.Profile.AddAddress.create({redirectRout: rout.User.Basket.Shopping.as()}));
            //     return
            // }
        }
    });

    const {data, defaultAddress} = da ? da : {};

    useEffect(() => {

        if (!state.newDefaultId)
            return;

        ControllerUser.User.Profile.Addresses.setDefault({address_id: state.newDefaultId}).then((res) => {

            setState({
                ...state,
                newDefaultId: false
            });
            mutate(d[0]);
        }).catch(() => {

            setState({
                ...state,
                newDefaultId: false
            })
        })
    }, [state.newDefaultId])

    useEffect(()=>{
        if (!data || defaultAddress)
            return
        handleOpenClick()
    },[data])



    return (
        <React.Fragment>
            <Head>
                <title>{getPageTitle(lang.get("details"))}</title>
            </Head>
            {error ?
                <ComponentError/>
                :
                (data) ?
                    <React.Fragment>
                        {
                            defaultAddress ?
                                <Box
                                    width={1}
                                    mb={2}
                                    display='flex'
                                    flexDirection='column'>
                                    <SmHidden>
                                        <Box display={'flex'}>
                                            <Box flex={1}>
                                                <ProductsContainerHeader
                                                    style={{
                                                        width: '%100',
                                                        marginBottom: theme.spacing(2)
                                                    }}>
                                                    {lang.get('transferee')}
                                                </ProductsContainerHeader>
                                            </Box>
                                            <ButtonBase
                                                onClick={() => {
                                                    setSelect(true);
                                                }}>
                                                <Typography
                                                    px={3}
                                                    py={2}
                                                    variant='h6'
                                                    fontWeight={400}
                                                    component='a'
                                                    style={{
                                                        color: cyan[400],
                                                    }}>
                                                    {lang.get('change_the_sending_address')}
                                                </Typography>
                                            </ButtonBase>
                                        </Box>
                                    </SmHidden>
                                    <Box display={'flex'} flexDirection={'column'}>
                                        <Item item={defaultAddress}/>

                                        <ButtonBase
                                            onClick={() => {
                                                setSelect(true);
                                            }}>
                                            <Typography
                                                px={3}
                                                py={2}
                                                variant='h6'
                                                fontWeight={400}
                                                component='a'
                                                style={{
                                                    color: cyan[400],
                                                }}>
                                                {lang.get('change_the_sending_address')}
                                            </Typography>
                                        </ButtonBase>
                                    </Box>
                                    <SelectAddress open={select}
                                                   deletable={false}
                                                   onSelect={(address) => {
                                                       if (address && address.id) {
                                                           try {
                                                               onChangeAddress(address)
                                                           } catch (e) {
                                                           }
                                                           setState({
                                                               ...state,
                                                               newDefaultId: address.id
                                                           });
                                                       }
                                                       setSelect(false);
                                                   }}/>

                                    <Backdrop open={Boolean(state.newDefaultId)}
                                              style={{
                                                  zIndex: '9999'
                                              }}>
                                        <CircularProgress color="inherit"
                                                          style={{
                                                              color: cyan[300],
                                                              width: 100,
                                                              height: 100
                                                          }}/>
                                    </Backdrop>
                                </Box> :
                                <AddAddressDialog
                                    open={open}
                                    onFinish={()=>{
                                        mutate(d[0])
                                        handleCloseClick()
                                    }}
                                    onClose={() => {
                                        handleCloseClick()
                                        if(!defaultAddress){
                                            redirectTo(undefined, rout.User.Basket.rout)
                                        }
                                    }}/>
                        }
                    </React.Fragment> :
                    <PleaseWait/>}
        </React.Fragment>
    )
}

AddressPage.propTypes = {
    defaultAddress: PropTypes.object,
    onChangeAddress: PropTypes.func.isRequired,
};


const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        width: '100%',
        "&:before": {
            content: "''",
            width: '100%',
            height: 4,
            backgroundColor: colors.primary.main,
            display: 'block'
        }
    }
}));

function Item({item, ...props}) {
    const classes = useStyles()
    const theme = useTheme();

    return (
        item &&
        <Box py={1}>
            <Box component={Card} className={classes.root}>
                <Box py={1} display={'flex'} px={1}>
                    <Typography display={'flex'} alignItems={'center'} variant={"body1"} pl={1}
                                fontWeight={400}>
                        <Person
                            style={{
                                marginRight: theme.spacing(1),
                                marginLeft: theme.spacing(1),
                            }}/>
                        {lang.get("receiver")} :
                    </Typography>
                    <Typography variant={"h6"} fontWeight={500} py={2}>
                        {item.name}
                    </Typography>
                </Box>
                <Divider/>
                <Box p={2}>
                    <Box display={'flex'} flexWrap={'wrap'}>
                        <Box py={2} display={'flex'} px={1}>
                            <Typography display={'flex'} alignItems={'center'} variant={"body1"} pl={1}
                                        fontWeight={400}>
                                <Call
                                    style={{
                                        marginRight: theme.spacing(1),
                                        marginLeft: theme.spacing(1),
                                    }}/>
                                {lang.get("call_number")} :
                            </Typography>
                            <Typography variant={'h6'} fontWeight={500}>
                                {item.phone}
                            </Typography>
                        </Box>
                        <Box py={2} display={'flex'} px={1}>
                            <Typography display={'flex'} alignItems={'center'} variant={"body1"} pl={1}
                                        fontWeight={400}>
                                <MarkunreadMailbox
                                    style={{
                                        marginRight: theme.spacing(1),
                                        marginLeft: theme.spacing(1),
                                    }}/>
                                {lang.get("postal_code")} :
                            </Typography>
                            <Typography variant={'h6'} fontWeight={500}>
                                {item.postalCode}
                            </Typography>
                        </Box>
                    </Box>
                    <Box py={2} display={'unset'} px={1}>
                        <Typography display={'unset'} alignItems={'center'} variant={"body2"} pr={0.5} fontWeight={400}>
                            <LocationOn
                                style={{
                                    marginLeft: theme.spacing(0.5),
                                }}/>
                            {lang.get("address")} :
                        </Typography>
                        <Typography display={'unset'} variant={'h6'} fontWeight={500}>
                            {item.state.label} - {item.city.label}
                        </Typography>
                        <Typography display={'unset'} variant={'h6'} pr={1}>
                            - {item.address}
                        </Typography>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}
