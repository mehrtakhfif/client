import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Steppers from "./Steppers";
import Container from "@material-ui/core/Container";
import Hidden from "@material-ui/core/Hidden";
import ProductsContainerHeader from "../../home/product/productsContainer/ProductsContainerHeader";
import {lang, theme} from "../../../repository";
import rout from "../../../router";
import InvoicePage from "./invoicePage/InvoicePage";
import BasketSummery from "./BasketSummary";
import ShoppingDetailsCard from "../ShoppingDetalisCard";
import Loadable from "react-loadable";
import ControllerUser from "../../../controller/ControllerUser";
import {SpecialOffersColumnPlaceHolder} from "../../home/SpecialProductsColumn";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import _ from "lodash";
import ComponentError from "../../base/ComponentError";
import CheckoutSummary, {GoToNextLevelButton} from "../CheckoutSummary";
import {useIsLoginContext} from "../../../context/IsLoginContextContainer";
import {useUserContext} from "../../../context/UserContext";

export default function BaseShoppingStep({basket_id, products, summary, ipgs, addressRequired = true, ...props}) {

    //region Variable
    const ADDRESS_KEY_STEP = "ADDRESS_KEY_STEP";
    const PAYMENT_KEY_STEP = "PAYMENT_KEY_STEP";
    const INVOICE_KEY_STEP = "INVOICE_KEY_STEP";
    const isLogin = useIsLoginContext();
    const [user] = useUserContext();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const {firstName, lastName, meliCode, email, isActiveData} = user || {}
    const personalDataRef = useRef();
    const [state, setState] = useState({
        address: {
            data: [],
            addressRequired: addressRequired
        },
        payment: {
            firstName: firstName,
            lastName: lastName,
            meliCode: meliCode,
            email: email ? email : "",
            activeIpg: _.toString(ipgs.default),
            hasError: true,
            disable: false,
        },
        summaryDisable: false,
    });

    useEffect(() => {

    }, []);

    //endregion Variable
    //region function
    function requestToPay() {
        ControllerUser.Shopping().payment({ipg_id: state.payment.activePsp}).then(() => unDisable()).catch(() => unDisable())
    }

    //endregion function


    //region ActiveStep
    let activeStep;
    switch (props.match.params.step) {
        case rout.User.Basket.Shopping.Params.Details.param:
            if (!addressRequired)
                props.history.push(rout.User.Basket.Shopping.Params.Payment.rout);
            activeStep = ADDRESS_KEY_STEP;
            break;
        case rout.User.Basket.Shopping.Params.Payment.param:
            activeStep = PAYMENT_KEY_STEP;
            break;
        case rout.User.Basket.Shopping.Params.Invoice.rout:
            activeStep = INVOICE_KEY_STEP;
            break;
        default:
            activeStep = ADDRESS_KEY_STEP;
            props.history.push(rout.User.Basket.Shopping.Params.Details.rout);
            break;
    }

    function getActiveStep() {
        if (activeStep === ADDRESS_KEY_STEP)
            return 0;
        if (activeStep === PAYMENT_KEY_STEP)
            return addressRequired ? 1 : 0;
        return addressRequired ? 2 : 1
    }

    //endregion ActiveStep

    function disable() {
        setState({
            ...state,
            payment: {
                ...state.payment,
                disable: true
            },
            summaryDisable: true
        })
    }

    function unDisable() {
        setState({
            ...state,
            payment: {
                ...state.payment,
                disable: false
            },
            summaryDisable: false
        })
    }

    const nextStepButtonText = lang.get(activeStep === 2 ? 'fallow_order' : (isLogin) ? 'continue_ordering' : "login_first_and_pay");

    function onNextStepButtonClick() {
        disable();
        if (activeStep === ADDRESS_KEY_STEP) {
            unDisable();
            props.history.push(rout.User.Basket.Shopping.Params.Payment.rout);
            return;
        }
        if (activeStep === PAYMENT_KEY_STEP) {
            if (!isActiveData) {
                if (personalDataRef.current && personalDataRef.current.attributes.hasError) {
                    unDisable();
                    enqueueSnackbar(lang.get("er_complete_profile"),
                        {
                            variant: "error",
                            action: (key) => (
                                <Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </Fragment>
                            )
                        });
                    return;
                }
                ControllerUser.User.Profile.update({
                    firstName: state.payment.firstName,
                    lastName: state.payment.lastName,
                    meliCode: state.payment.meliCode
                }).then((res) => {
                    unDisable();
                    requestToPay();
                }).catch(() => {
                    unDisable();
                    enqueueSnackbar(lang.get("er_problem"),
                        {
                            variant: "error",
                            action: (key) => (
                                <Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </Fragment>
                            )
                        });
                });
                return
            }
            unDisable();


            enqueueSnackbar(lang.get("me_forwarding_to_payment"),
                {
                    variant: "success",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

            ControllerUser.Basket().payment({basket_id: basket_id, ipg_id: state.payment.activeIpg}).then((res) => {
                res.data.redirect()
            }).catch(() => {
                enqueueSnackbar(lang.get("er_problem_to_forward_payment"),
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            })
            // props.history.push(rout.User.Basket.Shopping.Params.Factor.rout);
        }
        if (activeStep === INVOICE_KEY_STEP) {
            unDisable();
            props.history.push(rout.Main.home);
            return;
        }
        unDisable()
    }

    return (
        <>
            <Hidden mdUp>
                <CheckoutSummary
                    totalProfit={summary.totalProfit}
                    totalAmount={summary.totalPrice}
                    amountPayable={summary.discountPrice}
                    shippingCost={summary.shippingCost}
                    disable={state.summaryDisable || state.payment.disable}
                    buttonText={nextStepButtonText}
                    onButtonClick={onNextStepButtonClick}
                    style={{
                        backgroundColor: '#fff',
                        paddingLeft: theme.spacing(5),
                        paddingTop: theme.spacing(2),
                        paddingBottom: theme.spacing(2)
                    }}/>
                <Box
                    width={1}
                    display='flex'
                    justifyContent='center'
                    style={{
                        position: 'fixed',
                        bottom: 5,
                        zIndex:theme.zIndex.appBar-20,
                    }}>
                    <GoToNextLevelButton
                        onClick={onNextStepButtonClick}
                        disabled={state.summaryDisable || state.payment.disable}
                        style={{
                            width: '95%'
                        }}>
                        {nextStepButtonText}
                    </GoToNextLevelButton>
                </Box>
            </Hidden>
            <Box
                display="flex"
                justifyContent="center"
                width={1}
                mt={4}>
                <Steppers addressRequired={addressRequired} activeStep={getActiveStep()}/>
            </Box>
            <Container maxWidth="xl"
                       style={{
                           marginTop: theme.spacing(2),
                           marginBottom: theme.spacing(3)
                       }}>
                <Hidden mdDown>
                    {
                        activeStep === ADDRESS_KEY_STEP &&
                        <ProductsContainerHeader
                            style={{
                                width: '%100',
                                marginBottom: theme.spacing(2)
                            }}>
                            {lang.get('select_recipient_delivery_address')}
                        </ProductsContainerHeader>
                    }
                </Hidden>
                <Box display='flex' width={1}>
                    <Box
                        display='flex'
                        flexWrap='wrap'
                        flexDirection='column'
                        width={1}
                        flex={1}>
                        {
                            (activeStep === ADDRESS_KEY_STEP) ?
                                <AddressPage
                                    {...props}
                                    onChangeAddress={(val) => {
                                        // setState({
                                        //     ...state,
                                        //     address: val
                                        // });
                                    }}/>
                                : (activeStep === PAYMENT_KEY_STEP) ?
                                <PaymentPage
                                    firstName={state.payment.firstName}
                                    lastName={state.payment.lastName}
                                    meliCode={state.payment.meliCode}
                                    email={state.payment.email}
                                    disable={state.payment.disable}
                                    personalDataIsActive={isActiveData}
                                    activeIpg={state.payment.activeIpg}
                                    onChangeIpg={(val) => {
                                        setState({
                                            ...state,
                                            payment: {
                                                ...state.payment,
                                                activeIpg: val
                                            }
                                        })
                                    }}
                                    hasError={state.payment.hasError}
                                    ipgs={ipgs.data}
                                    personalDataRef={personalDataRef}
                                    onChangePersonalData={({firstName, lastName, meliCode, email, ...props}) => {
                                        setState({
                                            ...state,
                                            payment: {
                                                ...state.payment,
                                                firstName: firstName !== undefined ? firstName : state.payment.firstName,
                                                lastName: lastName !== undefined ? lastName : state.payment.lastName,
                                                meliCode: meliCode !== undefined ? meliCode : state.payment.meliCode,
                                                email: email !== undefined ? email : state.payment.email
                                            }
                                        });
                                    }}/>
                                :
                                <InvoicePage {...props}/>
                        }
                        <Box display='flex' width={1}>
                            <BasketSummery products={products}/>
                        </Box>
                    </Box>
                    {activeStep !== INVOICE_KEY_STEP &&
                    <ShoppingDetailsCard
                        totalProfit={summary.totalProfit}
                        totalAmount={summary.totalPrice}
                        amountPayable={summary.discountPrice}
                        shippingCost={summary.shippingCost}
                        disable={state.summaryDisable || state.payment.disable}
                        buttonText={nextStepButtonText}
                        onButtonClick={onNextStepButtonClick}/>}
                </Box>
            </Container>
        </>
    )
}

const PaymentPage = Loadable.Map({
    loader: {
        Component: () => import("./paymentPage/PaymentPage"),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component
            {...props}/>;
    },
});
const AddressPage = Loadable.Map({
    loader: {
        Component: () => import("./addressPage/AddressPage"),
        addresses: () => ControllerUser.User().Addresses().get({all: true, page: 1, step: 30}),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        const {addresses, defaultAddress} = loaded.addresses;
        return <Component addresses={addresses.data} defaultAddress={defaultAddress} {...props}/>;
    },
});
