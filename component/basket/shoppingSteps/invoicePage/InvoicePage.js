import React, {useEffect} from 'react'
import Box from "@material-ui/core/Box";
import { makeStyles} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import {GA, lang} from "../../../../repository";
import rout from '../../../../router';
import Typography from "../../../base/Typography";
import {cyan} from "@material-ui/core/colors";
import ButtonLink from "../../../base/link/ButtonLink";
import {useRouter} from "next/router";
import {getPageTitle} from "../../../../headUtils";
import Head from "next/head";

const useStyles = makeStyles(theme => ({}));
export default function InvoicePage(props) {
    //region Variable
    const router = useRouter();
    const qId = router.query.step[1];

    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.Invoice.rout});
    },[qId]);

    if (!qId){
        //Todo: change to as function and query detect check
        useRouter().push(rout.User.Profile.Main.Params.AllOrder.rout)
    }
    //endregion Variable
    return (
            <Box display='flex' width={1}
                 flexWrap='wrap'>

                <Head>
                    <title>{getPageTitle(lang.get("invoice"))}</title>
                </Head>
                <Box component={Card} width={1} mb={2} py={8} justifyContent='center' style={{textAlign: 'center'}}>
                    <Box display={'flex'} flexDirection={'column'} alignItems={'center'} justifyContent={'center'}>
                        <Typography variant={'h4'}>
                            {lang.get("me_payment_success")}
                        </Typography>
                        <Box display={'flex'} mt={5}
                             alignItems={'center'}
                             justifyContent={'center'}>
                            <ButtonLink
                                backgroundColor={cyan[300]}
                                toHref={rout.User.Profile.Invoice.Single.createRout({invoice_id:qId})}
                                color={'#fff'}
                                style={{
                                    color: '#fff'
                                }}>
                                نمایش فاکتور
                            </ButtonLink>
                        </Box>
                    </Box>
                </Box>
            </Box>
    )
}

//region Component
//endregion Component
