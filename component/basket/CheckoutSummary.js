import React, {Fragment} from 'react'
import Box from "@material-ui/core/Box";
import {colors, lang, theme} from "../../repository";
import {Card, createTheme, useTheme} from "@material-ui/core";
import {grey} from '@material-ui/core/colors';
import {UtilsConverter, UtilsFormat, UtilsStyle} from "../../utils/Utils";
import Divider from "@material-ui/core/Divider";
import {makeStyles} from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import Hidden from "@material-ui/core/Hidden";
import HelpIcon from "../base/HelpIcon";
import PropTypes from "prop-types";
import _ from 'lodash'
import Skeleton from '@material-ui/lab/Skeleton';
import {SmHidden, SmShow} from "../header/Header";
import {Typography} from "material-ui-helper";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useUserContext} from "../../context/UserContext";


export default function CheckoutSummary({
                                            totalAmount,
                                            totalProfit,
                                            shippingCost,
                                            amountPayable,
                                            max_shipping_time,
                                            count,
                                            disable = false,
                                            buttonText,
                                            onButtonClick,
                                            ...props
                                        }) {
    const theme = createTheme();
    return (
        <Fragment>
            <SmHidden>
                <Box
                    style={{
                        ...props.style,
                        position: 'relative'
                    }}>
                    <TotalAmount value={totalAmount} count={count}/>
                    <TotalProfit value={totalProfit}/>
                    <ShippingCost value={shippingCost}/>
                    <MaxShippingTime value={max_shipping_time}/>
                    <Divider
                        style={{marginTop: theme.spacing(0.5), marginBottom: theme.spacing(0.5)}}/>
                    <AmountPayable value={amountPayable}/>
                    <Box mb={2}
                         display='flex'
                         justifyContent='center'>
                        <GoToNextLevelButton
                            disabled={disable}
                            onClick={onButtonClick}
                            buttonText={lang.get('continue_ordering')}
                            style={{
                                width: '95%',
                            }}/>
                    </Box>
                    <BasketCaption/>
                    {disable &&
                    <Skeleton
                        variant="rect"
                        style={{
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            height: '100%',
                            width: '100%',
                            right: 0,
                            left: 0,
                            margin: 0,
                            background: '#0005'
                        }}/>
                    }
                </Box>
            </SmHidden>
            <SmShow>
                <Box
                    style={{backgroundColor: '#fff'}}
                    pt={2}>
                    <TotalAmount value={totalAmount} count={count}/>
                    <TotalProfit value={totalProfit}/>
                    <ShippingCost value={shippingCost}/>
                    <MaxShippingTime value={max_shipping_time}/>
                    <Divider/>
                    <BasketCaption
                        style={{
                            marginTop: theme.spacing(2),
                            paddingBottom: theme.spacing(1)
                        }}/>
                </Box>
                <AmountPayable value={amountPayable}/>
            </SmShow>
        </Fragment>
    )
}
CheckoutSummary.propTypes = {
    totalAmount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    totalProfit: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    shippingCost: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    amountPayable: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    buttonText: PropTypes.string,
    onButtonClick: PropTypes.func.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
//region Component
//region Amounts
const useAmountStyles = makeStyles(theme => ({
    root: {
        [theme.breakpoints.down('sm')]: {
            marginRight: theme.spacing(3),
            marginLeft: theme.spacing(3)
        }
    }
}));

//region TotalAmount
function TotalAmount({value, count, ...props}) {
    const theme = createTheme();
    const classes = useAmountStyles(props);
    return (
        <Box display='flex'
             width={{
                 md: '100% !important',
                 xs: 'unset'
             }}
             className={classes.root}
             flexWrap='wrap'
             alignItems='center'
             color={grey[500]}
             style={{...props.style}}>
            <Typography variant={"body2"} fontWeight={400}>
                {lang.get('total_amount')} ( {count} {lang.get('commodity')} )
            </Typography>
            <Typography
                display='flex'
                alignItems='center'
                justifyContent='flex-end'
                variant={"body1"} fontWeight={400}
                component='span'
                py={1}
                pr={1}
                flexGrow={1}>
                {UtilsFormat.numberToMoney(_.toNumber(value))}
                <Typography
                    pl={0.5}
                     variant={"caption"} fontWeight={300}
                    Weight='400'>
                    {lang.get('toman')}
                </Typography>
            </Typography>
        </Box>
    )
}

TotalAmount.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
//endregion TotalAmount
//region TotalProfits
function TotalProfit({value, ...props}) {
    const theme = createTheme();
    const classes = useAmountStyles(props);
    return (
        <Box display='flex'
             className={classes.root}
             width={{
                 md: '100% !important',
                 xs: 'unset'
             }}
             flexWrap='wrap'
             alignItems='center'
             color="primary.main"
             style={{
                 ...props.style
             }}>
            <Typography  variant={"body2"} fontWeight={400} component='span'>
                {lang.get('your_profits_from_buying')}
            </Typography>
            <Typography display='flex'
                 alignItems='center'
                 justifyContent='flex-end'
                 component='span'
                 variant={"body1"} fontWeight={400}
                 py={1}
                 pr={1}
                 flexGrow={1}>
                {UtilsFormat.numberToMoney(_.toInteger(value))}
                <Box
                    fontSize={UtilsConverter.addRem(theme.typography.subtitle1.fontSize, 0.6)}
                    pl={1}
                    fontWeight='400'>
                    {lang.get('toman')}
                </Box>
            </Typography>
        </Box>
    )
}

TotalProfit.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

//endregion TotalProfits
//region ShippingCost
function ShippingCost(props) {
    const theme = createTheme();
    const classes = useAmountStyles(props);

    return (
        <>
            <Box display='flex'
                 width={{
                     md: '100% !important',
                     xs: 'unset'
                 }}
                 className={classes.root}
                 flexWrap='wrap'
                 alignItems='center'
                 color={grey[600]}
                 style={{
                     ...props.style
                 }}>
                <Typography
                    display='flex'
                    alignItems='center'
                    justifyContent='center'
                    variant={"body2"} fontWeight={400}
                    component='span'>
                    {lang.get('shipping_cost')}
                    <HelpIcon message={lang.get('me_shipping_cost')} show_toast_in_upmd={true}/>
                </Typography>
                <Typography display='flex'
                     alignItems='center'
                     justifyContent='flex-end'
                            variant={"body1"} fontWeight={400}
                     component='span'
                     py={1}
                     pr={1}
                     flexGrow={1}>
                    {(props.value === undefined || props.value <= -1) ? lang.get('addressable') : (
                        props.value === 0 ?
                            <React.Fragment>
                                ارسال رایگان
                            </React.Fragment> :
                            <React.Fragment>
                                {UtilsFormat.numberToMoney(_.toNumber(props.value))}
                                <Box
                                    fontSize={UtilsConverter.addRem(theme.typography.subtitle1.fontSize, 0.5)}
                                    pl={1}
                                    fontWeight='400'>
                                    {lang.get('toman')}
                                </Box>
                            </React.Fragment>
                    )}
                </Typography>
            </Box>
        </>
    )
}

ShippingCost.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
//endregion ShippingCost
//endregion Amounts
//region AmountPayable
function AmountPayable({value, ...props}) {
    const theme = createTheme();
    return (
        <>
            <Hidden mdDown>
                <Box mt={3}
                     mb={2}
                     display='flex'
                     flexDirection='column'>
                    <Typography
                        display='flex'
                        width='100%'
                        alignItems='center'
                        justifyContent='center'
                        component='h5'
                        variant={"body2"} fontWeight={400}
                        color={grey[700]}>
                        {lang.get('the_amount_payable')}:
                    </Typography>
                    <Typography
                        pt={1}
                        display='flex'
                        alignItems='center'
                        justifyContent='center'
                        variant={"body1"} fontWeight={400}
                        color={colors.success.main}
                        component='h5'>
                        {UtilsFormat.numberToMoney(value)}
                        <Typography
                            mx={0.5}
                            variant={"caption"}>
                            {lang.get('toman')}
                        </Typography>
                    </Typography>
                </Box>
            </Hidden>
            <Hidden lgUp>
                <Box
                    py={[0.5, 1.7, 1.7, 0]}
                    display='flex'
                    alignItems='center'
                    justifyContent='center'
                    className={'positionSticky'}
                    style={{
                        backgroundColor: '#fff',
                        top: 0,
                        zIndex: 6,
                        ...UtilsStyle.transition(500),
                        boxShadow: 'rgba(0, 0, 0, 0.2) 0px 2px 4px -1px, rgba(0, 0, 0, 0.14) 0px 4px 5px 0px, rgba(0, 0, 0, 0.12) 0px 11px 10px 0px'
                    }}>
                    <Typography
                        display='flex'
                        variant={"body1"} fontWeight={400}
                        alignItems='center'
                        justifyContent='center'
                        component='h5'
                        color={grey[700]}>
                        {lang.get('the_amount_payable')}:
                    </Typography>
                    <Typography
                        px={1}
                        display='flex'
                        alignItems='center'
                        justifyContent='center'
                        color={colors.success.main}
                        variant={"body1"} fontWeight={400}
                        component='h5'>
                        {UtilsFormat.numberToMoney(value)}
                        <Typography
                            variant={"caption"}
                            mx={0.5}>
                            {lang.get('toman')}
                        </Typography>
                    </Typography>
                </Box>
            </Hidden>
        </>
    )
}

//endregion AmountPayable
//region BasketCaption
const useBasketCaptionStyles = makeStyles(theme => ({
    root: {
        [theme.breakpoints.down('sm')]: {
            textAlign: 'center'
        }
    }
}));

function BasketCaption(props) {
    const theme = createTheme();
    const classes = useBasketCaptionStyles(props);
    return (
        <Box
            className={classes.root}
            width='100%'
            variant={"caption"} fontWeight={400}
            color={grey[700]}
            style={{
                ...props.style,
            }}>
            کالا های موجود در سبد شما ثبت و رزور نشده است. برای ثبت سفارش مراحل بعدی را تکمیل کنید.
            <HelpIcon
                show_toast_in_upmd={true}
                message={lang.get("me_virtual_reserved_product")}
                icon_style={{
                    top: 4,
                }}/>
        </Box>
    )
}

//endregion BasketCaption
//region GoToNextLevelButton

export function GoToNextLevelButton({buttonText, textColor = '#fff', checkLogin = true, ...props}) {
    const theme = useTheme();
    const isLogin = useIsLoginContext();
    const [user] = useUserContext();

    if (checkLogin)
        buttonText = (checkLogin && !isLogin) ? lang.get("login_first_and_pay") : buttonText;
    return (
        <Button
            {...props}
            style={{
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                border: 0,
                borderRadius: 3,
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                color: '#fff',
                height: 48,
                padding: '0 30px',
                ...props.style,
            }}>
            <Typography variant={'body1'} color={textColor}>
                {buttonText}
            </Typography>
        </Button>
    )
}

//endregion GoToNextLevelButton

export function MaxShippingTime({value, ...props}) {

    const time = (() => {
        if (value < 24) {
            return value
        }
        return Math.ceil(value / 24)
    })()

    return (
        value && value > 1 ?
            <>
                <Box display='flex'
                     width={{
                         md: '100% !important',
                         xs: 'unset'
                     }}
                     flexWrap='wrap'
                     alignItems='center'
                     color={grey[600]}
                     style={{
                         ...props.style
                     }}>
                    <Box
                        display='flex'
                        alignItems='center'
                        justifyContent='center'
                        fontSize='body2.fontSize'
                        fontWeight='400'
                        component='span'>
                        زمان ارسال
                    </Box>
                    <Box display='flex'
                         alignItems='center'
                         justifyContent='flex-end'
                         fontSize='body2.fontSize'
                         component='span'
                         fontWeight='400'
                         py={1}
                         pr={1}
                         flexGrow={1}>
                        {
                            value < 24 ?
                                `ارسال پس از ` + time + " ساعت" :
                                'ارسال از ' + time + ' روز کاری دیگر'
                        }
                    </Box>
                </Box>
            </> :
            <React.Fragment/>

    )
}


export function PlaceHolder(props) {
    return (
        <Box width={1} minHeight={150} {...props}>
            <Card style={{
                marginRight: theme.spacing(2),
                marginLeft: theme.spacing(2),
                height: '100%'
            }}>
                <Box p={2} display='flex' flexDirection='column' height='100%'>
                    <Box display='flex'
                         flexDirection='column'
                         style={{
                             height: "65%",
                         }}>
                        {
                            [0, 1, 2].map((i) => {
                                return (
                                    <Box height={'30.3%'} my={1} key={i}>
                                        <Skeleton style={{
                                            height: "100%",
                                        }}/>
                                    </Box>
                                )
                            })
                        }
                    </Box>
                    <Box display='flex'
                         flexDirection='column'
                         justifyContent={'center'}
                         alignItems={'center'}
                         style={{
                             height: "35%",
                         }}>
                        <Box height={'40%'} width="70%" my={1}>
                            <Skeleton style={{
                                height: "100%",
                            }}/>
                        </Box>
                        <Box height={'50%'} width="50%" my={1}>
                            <Skeleton style={{
                                height: "100%",
                            }}/>
                        </Box>
                    </Box>
                </Box>
            </Card>
        </Box>
    )

}

//endregion Component
