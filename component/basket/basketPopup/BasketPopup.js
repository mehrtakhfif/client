import React from "react";
import {Box, Typography, zIndexComponent} from "material-ui-helper";
import ControllerUser from "../../../controller/ControllerUser";
import useSWR from "swr";
import {useTheme} from "@material-ui/core";
import {colors, lang} from "../../../repository";
import BasketIsEmpty from "./BasketIsEmpty";
import BasketItems from "./BasketItems";
import _ from "lodash";
import BasketFooter from "./BasketFooter";


export default function BasketPopup({onClick}) {
    const theme = useTheme();
    const d = ControllerUser.Basket.V2.get();
    const {data: da} = useSWR(...d)
    const {data} = da || {};
    const {basket,summary} = data || {};
    const {id:basketId,products} = basket || {};
    const isEmpty = _.isEmpty(products)


    return (
        <Box
            dir={'rtl'}
            flexDirection={'column'}
            width={theme.spacing(60)}>
            <Box
                px={4}
                py={1.5}
                style={{
                    zIndex: zIndexComponent.img + 10,
                    backgroundColor: colors.backgroundColor.hf8f8f8
                }}>
                <Typography variant={'subtitle2'} fontWeight={'bold'}>
                    {lang.get("basket")}
                    {
                        !isEmpty &&
                        <Typography pl={1} variant={'subtitle2'} color={colors.textColor.h757575}>
                            ,
                            {products.length}
                            {"  " + lang.get("product")}
                        </Typography>
                    }
                </Typography>
            </Box>
            {
                !isEmpty ?
                    <React.Fragment>
                        <BasketItems basketId={basketId} data={products}/>
                        <BasketFooter data={{summary}} onClick={onClick}/>
                    </React.Fragment>
                    :
                    <BasketIsEmpty/>
            }
        </Box>
    )
}