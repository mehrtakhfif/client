import React from "react";
import {Box, Button, tryIt, Typography} from "material-ui-helper";
import {colors, lang} from "../../../repository";
import {UtilsFormat} from "../../../utils/Utils";
import {useTheme} from "@material-ui/core";
import {useRouter} from "next/router";
import rout from "../../../router";
import {useUserContext} from "../../../context/UserContext";
import {useIsLoginContext} from "../../../context/IsLoginContextContainer";


export default function BasketFooter({data, onClick}) {
    const theme = useTheme();
    const router = useRouter();
    const {summary} = data || {};
    const {discount_price} = summary || {};
    const isLogin = useIsLoginContext();


    return (
        <Box flexDirection={'column'}>
            <Box px={4} py={2} style={{backgroundColor: colors.backgroundColor.hf8f8f8}}>
                <Typography variant={'body1'}>
                    {lang.get("the_amount_payable")}
                </Typography>
                <Box dir={'ltr'} flex={1}>
                    <Typography variant={'subtitle2'}>
                        {lang.get("toman")}
                    </Typography>
                    <Typography px={1} variant={'body1'} fontWeight={500}>
                        {UtilsFormat.numberToMoney(discount_price)}
                    </Typography>
                </Box>
            </Box>
            <Box p={2}>
                <Box width={1 / 2} pr={1}>
                    <Button
                        fullWidth={true}
                        disableElevation={true}
                        color={theme.palette.primary.main}
                        typography={{
                            py: 1,
                            variant: 'body1',
                            color: colors.textColor.hffffff
                        }}
                        onClick={() => {
                            tryIt(() => onClick())

                            if (!isLogin){
                                router.push(rout.User.Basket.rout)
                                return
                            }
                            const {
                                rout:r,
                                as
                            } = rout.User.Basket.Shopping.create({step: rout.User.Basket.Shopping.Params.Details.param});

                            router.push(r, as)
                        }}>
                        {lang.get("complete_the_purchase")}
                    </Button>
                </Box>
                <Box width={1 / 2} pl={1}>
                    <Button
                        disableElevation={true}
                        fullWidth={true}
                        color={colors.borderColor.hc5c5c5}
                        variant={"outlined"}
                        typography={{
                            py: 1,
                            variant: 'body1',
                        }}
                        onClick={() => {
                            tryIt(() => onClick())
                            router.push(rout.User.Basket.rout)
                        }}>
                        {lang.get("view_cart")}
                    </Button>
                </Box>
            </Box>
        </Box>
    )
}