import React, {useState} from "react";
import {Box, IconButton, Skeleton, Typography, UtilsStyle} from "material-ui-helper";
import {colors, lang, media} from "../../../repository";
import {DEBUG} from "../../../pages/_app";
import _ from "lodash";
import MtIcon from "../../MtIcon";
import {removeProduct} from "../../../middleware/shopping";
import Img from "../../base/Img";


export default function BasketItem({basketId, data}) {

    const [loading, setLoading] = useState(false)

    const {
        id,
        count,
        features,
        product
    } = data;

    const {name, thumbnail} = product;


    function handleRemove() {
        setLoading(true)
        removeProduct({
            basket_id: basketId,
            basketproduct_id: id,
        }).finally(() => {
            setLoading(false)
        })
    }

    return (
        <Box dir={'rtl'} py={2} px={2} style={{position: 'relative'}}>
            <Box width={1.7 / 5}>
                <Img
                    imageHeight={media.thumbnail.height}
                    imageWidth={media.thumbnail.width}
                    src={media.convertor(
                        DEBUG ?
                            "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/thumbnail/08-13-30-23-has-ph.jpg" :
                            thumbnail?.image,
                        {
                            width: media.thumbnail.width / 1.5,
                            height: media.thumbnail.height / 1.5,
                            quality: 50,

                        })}
                    alt={thumbnail?.title}/>
            </Box>
            <Box width={3.3 / 5} pl={2} flexDirection={'column'}>
                <Typography variant={'subtitle2'} fontWeight={'normal'}>
                    {name}
                </Typography>
                <Box pt={2} alignItems={'center'}>
                    <Box flex={1} flexWrap={'wrap'}>
                        <Typography pr={3} variant={'subtitle2'} color={colors.textColor.h757575}>
                            {`${count} ${lang.get("number")}`}
                        </Typography>
                        {
                            features?.map((fe) => {
                                const {id, feature, feature_value: featureV, feature_settings} = fe;
                                const {icon} = feature_settings || {};
                                let feature_value = featureV;
                                if (!_.isArray(featureV)) {
                                    feature_value = [featureV]
                                }

                                return (
                                    <React.Fragment key={id}>
                                        {feature_value?.map(fv => (
                                            <Typography
                                                pr={3}
                                                key={id + fv}
                                                variant={'subtitle2'}
                                                color={colors.textColor.h757575}>
                                                {fv}
                                            </Typography>
                                        ))}
                                    </React.Fragment>
                                )
                            })
                        }
                    </Box>
                    <Box>
                        <IconButton
                            size={'small'}
                            onClick={handleRemove}>
                            <MtIcon icon={"mt-delete"}/>
                        </IconButton>
                    </Box>
                </Box>
            </Box>
            {
                loading &&
                <Skeleton
                    style={{
                        position: 'absolute',
                        top: 0,
                        right: 0,
                        left: 0,
                        bottom: 0,
                        zIndex: 10,
                        ...UtilsStyle.borderRadius(0)
                    }}/>
            }
        </Box>
    )
}