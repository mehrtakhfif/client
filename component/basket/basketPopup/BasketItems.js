import React from "react";
import {Box} from "material-ui-helper";
import {theme} from "../../../repository";
import Divider from "@material-ui/core/Divider";
import BasketItem from "./BasketItem";


export default function BasketItems({basketId,data}) {

    // if (DEBUG)
    // data = [...data,...data,...data,...data,...data,...data,...data,...data,...data,...data,...data,...data]

    return (
        <Box
            dir={"ltr"}
            flexDirection={'column'}
            maxHeight={theme.spacing(34)}
            style={{
                overflowY: 'auto'
            }}>
            {
                data?.map((it, index) => (
                    <React.Fragment key={it?.id}>
                        <BasketItem
                            basketId={basketId}
                            data={it}/>
                        {
                            index + 1 !== data.length &&
                            <Box px={2}>
                                <Divider style={{width: "100%"}}/>
                            </Box>
                        }
                    </React.Fragment>
                ))
            }
        </Box>
    )
}