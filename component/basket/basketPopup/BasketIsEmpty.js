import React from "react";
import {Box, Button, gLog, Typography} from "material-ui-helper";
import Img from "../../base/Img";
import {colors, lang} from "../../../repository";
import siteRout from "../../../router";
import {useRouter} from "next/router";
import {useIsLoginContext} from "../../../context/IsLoginContextContainer";


export default function BasketIsEmpty({showWishlist}) {
    const isLogin = useIsLoginContext()
    const router = useRouter();

    return (
        <Box px={2} flexDirection={'column'}>
            <Box pt={5.5}>
                <Img
                    placeholderSrc={""}
                    imageWidth={567}
                    imageHeight={325}
                    alt={"empty basket icon"}
                    src={'/drawable/svg/emptyBasketIcon.svg'}/>
            </Box>
            <Box pb={6} center={true} flexDirection={'column'}>
                <Typography
                    py={3}
                    variant={'subtitle2'} color={colors.textColor.h757575}
                    fontWeight={'bold'}
                    center={true}>
                    {lang.get("basket_is_empty")}
                </Typography>
                {
                    (!showWishlist || isLogin) &&
                    <Box pt={2}>
                        <Button
                            disableElevation={true}
                            fullWidth={true}
                            color={colors.borderColor.hc5c5c5}
                            variant={"outlined"}
                            typography={{
                                py: 1,
                                variant: 'body1',
                            }}
                            onClick={() => {
                                // tryIt(() => onClick())
                                if (showWishlist) {
                                    const {rout, as} = siteRout.User.Profile.Main.Params.Wishlist.create()
                                    router.push(rout, as)
                                    return
                                }
                                router.push(siteRout.User.Basket.rout)
                            }}>
                            {lang.get(showWishlist ? "view_wishlist_list" : "view_cart")}
                        </Button>
                    </Box>
                }
            </Box>
        </Box>
    )
}