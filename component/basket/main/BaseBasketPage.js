import React, {useState} from "react";
import Hidden from "@material-ui/core/Hidden";
import Container from "@material-ui/core/Container";
import ProductsContainerHeader from "../../home/product/productsContainer/ProductsContainerHeader";
import {lang, theme} from "../../../repository";
import rout from "../../../router";
import Box from "@material-ui/core/Box";
import ShoppingDetailsCard from "../ShoppingDetalisCard";
import {GoToNextLevelButton, PlaceHolder as CheckoutSummaryPlaceHolder} from "../CheckoutSummary";
import Loadable from "react-loadable";
import BasketItem from "./BasketItem";
import Skeleton from "@material-ui/lab/Skeleton";
import _ from 'lodash'
import {Card} from "@material-ui/core";
import BaseButton from "../../base/button/BaseButton";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import {cyan, grey} from "@material-ui/core/colors";
import Dialog from "@material-ui/core/Dialog";
import Img from "../../base/oldImg/Img";
import Typography from "../../base/Typography";
import ButtonLink from "../../base/link/ButtonLink";
import {smFooterHeight} from "../../footer/FooterSm";
import {useRouter} from 'next/router'
import {removeProduct, updateProduct} from "../../../middleware/shopping";

export default function BaseBasketPage({basket_id, summary, products, addressRequired, ...props}) {
    const router = useRouter();
    const [state, setState] = useState({
        products: {
            data: products,
            disableProducts: []
        },
        summary: {
            ...summary,
            disable: false
        },
        addressRequired: addressRequired,
        goToShoppingSteps: false,
        goToLogin: false
    });
    const [removeItem, setRemoveItem] = useState({
        id: null,
        storage_id: null
    });

    //region function
    function onNextButtonClick() {
        router.push({
            pathname: rout.User.Basket.Shopping.Params.Details.rout,
            query: {
                addressRequired: state.addressRequired
            },
        });
    }

    //region itemChangeCountHandler
    function onItemChangeCount({id, storage_id, count}) {
        disable(storage_id);


        const index = _.findIndex(state.products.data, function ({count, product}) {
            return product.storage_id === storage_id;
        });
        const pData = state.products.data[index];
        const p = pData.product;

        updateProduct( {
            basket_id: basket_id,
            basketProductId: id,
            storage_id: storage_id,
            count: count
        }).then(res => {
            const index = _.findIndex(state.products.data, function ({count, product}) {
                return product.storage_id === storage_id;
            });
            const newProducts = state.products.data;
            newProducts[index] = {
                ...newProducts[index],
                count: count
            };
            const newState = unDisable(storage_id, true);
            setState({
                ...newState,
                products: {
                    ...newState.products,
                    data: newProducts,
                },
                summary: {
                    ...newState.summary,
                    ...res.data.summary,
                }
            })
        }).catch(() => {
            unDisable(storage_id)
        })
    }


    //endregion itemChangeCountHandler

    //region itemRemoveHandler
    function onItemRemoveHandler({id, storage_id}) {
        disable(storage_id);
        setRemoveItem({
            id: id,
            storage_id: storage_id
        })
    }

    function requestRemoveItem() {
        const id = removeItem.id;
        const storage_id = removeItem.storage_id;
        setRemoveItem({
            id: null,
            storage_id: null
        });
        removeProduct( {basket_id: basket_id, basketProductId: id, storage_id: storage_id}).then(res => {
            const newState = {
                ...state,
                ...unDisable(storage_id, true),
            };
            let newProductList = newState.products.data;
            _.remove(newProductList, function ({product, count}) {
                return product.storage_id === storage_id;
            });

            setState({
                ...newState,
                products: {
                    ...newState.products,
                    data: newProductList
                },
                summary: {
                    ...newState.summary,
                    ...res.data.summary
                },
            })
        }).catch(() => {
            unDisable(storage_id)
        })
    }

    function cancelRemoveItem() {
        const storage_id = removeItem.storage_id;
        unDisable(storage_id);
        setRemoveItem({
            id: null,
            storage_id: null
        })
    }

    //endregion itemRemoveHandler
    function disable(storage_id) {
        const newDisableProducts = state.products.disableProducts;
        newDisableProducts.push(storage_id);
        setState({
            ...state,
            products: {
                ...state.products,
                disableProducts: newDisableProducts
            },
            summary: {
                ...state.summary,
                disable: true
            }
        });
    }

    /**
     * @description disable all loading state
     * @param storage_id
     * @param getObject
     * @returns {{summary: {disable: boolean}, addressRequired: *, goToShoppingSteps: boolean, products: {data: *, disableProducts: []}}|{summary: ({disable: boolean}|{disable: boolean}), products: ({data: *, disableProducts: []}|{disableProducts: *})}} newState Object
     */
    function unDisable(storage_id, getObject = false) {
        const newDisableProducts = state.products.disableProducts;
        _.remove(newDisableProducts, function (n) {
            return n === storage_id;
        });
        const newState = {
            ...state,
            products: {
                ...state.products,
                disableProducts: newDisableProducts
            },
            summary: {
                ...state.summary,
                disable: !_.isEmpty(newDisableProducts)
            }
        };

        if (getObject)
            return newState;

        setState({
            ...newState
        });
    }

    //endregion function

    return (
        (!state.products.data || state.products.data.length === 0) ?
            <Box my={8} mx={5}>
                <Box component={Card} width={1} p={2} display={'flex'} flexDirection='column'
                     justifyContent={'center'}
                     alignItems={'center'}>
                    <Img mt={4} src={'/drawable/svg/basket.svg'}
                         alt={'Basket Image'}
                         style={{
                             textAlign: 'center',
                             maxWidth: 150,
                         }}
                         imgStyle={{
                             width: '100%'
                         }}/>
                    <Typography variant="h6" mb={4} mt={2}>
                        {lang.get("basket_is_empty")}!
                    </Typography>
                    <Typography variant="h6" mb={1.5} fontWeight={600}>
                        {lang.get("me_link_to_show_more_product")}
                    </Typography>
                    <Box display={'flex'} alignItems={'center'}>
                        <ButtonLink toHref={rout.Main.home} fontWeight={500} variant={'h6'} color={cyan[500]}>
                            {lang.get('discounts_and_offers')}
                        </ButtonLink>
                        |
                        <ButtonLink toHref={rout.Main.home} fontWeight={500} variant={'h6'} color={cyan[500]}>
                            {lang.get('best_selling_products')}
                        </ButtonLink>
                    </Box>
                </Box>
            </Box>
            :
            <>
                <Hidden mdUp>
                    <CheckoutSummary
                        totalProfit={state.summary.totalProfit}
                        totalAmount={state.summary.totalPrice}
                        amountPayable={state.summary.discountPrice}
                        shippingCost={state.summary.shippingCost}
                        disable={state.summary.disable}
                        onButtonClick={onNextButtonClick}
                        style={{
                            backgroundColor: '#fff',
                            paddingLeft: theme.spacing(5),
                            paddingTop: theme.spacing(2),
                            paddingBottom: theme.spacing(2)
                        }}/>
                    <Box
                        width={1}
                        display='flex'
                        justifyContent='center'
                        style={{
                            position: 'fixed',
                            bottom: smFooterHeight + 5,
                            zIndex:theme.zIndex.appBar-20,
                        }}>
                        <GoToNextLevelButton
                            onClick={onNextButtonClick}
                            disabled={state.summary.disable}
                            buttonText={lang.get('continue_ordering')}
                            style={{
                                width: '95%'
                            }}/>
                    </Box>
                </Hidden>
                <Container maxWidth="xl"
                           style={{
                               marginTop: theme.spacing(2)
                           }}>
                    <Hidden mdDown>
                        <ProductsContainerHeader
                            style={{
                                width: '%100',
                                marginBottom: theme.spacing(2)
                            }}>
                            {lang.get('basket')}
                        </ProductsContainerHeader>
                    </Hidden>
                    <Box display='flex' pb={`${(smFooterHeight * 2) + 5}px`}>
                        <Box
                            display={{
                                lg: 'block !important',
                                xs: 'flex'
                            }}
                            flexWrap='wrap'
                            justifyContent='center'
                            width={{
                                xs: '100%',
                                md: '70%'
                            }}>
                            {
                                state.products.data.map(({id,product, count, discountPrice, finalPrice, discountPercent, ...s}, index) => (
                                    <Box
                                        key={index}
                                        width={{
                                            xs: '100%',
                                            sm: '50%',
                                            lg: '100%'
                                        }}>
                                        <BasketItem
                                            label={product.storageTitle}
                                            permalink={product.permalink}
                                            finalPrice={finalPrice}
                                            product={product}
                                            discountPrice={discountPrice}
                                            discountPercent={discountPercent}
                                            shortDescription={product.shortDescription}
                                            maxCountForSale={product.maxCountForSale}
                                            disable={_.findIndex(state.products.disableProducts, function (pId) {
                                                return pId === product.storage_id;
                                            }) !== -1}
                                            imgSrc={product.thumbnail}
                                            count={count}
                                            onItemCountChange={count => onItemChangeCount({
                                                id: id,
                                                storage_id: product.storage_id,
                                                count: count
                                            })}
                                            onItemRemoveHandler={() => onItemRemoveHandler({
                                                id: id,
                                                storage_id: product.storage_id
                                            })}
                                            style={{
                                                marginBottom: theme.spacing(1.5),
                                                marginRight: theme.spacing(1),
                                                marginLeft: theme.spacing(1)
                                            }}/>
                                    </Box>
                                ))
                            }
                        </Box>
                        <ShoppingDetailsCard
                            totalProfit={state.summary.totalProfit}
                            totalAmount={state.summary.totalPrice}
                            amountPayable={state.summary.discountPrice}
                            shippingCost={state.summary.shippingCost}
                            disable={state.summary.disable}
                            onButtonClick={onNextButtonClick}/>
                    </Box>
                </Container>
                <Dialog
                    open={removeItem.storage_id ? true : false}
                    onClose={() => {
                        cancelRemoveItem()
                    }}
                    aria-labelledby="address_remove_alert-dialog-title"
                    aria-describedby="address_remove_alert-dialog-description">
                    <DialogTitle
                        style={{
                            paddingRight: theme.spacing(3),
                            paddingLeft: theme.spacing(3),
                            paddingBottom: theme.spacing(3)
                        }}>{lang.get("me_remove_product_on_basket")}</DialogTitle>
                    <DialogActions>
                        <BaseButton
                            onClick={cancelRemoveItem}
                            autoFocus
                            style={{
                                backgroundColor: cyan[400],
                                color: '#fff',
                                marginLeft: theme.spacing(1.5)
                            }}>
                            {lang.get("no")}
                        </BaseButton>
                        <BaseButton
                            size={"small"}
                            variant={"text"}
                            onClick={requestRemoveItem}
                            style={{
                                color: grey[800]
                            }}>
                            {lang.get("yes")}
                        </BaseButton>
                    </DialogActions>
                </Dialog>
            </>
    )
}

//region Component
const CheckoutSummary = Loadable.Map({
    loader: {
        Component: () => import('../CheckoutSummary'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <Box width='95%'>Error! <button onClick={props.retry}>Retry</button></Box>;
        }
        return <CheckoutSummaryPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component {...props}/>;
    },
});

export function BaseBasketPagePlaceHolder() {
    return (
        <Box mx={2}
             my={2}
             height={{
                 xs: 150,
                 md: 250,
                 lg: 350
             }}>
            <Hidden mdUp>
                <Box width='100%' height='100%'>
                    <Box height="80%" mx={2}>
                        <Skeleton variant="rect" style={{height: '90%'}}/>
                    </Box>
                </Box>
            </Hidden>
            <Hidden smDown>
                <Box display="flex" width='100%' height='100%'>
                    <Box width='70%' height="100%" mx={2}>
                        <Skeleton variant="rect" style={{height: '90%'}}/>
                    </Box>
                    <Box width='30%' height="100%" mx={2}>
                        <CheckoutSummaryPlaceHolder style={{height: '90%'}}/>
                    </Box>
                </Box>
            </Hidden>
        </Box>
    )
}

//endregion Component
