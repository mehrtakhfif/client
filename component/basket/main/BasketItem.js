import React from "react";
import {useTheme, withStyles} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Hidden from "@material-ui/core/Hidden";
import {HighlightOff, LocalShippingTwoTone} from "@material-ui/icons";
import {colors, lang, media} from "../../../repository";
import rout from "../../../router";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import {cyan, grey} from "@material-ui/core/colors";
import {UtilsFormat, UtilsStyle} from "../../../utils/Utils";
import BaseButton from "../../base/button/BaseButton";
import InputBase from "@material-ui/core/InputBase";
import MenuItem from "@material-ui/core/MenuItem";
import _ from 'lodash'
import IconButton from "@material-ui/core/IconButton";
import Skeleton from "@material-ui/lab/Skeleton";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Link from "../../base/link/Link";
import {Img, Typography} from "material-ui-helper";


const useStyle = makeStyles(theme => ({
    formControl: {
        '& label': {
            backgroundColor: 'unset !important'
        }
    },
}));


export default function BasketItem({
                                       label,
                                       permalink,
                                       product,
                                       finalPrice,
                                       discountPrice,
                                       discountPercent,
                                       imgSrc,
                                       shortDescription = "",
                                       maxCountForSale,
                                       count,
                                       disable = false,
                                       onItemCountChange,
                                       onItemRemoveHandler,
                                       ...props
                                   }) {
    const theme = useTheme();
    const classes = useStyle(props);


    return (
        <Card
            style={{
                position: "relative",
                ...props.style,
            }}>
            {discountPercent > 0 &&
            <Typography
                variant={'body2'}
                px={1}
                py={1}
                color={'#fff'}
                style={{
                    display: 'inline-block',
                    position: 'absolute',
                    left: 0,
                    zIndex:5,
                    backgroundColor: cyan[400],
                    ...UtilsStyle.borderRadius("0 0 5px 0 "),
                    ...UtilsStyle.disableTextSelection()
                }}>
                {_.toNumber(discountPercent)}%
            </Typography>
            }
            <Box display="flex" flexDirection={'column'}>
                <Box display="flex" px={1} py={[1.3, 4, 3]}>
                    <Hidden mdDown>
                        <Box alignSelf="center">
                            <IconButton disabled={disable}
                                        onClick={onItemRemoveHandler}
                                        style={{
                                            marginRight: theme.spacing(1),
                                            marginLeft: theme.spacing(1),
                                        }}>
                                <HighlightOff/>
                            </IconButton>
                        </Box>
                    </Hidden>
                    <Box width='100%'
                         mx={2}
                         display='flex'
                         alignItems='center'
                         justifyContent={{
                             xs: 'flex-start',
                             lg: 'row',
                         }}
                         flexDirection={{
                             xs: 'column',
                             lg: 'row',
                         }}>
                        <Box display='flex'
                             width={{
                                 xs: '100%',
                                 lg: '30% !important',
                             }}
                             alignItems='center'
                             justifyContent={{
                                 xs: 'flex-start',
                                 lg: 'row',
                             }}>
                            <Img
                                renderTimeout={500}
                                imageWidth={media.thumbnail.width}
                                imageHeight={media.thumbnail.height}
                                src={imgSrc}
                                alt={label}
                                style={{
                                    ...UtilsStyle.borderRadius(5),
                                }}/>
                        </Box>
                        <Box
                            mr={[0, 0, 0, 3]}
                            mt={[0, 0, 3, 0]}
                            justifyContent={{
                                xs: 'center',
                                lg: 'flex-start',
                            }}
                            flexDirection={{
                                xs: 'column',
                                lg: 'row',
                            }}
                            width={{
                                xs: '100%',
                                lg: '70% !important',
                            }}
                            display='flex'>
                            <Box
                                display={'flex'}
                                flexDirection='column'
                                px={{
                                    lg: '16px !important',
                                    xs: '0'
                                }}
                                py={0.5}
                                width={{
                                    lg: '60% !important',
                                    xs: '100%',
                                }}
                                justifyContent='center'
                                textAlign={{
                                    lg: 'start !important',
                                    xs: 'center',
                                }}>
                                <Box>
                                    <Link
                                        href={rout.Product.SingleV2.create( permalink)}
                                        style={{
                                            display: 'inline-block',
                                        }}>
                                        <Typography variant={'h6'}>
                                            {label}
                                        </Typography>
                                    </Link>
                                </Box>
                                <Typography variant={'body2'}>
                                    <BasketItemDetails features={product.features}/>
                                </Typography>
                                {shortDescription && (
                                    <Hidden smDown>
                                        <Typography variant={"body2"} component='p' mt={3}>
                                            {shortDescription}
                                        </Typography>
                                    </Hidden>
                                )}
                                {product.defaultStorage.max_shipping_time ?
                                    <Typography alignItems={'center'} variant={"caption"} pt={0.5}>
                                        <LocalShippingTwoTone
                                            fontSize={"small"}
                                            style={{
                                                marginLeft: theme.spacing(1)
                                            }}/>
                                        {
                                            product.defaultStorage.shippingTimeIsHours ?
                                                `ارسال پس از ` + product.defaultStorage.max_shipping_time + " ساعت" :
                                                'ارسال از ' + product.defaultStorage.max_shipping_time + ' روز کاری دیگر'
                                        }
                                    </Typography> :
                                    <React.Fragment/>
                                }
                            </Box>
                            <Box display='flex'
                                 my={1}
                                 flexDirection={{
                                     md: 'row !important',
                                     xs: 'column',
                                 }}
                                 justifyContent='center'
                                 alignItems='center'
                                 width={{
                                     lg: '40% !important',
                                     xs: '100%',
                                 }}>
                                <Hidden mdDown>
                                    <Box display='flex'
                                         width={{
                                             xs: '100%',
                                             md: '30%'
                                         }}
                                         justifyContent='center'
                                         alignItems='center'>
                                        <FormControl disabled={disable} className={classes.formControl}>
                                            <InputLabel
                                                style={{
                                                    top: 3,
                                                    left: '50%',
                                                    transform: 'translate(-50%, 0)'
                                                }}>
                                                <Typography variant={"body2"}>
                                                {lang.get('count')}
                                                </Typography>
                                            </InputLabel>
                                            <Select
                                                value={_.toString(count)}
                                                onChange={(event) => {
                                                    if (count !== event.target.value)
                                                        onItemCountChange(event.target.value)
                                                }}
                                                input={<BootstrapInput name="count"/>}>
                                                {
                                                    selectorItem(maxCountForSale)
                                                }
                                            </Select>
                                        </FormControl>
                                    </Box>
                                </Hidden>
                                <Box display='flex'
                                     flexDirection='column'
                                     width={{
                                         xs: '100%',
                                         md: '70%'
                                     }}
                                     px={1}
                                     py={0.5}
                                     justifyContent='center'
                                     alignItems='center'>
                                    <Typography
                                        display="flex"
                                        variant={"body2"}
                                        justifyContent='center'
                                        alignItems='center'
                                        style={{
                                            color: grey[600],
                                            textDecoration: 'line-through'
                                        }}>
                                        {UtilsFormat.numberToMoney(_.toNumber(finalPrice))}
                                        <Typography px={0.8}
                                             variant={"caption"}>
                                            {lang.get('toman')}
                                        </Typography>
                                    </Typography>
                                    <Typography display='flex'
                                                pt={1}
                                                variant={"body2"}
                                                justifyContent='center'
                                                alignItems='center'
                                                style={{
                                                    color: grey[500],
                                                }}>
                                        {UtilsFormat.numberToMoney((_.toNumber(finalPrice) - _.toNumber(discountPrice)))}
                                        <Typography
                                            px={0.8}
                                            variant={"caption"}>
                                            {lang.get('discount')}
                                        </Typography>
                                    </Typography>
                                    <Typography
                                        display='flex'
                                        pt={1}
                                        variant={"subtitle1"}
                                        component='h5'
                                        fontWeight='600'
                                        justifyContent='center'
                                        alignItems='center'
                                        style={{
                                            color: grey[700],
                                        }}>
                                        {UtilsFormat.numberToMoney(_.toNumber(discountPrice))}
                                        <Typography px={0.5}
                                                    variant={"caption"}
                                             fontWeight='500'
                                             style={{
                                                 color: grey[800],
                                             }}>
                                            {lang.get('toman')}
                                        </Typography>
                                    </Typography>
                                </Box>
                            </Box>
                        </Box>
                        <Hidden lgUp>
                            <Box
                                display='flex'
                                flexDirection='row-reverse'
                                width={1}>
                                <BaseButton
                                    loading={false}
                                    size='small'
                                    variant='outlined'
                                    onClick={onItemRemoveHandler}
                                    style={{
                                        color: colors.danger.main,
                                        fontWeight: 400
                                    }}>
                                    حذف
                                </BaseButton>
                                <Box display='flex'
                                     width={1}
                                     justifyContent='end'
                                     ml={2}
                                     alignItems='center'>
                                    <Typography variant={"body2"} px={0.5}>
                                        {lang.get('count')}:
                                    </Typography>
                                    <FormControl disabled={disable} className={classes.formControl}>
                                        <Select
                                            value={count}
                                            onChange={(event) => {
                                                onItemCountChange(event.target.value)
                                            }}
                                            input={<BootstrapInput name="count"
                                                                   id="age-customized-select"/>}>
                                            {
                                                selectorItem(maxCountForSale)
                                            }
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Box>
                        </Hidden>
                    </Box>
                </Box>
            </Box>
            {disable &&
            <Skeleton
                variant="rect"
                style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    height: '100%',
                    width: '100%',
                    right: 0,
                    left: 0,
                    margin: 0,
                    background: '#0005'
                }}/>
            }
        </Card>
    )
}

const BootstrapInput = withStyles(theme => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        ...UtilsStyle.borderRadius(4),
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: theme.typography.body2.fontSize,
        padding: '5px 20px 5px 5px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            ...UtilsStyle.borderRadius(4),
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);
const selectorItem = (maxCountForSale) => {
    const theme = useTheme();
    const items = [];
    for (let i = 1; i <= maxCountForSale; i++) {
        items.push(i)
    }
    const element = items.map(menuItem => (
        <MenuItem
            key={menuItem}
            value={menuItem}
            style={{
            }}>
            <Typography variant={"body2"}>
            {menuItem}
            </Typography>
        </MenuItem>
    ));
    return element;
};

function BasketItemDetails({features, ...props}) {
    const theme = useTheme();
    return (
        !_.isEmpty(features) ?
            <Box display={'flex'} flexWrap={'wrap'} pt={1}>
                {features.map((f, i) => (
                    <React.Fragment key={f.id}>
                        {i > 0 && lang.get("comma")}
                        <Box display={'flex'} pr={0.3} pl={0.5} pb={0.5}>
                            {f.feature}
                            <Box pl={0.5} display={'flex'} alignItems={'center'}>
                                {"( "}{
                                f.settings?.color &&
                                <FiberManualRecord
                                    fontSize={"small"}
                                    style={{
                                        color: f.settings?.color,
                                        border: `1px solid ${grey[300]}`,
                                        marginLeft: theme.spacing(0.4),
                                        ...UtilsStyle.borderRadius('100%')
                                    }}/>
                            }
                                {f.feature_value}
                                {" )"}
                            </Box>
                        </Box>
                    </React.Fragment>
                ))}
            </Box>
            : <React.Fragment/>
    )
}

