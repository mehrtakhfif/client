import React, {useCallback, useEffect, useState} from "react";
import {SentimentVeryDissatisfied} from "@material-ui/icons";
import {Box, Button, getSafe, gLog, tryIt, Typography, UtilsTime} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import {colors, lang} from "../../repository";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import _ from "lodash";
import ControllerUser from "../../controller/ControllerUser";
import {mutate} from "swr";
import NLink from "../base/link/NLink";
import rout from "../../router";


export default function ActiveInvoices({activeInvoice}) {
    const theme = useTheme()

    const [list, setList] = useState([])

    useEffect(() => {
        gLog("asfaksjkfjkas",activeInvoice)
        const newList = activeInvoice.filter((it) => {
            return (it.expire * 1000) >= new Date().getTime();
        })
        setList(newList)
    }, [activeInvoice])

    const handleOnExpired = useCallback((invoiceId) => {
        setList(list => list.filter((it) => it.id !== invoiceId))
    }, [list])

    return (
        (_.isArray(list) && !_.isEmpty(list)) ?
            <Box
                center={true}
                pt={4}
                px={[2,10]}
                flexDirectionColumn={true}>
                <Box
                    width={{
                        xs: "10.6875vw",
                        md: "4.6875vw"
                    }}>
                    <SentimentVeryDissatisfied
                        style={{
                            color: theme.palette.error.main,
                            width: "100%",
                            height: "100%"
                        }}/>
                </Box>
                <Typography
                    width={1}
                    py={[2, 4]}
                    justifyContent={{
                        xs: "flex-start",
                        md: 'center'
                    }}
                    variant={"h6"}
                    fontWeight={"normal"}>
                    {lang.get("payment_canceled")}
                </Typography>
                <Typography
                    width={1}
                    justifyContent={{
                        xs: "flex-start",
                        md: 'center'
                    }}
                    variant={"subtitle2"}
                    color={colors.textColor.h757575}
                >
                    {lang.get("payment_canceled_message")}
                </Typography>
                {
                    list.map(invoice => (<Invoice key={invoice.id} invoice={invoice} onExpired={handleOnExpired}/>))
                }
            </Box> :
            <React.Fragment/>
    )
}


const ORDER_MODIFICATION_LOADING = 1
const PAYMENT_LOADING = 2

function Invoice({invoice, onExpired}) {
    const theme = useTheme()
    const [loading, setLoading] = useState(0)

    return (
        <Box
            mt={[2, 5]}
            py={[3, 4]}
            px={[2, 6]}
            width={1}
            flexDirectionColumn={true}
            style={{
                position: "relative",
                border: `1px solid ${colors.borderColor.hc5c5c5}`,
                ...UtilsStyle.borderRadius(5)
            }}>
            <Typography variant={"h6"} fontWeight={"bold"}>
                {lang.get("final_order")}
            </Typography>
            <Item pt={[2, 4]} label={lang.get("order_id")} value={`MT-${invoice.id}`}/>
            <Item label={lang.get("order_cost")}
                  value={(
                      <React.Fragment>
                          {UtilsFormat.numberToMoney(invoice.amount)}
                          <Typography variant={"subtitle2"} pl={1.5} color={colors.textColor.h757575} fontWeight={"light"}>
                              {lang.get("toman")}
                          </Typography>
                      </React.Fragment>
                  )}/>
            <Box
                mt={2}
                justifyContent={{
                    xs: "center",
                    md: "flex-start"
                }}>
                <Typography
                    component={"p"}
                    variant={"body1"}
                    px={[1, 2, 0]}
                    py={[2,2, 0]}
                    fontWeight={"normal"}
                    responsiveProps={{
                        md: {
                            style: {
                                backgroundColor: "transparent",
                                ...UtilsStyle.borderRadius(5)
                            }
                        },
                        xs: {
                            style: {
                                backgroundColor: colors.backgroundColor.hf8f8f8,
                                ...UtilsStyle.borderRadius(5)
                            }
                        }
                    }}>
                    {lang.get("reserved_message")}
                </Typography>
            </Box>
            <Item pt={[2, 3]} label={lang.get("booking_time")}
                  value={
                      (<ExpireTimer
                          invoiceId={invoice.id}
                          expire={invoice.expire}
                          onExpired={onExpired}/>)}/>
            <Box pt={[3, 6]} width={1} center={true}>
                <Box
                    width={{
                        xs: "100%",
                        md: "60%",
                        lg: "40%"
                    }}
                    center={true}
                    flexWrap={"wrap"}>
                    <Box width={0.5} pr={[1, 1.25]}
                         justifyContent={{
                             xs: "flex-end",
                             md: "unset"
                         }}>
                        <Button
                            fullWidth={true}
                            disabled={loading !== 0}
                            loading={loading === PAYMENT_LOADING}
                            disableElevation={true}
                            color={theme.palette.primary.main}
                            typography={{
                                py: 1.25,
                                variant: "body1",
                                color: "#fff",
                            }}
                            onClick={() => {
                                tryIt(() => {
                                    if (!invoice.payment_url)
                                        return
                                    setLoading(PAYMENT_LOADING)
                                    ControllerUser.Shopping.getIpgLink({paymentUrl: invoice.payment_url}).then(res => {
                                        window.location.href = res.data.url;
                                    }).finally(() => {
                                        setLoading(0)
                                    })
                                })
                            }}>
                            {lang.get("payment")}
                        </Button>
                    </Box>
                    <Box
                        width={0.5}
                        pl={[1, 1.25]}
                        justifyContent={{
                            xs: "flex-start",
                            md: "unset"
                        }}>
                        <Button
                            fullWidth={true}
                            disabled={loading !== 0}
                            loading={loading === ORDER_MODIFICATION_LOADING}
                            variant={"outlined"}
                            color={colors.borderColor.hc5c5c5}
                            typography={{
                                py: 1.25,
                                variant: "body1",
                            }}
                            onClick={() => {
                                setLoading(ORDER_MODIFICATION_LOADING)
                                ControllerUser.Shopping.edit({invoiceId: invoice.id}).finally(() => {
                                    setLoading(0)
                                    mutate(ControllerUser.Basket.get()[0])
                                })
                            }}>
                            {lang.get("order_modification")}
                        </Button>
                    </Box>
                    <NLink
                        variant={"body1"}
                        disabled={loading !== 0}
                        disableElevation={true}
                        href={rout.User.Profile.Invoice.Single.create({invoice_id: invoice.id})}
                        prefetch={true}
                        textSelectable={false}
                        style={{
                            marginTop: theme.spacing(5.25),
                        }}>
                        {lang.get("see_order_details")}
                    </NLink>
                </Box>
            </Box>

        </Box>
    )
}


function Item({label, value, ...props}) {

    return (
        <Box pt={[1, 2]}
             {...props}>
            <Typography variant={"body1"} minWidth={"6.51vw"} pr={1} color={colors.textColor.h757575}>
                {label}
            </Typography>
            <Typography
                flex={1}
                justifyContent={{
                    xs: "flex-end",
                    md: "flex-start"
                }}
                variant={"body1"} pl={4.25} fontWeight={"normal"}>
                {value}
            </Typography>
        </Box>
    )
}

function ExpireTimer({invoiceId, expire, onExpired}) {
    const [time, setTime] = useState({
        min1: undefined,
        min2: undefined,
        sec1: undefined,
        sec2: undefined,
    })


    useEffect(() => {
        const interval = setInterval(() => {
            setTime(getSafe(() => {
                const ex = (expire * 1000)
                if (ex < new Date().getTime()) {
                    throw ""
                }
                const d = UtilsTime.getTimeRemaining(ex);
                if (d.seconds < 0) {
                    throw ""
                }
                return {
                    min1: Math.floor(d.minutes / 10),
                    min2: Math.floor(d.minutes % 10),
                    sec1: Math.floor(d.seconds / 10),
                    sec2: Math.floor(d.seconds % 10),
                }
            }, () => {

                onExpired(invoiceId)
                tryIt(() => clearInterval(interval))
                return {
                    min1: 0,
                    min2: 0,
                    sec1: 0,
                    sec2: 0,
                }
            }))
        }, 1000)
        return () => {
            clearInterval(interval)
        }
    }, [expire])


    return (
        _.isNumber(time.min1) ?
            <Box dir={"ltr"} alignCenter={"center"}>
                <TimerItem value={time.min1}/>
                <TimerItem value={time.min2}/>
                <Typography pl={[0.5, 0.7]} component={"span"} variant={"h3"}
                            color={colors.textColor.ha9a9a9}>
                    :
                </Typography>
                <TimerItem value={time.sec1}/>
                <TimerItem value={time.sec2}/>
            </Box> :
            <React.Fragment/>
    )
}

const timerItemStyle = {
    backgroundColor: colors.backgroundColor.hf8f8f8,
    minWidth: "1.302vw",
    padding: "5px 10px",
    ...UtilsStyle.borderRadius(5)
}

const TimerItem = React.memo(({value}) => {
    const theme = useTheme()

    return (
        <Box pl={[0.5, 0.7]}>
            <Typography
                py={0.1}
                variant={"body1"}
                component={"span"}
                center={true}
                style={{
                    ...timerItemStyle,
                }}>
                {value}
            </Typography>
        </Box>
    )
})
