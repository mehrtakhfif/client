import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import {grey} from "@material-ui/core/colors";
import makeStyles from "@material-ui/core/styles/makeStyles";
import useScrollTrigger from "@material-ui/core/useScrollTrigger/useScrollTrigger";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {ArrowBack, Close, Search as SearchIcon} from "@material-ui/icons";
import Zoom from "@material-ui/core/Zoom";
import {UtilsRouter, UtilsStyle} from "../../utils/Utils";
import {lang} from "../../repository";
import rout from "../../router"
import Typography from "../base/Typography";
import ExternalDrawable from "../../public/drawable/ExternalDrawable";
import {SmShow} from "./Header";
import clsx from "clsx";
import {focusSearchTextField} from "../search/Search";
import SearchDialog from "../base/dialog/SearchDialog";
import {useOpenWithBrowserHistory} from "material-ui-helper";


export const smHeaderHeight = 60;
export const smHeaderZIndex = 99;
const baseHeaderSmStyle = makeStyles(theme => ({
    headerSmRoot: {
        height: smHeaderHeight,
    },
    headerSmToolbar: {
        width: '100%',
        padding: 0,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        zIndex: smHeaderZIndex,
        height: smHeaderHeight,
        paddingRight: theme.spacing(1.5),
        paddingLeft: theme.spacing(1.5)
    },
}));

export default function HeaderSm(props) {
    const classes = baseHeaderSmStyle(props);
    const {window} = props;
    let trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 10,
        target: window ? window() : undefined,
    });

    return (
        <SmShow>
            <AppBar className={classes.headerSmRoot}
                    elevation={trigger ? 6 : 0}
                    style={{
                        backgroundColor: '#fff',
                    }}>
                <Toolbar className={clsx(classes.headerSmToolbar, 'headerRoot')}
                         style={{
                             transform: trigger ? 'scale(1)' : 'scale(0.96)',
                             ...UtilsStyle.transition(650)
                         }}>
                    <SearchCm/>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </SmShow>
    );
}

function SmBackIcon({smHeader, ...props}) {
    const trigger = useScrollTrigger({target: props.window ? props.window() : undefined});
    return (
        <React.Fragment>
            <Zoom in={!trigger}>
                <IconButton
                    onClick={() => {
                        let goTo = null;
                        try {
                            goTo = window.history.back();
                        } catch (e) {
                            goTo = rout.Main.home;
                        }
                        ;
                        UtilsRouter.goTo(props.history, {routUrl: goTo})
                    }}
                    style={{
                        position: 'fixed',
                        zIndex: 999999,
                        opacity: 0.5,
                        left: 0,
                        backgroundColor: 'rgba(255,255,255,0.8)'
                    }}>
                    <ArrowBack/>
                </IconButton>
            </Zoom>
        </React.Fragment>
    )
}

function SmCloseIcon({smHeader, ...props}) {
    const trigger = useScrollTrigger({target: props.window ? props.window() : undefined});
    return (
        <React.Fragment>
            <Zoom in={!trigger}>
                <IconButton
                    onClick={() => {
                        if (smHeader.closeFunction) {
                            smHeader.closeFunction();
                            return;
                        }

                        let goTo = null;
                        try {
                            goTo = window.history.back();
                        } catch (e) {
                            goTo = rout.Main.home;
                        }
                        ;
                        UtilsRouter.goTo(props.history, {routUrl: goTo})
                    }}
                    style={{
                        position: 'fixed',
                        zIndex: 999999,
                        top: 5,
                        right: 0,
                        backgroundColor: 'transparent'
                    }}>
                    <Close/>
                </IconButton>
            </Zoom>
        </React.Fragment>
    )
}


function SearchCm({...props}) {
    const [open, __, handleOpenClick, handleCloseClick] = useOpenWithBrowserHistory("Search-Dialog-Uniq")


    useEffect(() => {

        if (open)
            setTimeout(() => {
                focusSearchTextField()
            }, 300)

    }, [open])

    return (
        <Box display={'flex'} width={1} alignItems={'center'}>
            <Box display={'flex'} flex={1}
                 alignItems={'center'}
                 pr={1}
                 py={1}
                 onClick={handleOpenClick}
                 component={'div'}
                 style={{
                     width: '100%',
                     cursor: 'pointer',
                     backgroundColor: 'rgba(238, 238, 238, 1)',
                     border: `1px solid ${grey[300]}`,
                     ...UtilsStyle.borderRadius(5)
                 }}>
                <SearchIcon style={{color: grey[800]}}/>
                <Typography variant={'h6'} pr={2} pl={0.7} fontWeight={300}
                            color={grey[700]}>
                    {lang.get("search_in")}
                </Typography>
                <img src={ExternalDrawable.mtTypo} alt={'search-bar-logo'} style={{
                    filter:"invert(43%) sepia(68%) saturate(1163%) hue-rotate(312deg) brightness(100%) contrast(103%)",
                    width: 75}}/>
            </Box>
            <SearchDialog
                open={open}
                onClose={() => {
                    handleCloseClick()
                }}/>
        </Box>
    )
}

//region BaseHeader
