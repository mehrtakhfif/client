import React, {useState} from "react";
import {Box} from "material-ui-helper";
import {Slide, useTheme} from "@material-ui/core";
import {headerHeight} from "../HeaderLgV2Base";
import CategoryMenuItem from "../../CategoryMenuItem";
import AdminHeaderMenu from "./AdminHeaderMenu";


export default function HidableHeaderV2({open}) {
    const theme = useTheme();
    const [openCategory, setOpenCategory] = useState()

    return (
        <Slide
            in={openCategory || open}
            direction={"down"}
            appear={false}
            style={{
                position: 'fixed',
                top: theme.spacing(headerHeight),
                zIndex: theme.zIndex.appBar - 1,
                background: "#fff"
            }}>
            <Box
                component={'ul'}
                width={1}
                boxShadow={1}
                pl={8} py={0.5}
                style={{
                    listStyleType: "none"
                }}>
                <CategoryMenuItem
                    onOpen={(open) => {
                        setOpenCategory(open)
                    }}/>
                <AdminHeaderMenu/>
            </Box>
        </Slide>
    )
}