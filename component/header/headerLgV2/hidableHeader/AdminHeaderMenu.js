import React, {useEffect} from "react";
import {Box, useState} from "material-ui-helper";
import {useIsAdminContext} from "../../../../context/IsAdminContext";
import Link from "../../../base/link/Link";
import MtIcon from "../../../MtIcon";
import {useTheme} from "@material-ui/core";
import rout from "../../../../router";
import _ from "lodash";
import ControllerProduct from "../../../../controller/ControllerProduct";
import {useRouter} from "next/router";


export default function AdminHeaderMenu() {
    const router = useRouter();
    const {pathname} = router || {}
    const isAdmin = useIsAdminContext()
    const [productId, setProductId] = useState(0)


    useEffect(() => {
        try {
            if (!isAdmin)
                return;
            const permalink = router.query[rout.Product.SingleV2.Params.permalink]
            if (pathname === rout.Product.SingleV2.rout) {
                if (_.toNumber(router.query[rout.Product.SingleV2.Params.permalink]) > 0) {
                    setProductId(permalink)
                    return;
                }

                ControllerProduct.Product.getId({permalink: permalink}).then(res => {
                    setProductId(res.data.id)
                })
                return
            }
        } catch (e) {
        }
        setProductId(0)
    }, [isAdmin, pathname]);


    if (!isAdmin)
        return <React.Fragment/>


    return (
        <React.Fragment>
            <Item
                icon={"mt-shield-check"}
                title={"ورود به پنل ادمین"}
                href={process.env.REACT_APP_ADMIN_ROUT}
                iconProps={{
                    style: {
                        color: "green"
                    }
                }}/>
            {
                productId > 0 &&
                <Item title={"ویرایش محصول"} icon={"mt-pen"}
                      href={process.env.REACT_APP_ADMIN_ROUT + `/products/single/${productId}`}/>
            }
        </React.Fragment>
    )
}


function Item({icon, title, href, iconProps = {}}) {
    const theme = useTheme();

    return (
        <Box component={'li'} px={1} alignItems={'center'}>
            <Link
                variant={"subtitle2"}
                fontWeight={500}
                alignItems={"center"}
                hasHover={false}
                hasHoverBase={false}
                href={href}
                target={'_blank'}>
                <MtIcon icon={icon} fontSize={"small"} {...iconProps}
                        style={{margin: theme.spacing(0.7), ...iconProps?.style}}/>
                {title}
            </Link>
        </Box>
    )
}