import React, {useLayoutEffect, useMemo, useState} from "react"
import {Slide, useTheme} from "@material-ui/core";
import {useBackdropContext} from "../../../../context/BackdropContext";
import useSWR from "swr";
import ControllerSite from "../../../../controller/ControllerSite";
import {headerHeight} from "../HeaderLgV2Base";
import {Box, Typography} from "material-ui-helper";
import {lang} from "../../../../repository";
import Link from "../../../base/link/Link";
import rout from "../../../../router";
import _ from 'lodash'
import MtIcon from "../../../MtIcon";
import {useLastLocation} from "react-router-last-location";

export default function HidableHeaderV3({open}) {
    const theme = useTheme();
    const [openCategory, setOpenCategory] = useState(false)
    const [__, setShowBackdrop] = useBackdropContext()
    const d = ControllerSite.getAllCategories()
    const {data} = useSWR(d[0], () => d[1]().then(res => res?.data))

    if (!data)
        return <React.Fragment/>

    return (
        <Slide
            in={openCategory || open}
            direction={"down"}
            appear={false}
            style={{
                position: 'fixed',
                top: theme.spacing(headerHeight),
                zIndex: theme.zIndex.appBar - 1,
                background: "#fff"
            }}>
            <Box
                className={'menu'}
                component={'ul'}
                width={1}
                boxShadow={1}
                pl={8} py={0.5}
                style={{
                    listStyleType: "none"
                }}>
                <MenuItem
                    data={data.product}
                    type={'product'}
                    onHover={(hover) => {
                        setShowBackdrop(hover)
                        setOpenCategory(hover)
                    }}/>
                <MenuItem
                    data={data.service}
                    type={'service'}
                    onHover={(hover) => {
                        setShowBackdrop(hover)
                        setOpenCategory(hover)
                    }}/>
                <Box component={'li'}
                     px={2}
                     py={1}>
                    <Link
                        href={rout.Main.AboutUs.rout}
                        fontWeight={400} variant={'subtitle2'} component={'span'}>
                        {lang.get('about_us')}
                    </Link>
                </Box>
                <Box component={'li'}
                     px={2}
                     py={1}>
                    <Link
                        href={rout.Main.ContactUs.rout}
                        fontWeight={400} variant={'subtitle2'} component={'span'}>
                        {lang.get('contact_us')}
                    </Link>
                </Box>
            </Box>
        </Slide>
    )
}


function MenuItem({type, data, onHover}) {

    const theme = useTheme()
    const isProd = type === "product"
    const [minHeight, setMinHeight] = useState(0)

    useLayoutEffect(() => {
        try {
            let minHeight = 0
            _.forEach(document.querySelectorAll(`.menu li[type=${type}] ul`), el => {
                try {
                    if (0 < el.offsetWidth) {
                        minHeight = el.offsetWidth
                    }
                } catch {
                }
            })
            setMinHeight(minHeight)
        } catch {
        }
    }, [data])

    return (
        <Box component={'li'} px={2} py={1}
             style={{position:'unset'}}
             type={type}
             onMouseEnter={() => {
                 onHover(true)
             }}
             onMouseLeave={() => {
                 onHover(false)
             }}>
            <Link
                href={rout.Product.Search.as({
                    type: type
                })}
                fontWeight={400} variant={'subtitle2'} component={'span'}>
                {lang.get(isProd ? 'product2' : 'grouping_services')}
                <MtIcon icon={"mt-chevron-down"}
                        style={{
                            fontSize: theme.spacing(2.5),
                            margin: theme.spacing(0.7, 1, 0.7)
                        }}/>
            </Link>
            <List data={data} minHeight={'100vh'}/>
        </Box>
    )
}


function List({data,minHeight}) {
    const theme = useTheme()


    return (
        <Box component={'ul'} pt={1} pb={2} style={{minHeight:minHeight}}>
            {
                data.map(({
                              children,
                              id,
                              name,
                              permalink,
                              priority,
                              type
                          }) => (
                    <Box key={id} component={'li'}>
                        <Link
                            fontWeight={400}
                            href={rout.Product.Search.create({category: permalink})?.[1].pathname}
                            py={1.5}
                            display={'flex'}
                            justifyContent={'space-between'}
                            pl={2.5}
                            pr={1.5}
                            variant={'subtitle2'}>
                            {name}
                            {
                                !_.isEmpty(children) &&
                                <MtIcon icon={"mt-chevron-left"}
                                        style={{
                                            fontSize: theme.spacing(2.5),
                                            margin: theme.spacing(0.7, 1, 0.7)
                                        }}/>
                            }
                        </Link>
                        {
                            !_.isEmpty(children) &&
                            <List data={children} minHeight={minHeight}/>
                        }
                    </Box>
                ))
            }
        </Box>
    )
}