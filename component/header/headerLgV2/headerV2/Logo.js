import React from "react";
import {Box} from "material-ui-helper";
import Link from "../../../base/link/Link";
import rout from "../../../../router";
import makeStyles from "@material-ui/core/styles/makeStyles";
import ExternalDrawable from "../../../../public/drawable/ExternalDrawable";


const headerLgStyle = makeStyles(theme => ({
    headerLgLogo: {
        position: 'relative',
        width: 125,
        height: '100%',
        cursor: "pointer",
        '&>a': {
            width: "100%",
            color: '#fff',
            height: '100%',
            display: 'block',
            textAlign: 'right',
            textIndent: '-1000em',
            background: `url(${ExternalDrawable.mtTypo}) no-repeat 50%`,
            filter:"invert(43%) sepia(68%) saturate(1163%) hue-rotate(312deg) brightness(100%) contrast(103%)",
            backgroundSize: 'contain',
            WebkitTransition: 'background-position .3s cubic-bezier(.17,.67,.49,.93)',
            transition: 'background-position .3s cubic-bezier(.17,.67,.49,.93)',
        }
    }
}));


export default function Logo({imageRootProps={},minHeight,...props}) {
    const classes = headerLgStyle();

    return (
        <Box
            width={3 / 12} {...props}>
            <Box
                ml={8}
                className={classes.headerLgLogo}
                {...imageRootProps}>
                <Link
                    hasHover={false}
                    hasHoverBase={false}
                    href={rout.Main.home}
                style={{minHeight}}/>
            </Box>
        </Box>
    )
}