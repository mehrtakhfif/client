import React, {useMemo} from "react";
import {Box, Typography} from "material-ui-helper";
import rout from "../../../../../../../router";
import MtIcon from "../../../../../../MtIcon";
import {lang} from "../../../../../../../repository";
import Link from "../../../../../../base/link/Link";
import {useTheme} from "@material-ui/core";


const items = () => [
    {
        icon: "mt-user",
        title: "profile",
        link: rout.User.Profile.Main.create()
    },
    {
        icon: "mt-heart",
        title: "wish",
        link: rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.Wishlist.param})
    },
    {
        icon: "mt-shopping-bag",
        title: "my_purchases",
        link: rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.AllOrder.param})
    },
]


export default function ShowProfileItems({onClose}) {
    const theme = useTheme()
    const data = useMemo(() => items(), [])

    return (
        <Box>
            {data?.map(({icon, title, link}) => {
                return (
                    <Box px={1} width={1 / 3} py={3} justifyContent={'center'} alignItems={'center'}>
                        <Link
                            href={link}
                            hasHover={false}
                            hasHoverBase={false}
                            onClick={() => {
                                onClose()
                            }}>
                            <Box key={icon} pt={1.5} pb={2} flexDirection={'column'} alignItems={'center'}>
                                <Box py={1.5}>
                                    <MtIcon icon={icon} style={{fontSize: theme.spacing(5.5)}}/>
                                </Box>
                                <Typography variant={"body2"} fontWeight={300} style={{textAlign: 'center'}}>
                                    {lang.get(title)}
                                </Typography>
                            </Box>
                        </Link>
                    </Box>
                )
            })}
        </Box>
    )
}