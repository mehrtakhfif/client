import React from "react";
import {Box, Typography} from "material-ui-helper";
import Popover from "@material-ui/core/Popover";
import {useUserContext} from "../../../../../../../context/UserContext";
import {colors, lang} from "../../../../../../../repository";
import Divider from "@material-ui/core/Divider";
import ControllerUser from "../../../../../../../controller/ControllerUser";
import ShowProfileItems from "./ShowProfileItems";
import {useTheme} from "@material-ui/core";

export default function UserPopup({anchorEl, onClose}) {
    const theme = useTheme();
    const [user] = useUserContext()

    return (
        <Popover
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={onClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}>
            <Box flexDirection={'column'} minWidth={theme.spacing(50)}>
                <Box width={1} py={1} px={2}>
                    <Typography
                        variant={"body2"}
                        color={colors.textColor.h757575}
                        px={1}
                        py={1}>
                        {`${lang.get("hello")} ${user?.first_name || ""}`}
                    </Typography>
                    <Typography
                        className={'underline'}
                        flex={1}
                        variant={"body2"}
                        justifyContent={'flex-end'}
                        px={1}
                        py={1}
                        onClick={() => {
                                    onClose()
                            ControllerUser.User.logout().finally(() => {
                            });
                        }}
                        style={{
                            cursor: 'pointer',
                        }}>
                        {lang.get("exit")}
                    </Typography>
                </Box>
                <Divider/>
                <ShowProfileItems onClose={onClose}/>
            </Box>
        </Popover>
    )
}