import React from "react";
import {useUserContext} from "../../../../../../context/UserContext";
import {useIsLoginContext} from "../../../../../../context/IsLoginContextContainer";
import {Box} from "material-ui-helper";
import LogoutLayout from "./LogoutLayout";
import LoginLayout from "./LoginLayout";


export default function AuthPanel() {
    const [user] = useUserContext();
    const isLogin = useIsLoginContext();


    return (
        <Box pr={8}>
            {
                isLogin ?
                    <LoginLayout/> :
                    <LogoutLayout/>
            }
        </Box>
    )
}