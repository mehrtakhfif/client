import React, {useEffect} from "react";
import {Box, ButtonBase, useState} from "material-ui-helper";
import MtIcon from "../../../../../MtIcon";
import {colors, theme} from "../../../../../../repository";
import {useBackdropContext} from "../../../../../../context/BackdropContext";
import UserPopup from "./userPopup/UserPopup";


export default function LoginLayout() {
    const [__,setBackdrop] = useBackdropContext();
    const [open, setOpen, setClose] = useState(false);


    useEffect(()=>{
        setBackdrop(open)
    },[open])

    return (
        <Box alignItems={'center'}>
            <ButtonBase px={1} py={1.2} display={'flex'} alignItems={'center'} onClick={(e)=>setOpen(e.currentTarget)}>
                <MtIcon icon={"mt-user"} fontSize={"large"}
                        style={{
                            fontSize: theme.spacing(3.5),
                            color:colors.textColor.h404040
                        }}/>
                <MtIcon
                    icon={"mt-chevron-down"}
                    fontSize={"small"}
                    style={{
                        marginTop: 7,
                        marginRight: 2,
                        fontSize: theme.spacing(2.5),
                        color:colors.textColor.h404040
                    }}/>
            </ButtonBase>
            <UserPopup anchorEl={open} onClose={setClose}/>
        </Box>
    )
}