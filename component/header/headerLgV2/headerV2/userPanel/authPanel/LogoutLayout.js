import React from "react";
import {Typography} from "material-ui-helper";
import {lang} from "../../../../../../repository";
import {useLoginDialogContext} from "../../../../../../context/LoginDialogContextContainer";


export default function LogoutLayout() {
    const [__,___,handleOpenLoginDialog] = useLoginDialogContext()

    return (
        <React.Fragment>
            <Typography
                alignItems={'center'}
                variant={'body2'}
                onClick={handleOpenLoginDialog}
                style={{cursor: "pointer"}}>
                {lang.get("login_or_sign_in")}
            </Typography>
        </React.Fragment>
    )
}