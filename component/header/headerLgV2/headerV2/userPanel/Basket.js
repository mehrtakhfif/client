import React, {useEffect, useRef, useState} from "react";
import {Box, HoverWatcher, IconButton, Random, UtilsStyle} from "material-ui-helper";
import MtIcon from "../../../../MtIcon";
import withStyles from "@material-ui/core/styles/withStyles";
import Badge from "@material-ui/core/Badge";
import {colors, theme} from "../../../../../repository";
import {useBasketCountContext} from "../../../../../context/BasketCount";
import {DEBUG} from "../../../../../pages/_app";
import BasketPopup from "../../../../basket/basketPopup/BasketPopup";
import Popover from "@material-ui/core/Popover";
import {useBackdropContext} from "../../../../../context/BackdropContext";


export default function Basket() {
    const ref = useRef();
    const [__,setBackdrop] = useBackdropContext()
    const [anchorEl, setAnchorEl] = useState(false)
    const [openPopup, setOpenPopup] = useState(false)
    const basketCount = useBasketCountContext()
    const onHoverIcon = (open, e) => {
        if (!open)
            return
        setOpenPopup(true)
        // if (!anchorEl && e?.currentTarget)
            setAnchorEl(ref.current)
    };
    const handlePopoverHover = (open) => {
        if (open)
            return
        setOpenPopup(false)
    };

    useEffect(() => {
        setBackdrop(Boolean(openPopup))
    }, [openPopup])


    return (
        <Box alignItems={'center'} dir={'ltr'}>
            <HoverWatcher
                aria-owns={openPopup ? 'mouse-over-popover' : undefined}
                enterTimeout={300}
                onHover={onHoverIcon}>
                <Box flexDirection={"column"}>
                <IconButton
                    onClick={(e) => {
                        onHoverIcon(true,e)
                    }}>
                    <StyledBadge
                        badgeContent={basketCount}
                        color="primary"
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'left',
                        }}>
                        <MtIcon
                            icon={"mt-cart-thic"}
                            style={{
                                fontSize: theme.spacing(4),
                                color: colors.textColor.h404040
                            }}/>
                    </StyledBadge>
                </IconButton>
                    <Box ref={ref}/>
                </Box>
            </HoverWatcher>
            <Popover
                open={Boolean(openPopup)}
                anchorEl={anchorEl}
                onClose={() => handlePopoverHover(false)}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                style={{
                    direction: 'ltr'
                }}>
                <HoverWatcher leaveSkip={true} onHover={handlePopoverHover}>
                    <BasketPopup
                        onClick={() => {
                            handlePopoverHover(false)
                        }}/>
                </HoverWatcher>
            </Popover>
        </Box>
    )
}

const StyledBadge = withStyles((theme) => ({
    badge: {
        fontWeight: 400,
        ...UtilsStyle.borderRadius(5)
    },
}))(Badge);