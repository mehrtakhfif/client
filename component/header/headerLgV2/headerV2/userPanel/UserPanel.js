import React from "react";
import {Box} from "material-ui-helper";
import Basket from "./Basket"
import {useIsAdminContext} from "../../../../../context/IsAdminContext";
import AuthPanel from "./authPanel/AuthPanel";


export default function UserPanel(){
    const isAdmin = useIsAdminContext();


    return(
        <Box pr={8} width={3 / 12} justifyContent={"flex-end"}>
            <AuthPanel/>
            <Basket/>
        </Box>
    )
}