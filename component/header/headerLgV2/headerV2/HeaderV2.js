import React from "react";
import {AppBar, Box} from "material-ui-helper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useTheme} from "@material-ui/core";
import Logo from "./Logo";
import Search from "./Search";
import {headerHeight} from "../HeaderLgV2Base";
import UserPanel from "./userPanel/UserPanel";


const headerV2Style = makeStyles(theme => ({
    toolbar: {
        height: "100%",
        padding: 0,
        width: "100%",
        "&>div": {
            height: "100%",
        }
    }
}));


export default function HeaderV2({elevation}) {
    const theme = useTheme()
    const classes = headerV2Style()

    return (
        <AppBar
            elevation={elevation}
            toolbarProps={{
                className: classes.toolbar
            }}
            appBarProps={{
                style: {
                    height: "100%",
                }
            }}
            style={{
                height: theme.spacing(headerHeight),
                width: "100%",
            }}>
            <Box width={1} height={1}>
                <Logo/>
                <Search/>
                <UserPanel/>
            </Box>
        </AppBar>
    )
}