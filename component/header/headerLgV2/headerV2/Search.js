import React from "react";
import {Box} from "material-ui-helper";
import SearchBox from "../../../search/Search";
import {useTheme} from "@material-ui/core";

export default function Search() {
    const theme = useTheme();


    return (
        <Box width={1 / 2} center={true}>
            <Box width={{
                xl:6/12,
                lg:9/12
            }}>
                <SearchBox/>
            </Box>
        </Box>
    )
}