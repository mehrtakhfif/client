import React, {useEffect, useState} from "react";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import {Slide} from "@material-ui/core";
import HidableHeaderV2 from "./hidableHeader/HidableHeaderV2";
import HeaderV2 from "./headerV2/HeaderV2";
import {useBackdropContext} from "../../../context/BackdropContext";
import {SmHidden} from "../Header";
import HidableHeaderV3 from "./hidableHeader/HidableHeaderV3";

export const headerHeight = 15

export default function HeaderLgV2Base(props) {
    const t = useScrollTrigger();
    const [open, setOpen] = useState();
    const [showBackdrop] = useBackdropContext()


    useEffect(() => {
        if (!t) {
            setTimeout(() => {
                setOpen(t)
            }, 500)
        }
    }, [t])


    return (
        <SmHidden>
            <HeaderV2 elevation={!t ? 0:undefined}/>
            <HidableHeaderV3 open={!showBackdrop &&!open && !t}/>
            {/*<HidableHeaderV2 open={!showBackdrop &&!open && !t}/>*/}
            <Toolbar/>
            <Toolbar/>
        </SmHidden>
    )
}


function HideOnScroll(props = {}) {
    const {children, window} = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.

    const trigger = useScrollTrigger({target: window ? window() : undefined});

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}