import React from 'react';
import Placeholder from "../base/Placeholder";
import HeaderSm from "./HeaderSm";
import Hidden from "@material-ui/core/Hidden";
import {getSafe} from "material-ui-helper";
import {useHeaderAndFooterContext} from "../../context/HeaderAndFooterContext";
import HeaderLgV2Base from "./headerLgV2/HeaderLgV2Base";

export default function Header() {
    const [visibility] = useHeaderAndFooterContext()

    return (
        <React.Fragment>
            {
                getSafe(() => visibility.header.showLg, true) &&
                <HeaderLgV2Base lgHeader={visibility.header}/>
            }
            {
                getSafe(() => visibility.header.showSm, false) &&
                <HeaderSm/>
            }
        </React.Fragment>
    );
}


export function SmShow({...props}) {
    return (
        <Hidden lgUp={true}>
            {props.children}
            <Placeholder/>
        </Hidden>
    )
}

export function SmHidden({...props}) {
    return (
        <Hidden mdDown={true}>
            {props.children}
            <Placeholder/>
        </Hidden>
    )
}



