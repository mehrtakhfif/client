import React from "react";
import {HoverWatcher, Typography, useState} from "material-ui-helper";
import {lang} from "../../repository";
import Box from "@material-ui/core/Box";
import {useTheme} from "@material-ui/core";
import DesktopAllCategories from "../category/DesktopAllCategories";
import {useBackdropContext} from "../../context/BackdropContext";
import MtIcon from "../MtIcon";


export default function CategoryMenuItem({onOpen}) {
    const theme = useTheme()
    const [openProduct, setOpenProduct] = useState(false)
    const [openServices, setServices] = useState(false)
    const [__, setShowBackdrop] = useBackdropContext()


    function onHoverProduct(open) {
        setOpenProduct(open)
        setShowBackdrop(open)
        onOpen(open)
    }
    function onHoverServices(open) {
        setServices(open)
        setShowBackdrop(open)
        onOpen(open)
    }

    function handleClose() {
        onHoverProduct(false)
    }

    //Todo: Add backdrop from context

    return (
        <Box pr={1} component={"li"} position={"relative"}
             style={{
                 zIndex: theme.zIndex.appBar - 1,
                 display:'flex',
                 flexDirection:'row',
                 alignItems:'center',
                 justifyContent:'space-between',
                 padding:'8px'
             }}>
            <HoverWatcher enterTimeout={100} leaveTimeout={300} onHover={onHoverProduct} style={{cursor: "pointer"}}>
                <Typography variant={"subtitle2"} fontWeight={500} alignItems={"center"}>
                    {lang.get("products")}
                    <MtIcon icon={"mt-chevron-down"}
                            style={{
                                fontSize: theme.spacing(2.5),
                                margin: theme.spacing(0.7, 1, 0.7)
                            }}/>
                </Typography>
                <Box
                    style={{
                        position: "absolute",
                        top: 30,
                        right: theme.spacing(-8)
                    }}>
                    <DesktopAllCategories open={openProduct} onClick={handleClose}/>
                </Box>
            </HoverWatcher>
            <Box enterTimeout={100} leaveTimeout={100} onHover={onHoverServices} style={{cursor: "pointer",paddingRight:'8px'}}>
                <Typography variant={"subtitle2"} fontWeight={500} alignItems={"center"}>
                    {lang.get("grouping_services")}
                    <MtIcon icon={"mt-chevron-down"}
                            style={{
                                fontSize: theme.spacing(2.5),
                                margin: theme.spacing(0.7, 1, 0.7)
                            }}/>
                </Typography>
                <Box
                    style={{
                        position: "absolute",
                        top: 30,
                        right: theme.spacing(-8)
                    }}>
                    <DesktopAllCategories open={openServices} onClick={handleClose}/>
                </Box>
            </Box>
        </Box>
    )
}