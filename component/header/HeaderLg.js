import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import NLink from "../../component/base/link/NLink"
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import Slide from "@material-ui/core/Slide";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useSnackbar} from "notistack";
import Link from "../base/link/Link";
import {colors, lang, theme} from "../../repository";
import rout from '../../router';
import Popover from "@material-ui/core/Popover";
import {
    ChevronLeft,
    Edit,
    FavoriteBorder,
    KeyboardArrowDown,
    KeyboardArrowUp,
    LockOpen,
    PersonOutlineOutlined,
    RateReviewOutlined,
    ShoppingCartOutlined
} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge/Badge";
import {cyan, grey} from "@material-ui/core/colors";
import {UtilsConverter, UtilsStyle} from "../../utils/Utils";
import Avatar from "@material-ui/core/Avatar";
import Typography from "../base/Typography";
import ButtonLink from "../base/link/ButtonLink";
import Divider from "@material-ui/core/Divider";
import BaseButton from "../base/button/BaseButton";
import clsx from "clsx";
import ExternalDrawable from "../../public/drawable/ExternalDrawable";
import {DEBUG} from "../../pages/_app";
import {logout} from "../../middleware/auth";
import {SmHidden} from "./Header";
import Button from "@material-ui/core/Button";
import ButtonBase from "@material-ui/core/ButtonBase";
import {useRouter} from "next/router";
import ControllerProduct from "../../controller/ControllerProduct";
import Tooltip from "../base/Tooltip";
import ErrorBoundary from "../base/ErrorBoundary";
import Search from "../search/Search";
import {useTheme} from "@material-ui/core";
import Nlink from "../base/link/NLink";
import CategoryMenuItem from "./CategoryMenuItem";
import {useUserContext} from "../../context/UserContext";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useBasketCountContext} from "../../context/BasketCount";
import _ from "lodash"
import {Box as HelperBox, HoverWatcher} from "material-ui-helper";
import {useIsAdminContext} from "../../context/IsAdminContext";
import BasketPopup from "../basket/basketPopup/BasketPopup";
import {useLoginDialogContext} from "../../context/LoginDialogContextContainer";


export const maxAppBarHeight = 73;
export const hidableHeaderHeight = 30;
export const headerOffset = maxAppBarHeight + hidableHeaderHeight;
export const headerOffsetPlus = headerOffset + 60;


export default function HeaderLg({...props}) {
    const [openDrawer, setOpenDrawer] = useState(false);
    return (
        <SmHidden>
            <BaseHeader openMenu={(e) => setOpenDrawer(e)} {...props}/>
            <Toolbar/>
        </SmHidden>
    )
}


//region Components

//region BaseHeader
const baseHeaderLgStyle = makeStyles(theme => ({
    headerLgRoot: {
        backgroundColor: 'transparent',
        zIndex: theme.zIndex.appBar,
        maxHeight: maxAppBarHeight,
    },
    headerLgToolbar: {
        width: '100%',
        padding: 0,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        zIndex: theme.zIndex.appBar + 300,
    },
    headerLgLogo: {
        position: 'relative',
        minHeight: 1,
        maxWidth: 125,
        maxHeight: '100%',
        cursor: "pointer",
        '&>a': {
            color: '#fff',
            height: '100%',
            display: 'block',
            textAlign: 'right',
            textIndent: '-1000em',
            background: `url(${ExternalDrawable.mtTypo}) no-repeat 50%`,
            filter: "invert(43%) sepia(68%) saturate(1163%) hue-rotate(312deg) brightness(100%) contrast(103%)",
            backgroundSize: 'contain',
            WebkitTransition: 'background-position .3s cubic-bezier(.17,.67,.49,.93)',
            transition: 'background-position .3s cubic-bezier(.17,.67,.49,.93)',
        }
    },
    adminPanel: {
        height: (maxAppBarHeight + hidableHeaderHeight + 20),
        position: 'absolute',
        top: 0,
        zIndex: theme.zIndex.appBar + 2000,
        backgroundColor: grey[200],
    },
    expandAdminPanelArrow: {
        color: grey[100],
        right: 4,
        top: 125,
        position: 'absolute',
        cursor: 'pointer',
        background: grey[400],
        ...UtilsStyle.borderRadius("0 0 10px 10px"),
        ...UtilsStyle.transition(),
        '&:hover': {
            background: cyan[300],
            ...UtilsStyle.transition()
        },
        '& svg': {
            ...UtilsStyle.transition(),
            '&:hover': {
                animation: 'marginTopAnim 700ms',
                animationIterationCount: 'infinite',
                marginTop: theme.spacing(1),
                ...UtilsStyle.transition()
            }
        },
    }
}));

function BaseHeader({openMenu, ...props}) {
    const {window} = props;
    const [searchBoxOpen, setSearchBoxOpen] = useState(false)
    const classes = baseHeaderLgStyle(props);
    const [state, setState] = useState({
        adminHeader: false,
    });
    const isAdmin = useIsAdminContext();
    const [user] = useUserContext();

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined,
    });
    const t = useScrollTrigger();

    return (
        <AppBar className={clsx(classes.headerLgRoot)}
                elevation={(trigger && t) ? 4 : 0}>
            <Toolbar className={clsx(classes.headerLgToolbar, 'headerRoot')}>
                <Box display={'flex'} py={2} width={1} style={{position: 'relative'}}>
                    {(isAdmin || DEBUG) &&
                    <React.Fragment>
                        <AdminHeader
                            user={user}
                            open={state.adminHeader}
                            onClose={() => {
                                setState({
                                    ...state,
                                    adminHeader: false
                                })
                            }}/>
                        <ButtonBase
                            className={clsx(classes.expandAdminPanelArrow)}
                            onClick={() => setState({
                                ...state,
                                adminHeader: true
                            })}>
                            <KeyboardArrowDown/>
                        </ButtonBase>
                    </React.Fragment>
                    }
                    <Box
                        ml={3.5} mr={2} p={0}
                        flex={1}
                        className={classes.headerLgLogo}>
                        <NLink href={rout.Main.home}/>
                    </Box>
                    <Box width={0.35} my={1} pb={1}>
                        <SearchBox
                            onChange={(open) => {
                                setSearchBoxOpen(open)
                            }}/>
                    </Box>
                    <Box display={'flex'} flex={1} justifyContent={'flex-end'} pr={3}>
                        <HeaderLeftAction/>
                    </Box>
                </Box>
            </Toolbar>
            <HidableHeader openMenu={openMenu} searchBoxOpen={searchBoxOpen}/>
        </AppBar>
    )
}

//endregion BaseHeader

function AdminHeader({user, open, onClose, ...props}) {
    const classes = baseHeaderLgStyle(props);
    const theme = useTheme()
    const router = useRouter();
    const [cm, setCm] = useState(undefined)
    const [state, setState] = useState({})

    function adminGoToProductSingle(productId) {
        const adminRout = process.env.REACT_APP_ADMIN_ROUT;
        setCm(
            <React.Fragment>
                <ButtonLink href={adminRout + `/products/single/${productId}`}
                            target={'_blank'}>
                    رفتن به ویرایش محصول<Edit fontSize={"small"} style={{marginRight: theme.spacing(1)}}/>
                </ButtonLink>
                <ErrorBoundary>
                    {
                        (user?.roll === "superuser" || user?.roll === "content_manager") &&
                        <Tooltip>
                            <Box component={'span'}>
                                <NLink
                                    href={adminRout + `/products/review/single/${productId}`}
                                    target={'_blank'}>
                                    ثبت بازبینی محصول
                                    <RateReviewOutlined fontSize={"small"}
                                                        style={{marginRight: theme.spacing(1)}}/>
                                </NLink>
                            </Box>
                        </Tooltip>
                    }
                </ErrorBoundary>
            </React.Fragment>
        )
    }

    useEffect(() => {
        try {
            const r = router.pathname;
            const permalink = router.query[rout.Product.SingleV2.Params.permalink]
            if (r === rout.Product.SingleV2.rout) {
                if (_.toNumber(router.query[rout.Product.SingleV2.Params.permalink]) > 0) {
                    adminGoToProductSingle(permalink)
                    return;
                }

                ControllerProduct.Product.getId({permalink: permalink}).then(res => {
                    adminGoToProductSingle(res.data.id)
                }).catch(() => {
                    setCm(undefined)
                })
                return
            }
        } catch (e) {
        }
    }, [user, router.pathname]);


    return (
        <Slide direction="down" in={open} mountOnEnter unmountOnExit>
            <Box display={'flex'}
                 width={1}
                 px={2}
                 alignItems={'center'}
                 justifyContent={'flex-end'}
                 flexWrap={'wrap'}
                 className={classes.adminPanel}>
                {cm}
                <ButtonLink href={process.env.REACT_APP_ADMIN_ROUT}
                            target={'_blank'}>
                    ورود به پنل<LockOpen fontSize={"small"} style={{marginRight: theme.spacing(1)}}/>
                </ButtonLink>
                <ButtonBase
                    className={clsx(classes.expandAdminPanelArrow)}
                    onClick={() => onClose()}
                    style={{
                        top: (maxAppBarHeight + hidableHeaderHeight + 20)
                    }}>
                    <KeyboardArrowUp/>
                </ButtonBase>
            </Box>
        </Slide>
    )
}

//region HidableHeaderV2
const hidableHeaderLgStyle = makeStyles(theme => ({
    hidableHeaderLgRoot: {
        backgroundColor: 'transparent',
        color: '#000',
        position: 'fixed',
        top: maxAppBarHeight,
        left: 0,
        right: 0,
    },
}));

function HidableHeader({openMenu, searchBoxOpen, ...props}) {
    const theme = useTheme();
    const [open, setOpen] = useState(false)
    const t = useScrollTrigger();

    const classes = hidableHeaderLgStyle();
    return (
        <Slide in={!t} direction={"down"} appear={false}>
            <Box width={1}
                 height={hidableHeaderHeight}
                 className={clsx(classes.hidableHeaderLgRoot, 'paddingRightWhenOverflow')}
                 style={{
                     boxShadow: '0 4px 2px -2px #0c0c0c24',
                     zIndex: !t ? theme.zIndex.appBar + (!searchBoxOpen ? 2000 : -200) : undefined,
                     // zIndex: theme.zIndex.appBar-1
                 }}>
                <Box component='ul'
                     m={0}
                     mb={1.5}
                     pl={2}
                     alignItems={"center"}
                     className={'headerRoot'}
                     display='flex'
                     style={{listStyle: 'none', height: '100%', backgroundColor: 'rgba(255, 255, 255, 1)'}}>
                        <h4>1321321321</h4>
                    <Li>
                        <CategoryMenuItem
                            onOpen={(open) => {
                                setOpen(open)
                            }}/>
                    </Li>
                    <Li>
                        <Nlink
                            variant={"subtitle2"}
                            fontWeight={"normal"}
                            href={rout.Main.AboutUs.create()}
                            color={grey[700]}>
                            درباره‌ی ما
                        </Nlink>
                    </Li>
                    <Li>
                        <Nlink
                            variant={"subtitle2"}
                            fontWeight={"normal"}
                            href={rout.Main.ContactUs.create()}
                            color={grey[700]}>
                            پشتیبانی
                        </Nlink>
                    </Li>
                </Box>
            </Box>
        </Slide>
    );
}

//endregion HidableHeaderV2

const SearchBox = React.memo(function SearchBox({onChange, ...props}) {
    return <Search onChange={onChange}/>
})

function HeaderLeftAction({width, ...props}) {

    const isLogin = useIsLoginContext();
    const [user] = useUserContext();

    const [state, setState] = React.useState({
        display: false
    });
    const [anchorEl, setAnchorEl] = React.useState(null);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;


    if (user && !user.avatar && DEBUG)
        user.avatar = 'https://lh3.googleusercontent.com/-ic-yiGTO3HE/AAAAAAAAAAI/AAAAAAAAAAA/AKF05nAoHKKr-QEMELEGk_eo-84ENnts2Q.CMID/s64-c/photo.jpg';

    return (
        <>
            <Box display='flex' justifyContent='center' alignItems='center'
                 pr={width === 'sm' ? 0.5 : 2}
                 style={{color: grey[700]}}>
                {!isLogin ?
                    <LoginAction/> :
                    <>
                        <IconButton
                            onClick={(e) => {
                                setAnchorEl(e.currentTarget);
                            }}>
                            <Badge
                                badgeContent={0}
                                anchorOrigin={{
                                    horizontal: 'right',
                                    vertical: 'bottom'
                                }}
                                style={{
                                    color: grey[700]
                                }}>
                                <PersonOutlineOutlined/>
                            </Badge>
                        </IconButton>
                        <Popover
                            id={id}
                            open={open}
                            anchorEl={anchorEl}
                            onClose={(e) => {
                                setAnchorEl(null);
                            }}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}>
                            <Box minWidth={250} display={'flex'} flexDirection={'column'}>
                                <ButtonLink
                                    href={rout.User.Profile.Main.create()}
                                    hoverColor={false}
                                    onClick={() => setAnchorEl(null)}
                                    textRootStyle={{width: '100%'}}>
                                    <Box display={'flex'} flexDirection={'column'} px={2}>
                                        <Box display={'flex'} alignItems={'center'} width={1}>
                                            <Avatar src={user ? user.avatar : ""}
                                                    style={{
                                                        color: grey[700]
                                                    }}/>
                                            <Typography variant={"body1"} pr={1.5}>
                                                {user ? user.fullName : ""}
                                            </Typography>
                                        </Box>
                                        <Typography variant={'body2'} color={cyan[300]} pt={2}
                                                    style={{
                                                        display: 'flex',
                                                        alignItems: 'center'
                                                    }}>
                                            مشاهده حساب کاربری
                                            <ChevronLeft/>
                                        </Typography>
                                    </Box>
                                </ButtonLink>
                                <Divider/>
                                <BaseButton
                                    variant={"text"}
                                    size={"small"}
                                    onClick={() => setAnchorEl(null)}
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'flex-start',
                                        paddingTop: theme.spacing(1),
                                        paddingBottom: theme.spacing(1),
                                        paddingRight: theme.spacing(2),
                                        ...UtilsStyle.borderRadius(0),
                                    }}>
                                    <FavoriteBorder style={{marginLeft: theme.spacing(0.5)}}/>
                                    <Link
                                        prefetch={true}
                                        href={rout.User.Profile.Main.Params.Wishlist.create()}
                                        hasHoverBase={false}
                                        no_hover={true} rel={'nofollow'}>
                                        {lang.get('wishlist')}
                                    </Link>
                                </BaseButton>
                                <BaseButton
                                    variant={"text"}
                                    size={"small"}
                                    onClick={() => setAnchorEl(null)}
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'flex-start',
                                        paddingTop: theme.spacing(1),
                                        paddingBottom: theme.spacing(1),
                                        paddingRight: theme.spacing(2),
                                        ...UtilsStyle.borderRadius(0),
                                    }}>
                                    <ShoppingCartOutlined style={{marginLeft: theme.spacing(0.5)}}/>
                                    <Link
                                        prefetch={true}
                                        href={rout.User.Profile.Main.Params.AllOrder.create()} hasHoverBase={false}
                                        no_hover={true} rel={'nofollow'}>
                                        {lang.get('my_orders')}
                                    </Link>
                                </BaseButton>
                                <Divider/>
                                <BaseButton
                                    variant={"text"}
                                    size={"small"}
                                    onClick={() => {
                                        setAnchorEl(null);
                                        setState({
                                            ...state,
                                            disable: true
                                        });
                                        logout();
                                        enqueueSnackbar(lang.get("logout_successfully"),
                                            {
                                                variant: "success",
                                                action: (key) => (
                                                    <Fragment>
                                                        <Button onClick={() => {
                                                            closeSnackbar(key)
                                                        }}>
                                                            {lang.get('close')}
                                                        </Button>
                                                    </Fragment>
                                                )
                                            });
                                    }}
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'flex-start',
                                        paddingTop: theme.spacing(1),
                                        paddingBottom: theme.spacing(1),
                                        paddingRight: theme.spacing(2),
                                        ...UtilsStyle.borderRadius(0),
                                    }}>
                                    {lang.get('logout_from_account')}
                                </BaseButton>
                            </Box>
                        </Popover>
                    </>
                }
            </Box>
            <ShoppingCartIcon/>
        </>
    )
}


const shoppingCartIconStyle = makeStyles(theme => ({
    shoppingCartIconRoot: {},
    shoppingCartIconBadge: {
        '&>span': {
            color: '#fff !important',
        }
    }
}));

function ShoppingCartIcon({...props}) {
    const classes = shoppingCartIconStyle(props);
    const basketCount = useBasketCountContext();
    const [anchorEl, setAnchorEl] = useState(false)
    const [openPopup, setOpenPopup] = useState(false)
    const ref = useRef();

    const onHoverIcon = (open, e) => {
        if (!open)
            return
        setOpenPopup(true)
        if (!anchorEl && e?.currentTarget)
            setAnchorEl(e.currentTarget)
    };

    const handlePopoverHover = (open) => {
        if (open)
            return
        setOpenPopup(false)
    };

    return (
        <HelperBox ref={ref} center={true}>
            <HoverWatcher
                aria-owns={openPopup ? 'mouse-over-popover' : undefined}
                enterSkip={true}
                onHover={onHoverIcon}>
                <NLink
                    hasHoverBase={false}
                    color={grey[700]}
                    rel={'nofollow'}
                    prefetch={true}
                    href={rout.User.Basket.rout}>
                    <IconButton aria-label="Go to basket" color="inherit">
                        <Badge
                            badgeContent={basketCount}
                            color="primary"
                            className={classes.shoppingCartIconBadge}
                            anchorOrigin={{
                                horizontal: 'right',
                                vertical: 'bottom'
                            }}>
                            <ShoppingCartOutlined style={{color: colors.textColor.h404040}}/>
                        </Badge>
                    </IconButton>
                </NLink>
            </HoverWatcher>
            <Popover
                open={Boolean(openPopup)}
                anchorEl={anchorEl}
                onClose={() => handlePopoverHover(false)}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                style={{
                    direction: 'ltr'
                }}>
                <HoverWatcher leaveSkip={true} onHover={handlePopoverHover}>
                    <BasketPopup onClick={() => {
                        handlePopoverHover(false)
                    }}/>
                </HoverWatcher>
            </Popover>
        </HelperBox>
    )
}

function PersonIcon({user, ...props}) {
    return (
        <Link display='flex'
              alignItems='center'
              hasHover={false}
              toHref={user.isLogin ? rout.User.Profile.Main.rout : rout.User.Auth.Login.rout}
              fontSize={UtilsConverter.addRem(theme.typography.body1.fontSize, 1.05)}
              style={{
                  marginRight: theme.spacing(1)
              }}>
            <IconButton
                color="inherit">
                <Badge badgeContent={0} color="secondary"
                       anchorOrigin={{
                           horizontal: 'right',
                           vertical: 'bottom'
                       }}>
                    <PersonOutlineOutlined/>
                </Badge>
            </IconButton>
        </Link>
    )
}

//region li
const useLiStyles = makeStyles(theme => (
    {
        root: {
            ...UtilsStyle.transition('300'),
            '&:after , &:before': {
                content: '""',
                display: 'block',
                position: 'absolute',
                bottom: -7,
                height: 2,
                width: 0,
                left: '50%',
                textAlign: '50%',
                transform: 'translate(-50%, -50%) !important',
                backgroundColor: '#ffffff',
                ...UtilsStyle.transition('500'),
            },
            '&:hover':
                {
                    color: colors.primary.main,
                    ...UtilsStyle.transition('300'),
                    '&:before': {
                        width: '100%',
                        offsetRotate: 180,
                        transform: 'rotate(180deg)',
                        ...UtilsStyle.transition('500'),
                    }
                }
            ,
        }
    }
));

function Li({style, ...props}) {
    const classes = useLiStyles(props);

    return (
        <Box component='li' display='flex'
             alignContent='center'
             className={classes.root}
             px={1} style={{float: 'right', position: 'relative', ...style}} {...props}>
            {props.children}
        </Box>
    )
}

//endregion li


function LoginAction() {
    const [__, ___, handleOpenLoginDialog] = useLoginDialogContext()


    return (
        <React.Fragment>
            <Typography pr={1}
                        variant={"caption"}
                        fontWeight={'normal'}
                        color={grey[700]}
                        onClick={handleOpenLoginDialog}
                        style={{
                            cursor: "pointer"
                        }}>
                {lang.get('login')}
            </Typography>
            /
            <Typography
                pl={1}
                variant={"caption"}
                fontWeight={'normal'}
                color={grey[700]}
                onClick={handleOpenLoginDialog}
                style={{
                    cursor: "pointer"
                }}>
                {lang.get('registration')}
            </Typography>
        </React.Fragment>
    )
}

//endregion Components
