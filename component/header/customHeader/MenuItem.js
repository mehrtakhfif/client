import React, {useMemo, useState} from "react";
import {Box, IconButton, isElement, tryIt, useOpenWithBrowserHistory} from "material-ui-helper";
import MtIcon from "../../MtIcon";
import {useRouter} from "next/router";
import {lang} from "../../../repository";
import SearchDialog from "../../base/dialog/SearchDialog";
import {HeaderItem} from "./CustomHeader";
import Badge from "@material-ui/core/Badge";
import {useBasketCountContext} from "../../../context/BasketCount";
import {backdropType, useBackdropContext} from "../../../context/BackdropContext";


export default function MenuItem({item, ...props}) {

    const {isSearch} = useMemo(() => {
        return tryIt(() => {
            if (item.key === HeaderItem.Search.key)
                return {isSearch: true}
            throw ""
        }, {})
    }, [item])

    return (
        isSearch ?
            <SearchItem item={item} {...props}/> :
            <Base item={item} {...props}/>
    )
}

function SearchItem({item, ...props}) {

    const [open, setOpen] = useOpenWithBrowserHistory("MenuItemSearchItem")

    function handleClick() {
        setOpen(true)
    }

    function handleClose() {
        setOpen(false)
    }


    return (
        <React.Fragment>
            <Base item={item} onClick={handleClick} {...props}/>
            <SearchDialog open={open} onClose={handleClose}/>
        </React.Fragment>
    )
}

function Base({item, onClick, ...props}) {
    const router = useRouter()
    const basketCount = useBasketCountContext()
    const [loading, setLoading] = useState(false)
    const [backdrop,setBackDrop] =useBackdropContext()



    function handleOnClick(e) {
        tryIt(() => {
            e.stopPropagation()
            if (onClick) {
                onClick()
                return
            }
            if (item.disabledOnClick) {
                setLoading(true)
            }
            setBackDrop(backdropType.fillPageWithLoading)
            setTimeout(() => {
                item.onClick({e, router}).finally(()=>setBackDrop(false))
            }, 200)
        })
    }

    if (!item)
        return <React.Fragment/>

    return (
        isElement(item) ?
            React.cloneElement(item, props) :
            <Box {...props}>
                <IconButton disabled={loading} tooltip={lang.get(item.tooltip)} onClick={handleOnClick}>
                    {
                        item.key === HeaderItem.Basket.key ?
                            <Badge
                                color="primary"
                                badgeContent={basketCount > 0 ? basketCount : undefined}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}>
                                <MtIcon icon={item.icon}/>
                            </Badge> :
                            <MtIcon icon={item.icon}/>

                    }
                </IconButton>
            </Box>
    )
}
















