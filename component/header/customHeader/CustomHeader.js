import React, {useCallback} from "react";
import {AppBarHideOnScroll, Box, IconButton, Typography} from "material-ui-helper";
import Toolbar from '@material-ui/core/Toolbar';
import MtIcon from "../../MtIcon";
import ErrorBoundary from "../../base/ErrorBoundary";
import MenuItem from "./MenuItem";
import {useRouter} from "next/router";
import rout from "../../../router";
import {useBrowserHistoriesContext} from "../../../context/BrowserHistoriesContext";
import _ from "lodash";

export const HeaderItem = {
    Search: {
        key: "search",
        tooltip: "search",
        icon: "mt-search",
    },
    Basket: {
        key: "basket",
        tooltip: "basket",
        icon: "mt-cart-thic",
        disabledOnClick:true,
        onClick: ({router}) => {
            return router.push(rout.User.Basket.rout)
        }
    },

}


export default function CustomHeader({
                                         title,
                                         menus,
                                         backable,
                                         closable,
                                         showToolbar = true,
                                         onDismissClick,
                                         ...pr
                                     }) {
    const {children, ...props} = pr;
    const router = useRouter();
    const [browserHistories,{onBack}] = useBrowserHistoriesContext();


    const handleDismissClick = useCallback(() => {
        try {
            if (_.isFunction(onDismissClick)) {
                onDismissClick()
                return
            }
            onBack()
        } catch (e) {
        }
    }, [onDismissClick])

    return (
        <React.Fragment>
            <ErrorBoundary elKey={"CustomHeader::(component->header->customHeader->CustomHeader.js)"}>
                <AppBarHideOnScroll px={1.5} width={1} py={0.5}>
                    <Box alignItems={'center'}>
                        {
                            (closable || backable) &&
                            <IconButton size="small" onClick={handleDismissClick}>
                                <MtIcon fontSize={"small"} color={"#000"}
                                        icon={backable ? "mt-arrow-right" : "mt-close-thin"}/>
                            </IconButton>
                        }
                        {
                            title &&
                            <Typography pl={1} color={"#000"} variant={"subtitle1"}>
                                {title}
                            </Typography>
                        }
                    </Box>
                    {
                        menus &&
                        <Box flex={1} dir={'ltr'} alignItems={'center'}>
                            {
                                menus.map((it, index) => (
                                    <MenuItem key={it?.icon || index} item={it}/>
                                ))
                            }
                        </Box>
                    }
                </AppBarHideOnScroll>
                {
                    showToolbar &&
                    <Toolbar/>
                }
            </ErrorBoundary>
        </React.Fragment>
    )
}