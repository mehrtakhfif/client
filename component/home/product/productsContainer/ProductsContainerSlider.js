import React, {createRef} from "react";
import {makeStyles} from "@material-ui/core";
import PropTypes from "prop-types";
import Box from '@material-ui/core/Box'
import {colors, dir, isServer} from '../../../../repository'
import clsx from 'clsx'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import HomeProductCard from "../HomeProductCard";
import {UtilsStyle} from "../../../../utils/Utils";
import Loadable from "react-loadable";
import ComponentError from "../../../base/ComponentError";
import {SpecialOffersColumnPlaceHolder} from "../../SpecialProductsColumn";
import ClientLazyload from "../../../base/ClientLazyload";
import Skeleton from "@material-ui/lab/Skeleton";
import SSRProductCard from "../../../product/SSRProductCard";


export const sliderArrowStyles = makeStyles(theme => ({
    buttonIcon: {
        position: "absolute",
        zIndex: "2",
        backgroundColor: colors.backgroundColor.darker,
        color: "#fff",
        top: '50%',
        bottom: '50%',
        cursor: 'pointer',
        opacity: '0.4',
        transition: 'all 200ms ease',
        '&:hover': {
            transform: 'scale(1.2)',
            opacity: '1',
        },
        width: '35px',
        height: '50px',
        '& svg': {
            width: '35px',
            height: '50px'
        },
        [theme.breakpoints.down('sm')]: {
            width: '25px',
            height: '30px',
            '& svg': {
                width: '25px',
                height: '30px'
            },
        },

    },
    buttonLeftIcon: {
        left: 0,
        borderTopRightRadius: theme.spacing(2),
        borderBottomRightRadius: theme.spacing(2),
    },
    buttonRightIcon: {
        right: 0,
        borderTopLeftRadius: theme.spacing(2),
        borderBottomLeftRadius: theme.spacing(2),
    },
    buttonDisable: {
        opacity: 0,
        width: 0,
        '&:hover': {
            opacity: 0,
        }
    }
}));

export function NextArrow(props) {
    const {onClick} = props;
    const classes = sliderArrowStyles(props);
    const classesUse = [classes.buttonIcon, classes.buttonRightIcon, 'transition500', 'transition500hover'];
    if (props.currentSlide === 0) {
        classesUse.push(classes.buttonDisable)
    }
    return (
        <Box
            className={clsx(...classesUse)}>
            <ChevronLeft onClick={onClick}/>
        </Box>
    );
}

export function PrevArrow(props) {
    const {onClick} = props;
    const classes = sliderArrowStyles(props);
    const classesUse = [classes.buttonIcon, classes.buttonLeftIcon, 'transition500', 'transition500hover'];
    if (props.currentSlide >= (props.slideCount - 1)) {
        classesUse.push(classes.buttonDisable)
    }
    return (
        <Box
            className={clsx(...classesUse)}>
            <ChevronRight onClick={onClick}/>
        </Box>
    );
}

function ProductItem({data, ...props}) {
    return (
        <Box px={0.5} pb={0.2} display={'flex'} alignItems={'center'} justifyContent={'center'}>
            <HomeProductCard
                id={data.id}
                elevation={1}
                storageId={data.storage_id}
                permalink={data.permalink}
                availableCountForSale={data.maxCountForSale}
                name={data.name}
                imageSrc={data?.thumbnail?.image}
                imageAlt={data?.thumbnail?.title}
                discountPrice={data.discountPrice}
                discountPercent={data.discountPercent}
                finalPrice={data.finalPrice}
                category={data.category}
                style={{margin: '0 auto'}}
                {...props}/>
        </Box>
    )
}

ProductItem.propTypes = {
    onClick: PropTypes.func,
    onAddFavoriteClick: PropTypes.func,
    onAddToBasketClick: PropTypes.func,
    loading: PropTypes.bool,
    elevation: PropTypes.number
};


const sliderStyles = makeStyles(theme => ({
    sliderRoot: {
        position: 'relative',
        '& .tns-outer [data-action]': {
            display: 'none'
        },
        '& .tns-slider': {
            display: 'flex'
        }
    }
}));

const settings = {
    lazyload: true,
    nav: false,
    mouseDrag: true,
    loop: false,
    items: 1.7,
    controls: false,
    speed: 600,
    swipeAngle: false,
    center: false,
    responsive: {
        380: {
            items: 2.5,
        },
        500: {
            items: 2.5,
        },
        680: {
            items: 3.1,
        },
        960: {
            items: 4.5,
        },
        1920: {
            items: 6.1,
        }
    }
};

function SimpleSlider(props) {
    const {data} = props;
    const classes = sliderStyles(props);
    let sliderRef = createRef();
    if (props.settings) {
        //
        // settings = {
        //     ...settings,
        //     ...props.settings
        // }
    }
    const onGoTo = dir => sliderRef.slider.goTo(dir);

    return (
        <Box dir='ltr'
             py={1}
             className={classes.sliderRoot}
             style={{
                 zIndex: '9',
                 width: '100%'
             }}>
            <NextArrow
                onClick={() => {
                    onGoTo('prev');
                }}/>
            <PrevArrow
                onClick={() => {
                    onGoTo('next');
                }}/>

            {
                isServer()&&
                [...data].map((product, i) => (
                    <SSRProductCard
                        key={i}
                        permalink={product.permalink}
                        name={product.name}
                        imageSrc={product.thumbnail}
                        data={product}
                        style={{
                            ...UtilsStyle.disableTextSelection()
                        }}/>
                ))
            }

            <ClientLazyload lzPlaceholder={<Skeleton height={200} variant={'rect'}/>}>
                <TinySlider dir='ltr' settings={settings}
                            tinyRef={ts => {
                                sliderRef = ts;
                            }}
                            style={{
                                padding:'20px 10px'
                            }}>
                    {
                        [...data].map((product, i) => (
                            <ProductItem
                                {...props}
                                dir={dir()}
                                key={i}
                                data={product}
                                style={{
                                    ...UtilsStyle.disableTextSelection()
                                }}/>
                        ))
                    }
                </TinySlider>
            </ClientLazyload>
        </Box>
    );
}

const TinySlider = Loadable.Map({
    loader: {
        Component: () => import('tiny-slider-react-ar'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component ref={props.tinyRef} {...props}/>;
        // return <Box>s</Box>
    },
});

export default SimpleSlider;
