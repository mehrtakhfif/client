import {makeStyles} from "@material-ui/styles";
import PropTypes from "prop-types";
import React from "react";
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../../../utils/Utils";
import clsx from "clsx";
import {Typography} from "material-ui-helper";

const styles = makeStyles(theme => (
    {
        productsContainerHeaderTextHeader: {
            borderBottom: '4px solid',
            paddingBottom: "8px",
            ...UtilsStyle.widthFitContent(),
        },
    }));

function ProductsContainerHeader(props) {
    const classes = styles(props);
    return (
        <Box textAlign={props.textAlign} display="block" style={{...props.container_style}}>
            <Typography className={clsx(classes.productsContainerHeaderTextHeader,"maxWidth")}
                        variant={"h6"}
                 component={props.component}
                 pr={4}
                 style={{
                     ...props.style,
                     borderBottomColor: props.borderColor
                 }}>
                {props.children}
            </Typography>
        </Box>
    )
}

ProductsContainerHeader.defaultProps = {
    textAlign: "right",
    fontSize: "h6.fontSize",
    component: 'h5',
    borderWidth: "50px",
    borderColor: "#a56"
};

ProductsContainerHeader.prototype = {
    backgroundColor: PropTypes.string,
    borderWidth: PropTypes.number,
    borderColor: PropTypes.string,
    fontSize: PropTypes.oneOf([
        'fontSize',
        'h1.fontSize',
        'h2.fontSize',
        'h3.fontSize',
        'h4.fontSize',
        'h5.fontSize',
        'h6.fontSize',
        'subtitle1.fontSize',
        'subtitle2.fontSize',
        'body1.fontSize',
        'body2.fontSize',
        'button.fontSize',
        'overline.fontSize',
    ]),
    component: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    container_style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default ProductsContainerHeader;
