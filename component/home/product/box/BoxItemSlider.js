import React from 'react';
import Box from "@material-ui/core/Box";
import {lang} from "../../../../repository";
import rout from '../../../../router'
import ProductsContainerHeader from "../productsContainer/ProductsContainerHeader";
import Card from "@material-ui/core/Card";
import {createTheme} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
import ProductsContainerSlider from '../productsContainer/ProductsContainerSlider'
import Hidden from "@material-ui/core/Hidden";
import Link from "../../../base/link/Link";

const useStyles = makeStyles(theme => ({
    rootStyle: {},
    moreProductLinkStyle: {
        position: "absolute",
        padding: theme.spacing(1),
        paddingTop: theme.spacing(2),
        bottom: 5,
        right: 8,
        '&:hover': {
            color: 'red'
        }
    }
}));


export default function BoxItemSlider({data, ...props}) {
    const {moreProductLinkStyle} = useStyles(props);
    const theme = createTheme();
    const showMoreLink = rout.Product.Search.create({category: data?.permalink});
    return (
        <Box px={2} style={{...props.style}}>
            <Box style={{position: 'relative'}}>
                <Box style={{position: 'relative'}}>
                    <Link
                        hoverColor={"unset"}
                        hasHoverBase={false}
                        hasHover={false}
                        href={showMoreLink?.[0]}
                        as={showMoreLink?.[1]}>
                        <ProductsContainerHeader
                            style={{
                                margin: 0
                            }}
                            container_style={{
                                paddingBottom: theme.spacing(3)
                            }}
                            borderColor="#a56">
                            {data.name}
                        </ProductsContainerHeader>
                    </Link>
                    <Link
                        href={showMoreLink}
                        className={moreProductLinkStyle}>
                        {lang.get('see_more')}
                    </Link>
                </Box>
                {
                    data.specialProduct.length > 0 &&
                    <Box width={1} component={Card} py={1} style={{height: '100%'}}>
                        <ProductsContainerSlider  {...props} data={data.specialProduct}/>
                    </Box>
                }
            </Box>
        </Box>
    );
};


export function BoxItemSliderPlaceHolder() {
    return (
        <Box mx={2}
             my={2}
             height={{
                 xs: 150,
                 md: 250,
                 lg: 350
             }}>
            <Hidden mdUp>
                <Box width='100%' height='100%'>
                    <Box height="80%" mx={2}>
                        <Skeleton style={{height: '90%'}}/>
                    </Box>
                </Box>
            </Hidden>
            <Hidden smDown>
                <Box display="flex" width='100%' height='100%'>
                    <Box width='30%' height="100%" mx={2}>
                        <Skeleton style={{height: '90%'}}/>
                    </Box>
                    <Box width='70%' height="100%" mx={2}>
                        <Skeleton style={{height: '90%'}}/>
                    </Box>
                </Box>
            </Hidden>
        </Box>
    )
}
