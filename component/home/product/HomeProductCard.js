import React from 'react';
import {makeStyles} from "@material-ui/styles";
import PropTypes from "prop-types";
import ProductCard from "../../product/ProductCard";

const styles = makeStyles(theme => ({
    root: {
        // maxWidth:'95%',
        // [theme.breakpoints.up('660')]: {
        //     width: '280px',
        // },
        // [theme.breakpoints.up('680')]: {
        //     width: '280px',
        // },
        // [theme.breakpoints.up('740')]: {
        //     width: '310px',
        // },
        // [theme.breakpoints.up('800')]: {
        //     width: '220px',
        // },
        // [theme.breakpoints.up('850')]: {
        //     width: '250px',
        // },
        // [theme.breakpoints.up('870')]: {
        //     width: '153px',
        // },
        // [theme.breakpoints.up('960')]: {
        //     width: '147px !important',
        // },
        // [theme.breakpoints.up('1024')]: {
        //     width: '157px !important',
        // },
        // [theme.breakpoints.up('1200')]: {
        //     width: '190px !important',
        // },
        // [theme.breakpoints.up('1281')]: {
        //     width: '161px !important',
        // },
        // [theme.breakpoints.up('1450')]: {
        //     width: '196px !important',
        // },
        // [theme.breakpoints.up('1600')]: {
        //     width: '270px !important',
        // },
        // [theme.breakpoints.up('1750')]: {
        //     width: '300px !important',
        // },
        // [theme.breakpoints.up('xl')]: {
        //     width: '315px !important',
        // },
        // '&:oldImg':{
        //     height:'auto'
        // }
    },
}));

function HomeProductCard(props) {
    const classes = styles(props);
    return (
        <ProductCard
            className={classes.root}
            {...props}/>
    )
}

HomeProductCard.defaultProps = {
    elevation: 3
};

HomeProductCard.propTypes = {
    id: PropTypes.number,
    permalink: PropTypes.string.isRequired,
    name: PropTypes.string,
    discountPrice: PropTypes.number,
    discountPercent: PropTypes.number,
    finalPrice: PropTypes.number,
    imageSrc: PropTypes.string,
    imageAlt: PropTypes.string,
    category: PropTypes.number,
    availableCountForSale: PropTypes.number,
    onClick: PropTypes.func,
    onAddFavoriteClick: PropTypes.func,
    onAddToBasketClick: PropTypes.func,
    loading: PropTypes.bool,
    elevation: PropTypes.number
};
export default HomeProductCard;

