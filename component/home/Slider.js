import React, {createRef} from "react";
import {makeStyles} from "@material-ui/core";
import Box from '@material-ui/core/Box'
import {colors} from "../../repository";
import clsx from 'clsx'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import {UtilsStyle} from "../../utils/Utils";
import Loadable from "react-loadable";
import ComponentError from "../base/ComponentError";
import {SpecialOffersColumnPlaceHolder} from "./SpecialProductsColumn";


const arrowStyles = makeStyles(theme => ({
    buttonIcon: {
        position: "absolute",
        zIndex: "2",
        opacity: 0.5,
        backgroundColor: colors.backgroundColor.darker,
        color: "#fff",
        top: '50%',
        bottom: '50%',
        cursor: 'pointer',
        ...UtilsStyle.borderRadius("100%"),
        ...UtilsStyle.transition(),
        '&:hover': {
            opacity: 1,
            transform: 'scale(1.2)',
            ...UtilsStyle.transition()
        },
        [theme.breakpoints.up('md')]: {
            width: '50px',
            height: '50px',
            '& svg': {
                width: '50px',
                height: '50px'
            }
        },
        [theme.breakpoints.down('sm')]: {
            width: '35px',
            height: '35px',
            '& svg': {
                width: '35px',
                height: '35px'
            }
        },
        [theme.breakpoints.down('xs')]: {
            width: '20px',
            height: '20px ',
            '& svg': {
                width: '20px',
                height: '20px'
            }
        },

    },
    buttonLeftIcon: {
        [theme.breakpoints.up('md')]: {
            left: '15px'
        },
        [theme.breakpoints.down('sm')]: {
            left: '10px'
        },
        [theme.breakpoints.down('xs')]: {
            left: '5px'
        },
    },
    buttonRightIcon: {
        [theme.breakpoints.up('md')]: {
            right: '15px'
        },
        [theme.breakpoints.down('sm')]: {
            right: '10px'
        },
        [theme.breakpoints.down('xs')]: {
            right: '5px'
        },
    }
}));

function NextArrow(props) {
    const {onClick} = props;
    const classes = arrowStyles(props);
    return (
        <Box
            className={clsx(classes.buttonIcon, classes.buttonRightIcon)}>
            <ChevronLeft onClick={onClick}/>
        </Box>
    );
}

function PrevArrow(props) {
    const {onClick} = props;
    const classes = arrowStyles(props);
    return (
        <Box
            className={clsx(classes.buttonIcon, classes.buttonLeftIcon)}>
            <ChevronRight onClick={onClick}/>
        </Box>
    );
}

const sliderStyles = makeStyles(theme => ({
    sliderRoot: {
        position: 'relative',
        '& .tns-outer [data-action]': {
            display: 'none'
        },
        '& .tns-slider': {
            display: 'flex'
        }
    }
}));

const settings = {
    lazyload: true,
    nav: false,
    mouseDrag: true,
    loop: true,
    items: 1,
    controls: false,
    speed: 800,
    swipeAngle: false,
    arrowKeys: false,
    autoplayButton: false,
    autoplay: true,
    autoplayTimeout: 6000,
    autoplayHoverPause: true,
    center: true,
    axis: 'horizontal'
};

function SimpleSlider(props) {
    const classes = sliderStyles(props);
    let sliderRef = createRef();
    const onGoTo = dir => {
        try {
            sliderRef.slider.goTo(dir)
        } catch (e) {

        }
    };


    return (
        <Box dir={'ltr'} className={classes.sliderRoot} style={{...props.style}}>
            <NextArrow
                onClick={() => {
                    onGoTo('prev');
                }}/>
            <PrevArrow
                onClick={() => {
                    onGoTo('next');
                }}/>
            <TinySlider settings={settings}
                        style={{
                            height: 100
                        }}
                        tinyRef={ts => {
                            sliderRef = ts;
                        }}>
                {props.items.map((item, index) => (
                    <Box key={`${item.id}-${item.title}`}
                         style={{
                             textAlign: 'center',
                             height: "auto",
                         }}>
                        <a
                            href={item.url}
                            no_hover="true"
                            target={'_blank'}>
                            <img
                                style={{
                                    width: '95%',
                                    height: "auto",
                                    ...UtilsStyle.borderRadius(15),
                                }}
                                alt={item.title}
                                src={item.media.image}/>
                        </a>
                    </Box>
                ))}
            </TinySlider>
        </Box>
    );
}

const TinySlider = Loadable.Map({
    loader: {
        Component: () => import('tiny-slider-react-ar'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;

        return (
            <React.Fragment>

                <Component ref={props.tinyRef} {...props}/>
            </React.Fragment>
        );
        // return <Box>s</Box>
    },
});
export default SimpleSlider;

