import React, {createRef, Fragment} from 'react';
import PropTypes from "prop-types";
import TextHeader from '../base/textHeader/TextHeader'
import SpecialOffers from "./SpecialProducts";
import Box from "@material-ui/core/Box";
import {colors, lang} from "../../repository"
import rout from "../../router"
import Container from "@material-ui/core/Container";
import Hidden from "@material-ui/core/Hidden";
import ProductsContainerHeader from "./product/productsContainer/ProductsContainerHeader";
import clsx from "clsx";
import {Card, createTheme, makeStyles} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import _ from 'lodash'
import {NextArrow, PrevArrow} from "./product/productsContainer/ProductsContainerSlider";
import {UtilsStyle} from "../../utils/Utils";
import {cyan} from "@material-ui/core/colors";
import Typography from "../base/Typography";
import ProductCard from "../product/ProductCard";
import Loadable from "react-loadable";
import ComponentError from "../base/ComponentError";
import Holder from "../base/Placeholder";
import Link from "../base/link/Link";
import ErrorBoundary from "../base/ErrorBoundary";

const useStyles = makeStyles(theme => (
    {
        rootStyle: {},
        moreProductLinkStyle: {
            position: "absolute",
            padding: theme.spacing(1),
            paddingTop: theme.spacing(2),
            bottom: 5,
            right: 8,
            '&:hover': {
                color: 'red'
            }
        }
    }));

export default function SpecialProductsColumn({data, ...props}) {
    const theme = createTheme();
    const {moreProductLinkStyle} = useStyles(props);
    return (
        <Fragment>
            {!_.isEmpty(data) &&
            <Box width={1}>
                <Hidden smDown>
                    <Container maxWidth="xl" style={{...props.style}}>
                        <Box mt={[6, 7, 6]} mb={[3, 4, 5]}>
                            <TextHeader component="h2">
                                {lang.get('offers_of')}
                                <span style={{
                                    color: colors.danger.main,
                                    paddingRight: 3
                                }}>{" " + lang.get('special')}</span>
                            </TextHeader>
                        </Box>
                        <ErrorBoundary elKey={"SpecialProductsColumn::SpecialOffers"}>
                            <SpecialOffers data={data}/>
                        </ErrorBoundary>
                    </Container>
                </Hidden>
                <Hidden mdUp>
                    <Box style={{...props.style}}>
                        <Box my={[3, 4, 5]}
                             style={{
                                 overflow: 'hidden'
                             }}>
                            <Container maxWidth="xl" style={{position: 'relative'}}>
                                <ProductsContainerHeader
                                    style={{
                                        margin: 0
                                    }}
                                    container_style={{
                                        paddingTop: theme.spacing(3),
                                        paddingBottom: theme.spacing(3)
                                    }}
                                    borderColor="#a56">
                                    {lang.get('offers_of')}
                                    <span style={{color: colors.danger.main}}>{" " + lang.get('special')}</span>
                                </ProductsContainerHeader>
                                <Link href={rout.Main.home}>
                                    <a className={clsx(moreProductLinkStyle, 'transition500', 'transition500Hover')}>
                                        <Typography variant={'body2'}>
                                            {lang.get('see_more')}
                                        </Typography>
                                    </a>
                                </Link>
                            </Container>
                            <Box>
                                <SmSpecialProductColumn data={data} {...props}/>
                            </Box>
                        </Box>
                    </Box>
                </Hidden>
                <Holder/>
            </Box>}
        </Fragment>
    )
}

SpecialProductsColumn.prototype = {
    data: PropTypes.object,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};


export function SpecialOffersColumnPlaceHolder() {
    let theme = createTheme();
    return (
        <Box mx={2}
             my={2}
             height={{
                 xs: 150,
                 md: 250,
                 lg: 350
             }}>
            <Hidden mdUp>
                <Box width='100%' height='100%'>
                    <Skeleton style={{height: '100%', marginRight: theme.spacing(2), marginLeft: theme.spacing(2)}}/>
                </Box>
            </Hidden>
            <Hidden smDown>
                <Hidden lgUp>
                    <Box display="flex" width='100%' height='100%' flexDirection="column">
                        <Box height="80%" mx={2}>
                            <Skeleton style={{height: '90%'}}/>
                        </Box>
                        <Box height="20%" mx={2}>
                            <Skeleton style={{height: '90%'}}/>
                        </Box>
                    </Box>
                </Hidden>
                <Hidden mdDown>
                    <Box display="flex" width='100%' height='100%'>
                        <Box width='20%' mx={2}>
                            <Skeleton style={{marginTop: 0, height: '100%'}}/>
                        </Box>
                        <Box width='80%' mx={2}>
                            <Skeleton style={{marginTop: 0, height: '100%'}}/>
                        </Box>
                    </Box>
                </Hidden>
            </Hidden>
        </Box>
    )
}

//region SmSpecialProductColumn


const smSpecialProductColumnSliderStyles = makeStyles(theme => ({
    smSpecialProductColumnSliderRoot: {
        position: 'relative',
        '& .tns-outer [data-action]': {
            display: 'none'
        },
        '& .tns-slider': {
            display: 'flex',
            overflow: 'hidden',
        }
    }
}));
const settings = {
    lazyload: true,
    nav: false,
    mouseDrag: true,
    loop: false,
    items: 1.7,
    controls: false,
    speed: 600,
    swipeAngle: false,
    center: false,
    edgePadding: 10,
    responsive: {
        380: {
            items: 2.5,
        },
        500: {
            items: 2.5,
        },
        680: {
            items: 3.1,
        },
        960: {
            items: 3.5,
        },
        1920: {
            items: 5,
        }
    }
};

export function SmSpecialProductColumn({data, ...props}) {
    const classes = smSpecialProductColumnSliderStyles(props);
    let sliderRef = createRef();
    const onGoTo = dir => sliderRef.slider.goTo(dir);
    return (
        <Box dir='ltr'
             py={1}
             className={classes.smSpecialProductColumnSliderRoot}
             style={{
                 zIndex: '9',
             }}>
            <NextArrow
                onClick={() => {
                    onGoTo('prev');
                }}/>
            <PrevArrow
                onClick={() => {
                    onGoTo('next');
                }}/>
            <TinySlider dir='ltr' settings={settings}
                        tinyRef={ts => {
                            sliderRef = ts;
                        }}
                        style={{}}>
                {
                    [...data, data[0]].map((product, i) => (
                        i === data.length ?
                            <Box key={i} px={0.5} pb={1}>
                                <Box
                                    component={Card}
                                    height={1}
                                    style={{
                                        backgroundColor: cyan[100]
                                    }}>
                                    <Typography>
                                        {lang.get("show_more")}
                                    </Typography>
                                </Box>
                            </Box> :
                            <SmSpecialProductColumnItem key={product.id} index={i}
                                                        data={product}
                                                        style={{
                                                            opacity: i >= data.length ? 0 : 1
                                                        }}/>
                    ))
                }
            </TinySlider>
        </Box>
    )
}


export function SmSpecialProductColumnItem({index, data, ...props}) {
    return (
        <Box px={0.5} pb={1}  {...props}
             style={{
                 direction: 'rtl',
                 ...UtilsStyle.disableTextSelection(),
                 ...props.style
             }}>
            <ProductCard
                id={data.id}
                imageSrc={data.thumbnail}
                name={data.name}
                discountPrice={data.discountPrice}
                finalPrice={data.finalPrice}
                discountPercent={data.discountPercent}
                deadline={data.deadline}
                availableCountForSale={data.maxCountForSale}
                permalink={data.permalink}/>
        </Box>
    )
}


const TinySlider = Loadable.Map({
    loader: {
        Component: () => import('tiny-slider-react-ar'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component ref={props.tinyRef} {...props}/>;
        // return <Box>s</Box>
    },
});

//region SmSpecialProductColumn
