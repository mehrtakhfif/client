import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import {colors, lang} from "../../repository";
import rout from "../../router";
import withWidth, {isWidthDown} from "@material-ui/core/withWidth/withWidth";
import Img from "../base/oldImg/Img";
import clsx from "clsx";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import _ from "lodash";
import Typography from "../base/Typography";
import {grey, red} from "@material-ui/core/colors";
import moment from "moment";
import Link from "../base/link/Link";
import ErrorBoundary from "../base/ErrorBoundary";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        height: 350,
        flexDirection: 'row-reverse',
        ...UtilsStyle.borderRadius(theme.spacing(1)),
        [theme.breakpoints.down('md')]: {
            flexDirection: 'column-reverse'
        },
    },
    tabs: {
        minWidth: "230px",
        borderLeft: `1px solid ${theme.palette.divider}`,
        borderTopLeftRadius: theme.spacing(1),
        borderBottomLeftRadius: theme.spacing(1),
        '& > div ': {
            '& > div > button': {
                minWidth: '230px',
                '&.Mui-selected': {
                    backgroundColor: theme.palette.secondary.main,
                    color: '#fff',
                    borderLeft: '3px solid #fff'
                },
                [theme.breakpoints.down('md')]: {
                    display: '200px'
                },
            },
            '& > span.MuiTabs-indicator': {
                right: 'auto',
                left: 0,
                [theme.breakpoints.down('md')]: {
                    display: 'none'
                },
            }
        }
    },
    itemRoot: {
        width: '100%',
        height: 'auto',
        [theme.breakpoints.down('md')]: {
            width: 'auto',
            height: '100%',
        },
    }
}));

const itemUseStyles = makeStyles(theme => (
    {
        itemUseStylesRoot: {
            width: '100%',
            height: '100%',
            overflow: 'hidden',
            position: 'relative',
            borderBottomRightRadius: theme.spacing(1),
            borderTopRightRadius: theme.spacing(1),
        },
        itemRoot: props => ({
            width: '100%',
            height: '100%',
            overflow: 'hidden',
            position: 'relative',
            '&:before': {
                content: '"' + props.background + '"',
                position: 'absolute',
                backgroundImage: 'url(' + props.background + ')',
                filter: 'blur(8px)',
                '-webkit-filter': 'blur(8px)',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center bottom',
                backgroundSize: '100% auto',
                transform: 'scale(1.1)'
            },
            '& > div': {
                position: 'absolute',
            },
        }),
        img: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            '& > a,& > a img': {
                width: 450,
                maxWidth: 450,
                [theme.breakpoints.down('md')]: {
                    width: 350,
                    maxWidth: 350,
                },
            },
            '& > a div': {
                bottom: 0,
                position: 'absolute !important',
                right: '2%',
                '& img': {
                    marginBottom: 5,
                    cursor: 'pointer',
                    ...UtilsStyle.borderRadius(10),
                }
            }
        },
        itemUseBox: {
            paddingTop: theme.spacing(3),
            paddingRight: theme.spacing(6),
            paddingLeft: theme.spacing(6),
            paddingBottom: theme.spacing(2),
            borderBottomRightRadius: theme.spacing(1),
            borderBottomLeftRadius: theme.spacing(1),
            backgroundColor: '#ffffff4d',
            maxWidth: 400,
            left: '5%',
            '-webkit-box-shadow': '0px 4px 9px 1px #45454575',
            'box-shadow': '0px 4px 9px 1px #45454575',
            '&:before': {
                content: '""',
                backgroundColor: '#b6b6b654',
                position: 'absolute',
                width: '100%',
                height: '100%',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                borderBottomRightRadius: theme.spacing(1),
                borderBottomLeftRadius: theme.spacing(1),
            }
        },
        skeleton: {
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            '& > span > span': {
                ...UtilsStyle.borderRadius(0),
                lineHeight: 3,
            }
        },
        ribbon: {
            backgroundColor: colors.danger.dark,
            color: '#fff',
            display: 'block',
            filter: 'drop-shadow(0 1px 2px rgba(0, 0, 0, 0.5))',
            '-webkit-filter': 'drop-shadow(0 1px 2px rgba(0, 0, 0, 0.5))',
            fontSize: 25,
            lineHeight: '1.02',
            margin: '20px auto',
            maxWidth: '70%',
            padding: '10px 0',
            position: 'relative',
            textAlign: 'center',
            textDecoration: 'none',
            width: 350,
            transition: 'all 300ms',
            top: 10,
            '&:after': {
                content: '""',
                position: 'absolute',
                top: 0,
                border: '22.5px solid',
                borderColor: colors.danger.dark,
                zIndex: -1,
                right: -30,
                borderRight: '30px solid transparent',
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
            }
        }
    }));

// props => props.data ? props.data.media ? props.data.media : props.data.storage.product.media.file : 'http://192.168.1.95/media/boxes/3/2019-09-03/image/11-12-31-54.jpg'
// backgroundImage: 'url(' + props ? props.data ? props.data.media ? props.data.media : props.data.storage.product.media.file : 'http://192.168.1.95/media/boxes/3/2019-09-03/image/11-12-31-54.jpg' : 'http://192.168.1.95/media/boxes/3/2019-09-03/image/11-12-31-54.jpg' + ')',

function Item({data, value, index, ...props}) {
    const classes = itemUseStyles({...props, background: data.thumbnail.image});
    const rootClass = [classes.img];
    const singleLink = rout.Product.SingleV2.create(permalink);
    const name = !_.isEmpty(data.name) ? data.name : !_.isEmpty(data.storageTitle) ? data.storageTitle : !_.isEmpty(data.productName) ? data.productName : data.labelName
    return (
        <ErrorBoundary elKey={"SpecialProducts::Item"}>
            <Box
                className={clsx(classes.itemUseStylesRoot)}
                component="div"
                role="special product panel"
                hidden={value !== index}
                id={`vertical-tabpanel-${index}`}
                aria-labelledby={`vertical-tab-${index}`}
                {...props}>
                <div className={classes.itemRoot}>
                    <Box className={clsx(...rootClass)}>
                        <Link href={singleLink} isText={false}>
                            <a no_hover="true">
                                <Img
                                    src={data.thumbnail}
                                    alt={name}/>
                            </a>
                        </Link>
                    </Box>
                    {data.discountPercent ?
                        <Box
                            dir='rtl'
                            display="flex"
                            alignItems="center"
                            justifyContent="center">
                            <Box className={classes.ribbon} fontSize='h5.fontSize' fontWeight='700' mx={2}
                                 component="h6"
                                 align='center'
                                 style={{margin: '0 auto 0 0', color: '#fff'}}>
                                %{data.discountPercent} تخفیف
                            </Box>
                        </Box>
                        : null}
                    <div className={classes.itemUseBox}>
                        <Box style={{position: 'relative'}}>
                            <Link variant={'h4'} color={grey[800]} href={singleLink}
                                  fontWeight={600}
                                  style={{
                                      display: 'flex',
                                      alignItems: 'center',
                                      justifyContent: 'center'
                                  }}>
                                {name}
                            </Link>
                            <Box display='flex'
                                 alignItems="center"
                                 justifyContent="center" mt={3}>
                                <Box display='flex'
                                     width="70%"
                                     alignItems="center"
                                     justifyContent="center">
                                    <Box dir='rtl'
                                         display="flex"
                                         alignItems="center"
                                         justifyContent="center"
                                         mx={2}
                                         my={1}
                                         align='center'
                                         mt={1}
                                         style={{
                                             textShadow: '-1px 1px 5px #FFFFFF33',
                                         }}>
                                        <Box fontSize='h3.fontSize' fontWeight='700' mx={2} component="h6"
                                             align='center'
                                             style={{margin: '0 auto 0 5px', color: colors.danger.main}}>
                                            {UtilsFormat.numberToMoney(data.discountPrice)}
                                        </Box>
                                        <Box fontSize='subtitle2.fontSize' component="span"
                                             style={{
                                                 margin: '0 0 0 auto',
                                                 color: colors.danger.main
                                             }}>{lang.get('toman')}</Box>
                                    </Box>
                                </Box>
                                <Box display='flex'
                                     flexDirection="column"
                                     width="50%"
                                     alignItems="center"
                                     justifyContent="center"
                                     fontSize='h6.fontSize'
                                     mx={2}
                                     align='center'
                                     style={{
                                         color: grey[700], textDecoration: 'line-through',
                                         textShadow: '-1px 1px 5px #FFFFFF66',
                                     }}>
                                    {UtilsFormat.numberToMoney(data.finalPrice)}
                                </Box>
                            </Box>
                            <Box style={{textAlign: 'center'}}>
                                {data.deadline * 1000 > new Date().getTime() ?
                                    <>
                                        <Timer time={data.deadline}/>
                                        <Box mt={2} style={{color: "#f1f1f1"}}>
                                            {lang.get('order_remaining_time')}
                                        </Box>
                                    </> :
                                    <Box mt={2} style={{color: "#f1f1f1"}}>
                                        <Typography variant={'h6'} color={grey[100]} py={1.2} px={3}
                                                    style={{
                                                        backgroundColor: red[400],
                                                        justifyContent: 'center',
                                                        ...UtilsStyle.borderRadius(5)
                                                    }}>
                                            {lang.get('the_end')}
                                        </Typography>
                                    </Box>
                                }
                            </Box>
                        </Box>
                    </div>
                </div>
            </Box>
        </ErrorBoundary>
    )
}

export function Timer({time, textVariant = 'h6', textColor = grey[200], itemPadding = 0.5, ...props}) {
    const [state, setState] = useState({
        duration: moment.duration((time - moment().unix()) * 1000, 'milliseconds')
    });
    useEffect(() => {
        const interval = setInterval(() => {
            setState({
                ...state,
                duration: moment.duration((time - moment().unix()) * 1000 - interval, 'milliseconds')
            });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    function serializeTime(val) {
        const v = _.toString(val);
        return v.length === 1 ? '0' + v : v;
    }

    const du = state.duration;
    const days = serializeTime(Math.floor(du.asDays()));
    const hours = serializeTime(du.hours());
    const minutes = serializeTime(du.minutes());
    const seconds = serializeTime(du.seconds());

    return (
        <Box display={'flex'} alignItems={'flex-start'} justifyContent={'center'}
             {...props}
             style={{color: '#f1f1f1', ...props.style}}>
            <TimerItem time={days} title={lang.get('day')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
            <Typography variant={textVariant} color={textColor} px={0.2}>
                :
            </Typography>
            <TimerItem time={hours} title={lang.get('hour')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
            <Typography variant={textVariant} color={textColor} px={0.2}>
                :
            </Typography>
            <TimerItem time={minutes} title={lang.get('minute')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
            <Typography variant={textVariant} color={textColor} px={0.2}>
                :
            </Typography>
            <TimerItem time={seconds} title={lang.get('second')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
        </Box>
    )
}

function TimerItem({time, title, textVariant, textColor, itemPadding, ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'}
             justifyContent={'flex-end'} px={itemPadding}>
            <Typography variant={textVariant} color={textColor}>
                {time}
            </Typography>
            <Typography variant={'caption'} color={textColor}>
                {title}
            </Typography>
        </Box>
    )
}

function SpecialProducts({data, ...props}) {
    const classes = useStyles();
    const [value, setValue] = React.useState({
        value: 0,
        time: new Date().getTime()
    });
    const [timer, setTimer] = useState()

    const timeout = 16000;
    useEffect(() => {
        clearTimeout(timer);
        return
        setTimer(setTimeout(() => {
            if (value.value + 1 === data.length) {
                handleChange(null, 0);
                return
            }
            handleChange(null, value.value + 1);
        }, timeout));

        return () => clearTimeout(timer);
    }, [value.value]);

    function handleChange(event, newValue) {
        try {
            setValue({
                ...value,
                value: newValue,
                time: new Date().getTime()
            });
        } catch (e) {

        }
    }

    const tabsLayout = [];
    const tabsItemLayout = [];
    if (data) {
        _.forEach(data, (item, index) => {
            tabsLayout.push(<Tab key={'tab-' + item.id}
                                 label={!_.isEmpty(item.labelName) ? item.labelName : item.name} {...a11yProps(item.id)}/>);
            tabsItemLayout.push(<Item key={'item-' + item.id} index={index} data={item} value={value.value}/>);
        });
    } else {
        for (let i = 0; i < 7; i++) {
            tabsLayout.push(
                <Tab key={i}
                     label='-------' {...a11yProps(i)}
                     style={{
                         pointerEvents: 'none'
                     }}/>
            )
        }
    }

    return (
        <Box dir='ltr' mx={2} className={classes.root} style={{padding: "0"}}>
            <ErrorBoundary elKey={"SpecialOffers"}>
                <Tabs
                    orientation={isWidthDown('md', props.width) ? 'horizontal' : 'vertical'}
                    variant="scrollable"
                    value={value.value}
                    onChange={handleChange}
                    aria-label="Special Offers"
                    className={classes.tabs}>
                    {tabsLayout}
                </Tabs>
                <div className={classes.itemRoot}>
                    {
                        data && tabsItemLayout
                    }
                </div>
            </ErrorBoundary>
        </Box>
    );
}

SpecialProducts.prototype = {
    data: PropTypes.object,
};

export default withWidth()(SpecialProducts)

