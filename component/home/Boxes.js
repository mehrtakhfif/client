import React from "react";
import Box from "@material-ui/core/Box";
import Img from "../base/oldImg/Img";
import {UtilsStyle} from "../../utils/Utils";
import {makeStyles} from "@material-ui/core";


export default function Boxes(props) {


    return (
        <Box px={4} pt={4} display='flex' alignItems='center' width={1} flexWrap="wrap">
            {/*<Item src={Box1}/>*/}
            {/*<Item src={Box2}/>*/}
            {/*<Item src={Box3}/>*/}
            {/*<Item src={Box4}/>*/}
            {/*<Item src={Box5}/>*/}
            {/*<Item src={Box6}/>*/}
            {/*<Item src={Box7}/>*/}
            {/*<Item src={Box8}/>*/}
            {/*<Item src={Box9}/>*/}
            {/*<Item src={Box10}/>*/}
        </Box>
    )

}
const useStyles = makeStyles(theme => (
    {
        root: {
            "& img": {
                width: '95%',
                height: 'auto',
                cursor: 'pointer',
                ...UtilsStyle.borderRadius(5),
                ...UtilsStyle.transition(500),
                '&:hover': {
                    ...UtilsStyle.borderRadius(15),
                    ...UtilsStyle.transition(500),
                }
            }
        }
    }));

function Item({src, ...props}) {
    const classes = useStyles(props);
    return (
        <Box width="20%" className={classes.root} my={1} display='flex' justifyContent='center' alignItems='center'>
            <Img
                alt={'box image'} src={src}/>
        </Box>
    )
}
