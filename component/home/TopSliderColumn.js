import React from "react";
import Box from "@material-ui/core/Box";
import Slider from "./Slider"
import Skeleton from '@material-ui/lab/Skeleton';
import ControllerSite from "../../controller/ControllerSite";
import useSWR, {mutate} from "swr";
import ComponentError from "../base/ComponentError";


export default function TopSliderColumn(props) {
    const d = ControllerSite.Slider.getHomeSlider();
    const {data, error} = useSWR(...d);

    return (
        <Box mt={[2, 3, 4]} style={{overflow:'hidden'}}>
            {
                error ?
                    <ComponentError tryAgainFun={() => () => mutate(d[0])}/> :
                    (data && data.slider) ?
                        <Slider items={data.slider} style={{minHeight: 70}}/>
                        :
                        <Box mx={4}
                             my={2}
                             height={{
                                 xs: '70px',
                                 md: '200px',
                                 lg: '300px'
                             }}>
                            <Skeleton style={{height: '100%'}}/>
                        </Box>}
        </Box>
    )
}

export function TopSliderColumnPlaceHolder() {
    return (
        <Box mx={4}
             my={2}
             height={{
                 xs: 70,
                 md: 200,
                 lg: 300
             }}>
            <Skeleton style={{height: '100%'}}/>
        </Box>
    )
}


