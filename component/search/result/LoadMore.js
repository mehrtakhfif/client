import React from "react";
import {Box, Button, getSafe, Typography} from "material-ui-helper";
import {colors, lang} from "../../../repository";
import _ from "lodash"
import LinearProgress from "@material-ui/core/LinearProgress";
import {withStyles} from "@material-ui/core";


const BorderLinearProgress = withStyles((theme) => ({
    root: {
        height: 4,
        width: '100%',
        borderRadius: 5,
        backgroundColor: "#e9e9e9",
    },
    colorPrimary: {},
    bar: {
        borderRadius: 5,
    },
}))(LinearProgress);

export default function LoadMore({step, page, maxPage, allCount,isLoadingMore, onLoadMore}) {

    const showMoreItem = getSafe(()=>(step + 1) * page <= allCount ? step : allCount - (step * page),1);
    return (
        (page * step)?
        <Box width={1} pt={7} center={true} flexDirectionColumn={true}>
            <Box flexDirectionColumn={true} center={true}>
                <Typography pb={3} variant={"body1"} color={colors.textColor.h757575}>
                    {lang.get("you_have_viewed_x_out_of_y_products", {
                        args: {
                            x: getSafe(() => _.toString(page * step)),
                            y: getSafe(() => _.toString(allCount))
                        }
                    })}
                </Typography>
                <Box width={1} mb={3} px={4}>
                    <BorderLinearProgress color={"secondary"} variant="determinate" value={page * 100 / maxPage}/>
                </Box>
                <Button
                    variant={"outlined"}
                    onClick={onLoadMore}
                    disabled={isLoadingMore}
                    typography={{
                        variant:"body1",
                        fontWeight: "normal"
                    }}>
                    {lang.get("show_more")} ({showMoreItem || 1})
                </Button>
            </Box>
        </Box>:
            <React.Fragment/>
    )
}

