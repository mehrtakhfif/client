import React, {useContext, useEffect, useMemo} from "react";
import ControllerProduct from "../../../controller/ControllerProduct";
import {Box, getSafe} from "material-ui-helper";
import Backdrop from "../../base/Backdrop";
import {useTheme} from "@material-ui/core";
import {useRouter} from "next/router";
import ProductCardV2 from "../../product/ProductCardV2/ProductCardV2";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import {colors} from "../../../repository";
import ComponentError from "../../base/ComponentError";
import ResultIsEmpty from "./resultIsEmpty/ResultIsEmpty";
import _ from "lodash"
import useSWRInfinite from "../../../lib/useSWRInfinite";
import LoadMore from "./LoadMore";
import ParamsContext from "../context/ParamsContext";
import {DEBUG} from "../../../pages/_app";
import CategoryDetails from "../CategoryDetails";


const useStyle = makeStyles(theme => ({
    productCard: {
        borderRight: `1px solid ${colors.borderColor.hebebeb}`,
        borderTop: `1px solid ${colors.borderColor.hebebeb}`,
        "&#productCard-1, &#productCard-2": {
            borderTopColor: 'transparent'
        },
        [theme.breakpoints.up('md')]: {
            "&#productCard-3": {
                borderTopColor: 'transparent'
            },
            "&.last, &.end": {
                borderRightColor: 'transparent'
            }
        },
        [theme.breakpoints.only('sm')]: {
            "&.odd, &.end": {
                borderRightColor: 'transparent'
            }
        },
        [theme.breakpoints.only('xs')]: {
            "&": {
                borderRightWidth: 0,
                borderTopColor: `${colors.borderColor.hebebeb} !important`
            }
        },
    },
    singleRoot: {
        [theme.breakpoints.up('xs')]: {
            marginTop: theme.spacing(2),
        },
        [theme.breakpoints.up('lg')]: {
            marginTop: theme.spacing(3),
        },
    },
}));


function ResultContainer({initialData}) {
    const theme = useTheme()
    const {query} = useRouter();
    const classes = useStyle();
    const {loading: baseLoadingState} = useContext(ParamsContext);
    const [loading, ___, onFinishLoading] = baseLoadingState;
    const {getKey, fetcher} = ControllerProduct.Query.V2.Query2;
    const api = useMemo(() => getKey(query), [query]);

    //region swr
    const {
        data,
        error,
        page,
        maxPage,
        allCount,
        step,
        mutate,
        setNextPage,
        onClear,
        isEmpty,
        isLoadingMore,
        isReachingEnd,
        isRefreshing
    } = useSWRInfinite(api, fetcher, {
        revalidateOnFocus: false,
        initialData: initialData
    })
    //endregion swr

    useEffect(() => {
        if (data)
            onFinishLoading()
    }, [data])

    if (!isLoadingMore && _.isEmpty(data))
        return <ResultIsEmpty/>

    const renderData = getSafe(() => {
        const d = data || [];
        if (isLoadingMore && !error)
            return d.concat(getSafe(() => new Array(step || 6).fill({})))
        return d
    }, [])

    return (
        <React.Fragment>
            <Box
                px={{xs: 2, lg: 0}}
                flexWrap={"wrap"}>
                {renderData.map((product, index) => {
                    //region createClass
                    const cl = [classes.productCard, index % 2 ? "odd" : "even"]
                    cl.push(getSafe(() => {
                        let st = renderData?.length === index + 1 ? "end" : ""
                        const i = index % 3;
                        if (i === 2)
                            return `last ${st}`
                        if (i === 1)
                            return `center ${st}`
                        return `first ${st}`
                    }, "").trim())
                    //endregion createClass

                    return (
                        <ProductCardV2
                            groupKey={"resultContainer"}
                            py={{
                                xs: 3,
                                md: 5
                            }}
                            id={`productCard-${index + 1}`}
                            width={{
                                xs: 1,
                                sm: 1 / 2,
                                md: 1 / 3,
                            }}
                            key={product?.id || index}
                            className={clsx([cl])}
                            product={product}/>
                    )
                })}
                {
                    error &&
                    <Box width={1}>
                        <ComponentError tryAgainFun={() => mutate()}/>
                    </Box>
                }
                {
                    !isReachingEnd &&
                    <LoadMore
                        step={step}
                        page={page}
                        maxPage={maxPage}
                        allCount={allCount}
                        onLoadMore={setNextPage}
                        isLoadingMore={isLoadingMore || error}/>
                }
            </Box>

            <CategoryDetails/>
            <Backdrop
                timeout={8000}
                zIndex={theme.zIndex.appBar + 5}
                open={!data || loading}
                showPleaseWait={true}
                onTimedOut={() => {
                    if (data && loading) {
                        onFinishLoading()
                    }
                }}/>
        </React.Fragment>
    )
}


export default React.memo(ResultContainer)
