import React from "react";
import {Box, Typography} from "material-ui-helper";
import {useRouter} from "next/router";
import {colors, lang} from "../../../../repository";
import {UtilsStyle} from "../../../../utils/Utils";
import OnlyQuery from "./onlyQuery/OnlyQuery";


export default function ResultIsEmpty() {
    const {query} = useRouter()

    return (
        (Object.keys(query).length === 1 && query["q"]) ?
            <OnlyQuery query={query["q"]}/> :
            <Base/>
    )
}


function Base() {

    return (
        <Box py={3} px={{xs:2,md:0}} width={1} flexDirectionColumn={true}>
            <Typography variant={"h5"} fontWeight={"bold"}>
                {lang.get("no_products_found")}
            </Typography>
            <Typography pt={4} variant={"subtitle1"} fontWeight={"normal"}>
                {lang.get("the_combination_of_filters_you_selected_did_not_match_any_products")}
            </Typography>
            <Box
                py={3}
                mt={4}
                px={{
                    xs: 2,
                    md: 4
                }}
                flexDirectionColumn={true}
                style={{
                    backgroundColor: colors.borderColor.hf8f8f8,
                    ...UtilsStyle.borderRadius(5)
                }}>
                <Typography variant={"h6"} fontWeight={500}>
                    {lang.get("our_suggestion")}
                </Typography>
                <Typography variant={"body1"} pt={2.5} fontWeight={"normal"}>
                    {lang.get("remove_some_filters")}
                </Typography>
            </Box>
        </Box>
    )
}
