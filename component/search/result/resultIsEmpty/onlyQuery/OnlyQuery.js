import {Box, Typography} from "material-ui-helper";
import {colors, lang} from "../../../../../repository";
import {UtilsStyle} from "../../../../../utils/Utils";
import React from "react";

export default function OnlyQuery({query}) {

    return (
        <Box py={3} width={1} flexDirectionColumn={true}>
            <Typography variant={"h5"} fontWeight={"bold"}>
                {lang.get("query_result_not_found", {
                    args: {
                        query: query
                    }
                }) + " "}
            </Typography>
            <Typography pt={4} variant={"subtitle1"} fontWeight={"normal"}>
                {lang.get("we_are_trying_every_day_to_add_more_products_to_the_discount")}
            </Typography>
            <Box py={3} mt={4} px={{
                xs: 2,
                md: 4
            }}
                 flexDirectionColumn={true}
                 style={{
                     backgroundColor:colors.borderColor.hf8f8f8,
                     ...UtilsStyle.borderRadius(5)
                 }}>
                <Typography variant={"h6"} fontWeight={500}>
                    {lang.get("our_suggestion")}
                </Typography>
                <Typography variant={"body1"} pt={2.5} fontWeight={"normal"}>
                    {lang.get("you_can_use_more_common_words_and_phrases")}
                </Typography>
                <Typography variant={"body1"} pt={2} fontWeight={"normal"}>
                    {lang.get("please_check_the_spelling_of_the_entered_phrase")}
                </Typography>
            </Box>
        </Box>
    )
}
