import React, {useContext} from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useTheme} from "@material-ui/core";
import {Box, HiddenLgUp} from "material-ui-helper";
import Divider from "@material-ui/core/Divider";
import OrderingSection from "./OrderingSection";
import FilterSection from "./FilterSection";
import FilterDataContext from "../context/FilterDataContext";
import ParamsContext from "../context/ParamsContext";
import _ from "lodash";

const useStyles = makeStyles((theme) => ({
    appBar: {
        top: 'auto',
        bottom: 0,
        backgroundColor: "#fff !important",
        color: "#000",
        "&>div": {
            padding: 0,
            display: 'flex',
            alignItems: "stretch"
        }
    },
}));

function SmFilter() {
    const theme = useTheme();
    const classes = useStyles();
    const [data,dataError] = useContext(FilterDataContext);
    const {loading: baseLoadingState} = useContext(ParamsContext);
    const [__, onFilterChange] = baseLoadingState;


    return (
        !_.isEmpty(data)?
        <React.Fragment>
            <HiddenLgUp>
                <Toolbar/>
                <Toolbar/>
                <AppBar position="fixed" color="primary" className={classes.appBar}>
                    <Toolbar>
                        <Box width={1} center={true}>
                            <FilterSection/>
                            <Box height={0.7}>
                                <Divider orientation="vertical" flexItem/>
                            </Box>
                            <OrderingSection />
                        </Box>
                    </Toolbar>
                </AppBar>
            </HiddenLgUp>
        </React.Fragment>:
            <React.Fragment/>
    )
}

export default React.memo(SmFilter)



