import React, {useContext} from "react";
import Drawer from "@material-ui/core/Drawer";
import {Box, ButtonBase, getSafe, Typography, useOpenWithBrowserHistory} from "material-ui-helper";
import {lang} from "../../../repository";
import SectionBase from "./SectionBase";
import {defaultOrderingItem, orderingItems} from "../../../pages/search/[[...cat]]";
import Divider from "@material-ui/core/Divider";
import {useRouter} from "next/router";
import MtIcon from "../../MtIcon";
import {useTheme} from "@material-ui/core";
import rout from "../../../router";
import ParamsContext from "../context/ParamsContext";

export default function OrderingSection() {
    const theme = useTheme();
    const router = useRouter();
    const {o,...query} = router.query;
    const activeOrder = getSafe(() => o || defaultOrderingItem.key, defaultOrderingItem.key)
    const [open,__, onOpen, onClose] = useOpenWithBrowserHistory("OrderingSection")
    const {loading: baseLoadingState} = useContext(ParamsContext);
    const [___, onFilterChange] = baseLoadingState;

    function handleClick(k) {
        onClose()
        if (activeOrder === k)
            return
        onFilterChange()
        setTimeout(()=>{
            router.push(...rout.Product.Search.create({o: k,...query}))
        },1000)
    }

    return (
        <React.Fragment>
            <Drawer anchor={"bottom"} open={open} onClose={onClose}>
                <Box width={1} flexDirectionColumn={true}>
                    <Typography px={2} py={2} variant={"subtitle1"} fontWeight={"normal"}>
                        {lang.get("order_by")}
                    </Typography>
                    <Divider/>
                    {
                        Object.keys(orderingItems).map(k => {
                            const isActive = k === activeOrder
                            const item = orderingItems[k];

                            return (
                                    <ButtonBase key={k} borderRadius={0} width={1} onClick={()=>handleClick(k)}>
                                        <Box py={1} px={1} alignItems={"center"}>
                                            <MtIcon color={isActive ? theme.palette.secondary.dark : "#00000000"}
                                                    variant={"subtitle1"} icon={"mt-check-thin"}/>
                                            <Typography color={isActive ? theme.palette.secondary.main :undefined} pl={1} variant={"body1"}>
                                                {lang.get(item.title)}
                                            </Typography>
                                        </Box>
                                    </ButtonBase>
                            )
                        })
                    }
                </Box>
            </Drawer>
            <SectionBase icon={"mt-sort"} onClick={onOpen}>
                {lang.get(o?orderingItems[o].title:"ordering")}
            </SectionBase>
        </React.Fragment>
    )
}


