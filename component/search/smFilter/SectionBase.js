import {Box, ButtonBase, Typography} from "material-ui-helper";
import MtIcon from "../../MtIcon";
import React from "react";

export default function SectionBase({children, icon,hasChildPath, onClick}) {

    return (
        <Box width={0.5} height={1} center={true}>
            <ButtonBase
                width={1}
                borderRadius={0} center={true}
                buttonProps={{style: {height: "100%"}}}
                onClick={onClick}>
                <Typography center={true} variant={"subtitle1"}>
                    {children}
                    <Box pl={0.6}>
                        <MtIcon hasChildPath={hasChildPath} icon={icon}/>
                    </Box>
                </Typography>
            </ButtonBase>
        </Box>
    )
}
