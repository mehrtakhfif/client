import {useRouter} from "next/router";
import React, {useCallback, useContext} from "react";
import _ from "lodash";
import {lang} from "../../../repository";
import SectionBase from "./SectionBase";
import {Box, Dialog, Typography, useOpenWithBrowserHistory, useStateWithCallbackLazy} from "material-ui-helper";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MtIcon from "../../MtIcon";
import SearchFilter from "../filter/SearchFilter";
import Slide from "@material-ui/core/Slide";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {filterItemsKey} from "../../../pages/search/[[...cat]]";
import rout from "../../../router";
import useFilterQuery from "../useFilterQuery";
import ParamsContext from "../context/ParamsContext";

function FilterSection() {
    //region variable
    const router = useRouter();
    const [open, __, onOpen, onClose] = useOpenWithBrowserHistory("FilterSection")
    const {q, o, ...query} = useFilterQuery()
    const {loading: baseLoadingState} = useContext(ParamsContext);
    const [___, onFilterChange] = baseLoadingState;
    //endregion variable

    //region states
    const [temp, setTemp] = useStateWithCallbackLazy(query)
    //endregion states

    //region handler
    const handleSubmit = useCallback((newTemp=temp) => {
        onFilterChange()
        setTimeout(() => {
            //Todo: check cat and order filter work
            onClose()
            setTimeout(() => {
                router.push(...rout.Product.Search.create({
                    ...newTemp,
                    q: q,
                    o: o,
                    category: query[filterItemsKey.cat]
                }))
            }, 300)
        }, 100)
    }, [temp,query])

    const handleDialogOpen = useCallback(()=>{
        if (open)
            return
        setTemp(query)
        onOpen()
    },[open,onOpen,query])
    //endregion handler


    return (
        <React.Fragment>
            <FilterDialog
                open={open}
                transition={TransitionUp}
                title={lang.get("filter")}
                onClose={onClose}>
                <SearchFilter
                    temp={temp}
                    setTemp={setTemp}
                    mobileFilterOpen={true}
                    onCloseDialogSm={handleSubmit}/>
            </FilterDialog>
            <SectionBase
                hasChildPath={true}
                icon={_.isEmpty(query) ? "mt-filter" : "mt-filter-on"}
                onClick={handleDialogOpen}>
                {lang.get("filter")}
            </SectionBase>
        </React.Fragment>
    )
}

export default React.memo(FilterSection)

//region Transition
const TransitionUp = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const TransitionSide = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="right" ref={ref} {...props} />;
});
//endregion Transition

//region FilterDialog
const useStyles = makeStyles((theme) => ({
    filterDialogAppBar: {
        "& > div": {
            backgroundColor: "#fff",
            color: "#000",
            paddingRight: theme.spacing(2),
            paddingLeft: theme.spacing(2)
        }
    }
}));

export function FilterDialog({open,title, transition = TransitionSide, onClose, ...pr}) {
    const classes = useStyles()
    const {children, ...props} = pr

    return (
        <Dialog open={open} transition={transition} fullScreen={true} onClose={onClose} {...props}>
            <AppBar className={classes.filterDialogAppBar}>
                <Toolbar>
                    <Box
                        width={1}>
                        <Box py={1} pl={1} alignItems={"center"}>
                            <Box center={true} onClick={onClose}>
                                <MtIcon icon={"mt-arrow-right"}/>
                                <Typography pl={1} variant={"body1"}>
                                    {title}
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                </Toolbar>
            </AppBar>
            <Toolbar/>
            <Box width={1}>
                {children}
            </Box>
        </Dialog>
    )
}
//endregion FilterDialog