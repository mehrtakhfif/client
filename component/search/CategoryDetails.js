import React from 'react'
import {DEBUG} from "../../pages/_app";
import {UtilsParser} from "../../utils/Utils";


const details = '** اگر گلدان درون تصویر موجود نباشد، از گلدان با کیفیت و قیمت مشابه و رنگ‌بندی متفاوت استفاده خواهد شد.**'

export default function CategoryDetails(){

    if (!DEBUG)
        return <React.Fragment/>

    return(
        <div>
            {UtilsParser.html(details)}
        </div>
    )
}