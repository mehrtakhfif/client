import React, {useCallback, useContext, useMemo} from "react";
import {colors, lang} from "../../../../repository";
import {Box, getSafe, HiddenLgUp, HiddenMdDown, Skeleton, Typography, UtilsStyle} from "material-ui-helper";
import {useRouter} from "next/router";
import _ from "lodash"
import {getFilterPrimaryKey} from "../SearchFilter";
import {filterItemName, filterItemsKey, onFilterChange} from "../../../../pages/search/[[...cat]]";
import Chip from "@material-ui/core/Chip";
import {Close} from "@material-ui/icons";
import {ThemeProvider} from "@material-ui/styles";
import {useTheme} from "@material-ui/core";
import createPalette from "@material-ui/core/styles/createPalette";
import rout from "../../../../router";
import FilterDataContext from "../../context/FilterDataContext";
import ParamsContext from "../../context/ParamsContext";
import useFilterQuery from "../../useFilterQuery";
import {UtilsFormat} from "../../../../utils/Utils";


const chipsStyle = {
    minHeight: "2.083vw",
    height: "auto",
    ...UtilsStyle.borderRadius("1.042vw")
}
const chipsStyleSm = {
    minHeight: "2.083vw",
    height: "auto",
    backgroundColor: "#F8F8F8",
    ...UtilsStyle.borderRadius(5)
}

function ActiveFilters() {
    const theme = useTheme();
    const router = useRouter();
    const {o, cat, ...query} = useFilterQuery()
    const [data,] = useContext(FilterDataContext);
    const {loading: baseLoadingState} = useContext(ParamsContext);
    const [__, onFilterItemChange] = baseLoadingState;

    const handleClickChip = useCallback((key, item, primaryKey) => {
        onFilterItemChange()
        onFilterChange(router, {filterKey: key, value: getSafe(() => item[primaryKey]), remove: true})
    }, [data])

    const handleRemoveAll = useCallback(() => {
        onFilterItemChange()
        setTimeout(() => {
            const r = rout.Product.Search.create({category: cat, q: query?.q})
            router.push(r[1])
        }, 300)
    }, [data])

    const deletable = useMemo(() => Object.keys(query).length > 1, [query]);

    return (
        !_.isEmpty(query) ?
            <Box alignCenter={true} pb={1}>
                <HiddenMdDown>
                    <Typography variant={"body2"} color={colors.textColor.h404040}>
                        {lang.get("active_filters")} :
                    </Typography>
                </HiddenMdDown>
                <Box
                    pr={1}
                    flexWrap={{
                        xs: "unset",
                        lg: "wrap"
                    }}>
                    {
                        Object.keys(query).map((k) => {
                            let items = _.isArray(query[k]) ? query[k] : [query[k]];
                            const primaryKey = getSafe(() => data[k] ? getFilterPrimaryKey(data[k]) : undefined)
                            const filterName = lang.get(filterItemName[k])
                            let canShowFilterName = true;
                            let canShowTitle = !_.isEmpty(data);

                            switch (k) {
                                case filterItemsKey.query: {
                                    canShowTitle = true
                                    break
                                }
                                case filterItemsKey.price: {
                                    canShowTitle = true
                                    canShowFilterName = false
                                    items = [lang.get("from_x_to_y_toman", {
                                        args: {
                                            x: UtilsFormat.numberToMoney(items[0]),
                                            y: UtilsFormat.numberToMoney(items[1])
                                        }
                                    })]
                                }
                            }

                            return (
                                <React.Fragment key={k}>
                                    {
                                        items.map((it, index) => {
                                            const item = getSafe(() => {
                                                return _.find(data[k], (i => {
                                                    return _.toString(i[primaryKey]) === it
                                                }))
                                            })

                                            const handleClick = () => {
                                                handleClickChip(k, item, primaryKey)
                                            }

                                            const title = canShowTitle ? getSafe(() => item?.name || item?.title || item || it, item || it) : false;


                                            return (
                                                <Box key={`${k}-${it}`} p={1}>
                                                    <HiddenLgUp>
                                                        <Chip
                                                            onClick={handleClick}
                                                            deleteIcon={
                                                                deletable ?
                                                                    <Close
                                                                        style={{
                                                                        color: colors.textColor.h757575
                                                                    }}/> : undefined}
                                                            label={(
                                                                <Box
                                                                    py={1}
                                                                    pr={deletable ? 2 : 0}
                                                                    flexDirectionColumn={true} >
                                                                    {
                                                                        canShowFilterName &&
                                                                        <Typography variant={"caption"}>
                                                                            {lang.get(filterName)}
                                                                        </Typography>
                                                                    }
                                                                    {
                                                                        title ?
                                                                            <Typography
                                                                                pr={1}
                                                                                variant={"subtitle1"}
                                                                                fontWeight={500}>
                                                                                {lang.get(title)}
                                                                            </Typography> :
                                                                            <Skeleton minWidth={30} height={10}/>
                                                                    }
                                                                </Box>
                                                            )}
                                                            onDelete={deletable ? handleClick : undefined}
                                                            style={chipsStyleSm}/>
                                                    </HiddenLgUp>
                                                    <HiddenMdDown>
                                                        <Chip
                                                            onClick={handleClick}
                                                            variant={"outlined"}
                                                            deleteIcon={
                                                                deletable ?
                                                                    <Close style={{
                                                                        color: colors.textColor.h757575
                                                                    }}/> : undefined}
                                                            label={(
                                                                <Typography pr={1} variant={"body2"}
                                                                            fontWeight={500}>
                                                                    {canShowFilterName ? `${lang.get(filterName)} ` : ""}
                                                                    {
                                                                        title ?
                                                                            lang.get(title) :
                                                                            <Skeleton minWidth={30} height={10}/>
                                                                    }
                                                                </Typography>
                                                            )}
                                                            onDelete={deletable ? handleClick : undefined}
                                                            style={chipsStyle}/>
                                                    </HiddenMdDown>
                                                </Box>
                                            )
                                        })
                                    }
                                </React.Fragment>
                            )
                        })
                    }
                    {
                        deletable&&
                        <HiddenMdDown>
                            <Box p={1}>
                                <ThemeProvider
                                    theme={{
                                        ...theme,
                                        palette: createPalette({
                                            primary: {
                                                light: colors.danger.light,
                                                main: colors.danger.light,
                                                dark: colors.danger.dark
                                            },
                                        }),
                                    }}>
                                    <Chip
                                        color={"primary"}
                                        variant="outlined"
                                        onClick={handleRemoveAll}
                                        label={(
                                            <Typography variant={"body2"} fontWeight={300} color={colors.danger.light}>
                                                {lang.get("delete_all")}
                                            </Typography>
                                        )}
                                        style={chipsStyle}/>
                                </ThemeProvider>
                            </Box>
                        </HiddenMdDown>
                    }
                </Box>
            </Box>
            :
            <React.Fragment/>
    )
}

export default React.memo(ActiveFilters)
