import makeStyles from "@material-ui/core/styles/makeStyles";
import React, {useCallback, useMemo, useState} from "react";
import {Box, getSafe, HiddenLgUp, HiddenMdDown, Typography} from "material-ui-helper";
import _ from "lodash";
import {lang} from "../../../../repository";
import SelectableItem from "./SelectableItem";
import clsx from "clsx";
import {Collapse} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import FilterFooterPanel from "../FilterFooterPanel";


const maxShowItem = 6;

 function SelectableFilterRenderItems({title, items, selectedItem,setSelectedItem, primaryKey, onClick}) {
    return (
        <React.Fragment>
            <HiddenMdDown>
                <ComponentLg
                    title={title}
                    items={items}
                    selectedItem={selectedItem}
                    primaryKey={primaryKey}
                    onClick={onClick}/>
            </HiddenMdDown>
            <HiddenLgUp>
                <ComponentSm
                    items={items}
                    selectedItem={selectedItem}
                    setSelectedItem={setSelectedItem}
                    primaryKey={primaryKey}
                    onClick={onClick}/>
            </HiddenLgUp>
        </React.Fragment>
    )
}

export default React.memo(SelectableFilterRenderItems)


const useStyles = makeStyles((theme) => ({
    selectableFilterContainerShowMore: {
        "& > li:last-child": {
            position: "relative",
            "&::after": {
                content: "''",
                width: "100%",
                height: "100%",
                background: "linear-gradient(360deg, rgb(255 255 255) 0%, rgba(255,255,255,1) 28%, rgba(255,255,255,0) 100%)",
                position: "absolute",
                bottom: 0,
            }
        }
    }
}));

function ComponentLg({title, items, selectedItem, primaryKey, onClick}) {

    const classes = useStyles()
    const [showAll, setShowAll] = useState(false)


    const renderItems = useMemo(() => {
        return getSafe(() => {
            if (items.length <= maxShowItem) {
                throw ""
            }
            return items.slice(0, maxShowItem - 1)
        }, items)
    }, [items])

    const renderItems2 = useMemo(() => {
        return getSafe(() => items.slice(maxShowItem - 1, items.length), [])
    }, [items])


    if (_.isEmpty(renderItems)) return (
        <Box flexDirection={'column'}>
            <Typography variant={"body1"} py={1}>
                {lang.get("there_are_no_results")}
            </Typography>
        </Box>
    )


    function render(renderItems) {
        return renderItems.map((it) => {
            const active = _.findIndex(_.isArray(selectedItem) ? selectedItem : [selectedItem], i => i === _.toString(it[primaryKey])) !== -1;
            return (
                <SelectableItem
                    key={it[primaryKey]}
                    item={it}
                    primaryKey={primaryKey}
                    active={active}
                    onClick={onClick}/>
            )
        })
    }

    const showMore = (!showAll && items.length >= maxShowItem)
    return (
        <Box flexDirection={'column'}>
            <Box className={clsx(showMore ? [classes.selectableFilterContainerShowMore] : [])}
                 component={"ul"}
                 flexDirection={'column'}>
                {render(renderItems)}
            </Box>
            <Collapse in={showAll}>
                <Box
                    component={"ul"}
                    flexDirection={'column'}>
                    {render(renderItems2)}
                </Box>
            </Collapse>
            {
                (showMore) &&
                <Typography
                    variant={"body2"}
                    fontWeight={500}
                    py={2}
                    onClick={() => {
                        setShowAll(true)
                    }}
                    style={{
                        cursor: "pointer"
                    }}>
                    + {lang.get("show_all") + ` ${title + "‌ها"}`} ({items.length - renderItems.length + 1} {lang.get("item")})
                </Typography>
            }
        </Box>
    )
}


function ComponentSm({items, selectedItem,setSelectedItem, primaryKey, onClick:onSubmit}) {

    const handleItemClick = useCallback((item,active)=>{
        setSelectedItem(getSafe(()=>{
            if (active)
                return getSafe(() => _.uniq(selectedItem.concat(_.toString(item[primaryKey]))), selectedItem)
            return getSafe(() => selectedItem.filter(i => i !== _.toString(item[primaryKey])), selectedItem)
        }))
    },[selectedItem])

    const handleSubmit = useCallback(()=>{
        onSubmit(true)
    },[selectedItem])

    const handleClear = useCallback(()=>{
        setSelectedItem([])
    },[setSelectedItem])


    return (
        <Box width={1} pb={2} flexDirectionColumn={true}>
            {
                items?.map((it, index) => {
                    const active = _.findIndex(_.isArray(selectedItem) ? selectedItem : [selectedItem], i => i === _.toString(it[primaryKey])) !== -1;
                    return (
                        <React.Fragment key={it[primaryKey]}>
                            {
                                index !== 0 &&
                                <Divider/>
                            }
                            <SelectableItem
                                key={it[primaryKey]}
                                item={it}
                                primaryKey={primaryKey}
                                active={active}
                                onClick={handleItemClick}/>
                        </React.Fragment>
                    )
                })
            }
           <FilterFooterPanel onSubmit={handleSubmit} onClear={handleClear}/>
        </Box>
    )
}

