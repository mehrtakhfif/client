import React, {useCallback, useEffect, useMemo, useState} from "react";
import {useRouter} from "next/router";
import {
    Box,
    ButtonBase,
    getSafe, gLog,
    HiddenLgUp,
    HiddenMdDown,
    tryIt,
    Typography,
    useOpenWithBrowserHistory,
} from "material-ui-helper";
import _ from "lodash"
import SelectableFilterRenderItems from "./SelectableFilterRenderItems";
import Container from "../base/Container";
import {getFilterPrimaryKey} from "../SearchFilter";
import {FilterDialog} from "../../smFilter/FilterSection";
import Divider from "@material-ui/core/Divider";
import MtIcon from "../../../MtIcon";
import {colors} from "../../../../repository";


export const FILTER_TYPE = "SELECTABLE_FILTER"


function SelectableFilter({
                              title,
                              temp,
                              filterKey,
                              items,
                              activeItems = [],
                              defaultExpanded = true,
                              searchPlaceHolder,
                              bottomDivider = true,
                              onFilterChange,
                              ...props
                          }) {


    return (
        !(!_.isArray(items) || _.isEmpty(items) || !filterKey) ?
            <SelectableFilterShow
                temp={temp}
                filterKey={filterKey}
                defaultExpanded={defaultExpanded}
                onFilterChange={onFilterChange}
                title={title}
                bottomDivider={bottomDivider}
                items={items}
                activeItems={activeItems}
                searchPlaceHolder={searchPlaceHolder}/> :
            <React.Fragment/>
    )
};

export default React.memo(SelectableFilter)

function SelectableFilterShow({
                                  temp,
                                  title,
                                  filterKey,
                                  items,
                                  activeItems = [],
                                  defaultExpanded = true,
                                  searchPlaceHolder,
                                  bottomDivider = true,
                                  onFilterChange
                              }) {
    const [open, __, onOpen, onClose] = useOpenWithBrowserHistory(filterKey + "SelectableFilterShow")

    //region variables
    const {query} = useRouter();
    const primaryKey = useMemo(() => {
        return getFilterPrimaryKey(items)
    }, [items])

    const [selectedItem, setSelectedItem] = useState(activeItems || []);

    useEffect(() => {
        setSelectedItem(activeItems)
    }, [activeItems])
    //endregion variables

    //region states
    const [searchText, setSearchText] = useState("");

    const renderItem = useMemo(() => {
        return tryIt(() => {
            if (searchText === ""){
                throw ""
            }
            return items.filter(it => {
                return getSafe(() => {
                    const name = it.name || it.title;
                    if (_.isString(name))
                        return name.search(searchText) !== -1
                    return name.fa.search(searchText) !== -1 || _.toLower(name.en).search(_.toLower(searchText)) !== -1
                })
            })
        }, items)
    }, [items, searchText]);
    //endregion states

    //region handler
    const handleItemChange = (it, active) => {
        onFilterChange({
            item: it,
            type: FILTER_TYPE,
            filterKey,
            primaryKey,
            active,
        });
    }

    const handleSubmit = useCallback(() => {
        handleClose(true)
    }, [temp,selectedItem, primaryKey, filterKey])

    const handleTextChange = useCallback((v) => {
        gLog("asfkjaksjfkasjkf textChange" ,v)
        setSearchText(v)
    }, [])


    const handleClose = useCallback((closeDialog=false) => {
        setTimeout(() => {
            tryIt(() => onClose())
            setTimeout(() => {
                onFilterChange({
                    item: selectedItem,
                    type: FILTER_TYPE,
                    filterKey,
                    primaryKey,
                    closeDialog: closeDialog
                });
            }, 300)
        }, 200)
    }, [temp,selectedItem, primaryKey, filterKey])
    //endregion handler


    return (
        <React.Fragment>
            <HiddenMdDown>
                <SelectableFilterBase
                    title={title}
                    itemsLength={items.length}
                    searchPlaceHolder={searchPlaceHolder}
                    activeItems={activeItems}
                    bottomDivider={bottomDivider}
                    defaultExpanded={defaultExpanded}
                    filterKey={filterKey}
                    primaryKey={primaryKey}
                    renderItem={renderItem}
                    handleItemChange={handleItemChange}
                    handleTextChange={handleTextChange}/>
            </HiddenMdDown>
            <HiddenLgUp>
                <Box width={1} flexDirectionColumn={true}>
                    <ButtonBase py={2.5} px={3} borderRadius={0} width={1} onClick={onOpen}>
                        <Box flexDirectionColumn={true}>
                            <Box alignItems={"center"}>
                                <Typography flex={1} fontWeight={400} variant={"subtitle1"}>
                                    {title}
                                </Typography>
                                <MtIcon icon={"mt-chevron-left"}/>
                            </Box>
                            {
                                (!_.isEmpty(selectedItem) && _.isArray(selectedItem) && !open) &&
                                <Typography pt={1} width={1} variant={"subtitle2"} color={colors.textColor.h757575}>
                                    {
                                        selectedItem?.map((it, index) => {
                                            const item = items.find(item => _.toString(item[primaryKey]) === it)
                                            if (!item)
                                                return <React.Fragment key={`ind-${index}`}/>

                                            return (
                                                <React.Fragment key={it}>
                                                    {
                                                        index !== 0 && "، "
                                                    }
                                                    {item?.name || item?.title}
                                                </React.Fragment>
                                            )

                                        })
                                    }
                                </Typography>
                            }
                        </Box>
                    </ButtonBase>
                    <Divider/>
                </Box>
                <FilterDialog open={open} title={title} onClose={handleClose}>
                    <SelectableFilterBase
                        selectedItem={selectedItem}
                        setSelectedItem={setSelectedItem}
                        title={title}
                        itemsLength={items.length}
                        searchPlaceHolder={searchPlaceHolder}
                        activeItems={activeItems}
                        bottomDivider={bottomDivider}
                        defaultExpanded={defaultExpanded}
                        filterKey={filterKey}
                        primaryKey={primaryKey}
                        renderItem={renderItem}
                        handleSubmit={handleSubmit}
                        handleTextChange={handleTextChange}/>
                </FilterDialog>
            </HiddenLgUp>
        </React.Fragment>
    )
}

function SelectableFilterBase({
                                  title,
                                  itemsLength,
                                  bottomDivider,
                                  filterKey,
                                  searchPlaceHolder,
                                  defaultExpanded,
                                  handleTextChange,
                                  renderItem,
                                  primaryKey,
                                  activeItems,
                                  selectedItem,
                                  setSelectedItem,
                                  handleItemChange,
                                  handleSubmit
                              }) {
    return (
        <React.Fragment>
            <HiddenMdDown>
                <Container
                    title={title}
                    bottomDivider={bottomDivider}
                    filterKey={filterKey}
                    showSearch={itemsLength > 6}
                    searchPlaceHolder={searchPlaceHolder}
                    defaultExpanded={defaultExpanded}
                    onSearchChange={handleTextChange}>
                    <SelectableFilterRenderItems
                        title={title}
                        items={renderItem}
                        primaryKey={primaryKey}
                        selectedItem={activeItems}
                        onClick={handleItemChange}/>
                </Container>
            </HiddenMdDown>
            <HiddenLgUp>
                <SelectableFilterRenderItems
                    title={title}
                    items={renderItem}
                    primaryKey={primaryKey}
                    selectedItem={selectedItem}
                    setSelectedItem={setSelectedItem}
                    onClick={handleSubmit}/>
            </HiddenLgUp>
        </React.Fragment>
    )
}

// export default React.memo(SelectableFilter)



