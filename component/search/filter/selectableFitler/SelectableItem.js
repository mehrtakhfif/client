import {useTheme} from "@material-ui/core";
import {Box, ButtonBase, HiddenLgUp, HiddenMdDown, Typography} from "material-ui-helper";
import Checkbox from "../base/Checkbox";
import {grey} from "@material-ui/core/colors";
import _ from "lodash";
import React, {useMemo} from "react";
import MtIcon from "../../../MtIcon";
import withStyles from "@material-ui/core/styles/withStyles";
import {colors} from "../../../../repository";


const variant = {
    xs: "subtitle1",
    lg: "body2"
};
const getFontWeight = (active) => active ? 500 : 400;


const CustomCheckbox = withStyles(theme => ({
    root: {
        '& svg': {
            color: colors.backgroundColor.ha9a9a9,
            fill: colors.backgroundColor.ha9a9a9
        },
        '&$checked svg': {
            color: theme.palette.secondary.main,
            fill: theme.palette.secondary.main
        },
    },
    checked: {},
}))((props) => <Checkbox color="default" {...props} />);

export default function SelectableItem({item: it, primaryKey, active, onClick, ...props}) {
    const theme = useTheme()
    const fontWeight = useMemo(() => getFontWeight(active), [active])

    const handleItemClick = (e) => {
        e.stopPropagation();
        onClick(it, !active)
    }

    return (
        <Box
            width={1}
            component={"li"}
            alignItems={"stretch"}>
            <ButtonBase width={1}
                        borderRadius={0}
                        onClick={handleItemClick}>
                <Box
                    width={1}
                    alignItems={"center"}
                    py={{
                        xs: 2.5,
                        lg: 0.625
                    }}
                    px={2}>
                    <HiddenMdDown>
                        <CustomCheckbox
                            checked={active}
                            onChange={handleItemClick}
                            inputProps={{'aria-label': `item-${it[primaryKey]}`}}
                            style={{
                                padding: 0,
                                margin: 0
                            }}/>
                    </HiddenMdDown>
                    <HiddenLgUp>
                        <MtIcon
                            color={active ? theme.palette.secondary.main : "transparent"}
                            icon={"mt-check-thin"}/>
                    </HiddenLgUp>
                    <Box
                        flex={1}
                        px={{
                            xs: 1,
                            lg: 2
                        }}
                        alignCenter={true}
                        textSelectable={false}>
                        {
                            it.color &&
                            <Box
                                component={"span"}
                                borderRadius={"100%"}
                                width={20} height={20} mr={1.5}
                                style={{
                                    border: `1px solid ${grey[400]}`,
                                    background: it.color
                                }}/>
                        }
                        {
                            !_.isObject(it.name) ?
                                <Typography variant={variant} fontWeight={fontWeight}>
                                    {it.name}
                                </Typography> :
                                <Box flex={1}>
                                    <Typography variant={variant} fontWeight={fontWeight}>
                                        {it.name.fa}
                                    </Typography>
                                    <Typography flex={1} justifyContent={'flex-end'}
                                                fontWeight={fontWeight}
                                                variant={variant}>
                                        {it.name.en}
                                    </Typography>
                                </Box>
                        }
                    </Box>
                </Box>
            </ButtonBase>
        </Box>
    )
}
