import React from "react";
import {Box, HiddenLgUp} from "material-ui-helper";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import {useTheme} from "@material-ui/core";
import MtIcon from "../../../MtIcon";


export default function SmHeader() {
    const theme = useTheme();


    return <React.Fragment/>
    return (
        <React.Fragment>
            <HiddenLgUp>
                <AppBar>
                    <Toolbar>
                        <Box
                            width={1}
                            style={{
                                color: "#000",
                                backgroundColor: "#fff"
                            }}>
                            <Box my={1.5}>
                                <Box py={1} px={1}>
                                    <MtIcon fontSize={"small"} icon={"mt-arrow-right"}/>
                                </Box>
                            </Box>
                        </Box>
                    </Toolbar>
                </AppBar>
                <Toolbar/>
            </HiddenLgUp>
        </React.Fragment>
    )
}
