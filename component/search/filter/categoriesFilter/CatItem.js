import React, {useState} from "react";
import {Box, tryIt, Typography} from "material-ui-helper";
import MtIcon from "../../../MtIcon";
import {useTheme} from "@material-ui/core";
import {onFilterChange} from "../../../../pages/search/[[...cat]]";
import {useRouter} from "next/router";
import useFilterQuery from "../../useFilterQuery";
import _ from "lodash";

export default function CatItem({level=1,defaultOpen = false, data}) {
    const theme = useTheme()
    const router = useRouter();
    const [open, setOpen] = useState(defaultOpen)
    const {cat} = useFilterQuery()

    if (!data)
        return <React.Fragment/>


    const {id, name, permalink, children} = data;

    function handleClick() {
        onFilterChange(router, {category: permalink})
    }


    return (
        <Box py={0.75} flexDirection={'column'}>
            <Typography
                variant={"subtitle2"}
                onClick={handleClick}
                fontWeight={tryIt(() => cat?.[0] === permalink,false)?400:300}
                style={{
                    cursor: 'pointer'
                }}>
                <Box pr={1}  alignItems={'center'}>
                    <MtIcon
                        fontSize={"small"}
                        icon={level===1?"mt-chevron-left":"mt-chevron-down"}
                        style={{
                            opacity: !_.isEmpty(children) ? 1 : 0
                        }}/>
                </Box>
                {name}
            </Typography>
            {
                children &&
                <Box pl={2} flexDirection={'column'}>
                    {
                        children?.map(it => (
                            <CatItem
                                level={level+1}
                                key={it?.id}
                                data={it}
                                defaultOpen={children?.length === 1}/>
                        ))
                    }
                </Box>
            }
        </Box>
    )
}