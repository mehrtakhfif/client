import React from "react";
import Container from "../base/Container";
import {lang} from "../../../../repository";
import {filterItemName, filterItemsKey} from "../../../../pages/search/[[...cat]]";
import {useRouter} from "next/router";
import {Box} from "material-ui-helper";
import CatItem from "./CatItem";
import _ from "lodash"

export default function CategoriesFilter({items,...props}) {
    const {query} = useRouter()
    const activeCat = query[filterItemsKey.cat]


    return (
        !_.isEmpty(items) ?
            <Container
                title={lang.get(filterItemName.cat)}
                defaultExpanded={true}>
                <Box width={1} flexDirection={'column'} pl={{xs: 1, md: 3}} pr={1} {...props}>
                    {
                        items?.map((cat, index) =>
                            <CatItem
                                level={1}
                                key={cat?.id || index}
                                data={cat}
                                defaultOpen={items?.length ===1}/>
                        )
                    }
                </Box>
            </Container> :
            <React.Fragment/>
    )
}


