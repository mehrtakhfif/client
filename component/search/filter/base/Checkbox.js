import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';


// Inspired by blueprintjs
export default function StyledCheckbox({...props}) {

    return (
        <Checkbox
            color="secondary"
            {...props}
        />
    );
}
