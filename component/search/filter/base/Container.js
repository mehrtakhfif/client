import React, {useCallback, useState} from "react";
import {Box, DefaultTextField, IconButton, tryIt, Typography} from "material-ui-helper";
import {Close, Search} from "@material-ui/icons";
import {colors, lang, theme} from "../../../../repository";
import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import _ from "lodash";
import MtIcon from "../../../MtIcon";

export default function Container({
                                      title,
                                      showSearch,
                                      defaultExpanded,
                                      searchPlaceHolder,
                                      onSearchChange,
                                      filterKey,
                                      bottomDivider = true,
                                      children,
                                      ...props
                                  }) {

    //region states
    const [expand, setExpand] = useState(defaultExpanded);
    const [searchActive, setSearchActive] = useState(false);
    //endregion states


    //region handler
    const handleExpandChange = useCallback((open) => {
        setExpand(_.isBoolean(open) ? open : !expand)
    }, [expand])

    const handleOpenSearch = useCallback(() => {
        if (!onSearchChange)
            return
        setExpand(true)
        setSearchActive(true)
    }, [onSearchChange])

    const handleCloseSearch = useCallback(() => {
        tryIt(() => onSearchChange(""))
        setSearchActive(false)
    }, [onSearchChange])

    const handleTextChange = useCallback((_, v) => {
        tryIt(() => onSearchChange(v))
    }, [onSearchChange])

    //endregion handler


    return (
        <Box width={1} pt={1} flexDirection={'column'} {...props}>
            <Box flex={1} width={1} minHeight={50}>
                {
                    (!searchActive) ?
                        <React.Fragment>
                            <Typography
                                flex={1}
                                textSelectable={false}
                                px={1}
                                alignItems={'center'}
                                onClick={handleExpandChange}
                                style={{
                                    cursor: "pointer"
                                }}>
                                <Box pr={1}>
                                    <MtIcon fontSize={"small"} icon={!expand ? "mt-chevron-down" : "mt-chevron-up"}/>
                                </Box>
                                {title}
                            </Typography>
                            {
                                (showSearch && onSearchChange) &&
                                <IconButton onClick={handleOpenSearch}>
                                    <Search/>
                                </IconButton>
                            }
                        </React.Fragment> :
                        <Box pt={1} width={1}>
                            <DefaultTextField
                                name={`search-${filterKey}`}
                                defaultValue={""}
                                variant={"outlined"}
                                placeholder={searchPlaceHolder ? searchPlaceHolder : `جست و جو در میان ${title}`}
                                onChangeDelay={500}
                                autoFocus={true}
                                onChange={handleTextChange}
                                endAdornment={(
                                    <IconButton
                                        title={lang.get("clear")}
                                        onClick={handleCloseSearch}>
                                        <Close/>
                                    </IconButton>
                                )}/>
                        </Box>
                }
            </Box>
            <Collapse in={expand}>
                {children}
            </Collapse>
            {
                bottomDivider &&
                <Divider
                    style={{
                        marginTop: expand ? theme.spacing(2.5) : undefined,
                        height: 2,
                        backgroundColor: colors.borderColor.heff0ef
                    }}/>
            }
        </Box>
    )
}
