import React from "react";
import {defaultOrderingItem, orderingItems} from "../../../pages/search/[[...cat]]";
import {Box, Button, getSafe, gLog, HiddenMdDown, Typography} from "material-ui-helper";
import {useRouter} from "next/router";
import {colors, lang} from "../../../repository";
import {useTheme} from "@material-ui/core";
import rout from "../../../router";


export default function Ordering() {
    const theme = useTheme()
    const router = useRouter();
    const {o, ...query} = router.query;
    const activeOrder = getSafe(() => o || defaultOrderingItem.key, defaultOrderingItem.key)


    function handleClick(k) {
        // gLog("askfjkasjkfjkasjkf",...rout.Product.Search.create({o: k,...query}))
        router.push(...rout.Product.Search.create({o: k, ...query}))
    }

    return (
        <HiddenMdDown>
            <Box pl={2} pt={3} pb={1}>
                <Typography alignItems={'center'} variant={"body2"} color={colors.textColor.h000000} fontWeight={400}
                            pr={2}>
                    {lang.get("ordering_by")}
                </Typography>
                {
                    Object.keys(orderingItems).map(k => {
                        const isActive = k === activeOrder
                        const item = orderingItems[k];
                        const {key, title} = item;

                        return (
                            <Box key={key} px={1}>
                                <Button
                                    onClick={() => handleClick(k)}
                                    color={isActive ? theme.palette.secondary.main : undefined}
                                    variant={isActive ? "contained" : "text"}
                                    typography={{
                                        color: isActive ? colors.textColor.hffffff : colors.textColor.h000000,
                                        variant: "body2"
                                    }}>
                                    {lang.get(title)}
                                </Button>
                            </Box>
                        )
                    })
                }
            </Box>
        </HiddenMdDown>
    )
}