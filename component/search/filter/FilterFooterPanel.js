import makeStyles from "@material-ui/core/styles/makeStyles";
import {useTheme} from "@material-ui/core";
import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import {Box, Button} from "material-ui-helper";
import {colors, lang} from "../../../repository";

const useFilterFooterPanelStyle = makeStyles((theme) => ({
    appBar: {
        top: 'auto',
        bottom: 0,
        "& > div": {
            backgroundColor: "#fff",
            color: "#000"
        }
    },
}));

export default function FilterFooterPanel({
                               submitText = "apply_filter",
                               cleanText="clean",
                               onSubmit,
                               onClear}) {
    const classes = useFilterFooterPanelStyle()
    const theme = useTheme()


    return(
        <React.Fragment>
            <Toolbar/>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <Box
                        width={1}>
                        <Box p={1.5} width={0.5}>
                            <Button
                                fullWidth={true}
                                color={theme.palette.primary.main}
                                center={true}
                                onClick={onSubmit}
                                typography={{
                                    variant: "subtitle1",
                                    fontWeight: 500,
                                    color: "#fff"
                                }}>
                                {lang.get(submitText)}
                            </Button>
                        </Box>
                        <Box p={1.5} width={0.5}>
                            <Button
                                fullWidth={true}
                                variant={"text"}
                                center={true}
                                onClick={onClear}
                                typography={{
                                    variant: "subtitle1",
                                    fontWeight: 500,
                                    color: colors.textColor.h404040
                                }}>
                                {lang.get(cleanText)}
                            </Button>
                        </Box>
                    </Box>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    )
}
