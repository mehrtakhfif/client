import React, {useCallback, useState} from "react";
import {
    Box,
    Button,
    ButtonBase,
    getSafe,
    HiddenLgUp,
    HiddenMdDown,
    tryIt,
    Typography,
    useOpenWithBrowserHistory
} from "material-ui-helper";
import Container from "../base/Container";
import Slider from "@material-ui/core/Slider";
import {UtilsFormat} from "../../../../utils/Utils";
import {colors, lang, theme} from "../../../../repository";
import Divider from "@material-ui/core/Divider";
import {FILTER_TYPE} from "../selectableFitler/SelectableFilter";
import MtIcon from "../../../MtIcon";
import {FilterDialog} from "../../smFilter/FilterSection";

function PriceRange({filterKey, temp, title, activeItems, priceRange, defaultExpanded, onFilterChange}) {

    return (
        priceRange?.max_price ?
            <PriceRangeShow
                temp={temp}
                priceRange={priceRange}
                title={title}
                defaultExpanded={defaultExpanded}
                activeItems={activeItems}
                filterKey={filterKey}
                onFilterChange={onFilterChange}/> :
            <React.Fragment/>
    )
}

export default React.memo(PriceRange)


function PriceRangeShow({filterKey, temp, title, priceRange, activeItems, defaultExpanded, onFilterChange}) {
    const {min_price, max_price} = priceRange;

    const [open, __, onOpen, onClose] = useOpenWithBrowserHistory("PriceRangeShow")
    const [value, setValue] = useState([Number(min_price), Number(max_price)])

    const handleChange = useCallback((event, newValue) => {
        try {
            const round = (value) => Math.round(value / 500) * 500
            setValue([round(newValue[0]), round(newValue[1])])
        } catch {
        }
    }, [setValue])

    const handleTextChange = useCallback((e, v) => {
        setValue([v, value[0]])
    }, [value])

    function onSubmitClick(isMobile) {
        onFilterChange({
            temp: isMobile ? temp : undefined,

            item: value,
            type: FILTER_TYPE,
            filterKey
        });
    }

    const handleClose = useCallback((closeDialog = false) => {
        setTimeout(() => {
            tryIt(() => onClose())
            setTimeout(() => {
                // onFilterChange({});
            }, 300)
        }, 200)
    }, [temp, filterKey])

    const {price} = temp || {}

    return (
        <React.Fragment>
            <HiddenMdDown>
                <Container filterKey={filterKey} title={title} defaultExpanded={defaultExpanded}>
                    <Cm
                        priceRange={priceRange}
                        value={value}
                        handleChange={handleChange}
                        onSubmitClick={onSubmitClick}/>
                </Container>
            </HiddenMdDown>
            <HiddenLgUp>
                <Box width={1} flexDirectionColumn={true}>
                    <ButtonBase py={2.5} px={3} borderRadius={0} width={1} onClick={onOpen}>
                        <Box flexDirectionColumn={true}>
                            <Box alignItems={"center"}>
                                <Typography flex={1} fontWeight={400} variant={"subtitle1"}>
                                    {title}
                                </Typography>
                                <MtIcon icon={"mt-chevron-left"}/>
                            </Box>
                            <Typography pt={1} width={1} variant={"subtitle2"} color={colors.textColor.h757575}>
                                {
                                    price&&
                                    lang.get("from_x_to_y_toman", {
                                        args: {
                                            x: price?.[0],
                                            y: price?.[1]
                                        }
                                    })
                                }
                            </Typography>
                        </Box>
                    </ButtonBase>
                    <Divider/>
                </Box>
                <FilterDialog open={open} title={title} onClose={handleClose}>
                    <Cm
                        pt={2}
                        priceRange={priceRange}
                        value={value}
                        handleChange={handleChange}
                        onSubmitClick={()=> {
                            onSubmitClick(true)
                            handleClose()
                        }}/>
                </FilterDialog>
            </HiddenLgUp>
        </React.Fragment>
    )
}


function Cm({value, priceRange, onSubmitClick, handleChange, ...props}) {
    const {min_price, max_price} = priceRange;

    const buttonDisable = getSafe(() => min_price === value[0] && max_price === value[1])


    return (
        <Box width={1} flexDirection={'column'} pl={{xs: 1, md: 3}} pr={1} {...props}>
            <Box width={1} dir={'ltr'}>
                <Slider
                    value={value}
                    onChange={handleChange}
                    valueLabelDisplay="off"
                    aria-labelledby="price-rang"
                    min={min_price}
                    max={max_price}
                    color={"secondary"}/>
            </Box>
            <Box pt={2}>
                <Box width={0.5} pr={1} center={true}>
                    <Typography pr={1} variant={"body1"}>
                        از
                    </Typography>
                    <PriceShow
                        name={"minPrice"}
                        value={value[0]}
                        onChange={(e, v) => {
                            setValue([v, value[1]])
                        }}/>
                </Box>
                <Divider orientation="vertical" flexItem/>
                <Box width={0.5} pr={1} center={true}>
                    <Typography pr={1} variant={"body1"}>
                        تا
                    </Typography>
                    <PriceShow
                        name={"maxPrice"}
                        value={value[1]}
                        onChange={(e, v) => {
                            setValue([value[0], v])
                        }}/>
                </Box>
            </Box>
            <Box pt={3} center={true}>
                <Button
                    variant={"outlined"}
                    color={theme.palette.secondary.main}
                    disabled={buttonDisable}
                    typography={{
                        variant: {
                            xs: "body1",
                            md: "body2"
                        },
                        fontWeight: "normal",
                        color: buttonDisable ? colors.textColor.ha9a9a9 : colors.textColor.h000000
                    }}
                    onClick={onSubmitClick}>
                    {lang.get("apply_price_range")}
                </Button>
            </Box>
        </Box>
    )
}

function PriceShow({value, onChange}) {

    return (
        <Typography variant={{
            xs: "h6",
            md: "subtitle1"
        }}>
            {UtilsFormat.numberToMoney(value)}
        </Typography>
    )
}
