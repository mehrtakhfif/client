import React, {useMemo, useState} from "react";
import {useRouter} from "next/router";
import {Box, IconButton, tryIt, Typography} from "material-ui-helper";
import {ExpandLess, ExpandMore, Search} from "@material-ui/icons";
import _ from "lodash"
import Divider from "@material-ui/core/Divider";
import Checkbox from "./base/Checkbox";
import Collapse from "@material-ui/core/Collapse";
import {useTheme} from "@material-ui/core";

function SelectableFilter({
                              title,
                              filterKey,
                              items,
                              activeItems = [],
                              borderButton,
                              defaultExpanded = true,
                              searchPlaceHolder = "جست‌وجو درمیان گزینها",
                              primaryKey = "id",
                              searchKey,
                              render,
                              onClick,
                              onFilterChange,
                              ...props
                          }) {

    //region variables
    const {query} = useRouter();
    const selected = useMemo(() => {
        return tryIt(() => {
            return _.uniq((query[filterKey] || []).concat(activeItems));
        }, query[filterKey])
    }, [query, activeItems])
    //endregion variables

    //region states
    const [expand, setExpand] = useState(defaultExpanded);
    const [searchActive, setSearchActive] = useState(false);
    const [searchText, setSearchText] = useState("");
    const renderItem = useMemo(() => {
        return tryIt(() => {
            if (searchText === "")
                throw ""
            return items.filter(it => {
                return it.search(searchText) !== -1
            })
        }, items)
    }, [items, searchText]);
    //endregion states


    //region handler
    const handleExpandChange = (open) => {
        setExpand(_.isBoolean(open) ? open : !expand)
    }

    const handleSearchClick = (active) => {
        setSearchActive(_.isBoolean(active) ? active : !searchActive)
    }

    const handleItemChange = (it, active) => {
        onFilterChange({
            item: it,
            filterKey,
            primaryKey,
            active
        });
    }
    //endregion handler

    return (
        !(!_.isArray(renderItem) || _.isEmpty(renderItem) || !filterKey || !primaryKey) ?
            <Box width={1} flexDirection={'column'}>
                <Box flex={1} width={1}>
                    {
                        !searchActive ?
                            <React.Fragment>
                                <Typography
                                    flex={1}
                                    textSelectable={false}
                                    px={1}
                                    alignItems={'center'}
                                    onClick={handleExpandChange}
                                    style={{
                                        cursor: "pointer"
                                    }}>
                                    {
                                        !expand ?
                                            <ExpandMore/> :
                                            <ExpandLess/>
                                    }
                                    {title}
                                </Typography>
                                {
                                    searchKey &&
                                    <IconButton onClick={handleSearchClick}>
                                        <Search/>
                                    </IconButton>
                                }
                            </React.Fragment> :
                            <Box onClick={handleSearchClick}>
                                close
                            </Box>
                    }
                </Box>
                <Collapse in={expand}>
                    <Box flexDirection={'column'}>
                        {
                            renderItem.map((it, index) => {
                                const active = _.findIndex(selected, i => i === _.toString(it[primaryKey])) !== -1;
                                const onClick = (e) => {
                                    e.stopPropagation();
                                    handleItemChange(it, !active);
                                }
                                return (
                                    <Item
                                        item={it}
                                        primaryKey={primaryKey}
                                        active={active}
                                        onClick={onClick}/>
                                )
                            })
                        }
                    </Box>
                </Collapse>
                <Divider style={{height: 2}}/>
            </Box> :
            <React.Fragment/>
    )
};

// export default React.memo(SelectableFilter)
export default SelectableFilter


function Item({item: it, primaryKey, active, onClick, ...props}) {
    const theme = useTheme()

    return (
        <Box
            alignItems={"stretch"}
            width={'max-content'}
            onClick={onClick}
            style={{
                cursor: 'pointer'
            }}>
            <Checkbox
                checked={active}
                onChange={onClick}
                inputProps={{'aria-label': `item-${it[primaryKey]}`}}
                style={{
                    margin: theme.spacing(0, 2)
                }}
            />
            <Box
                px={1}
                alignCenter={true}
                textSelectable={false}>
                <Box
                    component={"span"}
                    borderRadius={"100%"}
                    width={20} height={20} mx={1}
                    style={{backgroundColor: it.color}}/>
                {
                    !_.isObject(it.title) ?
                        <Typography variant={"body2"}>
                            {it.title}
                        </Typography> :
                        <Box>
                            <Typography variant={"body2"}>
                                {it.title.fa}
                            </Typography>
                            <Typography flex={1} justifyContent={'flex-end'}
                                        variant={"body2"}>
                                {it.title.en}
                            </Typography>
                        </Box>
                }
            </Box>
        </Box>
    )
}

