import React, {useCallback, useContext, useEffect, useState} from "react";
import {useRouter} from "next/router";
import SelectableFilter, {FILTER_TYPE as SELECTABLE_FILTER_TYPE} from "./selectableFitler/SelectableFilter";
import {filterItemName, filterItemsKey, onFilterChange} from "../../../pages/search/[[...cat]]";
import {Box, getSafe, HiddenLgUp, HiddenMdDown, tryIt, useEffectWithoutInit} from "material-ui-helper";
import _ from "lodash"
import {lang} from "../../../repository";
import FilterFooterPanel from "./FilterFooterPanel";
import useFilterQuery from "../useFilterQuery";
import Backdrop from "../../base/Backdrop";
import StickyBox from "react-sticky-box";
import {headerOffset} from "../../header/HeaderLg";
import FilterDataContext from "../context/FilterDataContext";
import ParamsContext from "../context/ParamsContext";
import PriceRange from "./priceRange/PriceRange";
import CategoriesFilter from "./categoriesFilter/CategoriesFilter";


let backupData = undefined
export const getFilterPrimaryKey = (items) => {
    return getSafe(() => items[0].permalink ? "permalink" : "id", "id")
}

function SearchFilter({
                          temp,
                          setTemp,
                          mobileFilterOpen = false,
                          onFilterDialogOpen,
                          onFilterDialogClose,
                          onCloseDialogSm
                      }) {
    const router = useRouter();
    const query = useFilterQuery()
    const [dd,dataError] = useContext(FilterDataContext)
    const {loading:baseLoadingState} = useContext(ParamsContext)
    const [__,onFilterItemChange] = baseLoadingState;

    const [loading, setLoading] = useState(false)

    //region useEffect

    useEffect(() => {
        if (!_.isEmpty(dd)) {
            backupData = dd
        }
    }, [dd])

    //region handleFilterChange

    const handleFilterChange = useCallback(({
                                                filterKey,
                                                primaryKey,
                                                item,
                                                type,
                                                active=true,
                                                isTemp = false,
                                                closeDialog = false
                                            }) => {

        // gLog("askflkaslkflkaslfklak first",{
        //     filterKey,
        //     primaryKey,
        //     item,
        //     type,
        //     active,
        //     isTemp,
        //     isObj: _.isArray(item),
        //     closeDialog})
        // return;
        //
        //
        // if (filterKey === filterItemsKey.pricing_range){
        //
        // }




        const val = getSafe(() => _.toString(item[primaryKey]))
        if (isTemp || mobileFilterOpen) {
            const close = _.isBoolean(closeDialog) && closeDialog
            if (close)
                setLoading(true)
            const newTemp = getSafe(() => {
                if (!item)
                    throw ""
                const newTemp = _.cloneDeep(temp);
                if (_.isArray(item)) {
                    if (type === SELECTABLE_FILTER_TYPE) {
                        newTemp[filterKey] = item
                    }
                    return newTemp
                }
                newTemp[filterKey] = tryIt(() => {
                    if (type === SELECTABLE_FILTER_TYPE) {
                        if (active)
                            return getSafe(() => _.uniq(newTemp[filterKey].concat([_.toString(val)])), () => [_.toString(val)])
                        return getSafe(() => newTemp[filterKey].filter(i => i !== _.toString(val)), [])
                    }
                })
            }, temp)
            if (newTemp !== temp) {
                setTemp(newTemp, (temp) => {
                    if (close) {
                        tryIt(() => onCloseDialogSm(temp))
                        setLoading(false)
                    }
                })
                return;
            }
            tryIt(() => onCloseDialogSm())
            setLoading(false)
            return
        }
        onFilterItemChange()
        onFilterChange(router, {filterKey: filterKey, value: _.isArray(item)?item:item[primaryKey], remove: !active})
    }, [query,mobileFilterOpen, temp, setTemp])
    //endregion handleFilterChange

    useEffectWithoutInit(() => {
        initTemp()
    }, [mobileFilterOpen])

    //endregion useEffect


    const initTemp = useCallback(() => {
        try {
            if (!mobileFilterOpen && !_.isEqual(temp, query)) {
                onFilterItemChange()
                onFilterChange(router, {}, _.cloneDeep(temp))
            }
            setTemp(mobileFilterOpen ? query : {})
        } catch (e) {
        }
    }, [query, mobileFilterOpen, onFilterChange, setTemp])


    function handleSubmit() {
        handleFilterChange({closeDialog: true})
    }

    function handleClear() {
        setTemp({})
    }

    const data = dd ? dd : _.isObject(dd) ? {} : backupData

    if (!data && _.isObject(data) && !dataError)
        return <React.Fragment/>

    return (
        <React.Fragment>
            <HiddenMdDown>
                <Box pr={4} width={2.5 / 12} flexDirection={'column'}>
                    <StickyBox offsetTop={headerOffset} offsetBottom={10}>
                        <SearchFilterBase
                            data={data}
                            temp={temp}
                            mobileFilterOpen={mobileFilterOpen}
                            handleFilterChange={handleFilterChange}/>
                    </StickyBox>
                </Box>
            </HiddenMdDown>
            <HiddenLgUp>
                <Box width={1} flexDirectionColumn={true}>
                    <SearchFilterBase
                        data={data}
                        temp={temp}
                        mobileFilterOpen={mobileFilterOpen}
                        onFilterDialogOpen={onFilterDialogOpen}
                        onFilterDialogClose={onFilterDialogClose}
                        handleFilterChange={handleFilterChange}/>
                    <FilterFooterPanel
                        submitText={"apply_filters"}
                        cleanText={"clean_all"}
                        onClear={handleClear}
                        onSubmit={handleSubmit}/>
                    <Backdrop transparent={true} zIndex={99999999} open={loading} timeout={2000}
                              onTimedOut={() => setLoading(false)}/>
                </Box>
            </HiddenLgUp>
        </React.Fragment>
    )
}

function SearchFilterBase({data, mobileFilterOpen, temp, handleFilterChange}) {
    const router = useRouter();

    return (
        <React.Fragment>
            <CategoriesFilter items={data?.categories}/>
            <SelectableFilter
                pt={data?.categories ? 0 : 2}
                temp={temp}
                title={lang.get(filterItemName.colors)}
                filterKey={filterItemsKey.colors}
                searchKey={"title"}
                defaultExpanded={false}
                activeItems={mobileFilterOpen ? temp[filterItemsKey.colors] : router.query[filterItemsKey.colors]}
                onFilterChange={handleFilterChange}
                items={data?.colors}/>
            <SelectableFilter
                temp={temp}
                title={lang.get(filterItemName.brands)}
                filterKey={filterItemsKey.brands}
                defaultExpanded={false}
                activeItems={mobileFilterOpen ? temp[filterItemsKey.brands] : router.query[filterItemsKey.brands]}
                onFilterChange={handleFilterChange}
                items={data?.brands}/>
            <PriceRange
                temp={temp}
                title={lang.get("pricing_range")}
                filterKey={filterItemsKey.price}
                activeItems={mobileFilterOpen ? temp[filterItemsKey.price] : router.query[filterItemsKey.price]}
                defaultExpanded={true}
                bottomDivider={false}
                priceRange={{
                    min_price:data?.min_price,
                    max_price:data?.max_price
                }}
                onFilterChange={handleFilterChange}/>
        </React.Fragment>
    )
}

export default React.memo(SearchFilter)

