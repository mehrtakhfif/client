import React, {useMemo} from "react";
import _ from "lodash"
import {useRouter} from "next/router";
import {tryIt} from "material-ui-helper";
import {filterItemsKey} from "../../pages/search/[[...cat]]";

export default function useFilterQuery() {
    const {query: qu} = useRouter()

    return useMemo(() => {
        return tryIt(() => {
            const newQ = _.cloneDeep(qu);
            _.forEach(newQ, (v, k) => {
                if (!_.isString(v))
                    return
                switch (k) {
                    case filterItemsKey.query:
                    case filterItemsKey.ordering: {
                        return;
                    }
                }
                newQ[k] = [v]
            })
            return newQ
        }, {})
    }, [qu])
}
