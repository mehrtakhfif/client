import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {Box, DefaultTextField, getSafe, Random} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import {debounce} from "lodash";
import Backdrop from "@material-ui/core/Backdrop";
import makeStyles from "@material-ui/core/styles/makeStyles";
import SearchBox from "./SearchBox";
import useSWR from "swr"
import {DEBUG} from "../../pages/_app";
import ControllerProduct from "../../controller/ControllerProduct";
import {lang} from "../../repository";

export const searchHeight = 80;

const useStyles = makeStyles((theme) => ({
    searchContainer: {
        position: "relative",
        zIndex: theme.zIndex.appBar,
        backgroundColor: "#fff",
        "&>div:first-child": {
            zIndex: 2
        }
    },
    backdrop: {
        zIndex: theme.zIndex.appBar - 2,
        color: '#fff',
    },
}));


const DEBUG_SEARCH = DEBUG && true
export default function Search({...props}) {
    //region props
    const theme = useTheme();
    const ref = useRef()
    const classes = useStyles()
    const [value, setValue] = useState(DEBUG_SEARCH ? "دارچین" : "")
    const [searchValue, setSearchValue] = useState(DEBUG_SEARCH ? "دارچین" : "")
    const [focused, setFocused] = useState(false)
    const [init, setInit] = useState(false)
    const [backdrop, setBackdrop] = useState(false)
    const [backupData, setBackupData] = useState({})

    const d = ControllerProduct.Query.V2.search({query: searchValue});
    const {data: dd, error} = useSWR(d[0], d[1], {
        revalidateOnFocus: false,
        dedupingInterval:480000,
        onSuccess: (res) => {
            setBackupData(res)
        }
    })
    const data = useMemo(() => {
        return getSafe(() => dd.data, backupData?.data)
    }, [dd])
    //endregion props

    const delayedSearchValue = useCallback(debounce(() => setSearchValue(value), 300), [value]);
    const handleSearchTextFieldChange = useCallback((e, v) => {
        setValue(v)
    }, [])

    const handleFocusIn = useCallback(() => {
        if (!init)
            return
        setFocused(true)
    }, [init])
    const handleFocusOut = useCallback(() => {
        return
        setFocused(false)
    }, [])

    //region init
    useEffect(() => {
        setInit(true)
    }, [])
    useEffect(() => {
        delayedSearchValue()
        return delayedSearchValue.cancel;
    }, [value, delayedSearchValue])
    //endregion init

    const active = value.length > 2 && focused;
    const itemsBox = focused;
    const activeBackDrop = focused || backdrop;


    return (
        <React.Fragment>
            <Box
                width={1} p={2}
                className={classes.searchContainer}
                flexDirectionColumn={true}
                center={true}
                {...props}>
                <DefaultTextField
                    value={value}
                    variant={"outlined"}
                    placeholder={lang.get("search")}
                    name={"search"}
                    autoComplete={"off"}
                    autoFocus={false}
                    onFocusInDelay={0}
                    onFocusOutDelay={0}
                    onFocusIn={handleFocusIn}
                    onFocusOut={handleFocusOut}
                    onChangeTextField={handleSearchTextFieldChange}/>
                <SearchBox
                    open={itemsBox}
                    data={data}/>
            </Box>
            <Backdrop
                className={classes.backdrop}
                open={activeBackDrop}
                onClick={() => {
                    setBackdrop(false)
                }}/>
        </React.Fragment>
    )
}

const SearchData = () => {


    const temp1 = {
        "products": [
            {
                "name": "اسباب بازی فریز بی",
                "permalink": "اسباب-بازی-فریز-بی",
                "thumbnail": "http://api.mt.com/media/boxes/6/2020-05-15/thumbnail/14-44-05-50-has-ph.jpg"
            },
            {
                "name": "اسباب بازی دندانی هپی",
                "permalink": "اسباب-بازی-دندانی-هپی",
                "thumbnail": "http://api.mt.com/media/boxes/6/2020-05-15/thumbnail/11-56-32-28-has-ph.jpg"
            },
            {
                "name": "اسباب بازی دمبل چرخنده",
                "permalink": "اسباب-بازی-دمبل-چرخنده-سگ",
                "thumbnail": "http://api.mt.com/media/boxes/6/2020-05-07/thumbnail/17-35-46-88-has-ph.jpg"
            }
        ],
        "categories": [
            {
                "name": "اسباب بازی و اکسسوری",
                "permalink": "اسباب-بازی-و-اکسسوری-پرنده",
                "media": "http://api.mt.com/media/boxes/6/2020-07-06/category/07-55-33-16.jpg",
                "parent": "پرنده"
            },
            {
                "name": "اسباب بازی و اکسسوری",
                "permalink": "اسباب-بازی-و-اکسسوری-گربه",
                "media": "http://api.mt.com/media/avatar/%D8%A7%D8%B3%D8%A8%D8%A7%D8%A8_%D8%A8%D8%A7%D8%B2%DB%8C.jpg",
                "parent": "گربه"
            },
            {
                "name": "اسباب بازی و اکسسوری",
                "permalink": "اسباب-بازی-و-اکسسوری",
                "media": "http://api.mt.com/media/avatar/%D8%A7%D8%A7%D8%B3%D8%A8%D8%A7%D8%A8-%D8%A8%D8%A7%D8%B2%DB%8C-%D8%B3%DA%AF-%D9%87%D8%A7.jpg",
                "parent": "سگ"
            }
        ],
        "tags": [
            {
                "name": "دم اسب",
                "permalink": "دم-اسب"
            },
            {
                "name": "اسباب بازی",
                "permalink": "اسباب-بازی"
            },
            {
                "name": "قیماق اسباب",
                "permalink": "قیماق-اسباب"
            }
        ]
    }
    const temp2 = {
        "products": [
            {
                "name": "رز جاودان سبزMG12",
                "permalink": "رز-جاودان-سبز",
                "thumbnail": "http://api.mt.com/media/boxes/2/2020-06-23/thumbnail/11-10-38-37-has-ph.jpg"
            },
            {
                "name": "رز جاودان قرمزMG02",
                "permalink": "رز-جاودان-قرمز",
                "thumbnail": "http://api.mt.com/media/boxes/2/2020-06-23/thumbnail/10-43-40-61-has-ph.jpg"
            },
            {
                "name": "رز جاودان مشکیMG03",
                "permalink": "رز-جاودان-مشکی",
                "thumbnail": "http://api.mt.com/media/boxes/2/2020-06-23/thumbnail/10-46-05-80-has-ph.jpg"
            }
        ],
        "categories": [
            {
                "name": "رز جاودان",
                "permalink": "رز-جاودان",
                "media": "http://api.mt.com/media/boxes/2/2020-06-26/category/15-57-08-88.jpg",
                "parent": null
            },
            {
                "name": "گلدانی",
                "permalink": "گلدانی",
                "media": "http://api.mt.com/media/avatar/20-Best-Spring-Flower-Arrangements-Centerpieces-Decoration-Ideas.jpg",
                "parent": null
            },
            {
                "name": "دانه‌های خوراکی",
                "permalink": "دانه-خوراکی",
                "media": "http://api.mt.com/media/boxes/11/2020-06-27/category/08-49-12-98.jpg",
                "parent": null
            }
        ],
        "tags": [
            {
                "name": "جاودان",
                "permalink": "جاودان"
            },
            {
                "name": "رز جاودان",
                "permalink": "رز-جاودان"
            },
            {
                "name": "رز جاودان مینیاتوری",
                "permalink": "رز-جاودان-مینیاتوری"
            }
        ]
    }
    const temp3 = {
        "products": [
            {
                "name": "ادویه کاری فاطیما مقدار 100 گرم",
                "permalink": "ادویه-کاری-فاطیما-مقدار-100-گرم528",
                "thumbnail": "http://api.mt.com/media/boxes/10/2020-06-22/thumbnail/11-47-13-30-has-ph.jpg"
            }
        ],
        "categories": [
            {
                "name": "ادویه جات",
                "permalink": "ادویه",
                "media": "http://api.mt.com/media/boxes/11/2020-06-27/category/08-53-35-60.jpg",
                "parent": null
            },
            {
                "name": "ادویه جات",
                "permalink": "ادویه-جات2",
                "media": "http://api.mt.com/media/boxes/10/2020-07-03/category/09-00-34-22.jpg",
                "parent": "چاشنی ها و مواد افزودنی"
            }
        ],
        "tags": [
            {
                "name": "ادویه",
                "permalink": "ادویه"
            },
            {
                "name": "ادویه خاص",
                "permalink": "ادویه-خاص"
            },
            {
                "name": "ادویه جات",
                "permalink": "ادویه-جات"
            }
        ]
    }
    const temp4 = {
        "products": [
            {
                "name": "زردچوبه قلم",
                "permalink": "زردچوبه-قلم",
                "thumbnail": "http://api.mt.com/media/boxes/11/2020-07-30/thumbnail/10-13-48-74-has-ph.jpg"
            },
            {
                "name": "پودر زردچوبه",
                "permalink": "پودر-زردچوبه",
                "thumbnail": "http://api.mt.com/media/boxes/11/2020-07-30/thumbnail/10-03-21-55-has-ph.jpg"
            },
            {
                "name": "زردچوبه فاطیما مقدار 75 گرم",
                "permalink": "زردچوبه-فاطیما-مقدار-75-گرم570",
                "thumbnail": "http://api.mt.com/media/boxes/10/2020-06-22/thumbnail/12-04-29-90-has-ph.jpg"
            }
        ],
        "categories": [],
        "tags": [
            {
                "name": "زردچوبه",
                "permalink": "زردچوبه"
            },
            {
                "name": "زردچوبه قلم",
                "permalink": "زردچوبه-قلم"
            },
            {
                "name": "زردچوبه خالص",
                "permalink": "زردچوبه-خالص"
            }
        ]
    }
    const tempList = [temp1, temp2, temp3, temp4]
    return tempList[Random.randomInteger(0, tempList.length - 1)]
}
