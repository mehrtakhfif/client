import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {clearTextFieldValue, DefaultTextField, FormController, getSafe, IconButton, tryIt} from "material-ui-helper";
import {createTheme, ThemeProvider, useTheme} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import SearchBox, {getListElements, handleActiveItemChange} from "./SearchBox";
import useSWR from "swr"
import ControllerProduct from "../../controller/ControllerProduct";
import {colors, lang} from "../../repository";
import {useRouter} from "next/router";
import rout from "../../router";
import {ArrowForward, Close} from "@material-ui/icons";
import {debounce} from "lodash";
import {SmHidden, SmShow} from "../header/Header";
import MtIcon from "../MtIcon";
import {UtilsStyle} from "../../utils/Utils";
import {useBackdropContext} from "../../context/BackdropContext";

const useStyles = makeStyles((theme) => ({
    searchContainer: {
        position: "relative",
        zIndex: theme.zIndex.appBar + 2,
        backgroundColor: "#fff",
        "&>div:first-child": {
            zIndex: 2
        }
    },
    searchInput: {
        background: colors.backgroundColor.hf8f8f8,
        ...UtilsStyle.transition(),
        "& fieldset": {
            ...UtilsStyle.transition(),
            borderColor: `${colors.backgroundColor.hf8f8f8} !important`
        },
        "&>div.Mui-focused": {
            background: "#fff",
            "& fieldset": {
                borderColor: `${colors.borderColor.heff0ef} !important`
            }
        }
    },
    backdrop: {
        zIndex: theme.zIndex.appBar - 2,
        color: '#fff',
    },
}));


export default function Search({open, isMobile = false, onChange, onClose, ...props}) {

    //region props
    const theme = useTheme();
    const router = useRouter();
    const [showBackdrop, setShowBackdrop] = useBackdropContext()

    const ref = useRef()
    const classes = useStyles()
    const [searchValue, setSearchValue] = useState("")
    const [focused, setFocused] = useState(false)
    const [init, setInit] = useState(false)
    const [backdrop, setBackdrop] = useState(false)
    const [backupData, setBackupData] = useState({})


    const d = ControllerProduct.Query.V2.search({query: searchValue});
    const {data: dd, error} = useSWR(d[0], d[1], {
        revalidateOnFocus: false,
        dedupingInterval: 480000,
        onSuccess: (res) => {
            setBackupData(res)
        }
    })


    const data = useMemo(() => {
        return getSafe(() => dd.data, backupData?.data)
    }, [dd])
    const dataLength = useMemo(() => getSafe(() => dd.dataLength, 0), [dd])
    //endregion props


    useEffect(() => {
        if (isMobile)
            return
        setShowBackdrop(focused)
    }, [focused])


    const handleFocusIn = useCallback(() => {
        if (!init)
            return
        tryIt(() => onChange(true))
        setFocused(true)
    }, [init])

    const handleFocusOut = useCallback(() => {
        if (!isMobile)
            setTimeout(() => {
                handleOnClose()
            }, 400)
    }, [])

    const handleOnClose = useCallback((e) => {
        tryIt(() => e.stopPropagation())
        tryIt(() => onChange(false))
        tryIt(() => onClose())
        setFocused(false)
    }, [onChange, onClose, setFocused])

    const handleTextChange = useCallback((e, v) => {
        setSearchValue(v)
    }, [])

    const handleSubmitChange = useCallback(() => {
        tryIt(() => onClose())
        setTimeout(() => {
            try {
                const activeEl = getSafe(() => getListElements().activeElement, undefined)
                if (activeEl) {
                    activeEl.getElementsByTagName("a")[0].click()

                    // if (document.activeElement) {
                    //     document.activeElement.blur();
                    // }
                    return
                }
            } catch (e) {
            }


            const searchValue = getSafe(() => ref?.current?.serialize()?.baseSearch);

            if (searchValue.length >= 2) {
                tryIt(() => {
                    const as = rout.Product.Search.as({q: searchValue})
                    router.push(as)
                })
            }
        }, 500)
        tryIt(() => document.activeElement.blur())

    }, [ref])


    function handleClearChange() {
        tryIt(() => {
            setSearchValue("")
            clearTextFieldValue("baseSearch", true)
        })
    }

    const updateBackdropDebounce = debounce(() => {
        setBackdrop(Boolean((focused || backdrop)))
    }, 300)

    const updateBackdrop = () => {
        setBackdrop(Boolean(focused || backdrop))
        // dispatch((focused || backdrop) ? showBackdrop() : hideBackdrop())
    }

    //region init
    useEffect(() => {
        setInit(true)
        tryIt(() => {
            //Todo: check is working
            getSafe(() => router.prefetch(rout.Product.Search.rout))
            getSafe(() => router.prefetch(rout.Product.SingleV2.rout))
        })
    }, [])

    useEffect(() => {
        if (!init)
            return;
        if ((focused || backdrop)) {
            updateBackdrop()
            return
        }
        updateBackdropDebounce()
        return updateBackdropDebounce.cancel;
    }, [focused, backdrop])

    //endregion init

    const itemsBox = useMemo(() => (searchValue.length >= 2 && focused), [searchValue, focused]);

    const activeBackDrop = useMemo(() => focused || backdrop, [focused, backdrop]);

    function handleKeyPress(event) {
        if (event.key === "ArrowUp" || event.key === "ArrowDown") {
            event.stopPropagation()
            event.preventDefault();
            handleActiveItemChange(event.key === "ArrowDown")
            return false
        }
    }

    const customTheme = createTheme({
        ...theme,
        palette: {
            primary: {
                light: colors.backgroundColor.hf8f8f8,
                main: colors.backgroundColor.hf8f8f8,
                dark: colors.backgroundColor.hf8f8f8,
            },
            secondary: {
                light: colors.backgroundColor.hf8f8f8,
                main: colors.backgroundColor.hf8f8f8,
                dark: colors.backgroundColor.hf8f8f8
            }
        },
    })

    return (
        <ThemeProvider theme={customTheme}>
            <FormController
                innerRef={ref}
                name={"baseSearch-form-controller"}
                width={1}
                className={classes.searchContainer}
                flexDirectionColumn={true}
                center={true}
                onSubmit={handleSubmitChange}
                {...props}>
                <DefaultTextField
                    className={classes.searchInput}
                    defaultValue={searchValue}
                    variant={"outlined"}
                    type={"search"}
                    placeholder={lang.get("search")}
                    name={"baseSearch"}
                    autoComplete={"off"}
                    autoFocus={false}
                    onFocusInDelay={0}
                    onFocusOutDelay={0}
                    inputProps={{
                        onKeyDown: handleKeyPress
                    }}
                    onFocusIn={handleFocusIn}
                    onFocusOut={handleFocusOut}
                    onChangeTextFieldDelay={300}
                    onChangeDelay={1000}
                    onChange={handleTextChange}
                    startAdornment={(
                        <React.Fragment>
                            <SmHidden>
                                <MtIcon icon={"mt-search"} color={colors.textColor.h757575}/>
                            </SmHidden>
                            <SmShow>
                                <IconButton
                                    title={lang.get("back")}
                                    onClick={handleOnClose}>
                                    <ArrowForward/>
                                </IconButton>
                            </SmShow>
                        </React.Fragment>
                    )}
                    endAdornment={(
                        searchValue ?
                            <IconButton
                                title={lang.get("clear")}
                                onClick={handleClearChange}>
                                <Close/>
                            </IconButton> :
                            <React.Fragment/>
                    )}
                    textFieldProps={{
                        InputProps: {
                            style: {
                                // height: "3.4375vw",
                                // maxHeight: 62
                            }
                        }
                    }}/>
                <SearchBox
                    isMobile={isMobile}
                    loading={!Boolean(dd)}
                    dataLength={dataLength}
                    open={itemsBox}
                    data={data}/>
            </FormController>
        </ThemeProvider>
    )
}

export function focusSearchTextField() {
    tryIt(() => {
        document.getElementsByName("baseSearch")[0].focus()
    })
}
