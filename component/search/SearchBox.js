import React, {useMemo} from "react";
import {Box, getSafe, tryIt, Typography, UtilsStyle} from "material-ui-helper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useTheme} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import {UtilsParser} from "../../utils/Utils";
import _ from "lodash"
import {colors, media, theme} from "../../repository";
import {DEBUG} from "../../pages/_app";
import Nlink from "../base/link/NLink";
import rout from "../../router";
import LinearProgress from "@material-ui/core/LinearProgress";
import {maxAppBarHeight} from "../header/HeaderLg";
import Img from "../base/Img";


const useStyles = makeStyles((theme) => ({
    searchPopover: {
        position: 'absolute',
        right: 0,
        left: 0,
        top: 0,
        paddingTop: maxAppBarHeight + 10,
        backgroundColor: "#fff",
        ...UtilsStyle.borderRadius("0 0 5px 5px"),
        "& li": {
            "&:hover, &.active": {
                backgroundColor: DEBUG ? "red" : colors.backgroundColor.hf8f8f8
            }
        }
    },
}));


export default function SearchBox({open, isMobile = false, loading, dataLength = 0, data:dd, ...props}) {
    const theme = useTheme();
    const classes = useStyles()

    const data = useMemo(()=>{
        const newD={};
        _.forEach(dd,(d,key)=>{
            if (_.isEmpty(d))
                return
            newD[key] = d
        })
        return newD
    },[dd])

    const dataEmpty = useMemo(() => {
        return tryIt(() => {
            let empty = true;
            _.forEach(data, (d) => {
                if (!_.isEmpty(d)) {
                    empty = false
                    return false
                }
            })
            return empty
        }, true)
    }, [data])

    return (
        ((open || isMobile) && !dataEmpty) ?
            <Box id={"search-box"} className={classes.searchPopover} flexDirectionColumn={true}>
                <Box
                    display={"block"}
                    width={1}
                    style={{
                        opacity: loading ? 1 : 0
                    }}>
                    <LinearProgress/>
                </Box>
                {Object.keys(data).map((key, index) => {
                    const its = getSafe(() => data[key], []);
                    return (
                        <ItemTypeSelector
                            key={key}
                            data={its}
                            index={index} itemType={key}/>
                    )
                })}
            </Box> :
            <React.Fragment/>
    )
}

function ItemTypeSelector({itemType, data, index}) {

    return (
        !_.isEmpty(data) ?
            <Box width={1} flexDirectionColumn={true}>
                {
                    (index !== 0 && !_.isEmpty(data)) &&
                    <Box width={1} py={1}>
                        <Divider style={{width: "100%"}}/>
                    </Box>
                }
                <Box component={"ul"} width={1} flexDirectionColumn={true}>
                    {data.map(it => (
                        <React.Fragment key={it?.permalink || it?.id || it?.name || it}>
                            {
                                it ?
                                    itemType === "products" ?
                                        <ProductItem data={it}/> :
                                        itemType === "categories" ?
                                            <CategoryItem data={it}/> :
                                            itemType === "tags" ?
                                                <TagsItem data={it}/> :
                                                <React.Fragment/> :
                                    <React.Fragment/>
                            }
                        </React.Fragment>
                    ))}
                </Box>
            </Box> :
            <React.Fragment/>
    )
}

function ProductItem({data}) {
    const theme = useTheme();

    return (
        <Box component={"li"} width={1}>
            <Nlink
                width={1} href={rout.Product.SingleV2.create(data?.permalink)} hoverStyle={false}
                prefetch={true}>
                <Box
                    width={1}
                    px={4}
                    py={1}
                    alignCenter={true}>
                    <Box width={theme.spacing(12.75)}>
                        <Img
                            imageWidth={media.thumbnail.width}
                            imageHeight={media.thumbnail.height}
                            src={DEBUG ? "http://api.mt.com/media/boxes/4/2020-05-15/thumbnail/08-45-49-38-has-ph.jpg" : data?.thumbnail}
                            alt={data?.orgName || data?.name}/>
                    </Box>
                    <Typography pl={2} display={'block'} component={"p"} variant={"body2"}>
                        {getSafe(() => UtilsParser.html(data?.name), data?.name)}
                    </Typography>
                </Box>
            </Nlink>
        </Box>
    )
}

function CategoryItem({data}) {
    return (
        <Box component={"li"} width={1}>
            <Nlink
                width={1}
                href={rout.Product.Search.create({category: data.permalink})}
                hoverStyle={false} prefetch={true}>
                <Typography
                    width={1}
                    px={2}
                    py={1} feild-attr={"category-search-item"} display={'block'} variant={"body2"}>
                    {
                        data?.parent ?
                            <React.Fragment>
                                <Typography display={'unset'} variant={"body2"} color={theme.palette.secondary.main}>
                                    {getSafe(() => UtilsParser.html(data?.name), data?.name)}
                                </Typography>
                                &nbsp;
                                در دسته
                                &nbsp;
                                {getSafe(()=>UtilsParser.html(data?.parent), data?.parent)}
                            </React.Fragment>
                            :
                            <React.Fragment>
                                همه کالا های دسته بندی
                                &nbsp;
                                <Typography display={'unset'} variant={"body2"} color={theme.palette.secondary.main}>
                                    {getSafe(() => UtilsParser.html(data?.name), data?.name)}
                                </Typography>
                            </React.Fragment>
                    }
                </Typography>
            </Nlink>
        </Box>
    )
}


function TagsItem({data}) {
    return (
        <Box width={1} component={"li"}>
            <Nlink
                width={1}
                href={rout.Product.Search.create({q: data})}
                hoverStyle={false} prefetch={true}>
                <Typography
                    width={1} px={2} py={1} display={'block'} variant={"body2"}>
                    {getSafe(() => UtilsParser.html(data), data)}
                </Typography>
            </Nlink>
        </Box>

    )
}


export function handleActiveItemChange(arrowDown) {
    try {
        const {elements, activeIndex} = getListElements()
        if (activeIndex >= 0)
            tryIt(() => elements[activeIndex].classList.remove("active"))


        const nextIndex = getSafe(() => {
            if (arrowDown) {
                return activeIndex >= elements.length - 1 ? undefined : (activeIndex === -1) ? 0 : activeIndex + 1
            }
            return activeIndex === 0 ? undefined : (activeIndex === -1) ? elements.length - 1 : activeIndex - 1
        })
        if (nextIndex === undefined)
            throw ""

        elements[nextIndex].classList.add("active")
    } catch (e) {
    }
}

export function getListElements() {
    try {

        const lis = document.getElementById("search-box").querySelectorAll("li");
        let activeElement = undefined;
        let activeIndex = -1;
        _.forEach(lis, (it, index) => {
            try {
                if (it.classList.contains("active")) {
                    activeElement = it;
                    activeIndex = index
                    return false
                }
            } catch (e) {
            }
        })
        return {
            elements: lis,
            activeElement: activeElement,
            activeIndex: activeIndex,
        }
    } catch (e) {
    }
    return {}
}




