import React from "react";
import {NextSeo} from "next-seo";
import {lang, siteRout} from "../../repository";
import rout from "../../router";
import {useRouter} from "next/router";


export default function Seo(){
    const {query} = useRouter()

    return(
        <NextSeo
            title={lang.get("search_products")}
            noindex={false}
            nofollow={false}
            canonical={siteRout + rout.Product.Search.as({category: query.cat}).pathname}
            openGraph={{
                title: lang.get("search_products"),
            }}/>
    )
}