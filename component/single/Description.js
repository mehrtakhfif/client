import React from "react";
import BoxWithTopBorder from "../base/textHeader/BoxWithTopBorder";
import Card from "@material-ui/core/Card";
import {orange} from "@material-ui/core/colors";
import {UtilsParser} from "../../utils/Utils";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core/styles";
import {productDescription} from "../../controller/type";
import ErrorBoundary from "../base/ErrorBoundary";
import _ from "lodash"

const useDescriptionStyles = makeStyles(theme => ({
    descriptionRoot: {
        '& ul': {

            [theme.breakpoints.down('md')]: {
                paddingRight: 5
            },
        }
    }
}));

export default function Description({description, ...props}) {
    const classes = useDescriptionStyles()
    return (
        <React.Fragment>
            {
                (description && _.isArray(description.items)) && description.items.map(it => {
                    return (
                        <React.Fragment key={it.key}>
                            <ErrorBoundary elKey={"productDescription"}>
                                {(!_.isEmpty(it.value) && it.value !== '<p></p>') ?
                                    <BoxWithTopBorder
                                        component={Card}
                                        title={productDescription[it.key].label}
                                        borderColor={orange[700]}
                                        className={classes.descriptionRoot}
                                        borderStork={5}
                                        mt={2}>
                                        <Box
                                            lineHeight={1.8}
                                            px={2}
                                            py={1}>
                                            {UtilsParser.html(it.value)}
                                        </Box>
                                    </BoxWithTopBorder> :
                                    <React.Fragment/>
                                }
                            </ErrorBoundary>
                        </React.Fragment>
                    )
                })
            }
        </React.Fragment>
    )
}
