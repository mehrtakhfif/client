import React from "react";
import {Hidden, Paper} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import {LocationOn} from "@material-ui/icons";
import Loadable from "react-loadable";
import ComponentError from "../base/ComponentError";
import {SliderPlaceHolder} from "./slider/Slider";
import Typography from "../base/Typography";
import {lang} from "../../repository"
import ErrorBoundary from "../base/ErrorBoundary";
import {SmShow} from "../header/Header";
import {TimerComponent} from "../productV2/storagePanel/DeadlineTimer";

export default function HeaderAndSlider({boxName, name, media, deadline, shortAddress, ...props}) {
    return (
        <ErrorBoundary
            sendToSentry={false}
            onError={() => {
                window.location.reload()
            }}>
            <Paper elevation={2}>
                <Box p={2}>
                    <SmShow>
                        <Box display='flex' flexDirection='column'>
                            <Typography component={'h6'}
                                        flex={1}
                                        variant={'subtitle1'}
                                        fontWeight={300}>
                                {boxName}
                            </Typography>
                            <Typography
                                component={'h5'}
                                pt={2}
                                variant={'h6'}
                                fontWeight={400}>
                                {name}
                            </Typography>
                        </Box>
                    </SmShow>
                    <Slider items={media}/>
                </Box>
                {shortAddress &&
                <Hidden smDown>
                    <Box display='flex' alignItems='center' mx={4} pt={1} pb={3}>
                        <LocationOn fontSize="large"/>
                        <Typography pl={2}
                                    pr={1}
                                    fontWeight={500}
                                    component={'span'}
                                    variant={'h6'}>
                            {shortAddress}
                        </Typography>
                    </Box>
                </Hidden>
                }
                {deadline &&
                <Hidden mdUp>
                    <Box width={1} height={'2px'} style={{
                        borderTop: '1px dashed #000'
                    }}/>
                    <Box py={1} display={'flex'} flexWrap={'wrap'} alignItems={'center'}>
                        <Typography
                            component={"span"}
                            pr={1}
                            variant={"h5"}>
                            {lang.get("purchase_deadline")}:
                        </Typography>
                        <TimerComponent deadline={deadline} pl={0.5}/>
                    </Box>
                </Hidden>
                }
            </Paper>
        </ErrorBoundary>
    )
}


//region Component
const Slider = Loadable.Map({
    loader: {
        Component: () => import('./slider/Slider'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SliderPlaceHolder/>;
    },
    render(loaded, {items, ...props}) {
        const Component = loaded.Component.default;
        return (
            <Component items={items}/>
        );
    },
});

//endregion Component
