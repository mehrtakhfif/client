import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import useSWR, {mutate} from "swr";
import {gcLog} from "../../utils/ObjectUtils";
import Typography from "../base/Typography";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {lang} from "../../repository";
import {useTheme} from "@material-ui/core";
import {cyan, grey} from "@material-ui/core/colors";
import _ from "lodash"
import Fade from "@material-ui/core/Fade";
import BaseButton from "../base/button/BaseButton";
import ButtonBase from "@material-ui/core/ButtonBase";
import Skeleton from "@material-ui/lab/Skeleton";
import {addProduct2} from "../../middleware/shopping";
import ComponentError from "../base/ComponentError";
import {KeyboardArrowDown, RadioButtonChecked, RadioButtonUnchecked} from "@material-ui/icons";
import BookingSteps from "./BookingSteps";
import Img from "../base/oldImg/Img";
import IconButton from "@material-ui/core/IconButton";
import Popover from "@material-ui/core/Popover";
import {makeStyles} from "@material-ui/core/styles";


const purchaseTypes = {
    normal: {
        key: "normal",
        label: "ارسال عادی",
        caption: "",
        buttonLabel: "افزودن به سبد خرید"
    },
    booking: {
        key: "booking",
        label: "ارسال در ساعت و زمان خاص",
        caption: "امکان ارسال کارت پستال رایگان برای هدایای شما",
        buttonLabel: "خرید"
    },
}

export default function Storage2({permalink, storageId, onChangeStorage, onBooking, ...props}) {
    const theme = useTheme();
    const [activeItem, setActiveItem] = useState({})
    const [openBooking, setOpenBooking] = useState(false)
    const [defaultsFeatures, setDefaultsFeatures] = useState(undefined)
    const [backupData, setBackupData] = useState(undefined)
    const [purchaseType, setPurchaseType] = useState(purchaseTypes.normal.key);
    const [count, setCount] = useState(1);
    const d = ControllerProduct.Product.getStorage({
        permalink: permalink,
        storage_id: storageId,
        selectedFeatures: (() => {
            let list = []
            _.forEach(activeItem, (ac) => {
                list.push(ac)
            })
            list = _.sortBy(list)
            return list
        })()
    })

    const {data, error} = useSWR(...d, {
        dedupingInterval: 60000,
        onError: () => {
            // setActiveItem({})
            gcLog("errrrrrrrrrrrrrrr", error)
        }
    })

    const {storage, features} = data || backupData || {}
    const {least_booking_time, booking_cost} = storage || {};

    useEffect(() => {
        try {
            const features = data.features
            //region defaultsFeatures
            if (defaultsFeatures === undefined) {
                const newList = []
                _.forEach(data.features, (f) => {
                    newList.push(f.selected)
                })
                setDefaultsFeatures([..._.sortBy(newList)])
            }
            //endregion defaultsFeatures

            //region activeItem
            let newActiveItem = activeItem;
            _.forEach(features, (f) => {
                newActiveItem[f.id] = f.selected
            })
            setActiveItem({
                ...newActiveItem
            })
            //endregion activeItem


            setBackupData(data)
        } catch (e) {
        }
    }, [data])


    useEffect(() => {
        try {
            if (!storage || !onChangeStorage)
                return
            onChangeStorage(storage)
        } catch (e) {
        }
    }, [storage])


    function onFeaturesChange(feature, value) {
        const newState = !value.available ? {} : activeItem;
        newState[feature.id] = value.id
        setActiveItem({
            ...newState
        })
    }


    function addToStorage() {
        try {
            if (least_booking_time !== -1 && purchaseType === purchaseTypes.booking.key) {
                try {
                    onBooking(true)
                } catch (e) {
                }
                setOpenBooking(true)
                return
            }
            addProduct2( {storage_id: data.storage.id, count: count}).then(r => {
            })
        } catch (e) {
        }
    }

    return (
        <Box display={'flex'} flexDirection={'column'}
             {...props}
             style={{
                 ...props.style,
                 position: 'relative',
             }}>
            {
                !error && storage &&
                <React.Fragment>
                    {
                        storage.discount_price > 0 ?
                            <React.Fragment>
                                {
                                    openBooking ?
                                        <BookingSteps
                                            storage={storage}
                                            count={count}
                                            onCancel={() => {
                                                setOpenBooking(false)
                                                onBooking(false)
                                            }}/> :
                                        <React.Fragment>
                                            <Box display={'flex'} alignItems={'center'}>
                                                {
                                                    storage.discount_percent > 0 &&
                                                    <Typography variant={"h6"} pl={2}
                                                                component={'del'}
                                                                color={grey[600]}>
                                                        {UtilsFormat.numberToMoney(storage.final_price)}
                                                    </Typography>
                                                }
                                                <Typography variant={'h4'} alignItems={'center'} color={'#FF517A'}
                                                            fontWeight={500}>
                                                    {UtilsFormat.numberToMoney(storage.discount_price)}
                                                    <Typography variant={"body1"} pr={1}>
                                                        {lang.get("toman")}
                                                    </Typography>
                                                </Typography>

                                            </Box>
                                            {
                                                storage.discount_percent > 0 &&
                                                <Box my={2} display={'flex'}>
                                                    <Box display={'flex'}
                                                         alignItems={'center'}
                                                         py={0.5}
                                                         px={3}
                                                         style={{
                                                             border: `1.5px solid ${theme.palette.success.light}`,
                                                             ...UtilsStyle.borderRadius("50px")
                                                         }}>
                                                        <Typography color={theme.palette.success.main} pl={0.25}
                                                                    variant={'body1'}
                                                                    fontWeight={500}>
                                                            تخفیف شما:
                                                        </Typography>
                                                        <Typography variant={'h6'}
                                                                    color={theme.palette.success.main}
                                                                    fontWeight={600}>
                                                            {UtilsFormat.numberToMoney(storage.final_price - storage.discount_price)}
                                                        </Typography>
                                                        <Typography color={theme.palette.success.main} pr={0.25}
                                                                    variant={'body2'}
                                                                    fontWeight={500}>
                                                            تومان
                                                        </Typography>
                                                    </Box>
                                                </Box>
                                            }
                                            {
                                                _.isArray(features) &&
                                                features.map(f => (
                                                    <Box
                                                        key={f.id}
                                                        display={'flex'}
                                                        flexDirection={'column'}
                                                        py={2}>
                                                        <Typography fontWeight={500} variant={'body1'}>
                                                            {f.name}
                                                        </Typography>
                                                        <RadioButtonContainer>
                                                            {f.values.map((v, i) => (
                                                                <RadioButton
                                                                    disable={!v.available}
                                                                    active={activeItem[f.id] === v.id}
                                                                    selectable={v.selectable}
                                                                    onClick={(e) => {
                                                                        onFeaturesChange(f, v)
                                                                    }}>
                                                                    <Typography variant={'subtitle1'} fontWeight={500}>
                                                                        {v.name}
                                                                    </Typography>
                                                                    {
                                                                        v.settings.color &&
                                                                        <Box
                                                                            ml={2}
                                                                            style={{
                                                                                width: 18,
                                                                                height: 18,
                                                                                backgroundColor: v.settings.color,
                                                                                border: `1px solid ${v.available ? grey[300] : grey[500]}`,
                                                                                ...UtilsStyle.borderRadius('100%')
                                                                            }}/>
                                                                    }
                                                                </RadioButton>
                                                            ))}
                                                        </RadioButtonContainer>
                                                    </Box>
                                                ))
                                            }
                                            <MaxShippingTime value={storage.max_shipping_time}/>
                                            <Box display={'flex'} flexDirection={'column'} mt={2}>
                                                <SelectPurchaseTypes leastBookingTime={least_booking_time}
                                                                     purchaseType={purchaseType}
                                                                     onTypeChange={(type) => {
                                                                         setPurchaseType(type)
                                                                     }}/>
                                                <Box display={'flex'}>
                                                    <SelectCount
                                                        maxCountForSale={storage.max_count_for_sale}
                                                        count={count}
                                                        onCountChange={(count) => {
                                                            setCount(count)
                                                        }}/>
                                                    <BaseButton
                                                        onClick={addToStorage}
                                                        disabled={!data}
                                                        style={{
                                                            flex: "1",
                                                            boxShadow: "unset",
                                                            backgroundColor: data ? '#ff517a' : '#883c52'
                                                        }}>
                                                        <Typography py={1} fontWeight={600}
                                                                    variant={'subtitle1'}
                                                                    color={data ? '#fff' : grey[500]}>
                                                            {purchaseTypes[purchaseType].buttonLabel}
                                                        </Typography>
                                                    </BaseButton>
                                                </Box>
                                            </Box>
                                        </React.Fragment>
                                }
                            </React.Fragment> :
                            <React.Fragment>
                                <Box width={1} p={3} display={'flex'} alignitems={'center'} justifyContent={'center'}>
                                    <Typography variant={"h6"}>
                                        محصول ناموجود
                                    </Typography>
                                </Box>
                            </React.Fragment>
                    }
                </React.Fragment>
            }
            {
                error ?
                    <Box
                        p={2}>
                        <ComponentError tryAgainFun={() => mutate(d[0])}/>
                    </Box> :
                    !data &&
                    <Box
                        p={2}
                        style={{
                            width: '100%',
                            height: '100%',
                            opacity: 1,
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            position: 'absolute',
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Skeleton
                            variant="rect"
                            style={{
                                width: '100%',
                                height: '100%',
                                opacity: 1,
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0,
                                backgroundColor: 'rgba(0,0,0,0.46)',
                                ...UtilsStyle.borderRadius(5)
                            }}/>
                    </Box>
            }
        </Box>
    )
}

const useSelectCountStyle = makeStyles({
    selectCountItemSelector: {
        '&:hover': {
            backgroundColor: grey[200]
        }
    }
});

function SelectCount({maxCountForSale, count, onCountChange}) {
    const theme = useTheme();
    const [anchorEl, setAnchorEl] = React.useState(null);
const classes = useSelectCountStyle();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (count) => {
        setAnchorEl(null);
        if (count) {
            onCountChange(count)
        }
    };

    return (
        <Box display={'flex'} alignItems={'center'} justifyContent={'center'} pr={1}>
            <ButtonBase
                onClick={handleClick}
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    minWidth: 100,
                    border: `1px solid ${grey[300]}`,
                    height: '100%',
                    ...UtilsStyle.borderRadius(5)
                }}>
                <Box display={"flex"} alignItems={"center"} justifyContent={"center"} px={1}>
                    <Typography variant={"body1"} style={{minWidth: 40}}>
                        {count + " عدد"}
                    </Typography>
                    <KeyboardArrowDown
                        style={{
                            marginRight: theme.spacing(1)
                        }}/>
                </Box>
            </ButtonBase>
            <Popover
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={() => handleClose()}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}>
                <Box display={'flex'} flexDirection={'column'}>
                    {Array(maxCountForSale).fill(0).map((item, i) => (
                        <ButtonBase
                            key={i}
                            className={classes.selectCountItemSelector}
                            onClick={() => {
                                handleClose(i + 1)
                            }}>
                            <Typography variant={"h6"} p={1} alignItems={"center"} style={{minWidth: 100}}>
                                {i + 1}
                                <Typography variant={"body1"} pr={0.5}>
                                    عدد
                                </Typography>
                            </Typography>
                        </ButtonBase>
                    ))}
                </Box>
            </Popover>
        </Box>
    )
}


function SelectPurchaseTypes({leastBookingTime, purchaseType, onTypeChange}) {
    const theme = useTheme();

    return (
        leastBookingTime !== -1 ?
            <Box display={'flex'} pb={2} flexDirection={'column'}>
                <Typography pb={1} variant={"subtitle1"}>
                    انتخاب نحوه ارسال
                </Typography>
                {
                    Object.keys(purchaseTypes).map(k => {
                        const item = purchaseTypes[k];
                        const active = item.key === purchaseType
                        return (
                            <Box display={'flex'} my={1} alignItems={'center'}>
                                <Box mr={0.5}>
                                    <IconButton
                                        onClick={(e) => {
                                            e.stopPropagation()
                                            onTypeChange(item.key)
                                        }}>
                                        {
                                            active ?
                                                <RadioButtonChecked
                                                    style={{color: theme.palette.secondary.main}}/>
                                                :
                                                <RadioButtonUnchecked/>
                                        }
                                    </IconButton>
                                </Box>
                                <ButtonBase
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        onTypeChange(item.key)
                                    }}
                                    style={{width: "100%"}}>
                                    <Box
                                        display={'flex'}
                                        flexDirection={'column'}
                                        justifyContent={'center'}
                                        width={1}
                                        py={1}
                                        px={3}
                                        minHeight={80}
                                        style={{
                                            border: `1px solid ${active ? theme.palette.secondary.main : grey[300]}`,
                                            ...UtilsStyle.borderRadius(5)
                                        }}>
                                        <Typography variant={"subtitle1"}
                                                    fontWeight={400}>
                                            {item.label}
                                        </Typography>
                                        {
                                            item.caption &&
                                            <Typography pt={1.5}
                                                        variant={"caption"}
                                                        fontWeight={400}>
                                                {item.caption}
                                            </Typography>
                                        }
                                    </Box>
                                </ButtonBase>
                            </Box>
                        )
                    })
                }
            </Box> :
            <React.Fragment/>
    )
}

function MaxShippingTime({value, ...props}) {

    const time = (() => {
        if (value < 24) {
            return value
        }
        return Math.ceil(value / 24)
    })()


    return (
        value && value > 1 ?
            <Box display={'flex'} py={1} alignItems={'center'} justifyContent={'center'} width={1} {...props}>
                <Typography
                    px={2}
                    py={2.5}
                    alignItems={'center'}
                    variant={"body1"}
                    fontWeight={500}
                    style={{
                        width: '100%',
                        backgroundColor: "#F2F2F2",
                        ...UtilsStyle.borderRadius(5)
                    }}>
                    <Img width={35} mr={1.5} src={'/drawable/svg/truck-icon.svg'}
                         alt={'Truck Icon'}/>
                    ارسال
                    ({
                    value < 24 ?
                        `پس از ` + time + " ساعت" :
                        'از ' + time + ' روز کاری'
                })
                </Typography>
            </Box> :
            <React.Fragment/>
    )
}


//region RadioButton

function RadioButtonContainer({name, error, ...props}) {
    const theme = useTheme();

    return (
        <Box display={'flex'} flexDirection={'column'}{...props}>
            <Box display={'flex'} flexWrap={'wrap'} flex={1} width={1} pb={2}>
                <Typography variant={"h6"}>
                    {name}
                </Typography>
                <Fade in={error}>
                    <Typography variant={'body2'} pl={1} color={'#E4254A'}>
                        {error}
                    </Typography>
                </Fade>
            </Box>
            <Box display={'flex'} flexWrap={'wrap'}>
                {props.children}
            </Box>
        </Box>
    )
}


const rBTheme = {
    default: {
        key: 'default',
        borderColor: grey[400],
        opacity: 0.9,

    },
    active: {
        key: 'active',
        borderColor: cyan[400],
        opacity: 1,
        borderWidth: 2
    },
    selectable: {
        key: 'selectable',
        borderColor: grey[800],
        opacity: 0.8,
    },
    disable: {
        key: 'disable',
        borderColor: grey[300],
        opacity: 0.3,
    },
}


function RadioButton({active, disable, selectable, rootProps = {}, onClick, ...props}) {
    const theme = useTheme();

    const th = active ? rBTheme.active : disable ? (selectable ? rBTheme.selectable : rBTheme.disable) : rBTheme.default;

    return (
        <Box
            component={ButtonBase}
            onClick={onClick}
            disabled={disable && !selectable}
            {...rootProps}
            style={{
                marginLeft: theme.spacing(1),
                marginTop: theme.spacing(0.5),
                marginBottom: theme.spacing(0.5),
                ...UtilsStyle.borderRadius(5),
                position: 'relative',
                overflow: 'hidden',
                ...rootProps.style
            }}>
            <Box
                style={{
                    border: `${th.borderWidth || 1}px solid ${th.borderColor}`,
                    ...UtilsStyle.borderRadius(5),
                }}>
                <Box
                    display={'flex'}
                    py={0.5}
                    px={2}
                    alignItems={'center'}
                    {...props}
                    style={{
                        opacity: th.opacity,
                        ...props.style
                    }}>
                    {props.children}
                </Box>

            </Box>
            {(disable && !selectable) &&
            <Box
                component={'span'}
                style={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    clipPath: 'polygon(102% 0%, 97% 10%, -22% 117%, -11% 102%)',
                    background: th.borderColor
                }}/>}
        </Box>
    )
}

//endregion RadioButton
