import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {makeStyles, MuiThemeProvider} from "@material-ui/core";
import {UtilsConverter, UtilsStyle} from "../../utils/Utils";
import {dir, lang, theme} from "../../repository";
import Rating from "@material-ui/lab/Rating";
import moment from 'moment'
import _ from 'lodash'
import {grey, red} from "@material-ui/core/colors";
import clsx from "clsx";
import Paper from "@material-ui/core/Paper";
import Storages from "./Storages";
import Typography from "../base/Typography";
import Storage2 from "./Storage2";

export default function SidePanel({permalink, boxName, name, item, deadline, storageId, storagesData, rate, onRatingChange, ...props}) {

    const [activeStorage, setActiveStorage] = useState();
    const [bookingMode, setBookingMode] = useState(false)


    const percent = activeStorage ? activeStorage.discount_percent : 0;


    return (
        <Paper>
            <Box>
                {
                    !bookingMode &&
                    <React.Fragment>
                        <Box position='relative'>
                            {percent ?
                                <Ribbon percent={percent}/> :
                                <React.Fragment/>
                            }
                            <Box
                                display='flex'
                                justifyContent='center'
                                flexDirection='column'
                                pt={4}
                                pb={4}
                                textAlign='center'>
                                <Box component='span'
                                     mb={1}
                                     fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 1)}>
                                    {lang.get('customer_score')}
                                </Box>
                                <Box dir={'ltr'} display={'flex'} justifyContent='center'>
                                    <RatingComponent rate={rate} onRatingChange={onRatingChange}/>
                                </Box>
                            </Box>
                        </Box>
                        <Box width={1} mt={2} position='relative'>
                            <DeadlineTimer deadline={deadline} mt={3}/>
                        </Box>
                    </React.Fragment>
                }
                <Box width={1} mt={!bookingMode ? 2 : 0}>
                    {
                        !bookingMode &&
                        <Box display={'flex'} flexDirection={'column'} px={3}>
                            <Typography variant={"h6"} fontWeight={500}>
                                {name}
                            </Typography>
                            <Typography variant={"body2"} fontWeight={400} pt={0.5}>
                                {boxName}
                            </Typography>
                        </Box>
                    }
                    <Storage2
                        storageId={storageId}
                        permalink={permalink}
                        onBooking={(bookingMode) => {
                            setBookingMode(bookingMode);
                        }}
                        onChangeStorage={(s) => {
                            setActiveStorage(s)
                        }}
                        pt={!bookingMode ? 4 : 2} px={2} pb={2}/>
                    {false && !_.isEmpty(item.storages) &&
                    <Storages pid={item.id} storages={item.storages} activeStorage={activeStorage}
                              storagesData={storagesData}
                              {...props}/>
                    }
                </Box>
            </Box>
        </Paper>
    )
}
//region Component
//region ribbon
const ribbonWidth = 64;
const ribbonColor = red[500];

const ribbonStyle = makeStyles(theme => ({
    ribbonRoot: {
        position: 'absolute',
        width: ribbonWidth,
        backgroundColor: ribbonColor,
        right: 15,
        top: -8,
        paddingTop: 5,
        color: '#fff',
        WebkitAnimation: ' drop forwards 0.8s 1s cubic-bezier(0.165, 0.84, 0.44, 1)',
        animation: ' drop forwards 0.8s 1s cubic-bezier(0.165, 0.84, 0.44, 1)',
        '&:before': {
            content: '""',
            width: 5,
            height: 87,
            position: 'absolute',
            right: -5,
            top: 4,
            backgroundColor: '#04080f14',
            WebkitTransform: 'skewY(35deg) skewX(0)',
            transform: 'skewY(35deg) skewX(0)',
            background: 'linear-gradient(to right, rgba(0, 0, 0, 0.29) 0%,rgba(255,255,255,0) 100%)'
        }
    },
    ribbonEdge: {
        position: 'absolute',
        zIndex: 1,
        borderStyle: 'solid',
        height: 0,
        width: 0,
    },
    ribbonEdgeButtonLeft: {
        borderColor: `transparent transparent transparent ${ribbonColor}`,
        right: 0,
        position: 'absolute',
        borderWidth: '0 0 36px 32px',
        '&:before': {
            content: '""',
            top: 17,
            right: 0.5,
            width: 31,
            height: 7,
            position: 'absolute',
            transform: 'skewY(15deg) skewX(180deg)',
            WebkitTransform: 'skewY(48deg) skewX(180deg)',
            background: 'linear-gradient(to bottom,  rgba(0,0,0,0.44) 0%,rgba(255,255,255,0) 100%)',

        }
    },
    ribbonEdgeButtonRight: {
        borderColor: `${ribbonColor} ${ribbonColor} ${ribbonColor} transparent`,
        left: 0,
        position: 'absolute',
        borderWidth: '36px 0px 0px 32px'
    },
    ribbonEdgeTopRight: {
        top: 0,
        right: -6,
        borderColor: 'transparent transparent #b80101 transparent',
        borderWidth: '0 6px 8px 0',
    }
}));

function Ribbon({percent, ...props}) {
    const classes = ribbonStyle(props);
    return (
        <Box className={classes.ribbonRoot} {...props}>
            <Box display='flex' flexDirection='row-reverse' alignItems='baseline' justifyContent='center' py={2.5}>
                <Box fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.9)}>
                    %
                </Box>
                <Box fontSize={UtilsConverter.addRem(theme.typography.h3.fontSize, 1)}>
                    {percent}
                </Box>
            </Box>
            <Box className={clsx(classes.ribbonEdgeTopRight, classes.ribbonEdge)}/>
            <Box className={clsx(classes.ribbonEdgeButtonLeft, classes.ribbonEdge)}/>
            <Box className={clsx(classes.ribbonEdgeButtonRight, classes.ribbonEdge)}/>
        </Box>
    )
}

//endregion ribbon

//region RatingComponent
export function RatingComponent({name = "product rate", rate, showNoRateSubmit = true, onRatingChange, readOnly = false, size = 'medium', ...props}) {
    const [state, setState] = useState({
        onHover: false
    });

    return (

        <MuiThemeProvider theme={{
            ...theme,
            direction: 'ltr'
        }}>
            <Box style={{direction: 'ltr'}}>
                <Rating
                    name={name}
                    readOnly={readOnly}
                    size={size}
                    onMouseEnter={(e) => {
                        if (!readOnly)
                            setState({
                                ...state,
                                onHover: true
                            })
                    }}
                    onMouseLeave={(e) => {
                        if (!readOnly)
                            setState({
                                ...state,
                                onHover: false
                            })
                    }}
                    onChange={(e, val) => {
                        if (!readOnly)
                            onRatingChange(val)
                    }}
                    precision={state.onHover ? 1 : 0.5}
                    value={state.onHover ? 0 : rate}/>
                {(showNoRateSubmit && !rate) ?
                    <Typography pt={1} variant={'subtitle2'} style={{direction: dir}}>
                        {lang.get("no_rate")}
                    </Typography> :
                    null}
            </Box>
        </MuiThemeProvider>
    )
}

//endregion RatingComponent

//region DeadlineTimer
const color = '#d6d6d6';
const colorAfterBefore = '#afabab';
const deadlineTimerStyle = makeStyles(theme => ({
    deadlineTimerRoot: {
        WebkitBoxShadow: '0 7px 15px 0 #000000',
        boxShadow: '0 7px 15px 0 #000000'
    },
    deadlineTimerRootTimer: {
        '&>div span': {
            color: grey[900],
            minWidth: 23,
            textAlign: 'center',
            fontSize: UtilsConverter.addRem(theme.typography.h6.fontSize, 1),
            ...UtilsStyle.borderRadius(5),
        },
        '&>span': {
            fontWeight: 500,
            fontSize: UtilsConverter.addRem(theme.typography.h5.fontSize, 0.8),
            paddingRight: 0.5,
            paddingLeft: 0.5,
        }
    },
    content: {
        backgroundColor: color,
        width: '100%',
        position: 'relative',
        outline: `10px solid ${color}`,
        left: 0,
        border: '3px solid #d6d6d6 ',
        zIndex: 2,
        color: '#000',
        textAlign: 'center',
        textShadow: `0px 1px 2px ${color}`,
    },
    ribbonEdge: {
        position: 'absolute',
        zIndex: 1,
        borderStyle: 'solid',
        height: 0,
        width: 0,
    },
    ribbonEdgeTopLeft: {
        top: -15,
        borderWidth: '5px 10px 0 0',
        left: -10,
        borderColor: `transparent ${colorAfterBefore} transparent transparent`,
    },
    ribbonEdgeBottomLeft: {
        borderWidth: ' 0 10px 0px 0'
    },
    ribbonEdgeTopRight: {
        top: 0,
        borderWidth: ' 0px 0 0 10px'
    },
    ribbonEdgeBottomRight: {
        borderColor: `transparent transparent transparent ${colorAfterBefore}`,
        right: -10,
        bottom: -15,
        top: 'unset',
        position: 'absolute',
        borderWidth: ' 0 0 5px 10px'
    }
}));

function DeadlineTimer({deadline, ...props}) {
    const [state, setState] = useState({show: deadline > new Date().getTime() / 1000})
    const classes = deadlineTimerStyle(props);
    return (
        (deadline && state.show) ?
            <Box position='relative' {...props} className={classes.deadlineTimerRoot}>
                <Box
                    className={classes.content}
                    display='flex'
                    justifyContent='center'
                    alignItems='center'
                    px={2}>
                    <Box fontWeight={500} ml={1} fontSize={UtilsConverter.addRem(theme.typography.h5.fontSize, 0.9)}
                         style={{
                             color: grey[800]
                         }}>
                        {lang.get("purchase_deadline")}:
                    </Box>
                    <TimerComponent deadline={deadline}/>
                </Box>
                <Box className={clsx(classes.ribbonEdgeTopLeft, classes.ribbonEdge)}/>
                <Box className={clsx(classes.ribbonEdgeBottomRight, classes.ribbonEdge)}/>
            </Box>
            :
            <>
            </>
    )
}

export function TimerComponent({deadline, ...props}) {
    const classes = deadlineTimerStyle(props);

    const [state, setState] = useState({
        duration: moment.duration((deadline - moment().unix()) * 1000, 'milliseconds')
    });
    useEffect(() => {
        const interval = setInterval(() => {
            setState({
                ...state,
                duration: moment.duration((deadline - moment().unix()) * 1000 - interval, 'milliseconds')
            });
        }, 1000);
        return () => clearInterval(interval);
    }, []);


    function serializeTime(val) {
        const v = _.toString(val);
        return v.length === 1 ? '0' + v : v;
    }

    const du = state.duration;
    const days = serializeTime(du.days());
    const hours = serializeTime(du.hours());
    const minutes = serializeTime(du.minutes());
    const seconds = serializeTime(du.seconds());
    return (
        <Box dir='ltr' flex={1} className={classes.deadlineTimerRootTimer} display='flex' {...props}>
            <TimerItem title={lang.get('day')} item={days}/>
            <Box component='span'>
                :
            </Box>
            <TimerItem title={lang.get('hour')} item={hours}/>
            <Box component='span'>
                :
            </Box>
            <TimerItem title={lang.get('minute')} item={minutes}/>
            <Box component='span'>
                :
            </Box>
            <TimerItem title={lang.get('second')} item={seconds}/>
        </Box>
    )
}

function TimerItem({title, item, ...props}) {
    return (
        <Box display='flex' flexDirection='column' minWidth={50} px={1.5} justifyContent={'center'}
             alignItems={'center'}>
            <Box display='flex'>
                <Box component='span'>
                    {item[0]}
                    {item[1]}
                </Box>
            </Box>
            <Box pt={0.5} style={{
                color: grey[900]
            }}>
                {title}
            </Box>
        </Box>
    )
}

//endregion DeadlineTimer
//endregion Component
