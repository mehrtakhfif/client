import React, {useState} from "react";
import Card from "@material-ui/core/Card";
import {lang, theme} from "../../repository";
import {grey, orange} from "@material-ui/core/colors";
import BoxWithTopBorder from "../base/textHeader/BoxWithTopBorder";
import BaseButton from "../base/button/BaseButton";
import {UtilsParser, UtilsStyle} from "../../utils/Utils";
import {DateRange, Map, QueryBuilder} from "@material-ui/icons";
import _ from 'lodash';
import Link from "../base/link/Link";
import {Map as LeafletMap, Marker, Popup, TileLayer} from "react-leaflet-universal";
import {makeStyles} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Typography from "../base/Typography";
import {getSafe} from "material-ui-helper";

export default function Details({details, location, ...props}) {
    const [state, setState] = useState({
        showLocation: false
    });

    function haveText() {
        return details && details.text && details.text === "<p></p>"
    }

    function havePhone() {
        return getSafe(()=>details && details.phones && !_.isEmpty(details.phones),false)
    }

    function haveServingHours() {
        return getSafe(()=>details && details.servingHours,false)
    }

    function haveServingDays() {
        return getSafe(()=>details && details.servingDays,false)
    }

    function haveLocation() {
        return !_.isEmpty(location)
    }

    return (
        (haveText() || havePhone() || haveServingDays() || haveServingHours() ||haveLocation()) &&
        <BoxWithTopBorder
            component={Card}
            title={lang.get('information')}
            borderColor={orange[700]}
            borderStork={5}
            display={"flex"}
            flexDirection={"column"}
            mt={2}>
            <Box mt={1} mx={2}
                 display={'flex'}
                 flexWrap={'wrap'}>
                {
                    haveServingHours() && (
                        <Item icon={<QueryBuilder/>}
                              label={lang.get('serving_hours')}
                              value={details.servingHours}
                              alignItems={'start'}/>
                    )
                }
                {
                    haveServingDays() && (
                        <Item icon={<DateRange/>} label={lang.get('serving_day')} value={details.servingDays}/>
                    )
                }
                {
                    (havePhone()) && (
                        <Item icon={
                            <DateRange
                                style={{
                                    color: '#0000'
                                }}/>}
                              label={lang.get('phone')}
                              isMultiValue={true}
                              value={(
                                  <Box display={'flex'} mr={1} flexDirection={'column'}>
                                      {
                                          details.phones.map(({icon, value}, index) => (
                                              <Box key={index} pb={1}
                                                   display={'flex'}
                                                   alignItems={'center'}>
                                                  {icon}
                                                  <Typography variant={'h6'}>
                                                      <Link pr={0.7} href={`tel:${value}`}>
                                                          {value}
                                                      </Link>
                                                  </Typography>
                                              </Box>
                                          ))
                                      }
                                  </Box>
                              )}/>
                    )
                }
            </Box>
            {
                (haveText()) &&
                <Box mx={2}
                     lineHeight={2}>
                    {UtilsParser.html(details.text)}
                </Box>
            }
            {
               haveLocation() &&
                <Box my={2} mx={2} height={state.showLocation ? 400 : 300}
                display='flex'
                justifyContent={'center'}
                alignItems={'center'}
                style={{
                backgroundImage: state.showLocation ? null : `url(/drawable/image/map.jpg)`,
                border: `1px solid ${grey[700]}`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                ...UtilsStyle.borderRadius(5)
            }}>
                {state.showLocation ?
                    <>
                        <MapComponent location={[
                            location.lat,
                            location.lng,
                        ]}/>
                    </> :
                    <BaseButton
                        onClick={() => setState({...state, showLocation: true})}
                        style={{
                            backgroundColor: '#fff',
                            color: grey[800],
                            padding: theme.spacing(2)
                        }}>
                        {lang.get("show_address_on_map")}
                        <Map
                            style={{
                                marginRight: theme.spacing(1)
                            }}/>
                    </BaseButton>
                }
                </Box>
            }
        </BoxWithTopBorder>
    )
}

function Item({icon, label, value, isMultiValue = false, ...props}) {
    return (
        <Box pr={1}
             pl={2}
             py={2}
             minWidth={'50%'}
             display={'flex'}
             alignItems={_.isString(value) ? 'center' : 'start'}
             {...props}>
            {icon && (
                <Box ml={1} display={'flex'}
                     justifyContent={'center'}
                     alignItems={'center'}>
                    {icon}
                </Box>
            )}
            <Typography
                display={'flex'}
                variant={!isMultiValue ? 'h6' : null}
                component={isMultiValue ? 'div' : 'span'}
                isText={!isMultiValue}>
                {isMultiValue ?
                    <Typography variant={'h6'} component={'span'}>
                        {label}:
                    </Typography> :
                    <>
                        {label}:
                    </>
                } {value}
            </Typography>
        </Box>
    )
}


const useMapStyles = makeStyles({
    map: {
        '& .leaflet-marker-pane img': {
            top: -41,
            right: -12.5,
            width: 25,
            height: 41
        },
        '& .leaflet-popup': {
            bottom: '22px !important',
        }
    }
});

function MapComponent({location, ...props}) {
    const classes = useMapStyles(props);
    return (
        <LeafletMap
            className={classes.map}
            animate={true}
            center={location}
            doubleClickZoom={true}
            boxZoom={false}
            zoomControl={true}
            minZoom={7}
            zoom={14}
            maxZoom={19}
            length={4}
            style={{
                width: '100%',
                height: '100%',
                overflow: 'hidden'
            }}>
            {() => {
                return (
                    <React.Fragment>
                        <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                        <Marker position={location}>
                            {false &&
                            <Popup style={{width: 500}}>
                                <Box display='flex' flexDirection='column' justifyContent='center' alignItems='center'
                                     style={{width: 300}}>
                                    <Box display='flex' mb={1} flexDirection='row'
                                         justifyContent='center'
                                         alignItems='center'>
                                        <Box display='flex' flexDirection='column' justifyContent='center'
                                             alignItems='baseline' pr={2}>
                                            <Box display='flex' alignItems='center'>
                                                ssssssss
                                            </Box>
                                            <Box display='flex' alignItems='center' mt={0.5}>
                                                sf
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                            </Popup>}
                        </Marker>
                    </React.Fragment>
                )
            }}

        </LeafletMap>
    )
}
