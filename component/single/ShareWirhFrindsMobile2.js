import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import {lang} from "../../repository";
import {AddToFavorite} from "../product/ProductCard";
import SpeedDial from "@material-ui/lab/SpeedDial/SpeedDial";
import {FileCopy, Print, Save, Share} from "@material-ui/icons";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import Hidden from "@material-ui/core/Hidden";
import Typography from "../base/Typography";
import {makeStyles} from "@material-ui/styles";

``
const useStyles = makeStyles(theme => ({
    speedDialRoot: {
        "& button": {
            width: 25,
            height: 25,
            minHeight: 'unset',
            '& svg': {
                widht: '0.5em',
                height: '0.5em'
            }
        }
    },
}));``


const actions = [
    {icon: <FileCopy/>, name: 'Copy'},
    {icon: <Save/>, name: 'Save'},
    {icon: <Print/>, name: 'Print'},
    {icon: <Share/>, name: 'Share'},
];
export default function ShareWithFrindsMobile2({product_id, ...props}) {
    const classes = useStyles(props);
    const [state, setState] = useState({
        speedDial: false
    });

    return (
        <Hidden mdUp>
            <Box component={Card}
                 my={2} p={1}
                 flexWrap='wrap'
                 display={'flex'}>
                <Typography component={"span"}
                            variant={"h6"}
                            display={'flex'}
                            alignItems={'center'}
                            flex={{
                                xs: 'unset',
                                sm: 1
                            }}>
                    {lang.get("share_with_friends")}:
                </Typography>
                <Box display={'flex'} alignItems={'center'}
                     width={{
                         xs: 1,
                         sm: 'unset'
                     }}
                     justifyContent={{
                         xs: 'flex-end',
                         sm: 'unset'
                     }}>
                    <Box>
                        <SpeedDial
                            ariaLabel="Share with friends"
                            className={classes.speedDialRoot}
                            icon={<Share/>}
                            onClose={() => {
                                setState({
                                    ...state,
                                    speedDial: false
                                })
                            }}
                            onOpen={() => {
                                setState({
                                    ...state,
                                    speedDial: true
                                })
                            }}
                            open={state.speedDial}
                            direction={'left'}>
                            {actions.map(action => (
                                <SpeedDialAction
                                    title={'salam'}
                                    key={action.name}
                                    icon={action.icon}
                                    tooltipTitle={action.name}
                                    onClick={() => {

                                    }}/>
                            ))}
                        </SpeedDial>
                    </Box>
                    <AddToFavorite product_id={product_id}/>
                </Box>
            </Box>
        </Hidden>
    )
}
