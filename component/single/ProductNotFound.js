import React from "react";
import {Box, ButtonBase, Typography, UtilsStyle} from "material-ui-helper";
import MtIcon from "../MtIcon";
import {colors, lang} from "../../repository";
import {useTheme} from "@material-ui/core";
import Nlink from "../base/link/NLink";
import rout from "../../router";


export default function ProductNotFound() {
    const theme = useTheme();


    return (
        <Box flexDirectionColumn={true} center={true}>
            <Box pt={9}>
                <MtIcon
                    icon={"mt-times-circle"}
                    style={{
                        fontSize: "8.167vw",
                        color: colors.textColor.ha9a9a9
                    }}/>
            </Box>
            <Typography
                pt={4}
                variant={"h6"}
                color={colors.textColor.h757575}>
                {lang.get("product_not_found")}
            </Typography>
            <Box pt={7} pb={18}>
                <ButtonBase>
                    <Box
                        py={1} px={2}
                        style={{
                            backgroundColor: theme.palette.primary.main,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Nlink
                            variant={"body1"}
                            fontWeight={500}
                            color={colors.textColor.hffffff}
                            href={rout.Main.home}>
                            {lang.get("home_page")}
                        </Nlink>
                    </Box>
                </ButtonBase>
            </Box>
        </Box>
    )
}