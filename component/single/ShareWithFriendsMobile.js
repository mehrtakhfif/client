import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {lang} from "../../repository";
import SpeedDial from "@material-ui/lab/SpeedDial/SpeedDial";
import {Share, Sms, Telegram, Twitter, WhatsApp} from "@material-ui/icons";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import {makeStyles} from "@material-ui/styles";
import {SmShow} from "../header/Header";
import {grey} from "@material-ui/core/colors";
import Wish from "../base/Wish";


const useStyles = makeStyles(theme => ({
    title: props => ({
        top: '50%',
        transform: 'translate(0, -50%)',
        display: props.speedDial ? 'none' : 'flex',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
        },
    }),
    speedDialRoot: props => ({
        position: 'relative',
        '&>button': {
            backgroundColor: 'transparent !important',
            boxShadow: 'unset',
            color: grey[700],
            width:36,
            height:36
        },
        "&>div": {
            position: 'absolute',
            top: 30,
            "& button": {
                backgroundColor:grey[200],
                width: 36,
                height: 36,
                marginLeft: theme.spacing(0.2),
                minHeight: 'unset',
                '&:hover':{
                    color:grey[900],
                },
                '& svg': {
                    width: '0.7em',
                    height: '0.7em'
                }
            }
        }
    })
}));

const actions = (productName = "") => [
    {
        id: 1,
        icon: <Twitter/>,
        name: 'Twitter',
        href: `https://twitter.com/intent/tweet?url=${window.location.href}&text=${productName}  ${lang.get("product_share_text")}`
    },
    {
        id: 2, icon: <Telegram/>,
        name: 'Telegram',
        href: `https://telegram.me/share/url?text=${productName}  ${lang.get("product_share_text")} ${window.location.href}`
    },
    {
        id: 3,
        icon: <WhatsApp/>,
        name: 'WhatsApp',
        href: `https://api.whatsapp.com/send?phone=&text=${productName}\n \r${lang.get("product_share_text")} ${window.location.href}`
    },
    {id: 4, icon: <Sms/>, name: 'SMS', href: `sms:;?&body=${productName}  ${lang.get("product_share_text")} ${window.location.href}`},
];
export default function ShareWithFrindsMobile({product_id, productName,wish, ...props}) {
    const [state, setState] = useState({
        speedDial: false
    });
    const classes = useStyles({speedDial: state.speedDial, ...props});

    useEffect(() => {

    });
    return (
        <SmShow>
            <Box display={'flex'}
                 alignItems={'center'}
                 width={1}
                 justifyContent={'flex-end'}>
                <Box>
                    <SpeedDial
                        ariaLabel="Share with friends"
                        className={classes.speedDialRoot}
                        icon={<Share/>}
                        onClose={() => {
                            setState({
                                ...state,
                                speedDial: false
                            })
                        }}
                        onOpen={() => {
                            setState({
                                ...state,
                                speedDial: true
                            })
                        }}
                        open={state.speedDial}
                        direction={'down'}>
                        {actions(productName).map(action => (
                            <SpeedDialAction
                                title={''}
                                key={action.name}
                                icon={action.icon}
                                tooltipTitle={action.name}
                                onClick={(s) => {
                                    window.open(action.href)
                                }}/>
                        ))}
                    </SpeedDial>
                </Box>
                <Wish wish={wish} productId={product_id}/>
            </Box>
        </SmShow>
    )
}

