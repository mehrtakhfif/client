import React from "react";
import Card from "@material-ui/core/Card";
import BoxWithTopBorder from "../base/textHeader/BoxWithTopBorder";
import {lang, theme} from "../../repository";
import Gem2 from "../base/icon/Gem2";
import {cyan, green, orange} from "@material-ui/core/colors";
import {UtilsStyle} from "../../utils/Utils";
import {ThemeProvider} from "@material-ui/styles";
import {MuiThemeProvider} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Typography from "../base/Typography";
import _ from 'lodash'
import Icon from "../base/Icon";
import {convertIconName, convertIconUrl} from "../../controller/converter";

export default function UsageCondition({productName, property = [], usageCondition = [], ...prop}) {
    return (
        (!_.isEmpty(usageCondition) || !_.isEmpty(property)) ?
        <BoxWithTopBorder
            component={Card}
            title={lang.get('usage_condition')}
            borderColor={orange[700]}
            borderStork={5}
            mt={{
                xs:0,
                md:2
            }}>
            <Box pt={2} pb={1}>
                {
                    usageCondition.map((u, index) => <Item key={index} item={u}/>)
                }
                {
                    property &&
                    <>
                        <Box pr={2} pl={1} pt={!_.isEmpty(usageCondition)? 2.2 : 0} mt={1.5} mb={3} fontWeight={600}
                             display={'flex'}
                             alignItems={'center'}
                             fontSize={"h6"}
                             style={{
                                 borderTop:!_.isEmpty(usageCondition)? `1px #5a5a5a73 dashed`:null
                             }}>
                            <Gem2
                                gemColor={green[500]}
                                style={{
                                    width: 15,
                                    marginLeft: theme.spacing(1)
                                }}/>
                            {lang.get('property') + ' ' + productName}:
                        </Box>
                        {property.map((u, index) => <Item key={index} isUsageCondition={false} item={u}/>)}
                    </>
                }
            </Box>
        </BoxWithTopBorder>:<React.Fragment/>
    )
}

function Item({isUsageCondition = true, item, ...props}) {
    const {text,icon, type, priority, isHigh, isMedium, isLow} = item;

    return (
        <Box display={'flex'}
             alignItems={'center'}
             width={'fit-content'}
             py={1} px={2} mx={2} my={isHigh ? 1 : 0}
             style={{
                 border: isHigh && `1px solid ${isUsageCondition ? orange[600] : cyan[200]}`,
                 WebkitBoxShadow: isHigh && '0 1px 4px 0 #0a0a0a63',
                 boxShadow: isHigh && '0 1px 4px 0 #0a0a0a63',
                 ...UtilsStyle.borderRadius(4),
             }}>
            <Box display={'flex'} width={50} height={50} >
                <Icon
                    color={isUsageCondition?"orange":'cyan'}
                    width={50}
                    height={50}
                    alt={convertIconName(icon)}
                    src={convertIconUrl(icon)}/>
            </Box>
            <MuiThemeProvider theme={theme}>
                <ThemeProvider theme={theme}>
                    <Typography pr={2}
                         variant={!isLow ? 'subtitle1':'subtitle2'}
                         fontWeight={isHigh ? 400 : isMedium ? 400 : 300}>
                        {text}
                    </Typography>
                </ThemeProvider>
            </MuiThemeProvider>
        </Box>)
}

