import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core";
import {UtilsConverter, UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {colors, lang, theme} from "../../repository";
import Rating from "@material-ui/lab/Rating";
import moment from 'moment'
import _ from 'lodash'
import {amber, grey, red} from "@material-ui/core/colors";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import Gem from "../base/icon/Gem";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import BaseTextField from "../base/textField/base/BaseTextField";
import {Add, AddShoppingCart, Remove, ShoppingCart} from "@material-ui/icons";
import SuccessButton from "../base/button/buttonVariant/SuccessButton";
import BaseButton from "../base/button/BaseButton";
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles(theme => (
    {
        sidePanelRoot: (props => ({
            transition: 'transform .3s,-webkit-transform .3s',
            position: 'sticky',
            height: window.screen.availHeight - 210,
            overflow: 'scroll',
            overflowX: 'hidden',
            top: !props.showDefaultHeader ? 77 : 132,
            ...UtilsStyle.transition(200),
        }))
    }));

export default function SidePanel({item, activeStorage, rate, onRatingChange, onStorageCountChange, onChangeSelectedItem, ...props}) {
    const [state, setState] = useState({});
    const classes = useStyles();
    const percent = _.find(item.storages, function (s) {
        return s.id === activeStorage
    }).discountPercent;
    return (
        <Box className={classes.sidePanelRoot} {...props}>
            <Box>
                <Box position='relative'
                     mt={'8px'}
                     mx={'5px'}
                     style={{
                         backgroundColor: '#fff',
                         borderTopRightRadius: 4,
                         borderTopLeftRadius: 4,
                     }}>
                    <Ribbon percent={percent}/>
                    <Box
                        display='flex'
                        justifyContent='center'
                        flexDirection='column'
                        pt={4}
                        pb={4}
                        textAlign='center'>
                        <Box component='span'
                             mb={1}
                             fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 1)}>
                            {lang.get('customer_score')}
                        </Box>
                        <Box dir={'ltr'} display={'flex'} justifyContent='center'>
                            <RatingComponent rate={rate} onRatingChange={onRatingChange}/>
                        </Box>
                    </Box>
                </Box>
                <Box pt={2} mx={"5px"}
                     position='relative'
                     style={{
                         backgroundColor: '#fff'
                     }}>
                    <DeadlineTimer deadline={item.deadline} mt={3}/>
                </Box>
                <Box mx={"5px"}
                     pt={3}
                     pb={2}
                     style={{
                         backgroundColor: '#fff',
                         borderBottomRightRadius: 4,
                         borderBottomLeftRadius: 4,
                     }}>
                    <Storages storages={item.storages} activeStorage={activeStorage}
                              onStorageCountChange={onStorageCountChange}
                              onChangeSelectedItem={onChangeSelectedItem}/>
                </Box>
            </Box>
        </Box>
    )
}

//region Component
//region ribbon
const ribbonWidth = 64;
const ribbonColor = red[500];

const ribbonStyle = makeStyles(theme => ({
    ribbonRoot: {
        position: 'absolute',
        width: ribbonWidth,
        backgroundColor: ribbonColor,
        left: 15,
        top: -18,
        paddingTop: 5,
        color: '#fff',
        WebkitAnimation: ' drop forwards 0.8s 1s cubic-bezier(0.165, 0.84, 0.44, 1)',
        animation: ' drop forwards 0.8s 1s cubic-bezier(0.165, 0.84, 0.44, 1)',
        '&:before': {
            content: '""',
            width: 5,
            height: 108,
            position: 'absolute',
            right: -5,
            top: 18,
            backgroundColor: '#04080f14',
            WebkitTransform: 'skewY(35deg) skewX(0)',
            transform: 'skewY(15deg) skewX(0)',
            background: 'linear-gradient(to right, rgba(0, 0, 0, 0.29) 0%,rgba(255,255,255,0) 100%)'
        }
    },
    ribbonEdge: {
        position: 'absolute',
        zIndex: 1,
        borderStyle: 'solid',
        height: 0,
        width: 0,
    },
    ribbonEdgeButtonLeft: {
        borderColor: `transparent transparent transparent ${ribbonColor}`,
        left: 0,
        position: 'absolute',
        borderWidth: '0 0 36px 32px',
        '&:before': {
            content: '""',
            top: 17,
            right: 0.5,
            width: 31,
            height: 7,
            position: 'absolute',
            transform: 'skewY(15deg) skewX(0)',
            WebkitTransform: 'skewY(132deg) skewX(0)',
            background: 'linear-gradient(to bottom,  rgba(0,0,0,0.44) 0%,rgba(255,255,255,0) 100%)',

        }
    },
    ribbonEdgeButtonRight: {
        borderColor: `${ribbonColor} ${ribbonColor} ${ribbonColor} transparent`,
        right: 0,
        position: 'absolute',
        borderWidth: '36px 0px 0px 32px',
    },
    ribbonEdgeTopRight: {
        top: 10,
        right: -6,
        borderColor: 'transparent transparent #b80101 transparent',
        borderWidth: '0 6px 8px 0',
    }
}));

function Ribbon({percent, ...props}) {
    const classes = ribbonStyle(props);
    return (
        <Box className={classes.ribbonRoot} {...props}>
            <Box display='flex' flexDirection='row-reverse' alignItems='baseline' justifyContent='center' py={2.5}>
                <Box fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.9)}>
                    %
                </Box>
                <Box fontSize={UtilsConverter.addRem(theme.typography.h3.fontSize, 1)}>
                    {percent}
                </Box>
            </Box>
            <Box className={clsx(classes.ribbonEdgeTopRight, classes.ribbonEdge)}/>
            <Box className={clsx(classes.ribbonEdgeButtonLeft, classes.ribbonEdge)}/>
            <Box className={clsx(classes.ribbonEdgeButtonRight, classes.ribbonEdge)}/>
        </Box>
    )
}

//endregion ribbon

//region RatingComponent
function RatingComponent({rate, onRatingChange, ...props}) {
    const [state, setState] = useState({
        onHover: false
    });
    const element = {}
    if (!state.onHover) {


    }
    return (
        <Rating
            name="product rate"
            onMouseEnter={(e) => {
                setState({
                    ...state,
                    onHover: true
                })
            }}
            onMouseLeave={(e) => {
                setState({
                    ...state,
                    onHover: false
                })
            }}
            onChange={(e, val) => {
                onRatingChange(val)

            }}
            precision={state.onHover ? 1 : 0.5}
            value={state.onHover ? 0 : rate}/>
    )
}

//endregion RatingComponent

//region DeadlineTimer
const color = '#d6d6d6';
const colorAfterBefore = '#afabab';
const deadlineTimerStyle = makeStyles(theme => ({
    root: {
        WebkitBoxShadow: '0 7px 15px 0 #000000',
        boxShadow: '0 7px 15px 0 #000000'
    },
    timer: {
        '&>div span': {
            backgroundColor: '#000',
            color: '#fff',
            minWidth: 23,
            textAlign: 'center',
            marginRight: 1.5,
            marginLeft: 1.5,
            paddingTop: 2,
            paddingBottom: 2,
            paddingRight: 1,
            paddingLeft: 1,
            fontSize: UtilsConverter.addRem(theme.typography.h6.fontSize, 0.9),
            ...UtilsStyle.borderRadius(5),
        },
        '&>span': {
            fontWeight: 500,
            fontSize: UtilsConverter.addRem(theme.typography.h5.fontSize, 0.8),
            paddingRight: 0.5,
            paddingLeft: 0.5,
        }
    },
    content: {
        backgroundColor: color,
        width: '100%',
        position: 'relative',
        outline: `10px solid ${color}`,
        left: 0,
        zIndex: 2,
        color: '#000',
        textAlign: 'center',
        textShadow: `0px 1px 2px ${color}`,
    },
    ribbonEdge: {
        position: 'absolute',
        zIndex: 1,
        borderStyle: 'solid',
        height: 0,
        width: 0,
    },
    ribbonEdgeTopLeft: {
        top: -13,
        borderWidth: '5px 10px 0 0',
        left: -10,
        borderColor: `transparent ${colorAfterBefore} transparent transparent`,
    },
    ribbonEdgeBottomLeft: {
        borderWidth: ' 0 10px 0px 0'
    },
    ribbonEdgeTopRight: {
        top: 0,
        borderWidth: ' 0px 0 0 10px'
    },
    ribbonEdgeBottomRight: {
        borderColor: `transparent transparent transparent ${colorAfterBefore}`,
        right: -10,
        bottom: -13,
        top: 'unset',
        position: 'absolute',
        borderWidth: ' 0 0 5px 10px'
    }
}));

function DeadlineTimer({deadline, ...props}) {
    const classes = deadlineTimerStyle(props);
    const [state, setState] = useState({
        duration: moment.duration((deadline - moment().unix()) * 1000, 'milliseconds')
    });
    useEffect(() => {
        const interval = setInterval(() => {
            setState({
                ...state,
                duration: moment.duration((deadline - moment().unix()) * 1000 - interval, 'milliseconds')
            });
        }, 1000);
        return () => clearInterval(interval);
    }, []);


    function serializeTime(val) {
        const v = _.toString(val);
        return v.length === 1 ? '0' + v : v;
    }

    const du = state.duration;
    const days = serializeTime(du.days());
    const hours = serializeTime(du.hours());
    const minutes = serializeTime(du.minutes());
    const seconds = serializeTime(du.seconds());

    return (
        <Box position='relative' {...props} className={classes.root}>
            <Box
                className={classes.content}
                display='flex'
                justifyContent='center'
                alignItems='center'
                px={2}>
                <Box fontWeight={500} ml={1} fontSize={UtilsConverter.addRem(theme.typography.h5.fontSize, 0.9)}>
                    {lang.get("purchase_deadline")}:
                </Box>
                <Box dir='ltr' flex={1} className={classes.timer} display='flex'>
                    <TimerItem title={lang.get('day')} item={days}/>
                    <Box component='span'>
                        :
                    </Box>
                    <TimerItem title={lang.get('hour')} item={hours}/>
                    <Box component='span'>
                        :
                    </Box>
                    <TimerItem title={lang.get('minute')} item={minutes}/>
                    <Box component='span'>
                        :
                    </Box>
                    <TimerItem title={lang.get('second')} item={seconds}/>
                </Box>
            </Box>
            <Box className={clsx(classes.ribbonEdgeTopLeft, classes.ribbonEdge)}/>
            <Box className={clsx(classes.ribbonEdgeBottomRight, classes.ribbonEdge)}/>
        </Box>
    )
}

function TimerItem({title, item, ...props}) {
    return (
        <Box display='flex' flexDirection='column'>
            <Box display='flex'>
                <Box component='span'>
                    {item[0]}
                </Box>
                <Box component='span'>
                    {item[1]}
                </Box>
            </Box>
            <Box pt={0.5}>
                {title}
            </Box>
        </Box>
    )
}

//endregion DeadlineTimer

//region Storages
function Storages({storages, activeStorage, onStorageCountChange, onChangeSelectedItem, ...props}) {
    return (
        <Box display={'flex'} flexDirection='column' {...props}>
            {storages.map((storage) => (
                <StoragesItem key={storage_id} storage={storage}
                              onStorageCountChange={onStorageCountChange}
                              active={storage_id === activeStorage}
                              onChangeSelectedItem={onChangeSelectedItem}/>
            ))}
            {false &&
            <Box display={'flex'}
                 justifyContent={'center'}
                 flexDirection={'row-reverse'}
                 mt={1} px={2} py={1.5}
                 style={{
                     position: 'sticky',
                     bottom: 0,
                     backgroundColor: '#ffffff',
                     WebkitBoxShadow: '0 -5px 11px -5px #0000007d',
                     boxShadow: '0 -5px 11px -5px #0000007d',
                     ...UtilsStyle.borderRadius(4)
                 }}>
                <SuccessButton style={{
                    paddingTop: theme.spacing(1.2),
                    paddingBottom: theme.spacing(1.2),
                    marginRight: theme.spacing(0.5)
                }}>
                    <ShoppingCart/>
                    <Box mr={1}>
                        {lang.get("order_now")}
                    </Box>
                </SuccessButton>
                <BaseButton style={{
                    color: '#fff',
                    backgroundColor: amber[700],
                    paddingTop: theme.spacing(1.2),
                    paddingBottom: theme.spacing(1.2),
                    marginLeft: theme.spacing(0.5)
                }}>
                    <AddShoppingCart/>
                </BaseButton>
            </Box>}
        </Box>
    )
}

const storagesItemStyle = makeStyles((theme) => ({
    root: ({active, ...props}) => ({
        cursor: active ? 'default' : 'pointer',
        WebkitAnimation: active ? 'storage-box-shadow-animation 2s infinite' : '',
        animation: active ? 'storage-box-shadow-animation 2s infinite' : '',
        ...UtilsStyle.transition()
    }),
    line: ({active, ...props}) => ({
        width: 8,
        borderRadius: 5,
        backgroundColor: active ? red[400] : grey[400],
        WebkitBoxShadow: active ? `0 0 10px 0 ${red[500]}` : '',
        boxShadow: active ? `0 0 10px 0 ${red[500]}` : '',
        ...UtilsStyle.transition(700)
    }),
    percent: {},
    input: {
        width: 40,
        border: `1px solid ${grey[400]}`,
        padding: '0 !important',
        '& input': {
            textAlign: 'center'
        }
    },
    inputButton: {
        borderColor: grey[400],
        color: grey[600],
        backgroundColor: grey[300],
        minWidth: 'unset',
        padding: '0 5px'
    }
}));

function StoragesItem({storage, active = false, onStorageCountChange, onChangeSelectedItem, ...props}) {
    const classes = storagesItemStyle({active, ...props});
    return (
        <Box component={Card} display='flex' my={1} mx={2} p={2}
             className={classes.root}
             elevation={2}
             onClick={() => onChangeSelectedItem(storage_id)}>
            <Box flex={1} display='flex' flexDirection='column'>
                <Box fontWeight={400} fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 1)}
                     lineHeight={1.8}>
                    {storage.title}
                </Box>
                <Box display='flex' alignItems='center'
                     justifyContent='center'
                     flexWrap={'wrap-reverse'} py={1}>
                    <Box
                        flex={1}>
                        <Box
                            px={1}
                            py={0.5}
                            my={1}
                            fontWeight={500}
                            fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 1)}
                            style={{
                                background: red[500],
                                color: '#fff',
                                ...UtilsStyle.borderRadius(4),
                                ...UtilsStyle.widthFitContent(),
                            }}>
                            <Box component='span'
                                 pr={0.1}
                                 fontSize={UtilsConverter.addRem(theme.typography.body2.fontSize, 0.8)}>
                                %
                            </Box>
                            {storage.discountPercent}
                        </Box>
                    </Box>
                    <Box
                        mx={1}
                        fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.95)}
                        style={{
                            textDecoration: 'line-through',
                            color: grey[600]
                        }}>
                        {UtilsFormat.numberToMoney(storage.finalPrice)}
                    </Box>
                    <Box display='flex'
                         justifyContent='flex-end'
                         alignItems='baseline'
                         pl={1}
                         pr={2}
                         fontWeight={400}
                         fontSize={UtilsConverter.addRem(theme.typography.h5.fontSize, 1)}
                         style={{
                             color: colors.success.main
                         }}>
                        {UtilsFormat.numberToMoney(storage.discountPrice)}
                        <Box pr={0.4} fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.8)}>
                            {lang.get("toman")}
                        </Box>
                    </Box>
                </Box>
                <Collapse in={active}>
                    <Box mt={1}>
                        <Divider/>
                        <Box mt={2} display={'flex'} justifyContent={'center'} flexWrap={'wrap-reverse'}>
                            <Box display={'flex'} alignItems={'center'} my={1}>
                                <Box flex={1} display={'flex'} alignItems='center'>
                                    <Gem mx={1}/>
                                    {lang.get('deadline')}:
                                </Box>
                                <Box mr={1}>
                                    {storage.jDeadline}
                                </Box>
                            </Box>
                            <Box flex={1} display={'flex'} mr={2} my={1} justifyContent={'center'}>
                                {
                                    storage.maxCountForSale > 1 ?
                                        <ButtonGroup size="small" color="primary" dir={'ltr'}
                                                     aria-label="outlined primary button group">
                                            <Button className={classes.inputButton}
                                                    onClick={() => onStorageCountChange({
                                                        id: storage_id,
                                                        count: (storage.count - 1)
                                                    })}>
                                                <Remove fontSize="small"/>
                                            </Button>
                                            <BaseTextField
                                                className={classes.input}
                                                value={_.toString(storage.count)}
                                                optionalLabel=''
                                                type={'number'}
                                                onTextChange={(val) => {
                                                    onStorageCountChange({
                                                        id: storage_id,
                                                        count: val
                                                    })
                                                }}
                                                onTextFocusChange={(val) => {
                                                    if (!storage.count)
                                                        onStorageCountChange({
                                                            id: storage_id,
                                                            count: 0
                                                        })
                                                }}
                                                variant={"standardWhitOutButtonLine"}/>
                                            <Button className={classes.inputButton}
                                                    onClick={() => onStorageCountChange({
                                                        id: storage_id,
                                                        count: (storage.count + 1)
                                                    })}>
                                                <Add fontSize="small"/>
                                            </Button>
                                        </ButtonGroup>
                                        :
                                        <Box fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.8)}>
                                            {lang.get("max_count_order_one")}
                                        </Box>
                                }
                            </Box>
                        </Box>
                        <Box display={'flex'}
                             justifyContent={'center'}
                             flexDirection={'row-reverse'}
                             mt={1} px={1} py={1.5}>
                            <SuccessButton style={{
                                paddingTop: theme.spacing(1.2),
                                paddingBottom: theme.spacing(1.2),
                                marginRight: theme.spacing(0.5)
                            }}>
                                <ShoppingCart/>
                                <Box mr={1}>
                                    {lang.get("order_now")}
                                </Box>
                            </SuccessButton>
                            <Tooltip title={lang.get("add_to_basket")} placement="top"
                                     aria-label={lang.get("add_to_basket")}>
                                <Box>
                                    <BaseButton style={{
                                        color: '#fff',
                                        backgroundColor: amber[700],
                                        paddingTop: theme.spacing(1.2),
                                        paddingBottom: theme.spacing(1.2),
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                                        <AddShoppingCart/>
                                    </BaseButton>
                                </Box>
                            </Tooltip>
                        </Box>
                    </Box>
                </Collapse>
            </Box>
            <Box className={classes.line} component={'span'} mr={2}/>
        </Box>
    )
}

//endregion Storages
//endregion Component
