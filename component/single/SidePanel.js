import React, {useState} from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "../base/Typography";
import Storage2 from "./Storage2";
import {DEBUG} from "../../pages/_app";
import ErrorBoundary from "../base/ErrorBoundary";
import DeadlineTimer from "../productV2/storagePanel/DeadlineTimer";
import RatingComponent from "../base/RatingComponent";
import {Box} from "material-ui-helper";
import Share from "../productV2/storagePanel/Share";
import Wish from "../base/Wish";

export default function SidePanel({permalink, wish, notify, productId, boxName, name, item, deadline, storageId, rate, onRatingChange, ...props}) {

    const [activeStorage, setActiveStorage] = useState();
    const [bookingMode, setBookingMode] = useState(false)

    const percent = activeStorage ? activeStorage.discount_percent : 0;

    return (
        <Paper>
            <Box flexDirectionColumn={true}>
                <ErrorBoundary>
                    {
                        !bookingMode &&
                        <Box display={'flex'} px={3} flexDirection={'column'}>
                            <DeadlineTimer deadline={DEBUG ? (new Date().getTime() * 1.1 / 1000) : deadline}/>
                            <Box display={'flex'} pt={1}>
                                {
                                    DEBUG &&
                                    <Share/>
                                }
                                <Wish mr={2}  productId={productId} permalink={permalink} wish={wish}/>
                                <RatingComponent rate={rate} size={"small"} onRatingChange={onRatingChange}/>
                            </Box>
                        </Box>
                    }
                </ErrorBoundary>
                <Box width={1} mt={!bookingMode ? 2 : 0} flexDirectionColumn={true}>
                    <ErrorBoundary>
                        {
                            !bookingMode &&
                            <Box display={'flex'} flexDirection={'column'} px={3}>
                                <Typography variant={"h6"} fontWeight={500}>
                                    {name}
                                </Typography>
                                <Typography variant={"body2"} fontWeight={400} pt={0.5}>
                                    {boxName}
                                </Typography>
                            </Box>
                        }
                    </ErrorBoundary>
                    <ErrorBoundary>
                        <Storage2
                            storageId={storageId}
                            permalink={permalink}
                            onBooking={(bookingMode) => {
                                setBookingMode(bookingMode);
                            }}
                            onChangeStorage={(s) => {
                                setActiveStorage(s)
                            }}
                            pt={!bookingMode ? 4 : 2} px={3} pb={2}/>
                    </ErrorBoundary>
                </Box>
            </Box>
        </Paper>
    )
}


//endregion Component
