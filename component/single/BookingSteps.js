import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import {
    AccountCircle,
    ArrowForward,
    Close,
    DateRange,
    DateRangeOutlined,
    LocationOn,
    Phone,
    QueryBuilder,
    RadioButtonChecked,
    RadioButtonUnchecked,
    Redeem
} from "@material-ui/icons";
import Typography from "../base/Typography";
import {cyan, grey} from "@material-ui/core/colors";
import {UtilsStyle} from "../../utils/Utils";
import DatePicker, {convertDate} from "../base/DatePicker";
import JDate from "../../utils/JDate";
import _ from "lodash"
import TimePicker from "../base/TimePicker";
import ControllerUser from "../../controller/ControllerUser";
import useSWR, {mutate} from "swr";
import {createTheme, useTheme} from "@material-ui/core";
import BaseButton from "../base/button/BaseButton";
import ThreeDots from "../base/loading/treeDots/ThreeDots";
import AddAddress from "../../pages/add-address";
import Dialog from "@material-ui/core/Dialog";
import Backdrop from "@material-ui/core/Backdrop";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import {lang} from "../../repository";
import momentJalaali from "moment-jalaali";
import FormController from "../base/formController/NewFormController";
import DefaultTextField from "../base/textField/DefaultTextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import LinearProgress from "@material-ui/core/LinearProgress";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import IconButton from "@material-ui/core/IconButton";
import Img from "../base/oldImg/Img";
import {SmHidden} from "../header/Header";
import EmailTextField from "../base/textField/EmailTextField";
import MeliCodeTextField from "../base/textField/MeliCodeTextField";
import {useRouter} from "next/router";
import rout from "../../router";
import {update} from "../../middleware/auth"
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useUserContext} from "../../context/UserContext";


const linearProgressTheme = createTheme({
    palette: {
        primary: {
            main: cyan[400],
        },
    },
});


const maxStep = 3
export default function BookingSteps({storage,count=1, onCancel, ...props}) {
    const theme = useTheme();
    const ref = useRef()
    const router = useRouter();
    const userRef = useRef()
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const isLogin = useIsLoginContext()
    const [user] = useUserContext();
    const d = ControllerUser.User.Profile.Addresses.get({all: true});
    const {data: addressData, error} = useSWR(...d);
    const {least_booking_time} = storage
    const [date, setDate] = useState({
        date: undefined,
        moment: undefined
    })
    const [hour, setHour] = useState()
    const [openConfirm, setOpenConfirm] = useState()

    const [step, setStep] = useState(1)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (!isLogin)
            router.push(rout.User.Auth.Login.create({redirectRout: window.location.pathname}));
    }, [])

    const getRenderDate = () => {
        try {
            if (date.moment)
                return JDate.format(date.moment, "jDD jMMMM jYYYY")
        } catch (e) {
        }
        return "انتخاب تاریخ دریافت"
    }

    const handleStepChange = (st, disableCartPostal, retry = 0) => {
        if (st === 0) {
            onCancel()
            return
        }

        if (step === 2 && !user.isActiveData) {
            try {
                if (userRef.current.hasError()) {
                    errorSnack("لطفا اطلاعات فرستنده را کامل کنید.")
                    return;
                }
                const {firstName, lastName, email, meliCode} = userRef.current.serialize();
                setLoading(true)
                update( {firstName, lastName, email, meliCode}).then(res => {
                    setTimeout(() => {
                        handleStepChange(st, disableCartPostal)
                    }, 1000)
                }).finally(() => {
                    setLoading(false)
                })
            } catch (e) {
                return;
                if (retry <= 4) {
                    if (retry > 3) {
                        // errorSnack("مشکلی پیش آمده است لطفا مجدد تلاش فرمایید.")
                        return;
                    }
                }
            }
        }

        if (step === maxStep && st >= maxStep) {
            try {
                const {defaultAddress, data: addresses} = addressData
                if (!defaultAddress) {
                    throw"";
                }
                const {day, month, year} = date.date;
                const start = hour.start.value;
                const {sender, text} = ref.current.serialize()

                setLoading(true)
                const pr = (!disableCartPostal && (sender || text)) ? {sender, text} : undefined;

                ControllerUser.Basket.Booking.Datetime.add({
                    storageId: storage.id,
                    date: momentJalaali(`${year}/${month}/${day} ${start}:00`, "jYYYY/jM/jD HH:mm").unix(),
                    cart_postal_text: pr,
                    count:count
                }).then(res => {
                    setStep(maxStep + 1)
                    setOpenConfirm(res.data)
                }).finally(() => {
                    setLoading(false)
                })
                return;
            } catch (e) {
                errorSnack("آدرس را درست وارد کنید.")
                return;
            }
        }
        if (step === maxStep + 1 && st <= maxStep) {
            setOpenConfirm(false)
            setStep(st)
            return;
        }
        setStep(st)
    }

    function errorSnack(text) {
        enqueueSnackbar(text,
            {
                variant: "error",
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }

    const disabled = (() => {
        if (step === 1) {
            return !(date && date.date && hour)
        }
        if (step === 2) {
            try {
                const {defaultAddress} = addressData || {}
                return !Boolean(defaultAddress)
            } catch (e) {
                return false
            }
        }
        return step >= maxStep + 1
    })()

    function handleNextButtonClick(disableCartPostal = false) {
        const s = step + 1
        if (step === maxStep) {
            setLoading(true)
            handleStepChange(s, disableCartPostal)
            return
        }
        handleStepChange(s)
    }

    return (
        <Box display={'flex'} flexDirection={'column'}  {...props}>
            <Box display={'flex'} alignItems={'center'}>
                <ButtonBase
                    onClick={() => {
                        handleStepChange(step - 1)
                    }}>
                    <ArrowForward/>
                    <Typography pr={0.5} variant={"body2"}>
                        بازگشت
                    </Typography>
                </ButtonBase>
            </Box>
            <Box width={1} pt={2}>
                <ThemeProvider
                    theme={linearProgressTheme}>
                    <LinearProgress
                        value={_.toNumber(((step) / (maxStep + 1)) * 100)}
                        variant="determinate"
                        style={{
                            transform: 'rotate(180deg)'
                        }}/>
                </ThemeProvider>
            </Box>
            <Typography pt={2} pr={0.5} variant={"body2"} fontWeight={500}>
                ✱
                در حال حاضر ارسال فقط در استان گیلان امکان پذیر می‌باشد
            </Typography>
            {
                step === 1 &&
                <Box display={'flex'} pt={3} flexDirection={'column'} pb={3}>
                    <Typography variant={"body1"} fontWeight={500}>
                        لطفا زمان دریافت را انتخاب کنید
                    </Typography>
                    <Box pt={2.2} display={'flex'} flexDirection={'column'}>
                        <Typography pr={1} pb={1} variant={"body1"} fontWeight={500}>
                            تاریخ
                        </Typography>
                        <DatePicker
                            onChange={(date, moment) => setDate({date, moment})}
                            date={date.date}
                            minimumDate={(() => {
                                const d = JDate.timeStampToJalali().add(least_booking_time + 24, 'hours');
                                return convertDate(
                                    _.toNumber(d.format('jYYYY')),
                                    _.toNumber(d.format('jM')),
                                    _.toNumber(d.format('jDD'))
                                )
                            })()}
                            renderComponent={({onClick, selectedDay, ...props}) => (
                                <ButtonBase onClick={onClick} style={{width: '100%'}}>
                                    <Box
                                        width={1}
                                        p={1}
                                        display={'flex'}
                                        alignItems={'center'}
                                        style={{
                                            border: `1px solid ${grey[400]}`,
                                            ...UtilsStyle.borderRadius(5)
                                        }}>
                                        <Box display={'flex'} component={"span"} pr={1}>
                                            <DateRangeOutlined
                                                style={{
                                                    color: grey[500]
                                                }}/>
                                        </Box>
                                        <Typography variant={"h6"} fontWeight={400}>
                                            {getRenderDate()}
                                        </Typography>
                                    </Box>
                                </ButtonBase>
                            )}/>
                    </Box>
                    <Box pt={2.2} display={'flex'} flexDirection={'column'}>
                        <Typography pr={1} pb={1} variant={"body1"} fontWeight={500}>
                            ساعت
                        </Typography>
                        <TimePicker
                            onChange={(hour) => setHour(hour)}
                            date={hour}
                            renderComponent={({onClick, selectedDay, ...props}) => (
                                <ButtonBase onClick={onClick} style={{width: '100%'}}>
                                    <Box
                                        width={1}
                                        p={1}
                                        display={'flex'}
                                        alignItems={'center'}
                                        style={{
                                            border: `1px solid ${grey[400]}`,
                                            ...UtilsStyle.borderRadius(5)
                                        }}>
                                        <Box display={'flex'} component={"span"} pr={1}>
                                            <QueryBuilder
                                                style={{
                                                    color: grey[500]
                                                }}/>
                                        </Box>
                                        <Typography variant={"h6"} fontWeight={400}>
                                            {hour ? hour.component({py: 0}) : "انتخاب ساعت دریافت"}
                                        </Typography>
                                    </Box>
                                </ButtonBase>
                            )}/>
                    </Box>
                </Box>
            }
            {
                step === 2 &&
                <Address
                    innerref={userRef}
                    data={addressData}
                    onAddAddress={(addressId) => {
                        mutate(d[0])
                    }}
                    storage={storage}/>
            }
            <FormController innerref={ref}>
                <CartPostal display={(step === 3 || step === maxStep + 1) ? "flex" : "none"}/>
            </FormController>
            <Box display={'flex'} justifyContent={'flex-end'}>
                {
                    (step === 3 || step === maxStep + 1) &&
                    <Box pr={2}>
                        <BaseButton
                            variant={"outlined"}
                            disabled={disabled}
                            onClick={() => {
                                handleNextButtonClick(true)
                            }}>
                            <Typography variant={"h6"}>
                                کارت پستال نمیخواهم
                            </Typography>
                        </BaseButton>
                    </Box>
                }
                <BaseButton
                    disabled={disabled}
                    onClick={() => {
                        handleNextButtonClick()
                    }}
                    style={{
                        backgroundColor: disabled ? grey[400] : "#FF527B"
                    }}>
                    <Typography variant={"h6"} color={disabled ? grey[600] : '#fff'}>
                        مرحله بعد
                    </Typography>
                </BaseButton>
            </Box>
            <ConfirmDialog openConfirm={openConfirm} onClose={() => {
                handleStepChange(step - 1)
            }}/>
            <Backdrop open={loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: theme.palette.primary.main,
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Box>
    )
}


function Address({innerref, data, onAddAddress, storage, ...props}) {
    const theme = useTheme();
    const {defaultAddress, data: addresses} = data || {}
    const [addAddress, setAddAddress] = useState(false)
    const [user] = useUserContext();
    const {id, isActiveData, lastName, firstName, email, meliCode} = user || {};
    const [loading, setLoading] = useState(false)

    const TextFieldContainer = (props) => (
        <Box mb={3}>
            {props.children}
        </Box>
    )

    return (
        <Box display={'flex'} pt={3} flexDirection={'column'} mb={2} {...props}>
            {
                (!isActiveData) &&
                <FormController
                    name={"user_details"}
                    display={'flex'}
                    flexDirection={'column'}
                    py={1}
                    innerref={innerref}>
                    <Typography variant={"h6"} pb={2} fontWeight={500}>
                        تکمیل اطلاعات فرستنده:
                    </Typography>
                    <TextFieldContainer>
                        <DefaultTextField
                            required={true}
                            name={"firstName"}
                            variant={"outlined"}
                            label={"نام"}
                            placeholder={"مانند: سهیل"}
                            defaultValue={firstName}/>
                    </TextFieldContainer>
                    <TextFieldContainer>
                        <DefaultTextField
                            required={true}
                            label={"نام خانوادگی"}
                            name={"lastName"}
                            variant={"outlined"}
                            placeholder={"مانند: رواسانی"}
                            defaultValue={lastName}/>
                    </TextFieldContainer>
                    <TextFieldContainer>
                        <MeliCodeTextField
                            required={true}
                            label={"کدملی"}
                            name={"meliCode"}
                            variant={"outlined"}
                            placeholder={"کدملی"}
                            defaultValue={meliCode}/>
                    </TextFieldContainer>
                    <TextFieldContainer>
                        <EmailTextField
                            required={false}
                            label={"پست الکترونیکی"}
                            name={"email"}
                            variant={"outlined"}
                            placeholder={"مانند: example@info.com"}
                            defaultValue={email}/>
                    </TextFieldContainer>
                </FormController>
            }
            <ButtonBase
                disabled={!Boolean(data)}
                onClick={() => {
                    setAddAddress(true)
                }}
                style={{
                    width: '100%',
                    ...UtilsStyle.borderRadius(5)
                }}>
                <Typography
                    py={1}
                    width={1}
                    textAlign={"center"}
                    variant={"subtitle1"}
                    alignItems={"center"}
                    justifyContent={"center"}
                    fontWeight={400}
                    style={{
                        border: `1px solid ${grey[400]}`,
                        ...UtilsStyle.borderRadius(5)
                    }}>
                    <Typography pl={1} variant={"h6"} fontWeight={400}>
                        +
                    </Typography>
                    افزودن آدرس جدید
                </Typography>
            </ButtonBase>
            {
                data ?
                    <Box display={'flex'} flexDirection={'column'}>
                        {
                            addresses.map(ad => {
                                const active = ad.id === defaultAddress.id
                                return (
                                    <Box
                                        mt={2}
                                        alignItems={"center"}
                                        display={'flex'}
                                        onClick={() => {
                                            setLoading(true)
                                            ControllerUser.User.Profile.Addresses.setDefault({address_id: ad.id}).then(res => {
                                                onAddAddress()
                                            }).finally(() => {
                                                setLoading(false)
                                            })
                                        }}
                                        style={{
                                            cursor: "pointer"
                                        }}>
                                        {
                                            active ?
                                                <RadioButtonChecked style={{color: cyan[500]}}/> :
                                                <RadioButtonUnchecked style={{color: grey[500]}}/>
                                        }
                                        <Box pl={1} flex={1}>
                                            <ButtonBase
                                                style={{
                                                    width: '100%',
                                                    ...UtilsStyle.borderRadius(5)
                                                }}>
                                                <Box width={1} display={'flex'} flexDirection={'column'} p={1}
                                                     style={{
                                                         border: `1px solid ${active ? cyan[500] : grey[400]}`,
                                                         ...UtilsStyle.borderRadius(5)
                                                     }}>
                                                    <Typography variant={"body2"} fontWeight={500}>
                                                        {ad.fullAddress}
                                                    </Typography>
                                                    <Box display={'flex'} width={1} pt={2}>
                                                        <Typography width={"25%"} variant={"body2"} color={grey[500]}>
                                                            شماره تماس
                                                        </Typography>
                                                        <Typography width={"75%"} variant={"body1"} fontWeight={500}>
                                                            {ad.phone}
                                                        </Typography>
                                                    </Box>
                                                    <Box display={'flex'} width={1} alignItems={'center'} pt={2}>
                                                        <Typography width={"25%"} variant={"body2"} color={grey[500]}>
                                                            گیرنده
                                                        </Typography>
                                                        <Typography width={"75%"} variant={"body1"} fontWeight={400}>
                                                            {ad.name}
                                                        </Typography>
                                                    </Box>
                                                    <Box display={'flex'} alignItems={'center'}
                                                         justifyContent={'flex-end'}>
                                                        <ButtonBase
                                                            onClick={(e) => {
                                                                e.stopPropagation()
                                                                setAddAddress(ad)
                                                            }}
                                                            style={{...UtilsStyle.borderRadius(5)}}>
                                                            <Typography p={0.5} variant={"caption"} color={cyan[600]}>
                                                                ویرایش
                                                            </Typography>
                                                        </ButtonBase>
                                                        {
                                                            false &&
                                                            <React.Fragment>
                                                                <Box component={"span"}
                                                                     mx={0.5}
                                                                     height={17}
                                                                     style={{
                                                                         borderRight: `1px solid ${grey[300]}`
                                                                     }}/>
                                                                <ButtonBase
                                                                    onClick={(e) => {
                                                                        e.stopPropagation()
                                                                    }}
                                                                    style={{...UtilsStyle.borderRadius(5)}}>
                                                                    <Typography p={0.5} variant={"caption"}
                                                                                color={cyan[600]}>
                                                                        حذف
                                                                    </Typography>
                                                                </ButtonBase>
                                                            </React.Fragment>
                                                        }
                                                    </Box>
                                                </Box>
                                            </ButtonBase>
                                        </Box>
                                    </Box>
                                )
                            })
                        }
                    </Box> :
                    <Box display={'flex'} width={1} justifyContent={'center'} py={5}>
                        <ThreeDots justifyContent={'center'}/>
                    </Box>
            }
            <Dialog
                maxWidth={"xl"}
                onClose={() => {
                    setAddAddress(false)
                }}
                open={Boolean(addAddress)}>
                <Box pb={3}>
                    <AddAddress
                        onClose={() => {
                            setAddAddress(false)
                        }}
                        address={_.isObject(addAddress) ? addAddress : undefined}
                        directBack={false}
                        onFinish={(address) => {
                            setAddAddress(false)
                            onAddAddress()
                        }}/>
                </Box>
            </Dialog>
            <Backdrop open={loading} style={{zIndex: 99999999}}/>
        </Box>
    )
}

function CartPostal({...props}) {

    return (
        <Box display={'flex'} pt={3} flexDirection={'column'} mb={2} {...props}>
            <Typography variant={"h6"} alignItems={"center"} fontWeight={400}>
                ارسال کارت پستال
                <Typography pr={0.4} variant={"body2"} color={grey[600]}>
                    (اختیاری)
                </Typography>
            </Typography>
            <Box display={'flex'} flexDirection={'column'} pt={3}>
                <TextField name={"sender"} label={"نام فرستنده"} placeholder={"نمونه: از طرف دایی میلاد"}/>
                <TextField name={"text"} pt={1.5} label={"متن روی کارت"}
                           placeholder={"نمونه: با آرزوی بهترین‌ها برای سهیلا جانم"}
                           multiline={true}/>
            </Box>
        </Box>
    )
}

function TextField({name, label, placeholder, multiline, ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} {...props}>
            <Typography pr={1} pb={1} fontWeight={400} variant={"body1"}>
                {label}
            </Typography>
            <DefaultTextField
                name={name}
                variant={'outlined'}
                multiline={multiline}
                rows={multiline ? 4 : undefined}
                placeholder={placeholder}/>
        </Box>
    )
}

function ConfirmDialog({openConfirm, onClose}) {


    return (
        <Dialog maxWidth={"lg"}
                fullWidth={true}
                open={Boolean(openConfirm)}>
            {
                openConfirm &&
                <Box display={'flex'} width={1} flexDirection={'column'} pt={1} px={2} pb={2}>
                    <Box display={'flex'} alignItems={'center'} width={1}
                         style={{
                             position: 'sticky',
                             top: 0,
                             zIndex: 99999,
                             backgroundColor: "#fff"
                         }}>
                        <Typography variant={"h6"} fontWeight={600} pr={2}
                                    style={{
                                        flex: 1
                                    }}>
                            بازبینی نهایی اطلاعات سفارش انتخابی شما
                        </Typography>
                        <IconButton
                            onClick={() => {
                                onClose()
                            }}>
                            <Close fontSize={"large"}/>
                        </IconButton>
                    </Box>
                    <Box display={'flex'} mt={2}>
                        <SmHidden>
                            <Box display={'flex'} width={'60%'} alignItems={'center'} pr={3}>
                                <Img src={openConfirm.thumbnail.image}/>
                            </Box>
                        </SmHidden>
                        <Box display={'flex'} justifyContent={'center'} flexDirection={'column'} flex={1}>
                            <DialogItem icon={<AccountCircle style={{color: grey[700]}}/>}
                                        label={"نام تحویل گیرنده"}
                                        text={openConfirm.address.name}/>
                            <DialogItem icon={<Phone style={{color: grey[700]}}/>}
                                        label={"شماره تماس تحویل گیرننده"}
                                        text={openConfirm.address.phone}/>
                            <DialogItem icon={<LocationOn style={{color: grey[700]}}/>}
                                        label={"نشانی تحویل گیرنده"}
                                        text={openConfirm.address.fullAddress}/>
                            <DialogItem icon={<DateRange style={{color: grey[700]}}/>}
                                        label={"تاریخ تحویل"}
                                        text={(() => {
                                            const d = JDate.timeStampToJalali(openConfirm.start_date)
                                            const date = JDate.format(d, "jD jMMMM jYYYY")
                                            const hour = _.toInteger(JDate.format(d, "HH"))
                                            return (
                                                <React.Fragment>
                                                    {date}
                                                    <Box mx={1} style={{borderRight: `2px solid ${grey[500]}`}}/>
                                                    {hour}:00
                                                    {" تا "}
                                                    {hour + 1}:00
                                                </React.Fragment>
                                            )
                                        })()}/>
                            <DialogItem icon={<Redeem style={{color: grey[700]}}/>}
                                        label={"کارت پستال"}
                                        text={_.isEmpty(openConfirm.cart_postal) ? "ندارد" : "دارد"}/>

                            <Box display={'flex'}>
                                <Box width={'80%'} pr={1}>
                                    <BaseButton
                                        onClick={() => {
                                            window.open(openConfirm.invoice, "_self")
                                        }}
                                        style={{
                                            width: '100%',
                                            backgroundColor: '#FF527B'
                                        }}>
                                        <Typography variant={"h6"} color={"#fff"}>
                                            پرداخت
                                        </Typography>
                                    </BaseButton>
                                </Box>
                                <Box>
                                </Box>
                            </Box>
                        </Box>
                    </Box>

                </Box>
            }
        </Dialog>
    )
}

function DialogItem({icon, label, text, ...props}) {

    return (
        <Box display={'flex'} pb={3.5} flexDirection={'column'} {...props}>
            <Box display={'flex'} alignItems={'center'}>
                {icon}
                <Typography pr={0.5} variant={"body1"} fontWeight={400} color={grey[600]}>
                    {label}
                </Typography>
            </Box>
            <Typography pt={1.2} variant={"subtitle1"} fontWeight={500}>
                {text}
            </Typography>
        </Box>
    )
}
