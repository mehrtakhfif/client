import React, {Fragment, useCallback, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {makeStyles, withWidth} from "@material-ui/core";
import {UtilsConverter, UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {amber, cyan, green, grey, red} from "@material-ui/core/colors";
import {useSnackbar} from "notistack";
import {colors, GA, lang, theme} from "../../repository";
import rout from "../../router";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import Gem from "../base/icon/Gem";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import {Add, AddShoppingCart, ExpandMore, LocalShippingTwoTone, Remove, ShoppingCartOutlined} from "@material-ui/icons";
import BaseTextField from "../base/textField/base/BaseTextField";
import _ from "lodash";
import SuccessButton from "../base/button/buttonVariant/SuccessButton";
import BaseButton from "../base/button/BaseButton";
import {isWidthDown} from "@material-ui/core/withWidth";
import Typography from "../base/Typography";
import Switch from "@material-ui/core/Switch";
import Radio from "@material-ui/core/Radio";
import Checkbox from "@material-ui/core/Checkbox";
import {addProduct} from "../../middleware/shopping";
import ButtonLink from "../base/link/ButtonLink";
import {useRouter} from "next/router";
import Tooltip from "../base/Tooltip";

function Storages({pid, storages, storagesData, onStorageCountChange, onFeatureChange, ...props}) {
    const [state, setState] = useState({
        showAll: false
    });
    const [activeStorage, setActiveStorage] = useState(storages[0].storage_id);
    const count = isWidthDown('sm', props.width) ? 3 : 4;
    const storages1 = storages.slice(0, isWidthDown('md', props.width) ? count : storages.length);
    const storages2 = isWidthDown('md', props.width) ? storages.slice(count, storages.length) : [];

    const onChangeSelectedItem = useCallback((storage_id) => {
        setActiveStorage(storage_id)
    }, [setActiveStorage]);

    return (
        <>
            <Box display={'flex'}
                 flexDirection={{
                     xs: 'column',
                     md: 'row',
                     lg: 'column'
                 }}
                 flexWrap={'wrap'}
                 mx={{
                     xs: 0,
                     lg: 2
                 }}>
                {storages1.map((storage, index) => {
                    return (
                        <StoragesItem key={index}
                                      selectable={storages1.length > 1}
                                      storage={storage}
                                      active={storage.storage_id === activeStorage}
                                      onChangeSelectedItem={onChangeSelectedItem}
                                      index={index}/>
                    )
                })}
            </Box>
            <Collapse in={state.showAll}>
                <Box display={'flex'}
                     flexDirection={{
                         xs: 'column',
                         md: 'row',
                         lg: 'column'
                     }}
                     flexWrap={'wrap'}
                     mb={1}
                     mx={{
                         xs: 0,
                         lg: 2
                     }}>
                    {storages2.map((storage, index) => (
                        <StoragesItem
                            key={index}
                            selectable={storages1.length > 1}
                            storage={storage}
                            onStorageCountChange={onStorageCountChange}
                            storageData={storagesData[index + count]}
                            active={storage.storage_id === activeStorage}
                            onChangeSelectedItem={onChangeSelectedItem}
                            onFeatureChange={onFeatureChange}
                            index={index}/>
                    ))}
                </Box>
            </Collapse>
            {(!state.showAll && isWidthDown('md', props.width) && !_.isEmpty(storages2)) &&
            <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}
                 mt={1}
                 mb={1.5}>
                <BaseButton variant={"outlined"} style={{borderColor: cyan[600]}}>
                    <Typography
                        display={'flex'}
                        variant={'h6'}
                        justifyContent={'center'}
                        onClick={() => {
                            setState({
                                ...state,
                                showAll: !state.showAll
                            })
                        }}>
                        {lang.get("show_more")}
                        <ExpandMore/>
                    </Typography>
                </BaseButton>
            </Box>}
        </>
    )
}

export default withWidth()(Storages)
const storagesItemStyle = makeStyles((theme) => ({
    storagesItemRoot: ({active, ...props}) => ({
        cursor: active ? 'default' : 'pointer',
        ...UtilsStyle.transition()
    }),
    storagesItemLine: ({active, ...props}) => ({
        width: 8,
        borderRadius: 5,
        backgroundColor: active ? green[300] : grey[400],
        ...UtilsStyle.transition(700)
    }),
    percent: {},
    input: {
        width: 40,
        border: `1px solid ${grey[400]}`,
        padding: '0 !important',
        '& div[class*="MuiInputBase-root"]': {
            margin: '0 !important'
        },
        '& input': {
            textAlign: 'center'
        }
    },
    inputButton: {
        borderColor: grey[400],
        color: grey[600],
        backgroundColor: grey[300],
        minWidth: 'unset',
        padding: '0 5px'
    }
}));


const StoragesItem = React.memo(function StoragesItem({storage, active = false, onChangeSelectedItem, selectable, index, ...props}) {
    const router = useRouter();
    const classes = storagesItemStyle({active, ...props});
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();


    const [state, setState] = useState({
        goToBasket: false
    });
    const [setting, setSetting] = useState({
        disable: false
    });
    const [storageData, setStorageData] = useState({
        storage_id: storage.storage_id,
        count: 1,
        addTryFailed: 0,
        features: storage.features,
    });

    function onStorageCountChange({count}) {
        if (count > storage.maxCountForSale)
            count = storage.maxCountForSale;
        setStorageData({...storageData, count: count})
    }


    function onFeatureChange({feature_id, activeValue, ...props}) {
        try {
            const sd = storageData;
            const ftIndex = _.findIndex(sd.features, (f) => {
                return f.id === feature_id
            });
            if (ftIndex === -1) {
                return;
            }

            sd.features[ftIndex].activeValue = activeValue;
            setStorageData(_.cloneDeep(sd))
        } catch (e) {
        }
    }

    function addToBasket(goToBasket) {

        setSetting({
            ...setting,
            disable: true
        });

        setTimeout(() => {
            const fe = [];
            _.forEach(storageData.features, (f) => {
                if (f.isBool) {
                    if (f.activeValue) {
                        fe.push({
                            fid: f.fid,
                            fsid: f.id,
                            fvid: [f.value[1].id]
                        })
                    }
                    return
                }
                if (f.isSingle) {
                    const fi = _.find(f.value, (v) => {
                        return v.id === f.activeValue
                    });
                    if (fi.price > 0) {
                        fe.push({
                            fid: f.fid,
                            fsid: f.id,
                            fvid: [fi.id]
                        })
                    }
                    return
                }
                if (f.isMulti) {
                    if (!_.isEmpty(f.activeValue)) {
                        fe.push({
                            fid: f.fid,
                            fsid: f.id,
                            fvid: f.activeValue
                        });
                    }
                }
            });


            addProduct( {storage_id: storage.storage_id, count: storageData.count, features: fe}).then(res => {
                setSetting({
                    ...setting,
                    disable: false
                });
                if (goToBasket) {
                    router.push(rout.User.Basket.rout);
                    return
                }
                enqueueSnackbar(lang.get("me_product_add_to_basket_successfully"),
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <ButtonLink
                                    href={rout.User.Basket.rout}
                                    variant={'body1'}
                                    style={{
                                        color: colors.success.main,
                                        backgroundColor: '#fff',
                                        ...UtilsStyle.borderRadius(5)
                                    }}
                                    onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                    {lang.get('basket')}
                                </ButtonLink>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                GA.User.Basket.addToBasket({storage_id: storage.storage_id})
            }).catch((e) => {
                setSetting({
                    ...setting,
                    disable: false
                });
                enqueueSnackbar(lang.get("er_product_add_to_basket_fail"),
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                GA.User.Basket.addToBasket({fail: true, storage_id: storage.storage_id})
            })
        }, 1500)
    }

    return (
        <Box
            width={{
                xs: 1,
                md: '50%',
                lg: 1
            }}
            pr={{
                xs: 0,
                md: index % 2 !== 0 ? 0 : 1,
                lg: 0
            }}
            pl={{
                xs: 0,
                md: index % 2 !== 0 ? 1 : 0,
                lg: 0
            }}>
            <Box component={Card} display='flex' my={1} p={2}
                 className={classes.storagesItemRoot}
                 elevation={2}
                 order={storage.priority}
                 onClick={() => {
                     onChangeSelectedItem(storage.storage_id)
                 }}>
                <Box flex={1} display='flex' flexDirection='column'>
                    <Box fontWeight={400} fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 1)}
                         lineHeight={1.8}>
                        {storage.storageTitle}
                    </Box>
                    {(storage.maxCountForSale && storage.maxCountForSale > 0) ?
                        <React.Fragment>
                            <Box display='flex' alignItems='center'
                                 justifyContent='flex-end'
                                 flexWrap={'wrap-reverse'} py={1}>
                                {storage.discountPercent &&
                                <Box
                                    flex={1}>
                                    <Box
                                        px={1}
                                        py={0.5}
                                        my={1}
                                        fontWeight={500}
                                        style={{
                                            background: red[500],
                                            color: '#fff',
                                            ...UtilsStyle.borderRadius(4),
                                            ...UtilsStyle.widthFitContent(),
                                        }}>
                                        <Tooltip title={lang.get("your_profits_from_buying")} placement="top"
                                                 aria-label={lang.get("your_profits_from_buying")}>
                                            <Box>
                                                <Typography variant={"body1"} color={'#fff'}
                                                            textStyle={{direction: 'ltr'}}>
                                                    {UtilsFormat.numberToMoney(storage.finalPrice - storage.discountPrice)} -
                                                </Typography>
                                            </Box>
                                        </Tooltip>
                                    </Box>
                                </Box>
                                }
                                {
                                    storage.finalPrice > storage.discountPrice &&
                                    <Box
                                        mx={1}
                                        fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.95)}
                                        style={{
                                            textDecoration: 'line-through',
                                            color: grey[600]
                                        }}>
                                        {UtilsFormat.numberToMoney(storage.finalPrice)}
                                    </Box>
                                }
                                <Box display='flex'
                                     justifyContent='flex-end'
                                     alignItems='baseline'
                                     pl={1}
                                     pr={2}
                                     fontWeight={400}
                                     fontSize={UtilsConverter.addRem(theme.typography.h5.fontSize, 1)}
                                     style={{
                                         color: colors.success.main
                                     }}>
                                    {UtilsFormat.numberToMoney(storage.discountPrice)}
                                    <Box pl={0.4} fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.8)}>
                                        {lang.get("toman")}
                                    </Box>
                                </Box>
                            </Box>
                            <Collapse in={active}>
                                <Box mt={1}>
                                    <Divider/>
                                    <Box mt={2} display={'flex'} justifyContent={'center'} flexWrap={'wrap-reverse'}>
                                        {storage.jDeadline &&
                                        <Box display={'flex'} alignItems={'center'} my={1}>
                                            <Box flex={1} display={'flex'} alignItems='center'>
                                                <Gem mx={1}/>
                                                {lang.get('deadline')}:
                                            </Box>
                                            <Box ml={1}>
                                                {storage.jDeadline}
                                            </Box>
                                        </Box>
                                        }
                                        <Box flex={1} display={'flex'} mr={2} my={1}
                                             justifyContent={storage.jDeadline ? 'flex-end' : 'center'}>
                                            {
                                                storageData && storage.maxCountForSale > 1 ?
                                                    <Box display={'flex'} alignItems={'center'}
                                                         justifyContent={'center'}>
                                                        {!storage.jDeadline &&
                                                        <Typography px={2}>
                                                            {lang.get("count")}:
                                                        </Typography>}
                                                        <Tooltip
                                                            title={storage.maxCountForSale <= storageData.count ? `تعداد موجود: ${storage.maxCountForSale}` : undefined}>
                                                            <ButtonGroup size="small" color="primary"
                                                                         aria-label="outlined primary button group">
                                                                <Button className={classes.inputButton}
                                                                        disabled={storage.maxCountForSale <= storageData.count}
                                                                        onClick={() => onStorageCountChange({
                                                                            count: (storageData.count + 1)
                                                                        })}>
                                                                    <Add fontSize="small"/>
                                                                </Button>
                                                                <BaseTextField
                                                                    className={classes.input}
                                                                    value={_.toString(storageData.count)}
                                                                    optionalLabel=''
                                                                    type={'number'}
                                                                    onTextChange={(val) => {
                                                                        onStorageCountChange({
                                                                            count: val
                                                                        })
                                                                    }}
                                                                    onTextFocusChange={(val) => {
                                                                        if (!storageData.count)
                                                                            onStorageCountChange({
                                                                                count: 1
                                                                            })
                                                                    }}
                                                                    variant={"standardWhitOutButtonLine"}/>
                                                                <Button className={classes.inputButton}
                                                                        disabled={1 >= storageData.count}
                                                                        onClick={() => onStorageCountChange({
                                                                            count: (storageData.count - 1)
                                                                        })}>
                                                                    <Remove fontSize="small"/>
                                                                </Button>
                                                            </ButtonGroup>
                                                        </Tooltip>
                                                    </Box>
                                                    :
                                                    <Box
                                                        fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.8)}>
                                                        {lang.get("max_count_order_one")}
                                                    </Box>
                                            }
                                        </Box>
                                    </Box>
                                    {
                                        false &&
                                        <Features data={storage.features}
                                                  onFeatureChange={({id, activeValue}) => {
                                                      onFeatureChange({
                                                          feature_id: id,
                                                          activeValue: activeValue
                                                      })
                                                  }}/>
                                    }
                                    <Box display={'flex'}
                                         justifyContent={'center'}
                                         flexDirection={'row-reverse'}
                                         mt={1} px={1} py={1.5}>
                                        <SuccessButton
                                            disabled={setting.disable}
                                            onClick={() => {
                                                addToBasket(true);
                                            }}
                                            style={{
                                                paddingTop: theme.spacing(1.2),
                                                paddingBottom: theme.spacing(1.2),
                                                marginRight: theme.spacing(0.5)
                                            }}>
                                            <ShoppingCartOutlined/>
                                            <Box mr={1}>
                                                {lang.get("order_now")}
                                            </Box>
                                        </SuccessButton>
                                        <Tooltip title={lang.get("add_to_basket")} placement="top"
                                                 aria-label={lang.get("add_to_basket")}>
                                            <Box>
                                                <BaseButton
                                                    disabled={setting.disable}
                                                    variant="outlined"
                                                    style={{
                                                        color: amber[800],
                                                        borderColor: amber[700],
                                                        paddingTop: theme.spacing(1.2),
                                                        paddingBottom: theme.spacing(1.2),
                                                        marginLeft: theme.spacing(0.5)
                                                    }}
                                                    onClick={() => addToBasket()}>
                                                    <AddShoppingCart/>
                                                </BaseButton>
                                            </Box>
                                        </Tooltip>
                                    </Box>

                                    {storage.max_shipping_time ?
                                    <Typography alignItems={'center'} justifyContent={"center"} variant={"body2"} pt={0.5}>
                                        <LocalShippingTwoTone
                                        style={{
                                            marginLeft:theme.spacing(1)
                                        }}/>
                                        {
                                            storage.shippingTimeIsHours?
                                                `ارسال پس از ` + storage.max_shipping_time + " ساعت":
                                                'ارسال از '+storage.max_shipping_time + ' روز کاری دیگر'
                                        }
                                    </Typography>:
                                        <React.Fragment/>
                                    }
                                </Box>
                            </Collapse>
                        </React.Fragment> :
                        <React.Fragment>
                            <Box pt={2}>
                                <Divider/>
                                <Typography pt={2} width={1} justifyContent={"center"} variant={"h6"}>
                                    {lang.get("unavailable")}
                                </Typography>
                            </Box>
                        </React.Fragment>}
                </Box>
                {selectable && <Box className={classes.storagesItemLine} component={'span'} ml={2}/>}
            </Box>
        </Box>
    )
});


//region Components

//region Features

function Features({data, onFeatureChange, ...props}) {
    return (
        data ?
            <Box mt={1} {...props}>
                <Divider/>
                <Box display={'flex'} flexDirection={'column'} mt={1.5}>
                    {data.map((f, index) => (
                        <Box key={`features-${index}`}>
                            {index !== 0 ?
                                <Box my={1}>
                                    <Divider/>
                                </Box> : null
                            }
                            {f.isBool ?
                                <BoolFeature data={f} onChange={onFeatureChange}/> :
                                f.isSingle ?
                                    <SingleFeature data={f} onChange={onFeatureChange}/> :
                                    f.isMulti ?
                                        <MultiFeature data={f} onChange={onFeatureChange}/> : null}
                        </Box>
                    ))}
                </Box>
            </Box> :
            <React.Fragment/>
    )
}

function BoolFeature({data, onChange, ...props}) {
    const [timer, setTimer] = useState();

    function onChangeHandler(event, h) {
        clearTimeout(timer);
        setTimer(setTimeout(() => {
            onChange({
                id: data.id,
                activeValue: h
            })
        }, 1000));
    }

    return (
        <Box display={'flex'} alignItems={'center'}>
            <Typography variant={'body1'} textStyle={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                {data.name}:
                {!data.value[1].name ?
                    <Typography variant={'body2'}>
                        {UtilsFormat.numberToMoney(data.value[1].price)}
                        <Typography pr={0.4} variant={'caption'} color={grey[800]}>
                            {lang.get("toman")}
                        </Typography>
                    </Typography>
                    : null}
            </Typography>
            <Box display={'flex'} flex={1} mr={0.5} alignItems={'center'} flexWrap={'wrap'}>
                <Box
                    style={{
                        transform: 'rotate(180deg)'
                    }}>
                    <Switch
                        defaultChecked={data.activeValue}
                        onChange={onChangeHandler}
                        value="checkedB"
                        color="primary"
                        inputProps={{'aria-label': 'primary checkbox'}}/>
                </Box>
                {(data.value[0].name) ?
                    <Box display={'flex'} flexDirection={'column'}>
                        <Typography variant={'body2'}
                                    pb={0.5}
                                    color={grey[800]}>
                            {data.value[0].name}
                        </Typography>
                        <Typography variant={'body2'}>
                            {UtilsFormat.numberToMoney(data.value[0].price)}
                            <Typography pr={0.4} variant={'caption'} color={grey[800]}>
                                {lang.get("toman")}
                            </Typography>
                        </Typography>
                    </Box>
                    : null
                }
            </Box>
        </Box>
    )
}

function SingleFeature({data, onChange, ...props}) {
    const [selectedValue, setSelectedValue] = React.useState(data.activeValue);
    const [timer, setTimer] = useState();
    useEffect(() => {
        clearTimeout(timer);
        setTimer(setTimeout(() => {
            onChange({
                id: data.id,
                activeValue: selectedValue
            })
        }, 1000));

        return clearTimeout(timer);
    }, [selectedValue]);

    const handleChange = feature_id => {
        setSelectedValue(feature_id);
    };

    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Typography variant={'body1'}>
                {data.name}:
            </Typography>
            <Box display={'flex'} width={1} flexWrap={'wrap'} justifyContent={'center'} pt={0.8}>
                {data.value.map((d, index) => (
                    <React.Fragment key={`${data.id}${d.id}${index}`}>
                        {
                            _.isNumber(d.price) &&
                            <Box ml={0.5} mb={0.5} display={'flex'} alignItems={'center'}>
                                <Box display={'flex'} flexDirection={'column'}
                                     onClick={() => handleChange(d.id)}
                                     style={{
                                         cursor: 'pointer'
                                     }}>
                                    <Typography variant={'body2'}
                                                pb={0.5}
                                                color={grey[800]}>
                                        {d.name}
                                    </Typography>
                                    {d.price > 0 ?
                                        <Typography variant={'body2'}>
                                            {UtilsFormat.numberToMoney(d.price)}
                                            <Typography pr={0.4} variant={'caption'} color={grey[800]}>
                                                {lang.get("toman")}
                                            </Typography>
                                        </Typography>
                                        : null}
                                </Box>
                                <Radio
                                    checked={selectedValue === d.id}
                                    onChange={() => handleChange(d.id)}
                                    value={d.name}
                                    inputProps={{'aria-label': d.name}}
                                />
                            </Box>
                        }
                    </React.Fragment>
                ))}
            </Box>
        </Box>
    )
}

function MultiFeature({data, onChange, ...props}) {
    const [selectedValue, setSelectedValue] = React.useState({});
    const [timer, setTimer] = useState();
    useEffect(() => {
        clearTimeout(timer);
        setTimer(setTimeout(() => {
            const val = [];
            _.forEach(selectedValue, (a, k) => {
                if (a)
                    val.push(_.toNumber(k))
            });
            onChange({
                id: data.id,
                activeValue: val
            })
        }, 1000));
    }, [selectedValue]);

    const handleChange = (feature_id) => {
        const newValue = selectedValue;
        newValue[feature_id] = !selectedValue[feature_id];
        setSelectedValue({...newValue})
    };

    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Typography variant={'body1'}>
                {data.name}:
            </Typography>
            <Box display={'flex'} pt={0.8} width={1} flexWrap={'wrap'} justifyContent={'center'}>
                {data.value.map((d, index) => (
                    <React.Fragment key={`${data.id}${d.id}${index}`}>
                        {_.isNumber(d.price) &&
                        <Box ml={0.5} mb={0.5} display={'flex'} alignItems={'center'}>
                            <Box display={'flex'} flexDirection={'column'}
                                 onClick={() => handleChange(d.id)}
                                 style={{
                                     cursor: 'pointer'
                                 }}>
                                <Typography variant={'body2'}
                                            pb={0.5}
                                            color={grey[800]}>
                                    {d.name}
                                </Typography>
                                {d.price > 0 ?
                                    <Typography variant={'body2'}>
                                        {UtilsFormat.numberToMoney(d.price)}
                                        <Typography pr={0.4} variant={'caption'} color={grey[800]}>
                                            {lang.get("toman")}
                                        </Typography>
                                    </Typography>
                                    : null}
                            </Box>
                            <Checkbox
                                checked={selectedValue[d.id] === true}
                                onChange={(e) => handleChange(d.id)}
                                value={d.name}
                                inputProps={{'aria-label': d.name}}
                            />
                        </Box>}
                    </React.Fragment>
                ))}
            </Box>
        </Box>
    )
}

//endregion Features

//endregion Components
