import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../../utils/Utils";
import {makeStyles, withWidth} from "@material-ui/core";
import {theme} from "../../../repository";
import _ from 'lodash'
import Loadable from "react-loadable";
import ComponentError from "../../base/ComponentError";
import {SpecialOffersColumnPlaceHolder} from "../../home/SpecialProductsColumn";
import {cyan} from "@material-ui/core/colors";
import clsx from "clsx";

const VSliderStyle = makeStyles(theme => ({
    sliderRoot: {
        position: 'relative',
        '& .tns-outer [data-action]': {
            display: 'none'
        },
        '& .tns-inner': {
            overflow: 'auto'
        }
    },
    vSliderItem: {
        width: '80%',
        maxWidth: 120,
        cursor: 'pointer',
        ...UtilsStyle.borderRadius(7),
        ...UtilsStyle.transition(500),
    }
}));

const settings = {
    lazyload: true,
    nav: false,
    mouseDrag: true,
    loop: false,
    items: 6,
    controls: false,
    speed: 1500,
    swipeAngle: false,
    arrowKeys: false,
    autoplayButton: false,
    autoplay: false,
    autoplayTimeout: 8000,
    autoplayHoverPause: false,
    center: true
};

function VSlider({inRef, items, activeItem, onClick, ...props}) {
    const classes = VSliderStyle(props);
    const [it, setIt] = useState([]);

    useEffect(() => {
        if (!_.isArray(items))
            return
        const i = [...items];
        setIt([...i]);
    }, [items])

    function isMd() {
        return (props.width === "md" || props.width === 'lg' || props.width === 'xl')
    }

    const se = {
        ...settings,
        items: isMd() ? 6 : 4,
        axis: isMd() ? 'vertical' : 'horizontal'
    };

    useEffect(() => {

    }, [activeItem])


    return (
        <Box dir={'ltr'} pt={isMd() ? 0 : 1.5} width={1} className={clsx(classes.sliderRoot,"custom-tiny-slider")}>
            {isMd() ?
                <TinySlider
                    tinyRef={ts => {
                        inRef = ts;
                    }}
                    settings={se}>
                    {it.map((item, index) => (
                        <img
                            key={index}
                            src={item.image}
                            className={classes.vSliderItem}
                            alt={item.title}
                            onClick={() => {
                                onClick(item.id)
                            }}
                            style={{
                                height: 60,
                                border:`1px solid ${activeItem === item.id ? cyan[300] : 'transparent'}`,
                                cursor: 'pointer',
                                ...UtilsStyle.borderRadius(5)
                            }}/>
                    ))}
                </TinySlider> :
                <TinySlider
                    tinyRef={ts => {
                        inRef = ts;
                    }}
                    settings={se}>
                    {it.map((item, index) => (
                        <img key={index}
                             src={item.image}
                             className={classes.vSliderItem}
                             alt={item.title}
                             onClick={() => {
                                 onClick(item.id)
                             }}
                             style={{
                                 height: 60,
                                 paddingLeft: theme.spacing(0.3),
                                 paddingRight: theme.spacing(0.3),
                                 border:`1px solid ${activeItem === item.id ? cyan[300] : 'transparent'}`,
                                 cursor: 'pointer',
                                 ...UtilsStyle.borderRadius(5)
                             }}/>
                    ))}
                </TinySlider>
            }
        </Box>

    )


}

export default withWidth()(VSlider);


const TinySlider = Loadable.Map({
    loader: {
        Component: () => import('tiny-slider-react-ar'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component ref={props.tinyRef} {...props}/>;
        // return <Box>s</Box>
    },
});
