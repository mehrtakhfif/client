import React, {createRef, useState} from "react";
import Box from "@material-ui/core/Box";
import VSlider from "./VSlider";
import HSlider from "./HSlider";
import {UtilsStyle} from "../../../utils/Utils";
import {makeStyles} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";


const useSlider = makeStyles(theme => (
    {
        singleSliderRoot: {
            // overflow:'hidden'
        },
        hSlider: {
            '& .slick-list': {
                lineHeight: 0
            }
        },
        vSlider: {
            '& .slick-list': {
                '& .slick-track': {
                    height: 'fit-content !important',
                }
            }
        },
    }
));

export default function Slider({items, ...props}) {
    const classes = useSlider(props);
    let vSliderRef = createRef();
    let hSliderRef = createRef();

    const [activeItem,setActiveItem] = useState(0)

    return (
        <Box mt={3} display='flex'
             className={classes.singleSliderRoot}
             alignItems={'center'}
             flexDirection={{
                 xs: 'column-reverse',
                 md: 'row'
             }}
             style={{
                 overflow: 'hidden'
             }}>
            <Box
                width={{
                    xs: '100%',
                    md: '15%'
                }}
                display='flex'
                className={classes.vSlider}>
                <VSlider
                    inRef={vSliderRef}
                    items={items}
                    activeItem={activeItem}
                    onClick={(itemIndex) => {
                        setActiveItem(itemIndex)
                    }}/>
            </Box>
            <Box
                width={{
                    xs: '100%',
                    md: '85%'
                }}
                ml={{
                    xs: 0,
                    md: 4
                }}
                mr={{
                    xs: 0,
                    md: 2
                }}
                my={0.5}
                boxShadow={0}
                className={classes.hSlider}
                flex={1}
                style={{
                    overflow: 'hidden',
                    height: 'fit-content',
                    ...UtilsStyle.borderRadius(7),
                }}>
                <HSlider inRef={hSliderRef} items={items}
                         activeItem={activeItem}
                         onChange={(activeSlide) => {
                             setActiveItem(activeSlide)
                         }}/>
            </Box>

        </Box>
    )
}

export function SliderPlaceHolder({...props}) {
    return (
        <Skeleton/>
    )
}
