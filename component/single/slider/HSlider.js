import React, {createRef, useEffect} from "react";
import {makeStyles} from "@material-ui/core";
import {UtilsStyle} from "../../../utils/Utils";
import Box from "@material-ui/core/Box";
import clsx from "clsx";
import {ChevronLeft, ChevronRight} from "@material-ui/icons";
import _ from 'lodash'
import Img from "../../base/oldImg/Img";
import Loadable from "react-loadable";
import ComponentError from "../../base/ComponentError";
import {SpecialOffersColumnPlaceHolder} from "../../home/SpecialProductsColumn";


//region Props
const iconSize = 40;


const hArrowStyles = makeStyles(theme => ({

    buttonIcon: {
        position: "absolute",
        zIndex: "2",
        backgroundColor: '#ffffffad',
        color: "#5d5d5d",
        ...UtilsStyle.borderRadius("100%"),
        top: '50%',
        bottom: '50%',
        cursor: 'pointer',
        transition: 'all 200ms ease',
        opacity: '0.7',
        '&:hover': {
            transform: 'scale(1.2)',
            opacity: '1'
        },
        [theme.breakpoints.up('md')]: {
            width: iconSize,
            height: iconSize,
            '& svg': {
                width: iconSize,
                height: iconSize
            }
        },
        [theme.breakpoints.down('sm')]: {
            width: iconSize - 15,
            height: iconSize - 15,
            '& svg': {
                width: iconSize - 15,
                height: iconSize - 15
            }
        },
        [theme.breakpoints.down('xs')]: {
            width: iconSize - 20,
            height: iconSize - 20,
            '& svg': {
                width: iconSize - 20,
                height: iconSize - 20
            }
        },

    },
    buttonLeftIcon: {
        [theme.breakpoints.up('md')]: {
            left: '15px'
        },
        [theme.breakpoints.down('sm')]: {
            left: '10px'
        },
        [theme.breakpoints.down('xs')]: {
            left: '5px'
        },
    },
    buttonRightIcon: {
        [theme.breakpoints.up('md')]: {
            right: '15px'
        },
        [theme.breakpoints.down('sm')]: {
            right: '10px'
        },
        [theme.breakpoints.down('xs')]: {
            right: '5px'
        },
    }
}));

function HNextArrow(props) {
    const {onClick} = props;
    const classes = hArrowStyles(props);
    return (
        <Box
            className={clsx(classes.buttonIcon, classes.buttonRightIcon)}>
            <ChevronLeft onClick={onClick}/>
        </Box>
    );
}

function HPrevArrow(props) {
    const {onClick} = props;
    const classes = hArrowStyles(props);

    return (
        <Box
            className={clsx(classes.buttonIcon, classes.buttonLeftIcon)}>
            <ChevronRight onClick={onClick}/>
        </Box>
    );
}


const settings = {
    lazyload: true,
    nav: false,
    mouseDrag: true,
    loop: true,
    items: 1,
    controls: false,
    speed: 300,
    swipeAngle: false,
    arrowKeys: false,
    autoplayButton: false,
    autoplay: false,
    autoplayTimeout: 8000,
    autoplayHoverPause: true,
    center: true
};

//endregion Props

const HSliderStyle = makeStyles(theme => ({
    sliderRoot: {
        position: 'relative',
        '& .tns-outer [data-action]': {
            display: 'none'
        },
        '& .tns-slider': {
            display: 'flex'
        }
    },
}));
export default function HSlider({inRef, items, activeItem, onChange, ...props}) {
    const classes = HSliderStyle();
    let sliderRef = createRef();
    const onGoTo = dir => sliderRef.slider.goTo(dir);

    useEffect(() => {
        const i = _.findIndex(items, (i) => i.id === activeItem)
        if (i !== -1)
            onGoTo(i)
    }, [activeItem]);


    return (
        <Box dir={'ltr'} width={1} className={classes.sliderRoot}>
            <HPrevArrow onClick={() => onGoTo('prev')}/>
            <HNextArrow onClick={() => onGoTo('next')}/>
            <TinySlider
                tinyRef={ts => {
                    sliderRef = ts;
                }}
                onIndexChanged={(e, a) => {
                    try {
                    onChange(items[e.index - 1].id)
                    }catch (e) {

                    }
                }}
                settings={settings}>
                {items.map(item => (
                    <Box key={item.id}>
                        <Img
                            height={"100%"}
                            imgStyle={{
                                width: '100%',
                                height:'100%'
                            }}
                            alt={item.title}
                            src={item.image}/>
                    </Box>
                ))}
            </TinySlider>
        </Box>
    )
}


const TinySlider = Loadable.Map({
    loader: {
        Component: () => import('tiny-slider-react-ar'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component ref={props.tinyRef} {...props}/>;
        // return <Box>s</Box>
    },
});
