import React from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../../controller/ControllerProduct";
import useSWR, {useSWRInfinite as useSWRPages} from "swr";
import CommentItem, {CommentItemPlaceholder} from "./CommentItem";
import {lang, theme} from "../../../repository";
import {UtilsStyle} from "../../../utils/Utils";
import {grey} from "@material-ui/core/colors";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "../../base/Typography";


export default function ReplayComments({comment_id, ...props}) {


    //region getData
    const q = ControllerProduct.Comments.getCommentReplays();

    const {
        pages,
        isLoadingMore,
        isReachingEnd,
        loadMore,
        pageSWRs,
        ...paramsQ
    } = useSWRPages(
        `replay-comments-${comment_id}`,
        ({offset, withSWR}) => {
            const {data, error} = withSWR(
                // eslint-disable-next-line react-hooks/rules-of-hooks
                useSWR(q[0] + (offset || 1), () => {
                    const params = {
                        comment_id: comment_id
                    };

                    return q[1]({page: offset || 1, ...params})
                })
            );

            if (error) {
                return <React.Fragment/>
            }

            if (!data) {
                return <CommentItemPlaceholder/>
            }

            return data.comments.map((item, index) =>
                <CommentItem key={`cm-r-${index}`}
                             comment={{
                                 ...item,
                                 replayTo: comment_id,
                             }}
                             py={1}
                             hasButtonBorder={data.comments.length - 1 !== index}/>
            )
        },
        // get next page's offset from the index of current page
        (SWR, index) => {
            if (SWR.data)
                if (SWR.data && SWR.data.pagination.lastPage <= index) return null;
            return (index + 1)
        },
        []
    );

    //endregion getData


    return (
        <React.Fragment>
            <Box displa={'flex'} flexDirection={'column'} pr={5} width={1}>
                {pages}
            </Box>
            {(!isReachingEnd) &&
            <ButtonBase
                focusRipple
                disable={isLoadingMore}
                onClick={loadMore}
                style={{
                    marginBottom: theme.spacing(1),
                    padding: theme.spacing(1),
                    textAlign: 'center',
                    ...UtilsStyle.borderRadius(5)
                }}>
                <Typography variant={"body2"} color={grey[500]}>
                    {lang.get('show_replays')}
                </Typography>
            </ButtonBase>
            }
        </React.Fragment>
    )
}
