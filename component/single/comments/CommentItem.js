import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../../utils/Utils";
import Typography from "../../base/Typography";
import {cyan, grey, red} from "@material-ui/core/colors";
import {lang, theme} from "../../../repository";
import {Hidden, makeStyles} from "@material-ui/core";
import ButtonBase from "@material-ui/core/ButtonBase";
import {Reply, ThumbDownAltOutlined, ThumbUpAltOutlined} from "@material-ui/icons";
import Skeleton from "@material-ui/lab/Skeleton";
import CurentCommponent from "../../single/comments/CommentItem"
import ReplayComments from "./ReplayComments";
import Card from "@material-ui/core/Card";
import AddComment from "./AddComment";
import Collapse from "@material-ui/core/Collapse";
import RatingComponent from "../../base/RatingComponent";

const useCommentItemStyle = makeStyles(theme => ({
    commentItemRoot: {},
    commentItemCardRoot: (props => ({})),
    replayCommentItem: {
        position: 'absolute',
        bottom: -5,
        left: 10,
        cursor: 'pointer',
        '&:hover': {
            color: cyan[600],
        }
    }
}));


export default function CommentItem({comment, disable, hasButtonBorder, ...props}) {
    const classes = useCommentItemStyle(props);
    const {id, text, user, rate, approved, satisfied, replyCount, firstReply, type, purchaseAt, createdAt, commentTimeAfterPurchase, replayTo} = comment;
    const {name} = user;
    const purchaseText = purchaseAt ? (commentTimeAfterPurchase.days !== 0 ?
        commentTimeAfterPurchase.days + " " + lang.get("day") :
        commentTimeAfterPurchase.hours + " " + lang.get("hour")) : null;

    const [state, setState] = useState({
        disable: false,
        showReplay: false,
        replay: false
    });


    function onShowReplays() {
        setState({
            ...state,
            showReplay: true,
        })
    }

    const showReplaysButton = (
        (!state.showReplay && replyCount > 1) &&
        <ButtonBase
            focusRipple
            onClick={onShowReplays}
            disabled={state.disable}
            style={{
                marginBottom: theme.spacing(1),
                padding: theme.spacing(1),
                textAlign: 'center',
                ...UtilsStyle.borderRadius(5)
            }}>
            <Typography variant={"body2"} color={grey[500]}>
                {lang.get('show_replays')}
            </Typography>
        </ButtonBase>);

    return (
        <Box className={classes.commentItemRoot} display={'flex'} flexDirection={'column'} width={1}
             alignItems={'center'} {...props}>
            <Box className={classes.commentItemCardRoot} display={'flex'} width={1} flexDirection={{
                xs: 'column',
                sm: 'unset'
            }}>
                <Box display={'flex'}
                     flexDirection={{
                         xs: 'unset',
                         sm: 'column'
                     }}
                     flexWrap={'wrap'}
                     alignItems={'center'} justifyContent={'center'}
                     py={1} mx={2}>
                    <Box display={'flex'} flexDirection={'column'}
                         px={{
                             xs: 1,
                             sm: 0
                         }}
                         alignItems={'center'} justifyContent={'center'}>
                        {rate ?
                            <Box display={'flex'}
                                 flexDirection={'column'}
                                 alignItems={'center'}
                                 justifyContent={'center'}
                                 pb={0.5}>
                                <RatingComponent rate={rate} readOnly size="small"/>
                            </Box> : null
                        }
                        <Typography variant={'subtitle1'}>
                            {name}
                        </Typography>
                    </Box>
                    <Box display={'flex'} flexDirection={'column'}
                         px={{
                             xs: 1,
                             sm: 0
                         }}
                         alignItems={'center'} justifyContent={'center'}>
                        <Typography variant={'subtitle2'} pt={0.5} fontWeight={300} textStyle={{direction: 'rtl'}}>
                            {createdAt}
                        </Typography>
                        {purchaseText ?
                            <Typography variant={'caption'} pt={0.5} fontWeight={300}>
                                {purchaseText + " " + lang.get("after_purchase")}
                            </Typography> : null
                        }
                    </Box>
                    {satisfied !== null ?
                        <Hidden xlUp>
                            <Box
                                display={'flex'}
                                alignItems={'center'}
                                justifyContent={'center'}
                                pt={0.75}
                                px={{
                                    xs: 1,
                                    sm: 0
                                }}
                                pb={{
                                    xs: 0.2,
                                    sm: 0.5
                                }}
                                style={{
                                    color: satisfied ? cyan[300] : red[300],
                                    cursor: 'default',
                                    ...UtilsStyle.disableTextSelection(),
                                }}>
                                <Typography variant={'body2'} pl={0.5} fontWeight={300}
                                            color={satisfied ? cyan[300] : red[300]}>
                                    {lang.get(satisfied ? "i_am_happy_with_my_purchase" : 'i_am_not_happy_with_my_purchase')}
                                </Typography>
                                {satisfied ? <ThumbUpAltOutlined fontSize="small"/> :
                                    <ThumbDownAltOutlined fontSize="small"/>}
                            </Box>
                        </Hidden>
                        : null}
                </Box>
                <Box display={'flex'} pl={1.5} ml={3} flexDirection={'column'}
                     flex={1} width={1} alignItems={'center'}
                     py={1}
                     style={{
                         borderBottom: (hasButtonBorder) ? `1px solid ${grey[200]}` : null
                     }}>
                    <Box display={'flex'} justifyContent={'flex-start'} pt={1} pb={type === "q-a" ? 3.5 : 2.5} flex={1}
                         flexDirection={'column'} width={1}
                         pl={2}
                         style={{
                             position: 'relative',
                         }}>
                        <Typography variant={'subtitle2'} pb={1.5} fontWeight={300}
                                    display={'flex'}
                                    textStyle={{
                                        lineHeight: 1.9
                                    }}>
                            {text}
                        </Typography>
                        {(type === "q-a") ?
                            <React.Fragment>
                                {
                                    (false &&!state.replay) ?
                                        <ButtonBase
                                            focusRipple
                                            className={classes.replayCommentItem}
                                            disabled={disable}
                                            onClick={()=>setState({
                                                ...state,
                                                replay: true
                                            })}
                                            style={{
                                                color: replayTo ? cyan[200] : cyan[400],
                                                ...UtilsStyle.borderRadius(5)
                                            }}>
                                            <Typography py={1} px={2} display={'flex'} alignItems={'center'}
                                                        color={replayTo ? cyan[200] : cyan[400]}
                                                        justifyContent={'center'}>
                                                {!replayTo ? lang.get("me_replay_for_question") : null}
                                                <Reply style={{paddingRight: theme.spacing(0.5)}}/>
                                            </Typography>
                                        </ButtonBase> :
                                        <Collapse in={state.replay}
                                                  style={{
                                                      width: '100%'
                                                  }}>
                                            <Box component={Card} p={1} my={3} width={1}>
                                                <AddComment comment={comment} isRateComment={false}
                                                            onCommentSent={() => {
                                                                setState({
                                                                    ...state,
                                                                    replay: false
                                                                })
                                                            }}/>
                                            </Box>
                                        </Collapse>
                                }
                            </React.Fragment> : null}
                    </Box>
                </Box>
                {satisfied !== null ?
                    <Hidden lgDown>
                        <Box py={1} px={2}>
                            <Box
                                display={'flex'}
                                flexDirection={'column'}
                                alignItems={'center'}
                                justifyContent={'center'}
                                style={{
                                    backgroundColor: satisfied ? cyan[100] : red[100],
                                    padding: theme.spacing(1.5),
                                    color: grey[600],
                                    cursor: 'default',
                                    ...UtilsStyle.borderRadius(5),
                                    ...UtilsStyle.disableTextSelection(),
                                }}>
                                {satisfied ? <ThumbUpAltOutlined fontSize="large"/> :
                                    <ThumbDownAltOutlined fontSize="large"/>}
                                <Typography variant={'body2'} pt={2} fontWeight={300} color={grey[800]}>
                                    {lang.get(satisfied ? "i_am_happy_with_my_purchase" : 'i_am_not_happy_with_my_purchase')}
                                </Typography>
                            </Box>
                        </Box>
                    </Hidden>
                    : null}
            </Box>
            {
                !state.showReplay && firstReply &&
                <Box displa={'flex'} flexDirection={'column'} pr={5} width={1}>
                    <CurentCommponent
                        disable={disable}
                        comment={{
                            ...firstReply,
                            replayTo: id,
                        }}
                        hasButtonBorder={false}/>
                </Box>
            }
            {state.showReplay &&
            <ReplayComments comment_id={id}/>}

            {showReplaysButton}
            {
                (hasButtonBorder) ?
                    <Box
                        width={3 / 4}
                        style={{
                            borderBottom: `1px solid ${grey[200]}`
                        }}/> : null
            }
        </Box>
    )
}


export function CommentItemPlaceholder({...props}) {
    return (
        <Box {...props} width={1}>
            <Skeleton variant="rect" height={200}/>
        </Box>
    )
}
