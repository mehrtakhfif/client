import React, {Fragment, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import {lang} from "../../../repository";
import FormController from "../../base/formController/NewFormController";
import {makeStyles, MuiThemeProvider, Switch, useTheme} from "@material-ui/core";
import Typography from "../../base/Typography";
import BaseButton from "../../base/button/BaseButton";
import SuccessButton from "../../base/button/buttonVariant/SuccessButton";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import ControllerProduct from "../../../controller/ControllerProduct";
import _ from 'lodash'
import DefaultTextField from "../../base/textField/DefaultTextField";
import createPalette from "@material-ui/core/styles/createPalette";
import {cyan} from "@material-ui/core/colors";
import RatingComponent from "../../base/RatingComponent";
import {useUserContext} from "../../../context/UserContext";


const useAddCommentStyles = makeStyles(theme => ({
    textField: {
        width: '100%',
        '& input': {
            paddingLeft: theme.spacing(0.5),
            paddingRight: theme.spacing(0.5),
        }
    },
}));

export default function AddComment({productPermalink, comment, isRateComment, onCommentSent, ...props}) {
    const formRef = useRef();
    const theme = useTheme();
    const classes = useAddCommentStyles();
    const [user] = useUserContext();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        data: {
            firstName: (user && _.isString(user.firstName)) ? user.firstName : '',
            lastName: (user && _.isString(user.lastName)) ? user.lastName : '',
            comment: '',
            rate: 0,
            satisfied: true,
        },
        disable: false,
        reset: () => {
            setState({
                ...state,
                data: {
                    firstName: (user && _.isString(user.firstName)) ? user.firstName : '',
                    lastName: (user && _.isString(user.lastName)) ? user.lastName : '',
                    comment: '',
                    rate: 0,
                    satisfied: true,
                },
                disable: false,
            })
        }
    });
    const [commentText, setCommentText] = useState("");

    const {data, disable} = state;

    function onSubmitClick() {
        try {
            if (formRef.current.hasError()) {
                enqueueSnackbar(lang.get("er_fill_all_inputs"),
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return
            }
            if (isRateComment && state.data.rate < 1) {
                enqueueSnackbar(lang.get("er_fill_rate"),
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }

            setState({
                ...state,
                disable: true,
            });

            const param = formRef.current.serialize();

            if (comment) {
                const cmText = "@" + _.snakeCase(comment.user.name) + "↵" + param.comment_text;
                ControllerProduct.Comments.setReplayComment( {
                    comment_id: comment.id,
                    text: cmText,
                    firstName: param.first_name,
                    lastName: param.last_name
                }).then(sentSuccess).catch(sentFail)
                return;
            }

            if (isRateComment) {
                ControllerProduct.Comments.setPointsAndComments( {
                    productPermalink: productPermalink,
                    rate: state.data.rate,
                    satisfied: state.data.satisfied,
                    text: param.comment_text,
                    firstName: param.first_name,
                    lastName: param.last_name
                }).then(sentSuccess).catch(sentFail)
                return;
            }


            ControllerProduct.Comments.setQuestionAndAnswerComments( {
                productPermalink: productPermalink,
                text: param.comment_text,
                firstName: param.first_name,
                lastName: param.last_name
            }).then(sentSuccess).catch(sentFail)
        } catch (e) {
        }

    }

    function sentSuccess(res) {
        // enqueueSnackbar(lang.get("me_comment_submit_successfully"),
        //     {
        //         variant: "success",
        //         action: (key) => (
        //             <Fragment>
        //                 <Button onClick={() => {
        //                     closeSnackbar(key)
        //                 }}>
        //                     {lang.get('close')}
        //                 </Button>
        //             </Fragment>
        //         )
        //     });
        state.reset();
        onCommentSent()
    }

    function sentFail() {
        setState({
            ...state,
            disable: false
        })
    }

    return (
        <FormController
            innerref={formRef}
            width={1}
            name={"comment"}
            display={'flex'} flex={1} pl={2}
            pr={{
                xs: 2,
                md: 5
            }}
            py={2}
            alignItems={'flex-end'}
            flexWrap={'wrap'}>
            {(!(state.data.firstName && state.data.lastName)) &&
            <React.Fragment>
                <TextFieldItem>
                    <DefaultTextField
                        label={lang.get('first_name')}
                        defaultValue={(user && _.isString(user.firstName)) ? user.firstName : ''}
                        name={"first_name"}
                        required={true}/>
                </TextFieldItem>
                <TextFieldItem>
                    <DefaultTextField
                        label={lang.get('last_name')}
                        defaultValue={(user && _.isString(user.lastName)) ? user.lastName : ''}
                        name={"last_name"}
                        required={true}
                    />
                </TextFieldItem>
            </React.Fragment>
            }
            {isRateComment &&
            <Box display={'flex'} width={1} alignItems={'center'} flexWrap={'wrap'}>
                <TextFieldItem display={'flex'} flexDirection={'column'} alignItems={'center'}>
                    <Typography variant={'h6'} pb={2}>
                        {lang.get("are_you_happy_with_your_purchase")}
                    </Typography>
                    <Box display={'flex'} alignItems={'center'}>
                        <Typography variant={'body1'} pr={1}>
                            {lang.get("no")}
                        </Typography>

                        <MuiThemeProvider
                            theme={{
                                ...theme,
                                palette: createPalette({
                                    primary: {
                                        light: cyan[400],
                                        main: cyan[500],
                                        dark: cyan[700]
                                    },
                                    secondary: {
                                        light: cyan[500],
                                        main: cyan["A700"],
                                        dark: cyan[900]
                                    }
                                })
                            }}>
                            <Switch checked={data.satisfied}
                                    disabled={disable}
                                    onChange={() => {
                                        setState({
                                            ...state,
                                            data: {
                                                ...state.data,
                                                satisfied: !state.data.satisfied
                                            }
                                        })
                                    }}/>
                        </MuiThemeProvider>
                        <Typography variant={'body1'} pl={1}>
                            {lang.get("yes")}
                        </Typography>
                    </Box>
                </TextFieldItem>
                <TextFieldItem display={'flex'} flexDirection={'column'} alignItems={'center'}>
                    <Typography variant={'h6'} pb={2}>
                        {lang.get('customer_score')}
                    </Typography>
                    <RatingComponent rate={data.rate}
                                     showNoRateSubmit={false}
                                     readOnly={disable}
                                     onRatingChange={(rate) => {
                                         setState({
                                             ...state,
                                             data: {
                                                 ...state.data,
                                                 rate: rate
                                             }
                                         })
                                     }}/>
                </TextFieldItem>
            </Box>}
            <TextFieldItem width={1}>
                <DefaultTextField
                    label={lang.get('comment_text')}
                    defaultValue={""}
                    name={"comment_text"}
                    required={true}
                    textFieldProps={{
                        multiline: true,
                        rows: 6
                    }}/>
            </TextFieldItem>
            <TextFieldItem width={1} mt={2} mb={2}>
                <SuccessButton onClick={onSubmitClick}>
                    <Typography variant={'body2'} color={'#fff'}>
                        {lang.get("submit")}
                    </Typography>
                </SuccessButton>
                <BaseButton variant={"text"} onClick={onCommentSent} style={{
                    marginRight: theme.spacing(1)
                }}>
                    <Typography variant={'body2'}>
                        {lang.get("close")}
                    </Typography>
                </BaseButton>
            </TextFieldItem>
        </FormController>
    )
}

function TextFieldItem({full = false, ...props}) {
    return (
        <Box
            height={'max-content'}
            pb={3}
            pr={{
                xs: 2.5,
                md: 0
            }}
            pl={{
                xs: 2.5,
                md: 4
            }}
            width={{
                xs: 1,
                md: 1 / 2
            }}
            {...props}>
            {props.children}
        </Box>
    )
}
