import {makeStyles} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import React, {useState} from "react";
import BoxWithTopBorder from "../../base/textHeader/BoxWithTopBorder";
import Card from "@material-ui/core/Card";
import {lang, theme} from "../../../repository";
import rout from "../../../router";
import {cyan, grey, orange} from "@material-ui/core/colors";
import BaseButton from "../../base/button/BaseButton";
import Link from "@material-ui/core/Link";
import {UtilsStyle} from "../../../utils/Utils";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "../../base/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import AddComment from "./AddComment";
import Collapse from "@material-ui/core/Collapse";
import RatingComponent from "../../base/RatingComponent";
import {useIsLoginContext} from "../../../context/IsLoginContextContainer";

const useCommentsStyle = makeStyles(theme => ({
    commentsTabsRoot: {
        backgroundColor: 'transparent',
        boxShadow: 'unset',
        '& div': {
            backgroundColor: 'transparent',
        },
        '& button>span': {
            color: '#000',
        },
        '&>div>div>div': {
            borderBottom: '2px solid #0000000a'
        }
    },
    tabPanelRoot: {
        '&>div': {
            width:'100%',
            padding: 0,
        }
    }
}));

export default function Comments({
                                     permalink,
                                     rateComments,
                                     rateCommentsDetails,
                                     questionsComments,
                                     questionsCommentsDetails,
                                     rate, purchased,
                                     onRequestNextPage,
                                     onShowReplays, ...props
                                 }) {
    const classes = useCommentsStyle(props);
    const isLogin = useIsLoginContext()
    const [state, setState] = useState({
        activeTab: 0,
        rateSetting: {
            showAddComment: false,
        },
        questionsSetting: {
            showAddComment: false,
        }
    });
    return (
        <BoxWithTopBorder
            component={Card}
            title={lang.get('user_rate_and_comments')}
            borderColor={orange[700]}
            borderStork={5}
            display={'flex'}
            flexDirection={'column'}
            mt={{
                xs: 0,
                md: 2
            }}>
            <Box display={'flex'} px={2} py={1}>
                {rate ?
                    <Box display={'flex'}
                         flexDirection={'column'}
                         alignItems={'center'}
                         justifyContent={'center'}
                         px={3}
                         py={3}>
                        <Typography variant={'h6'} component={'h6'} pb={2}>
                            {lang.get('user_rating')}
                        </Typography>
                        <RatingComponent rate={rate} readOnly/>
                    </Box> : null
                }
                <Box width={1} flex={1} display={'flex'} alignItems={'center'} justifyContent={'center'} py={2} px={1}
                     flexDirection={'column'}>
                    <Typography variant={'subtitle1'}>
                        {lang.get(!isLogin ? 'me_for_add_comment_first_login' : !purchased ? "me_for_add_comment_must_buying_product" : "me_add_comment_for_product")}
                    </Typography>
                    <Box dsiplay={'flex'} pt={3}>
                        {isLogin ?
                            <>
                                <BaseButton
                                    disabled={!purchased}
                                    variant="outlined"
                                    onClick={() => setState({
                                        ...state,
                                        activeTab: 0,
                                        rateSetting: {
                                            ...state.rateSetting,
                                            showAddComment: true,
                                        }
                                    })}
                                    style={{
                                        borderColor: purchased ? cyan[400] : null,
                                        marginLeft: theme.spacing(1),
                                        marginRight: theme.spacing(1),
                                    }}>
                                    {lang.get("add_rate_and_comments")}
                                </BaseButton>
                                <BaseButton
                                    variant="outlined"
                                    onClick={() => setState({
                                        ...state,
                                        activeTab: 1,
                                        questionsSetting: {
                                            ...state.questionsSetting,
                                            showAddComment: true,
                                        }
                                    })}
                                    style={{
                                        borderColor: cyan[400],
                                        marginLeft: theme.spacing(1),
                                        marginRight: theme.spacing(1),
                                    }}>
                                    {lang.get("add_question")}
                                </BaseButton>
                            </> :
                            <Link
                                href={rout.User.Auth.Login.create({
                                    redirectRout: rout.Product.SingleV2.as(permalink)
                                })}
                                style={{
                                    padding: theme.spacing(1, 2),
                                    color: '#000',
                                    border: `1px solid ${cyan[400]}`,
                                    ...UtilsStyle.borderRadius(4)
                                }}>
                                {lang.get("sign_in")}
                            </Link>}
                    </Box>
                </Box>
            </Box>
            <Box>
                <AppBar position="static" className={classes.commentsTabsRoot}>
                    <Tabs value={state.activeTab}
                          onChange={(e, activeTab) =>
                              setState({
                                  ...state,
                                  activeTab: activeTab
                              })} aria-label="Comments pnael">
                        <Tab
                            label={lang.get("rate_and_comments") + (rateCommentsDetails.count > 0 ? ` (${rateCommentsDetails.count})` : '')} {...a11yProps(0)} />
                        <Tab
                            label={lang.get("question_replay") + (questionsCommentsDetails.count > 0 ? ` (${questionsCommentsDetails.count})` : '')} {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <Box display={state.activeTab === 0 ?'flex' :'none'} flexDirection={'column'} width={1} alignItems={'center'}
                     pb={2}
                     px={2}
                     justifyContent={'center'}>

                    {
                        (purchased) ?
                            <Collapse in={state.rateSetting.showAddComment}
                            style={{
                                width:'100%'
                            }}>
                                <Box component={Card} p={1} my={3} width={1}>
                                    <AddComment productPermalink={permalink} isRateComment={true} onCommentSent={()=>{
                                        setState({
                                            ...state,
                                            rateSetting:{
                                                ...state.rateSetting,
                                                showAddComment:false
                                            }
                                        })
                                    }}/>
                                </Box>
                            </Collapse> : null
                    }
                    {rateComments}
                    {rateCommentsDetails.hasMoreItems ?
                        <Box width={1} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                            <ButtonBase
                                onClick={()=>onRequestNextPage(false)}
                                focusRipple
                                style={{
                                    minWidth: '60%',
                                    marginTop: theme.spacing(1),
                                    padding: theme.spacing(1),
                                    textAlign: 'center',
                                    backgroundColor: grey[100],
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                                <Typography variant={'body1'}>
                                    {lang.get("show_more")}
                                </Typography>
                            </ButtonBase>
                        </Box> : null}
                </Box>
                <Box display={state.activeTab === 1 ?'flex' :'none'} flexDirection={'column'} width={1} alignItems={'center'}
                     pb={2}
                     px={2}
                     justifyContent={'center'}>
                    <Collapse in={state.questionsSetting.showAddComment}
                              style={{
                                  width:'100%'
                              }}>
                        <Box component={Card} p={1} my={3} width={1}>
                            <AddComment productPermalink={permalink} isRateComment={false} onCommentSent={()=>{
                                setState({
                                    ...state,
                                    questionsSetting:{
                                        ...state.rateSetting,
                                        showAddComment:false
                                    }
                                })
                            }}/>
                        </Box>
                    </Collapse>
                    {
                        questionsComments
                    }
                    {questionsCommentsDetails.hasMoreItems ?
                        <Box width={1} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                            <ButtonBase
                                onClick={()=>onRequestNextPage(true)}
                                focusRipple
                                style={{
                                    minWidth: '60%',
                                    marginTop: theme.spacing(1),
                                    padding: theme.spacing(1),
                                    textAlign: 'center',
                                    backgroundColor: grey[100],
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                                <Typography variant={'body1'}>
                                    {lang.get("show_more")}
                                </Typography>
                            </ButtonBase>
                        </Box> : null}
                </Box>
            </Box>
        </BoxWithTopBorder>
    )
}


function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}


function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}
