import React from "react";
import {Box} from "@material-ui/core";
import Img from "./base/oldImg/Img";

export default function IPGImage({IPGKey, ...props}) {
    return (
        IPGKey === "saman" ?
            <Saman {...props}/>
            :
            <Melat{...props}/>
    )
}

export function Saman(props) {
    return (
        <Box width={25}
             height={25}
             {...props}
             style={{
                 backgroundColor: '#000',
                 margin: 2
             }}>

        </Box>
    )
}

export function Melat(props) {
    return (
        <Img src={'/drawable/image/melat-ipg.png'}
             alt={"melat ipg"}
             showSkeleton={false}
             minHeight={25}
             width={25}
             height={25}
             style={{
                 width: 25,
                 height: 25
             }}
            {...props}/>
    )
}
