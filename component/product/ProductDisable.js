import React, {useContext, useState} from "react";
import {Box, Typography, UtilsStyle} from "material-ui-helper";
import {theme} from "../../repository";
import MtIcon from "../MtIcon";
import ProductDataContext from "../productV2/context/ProductDataContext";
import StorageDataContext from "../productV2/context/StorageDataContext";


export default function ProductDisable({...props}) {
    const [productData] = useContext(ProductDataContext);
    const [storageData] = useContext(StorageDataContext);

    const [show, setShow] = useState(true);

    if (!(productData?.disable || storageData?.storage?.disable) || !show)
        return <React.Fragment/>


    return (
        <Box width={1}
             onClick={() => {
                 setShow(false)
             }}
             px={{
                 xs: 2,
                 md: 3
             }}
             pt={{
                 xs: 0.2,
                 md: 2
             }}
             pb={{
                 xs: 2,
                 md: 0
             }}
             style={{
                 cursor: "pointer"
             }}
             {...props}>
            <Typography
                className={"fastShake"}
                width={1}
                px={2}
                py={1}
                variant={"h6"}
                color={"#fff"}
                alignItems={'center'}
                style={{
                    backgroundColor: theme.palette.error.light,
                    ...UtilsStyle.borderRadius(5)
                }}>
                <MtIcon
                    icon={"mt-exclamation-circle"}
                    color={"#fff"}
                    style={{
                        marginLeft: 10
                    }}/>
                {
                    storageData?.storage?.disable ?
                        "انبار پیشنمایش میباشد" :
                        "محصول پیشنمایش میباشد"
                }
            </Typography>
        </Box>
    )
}