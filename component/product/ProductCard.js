import React, {useState} from 'react';
import Card from "@material-ui/core/Card";
import {makeStyles} from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import {colors, lang, media} from '../../repository';
import rout from '../../router';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import {grey, red} from '@material-ui/core/colors';
import clsx from "clsx";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import BaseButton from "../base/button/BaseButton";
import {BugReportOutlined, Favorite, Visibility} from "@material-ui/icons";
import Skeleton from '@material-ui/lab/Skeleton';
import Link from "../base/link/Link";
import Typography from "../base/Typography";
import {Timer} from "../home/SpecialProducts";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {DEBUG} from "../../pages/_app";
import Tooltip from "../base/Tooltip";
import {HiddenLgUp, HiddenMdDown} from "material-ui-helper";
import Img from "../base/Img";

const styles = makeStyles(theme => ({
    cardRoot: {
        paddingBottom: theme.spacing(2)
    },
    title: {
        overflow: 'hidden',
        color: grey[800],
        cursor: 'pointer'
    }
}));


function ProductCard({
                         id,
                         name,
                         storageId,
                         permalink,
                         discountPrice,
                         discountPercent,
                         finalPrice,
                         imageSrc,
                         imageAlt,
                         deadline,
                         category,
                         disable = false,
                         availableCountForSale,
                         beforeGoToSingleValidation = () => true,
                         addToBasketClick,
                         ...props
                     }) {

    const classes = styles(props);
    const className = ['product-' + id];
    delete props.staticContext;

    const singleLink = rout.Product.SingleV2.create(permalink);

    const isSmall = !useMediaQuery('(min-width:600px)');
    const [state, setState] = useState({
        goToSingle: false
    });


    if (props.className) {
        className.push(props.className)
    }


    const {history, location, match, ...rootProps} = props;

    const titleMax = !isSmall ? 70 : 45;
    return (
        <Box className={clsx(className)}
             {...rootProps}
             style={{
                 maxWidth: !isSmall ? 400 : 195,
                 width: '95%',
                 ...props.style
             }}>
            <Card elevation={props.elevation}
                  className={classes.cardRoot}
                  style={{
                      position: 'relative',
                      ...props.style
                  }}
                  product_id={id}>
                {(disable) &&
                <Tooltip title={"پیش‌نمایش"}>
                    <BugReportOutlined
                        style={{
                            position: "absolute",
                            top: 5,
                            left: 5,
                            color: red[400],
                            zIndex: 999
                        }}/>
                </Tooltip>
                }
                {
                    discountPercent &&
                    <Box
                        px={0.7}
                        py={0.2}
                        component={'span'}
                        display={'flex'}
                        alignItems={'center'}
                        justifyContent={'center'}
                        style={{
                            position: 'absolute',
                            left: 7,
                            top: 5,
                            cursor: 'default',
                            color: '#fff',
                            background: red[400],
                            zIndex: 20,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Typography
                            variant={!isSmall ? 'h6' : 'body1'}
                            pl={0.05}
                            color={'#fff'}>
                            {discountPercent}
                        </Typography>
                        <Typography
                            variant={'caption'}
                            color={'#fff'}>%</Typography>
                    </Box>
                }
                <Link href={singleLink} hasHover={false} hasHoverBase={false}>
                    <HiddenLgUp>
                        <Img
                            src={media.convertor(imageSrc,{width:200,height:124,quality:50})}
                            alt={name}
                            p={2}
                            imageWidth={media.thumbnail.width}
                            imageHeight={media.thumbnail.height}/>
                    </HiddenLgUp>
                    <HiddenMdDown>
                        <Img
                            src={media.convertor(imageSrc,{width:400,height:248,quality:50})}
                            alt={name}
                            p={2}
                            imageWidth={media.thumbnail.width}
                            imageHeight={media.thumbnail.height}/>
                    </HiddenMdDown>
                </Link>
                {(deadline) &&
                <Box height={'33px'} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                    {(availableCountForSale <= 0 || deadline * 1000 > new Date().getTime()) ?
                        <Timer time={deadline} textColor={grey[800]} textVariant={'body2'}
                               itemPadding={0.1}
                               style={{direction: 'ltr'}}/> :
                        <Typography variant={'body1'}
                                    color={'#fff'}
                                    py={0.2}
                                    px={0.7}
                                    style={{
                                        justifyContent: 'center',
                                        backgroundColor: red[200],
                                        ...UtilsStyle.borderRadius(5)
                                    }}>
                            {lang.get("the_end")}
                        </Typography>}
                </Box>
                }
                <Box>
                    <Tooltip title={(!name || name.length < titleMax) ? undefined : name} placement="top">
                        <Box>
                            <Link
                                display="block"
                                className={classes.title}
                                hasHover={true}
                                href={singleLink}
                                style={{
                                    opacity: "1 !important"
                                }}>
                                <Typography variant={'body1'}
                                            py={!isSmall ? 2 : 0.5}
                                            px={!isSmall ? 1.5 : 0.75}
                                            fontWeight={!isSmall ? 400 : 300}
                                            style={{
                                                height: !isSmall ? 70 : 40
                                            }}>
                                    {name.substr(0, titleMax)}{name.length > titleMax ? "..." : ""}
                                </Typography>
                            </Link>
                        </Box>
                    </Tooltip>
                    <Box
                        display='flex'
                        pt={1}
                        px={0.5}
                        alignItems='center'
                        flexWrap={'wrap'}
                        dir={'ltr'}
                        justifyContent={'flex-end'}
                        style={{
                            borderTop: '1px dashed #0000001c'
                        }}>
                        {
                            discountPrice ?
                                <ProductPrice
                                    dir={'rtl'}
                                    isSmall={isSmall}
                                    discountPrice={discountPrice}
                                    finalPrice={finalPrice}/> :
                                <Typography width={1} justifyContent={'center'} variant={"h6"}>
                                    {lang.get("unavailable")}
                                </Typography>
                        }
                        {(!isSmall && DEBUG && false) ?
                            <Actions product_id={id}
                                     dir={'rtl'}
                                     onQuickViewClick={() => {
                                         // dispatch(productQuickShow({permalink: permalink}))
                                     }}
                                     {...props}/>
                            : null}
                    </Box>
                </Box>
            </Card>
        </Box>
    )
}

export default ProductCard;

//region Component
//region ProductPrice
const productPriceStyles = makeStyles(theme => ({
    root: {},
}));

function ProductPrice({finalPrice, discountPrice, isSmall, ...props}) {
    const classes = productPriceStyles(props);

    return (
        <Box display='flex' px={0.5} alignItems='center' flex={1}
             className={classes.root}
             flexWrap={'wrap'}
             {...props}
             style={{

                 direction: 'ltr',
                 ...props.style
             }}>
            <Box display='flex'
                 alignItems={!isSmall ? 'center' : 'flex-end'}
                 flexWrap={'wrap'}>
                <Typography pr={0.5} component='span' fontWeight='200'
                            variant={'caption'}
                            style={{
                                color: 'initial'
                            }}>
                    {lang.get('toman')}
                </Typography>
                <Typography
                    fontWeight='500'
                    variant={!isSmall ? 'h4' : 'h6'}
                    pr={0.3}
                    component='strong'
                    color={colors.primary.main}>
                    {UtilsFormat.numberToMoney(discountPrice)}
                </Typography>

            </Box>
            {
                parseFloat(finalPrice) > parseFloat(discountPrice) && (
                    <>
                        <Typography
                            fontWeight='500'
                            color={grey[700]}
                            component='span'
                            variant={!isSmall ? 'h5' : 'h6'}>
                            /
                        </Typography>
                        <Typography pl={0.25}
                                    variant={!isSmall ? 'subtitle2' : 'body2'}
                                    style={{
                                        textDecoration: 'line-through'
                                    }}>
                            {UtilsFormat.numberToMoney(finalPrice)}
                        </Typography>
                    </>
                )
            }
        </Box>
    )
}

//endregion ProductPrice
//region Actions
const productCardProductActionsStyles = makeStyles(theme => ({
    productCardProductActionsIcon: {
        padding: theme.spacing(1),
        fill: grey[700],
        height: 45,
        minWidth: 45,
        ...UtilsStyle.widthFitContent(),
        '&:hover': {
            color: theme.palette.primary.light
        }
    },
}));

function Actions({product_id, isFavorite = false, onQuickViewClick, ...props}) {
    const classes = productCardProductActionsStyles(props);
    return (
        <Box display={'flex !important'} width={'auto !important'} {...props}>
            <AddToFavorite product_id={product_id} isFavorite={isFavorite}/>
            <BaseButton variant={"outlined"}
                        className={classes.productCardProductActionsIcon}
                        onClick={onQuickViewClick}>
                <Tooltip title={lang.get('show')} placement="top">
                    <Visibility/>
                </Tooltip>
            </BaseButton>
        </Box>
    )
}

export function AddToFavorite({product_id, isFavorite, ...props}) {
    const [state, setState] = useState({
        addToBasketLoading: false,
        isFavorite: isFavorite,
        favoriteHover: false
    });


    const iconStyle = {
        color: isFavorite ? red[400] : state.favoriteHover ? red[300] : red[300],
        transform: 'scale(1.1)',
        WebkitAnimation: isFavorite ? 'like-animation 0.5s' : '',
        animation: isFavorite ? 'like-animation 0.5s' : '',
        ...UtilsStyle.transition(500)
    };
    return (
        <Tooltip title={lang.get('add_to_favorite')} placement="top">
            <Box ml={0.2}
                 p={1.5}
                 display={'flex'}
                 alignItems={'center'}
                 onClick={() => {
                     setState({
                         ...state,
                         isFavorite: !state.isFavorite
                     })
                 }}
                 onMouseEnter={() => {
                     setState({
                         ...state,
                         favoriteHover: true
                     })
                 }}
                 onMouseLeave={() => {
                     setState({
                         ...state,
                         favoriteHover: false
                     })
                 }}
                 style={{
                     cursor: 'pointer'
                 }}>
                {state.isFavorite ?
                    <Favorite style={{...iconStyle}}/> :
                    <FavoriteBorder style={{...iconStyle}}/>}
            </Box>
        </Tooltip>
    )
}

//endregion Actions

//region ProductCardPlaceHolder
export function ProductCardPlaceHolder(props) {
    return (
        <Box display="flex" width={1} justifyContent="center">
            <Box display="flex"
                 width="95%"
                 justifyContent="center">
                <Card
                    style={{
                        width: '100%',
                        boxShadow: 'unset'
                    }}>
                    <Box
                        display='block'
                        width={1}
                        height={{
                            xs: 120,
                            md: 170
                        }}>
                        <Skeleton
                            style={{
                                width: "100%",
                                height: "100%"
                            }}/>
                    </Box>
                    <Box
                        display='block'
                        width={1}
                        my={1}>
                        <Skeleton width="35%"/>
                        <Skeleton width="100%"/>
                    </Box>
                    <Box
                        width={1}
                        mb={1}
                        justifyContent="center"
                        display="flex">
                        <Box width="40%" justifyContent="center">
                            <Box display="inline-flex">
                                <Skeleton
                                    height={45}
                                    width={45}
                                    style={{
                                        margin: 0
                                    }}/>
                            </Box>
                            <Box display="inline-flex" mr={1}>
                                <Skeleton
                                    height={45}
                                    width={45}
                                    style={{
                                        margin: 0
                                    }}/>
                            </Box>
                        </Box>
                        <Box width="60%" display="flex" justifyContent="flex-end">
                            <Skeleton
                                height={45}
                                width="75%"
                                style={{
                                    margin: 0
                                }}/>
                        </Box>
                    </Box>
                </Card>
            </Box>
        </Box>
    )
}

//endregion ProductCardPlaceHolder

//endregion Component


