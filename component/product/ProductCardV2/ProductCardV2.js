import React, {useMemo} from "react";
import {Box, ButtonBase, HiddenSmUp, HiddenXsDown, Typography} from "material-ui-helper";
import {media, theme} from "../../../repository";
import Colors from "./Colors";
import DiscountPercent from "./DiscountPercent";
import Price from "./Price";
import Nlink from "../../base/link/NLink";
import rout from "../../../router";
import Img from "../../base/Img";
import {DEBUG} from "../../../pages/_app";
import MtIcon from "../../MtIcon";
import Skeleton from "./Skeleton";


//Todo: Check prefetch on product
export default function ProductCardV2({groupKey, width = 1, product, skeletonProps = {}, ...props}) {

    return (
        <React.Fragment>
            <HiddenXsDown>
                <LgProduct groupKey={groupKey} width={width} product={product}
                           skeletonProps={skeletonProps} {...props}/>
            </HiddenXsDown>
            <HiddenSmUp>
                <SmProduct groupKey={groupKey} width={width} product={product}
                           skeletonProps={skeletonProps} {...props}/>
            </HiddenSmUp>
        </React.Fragment>
    )
}

function LgProduct({groupKey, width = 1, product, skeletonProps = {}, ...props}) {

    const {id, thumbnail,available, permalink: perma, disable, colors, default_storage} = product || {};
    const {title} = default_storage || {};

    const permalink = useMemo(() => {
        return rout.Product.SingleV2.create(perma)
    }, [perma])

    if (!id)
        return <Skeleton groupKey={groupKey} width={width} {...props} {...skeletonProps}/>

    return (
        <Box
            width={width}
            flexDirectionColumn={true}
            py={2.25}
            px={2}
            {...props}>
            <Nlink
                prefetch={true}
                href={permalink}
                hoverStyle={false}>
                <Img
                    imageWidth={media.thumbnail.width}
                    imageHeight={media.thumbnail.height}
                    alt={thumbnail?.title}
                    src={thumbnail?.image}/>
            </Nlink>
            {
                disable &&
                <Box width={1} py={1}>
                    <Typography
                        width={1}
                        color={"#fff"}
                        variant={"subtitle1"}
                        px={2}
                        alignItems={'center'}
                        style={{
                            backgroundColor: theme.palette.error.light
                        }}>
                        <MtIcon
                            icon={"mt-exclamation-circle"}
                            color={"#fff"}
                            style={{
                                marginLeft: 10
                            }}/>
                        محصول پیشنمایش میباشد
                    </Typography>
                </Box>
            }
            <Colors colors={colors}/>
            <Box
                flexDirection={'column'}
                justifyContent={'flex-end'}
                style={{
                    height: "100%"

                }}>
                <Nlink
                    prefetch={true}
                    href={permalink}
                    className={"text-max-line2"} mt={2} pt={1} variant={"subtitle2"} fontWeight={"normal"}
                    hoverStyle={false}>
                    {title}
                </Nlink>
                <Box pt={3}>
                    <DiscountPercent value={default_storage?.discount_percent}/>
                    <Price
                        available={available}
                        discountPrice={default_storage?.discount_price}
                        finalPrice={default_storage?.final_price}/>
                </Box>
            </Box>
        </Box>
    )
}

function SmProduct({groupKey, width = 1, product, skeletonProps = {}, ...props}) {

    const {id,available, thumbnail, permalink: perma, colors, disable, default_storage} = product || {};
    const {title, discount_price, discount_percent, final_price} = default_storage || {};

    const permalink = useMemo(() => {
        return rout.Product.SingleV2.create(perma)
    }, [perma])

    if (!id)
        return <Skeleton groupKey={groupKey} width={width} {...props} {...skeletonProps}/>


    return (
        <ButtonBase
            disableTouchRipple={true}
            width={1}
            px={{xs: 2, lg: 0}}
            flexDirectionColumn={true}
            {...props}>
            <Nlink
                prefetch={true}
                width={1}
                href={permalink}
                hoverStyle={false}>
                <Box flexDirectionColumn={true} width={"40%"}>
                    <Box pr={1}>
                        <Img
                            alt={thumbnail?.title}
                            src={thumbnail?.image}
                            imageWidth={media.thumbnail.width}
                            imageHeight={media.thumbnail.height}/>
                    </Box>
                    <Colors/>
                </Box>
                <Box flexDirection={"column"} width={"60%"}>
                    {
                        disable &&
                        <Typography
                            width={1}
                            color={"#fff"}
                            variant={"body2"}
                            px={2}
                            mb={1.5}
                            py={0.5}
                            alignItems={'center'}
                            style={{
                                backgroundColor: theme.palette.error.light
                            }}>
                            محصول پیشنمایش میباشد
                        </Typography>
                    }
                    <Typography
                        fontWeight={300}
                                variant={{
                        xs: "subtitle2",
                        md: "subtitle1",
                    }}
                                style={{textAlign: 'start'}}>
                        {title}
                    </Typography>
                    <Price pt={2}
                           available={available}
                           discountPrice={discount_price} finalPrice={final_price}
                           discountPercent={discount_percent}/>
                </Box>
            </Nlink>
        </ButtonBase>
    )
}