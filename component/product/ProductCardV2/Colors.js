import React, {useMemo, useState} from "react";
import {Box, Tooltip, tryIt, UtilsStyle} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import {Add} from "@material-ui/icons";
import {DEBUG} from "../../../pages/_app";

export default function Colors({colors}) {

    return (
        (colors?.length > (DEBUG ? 0 : 1)) ?
            <ColorsBase colors={colors}/> :
            <React.Fragment/>
    )
}


function ColorsBase({colors: cos}) {
    const theme = useTheme()
    const [active, setActive] = useState()
    const [showAll, setShowAll] = useState(false)

    const colors = useMemo(() => {
        if (showAll || cos.length <= 8) {
            return cos
        }
        return cos.slice(0, 7)
    }, [cos, showAll])

    const handleClick = (color) => {
        tryIt(() => {
            if (color.id === active)
                return
            if (color.id === -1) {
                setShowAll(true)
                return;
            }
            setActive(color.id)
        })
    }


    return (
        <Box pt={3} pr={4.62} pl={4.62 - 1.25} width={1} flexWrap={"wrap"}>
            {
                colors.map((co, i) => {
                    return (
                        <ColorItem
                            key={co.id}
                            color={co}
                            onClick={handleClick}/>
                    )
                })
            }
            {
                (!showAll && cos.length > colors.length) &&
                <ColorItem
                    onClick={handleClick}
                    color={{
                        id: -1,
                        color: "#fff",
                    }}/>
            }
        </Box>
    )
}


function ColorItem({color}) {

    return (
        <Box
            width={1 / 12}
            pl={0.75}>
            <Box
                width={1}
                p={0.25}
                style={{
                    ...UtilsStyle.borderRadius("100%")
                }}>
                <Tooltip title={color?.name}>
                    <Box
                        className={`circle`}
                        center={true}
                        style={{
                            border: "1px solid #00000033",
                            backgroundColor: color.color,
                        }}>
                        {color.id === -1 &&
                        <Add fontSize={"small"}
                             style={{
                                 color: "#000",
                             }}/>}
                    </Box>
                </Tooltip>
            </Box>
        </Box>
    )
}
