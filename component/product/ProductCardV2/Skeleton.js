import React from "react";
import {Box, HiddenSmUp, HiddenXsDown, Skeleton as MaterialSkeleton} from "material-ui-helper";
import {media} from "../../../repository";
import Img from "../../base/Img";

export default function Skeleton({groupKey, width, ...props}) {

    return (
        <React.Fragment>
            <HiddenXsDown>
                <LgSkeleton groupKey={groupKey} width={width} {...props}/>
            </HiddenXsDown>
            <HiddenSmUp>
                <SmSkeleton groupKey={groupKey} width={width} {...props}/>
            </HiddenSmUp>
        </React.Fragment>
    )
}

function LgSkeleton({groupKey, width, ...props}) {
    return (
        <Box
            py={2.25}
            px={2}
            width={width}
            flexDirectionColumn={true}
            {...props}>
            <Img src={""}
                 alt={"thumbnail-skeleton"}
                 imageWidth={media.thumbnail.width}
                 imageHeight={media.thumbnail.height}/>
            <MaterialSkeleton borderRadius={3} width={2 / 3} mt={3} height={30}/>
            <Box width={1} mt={3}>
                <MaterialSkeleton borderRadius={3} width={1 / 4} height={30}/>
                <Box width={1} pl={3}>
                    <MaterialSkeleton borderRadius={3} height={30}/>
                </Box>
            </Box>
        </Box>
    )
}

function SmSkeleton({groupKey, width, ...props}) {

    return (
        <Box
            py={2.25}
            px={2}
            width={width}
            {...props}>
            <Box flexDirectionColumn={true} width={"40%"}>
                <Box width={1} pr={1}>
                    <Img
                        src={""}
                        groupKey={groupKey}
                        alt={"thumbnail-skeleton"}
                        imageWidth={media.thumbnail.width}
                        imageHeight={media.thumbnail.height}/>
                </Box>
            </Box>
            <Box width={"60%"}>
                <MaterialSkeleton borderRadius={3} width={2 / 3} mt={3} height={30}/>
            </Box>
        </Box>
    )
}

