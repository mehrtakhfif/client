import React from "react";
import {Box, HiddenSmUp, HiddenXsDown, Skeleton, Typography, UtilsStyle} from "material-ui-helper";
import {colors as siteColors, colors, lang, theme} from "../../../repository";
import {UtilsFormat} from "../../../utils/Utils";

export default function Price({available, finalPrice, discountPrice, discountPercent, ...props}) {

    return (
        <React.Fragment>
            <HiddenXsDown>
                <LgPrice
                    available={available}
                    discountPrice={discountPrice}
                    finalPrice={finalPrice} {...props}/>
            </HiddenXsDown>
            <HiddenSmUp>
                <SmPrice
                    available={available}
                    discountPrice={discountPrice} finalPrice={finalPrice}
                    discountPercent={discountPercent} {...props}/>
            </HiddenSmUp>
        </React.Fragment>
    )
}


function SmPrice({discountPrice, finalPrice, discountPercent, ...props}) {

    return (
        <Box flexDirection={"column"} {...props}>
            {
                discountPercent > 0 &&
                <Box alignItems={'center'} dir={'ltr'}>
                    <Box
                        center={true}
                        px={1}
                        py={0.25}
                        ml={0.5}
                        style={{
                            backgroundColor: theme.palette.primary.light,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Typography fontWeight={500} variant={'body2'} color={siteColors.textColor.hffffff}>
                            {discountPercent}%
                        </Typography>
                    </Box>
                    <Typography component={'del'} fontWeight={400} variant={'caption'}
                                colors={siteColors.textColor.h757575}>
                        {UtilsFormat.numberToMoney(finalPrice)}
                    </Typography>
                </Box>
            }
            <Box alignItems={"center"} dir={'ltr'} pt={1}>
                <Box ml={0.5}>
                    <Typography fontWeight={400} variant={'caption'} color={siteColors.textColor.h757575}>
                        {lang.get("toman")}
                    </Typography>
                </Box>
                <Typography fontWeight={500} variant={'body1'} color={siteColors.textColor.h000000}>
                    {UtilsFormat.numberToMoney(discountPrice)}
                </Typography>
            </Box>
        </Box>
    )
}


function LgPrice({available, discountPrice, finalPrice}) {

    if (!available)
        return (
            <Typography variant={"body1"} width={1} color={colors.textColor.h9e9e9e}>
                {lang.get("unavailable")}
            </Typography>
        )
    return (
        <Box px={2} flex={1} dir={'ltr'} alignCenter={true}>
            <Box alignItems={"flex-end"} alignCenter={true}>
                <Typography pl={0.7} pb={"3px"}
                            fontWeight={"normal"} variant={"body2"}
                            color={colors.textColor.h757575}>
                    {lang.get("toman")}
                </Typography>
                <Typography variant={"h6"} fontWeight={"bold"}>
                    {UtilsFormat.numberToMoney(discountPrice)}
                </Typography>
            </Box>
            {
                discountPrice < finalPrice &&
                <Typography color={colors.textColor.h9e9e9e} alignItems={"center"} component={"del"} pr={1}>
                    &nbsp;{UtilsFormat.numberToMoney(finalPrice)}&nbsp;
                </Typography>
            }
        </Box>
    )
}