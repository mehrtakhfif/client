import React from "react";
import {Typography, UtilsStyle} from "material-ui-helper";
import {useTheme} from "@material-ui/core";

export default function DiscountPercent({value}) {
    const theme = useTheme()

    return (
        value ?
            <Typography component={"span"}
                        color={"#fff"} variant={"h6"}
                        minWidth={theme.spacing(9)}
                        minHeight={theme.spacing(5)} center={true}
                        py={0.01}
                        px={0.03}
                        style={{
                            backgroundColor: theme.palette.primary.main,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                %{value}
            </Typography> :
            <React.Fragment/>
    )
}
