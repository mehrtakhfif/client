import React from "react";
import rout from "../../router";
import {siteLang} from "../../repository";
import Img from "../base/oldImg/Img";
import ServerRender from "../base/ServerRender";

export default function SSRProductCard({name, permalink, imageSrc, imageAlt, category, ...props}) {
    return (
        <ServerRender>
            <Img alt={imageAlt} src={imageSrc}/>
            <a href={rout.Product.SingleV2.create(permalink)?.[1]} rel={'search'} hrefLang={siteLang}>
                <h3>
                    {name}
                </h3>
            </a>
        </ServerRender>
    )
}
