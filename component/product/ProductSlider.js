import React, {createRef} from "react";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core";
import {colors} from "../../repository";
import clsx from "clsx";
import {ChevronLeft, ChevronRight} from "@material-ui/icons";
import Loadable from "react-loadable";
import ComponentError from "../base/ComponentError";
import {SpecialOffersColumnPlaceHolder} from "../home/SpecialProductsColumn";


const arrowStyles = makeStyles(theme => ({
    buttonIcon: {
        position: "absolute",
        zIndex: "2",
        backgroundColor: colors.backgroundColor.darker,
        color: "#fff",
        top: '50%',
        transform: 'translate(0, -50%)',
        bottom: '50%',
        cursor: 'pointer',
        opacity: '0.4',
        transition: 'all 200ms ease',
        '&:hover': {
            transform: 'scale(1.2) translate(0, -50%)',
            opacity: '1',
        },
        width: '35px',
        height: '50px',
        '& svg': {
            width: '35px',
            height: '50px'
        },

    },
    buttonLeftIcon: {
        left: 0,
        borderTopRightRadius: theme.spacing(2),
        borderBottomRightRadius: theme.spacing(2),
    },
    buttonRightIcon: {
        right: 0,
        borderTopLeftRadius: theme.spacing(2),
        borderBottomLeftRadius: theme.spacing(2),
    },
    buttonDisable: {
        opacity: 0,
        width: 0,
        '&:hover': {
            opacity: 0,
        }
    }
}));

function NextArrow(props) {
    const {onClick} = props;
    const classes = arrowStyles(props);
    const classesUse = [classes.buttonIcon, classes.buttonLeftIcon, 'transition500', 'transition500hover'];
    if (props.currentSlide === 0) {
        classesUse.push(classes.buttonDisable)
    }
    return (
        <Box
            className={clsx(...classesUse)}>
            <ChevronLeft onClick={onClick}/>
        </Box>
    );
}

function PrevArrow(props) {
    const {onClick} = props;
    const classes = arrowStyles(props);
    const classesUse = [classes.buttonIcon, classes.buttonRightIcon, 'transition500', 'transition500hover'];
    if (props.currentSlide >= (props.slideCount - 1)) {
        classesUse.push(classes.buttonDisable)
    }
    return (
        <Box
            className={clsx(...classesUse)}>
            <ChevronRight onClick={onClick}/>
        </Box>
    );
}




const productSliderStyles = makeStyles(theme => ({
    productSliderRoot: {
        position: 'relative',
        '& .tns-outer [data-action]': {
            display: 'none'
        },
        '& .tns-slider': {
            display: 'flex'
        }
    }
}));

const settings = {
    lazyload: true,
    nav: false,
    mouseDrag: true,
    loop: false,
    items: 1.5,
    controls: false,
    speed: 600,
    swipeAngle: false,
    center: false,
    edgePadding: 10,
    responsive: {
        450: {
            items: 2.5,
        },
        600: {
            items: 3.5,
        },
        960: {
            items: 2.5,
        },
        1920: {
            items: 3.5,
        }
    }
};

export default function ProductSlider({...props}) {
    let sliderRef = createRef();
    const classes = productSliderStyles(props);
    const onGoTo = dir => sliderRef.slider.goTo(dir);

    return (
        <Box dir='ltr'
             py={1}
             className={classes.productSliderRoot}
             style={{
                 zIndex: '9',
             }}
             {...props}>
            <NextArrow
                onClick={() => {
                    onGoTo('prev');
                }}/>
            <PrevArrow
                onClick={() => {
                    onGoTo('next');
                }}/>
        <TinySlider dir='ltr'
                    settings={settings}
                    tinyRef={ts => {
                        sliderRef = ts;
                    }}
                    style={{}}>
            {props.children}
        </TinySlider>
        </Box>
    );
}

const TinySlider = Loadable.Map({
    loader: {
        Component: () => import('tiny-slider-react-ar'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <SpecialOffersColumnPlaceHolder/>;
    },
    render(loaded, props) {
        const Component = loaded.Component.default;
        return <Component ref={props.tinyRef} {...props}/>;
        // return <Box>s</Box>
    },
});
