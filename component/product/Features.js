import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "../base/Typography";
import {cyan, grey} from "@material-ui/core/colors";
import BoxWithTopBorder from "../base/textHeader/BoxWithTopBorder";
import Card from "@material-ui/core/Card";
import _ from "lodash"


export default function Features({data, ...props}) {

    const {features, group_features} = data;


    return (
        (!_.isEmpty(features) || !(() => {
            let isEmpty = true;
            try {
                _.forEach(group_features, (gf) => {
                    isEmpty = _.isEmpty(gf.features);
                    return isEmpty
                })
            } catch (e) {
            }
            return isEmpty
        })()) ?
            <BoxWithTopBorder
                component={Card}
                title={"مشخصات"}
                borderColor={cyan[500]}
                borderStork={5}
                display={"flex"}
                flexDirection={"column"}
                mt={2}>
                <Box component={"table"}
                     px={2}
                     pt={1}
                     pb={2}>
                    <Container title={undefined} pb={_.isEmpty(group_features) ? 0 : 2} items={features}/>
                    {
                        group_features.map(({id, title, features}, i) => (
                            <Container key={id} pb={(group_features.length !== i + 1) ? 2 : 0} title={title}
                                       items={features}/>
                        ))
                    }
                </Box>
            </BoxWithTopBorder> :
            <React.Fragment/>
    )
}

function Container({title, items, ...props}) {

    return (
        !_.isEmpty(items) ?
            <React.Fragment>
                {title &&
                <Box dir={"rtl"} component={"tbody"}>
                    <Typography
                        component={"tr"}
                        variant={"h6"}
                        fontWeight={500}
                        color={grey[600]}
                        {...props}
                        style={{
                            ...props.style,
                            minWidth: 100,
                        }}>
                        {title}
                    </Typography>
                </Box>}
                <Box component={"tbody"}
                     display={'flex'} flexDirection={'column'}
                     pt={1}
                     pl={{
                         xs: 1,
                         md: 3
                     }}
                     {...(title ? {} : props)}>
                    {
                        items.map(it => (
                            <Item key={it.id} label={it.feature} value={it.feature_value}/>
                        ))
                    }
                </Box>
            </React.Fragment> :
            <React.Fragment/>
    )
}

function Item({label, value, ...props}) {
    return (
        <Box component={"tr"} display={'flex'} {...props} pt={2}>
            <Typography className={"feature-title"}
                        component={"td"}
                        variant={"subtitle1"}
                        fontWeight={'400'}
                        dir={"rtl"}
                        justifyContent={"end"}
                        pl={2}
                        style={{
                            width: 180,
                            minWidth: 180
                        }}>
                {label}
            </Typography>
            <Typography className={"feature-details"} component={"td"} variant={"subtitle1"}
                        color={grey[600]}
                        style={{
                            whiteSpace: 'pre-wrap'
                        }}>
                {_.isArray(value) ? value.map((v, i) => (
                    <React.Fragment key={v.id}>
                        {i !== 0 && " ،  "}
                        {v.name}
                    </React.Fragment>
                )) : value}
            </Typography>
        </Box>
    )
}
