import React, {useEffect, useState} from 'react';
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import {fade} from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import PropTypes from "prop-types";
import {UtilsStyle} from "../../../utils/Utils";

const searchStyle = makeStyles(theme => ({
    root: {},
    search: {
        position: 'relative',
        ...UtilsStyle.borderRadius(theme.shape.borderRadius),
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
        border: '2px solid #8BC34A',
        padding: theme.spacing(1),
        width: '100%',
        [theme.breakpoints.down('1250')]: {
            marginRight: 0,
            marginLeft: 0,
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        top: 0,
        right: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        [theme.breakpoints.down('1250')]: {
            width: 30,
            '& svg': {
                width: '0.8rem'
            }
        },
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        paddingRight: 35,
        '&::placeholder': {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.down('1250')]: {
            paddingRight: 20,
        }
    },
    inputInput: {
        padding: theme.spacing(1, 0, 0, 0),
        transition: theme.transitions.create('width'),
        fontSize: '0.9rem',
        width: '100%',
        '&::placeholder': {
            fontSize: '0.9rem',
        },
        [theme.breakpoints.down('1360')]: {
            '&::placeholder': {
                fontSize: '0.8rem',
            },
        },
        [theme.breakpoints.down('1250')]: {
            '&::placeholder': {
                fontSize: '0.77rem',
            },
        },
        [theme.breakpoints.down('1100')]: {
            '&::placeholder': {
                fontSize: '0.7rem',
            },
        },
        [theme.breakpoints.down('1015')]: {
            '&::placeholder': {
                fontSize: '0.66rem',
            },
        }
    },
}));

export default function Search({value, ...props}) {
    const classes = searchStyle(props);
    const [state, setState] = useState(value);
    useEffect(() => {
        ///test
        // setState(value)
        // return
        if (value === "")
            setState("")
    }, [value])
    return (
        <Box mx={1} className={classes.search} style={{...props.style}}>
            <div className={classes.searchIcon}>
                <SearchIcon/>
            </div>
            <InputBase
                value={state}
                placeholder={props.hint}
                onChange={(event) => {
                    setState(event.target.value);
                    props.onChange(event)
                }}
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
                inputProps={{'aria-label': 'search in box'}}
            />
        </Box>
    )
}

Search.propTypes = {
    hint: PropTypes.string,
    onChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
