import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core";
import {lang, theme} from "../../../repository";
import TreeView from "@material-ui/lab/TreeView";
import TreeItemComponent from "@material-ui/lab/TreeItem";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ContainerFilterBox from "./ContainerFilterBox"
import _ from 'lodash'
import Typography from "../../base/Typography";
import {UtilsStyle} from "../../../utils/Utils";

const treeViewStyle = makeStyles(theme => ({
    treeViewStyleRoot: {
        minHeight: '100px',
        flexGrow: 1,
        maxWidth: 400,
        '& > .MuiTreeItem-root': {
            marginRight: 5,
        },
        '& .MuiTreeItem-group': {
            marginRight: 25,
            marginLeft: 0,
        },
        '& .MuiTreeItem-label': {
            fontSize: 13,
            paddingRight: 5
        },
        '& .MuiTreeItem-iconContainer': {
            width: '0.8em'
        }
    },
    activeItem: {
        backgroundColor: 'rgba(0,0,0,0.04)'
    }
}));

export default function Categories({open,categories, activeCategory, onCategoryClick, ...props}) {
    const treeViewClasses = treeViewStyle(props);
    const [expanded, setExpanded] = React.useState([]);
    useEffect(() => {
        if (activeCategory) {
            setExpanded([
                ...expanded,
                _.toString(activeCategory.parent !== null ? activeCategory.parent : activeCategory.id)
            ])
        }
    }, [activeCategory]);

    function onCatClickHandler(permalink) {
        onCategoryClick(permalink)
    }

    return (
        <ContainerFilterBox
            expanded={open}
            header={lang.get('sort_results') + ":"}
            {...props}>
            <TreeView
                className={treeViewClasses.treeViewStyleRoot}
                defaultCollapseIcon={<ExpandMoreIcon/>}
                defaultExpandIcon={<ChevronLeftIcon/>}
                expanded={expanded}
                onNodeToggle={(event, nodes) => {

                    setExpanded(nodes);
                }}>
                {categories.map((cat, index) => (
                    <TreeItem key={index} nodeId={_.toString(cat.id)}
                              label={cat.name}
                              className={(activeCategory && !cat.child && cat.permalink === activeCategory.permalink) ? treeViewClasses.activeItem : null}
                              onClick={() => {
                                  if (!cat.child)
                                      onCatClickHandler(cat.permalink)
                              }}>
                        {cat.child &&
                        <>
                            <TreeItem key={index} nodeId={_.toString(_.toInteger(cat.id) + 300)}
                                      className={activeCategory && cat.permalink === activeCategory.permalink ? treeViewClasses.activeItem : null}
                                      onClick={() => onCatClickHandler(cat.permalink)}
                                      label={lang.get("show_all")}/>
                            {cat.child.map((cat1, index) => (
                                <TreeItem key={index} nodeId={_.toString(cat1.id)}
                                          className={activeCategory && cat1.permalink === activeCategory.permalink ? treeViewClasses.activeItem : null}
                                          label={cat1.name}
                                          onClick={() => onCatClickHandler(cat1.permalink)}/>
                            ))}
                        </>
                        }
                    </TreeItem>
                ))}
            </TreeView>
        </ContainerFilterBox>
    )
}

function TreeItem({nodeId, label, permalink = null, ...props}) {


    return (
        <TreeItemComponent nodeId={nodeId}
                           label={<Typography py={0.8} variant={"subtitle1"}>{label}</Typography>}
                           {...props}
                           style={{
                               overflow: 'hidden',
                               marginTop: theme.spacing(0.2),
                               marginBottom: theme.spacing(0.2),
                               ...UtilsStyle.borderRadius(2)
                           }}/>
    )
}
