import React from 'react';
import {lang} from "../../../repository";
import ContainerFilterBox from "./ContainerFilterBox"
import SearchComponent from "./SearchComponent"
import {debounce} from "lodash";


export default function Search({open,value,onChange,...props}) {
    const searchDebounced = debounce(handleSearch, 1000);

    function handleSearch(text) {
        onChange(text)
    }

    return (
        <ContainerFilterBox
            expanded={open}
            header={lang.get('search_in_results') + ":"}
            {...props}>
            <SearchComponent
                value={value}
                onChange={(event) => {
                    searchDebounced(event.target.value)
                }}
                hint={lang.get('type_the_product_or_brand_name_you_want')}/>
        </ContainerFilterBox>
    )
}
