import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    icon: {
        verticalAlign: 'bottom',
        height: 20,
        width: 20,
    },
    details: {
        alignItems: 'center',
    },
    column: {
        flexBasis: '33.33%',
    },
    helper: {
        borderLeft: `2px solid ${theme.palette.divider}`,
        padding: theme.spacing(1, 2),
    },
    link: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'underline',
        },
    },
}));

export default function ContainerFilterBox({expanded,detailsStyle,...props}) {
    const classes = useStyles();

    return (
        <Box className={classes.root} my={1.5} {...props}>
            <ExpansionPanel defaultExpanded={expanded}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1c-content"
                    id="panel1c-header">
                    <Box mx={0} my={1} fontWeight="fontWeightRegular" fontSize={16} component='h4'>
                        {props.header}
                    </Box>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.details} style={{...detailsStyle}}>
                    {props.children}
                </ExpansionPanelDetails>
                <Divider/>
                {
                    props.footer &&
                    <ExpansionPanelActions>
                        {props.footer}
                    </ExpansionPanelActions>
                }
            </ExpansionPanel>
        </Box>
    );
}

ContainerFilterBox.propTypes = {
    expanded: PropTypes.bool,
    header: PropTypes.string,
    footer: PropTypes.element,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    detailsStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
