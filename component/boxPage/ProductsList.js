import React from "react";
import Box from "@material-ui/core/Box";
import Hidden from "@material-ui/core/Hidden";
import {Sort} from "@material-ui/icons";
import {cyan, grey} from "@material-ui/core/colors";
import {lang, theme} from "../../repository";
import _ from "lodash";
import Grow from "@material-ui/core/Grow";
import {ProductCardPlaceHolder} from "../product/ProductCard";
import Loadable from "react-loadable";
import ComponentError from "../base/ComponentError";
import {makeStyles, withStyles} from "@material-ui/core";
import {UtilsStyle} from "../../utils/Utils";
import Chip from "@material-ui/core/Chip";
import clsx from "clsx";
import Switch from "@material-ui/core/Switch";
import {DEBUG} from "../../pages/_app";

export const orderByItems = [
    {
        label: 'most_newest',
        value: ''
    },
    {
        label: 'most_cheapest',
        value: 'cheap'
    },
    {
        label: 'most_expensive',
        value: 'expensive'
    },
    {
        label: 'most_bestseller',
        value: 'best_seller'
    },
    {
        label: 'most_popular',
        value: 'popular'
    },
    {
        label: 'most_discount',
        value: 'discount'
    },
];

export default function ProductsList({filter, onChangeOrder, data, ...props}) {
    const {orderBy} = filter;

    function chipsClickHandler(key) {
        onChangeOrder(key)
    }

    return (
        <Box py={1}
             px={{
            xs: 0,
            md: 1
        }}>

            {
                DEBUG &&
                <Hidden smDown>
                    <Box display='flex'>
                        <Box width='80%'
                             display="flex"
                             alignItems="center"
                             fontSize={16}>
                            <Sort fontSize="large"
                                  style={{color: grey[500], marginLeft: theme.spacing(1)}}/>
                            {lang.get('order_by')}:
                            <Box pr={1}>
                                {orderByItems.map((item, index) => (
                                    <Chips key={index} label={lang.get(item.label)}
                                           {...((orderBy === item) ? {active: ""} : null)}
                                           onClick={() => chipsClickHandler(item)}/>
                                ))}
                            </Box>
                        </Box>
                        <Box width='20%'
                             display='flex'
                             justifyContent='center'
                             alignItems="center">
                            {lang.get('only_available_product')}:
                            <IOSSwitch/>
                        </Box>
                    </Box>
                </Hidden>
            }
            <Box display='flex'
                 flexWrap='wrap'
                 alignItems='stretch'
                 width='100%' pt={2}>
                {
                    (!_.isEmpty(data)) ?
                        data.map((item, index) => (
                            <BoxItem
                                key={item.id}
                                index={index}
                                data={item}
                                elevation={4}
                                {...props}/>
                        ))
                        :
                        <>
                            <BoxItem data={null}/>
                            <BoxItem data={null}/>
                            <Hidden smDown={true}>
                                <BoxItem data={null}/>
                            </Hidden>
                        </>
                }
            </Box>
        </Box>
    )
}

//region component

function BoxItem({index, data, ...props}) {
    return (
        index ?
            <Grow
                in={true}
                style={{transformOrigin: '0 0 0'}}
                timeout={500 + (_.toInteger(index + 1) * 600)}>
                <Box display="flex"
                     justifyContent="center"
                     width={{
                         xs: '100%',
                         sm: '50%',
                         md: '33.3%',
                         lg: '33.3%',
                     }}
                     style={{
                         marginTop: theme.spacing(1),
                         marginBottom: theme.spacing(2)
                     }}>
                    {
                        data ?
                            <BoxItemProduct data={data} {...props}/>
                            :
                            <ProductCardPlaceHolder/>
                    }
                </Box>
            </Grow>
            :
            <Box display="flex"
                 justifyContent="center"
                 width={{
                     xs: '100%',
                     sm: '50%',
                     md: '33.3%',
                     lg: '33.3%',
                 }}
                 style={{
                     marginTop: theme.spacing(1),
                     marginBottom: theme.spacing(2)
                 }}>
                {
                    data ?
                        <BoxItemProduct data={data} {...props}/>
                        :
                        <ProductCardPlaceHolder/>
                }
            </Box>
    )
}

const BoxItemProduct = Loadable.Map({
    loader: {
        Component: () => import('../../component/product/ProductCard'),
    },
    loading: (props) => {
        if (props.error || props.timedOut) {
            return <ComponentError tryAgainFun={props.retry}/>
        }
        return <div>loading...</div>;
    },
    render(loaded, {data, ...props}) {
        const Component = loaded.Component.default;
        return (
            <Component
                id={data.id}
                name={data.name}
                permalink={data.permalink}
                discountPrice={data.discountPrice}
                discountPercent={data.discountPricePercent}
                finalPrice={data.finalPrice}
                imageSrc={data.thumbnail}
                imageAlt={data.name}
                category={2}
                availableCountForSale={1}
                style={{
                    height: '100%',
                    marginLeft: theme.spacing(1),
                    marginRight: theme.spacing(1),
                }}
                {...props}/>
        );
    },
});

//region chips
const chipsStyles = makeStyles(theme => (
    {
        root: {
            height: 30,
            padding: 1,
            marginRight: 2,
            marginLeft: 2,
            border: 0,
            ...UtilsStyle.borderRadius(5),
        },
        active: {
            backgroundColor: cyan['A400'],
            color: '#fff',
            ...UtilsStyle.transition(300),
            '&:hover': {
                backgroundColor: cyan['A400'] + ' !important',
                ...UtilsStyle.transition(300),
            },
            '&:focus': {
                backgroundColor: cyan['A400'] + ' !important',
                ...UtilsStyle.transition(300),
            }
        }
    }));

function Chips({label, ...props}) {
    const classes = chipsStyles(props);
    const itemClass = [classes.root];
    if (props.active !== undefined)
        itemClass.push(classes.active);
    return (
        <Chip className={clsx(itemClass)} variant="outlined" size="small" label={label} {...props}/>
    )
}

//endregion chips
const IOSSwitch = withStyles(theme => ({
    root: {
        width: 50,
        height: 24,
        padding: 0,
        margin: theme.spacing(1),
    },
    switchBase: {
        top: 2.5,
        left: 2,
        padding: 1,
        '&.Mui-focusVisible > span > span': {
            borderWidth: '0 !important',
            color: '#fff !important'
        },
        '&$checked': {
            transform: 'translateX(26px)',
            color: theme.palette.common.white,
            '& + $track': {
                backgroundColor: '#52d869',
                opacity: 1,
                border: 'none',
            },
        },
        '& + $track': {
            backgroundColor: '#bfbfbf',
            opacity: 1,
            border: 'none',
        },
        '&$focusVisible $thumb': {
            color: '#52d869',
            border: '6px solid #fff',
        },
    },
    thumb: {
        width: 17,
        height: 17,

    },
    track: {
        border: `1px solid ${theme.palette.grey[400]}`,
        backgroundColor: theme.palette.grey[50],
        opacity: 1,
        transition: `theme.transitions.create(['all'])`,
        ...UtilsStyle.borderRadius(20),
    },
    checked: {},
    focusVisible: {},
}))(({classes, ...props}) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}/>
    );
});
//endregion component
