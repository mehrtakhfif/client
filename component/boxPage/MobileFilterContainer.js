import React, {Fragment, useState} from 'react';
import {lang, theme} from "../../repository";
import Box from "@material-ui/core/Box";
import {cyan, grey} from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import {UtilsConverter, UtilsStyle} from "../../utils/Utils";
import {Ballot, FilterList, Sort} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "../base/Typography";
import Dialog from "@material-ui/core/Dialog";
import {orderByItems} from "./ProductsList";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import {smFooterHeight} from "../footer/FooterSm";


const useMobileFilterStyles = makeStyles(theme => (
    {
        drawer: {}
    }
))

export default function FilterContainer({activeOrder, onOrderByClick, ...props}) {
    //region variable
    const classes = useMobileFilterStyles(props);
    const [state, setState] = useState({
        drawer: false
    });

    const [orderByDialog, setOrderByDialog] = useState({
        open: false
    });
    //endregion variable

    //region functions
    //region handler
    function drawerStateHandler(drawerState) {
        setState({
            ...state,
            drawer: drawerState
        })
    }

    //endregion handler
    //endregion functions
    return (
        <Fragment>
            <SwipeableDrawer
                anchor="bottom"
                open={state.drawer}
                onClose={() => drawerStateHandler(false)}
                onOpen={() => drawerStateHandler(true)}
                className={classes.drawer}>
                <Typography
                    display={'flex'}
                    justifyContent={'center'}
                    variant={'h6'}
                    py={0.7}
                    style={{
                        cursor: 'pointer'
                    }}
                    onClick={() => drawerStateHandler(false)}>
                    {lang.get("close")}
                </Typography>
                {props.children}
            </SwipeableDrawer>
            <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                width={1}
                style={{
                    position: 'sticky',
                    bottom: smFooterHeight-5,
                    backgroundColor: "#fff",
                    zIndex:999
                }}>
                <Box flexGrow={1}
                     display="flex">
                    <Box width="50%"
                         display="flex"
                         style={{
                             borderLeft: "0.5px solid",
                             borderColor: grey[500]
                         }}>
                        <Button
                            onClick={() => drawerStateHandler(true)}
                            style={{
                                width: "100%"
                            }}>
                            <Box
                                py={0.5}
                                alignItems="center"
                                justifyContent="center">
                                <Box display='flex'
                                     alignItems='center'
                                     fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.8)}
                                     style={{
                                         color: grey[900]
                                     }}>
                                    <FilterList style={{marginLeft: theme.spacing(1)}}/>
                                    {lang.get("set_filter")}
                                </Box>
                            </Box>
                        </Button>
                    </Box>
                    <Box width="50%"
                         display="flex"
                         style={{
                             borderRight: "0.5px solid",
                             borderColor: grey[500]
                         }}>
                        <Button
                            onClick={() => setOrderByDialog({
                                ...orderByDialog,
                                open: true
                            })}
                            style={{
                                width: "100%"
                            }}>
                            <Box
                                py={0.5}
                                alignItems="center"
                                justifyContent="center">
                                <Box display='flex'
                                     alignItems='center'
                                     fontSize={UtilsConverter.addRem(theme.typography.h6.fontSize, 0.8)}
                                     style={{
                                         color: grey[900]
                                     }}>
                                    <Sort style={{marginLeft: theme.spacing(1)}}/>
                                    {lang.get("sorting")}
                                </Box>
                            </Box>
                        </Button>
                    </Box>
                </Box>
                {false &&
                <Box display="flex" px={2}
                     onClick={() => drawerStateHandler(true)}
                     style={{
                         borderRight: '1px solid',
                         borderColor: grey[500]
                     }}>
                    <IconButton aria-label="change layout">
                        <Ballot/>
                    </IconButton>
                </Box>}
            </Box>
            <Dialog
                onClose={() => {
                    setOrderByDialog({
                        ...orderByDialog,
                        open: false
                    })
                }}
                aria-labelledby="order-by-dialog"
                open={orderByDialog.open}>
                <Box display={'flex'} flexDirection={'column'} width={1} px={4} py={2} px={1}>
                    <List component="nav" aria-label="list of orders">
                        {orderByItems.map((item, index) => (
                            <ListItem key={index} button
                                      onClick={() => {
                                          onOrderByClick(item);
                                          setOrderByDialog({
                                              ...orderByDialog,
                                              open: false
                                          })
                                      }}
                                      style={{
                                          backgroundColor: activeOrder === item.label ? cyan['A400'] : null,
                                      }}>
                                <Typography display={'flex'}
                                            justifyContent={'center'}
                                            minWidth={'150px'}
                                            color={activeOrder === item.label ? '#fff' : null}
                                            py={0.5} variant={'h6'}>
                                    {lang.get(item.label)}
                                </Typography>
                            </ListItem>
                        ))}
                        <ListItem button
                                  onClick={() => {
                                      setOrderByDialog({
                                          ...orderByDialog,
                                          open: false
                                      })
                                  }}>
                            <Typography display={'flex'} justifyContent={'center'}
                                        minWidth={'150px'}
                                        py={0.5}
                                        px={2}
                                        variant={'h6'}
                                        color={'#fff'}
                                        style={{
                                            paddingRight: theme.spacing(1.5),
                                            paddingLeft: theme.spacing(1.5),
                                            paddingTop: theme.spacing(0.7),
                                            paddingBottom: theme.spacing(0.7),
                                            backgroundColor: grey[500],
                                            ...UtilsStyle.borderRadius(5),
                                        }}>
                                {lang.get('close')}
                            </Typography>
                        </ListItem>
                    </List>
                </Box>
            </Dialog>
        </Fragment>
    )
}
