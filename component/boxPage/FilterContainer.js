import React, {useEffect, useState} from 'react';
import {Card, makeStyles} from "@material-ui/core";
import Categories from "./filter/Categories";
import Search from "./filter/Search";
import PriceRange from "./filter/PriceRange";
import Brand from "./filter/Brands"
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../utils/Utils";
import Chip from "@material-ui/core/Chip";
import _ from 'lodash'
import {lang, theme} from '../../repository'
import {AttachMoney, CategoryOutlined, FilterList, Search as SearchIcon, Sort} from "@material-ui/icons";

const useStyles = makeStyles(theme => (
    {
        root: (props => ({
            padding: theme.spacing(2),
            paddingBottom: theme.spacing(4),
            ...UtilsStyle.transition(200),
        }))
    }));
export default function FilterContainer({
                                            filter,
                                            queryString,
                                            categories,
                                            onCategoryClick,
                                            minPrice,
                                            maxPrice,
                                            availableProduct,
                                            openFilters = {
                                                categories: true,
                                                search: true,
                                                pricingRange: true,
                                                brands: true
                                            },
                                            onQueryStringChange,
                                            onPriceRangeFilter,
                                            brands,
                                            onBrandClick,
                                            onFilterRemove,
                                            ...props
                                        }) {

    const classes = useStyles();
    //region openFilters
    if (_.isBoolean(openFilters)) {
        openFilters = {
            categories: openFilters,
            search: openFilters,
            pricingRange: openFilters,
            brands: openFilters
        }
    } else {
        if (!openFilters.categories)
            openFilters.categories = true;
        if (!openFilters.search)
            openFilters.search = true;
        if (!openFilters.pricingRange)
            openFilters.pricingRange = true;
        if (!openFilters.brands)
            openFilters.brands = true;
    }
    //endregion openFilters


    const [state, setState] = useState({
        activeCat: null,
        activeBr: null
    });

    useEffect(() => {
        if (!filter.categoryPermalink) {
            setState({
                ...state,
                activeCat: null
            });
            return
        }
        getObject();
    }, [filter.categoryPermalink]);

    function getObject(theObject = categories, permalink = filter.categoryPermalink) {
        let result = null;
        if (theObject instanceof Array) {
            for (let i = 0; i < theObject.length; i++) {
                result = getObject(theObject[i]);
                if (result) {
                    setState({
                        activeCat: result
                    });
                }
            }
        } else {
            for (const prop in theObject) {
                if (prop === 'permalink') {
                    if (theObject[prop] === permalink) {
                        return theObject;
                    }
                }
                if (theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                    result = getObject(theObject[prop]);
            }
        }
        return result;
    }


    return (
        <Box className={classes.root}>
            <FilterSummery mb={2} filter={filter}
                           queryString={queryString}
                           categories={categories}
                           availableProduct={availableProduct}
                           brands={brands}
                           activeCategory={state.activeCat}
                           onFilterRemove={onFilterRemove}/>

            <Categories open={openFilters.categories} categories={categories} activeCategory={state.activeCat}
                        onCategoryClick={onCategoryClick}/>
            <Search open={openFilters.search} value={queryString} onChange={onQueryStringChange}/>

            <PriceRange open={openFilters.pricingRange} minPrice={minPrice} maxPrice={maxPrice}
                        activePrice={{
                            minPrice: filter.minPrice,
                            maxPrice: filter.maxPrice
                        }}
                        onPriceRangeFilter={onPriceRangeFilter}/>
            {!_.isEmpty(brands) &&
            <Brand brands={brands}
                   activeBrands={filter.brands}
                   open={openFilters.brands}
                   onItemClicked={onBrandClick}/>}
        </Box>
    )
}


const useFilterSummery = makeStyles(theme => ({
    rootFilterSummery: {},
    chip: {
        marginLeft: theme.spacing(1),
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
    }
}));

function FilterSummery({filter, categories, brands: brandsData, activeCategory,availableProduct, onFilterRemove, ...props}) {
    const classes = useFilterSummery();
    const {queryString, orderBy, categoryPermalink, minPrice, maxPrice, brands} = filter;


    const hasFilter = (queryString || orderBy || activeCategory || (minPrice && maxPrice) || (brands && !_.isEmpty(brands)));

    return (
        hasFilter ?
            <Box component={Card} className={classes.rootFilterSummery} p={1} {...props}>
                {queryString ?
                    <Chip
                        className={classes.chip}
                        key={"queryString"}
                        label={queryString}
                        icon={<SearchIcon/>}
                        onDelete={() => {
                            onFilterRemove('queryString')
                        }}/> : <React.Fragment/>
                }
                {orderBy ?
                    <Chip
                        className={classes.chip}
                        key={orderBy.label}
                        label={lang.get(orderBy.label)}
                        icon={<Sort/>}
                        onDelete={() => {
                            onFilterRemove('orderBy')
                        }}/> : <></>
                }
                {activeCategory ?
                    <Chip
                        className={classes.chip}
                        key={categoryPermalink}
                        label={activeCategory.name}
                        icon={<CategoryOutlined/>}
                        onDelete={() => {
                            onFilterRemove('categoryPermalink')
                        }}/> : <></>
                }
                {(minPrice && maxPrice) ?
                    <Chip
                        key={'minMax'}
                        className={classes.chip}
                        label={lang.get("from_to", {args: {min: _.toString(minPrice), max: _.toString(maxPrice)}})}
                        icon={<AttachMoney/>}
                        onDelete={() => {
                            onFilterRemove('minMax')
                        }}
                        style={{
                            marginLeft: theme.spacing(1)
                        }}/> : <></>
                }
                {(brands && !_.isEmpty(brands)) ?
                    <React.Fragment>
                        {
                            brands.map((b, i) => {
                                const d = _.find(brandsData, (bd) => _.toString(bd.id) === b);
                                return (
                                    d ?
                                        <Chip
                                            key={d.id}
                                            className={classes.chip}
                                            label={d.name}
                                            icon={<FilterList/>}
                                            onDelete={() => {
                                                onFilterRemove('brand', b)
                                            }}
                                            style={{
                                                marginLeft: theme.spacing(1)
                                            }}/> :
                                        <React.Fragment key={i}/>
                                )
                            })
                        }
                    </React.Fragment> : <React.Fragment/>
                }
            </Box> :
            <Box>
            </Box>
    )
}
