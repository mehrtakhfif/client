import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import Hidden from "@material-ui/core/Hidden";
import {Sort} from "@material-ui/icons";
import {cyan, grey} from "@material-ui/core/colors";
import {lang, theme} from "../../repository";
import _ from "lodash";
import Grow from "@material-ui/core/Grow";
import BoxItemProduct, {ProductCardPlaceHolder} from "../product/ProductCard";
import ComponentError from "../base/ComponentError";
import {makeStyles, withStyles} from "@material-ui/core";
import {UtilsStyle} from "../../utils/Utils";
import Chip from "@material-ui/core/Chip";
import clsx from "clsx";
import Switch from "@material-ui/core/Switch";
import ControllerProduct from "../../controller/ControllerProduct";
import useSWR, {mutate, useSWRInfinite as useSWRPages} from "swr";
import Button from "@material-ui/core/Button";
import {useRouter} from "next/router";
import {defaultOrder, orderByItems} from '../../pages/filter/[...params]'
import {gcLog} from "../../utils/ObjectUtils";


export default function
    ProductsList({
                                         initialData,
                                         filterProps,
                                         filter,
                                         availableProduct,
                                         onChangeOrder,
                                         onAvailableProductChange,
                                         ...props
                                     }) {
    const {box, cat, orderBy, queryString, brand, pricing} = filterProps;

    const router = useRouter();
    useEffect(() => {
        if (f && !_.isEmpty(f.pageSWRs) && f.pageSWRs[0].revalidate) {
            _.forEach(f.pageSWRs, (f, i) => {
                // f.mutate();
            })
        }
    }, [router.query])
    //region getData
    const d = ControllerProduct.Query.products();
    let apiKey = `filter-box${box}-cat${cat || ""}-available${availableProduct}-o${orderBy.value}-q${queryString}-price${pricing.minp}_${pricing.maxp}`;
    _.forEach(brand, (b, i) => {
        apiKey = apiKey + `brand${b}`
    });

    const {
        pages,
        isLoadingMore,
        isReachingEnd,
        loadMore,
        ...f
    } = useSWRPages(
        apiKey,
        ({offset, withSWR}) => {

            const {data, error} = withSWR(
                // eslint-disable-next-line react-hooks/rules-of-hooks
                useSWR(apiKey + (offset || 1), () => {
                    return d[1]({page: offset || 1, ...getFilterProps()})
                }, {
                    initialData: initialData,
                })
            );

            if (error) {
                return <ComponentError
                    tryAgainFun={() => {
                        mutate(apiKey)
                    }}/>
            }

            if (!data) {
                return <LoadingComponent/>
            }


            return data.products.map((item, index) =>
                <BoxItem
                    key={item.id}
                    index={index}
                    data={item}
                    elevation={4}/>
            )
        },
        // get next page's offset from the index of current page
        (SWR, index) => {
            if (SWR.data && SWR.data.pagination.lastPage <= index + 1) return null;
            return (index + 2)
        },
        [filterProps, box, cat, queryString, orderBy, brand, pricing, apiKey]
    );

    //endregion getData

    function chipsClickHandler(key) {
        onChangeOrder(key)
    }


    function getFilterProps() {
        const filter = {
            b: box !== "all" ? box : undefined,
            available: availableProduct ? true : undefined
        };
        if (cat) {
            filter.cat = cat
        }
        if (queryString) {
            filter.q = queryString
        }
        if (orderBy && orderBy.value !== defaultOrder.value) {
            filter.o = orderBy.value
        }
        if (!_.isEmpty(brand)) {
            filter.brand = brand;
        }


        if (pricing.maxp && pricing.maxp > 0) {
            filter.min_price = pricing.minp;
            filter.max_price = pricing.maxp;
        }
        return filter
    }

    return (
        <Box
            py={1}
            px={{
                xs: 0,
                md: 1
            }}>
            <Hidden smDown>
                <Box display='flex'>
                    <Box width='80%'
                         display="flex"
                         alignItems="center"
                         fontSize={16}>
                        <Sort fontSize="large"
                              style={{color: grey[500], marginLeft: theme.spacing(1)}}/>
                        {lang.get('order_by')}:
                        <Box pl={1}>
                            {orderByItems.map((item, index) => (
                                <Chips key={index} label={lang.get(item.label)}
                                       {...((orderBy === item) ? {active: ""} : null)}
                                       onClick={() => chipsClickHandler(item)}/>
                            ))}
                        </Box>
                    </Box>
                    <Box width='20%'
                         display='flex'
                         justifyContent='center'
                         alignItems="center">
                        {lang.get('only_available_product')}:
                        <IOSSwitch
                            checked={availableProduct}
                            onChange={() => {
                                onAvailableProductChange(!availableProduct)
                            }}/>
                    </Box>
                </Box>
            </Hidden>
            <Box display='flex'
                 flexWrap='wrap'
                 alignItems='stretch'
                 width='100%' pt={2}>
                <>
                    {pages}
                    <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                        {
                            (!isReachingEnd && !isLoadingMore) &&
                            <Button onClick={() => loadMore()}>
                                {lang.get("load_more")}
                            </Button>
                        }
                    </Box>
                </>
            </Box>
        </Box>
    )
}

export const getProductsListInitialProps = async function ({query, ...ctx}) {
    if (query && !query.b)
        return {};

    const data = await ControllerProduct.Query.products()[1]({
        headers: ctx.req ? {Cookie: ctx.req.headers.cookie} : undefined,
        page: 1,
        ...getFilterProps(query),
    });
    return {
        initialData: data
    }
};


//region component

function LoadingComponent() {
    return (
        <>
            <BoxItem index={1} data={null}/>
            <BoxItem index={2} data={null}/>
            <Hidden smDown={true}>
                <BoxItem index={3} data={null}/>
            </Hidden>
        </>
    )
}


function BoxItem({index, data, ...props}) {

    gcLog("safjasjashjfhashf", data)
    return (
        <Grow
            in={true}
            style={{transformOrigin: '0 0 0'}}
            timeout={500 + (_.toInteger(index + 1) * 100)}>
            <Box display="flex"
                 justifyContent="center"
                 width={{
                     xs: '50%',
                     sm: '50%',
                     md: '33.3%',
                     lg: '33.3%',
                 }}
                 style={{
                     marginTop: theme.spacing(1),
                     marginBottom: theme.spacing(2)
                 }}>
                {
                    data ?
                        <>
                            <BoxItemProduct id={data.id}
                                            name={data.name}
                                            disable={data.disable}
                                            permalink={data.permalink}
                                            discountPrice={data.discountPrice}
                                            discountPercent={data.discountPercent}
                                            finalPrice={data.finalPrice}
                                            imageSrc={data.thumbnail}
                                            imageAlt={data.name}
                                            category={2}
                                            availableCountForSale={1}
                                            style={{
                                                height: '100%',
                                                marginLeft: theme.spacing(1),
                                                marginRight: theme.spacing(1),
                                            }}
                                            {...props}/>
                        </>
                        :
                        <ProductCardPlaceHolder/>
                }
            </Box>
        </Grow>
    )
}


//region chips
const chipsStyles = makeStyles(theme => (
    {
        root: {
            height: 30,
            padding: 1,
            marginRight: 2,
            marginLeft: 2,
            border: 0,
            ...UtilsStyle.borderRadius(5),
        },
        active: {
            backgroundColor: cyan['A400'],
            color: '#fff',
            ...UtilsStyle.transition(300),
            '&:hover': {
                backgroundColor: cyan['A400'] + ' !important',
                ...UtilsStyle.transition(300),
            },
            '&:focus': {
                backgroundColor: cyan['A400'] + ' !important',
                ...UtilsStyle.transition(300),
            }
        }
    }));

function Chips({label, ...props}) {
    const classes = chipsStyles(props);
    const itemClass = [classes.root];
    if (props.active !== undefined)
        itemClass.push(classes.active);
    return (
        <Chip className={clsx(itemClass)} variant="outlined" size="small" label={label} {...props}/>
    )
}

//endregion chips
const IOSSwitch = withStyles(theme => ({
    root: {
        width: 50,
        height: 24,
        padding: 0,
        margin: theme.spacing(1),
    },
    switchBase: {
        top: 2.5,
        left: 2,
        padding: 1,
        '&.Mui-focusVisible > span > span': {
            borderWidth: '0 !important',
            color: '#fff !important'
        },
        '&$checked': {
            transform: 'translateX(26px)',
            color: theme.palette.common.white,
            '& + $track': {
                backgroundColor: '#52d869',
                opacity: 1,
                border: 'none',
            },
        },
        '& + $track': {
            backgroundColor: '#bfbfbf',
            opacity: 1,
            border: 'none',
        },
        '&$focusVisible $thumb': {
            color: '#52d869',
            border: '6px solid #fff',
        },
    },
    thumb: {
        width: 17,
        height: 17,

    },
    track: {
        border: `1px solid ${theme.palette.grey[400]}`,
        backgroundColor: theme.palette.grey[50],
        opacity: 1,
        transition: `theme.transitions.create(['all'])`,
        ...UtilsStyle.borderRadius(20),
    },
    checked: {},
    focusVisible: {},
}))(({classes, ...props}) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}/>
    );
});
//endregion component
