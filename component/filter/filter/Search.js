import React from 'react';
import {lang} from "../../../pages/BaseSite";
import ContainerFilterBox from "./ContainerFilterBox"
import SearchComponent from "./SearchComponent"
import {debounce} from "lodash";


export default function Search({open,...props}) {
    const searchDebounced = debounce(handleSearch, 1000);

    function handleSearch(text) {

    }

    return (
        <ContainerFilterBox
            expanded={open}
            header={lang.get('search_in_results') + ":"}
            {...props}>
            <SearchComponent
                onChange={(event) => {
                    searchDebounced(event.target.value)
                }}
                hint={lang.get('type_the_product_or_brand_name_you_want')}/>
        </ContainerFilterBox>
    )
}
