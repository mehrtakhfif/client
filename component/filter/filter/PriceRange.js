import React, {useEffect, useState} from 'react';
import Box from "@material-ui/core/Box";
import {MuiThemeProvider, withStyles} from "@material-ui/core";
import _ from 'lodash'
import Slider from "@material-ui/core/Slider";
import ContainerFilterBox from "./ContainerFilterBox"
import ArrowForward from "@material-ui/icons/ArrowForward"
import FilterList from "@material-ui/icons/FilterList"
import SuccessButton from "../../base/button/buttonVariant/SuccessButton";
import {lang, theme} from "../../../repository";
import {UtilsFormat} from "../../../utils/Utils";

const AirbnbSlider = withStyles({
    root: {
        color: '#3a8589',
        height: 3,
        padding: '13px 0',
    },
    thumb: {
        height: 27,
        width: 27,
        backgroundColor: '#fff',
        border: '1px solid currentColor',
        marginTop: -12,
        marginLeft: -13,
        boxShadow: '#ebebeb 0px 2px 2px',
        '&:focus,&:hover,&$active': {
            boxShadow: '#ccc 0px 2px 3px 1px',
        },
        '& .bar': {
            height: 9,
            width: 2,
            backgroundColor: 'currentColor',
            marginLeft: 1,
            marginRight: 1,
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 4px)',
    },
    track: {
        height: 3,
    },
    rail: {
        color: '#d8d8d8',
        opacity: 1,
        height: 3,
    },
})(Slider);

function AirbnbThumbComponent(props) {
    return (
        <span {...props}>
          <span className="bar"/>
          <span className="bar"/>
          <span className="bar"/>
        </span>
    );
}

function valuetext(value) {
    return `${value}°C`;
}

export default function PriceRange({
    open,
                                       minPrice,
                                       maxPrice,
                                       activePrice,
                                       onPriceRangeFilter,
                                       ...props
                                   }) {
    const [range, setRange] = useState([
        (activePrice && activePrice.minPrice) ? (((activePrice.minPrice - minPrice) * 100) / (maxPrice - minPrice)) : 30,
        (activePrice && activePrice.maxPrice) ? (((activePrice.maxPrice - minPrice) * 100) / (maxPrice - minPrice)) : 60
    ]);

    useEffect(() => {

    });

    function getMinPrice() {
        return minPrice + Math.floor((_.toInteger(((maxPrice + minPrice) / 2) * (range[0] / 100))) / 100) * 100
    }

    function getMaxPrice() {
        const rounder = range[1] === 100 ? 1000 : 100;
        return minPrice + 1000 + Math.ceil(_.toInteger(((maxPrice + minPrice) / 2) * (range[1] / 100)) / rounder) * rounder
    }

    return (
        <ContainerFilterBox
            expanded={open}
            header={lang.get('your_pricing_range') + ":"}
            {...props}>
            <Box display="flex"
                 flexDirection="column"
                 alignItems="center"
                 width="100%">
                <MuiThemeProvider theme={{
                    ...theme,
                    direction: "ltr"
                }}>
                    <AirbnbSlider
                        valueLabelDisplay="auto"
                        aria-labelledby="price range"
                        getAriaLabel={index => (index === 0 ? 'Minimum price' : 'Maximum price')}
                        defaultValue={range}
                        style={{
                            width: "100%"
                        }}
                        ThumbComponent={AirbnbThumbComponent}
                        onChange={(e, value) => {
                            if ((value[1] - value[0]) > 5)
                                setRange(value)
                        }}
                        marks={[
                            {
                                value: 50,
                                label: <Box
                                    fontSize={11}>{UtilsFormat.numberToMoney(Math.round(_.toNumber((minPrice + maxPrice) / 2) / 1000) * 1000)}</Box>,
                            }
                        ]}/>
                </MuiThemeProvider>
                <Box mt={2}
                     display='flex'
                     flexDirection='row-reverse'
                     justifyContent="center"
                     alignItems="center"
                     width="100%">
                    <Box display='flex'
                         flexDirection="column"
                         width="47%"
                         justifyContent="center"
                         alignItems="center">
                        <Box py={1} fontSize={20}>
                            {UtilsFormat.numberToMoney(getMinPrice())}
                        </Box>
                        <Box>
                            {lang.get('toman')}
                        </Box>
                    </Box>
                    <Box display='flex'
                         flexDirection="column"
                         width="6%"
                         justifyContent="center"
                         alignItems="center">
                        <Box py={1} fontSize={18}>
                            تا
                        </Box>
                        <ArrowForward/>
                    </Box>
                    <Box
                        display='flex'
                        flexDirection="column"
                        justifyContent="center"
                        alignItems="center"
                        width="47%">
                        <Box py={1} fontSize={20}>
                            {UtilsFormat.numberToMoney(getMaxPrice())}
                        </Box>
                        <Box>
                            {lang.get('toman')}
                        </Box>
                    </Box>
                </Box>
                <SuccessButton variant="contained"
                               loading={false}
                               onClick={() => onPriceRangeFilter({
                                   minPrice: getMinPrice(),
                                   maxPrice: getMaxPrice()
                               })}
                               style={{
                                   marginTop: theme.spacing(3),
                                   width: "fit-content"
                               }}
                               size="large">
                    <FilterList style={{
                        marginLeft: theme.spacing(1)
                    }}/>
                    اعمال محدودیت قیمت
                </SuccessButton>
            </Box>
        </ContainerFilterBox>
    )
}
