import React, {useState} from 'react';
import {createTheme, makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import {lang} from "../../../pages/BaseSite";
import ContainerFilterBox from "./ContainerFilterBox";
import Divider from "@material-ui/core/Divider";
import SearchComponent from "./SearchComponent"
import _, {debounce} from 'lodash'

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
    },
    listSection: {
        backgroundColor: 'inherit',
    },
    ul: {
        backgroundColor: 'inherit',
        padding: 0,
    },
}));

export default function Brands({brands, activeBrands, open, onItemClicked, ...props}) {

    const theme = createTheme();
    const classes = useStyles();
    const [data, setData] = useState({
        items: brands,
        selectedItem: []
    });
    const searchDebounced = debounce(handleSearch, 200);

    function handleSearch(text) {
        if (text.length === 0) {
            const newData = Object.assign({}, data, {items: brands});
            setData(newData);
            return
        }
        const newData = Object.assign({}, data, {
            items: _.filter(brands, function (item, i) {
                if (_.lowerCase(item.name).match(text)) {
                    return true;
                }
            })
        });
        setData(newData)
    }


    function getItemChecked(item) {
        const id = _.toString(item.id);
        const index = _.findIndex(activeBrands, function (o) {
            return o === id
        });
        return index !== -1
    }

    function onCheckBoxClicked(item) {
        onItemClicked({
            item: item,
            checked: !getItemChecked(item)
        })
    }

    return (
        brands &&
        <ContainerFilterBox
            expanded={open}
            header={lang.get('brand') + ":"}
            style={{...props.style}}
            detailsStyle={{
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0
            }}>
            <Box display='flex' flexDirection='column' width='100%'>
                <SearchComponent
                    hint={lang.get('type_the_brand_name_you_want')}
                    onChange={(event) => {
                        searchDebounced(_.lowerCase(event.target.value))
                    }}
                    style={{
                        width: 'auto',
                        margin: theme.spacing(1, 2)
                    }}/>
                <Divider/>
                <List className={classes.root} subheader={<li/>}>
                    {data.items.map(item => (
                        <ListItem key={`item-${item.id}`}
                                  onClick={() => {
                                      onCheckBoxClicked(item)
                                  }}
                                  style={{
                                      textAlign: 'start',
                                      cursor: 'pointer'
                                  }}>
                            <ListItemText primary={item.name}/>
                            <ListItemSecondaryAction>
                                <Checkbox
                                    checked={getItemChecked(item)}
                                    edge="end"
                                    inputProps={{'aria-labelledby': item.name}}
                                    onChange={(e, k) => {
                                        onCheckBoxClicked(item)
                                    }}
                                />
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))}
                </List>
            </Box>
        </ContainerFilterBox>
    );
}
