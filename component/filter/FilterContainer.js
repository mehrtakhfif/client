import React, {useEffect, useState} from 'react';
import {Card, makeStyles} from "@material-ui/core";
import Categories from "./filter/Categories";
import Search from "./filter/Search";
import PriceRange from "./filter/PriceRange";
import Brand from "./filter/Brands"
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../utils/Utils";
import Chip from "@material-ui/core/Chip";
import _ from 'lodash'
import {lang, theme} from '../../repository'
import {AttachMoney, Category, Sort} from "@material-ui/icons";

const useStyles = makeStyles(theme => (
    {
        root: (props => ({
            padding: theme.spacing(2),
            paddingBottom: theme.spacing(4),
            ...UtilsStyle.transition(200),
        }))
    }));
export default function FilterContainer({
                                            filter,
                                            categories,
                                            onCategoryClick,
                                            minPrice,
                                            maxPrice,
                                            openFilters = {
                                                categories: true,
                                                search: true,
                                                pricingRange: true,
                                                brands: true
                                            },
                                            onPriceRangeFilter,
                                            brands,
                                            onBrandClick,
                                            onFilterRemove,
                                            ...props
                                        }) {
    const classes = useStyles();
    //region openFilters
    if (_.isBoolean(openFilters)) {
        openFilters = {
            categories: openFilters,
            search: openFilters,
            pricingRange: openFilters,
            brands: openFilters
        }
    } else {
        if (!openFilters.categories)
            openFilters.categories = true;
        if (!openFilters.search)
            openFilters.search = true;
        if (!openFilters.pricingRange)
            openFilters.pricingRange = true;
        if (!openFilters.brands)
            openFilters.brands = true;
    }
    //endregion openFilters


    const [state, setState] = useState({
        activeCat: null,
        activeBr:null
    });

    useEffect(() => {
        if (!filter.categoryPermalink) {
            setState({
                ...state,
                activeCat: null
            });
            return
        }
        getObject();
    }, [filter.categoryPermalink]);

    function getObject(theObject = categories, permalink = filter.categoryPermalink) {
        let result = null;
        if (theObject instanceof Array) {
            for (let i = 0; i < theObject.length; i++) {
                result = getObject(theObject[i]);
                if (result) {
                    setState({
                        activeCat: result
                    });
                }
            }
        } else {
            for (const prop in theObject) {
                if (prop === 'permalink') {
                    if (theObject[prop] === permalink) {
                        return theObject;
                    }
                }
                if (theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                    result = getObject(theObject[prop]);
            }
        }
        return result;
    }

    return (
        <Box className={classes.root}>
            <FilterSummery mb={2} filter={filter} categories={categories} activeCategory={state.activeCat}
                           onFilterRemove={onFilterRemove}/>
            <Categories open={openFilters.categories} categories={categories} activeCategory={state.activeCat}
                        onCategoryClick={onCategoryClick}/>
            {false &&
            <Search open={openFilters.search}/>
            }
            <PriceRange open={openFilters.pricingRange} minPrice={minPrice} maxPrice={maxPrice}
                        activePrice={{
                            minPrice: filter.minPrice,
                            maxPrice: filter.maxPrice
                        }}
                        onPriceRangeFilter={onPriceRangeFilter}/>
            <Brand brands={brands}
                   activeBrands={filter.brands}
                   open={openFilters.brands}
                   onItemClicked={onBrandClick}/>
        </Box>
    )
}


const useFilterSummery = makeStyles(theme => ({
    rootFilterSummery: {

    },
    chip: {
        marginLeft: theme.spacing(1),
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
    }
}));

function FilterSummery({filter, categories, activeCategory, onFilterRemove, ...props}) {
    const classes = useFilterSummery();
    const {orderBy, categoryPermalink, minPrice, maxPrice} = filter;


    const hasFilter = (orderBy || activeCategory || (minPrice && maxPrice));

    return (
        hasFilter ?
            <Box component={Card} className={classes.rootFilterSummery} p={1} {...props}>
                {orderBy ?
                    <Chip
                        className={classes.chip}
                        key={orderBy.label}
                        label={lang.get(orderBy.label)}
                        icon={<Sort/>}
                        onDelete={() => {
                            onFilterRemove('orderBy')
                        }}/> : <></>
                }
                {activeCategory ?
                    <Chip
                        className={classes.chip}
                        key={categoryPermalink}
                        label={activeCategory.name}
                        icon={<Category/>}
                        onDelete={() => {
                            onFilterRemove('categoryPermalink')
                        }}/> : <></>
                }
                {(minPrice && maxPrice) ?
                    <Chip
                        key={'minMax'}
                        className={classes.chip}
                        label={lang.get("from_to", {args:{min: _.toString(minPrice), max: _.toString(maxPrice)}})}
                        icon={<AttachMoney/>}
                        onDelete={() => {
                            onFilterRemove('minMax')
                        }}
                        style={{
                            marginLeft: theme.spacing(1)
                        }}/> : <></>
                }
            </Box> :
            <Box>
            </Box>
    )
}
