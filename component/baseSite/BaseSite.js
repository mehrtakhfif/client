import React, {useCallback, useState} from "react";
import Box from "@material-ui/core/Box";
import Header from "../header/Header";
import {dir, siteBackground} from "../../repository";
import Footer from "../footer/Footer";
import ErrorBoundary from "../base/ErrorBoundary";
import Typography from "../base/Typography";
import packageJson from "../../package.json";
import {getSafe, tryIt, UtilsObject} from "material-ui-helper";
import BaseComponentContainer from "./BaseComponentContainer";
import _ from "lodash"
import HeaderAndFooterContext, {defaultHeaderAndFooterContext} from "../../context/HeaderAndFooterContext";
import {useUserContext} from "../../context/UserContext";

export default function BaseSite({children, pageProps, ...props}) {

    const [visibility, setVisibility] = useState(defaultHeaderAndFooterContext)

    const handleVisibilityChange = useCallback((newD) => {
        tryIt(() => {
            const d = UtilsObject.smartAssign(defaultHeaderAndFooterContext, newD)
            if (!_.isEqual(visibility, d))
                setVisibility(d)
        })
    }, [visibility])

    return (
        <HeaderAndFooterContext.Provider value={[visibility, handleVisibilityChange]}>
            <Box dir={dir()} style={{backgroundColor: siteBackground,  maxWidth: 'unset'}}>
                <Header/>
                <BaseComponentContainer {...pageProps}>
                    {children}
                </BaseComponentContainer>
                <Footer {...props}/>
                <ShowVersion/>
            </Box>
        </HeaderAndFooterContext.Provider>
    )
}

function ShowVersion() {
    const [user] = useUserContext()

    return (
        <ErrorBoundary>
            {
                (getSafe(() => user?.isStaff, false)) ?
                    <Typography variant={"caption"}
                                style={{
                                    position: 'fixed',
                                    bottom: 5,
                                    left: 20,
                                    zIndex: 9999
                                }}>
                        V {packageJson.version}
                    </Typography> :
                    <React.Fragment/>
            }
        </ErrorBoundary>
    )
}




