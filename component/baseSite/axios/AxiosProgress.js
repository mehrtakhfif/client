import React from "react";
import NProgress from 'nprogress';
import Router from 'next/router';
import 'nprogress/nprogress.css';


Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

export default function AxiosProgress() {


    return <React.Fragment/>
}


