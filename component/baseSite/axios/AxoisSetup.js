import React, {Fragment, useEffect} from "react";
import {useSnackbar} from "notistack";
import axios from "axios";
import Button from "@material-ui/core/Button";
import {lang} from "../../../repository";
import {UtilsEncrypt} from "../../../utils/Utils";
import cookies from "../../../cookies";
import _ from 'lodash'
import moment from 'moment'
import {generateServerKey} from "../../../sv";
import AxiosProgress from "./AxiosProgress";
import {tryIt, useWindowSize} from "material-ui-helper";


axios.defaults.baseURL = process.env.REACT_APP_API
// axios.defaults.headers.common['Cache-Control'] = 'public, max-age=1800';
axios.defaults.headers.common['agent'] = "desktop";
axios.defaults.withCredentials = true;

export const defaultHeader = {
    noCache: {
        headers: {
            'Cache-Control': 'no-cache'
        }
    }
}

export default function AxiosSetup(props) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [width] = useWindowSize()


    useEffect(() => {
        //region setup
        axios.defaults.baseURL = process.env.REACT_APP_API
        axios.defaults.withCredentials = true;
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        //endregion setup
        //region request
        tryIt(() => {
            axios.interceptors.request.use(function (config) {
                try {
                    try {
                        switch (_.upperCase(config.method)) {
                            case "POST":
                            case "PUT":
                            case "PATCH": {
                                const cs = cookies.getServer({key: 'csrf_cookie'});
                                if (cs) {
                                    const t = moment().utc().format('YYYY-MM-DD-HH-mm');
                                    config.headers['X-CSRF-Token'] = UtilsEncrypt.SHA.S224(generateServerKey(cs, t));
                                    break;
                                }
                            }
                        }
                    } catch (e) {

                    }
                } catch (e) {
                }
                return config;
            }, function (error) {
                return Promise.reject(error);
            });
        })
        //endregion request
        //region response
        tryIt(() => {
            axios.interceptors.response.use(function (response) {
                let message = undefined
                let variant = undefined
                try {
                    message = response.data.message
                } catch (e) {
                }
                try {
                    variant = response.data.variant
                } catch (e) {
                }
                try {
                    if (message)
                        enqueueSnackbar(message,
                            {
                                variant: variant || "success",
                                action: (key) => (
                                    <Fragment>
                                        <Button onClick={() => {
                                            closeSnackbar(key)
                                        }}>
                                            {lang.get('close')}
                                        </Button>
                                    </Fragment>
                                )
                            });
                } catch (e) {

                }
                return response
            }, function (error) {
                if (error.response) {
                    let message = undefined
                    let variant = undefined
                    try {
                        message = error.response.data.message
                    } catch (e) {
                    }
                    try {
                        variant = error.response.data.variant
                    } catch (e) {
                    }
                    try {
                        if (!error.response.status || error.response.status === 500) {
                            enqueueSnackbar(message || lang.get("er_server_error"),
                                {
                                    variant: "error",
                                    action: (key) => (
                                        <Fragment>
                                            <Button onClick={() => {
                                                closeSnackbar(key)
                                            }}>
                                                {lang.get('close')}
                                            </Button>
                                        </Fragment>
                                    )
                                });
                        }
                    } catch (e) {
                    }
                    try {


                        // if (error.response.status === 403) {
                        //     // logout(dispatch)
                        //     enqueueSnackbar(message || lang.get("me_login_required"),
                        //         {
                        //             variant: variant || "warning",
                        //             action: (key) => (
                        //                 <Fragment>
                        //                     <Button onClick={() => {
                        //                         closeSnackbar(key)
                        //                     }}>
                        //                         {lang.get('close')}
                        //                     </Button>
                        //                 </Fragment>
                        //             )
                        //         });
                        // } else {
                        if (message)
                            enqueueSnackbar(message,
                                {
                                    variant: variant || "error",
                                    action: (key) => (
                                        <Fragment>
                                            <Button onClick={() => {
                                                closeSnackbar(key)
                                            }}>
                                                {lang.get('close')}
                                            </Button>
                                        </Fragment>
                                    )
                                });
                        // }
                    } catch (e) {
                    }
                }
                return Promise.reject(error);
            });
        })
        //endregion  response
    }, [])

    useEffect(() => {
        axios.defaults.headers.common['agent'] = width < 960 ? "mobile" : "desktop";
    }, [width])


    return (
        <React.Fragment>
            <AxiosProgress/>
        </React.Fragment>
    )
}
