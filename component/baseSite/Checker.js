import React, {useEffect} from "react";
import {tryIt} from "material-ui-helper";
import Router from "next/router";
import {useBackdropContext} from "../../context/BackdropContext";


export default function Checker() {

    const [backdrop,setBackDrop] =useBackdropContext()


    useEffect(() => {
        const handleRouteChange = url => {
            tryIt(() => {
                if (backdrop) {
                    setBackDrop(false)
                }
                setTimeout(() => {
                    document.getElementById("baseButtonChecker").focus()
                }, 300)
            })
        };

        Router.events.on('routeChangeStart', handleRouteChange);
        return () => {
            Router.events.off('routeChangeStart', handleRouteChange)
        }
    }, [backdrop])


    return (
        <React.Fragment>
            <button id={"baseButtonChecker"}
                   style={{opacity: 0, position: "absolute", top: 0, zIndex: -9999}}/>
        </React.Fragment>
    )
}
