import React from "react";

export default React.memo(function BaseComponentContainer({children,...props}) {
    return(
        <React.Fragment>
            {React.cloneElement(children,props)}
        </React.Fragment>
    )
})
