import React from "react";
import FireBase from "./FireBase";
import AxiosSetup from "./axios/AxoisSetup";
import Prefetch from "./Prefetch";

//
// export default function (){
//     return <React.Fragment/>
// }

export default React.memo(function Init() {


    return (
        <React.Fragment>
            <AxiosSetup/>
            <FireBase/>
            <Prefetch/>
        </React.Fragment>
    )
})
