import React, {Fragment, useEffect} from "react";
import {DEBUG} from "../../pages/_app";
import {firebaseCloudMessaging} from "../../webPush";
import {gcLog} from "../../utils/ObjectUtils";
import ControllerUser from "../../controller/ControllerUser";
import firebase from "firebase";
import Button from "@material-ui/core/Button";
import {lang} from "../../repository";
import {showNotification} from "../../utils/Utils";
import {useSnackbar} from "notistack";

export default function FireBase() {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    useEffect(() => {
        if (DEBUG)
            return
        async function setToken() {
            try {
                const token = await firebaseCloudMessaging.init();
                if (token) {
                    if (DEBUG)
                        gcLog("fcm token", token)
                    getMessage();
                    ControllerUser.User.addDevice({token: token})
                }
            } catch (error) {
                console.log(error);
            }
        }

        setToken();

        function getMessage() {
            const messaging = firebase.messaging();

            try {
                messaging.onMessage((message) => {
                    enqueueSnackbar(`${message.notification.title}: ${message.notification.body}`,
                        {
                            variant: "success",
                            action: (key) => (
                                <Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </Fragment>
                            )
                        });
                    showNotification({
                        title: message.notification.title,
                        body: message.notification.body
                    })
                });
            } catch (e) {
            }
        }
    }, []);


    return <React.Fragment/>
}
