import React, {useEffect} from "react";
import {isClient, tryIt, useEffectWithoutInit} from "material-ui-helper";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useRouter} from "next/router";
import {useBasketCountContext} from "../../context/BasketCount";
import ControllerSite from "../../controller/ControllerSite";
import useSWR, {mutate} from "swr";
import ControllerUser from "../../controller/ControllerUser";
import rout from "../../router";


export default function InitNeedContext(){
    return(
        <React.Fragment>
            {isClient()&& <InitReq/>}
        </React.Fragment>
    )
}

function InitReq() {
    const isLogin = useIsLoginContext();
    const router = useRouter();
    const basketCount = useBasketCountContext()

    const initReq = ControllerSite.init();
    const {} = useSWR(...initReq)

    const basketReq = ControllerUser.Basket.V2.get();
    const {} = useSWR(...basketReq)


    useEffectWithoutInit(() => {
        if (basketReq?.[0])
            mutate(basketReq[0])
    }, [isLogin,basketCount])


    useEffect(() => {
        tryIt(() => {
            if (_.isBoolean(isLogin) && !isLogin) {
                router.prefetch(rout.User.Auth.Login.rout)
            }
        })
    }, [isLogin])

    return <React.Fragment/>
}
