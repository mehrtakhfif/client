import React, {useRef, useState} from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Box from "@material-ui/core/Box";
import Img from "./base/oldImg/Img";
import {lang, theme} from "../repository";
import {Close, Search, Visibility} from "@material-ui/icons";
import Skeleton from "@material-ui/lab/Skeleton";
import IconButton from "@material-ui/core/IconButton";
import CircularProgress from "@material-ui/core/CircularProgress";
import {UtilsStyle} from "../utils/Utils";
import _ from 'lodash'
import {SearchRoutCreator} from "../pages/lastSearch";
import Link from "./base/link/Link";
import Typography from "./base/Typography";
import {useRouter} from "next/router";
import TextFieldContainer from "./base/textField/TextFieldContainer";
import TextField from "./base/textField/TextField";

export const minCharacterForSearch = 2;
const showAll = 'SHOW_ALL';
const listIsEmpty = 'LIST_IS_EMPTY';


const timer = {};
export default function SearchBar({value: val, name = "searchTextField", interval = 300, loadingReq, openList = true, inputWidth = '60%', onChange, onListOpen, onListClosed, list = [], ...props}) {
    const [open, setOpen] = React.useState(false);
    const loading = loadingReq && (((list.length <= 0) || (value.length >= 2 && _.isEmpty(list))));
    const inputRef = useRef(null);
    const router = useRouter();
    const [value, setValue] = useState(val);


    const newList = [...list];
    if (!loading) {
        if (!_.isEmpty(list)) {
            newList.push(showAll);
        } else {
            newList.push(listIsEmpty);
        }
    }
    return (
        <Autocomplete
            ref={inputRef}
            disableClearable
            variant="outlined"
            value={value}
            options={newList}
            loading={loading}
            open={openList && open && value.length >= minCharacterForSearch}
            onKeyPress={(e) => {
                if (e.key === 'Enter') {
                    const {rout, as} = SearchRoutCreator({qParam: value});
                    router.push(rout, as);
                }
            }}
            onOpen={() => {
                setOpen(true);
                if (onListOpen)
                    onListOpen()
            }}
            onClose={() => {
                setOpen(false);
                if (onListClosed)
                    onListClosed()
            }}
            loadingText={(
                <Box display='flex' width={1} alignItems='center'>
                    <Box>
                        <Skeleton width={75} height={75} style={{
                            transform: 'none'
                        }}/>
                    </Box>
                    <Box width='1' mx={1}>
                        <Skeleton height={50} style={{
                            width: '100%',
                            marginRight: theme.spacing(1)
                        }}/>
                    </Box>
                </Box>)}
            renderOption={(option) => (
                option !== null &&
                <Item
                    item={option}
                    value={value}
                    onClick={() => {
                    }}/>
            )}
            renderInput={params => (
                <SearchBox value={value}
                           inputWidth={inputWidth}
                           name={name}
                           interval={interval}
                           onChange={(v) => {
                               v = _.trim(v)
                               if (value === v)
                                   return
                               setValue(v);
                               clearTimeout(timer[name]);
                               timer[name] = setTimeout(() => {
                                   onChange(v);
                               }, interval);
                           }}
                           loading={loading}
                           list={list}
                           {...params}/>
            )}
            style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
                ...props.style
            }}/>
    );
}

function SearchBox({inputWidth, value, name, interval, onChange, loading, ...params}) {

    return (
        <TextFieldContainer
            name={name}
            active={false}
            defaultValue={value}
            onChangeDelay={0}
            onChange={(value) => {
                onChange(value);
            }}
            render={(ref, {name, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                return (
                    <TextField
                        {...props}
                        error={!valid}
                        name={name}
                        defaultValue={value}
                        variant="outlined"
                        fullWidth
                        requestFocus={true}
                        inputRef={ref}
                        endAdornment={(
                            <Box
                                display='flex'
                                justifyContent='center'
                                alignItems='center'
                                flexDirection={params.isLtr ? 'row-reverse' : 'row'}>
                                {
                                    value &&
                                    <IconButton className={"search-clear-text"}
                                                size='small'
                                                onClick={() => {
                                                    onChange("")
                                                }}
                                                style={{
                                                    marginLeft: theme.spacing(0.1),
                                                    marginRight: theme.spacing(0.1)
                                                }}>
                                        <Close/>
                                    </IconButton>
                                }
                                {loading ?
                                    <CircularProgress color="inherit" size={20} style={{
                                        marginLeft: theme.spacing(1),
                                        marginRight: theme.spacing(1)
                                    }}/>
                                    :
                                    <IconButton
                                        size='small'
                                        style={{
                                            marginLeft: theme.spacing(0.1),
                                            marginRight: theme.spacing(0.1)
                                        }}>
                                        <Search
                                            edge="end"
                                            aria-label="toggle password visibility">
                                            <Visibility/>
                                        </Search>
                                    </IconButton>
                                }
                            </Box>
                        )}
                        placeholder={lang.get("search")}
                        style={style}
                        inputProps={{
                            ...inputProps
                        }}
                        {...params}/>
                )
            }}/>
    )
}

function Item({item, value, onClick, ...props}) {
    const {name, thumbnail, rout} = item;
    return (
        item === showAll ?
            <Box display={'flex'} width={1} justifyContent={'center'} py={0.5}>
                <Link
                    href={SearchRoutCreator({qParam: value})}
                    style={{padding: "5px"}}>
                    {lang.get("show_all")}
                </Link>
            </Box> :
            item === listIsEmpty ?
                <Box display={'flex'}
                     width={1}
                     justifyContent={'center'}
                     py={0.7}>
                    {lang.get("products_not_found")}
                </Box>
                :
                <Link display='flex' alignItems={"center"} width={1} href={rout || "#"}>
                    <Box display={'flex'}>
                        <Img src={thumbnail}
                             alt={name}
                             minHeight={1}
                             imgStyle={{
                                 maxWidth: '70px',
                                 minHeight: 1,
                                 height: 'auto',
                                 ...UtilsStyle.borderRadius(5)
                             }}/>
                    </Box>
                    <Typography width={1} mx={2} variant={'body2'} component='span'>
                        {name}
                    </Typography>
                </Link>
    )
}

