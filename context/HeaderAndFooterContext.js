import React, {useContext} from "react";

export const defaultHeaderAndFooterContext = {
    header: {
        showLg: true,
        showSm: true
    },
    footer: {
        showLg: true,
        showSm: true
    },
};
const HeaderAndFooterContext = React.createContext(defaultHeaderAndFooterContext)
export default HeaderAndFooterContext;


export function useHeaderAndFooterContext(){
    return useContext(HeaderAndFooterContext)
}
