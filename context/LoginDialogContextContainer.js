import React, {useContext} from "react";
import {useOpenWithBrowserHistory} from "material-ui-helper";
import Login from "../pages/login";
import {useIsLoginContext} from "./IsLoginContextContainer";

export const defaultLoginDialogContext = undefined;
const LoginDialogContext = React.createContext(defaultLoginDialogContext)
export default LoginDialogContext;


export function LoginDialogContextContainer({children}) {
    const isLogin = useIsLoginContext();

    const [openLoginDialog, __, handleOpenLoginDialog, handleCloseLoginDialog] = useOpenWithBrowserHistory(" login_dialog_context")


    function handle(show) {
        if (!show) {
            handleCloseLoginDialog()
            return
        }
        handleShow()
    }

    function handleShow() {
        if (isLogin)
            return
        handleOpenLoginDialog()
    }


    return (
        <LoginDialogContext.Provider value={[openLoginDialog, handle, handleShow,handleCloseLoginDialog]}>
            {children}
            <Login
                open={openLoginDialog}
                onClose={handleCloseLoginDialog}/>
        </LoginDialogContext.Provider>
    )
}


export function useLoginDialogContext() {
    return useContext(LoginDialogContext)
}
