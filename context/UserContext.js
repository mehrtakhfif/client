import React, {useCallback, useContext, useEffect} from "react";
import useSWR, {mutate} from "swr";
import ControllerUser from "../controller/ControllerUser";
import {tryIt} from "material-ui-helper";
import rout from "../router";
import {useRouter} from "next/router";
import {useIsLoginContext} from "./IsLoginContextContainer";

export const defaultUserContext = {};
const UserContext = React.createContext(defaultUserContext)
export default UserContext;


export function UserContextContainer({children}) {
    const isLogin = useIsLoginContext();
    const router = useRouter()
    const [apiKey, req, swrProps] = ControllerUser.User.Profile.V2.get()
    const {data} =  useSWR(apiKey+isLogin, ()=>{
        if (!isLogin)
            return  {}
        return req()
    }, swrProps);

    const refresh = useCallback(() => {
        return mutate(apiKey)
    }, [apiKey])

    useEffect(() => {
        tryIt(() => {
            if (!(data?.data?.user)) {
                return router.prefetch(rout.User.Auth.Login.rout)
            }
        })
    }, [data])


    return (
        <UserContext.Provider value={[data?.data?.user || {}, refresh]}>
            {children}
        </UserContext.Provider>
    )
}


export function useUserContext() {
    return useContext(UserContext)
}
