import {useContext} from "react";
import UserContext from "./UserContext";


export function useIsAdminContext(){
    const [user] = useContext(UserContext)
    return user?.is_staff
}
