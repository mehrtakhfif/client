import React, {useContext, useEffect} from "react";
import {tryIt, useState} from "material-ui-helper";
import Router, {useRouter} from "next/router";
import rout from "../router";

export const defaultBrowserHistories = [];
const BrowserHistoriesContext = React.createContext(defaultBrowserHistories)
export default BrowserHistoriesContext;


export function BrowserHistoriesContextContainer({children}) {
    const router = useRouter()
    const [histories, setHistories] = useState([])

    useEffect(() => {
        tryIt(() => {
            setHistories([location.pathname])
        })

        const handleRouteChange = url => {
            setHistories(histories => [...histories, url])
        };

        Router.events.on('routeChangeStart', handleRouteChange);
        return () => {
            Router.events.off('routeChangeStart', handleRouteChange)
        }
    }, []);

    function onBack(){
        if (histories.length > 1) {
            router.back()
            return;
        }
        router.push(rout.Main.home)
    }

    return (
        <BrowserHistoriesContext.Provider value={[histories, {onBack}]}>
            {children}
        </BrowserHistoriesContext.Provider>
    )
}


export function useBrowserHistoriesContext() {
    return useContext(BrowserHistoriesContext)
}
