import React, {useContext} from "react";
import {DEBUG_AUTH} from "../pages/_app";

export const defaultIsLoginContext = false;
const IsLoginContext = React.createContext(defaultIsLoginContext)
export default IsLoginContext;



export function useIsLoginContext(){
    return DEBUG_AUTH || useContext(IsLoginContext)
}
