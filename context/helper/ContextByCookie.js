import React, {useEffect} from "react";
import {tryIt, useState} from "material-ui-helper";
import Actions from "../../Actions";
import BasketCountContext from "../BasketCount";
import IsLoginContext from "../IsLoginContextContainer";

function onCookieChange(ctx) {
    return tryIt(() => {
        const isLogin = Actions.User.isLogin({ctx})
        const basketCount = Actions.Basket.basketCount({ctx})
        return {
            isLogin,
            basketCount
        }
    })
}

export const checkCookie = () => {
    let lastCookie = document.cookie;
    return () => {
        const currentCookie = document.cookie;
        if (currentCookie === lastCookie)
            return
        lastCookie = currentCookie;
        return onCookieChange()
    }
}

export default function ContextByCookie({children, ...props}) {
    const [isLogin, setIsLogin] = useState()
    const [basketCount, setBasketCount] = useState()

    function handleChangeCookie({isLogin, basketCount}) {
        setIsLogin(Boolean(isLogin))
        setBasketCount(basketCount)
    }

    useEffect(() => {
        handleChangeCookie(onCookieChange())
        const checker = checkCookie()
        const checkCookieInterval = window.setInterval(() => {
            const res = checker()
            if (res)
                handleChangeCookie(res)
        }, 1000);
        return () => {
            clearInterval(checkCookieInterval)
        }
    }, [])

    return (
        <IsLoginContext.Provider value={isLogin}>
            <BasketCountContext.Provider value={basketCount}>
                {children}
            </BasketCountContext.Provider>
        </IsLoginContext.Provider>
    )
}
