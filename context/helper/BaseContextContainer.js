import React from "react";
import {UserContextContainer} from "../UserContext";
import ContextByCookie from "./ContextByCookie";
import {BackdropContextContainer} from "../BackdropContext";
import {BrowserHistoriesContextContainer} from "../BrowserHistoriesContext";
import InitNeedContext from "../../component/baseSite/InitNeedContext";
import {LoginDialogContextContainer} from "../LoginDialogContextContainer";


export default function BaseContextContainer({children, ...props}) {
    return (
        <BrowserHistoriesContextContainer>
            <ContextByCookie>
                <UserContextContainer>
                    <BackdropContextContainer>
                        <LoginDialogContextContainer>
                                <InitNeedContext/>
                                {React.cloneElement(children, props)}
                        </LoginDialogContextContainer>
                    </BackdropContextContainer>
                </UserContextContainer>
            </ContextByCookie>
        </BrowserHistoriesContextContainer>
    )
}
