import React, {useContext} from "react";

export const defaultBasketCountContext = 0;
const BasketCountContext = React.createContext(defaultBasketCountContext)
export default BasketCountContext;


export function useBasketCountContext(){
    return useContext(BasketCountContext)
}
