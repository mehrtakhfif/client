import React, {useContext, useState} from "react";
import {Backdrop} from "material-ui-helper";
import {useTheme} from "@material-ui/core";
import {cyan} from "@material-ui/core/colors";
import CircularProgress from "@material-ui/core/CircularProgress";

export const defaultBackdropContext = {};
const BackdropContext = React.createContext(defaultBackdropContext)
export default BackdropContext;


export const backdropType = {
    onlyBody: "backdrop_only_body",
    fillPage: 'fill_page',
    onlyBodyWithLoading: "backdrop_only_body_with_loading",
    fillPageWithLoading: 'fill_page_with_loading',
}

export function BackdropContextContainer({children}) {
    const theme = useTheme();
    const [show, setShow] = useState(undefined)

    function showBackdrop(type =backdropType.onlyBody ) {
        setShow(type)
    }

    function closeBackdrop() {
        setShow(undefined)
    }

    const isFullPage = show === backdropType.fillPage || show === backdropType.fillPageWithLoading;
    const isLoading = show === backdropType.fillPageWithLoading || show === backdropType.onlyBodyWithLoading;

    return (
        <BackdropContext.Provider value={[show, setShow, showBackdrop, closeBackdrop]}>
            {children}
            <Backdrop
                open={Boolean(show)}
                zIndex={isFullPage ? theme.zIndex.appBar + 2 : theme.zIndex.appBar - 2}>
                {
                    isLoading &&
                    <CircularProgress
                        color="inherit"
                        style={{
                            color: cyan[300],
                            width: 100,
                            height: 100
                        }}/>
                }
            </Backdrop>
        </BackdropContext.Provider>
    )
}


export function useBackdropContext() {
    return useContext(BackdropContext)
}
