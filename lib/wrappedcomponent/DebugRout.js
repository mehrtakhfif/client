import React from "react";
import {DEBUG} from "../../pages/_app";
import {Error} from "../../pages/_error";
import Head from "next/head";
import {NextSeo} from "next-seo";

export default function ({children,...props}) {

    return (
        <React.Fragment>
            <NextSeo noindex={true} nofollow={true} />
            {
                DEBUG?
                    React.cloneElement(children, props):
                    <Error/>
            }
        </React.Fragment>
    )
}
