import React, {useEffect} from "react";
import _ from "lodash";
import {Box} from "material-ui-helper";
import {useRouter} from "next/router";
import rout from "../../router";
import PleaseWait from "../../component/base/loading/PleaseWait";
import {DEBUG_AUTH} from "../../pages/_app";
import BasePage from "../../component/base/BasePage";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";


export default function LogoutRequiredRout({autoChangeRout = true, children, ...props}) {
    const router = useRouter();
    const isLogin = useIsLoginContext();


    useEffect(() => {
        if (!_.isBoolean(isLogin) || !isLogin || !autoChangeRout)
            return
        // dispatch(headerFooterVisibility({show: true}))
        router.push(rout.Main.home)
    }, [isLogin])


    const show = isLogin && autoChangeRout

    return (
        <BasePage
            setting={{
                header: {
                    showLg: show,
                    showSm: show,
                },
                footer: {
                    showLg: show,
                    showSm: show,
                }
            }}>
            {
                _.isBoolean(isLogin) ?
                    (DEBUG_AUTH || !isLogin || !autoChangeRout) ?
                        <React.Fragment>
                            <Box/>
                            {React.cloneElement(children, props)}
                        </React.Fragment> :
                        <React.Fragment>
                            <Box/>
                            <PleaseWait/>
                        </React.Fragment> :
                    <React.Fragment/>
            }
        </BasePage>
    )
}
