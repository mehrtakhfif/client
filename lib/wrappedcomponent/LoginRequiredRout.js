import React, {useCallback, useEffect, useMemo, useState} from "react";
import _ from "lodash";
import Login from "../../pages/login";
import {getSafe, gLog} from "material-ui-helper";
import PleaseWait from "../../component/base/loading/PleaseWait";
import {useUserContext} from "../../context/UserContext";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import BasePage from "../../component/base/BasePage";
import {DEBUG_AUTH} from "../../pages/_app";
import {NextSeo} from "next-seo";


export default function LoginRequiredRout({children, ...props}) {
    const [user] = useUserContext();
    const isLogin = useIsLoginContext();

    const [freezeLogin, setFreezeLogin] = useState(false);

    const showLayout = useMemo(() => {
        return isLogin && !freezeLogin
    }, [isLogin, freezeLogin])

    const handleFreezeChange = useCallback((val) => {
        setFreezeLogin(val)
    }, [])

    useEffect(() => {
        if (!_.isBoolean(isLogin)) {
            return;
        }
        // disptach(headerFooterVisibility({show: showLayout}))
    }, [isLogin, showLayout])


    return (
        <React.Fragment>
            <NextSeo noindex={true} nofollow={true} />
            {
                (DEBUG_AUTH ||_.isBoolean(isLogin)) ?
                    (DEBUG_AUTH || showLayout) ?
                        (getSafe(() => user?.username, false)) ?
                            <React.Fragment>
                                {React.cloneElement(children, props)}
                            </React.Fragment> :
                            <PleaseWait/>
                        :
                        <BasePage
                            name="loginRequiredRout"
                            setting={{
                                header: {
                                    showLg:showLayout,
                                    showSm:showLayout
                                },
                                footer: {
                                    showLg:showLayout,
                                    showSm: showLayout
                                },
                            }}>
                            <Login onAutoChange={handleFreezeChange}/>
                        </BasePage> :
                    <React.Fragment/>
            }
        </React.Fragment>
    )
}
