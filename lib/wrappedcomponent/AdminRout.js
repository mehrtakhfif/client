import React from "react";
import {Error} from "../../pages/_error";
import PleaseWait from "../../component/base/loading/PleaseWait";
import {DEBUG} from "../../pages/_app";
import Head from "next/head";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useUserContext} from "../../context/UserContext";


export default function AdminRout({children, ...props}) {
    const isLogin = useIsLoginContext();
    const [user] = useUserContext();

    return (
        <React.Fragment>
            <Head>
                <meta name="robots" content="noindex"/>
            </Head>
            {
                ((isLogin && !user) || DEBUG) ?
                    <PleaseWait/> :
                    ((isLogin && user?.isStaff) || DEBUG) ?
                        React.cloneElement(children, props) :
                        <Error/>
            }
        </React.Fragment>
    )
}
