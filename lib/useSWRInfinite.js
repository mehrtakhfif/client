import React, {useMemo} from "react";
import {useSWRInfinite as useSWRInfiniteBase} from "swr";
import _ from "lodash";
import {getSafe, tryIt, useState} from "material-ui-helper";


const addPageToApi = (api, page) => {
    if (/\w+\?\w+/.test(api)) {
        return api + `&p=${page}`
    }
    if (/\/\w+/.test(api)) {
        return api + `?p=${page}`
    }
    return api.substr(api.length - 2, api.length - 1) + `?p=${page}`
}


export default function useSWRInfinite(api, fetcher, options = {}) {
    const [firstInit, setFirstInit] = useState(false)

    const getKey = (index, previousPageData) => {
        try {
            if (previousPageData?.pagination?.last_page <= index) return null
            return addPageToApi(api, index + 1)
        } catch (e) {
            return null
        }
    }

    const {initialData, onSuccess, onError, onFinish, ...op} = options
    const {data: dd, error, size, mutate, setSize, isValidating} =
        useSWRInfiniteBase(getKey, fetcher, {
            revalidateOnFocus: false,
            onSuccess: (...args) => {
                tryIt(() => onSuccess(...args))
                tryIt(() => onFinish())
                tryIt(() => {
                    setTimeout(() => {
                        setFirstInit(true)
                    }, 600)
                })
            },
            onError: (...args) => {
                setFirstInit(true)
                tryIt(() => onError(...args))
                tryIt(() => onFinish())
            },
            ...op
        })

    const data = useMemo(() => {
        return getSafe(() => {
            let data = [];
            _.forEach(dd, d => {
                if (d?.data)
                    data = data.concat(d.data)
            })
            if (_.isEmpty(data))
                throw ""
            return data
        }, firstInit ? [] : getSafe(() => {
            return initialData.data || initialData
        }, initialData) || [])
    }, [dd, firstInit])

    const isLoadingInitialData = !dd && !error

    const isLoadingMore = getSafe(() => isLoadingInitialData || (size > 0 && _.isUndefined(dd[size - 1])), false)

    const isEmpty = getSafe(() => _.isEmpty(dd[0]))

    const isReachingEnd = getSafe(() => isEmpty || (dd[0].pagination.last_page <= size), false)

    const isRefreshing = getSafe(() => isValidating && dd.length === size, false)


    const handleNextPage = () => {
        if (isReachingEnd)
            return
        setSize(size => size + 1)
    }

    const onClear = () => {
        setSize(0)
    }

    return {
        data,
        baseData: dd,
        error,
        page: size,
        setPage: setSize,
        setNextPage: handleNextPage,
        mutate,
        isValidating,
        isLoadingInitialData,
        isLoadingMore,
        isEmpty,
        isReachingEnd,
        isRefreshing,
        maxPage: getSafe(() => dd[0].pagination.last_page),
        allCount: getSafe(() => dd[0].pagination.count),
        step: getSafe(() => dd[0].pagination.step),
        onClear
    }
}

// const {
//     data,
//     error,
//     page,
//     maxPage,
//     allCount,
//     mutate,
//     setNextPage,
//     onClear,
//     isEmpty,
//     isLoadingMore,
//     isReachingEnd,
//     isRefreshing
// } = useSWRInfinite(api, fetcher)
