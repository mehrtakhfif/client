import {DEBUG} from "../pages/_app";
import * as Sentry from "@sentry/browser";
import {gLog, tryIt} from "material-ui-helper";


const sentry = {
    error: (text) => {
        tryIt(() => {
            if (!text)
                return;
            if (DEBUG){
                gLog("Sentry error:",text)
                return
            }
            Sentry.captureException(new Error(text));
        })
    }
}

export default sentry