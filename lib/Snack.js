import React from "react";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import {grey} from "@material-ui/core/colors";

const snackThemes = {
    default: {
        key: "default",
        closeButtonColor: '#000',
        closeButtonBackgroundColor: grey[300],
    },
    success: {
        key: "success",
        closeButtonColor: "#000",
        closeButtonBackgroundColor: grey[300],
    },
    error: {
        key: "error",
        closeButtonColor: "#000",
        closeButtonBackgroundColor: grey[300],
    },
    warning: {
        key: "warning",
        closeButtonColor: "#000",
        closeButtonBackgroundColor: grey[300],
    },
    info: {
        key: "info",
        closeButtonColor: "#000",
        closeButtonBackgroundColor: grey[300],
    },
}

export const snack = (snackBar, {
    message,
    preventDuplicate = true,
    variant = 'default',
    action,
    closable = true,
    autoHideDuration = 3000,
    persist = false
}) => {
    snackBar.enqueueSnackbar(message, {
        action: (key) => (
            <React.Fragment>
                {
                    action && action(key)
                }
                {
                    closable && closeEl(snackBar, key, variant)
                }
            </React.Fragment>
        ),
        variant,
        preventDuplicate,
        autoHideDuration,
        persist
    })
}



export const successSnack = (snackBar, {
    message,
    preventDuplicate = true,
    action,
    closable = true,
    autoHideDuration = 3000,
    persist = false
}) => {
    snack(snackBar, {
        variant: 'success',
        message,
        preventDuplicate,
        action,
        closable,
        autoHideDuration,
        persist
    })
}


const closeEl = (snackBar, key, variant) => {
    return (
        <Box mx={0.5}>
            <IconButton
                size={"small"}
                onClick={() => {
                    snackBar.closeSnackbar(key)
                }}
                style={{
                    backgroundColor: snackThemes[variant].closeButtonBackgroundColor
                }}>
                <Icon
                    className="fal fa-times-circle"
                    style={{
                        color: snackThemes[variant].closeButtonColor,
                        fontSize: '1.1rem'
                    }}/>
            </IconButton>
        </Box>
    )
}

