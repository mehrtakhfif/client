const withPwa = require('next-pwa');

const {
    PHASE_DEVELOPMENT_SERVER,
    PHASE_PRODUCTION_BUILD,
} = require('next/constants')

const env = (phase) => {

    const debugServer = true
    const debugWithIp = false
    const localRout = debugWithIp ? "http://192.168.137.213:80" : 'http://api.mt.com:8000'
    const stagingLocalRout = 'http://api.mt.com:80'
    // const stagingLocalRout = "http://192.168.1.95:80"
    // const localRout = debugWithIp ? "http://192.168.1.95:80" : 'http://api.mt.com:3001'
    // const localRout = debugWithIp ? "http://192.168.1.95:80" : 'http://admin.mt.com:80'
    // const localRout = debugWithIp ? "http://192.168.1.95:80" : 'https://tall-dog-85.loca.lt'
    // const serverRout = 'https://api.mehrtakhfif.com'
    const serverRout = 'https://api.mehrtakhfif.com'
    // const serverRout =  'http://api.mt.com:80'



    const isDev = phase === PHASE_DEVELOPMENT_SERVER
    const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'
    const isStaging = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'


    return {
        REACT_APP_IS_DEV: phase === PHASE_DEVELOPMENT_SERVER,
        REACT_APP_IS_PROD: phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1',
        REACT_APP_IS_STAGING: phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1',
        REACT_APP_DEBUG: (() => {
            if (isDev) return "true"
            if (isStaging) return "true"
            if (isProd) return "false"
            return 'REACT_APP_DEBUG:not (isDev,isProd)'
        })(),
        REACT_APP_API: (() => {
            // if (isDev) return debugServer ? 'https://swf5jv5z.tunnelto.dev' : localRout
            if (isDev) return debugServer ? serverRout : localRout
            // if (isDev) return debugServer ? 'https://api.mehrtakhfif.com' : 'http://192.168.1.95'
            // if (isDev) return debugServer ? 'http://192.168.1.95' : 'http://api.mt.com'
            // if (isStaging) return 'https://swf5jv5z.tunnelto.dev'
            if (isStaging) return stagingLocalRout
            // if (isStaging) return 'http://192.168.1.95'
            if (isProd) return serverRout
            return 'REACT_APP_NODE_ENV:not (isDev,isProd)'
        })(),
        REACT_APP_SITE_ROUT: (() => {
            if (isDev) return debugServer ? 'https://mehrtakhfif.com' : 'http://mt.com:3001'
            if (isStaging) return 'http://mt.com:3002'
            if (isProd) return 'https://mehrtakhfif.com'
            return 'REACT_APP_NODE_ENV:not (isDev,isProd)'
        })(),
        REACT_APP_ADMIN_ROUT: (() => {
            if (isDev || isStaging) return debugServer ? 'https://admin.mehrtakhfif.com' : 'http://mt.com:3000'
            if (isProd) return 'https://admin.mehrtakhfif.com'
            return 'REACT_APP_NODE_ENV:not (isDev,isProd)'
        })(),
        REACT_APP_SHORT_ROUT:"https://mhrt.ir",
        REACT_APP_MATERIAL_HELPER_LOGGER: isProd ? "false" : "true",
        REACT_APP_G_ANALYTICS: 'UA-160176823-1',
        REACT_APP_API_KEY: "AIzaSyAPoyIJc-tp_fCPafgnOsW8FzrLJjs9cIs",
        REACT_APP_AUTH_DOMAIN: "mehrtakhfif-e5b93.firebaseapp.com",
        REACT_APP_DATABASE_URL: "https://mehrtakhfif-e5b93.firebaseio.com",
        REACT_APP_FIREBASE_CONFIG_PROJECT_ID: "mehrtakhfif-e5b93",
        REACT_APP_STORAGE_BUCKET: "mehrtakhfif-e5b93.appspot.com",
        REACT_APP_MESSAGING_SENDER_ID: '14720050507',
        REACT_APP_MESSAGING_APP_ID: '1:14720050507:web:0b061137e84a94bb4534ce',
        REACT_APP_MEASUREMENT_ID: 'G-Y188RXNMC9',
    }
}


module.exports = (phase) => withPwa({
    pwa: {
        dest: "public",
        register: true,
        skipWaiting: true,
        swSrc: "service-worker.js",
    },
    env: env(phase),
    reactStrictMode: true,
    swcMinify: false,
    images: {
        domains: ['api.mehrtakhfif.com','api.mt.com'],
    },
});
