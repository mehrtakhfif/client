const express = require('express');
const apm = require('elastic-apm-node').start({
    serviceName: 'nextJs',
    secretToken: '@bn9NnKS!*Hjb+Ykk$sQC6C@$6yA4*LDT2*8unjX9xVs^Nn+c8a$fr48-yx$PBa+dxStdXNErNCf8q_8&BxmCSb4e@Cs+@UZhnDw3e4',
})
const next = require('next');
const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();
// Add this to the VERY top of the first file loaded in your app



app.prepare().then(() => {
    const server = express();
    apm.start()
    server.get('/service-worker.js', (req, res) => {
        app.serveStatic(req, res, './.next/service-worker.js');
    });
    //scoping the service workers
    const serviceWorkers = [
        {
            filename: 'service-worker.js',
            path: './.next/service-worker.js',
        },
        {
            filename: 'firebase-messaging-sw.js',
            path: './public/firebase-messaging-sw.js',
        },
    ];
    serviceWorkers.forEach(({filename, path}) => {
        alert(filename+" ---- "+path)
        server.get(`/${filename}`, (req, res) => {
            app.serveStatic(req, res, path);
        });
    });
    server.get('*', (req, res) => {
        return handle(req, res);
    });
    server.listen(port, (err) => {
        if (err) throw err;
    });
});
