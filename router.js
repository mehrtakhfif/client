import {UtilsRouter} from "./utils/Utils";
import _ from 'lodash';
import {filterItemsKey, orderingItems} from "./pages/search/[[...cat]]";
import {gLog} from "material-ui-helper";

const rout = {
    User: {
        Auth: {
            Login: {
                rout: "/login",
                Params: {
                    redirectRout: 'R',
                },
                create: ({redirectRout}) => {
                    let param = "?";
                    if (redirectRout) {
                        param = param + "R=" + redirectRout + "&"
                    }
                    param = param.slice(0, -1);
                    return "/login" + param
                }
            },
            signup: "/signup",
            verifyMobile: "/activate",
            resendCode: "/resend_code",
            resetPassword: "/reset-password",
        },
        Basket: {
            rout: "/basket",
            Shopping: {
                rout: "/shopping/[...step]",
                mainWithoutParam: "/shopping",
                as: ({step} = {step: rout.User.Basket.Shopping.Params.Details.param}) => {
                    return rout.User.Basket.Shopping.mainWithoutParam + `/${step}`
                },
                create: ({step}) => {
                    return {
                        rout: rout.User.Basket.Shopping.rout,
                        as: rout.User.Basket.Shopping.as({step: step})
                    }
                },
                Params: {
                    Details: {
                        param: "details",
                        rout: "/shipping/details",
                        as: () => {
                            return rout.User.Basket.Shopping.as({step: rout.User.Basket.Shopping.Params.Details.param})
                        },
                        create: () => {
                            return rout.User.Basket.Shopping.create({step: rout.User.Basket.Shopping.Params.Details.param})
                        }
                    },
                    Payment: {
                        param: "payment",
                        rout: "/shipping/payment",
                        as: () => {
                            return rout.User.Basket.Shopping.as({step: rout.User.Basket.Shopping.Params.Payment.param})
                        },
                        create: () => {
                            return rout.User.Basket.Shopping.create({step: rout.User.Basket.Shopping.Params.Payment.param})
                        }
                    },
                    Invoice: {
                        param: "invoice",
                        rout: "/shipping/invoice",
                        Params: {
                            invoice_id: 'id'
                        },
                        as: () => {
                            return rout.User.Basket.Shopping.as({step: rout.User.Basket.Shopping.Params.Invoice.param})
                        },
                        create: () => {
                            return rout.User.Basket.Shopping.create({step: rout.User.Basket.Shopping.Params.Invoice.param})
                        }
                    },
                    Fail: {
                        param: "fail",
                        rout: "/shipping/fail",
                        as: () => {
                            return rout.User.Basket.Shopping.as({step: rout.User.Basket.Shopping.Params.Fail.param})
                        },
                        create: () => {
                            return rout.User.Basket.Shopping.create({step: rout.User.Basket.Shopping.Params.Fail.param})
                        }
                    },
                },
            },
        },
        Profile: {
            Main: {
                rout: "/profile/[...page]",
                routWithoutParam: "/profile",
                as: ({page = rout.User.Profile.Main.Params.Profile.param}) => {
                    return rout.User.Profile.Main.routWithoutParam + `/${page}`
                },
                create: ({page = rout.User.Profile.Main.Params.Profile.param} = {}) => {
                    return {
                        rout: rout.User.Profile.Main.rout,
                        as: rout.User.Profile.Main.as({page: page})
                    }
                },
                Params: {
                    Profile: {
                        param: "main",
                        rout: `/profile/main`,
                        as: () => {
                            return rout.User.Profile.Main.as({page: rout.User.Profile.Main.Params.Profile.param})
                        },
                        create: () => {
                            return rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.Profile.param})
                        }
                    },
                    AllOrder: {
                        param: "all-order",
                        rout: `/profile/all-order`,
                        as: () => {
                            return rout.User.Profile.Main.as({page: rout.User.Profile.Main.Params.AllOrder.param})
                        },
                        create: () => {
                            return rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.AllOrder.param})
                        }
                    },
                    Wishlist: {
                        param: "wishlist",
                        rout: `/profile/wishlist`,
                        as: () => {
                            return rout.User.Profile.Main.as({page: rout.User.Profile.Main.Params.Wishlist.param})
                        },
                        create: () => {
                            return rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.Wishlist.param})
                        }
                    },
                    CommentsAndReview: {
                        param: "comment-and-review",
                        rout: `/profile/comment-and-review`,
                        as: () => {
                            return rout.User.Profile.Main.as({page: rout.User.Profile.Main.Params.CommentsAndReview.param})
                        },
                        create: () => {
                            return rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.CommentsAndReview.param})
                        }
                    },
                    Addresses: {
                        param: "addresses",
                        rout: `/profile/addresses`,
                        as: () => {
                            return rout.User.Profile.Main.as({page: rout.User.Profile.Main.Params.Addresses.param})
                        },
                        create: () => {
                            return rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.Addresses.param})
                        }
                    },
                    UserInfo: {
                        param: "user-info",
                        rout: `/profile/user-info`,
                        as: () => {
                            return rout.User.Profile.Main.as({page: rout.User.Profile.Main.Params.UserInfo.param})
                        },
                        create: () => {
                            return rout.User.Profile.Main.create({page: rout.User.Profile.Main.Params.UserInfo.param})
                        }
                    },
                },
            },
            AddAddress: {
                rout: "/add-address",
                create: ({redirectRout = rout.User.Profile.Main.rout}) => {
                    let param = "?";
                    if (redirectRout) {
                        param = param + "R=" + redirectRout + "&"
                    }
                    param = param.slice(0, -1);
                    return rout.User.Profile.AddAddress.rout + param
                }
            },
            Invoice: {
                rout: "/invoice",
                Single: {
                    main: "/invoice/[invoice_id]",
                    Params: {
                        invoice_id: 'invoice_id'
                    },
                    as: ({invoice_id}) => {
                        return rout.User.Profile.Invoice.rout + `/${invoice_id}`
                    },
                    create: ({invoice_id}) => {
                        return {
                            rout: rout.User.Profile.Invoice.Single.main,
                            as: rout.User.Profile.Invoice.Single.as({invoice_id})
                        }
                    },
                    Show: {
                        Digital: {
                            main: "/invoice/dg/[product_id]",
                            createRout: ({product_id}) => {
                                return `/invoice/dg/${product_id}`
                            },
                            Params: {
                                invoice_id: 'product_id'
                            },
                        }
                    }
                }

            }
        },
    },
    Product: {
        permalink_id: 'permalink_id',
        Filter: {
            rout: "/filter/[...params]",
            createParams: ({
                               boxPermalink = "all",
                               category = null,
                               priceFilter = {
                                   maxPrice: null,
                                   minPrice: null
                               },
                               returnObject = false
                           }) => {
                return createProductFilterParams({
                    boxPermalink: boxPermalink,
                    category: category,
                    priceFilter: {
                        maxPrice: priceFilter.maxPrice,
                        minPrice: priceFilter.minPrice
                    },
                    returnObject: returnObject,
                })
            },
            as: ({
                     boxPermalink = "all",
                     queryString = null,
                     orderBy = null,
                     category = null,
                     priceFilter = {
                         maxPrice: null,
                         minPrice: null
                     },
                     brand = [],
                     tags = [],
                 }) => {
                return "/filter" + createProductFilterParams({
                    boxPermalink: boxPermalink,
                    category: category,
                    queryString: queryString,
                    orderBy: orderBy,
                    brand: brand,
                    priceFilter: priceFilter,
                    tags: tags
                });


                let param = "?";

                if (boxPermalink) {
                    param = param + "b=" + boxPermalink + "&"
                }

                if (queryString) {
                    param = param + "q=" + queryString + "&"
                }

                if (orderBy) {
                    param = param + "order=" + orderBy + "&"
                }
                if (category) {
                    param = param + "cat=" + category + "&"
                }
                if (priceFilter && priceFilter.maxPrice && priceFilter.minPrice) {
                    param = param + "min_price=" + priceFilter.minPrice + "&max_price=" + priceFilter.maxPrice + "&";
                }
                if (brand && !_.isEmpty(brand)) {
                    let p = ''
                    _.forEach(brand, (b) => {
                        p = p + `br=${b}&`
                    });
                    param = param + p
                }

                return "/filter" + param.slice(0, -1);
            },
            create: ({
                         boxPermalink = "all",
                         queryString = null,
                         orderBy = null,
                         category = null,
                         priceFilter = {
                             maxPrice: null,
                             minPrice: null
                         },
                         brand = [],
                         tags = [],
                     }) => {
                return {
                    rout: rout.Product.Filter.rout,
                    as: rout.Product.Filter.as({
                        boxPermalink: boxPermalink,
                        queryString: queryString,
                        orderBy: orderBy,
                        category: category,
                        priceFilter: priceFilter,
                        brand: brand,
                        tags: tags
                    })
                }
            },
        },
        Search: {
            rout: "/search/[[...cat]]",
            baseRout: "/search",
            create: ({
                         category,
                         b,
                         q,
                         o,
                         colors,
                         brands,
                         price,
                         type,
                         ...params
                     } = {}) => {
                const as = rout.Product.Search.as({
                    category,
                    b,
                    q,
                    o,
                    colors,
                    brands,
                    price,
                    type,
                    ...params
                })
                return [
                    {
                        pathname: rout.Product.Search.rout,
                        query: as.query,
                    },
                    as
                ]
            },
            as: ({
                     category,
                     b,
                     q,
                     o,
                     colors,
                     brands,
                     price,
                     type,
                     ...params
                 } = {}) => {
                const query = {
                    q,
                    b,
                    o,
                    colors,
                    brands,
                    price,
                    type,
                    ...params
                }
                const cat = query[filterItemsKey.cat]
                category = category || cat
                delete query[filterItemsKey.cat];

                if (o === orderingItems.newest.key) {
                    delete query[filterItemsKey.ordering];
                }

                //remove undefined variable
                Object.keys(query).forEach(key => query[key] === undefined && delete query[key])
                return {
                    pathname: rout.Product.Search.baseRout + (category ? `/${category}` : ""),
                    query: query,
                }
            },
        },
        Single: {
            rout: "/product/[permalink]",
            routWithoutParam: "/product/",
            Params: {
                permalink: 'permalink',
                storage_id: 's',
            },
            goTo: "/go-to-product",
            as: ({permalink, addComment, storage_id}) => {
                if (!permalink)
                    return '/404';

                let pr = `/${permalink}?`;
                if (addComment)
                    pr = pr + 'cm&';
                if (storage_id)
                    pr = pr + `s=${storage_id}&`;
                if (pr === '?')
                    pr = '';
                if (pr)
                    pr = pr.substring(0, pr.length - 1);
                return `/product${pr}`
            },
            create: ({permalink, addComment, storage_id}) => {
                return {
                    rout: rout.Product.Single.rout,
                    as: rout.Product.Single.as({permalink, addComment, storage_id})
                }
            }
        },
        SingleV2: {
            rout: "/product/[permalink]",
            baseRout: "/product",
            Params: {
                permalink: 'permalink',
                productPreview: 'pw',
                storage_id: 's',
            },
            create: (permalink, {
                isShortLink
            } = {}) => {
                const as = rout.Product.SingleV2.as(permalink, {isShortLink})
                return [
                    {
                        pathname: rout.Product.SingleV2.rout,
                        query: {
                            permalink,
                        },
                    },
                    as
                ]
            },
            as: (permalink, {
                isShortLink
            } = {}) => {
                if (!isShortLink)
                    return rout.Product.SingleV2.baseRout + "/" + permalink
                return process.env.REACT_APP_SHORT_ROUT + "/p" + permalink
            }
        }
    },
    Main: {
        home: "/",
        search: "/search",
        BoxList: {
            main: "/categories",
            rout: "/categories",
            as: ({
                     boxPermalink = "any",
                     catsPermalink = []
                 }) => {
                return "/categories"
                let as = "/categories/" + boxPermalink;
                if (boxPermalink !== "any")
                    _.forEach(catsPermalink, (cat, i) => {
                        as = as + "/" + cat
                    })
                return as
            },
            create: ({
                         boxPermalink = "any",
                         catsPermalink = []
                     }) => {
                return {
                    rout: rout.Main.BoxList.rout,
                    as: rout.Main.BoxList.as({
                        boxPermalink: boxPermalink,
                        catsPermalink: catsPermalink
                    })
                }
            },
        },
        AboutUs: {
            rout: "/about-us",
            key: "about-us",
            create: () => {
                return {
                    rout: "/[...index]",
                    as: "/" + rout.Main.AboutUs.key
                }
            }
        },
        ContactUs: {
            rout: "/contact-us",
            key: "contact-us",
            create: () => {
                return {
                    rout: "/[...index]",
                    as: "/" + rout.Main.ContactUs.key
                }
            }
        },
        PrivacyPolicy: {
            rout: "/privacy-policy",
            key: "privacy-policy",
            create: () => {
                return {
                    rout: "/[...index]",
                    as: "/" + rout.Main.PrivacyPolicy.key
                }
            }
        },
        Test: {
            rout: '/test/[id]',
            as: ({id}) => {
                return `/test/${id}`;
            }
        },
        p404: '404'
    },
    generate: (rout, params = {}) => {
        if (_.isObject(rout)){
            const {pathname,query} = rout
            params = query
            rout = pathname
        }
        try {
        const pr = new URLSearchParams(params).toString();
        return rout + pr ? `?${pr}` : ''
        }catch {
return "#"
        }
    }

};

export default rout;

export const goBack = (routHistory, {defaultRout = rout.Main.home} = {defaultRout: rout.Main.home}) => {
    // try {
    //     if (!_.isEmpty(routHistory)) {
    //         Router.back();
    //     }
    // } catch (e) {
    // }
    UtilsRouter.goTo({routUrl: defaultRout});
};

export function routIsExact(activeRout, rout) {
    try {
        return activeRout.indexOf(rout) >= 0
    } catch (e) {
        return false;
    }
}


function createProductFilterParams(
    {
        boxPermalink = "all",
        orderBy = null,
        queryString = null,
        category = null,
        priceFilter = {
            maxPrice: null,
            minPrice: null
        },
        brand = [],
        tags = [],
    }) {
    let param = `/${boxPermalink}`;
    if (category) {
        param = param + "/" + category;
    }
    param = param + "?";


    if (orderBy) {
        param = param + "order=" + orderBy + "&"
    }
    if (queryString) {
        param = param + "query=" + queryString + "&"
    }

    if (priceFilter && priceFilter.maxPrice && priceFilter.minPrice) {
        param = param + "min_price=" + priceFilter.minPrice + "&max_price=" + priceFilter.maxPrice + "&";
    }
    if (brand && !_.isEmpty(brand)) {
        let p = ''
        _.forEach(brand, (b) => {
            p = p + `br=${b}&`
        });
        param = param + p
    }
    if (tags && !_.isEmpty(tags)) {
        let p = ''
        _.forEach(tags, (b) => {
            p = p + `tag=${b}&`
        });
        param = param + p
    }
    //remove last character of param
    return param.slice(0, -1);
}
