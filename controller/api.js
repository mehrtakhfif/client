
export default {
    User: {
        Auth: {
            signup: "/signup",
            login: "/login",
            logout: "/logout",
            verifyMobile: "/activate",
            sendCode: "/send_code",
            setPassword:"set_password",
            resetPassword: "/reset_password",
            privacyPolicy: "/privacy_policy",
        },
        Profile: {
            main: "/profile",
            avatar: "/avatar",
            orders: "/orders",
            orderProduct: "/orders/product",
            wishlist: "/wishlist",
            comments: "/user_comments",
            address: "/address"
        },
        Shopping: {
            discountCode:"/discount_code",
            basket: "/basket",
            booking: "/booking",
            ipg: "/ipg",
            payment: "/payment",
            editInvoice: "/edit_invoice"
        },
        Global: {
            getState: "/states",
            getCities: "/cities",
            addDevice:"/add_device"
        },
    },
    Main: {
        init:"/init",
        Special: {
            specialOffer: "/special_offer",
            specialProduct: "/special_product",
            allSpecialProduct: "/box_special_product",
        },
        Global: {
            slider: "/slider",
            ads: "/ads",
            search: "/suggest",
            searchHome: "/search",
            promotedCategories:'/promoted_categories',
            limitedSpecialProduct:'/limited_special_product',
            specialProduct:'/special_product',
            Test: {
                test: "/test",
                pagination: "/pagination"
            }
        }
    },
    Products: {
        get: "/product",
        getForAdmin: "/admin/product_preview",
        features: "/features",
        relatedProducts: "/related_products",
        comments: "/comment",
        query: '/filter',
        search: '/search',
        boxWithCategory: '/box_with_category',
        filter: "/box_with_category",
        filterDetail: "/filter_detail",
        getProducts: "/get_products",
        allCategories: "/categories",

    }
};

export const apiHelper = {
    generator:(link)=>{
        return process.env.REACT_APP_API + link
    },
    errorParams: (statusCode, delay = 0) => {
        if (statusCode === undefined)
            return {};
        return {
            ...apiHelper.delay(delay),
            error: true,
            status_code: statusCode,
        }
    },
    delay: (delay) => {
        if (delay === undefined)
            return {};
        const s = delay / 1000;
        return {
            delay: s
        }
    },
    getCacheParam: ({
                        staleWhileRevalidate,
                        cacheFirst,
                        networkFirst,
                        networkOnly,
                    }) => {
        return {}
        return {
            cacheR: staleWhileRevalidate ? 1 : cacheFirst ? 2 : networkFirst ? 3 : networkOnly ? 4 : null
        }
    },
    convertLocation: (latLong) => {
        if (latLong){
            return [latLong.lat, latLong.lng];
        }
        return null
    },
    createProductItem: ({storage_id, count, features = [], ...params}) => {
        return {
            ...params,
            id: storage_id,
            count: count,
            features: features
        }
    }
};
