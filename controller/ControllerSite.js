import axios from 'axios';
import _ from 'lodash';
import base_api, {apiHelper} from './api'
import {lang} from "../repository";
import rout from "../router";
import {defaultHeader} from "../component/baseSite/axios/AxoisSetup";
import {DEBUG} from "../pages/_app";

const site = {
    Search: {
        get: async ({text, ...params}) => {
            const api = base_api.Main.Global.search;
            return await axios.get(api, {
                params: {
                    q: text,
                    ...params
                }
            }).then((res) => {
                const list = [];
                let id = 1;
                _.forEach(res.data.products, (p, index) => {
                    list.push({
                        id: id++,
                        name: p.name,
                        thumbnail: {
                            type: "image",
                            title: "",
                            image: p.thumbnail,
                            box: 0
                        },
                        rout: rout.Product.SingleV2.as(p?.permalink)
                    })
                })
                return {
                    status: 200,
                    data: list
                }
            });
        },
    },
    Slider: {
        get: ({sliderName, ...params}) => {
            const api = base_api.Main.Global.slider +(sliderName ?  `/${sliderName}`:"");
            const req = async () => {
                return await axios.get(api, {
                    ...params
                }).then((res) => {
                    return {
                        slider: res.data.slider
                    }
                })
            };
            return [api, req];
        },
        getHomeSlider: (params={}) => {
            return site.Slider.get({sliderName: undefined,...params});
        }
    },
    getBoxOrCategories: ({boxPermalink, ...params} = {}) => {
        const boxApiKey = `box-list-${boxPermalink}`;
        const req = async () => {
            return await axios.get(base_api.Products.boxWithCategory, {
                params: {
                    permalink: boxPermalink,
                    ...params
                },
                ...defaultHeader.noCache
            }).then((res) => {
                return {
                    status: 200,
                    data: res.data
                }
            });
        };
        return [boxApiKey, req]
    },
    getAllCategories: () => {
        const boxApiKey = `All-Categories-list-`;
        const req = async () => {
            return await axios.get(base_api.Products.allCategories).then((res) => {
                return {
                    status: 200,
                    data: res.data
                }
            });
        };
        return [boxApiKey, req]
    },
    getCategories: async ({boxPermalink, ...params}) => {
        return await axios.get(base_api.Products.boxWithCategory, {
            params: {
                permalink: boxPermalink,
                ...params
            }
        }).then((res) => {
            return {
                status: 200,
                data: res.data
            }
        });
    },
    getBoxes: async ({withAll = false} = {}) => {
        return await axios.get(base_api.Products.boxWithCategory, {
            params: {
                ...params
            }
        }).then((res) => {
            if (withAll) {
                let child = [];
                _.forEach(res.data.boxes, (s) => {
                    child = _.concat(child, s.categories)
                });
                const all = [{
                    id: 0,
                    name: lang.get('all'),
                    permalink: 'all',
                    categories: child
                }];
                res.data.boxes = _.concat(all, res.data.boxes);
            }
            return {
                status: 200,
                data: res.data
            }
        });
    },
    IPG: {
        get: ({...params} = {}) => {
            const api = base_api.User.Shopping.ipg;
            const req = async () => {
                return await axios.get(api, {
                    params: {
                        ...params,
                        ...apiHelper.getCacheParam({networkOnly: true}),
                    },
                    ...defaultHeader.noCache
                }).then((res) => {

                    return {ipg: res.data.ipg}
                })
            };
            return [api, req]

        }
    },
    ADS: {
        get: ({key, ...params} = {}) => {
            const api = base_api.Main.Global.ads;
            const apiKey = `${key||""}-` + base_api.Main.Global.ads;

            const req = async () => {
                if (DEBUG && false) {
                    const res ={
                            "ads": [
                                {
                                    "priority": 0,
                                    "product_permalink": null,
                                    "media": {
                                        "image": "http://api.mt.com/media/boxes/None/2021-02-06/ads/10-28-16-96-has-ph.jpg",
                                        "type": "ads",
                                        "id": 10045,

                                        "category": null,

                                        "title": "handicraft web"
                                    },
                                    "url": "https://mehrtakhfif.com/search/%D8%B5%D9%86%D8%A7%DB%8C%D8%B9-%D8%AF%D8%B3%D8%AA%DB%8C",
                                    "id": 19,
                                    "title": "خرید کیف زنانه",
                                    "settings": {
                                        "size": 4
                                    }
                                },
                                {
                                    "priority": 0,
                                    "product_permalink": null,
                                    "media": {
                                        "image": "http://api.mt.com/media/boxes/None/2020-06-19/ads/08-58-12-61-has-ph.jpg",
                                        "type": "ads",
                                        "id": 2881,

                                        "category": null,
                                        "title": "ادوات موسیقی"
                                    },
                                    "url": "",
                                    "id": 8,
                                    "title": "ادوات موسیقی",
                                    "settings": {
                                        "size": 4
                                    }
                                },
                                {
                                    "priority": 0,
                                    "product_permalink": null,
                                    "media": {
                                        "image": "http://api.mt.com/media/boxes/None/2020-06-25/ads/09-24-47-28-has-ph.jpg",
                                        "type": "ads",
                                        "id": 3257,

                                        "category": null,
                                        "title": "بازی و سرگرمی"
                                    },
                                    "url": "",
                                    "id": 3,
                                    "title": "بازی و سرگرمی",
                                    "settings": {
                                        "size": 4
                                    }
                                },
                                {
                                    "priority": 1,
                                    "product_permalink": null,
                                    "media": {
                                        "image": "http://api.mt.com/media/boxes/None/2021-02-06/ads/14-00-45-74-has-ph.jpg",
                                        "type": "ads",
                                        "id": 10070,

                                        "category": null,
                                        "title": "پیراهن های نوستالژیک"
                                    },
                                    "url": "https://mehrtakhfif.com/search/club-and-national-nostalgia-shirts",
                                    "id": 21,
                                    "title": "خرید پیراهن های نوستالژیک ملی و باشگاهی",
                                    "settings": {
                                        "size": 2
                                    }
                                },
                                {
                                    "priority": 2,

                                    "product_permalink": null,

                                    "media": {
                                        "image": "http://api.mt.com/media/boxes/None/2021-02-14/ads/14-45-36-34-has-ph.jpg",
                                        "type": "ads",
                                        "id": 10153,

                                        "category": null,
                                        "title": "خرید بونسای کاج"
                                    },
                                    "url": "https://mehrtakhfif.com/product/%D8%A8%D9%88%D9%86%D8%B3%D8%A7%DB%8C-%DA%A9%D8%A7%D8%AC",
                                    "id": 20,
                                    "title": "خرید بونسای کاج کونیکا",
                                    "settings": {
                                        "size": 4
                                    }
                                },
                                {
                                    "priority": 3,
                                    "product_permalink": null,
                                    "media": {
                                        "image": "http://api.mt.com/media/boxes/None/2021-04-25/ads/09-02-46-58-has-ph.jpg",
                                        "type": "ads",
                                        "id": 10430,

                                        "category": null,
                                        "title": "خرید ادویه و چاشنی"
                                    },
                                    "url": "https://mehrtakhfif.com/search/%D8%A7%D8%AF%D9%88%DB%8C%D9%87",
                                    "id": 18,
                                    "title": "خرید ادویه و چاشنی",
                                    "settings": {
                                        "size": 2
                                    }
                                },
                                {
                                    "priority": 3,
                                    "product_permalink": null,
                                    "media": {
                                        "image": "https://api.mehrtakhfif.com/media/boxes/None/2021-04-25/slider/09-12-26-09-has-ph.jpg",
                                        "type": "ads",
                                        "id": 10430,

                                        "category": null,
                                        "title": "خرید ادویه و چاشنی"
                                    },
                                    "url": "https://mehrtakhfif.com/search/%D8%A7%D8%AF%D9%88%DB%8C%D9%87",
                                    "id": 659,
                                    "title": "خرید ادویه و چاشنی",
                                    "settings": {
                                        "size": 1
                                    }
                                },
                            ]
                        };
                    return new Promise(function (resoleve, reject) {
                        resoleve(res)
                    })
                }
                return await axios.get(api +(key ?  `/${key}`:''), {
                    ...params
                }).then((res) => {
                    return res.data
                })
            };
            return [apiKey, req]
        },
        getHome: ({...params}={}) => {
            return site.ADS.get({key: undefined, ...params})
        }
    },
    init: (params = {}) => {
        const api = base_api.Main.init;
        const req = async () => {
            return await axios.get(api, {
                ...params,
                withCredentials: true,
                ...defaultHeader.noCache
            })
        };

        return [api, req,
            {
                revalidateOnFocus: true,
                refreshInterval: 0,
                dedupingInterval: 60000,
            }]
    },
    getCities: () => {
        const api = base_api.User.Global.getCities;
        const req = async ({state_id, ...params}) => {
            return await axios.get(api + '/' + state_id, {
                params: {
                    ...params
                }
            });
        };
        return [api, req]
    },
    promotedCategories: ({...params}={}) => {
        const api = base_api.Main.Global.promotedCategories
        const req = async () => {
            if (DEBUG && false) {
                const res = {
                    "product": [
                        {
                            "permalink": "پت-شاپ",
                            "priority": 0,

                            "parent": null,

                            "media": {
                                "image": "http://api.mt.com/media/boxes/6/2020-07-09/category/08-45-06-99.jpg",
                                "type": "category",
                                "id": 4338,

                                "category": null,
                                "title": "خدمات حیوانات"
                            },
                            "type": "product",
                            "id": 388,

                            "children": [],
                            "name": "خدمات حیوانات"
                        },
                        {
                            "permalink": "عطاری",
                            "priority": 0,

                            "parent": null,

                            "media": {
                                "image": "http://api.mt.com/media/boxes/11/2020-06-29/category/15-06-48-21.jpg",
                                "type": "category",
                                "id": 3650,

                                "category": null,
                                "title": "عطاری"
                            },
                            "type": "product",
                            "id": 387,

                            "children": [],
                            "name": "عطاری"
                        },

                        {
                            "permalink": "کودک-نوجوان",
                            "priority": 0,

                            "parent": null,

                            "media": {
                                "image": "http://api.mt.com/media/boxes/4/2020-08-28/category/09-31-07-88-has-ph.jpg",
                                "type": "category",
                                "id": 52,
                                "category": 393,
                                "title": "بازی فکری"
                            },
                            "type": "product",
                            "id": 393,

                            "children": [],
                            "name": "کودک و نوجوان"
                        },

                        {
                            "permalink": "permalink-test19",
                            "priority": 0,

                            "parent": null,

                            "media": null,
                            "type": "product",
                            "id": 397,

                            "children": [],
                            "name": "category-test16"
                        },

                        {
                            "permalink": "ورزش",
                            "priority": 0,

                            "parent": null,

                            "media": {
                                "image": "http://api.mt.com/media/boxes/3/2020-06-17/category/11-30-44-33-has-ph.jpg",
                                "type": "category",
                                "id": 2735,
                                "category": 396,
                                "title": "ورزش"
                            },
                            "type": "product",
                            "id": 396,

                            "children": [],
                            "name": "لوازم ورزشی"
                        }
                    ],
                    "service": [
                        {
                            "permalink": "home-jobs",
                            "priority": 0,

                            "parent": null,

                            "media": {
                                "image": "http://api.mt.com/media/boxes/14/2020-11-01/category/09-01-01-99.jpg",
                                "type": "category",
                                "id": 7924,
                                "category": 390,
                                "title": "مشاغل تحت حمایت 2"
                            },
                            "type": "service",
                            "id": 390,

                            "children": [],
                            "name": "مشاغل تحت حمایت"
                        },

                        {
                            "permalink": "سوپرمارکت",
                            "priority": 0,

                            "parent": null,

                            "media": {
                                "image": "http://api.mt.com/media/boxes/10/2020-06-23/category/07-31-42-52.jpg",
                                "type": "category",
                                "id": 3135,
                                "category": 389,
                                "title": "سوپرمارکت"
                            },
                            "type": "service",
                            "id": 389,

                            "children": [],
                            "name": "سوپرمارکت"
                        },

                        {
                            "permalink": "electrical-components",
                            "priority": 0,

                            "parent": null,

                            "media": null,
                            "type": "service",
                            "id": 392,

                            "children": [],
                            "name": "قطعات الکتریکی"
                        },

                        {
                            "permalink": "sport-entertainment",
                            "priority": 0,

                            "parent": null,

                            "media": null,
                            "type": "service",
                            "id": 394,

                            "children": [],
                            "name": "تفریحی و ورزشی"
                        },

                        {
                            "permalink": "city-services",
                            "priority": 0,

                            "parent": null,

                            "media": null,
                            "type": "service",
                            "id": 391,

                            "children": [],
                            "name": "خدمات شهری"
                        }
                    ]
                };
                return new Promise(function (resoleve, reject) {
                    resoleve(res)
                })
            }


            return await axios.get(api,{
                ...params
            }).then((res) => {
                return res.data
            });
        };
        return [api, req]
    },
    limitedSpecialProduct:({...params}={})=>{
        const api = base_api.Main.Global.limitedSpecialProduct
        const req = async () => {
            // if (DEBUG) {
            //     const res = {
            //             "products": [
            //                 {
            //                     "default_storage": {
            //                         "deadline": null,
            //                         "id": 2635,
            //                         "title": "بونسای کاج کونیکا",
            //                         "max_count_for_sale": 10,
            //                         "vip_max_count_for_sale": null,
            //                         "vip_type": null,
            //                         "min_count_alert": 5,
            //                         "final_price": 250000,
            //                         "discount_price": 170000,
            //                         "max_shipping_time": 24,
            //                         "discount_percent": 32
            //                     },
            //                     "id": 147,
            //                     "permalink": "بونسای-کاج",
            //                     "url": null,
            //                     "thumbnail": {
            //                         "id": 10144,
            //                         "title": "بونسای کاج",
            //                         "image": "http://api.mt.com/media/boxes/2/2021-02-14/thumbnail/09-42-40-03-has-ph.jpg",
            //                         "category": 385,
            //                         "type": "thumbnail"
            //                     },
            //                     "category": null,
            //                     "name": "بونسای کاج کونیکا",
            //                     "category_id": null
            //                 },
            //                 {
            //
            //                     "default_storage": {
            //
            //                         "deadline": null,
            //                         "id": 2178,
            //                         "title": "بازی فکری مدل مافیا",
            //                         "max_count_for_sale": 1,
            //
            //                         "vip_max_count_for_sale": null,
            //
            //                         "vip_type": null,
            //                         "min_count_alert": 1,
            //                         "final_price": 37000,
            //                         "discount_price": 33000,
            //                         "max_shipping_time": 0,
            //                         "discount_percent": 10
            //                     },
            //                     "id": 146,
            //                     "permalink": "بازی-فکری-مدل-مافیا",
            //
            //                     "url": null,
            //
            //                     "thumbnail": {
            //                         "id": 9644,
            //                         "title": "مافیا_تامنیل",
            //                         "image": "http://api.mt.com/media/boxes/4/2021-01-02/thumbnail/12-26-22-27-has-ph.jpg",
            //                         "category": 393,
            //                         "type": "thumbnail"
            //                     },
            //
            //                     "category": null,
            //                     "name": "بازی فکری مدل مافیا",
            //
            //                     "category_id": null
            //                 },
            //
            //                 {
            //
            //                     "default_storage": {
            //
            //                         "deadline": null,
            //                         "id": 1601,
            //                         "title": "گلدان سفالی دیش گاردن متوسط سفید",
            //                         "max_count_for_sale": 4,
            //
            //                         "vip_max_count_for_sale": null,
            //
            //                         "vip_type": null,
            //                         "min_count_alert": 5,
            //                         "final_price": 49000,
            //                         "discount_price": 45000,
            //                         "max_shipping_time": 0,
            //                         "discount_percent": 8
            //                     },
            //                     "id": 145,
            //                     "permalink": "گلدان-سفالی-دیش-گاردن-ga04",
            //
            //                     "url": null,
            //
            //                     "thumbnail": {
            //                         "id": 8501,
            //                         "title": "گلدان سفالی دیش گاردن",
            //                         "image": "http://api.mt.com/media/boxes/2/2020-11-10/thumbnail/22-17-00-78-has-ph.jpg",
            //                         "category": 385,
            //                         "type": "thumbnail"
            //                     },
            //
            //                     "category": null,
            //                     "name": "گلدان سفالی دیش گاردن متوسط سفید",
            //
            //                     "category_id": null
            //                 },
            //
            //                 {
            //
            //                     "default_storage": {
            //
            //                         "deadline": null,
            //                         "id": 1185,
            //                         "title": "عرق نسترن ( 1 لیتری )",
            //                         "max_count_for_sale": 5,
            //
            //                         "vip_max_count_for_sale": null,
            //
            //                         "vip_type": null,
            //                         "min_count_alert": 10,
            //                         "final_price": 10500,
            //                         "discount_price": 10000,
            //                         "max_shipping_time": 0,
            //                         "discount_percent": 4
            //                     },
            //                     "id": 153,
            //                     "permalink": "عرق-نسترن-1-لیتری",
            //
            //                     "url": null,
            //
            //                     "thumbnail": {
            //                         "id": 11029,
            //                         "title": "باشگاه بدنسازی اورانوس",
            //                         "image": "http://api.mt.com/media/boxes/18/2021-07-09/thumbnail/19-10-22-73-has-ph.jpg",
            //
            //                         "category": null,
            //                         "type": "thumbnail"
            //                     },
            //
            //                     "category": null,
            //                     "name": "کفش ورزشی زنانه",
            //
            //                     "category_id": null
            //                 },
            //
            //                 {
            //
            //                     "default_storage": {
            //
            //                         "deadline": null,
            //                         "id": 2635,
            //                         "title": "بونسای کاج کونیکا",
            //                         "max_count_for_sale": 10,
            //
            //                         "vip_max_count_for_sale": null,
            //
            //                         "vip_type": null,
            //                         "min_count_alert": 5,
            //                         "final_price": 250000,
            //                         "discount_price": 170000,
            //                         "max_shipping_time": 24,
            //                         "discount_percent": 32
            //                     },
            //                     "id": 149,
            //                     "permalink": "بونسای-کاج",
            //
            //                     "url": null,
            //
            //                     "thumbnail": {
            //                         "id": 10144,
            //                         "title": "بونسای کاج",
            //                         "image": "http://api.mt.com/media/boxes/2/2021-02-14/thumbnail/09-42-40-03-has-ph.jpg",
            //                         "category": 385,
            //                         "type": "thumbnail"
            //                     },
            //
            //                     "category": null,
            //                     "name": "بونسای کاج کونیکا",
            //
            //                     "category_id": null
            //                 }
            //             ]
            //         };
            //     return new Promise(function (resoleve, reject) {
            //         resoleve(res)
            //     })
            // }


            return await axios.get(api,{
                ...params
            }).then((res) => {
                return {
                    status: 200,
                    data: res.data
                }
            });
        };
        return [api, req]
    },
    specialProduct:({...params}={})=>{
        const api = base_api.Main.Global.specialProduct;
        const req = async () => {
            if (DEBUG && false) {
                const res = {
                    "products": [
                        {
                            "id": 2,
                            "priority": 0,
                            "name": "گلکده",
                            "permalink": "گلکده",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 15,
                                        "title": " پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 48,
                                        "deadline": null,
                                        "discount_price": 72000,
                                        "max_count_for_sale": 10,
                                        "id": 721,
                                        "final_price": 85000
                                    },
                                    "permalink": "پتوس-سبز",
                                    "box_id": 2,
                                    "url": null,
                                    "name": " پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز پتوس سبز",
                                    "thumbnail": {
                                        "title": "پیتوس سبز",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/thumbnail/08-13-30-23-has-ph.jpg",
                                        "box": 2,
                                        "id": 5179,
                                        "type": "thumbnail"
                                    },
                                    "id": 131
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 12,
                                        "title": "دیفن باخیا",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 48,
                                        "deadline": null,
                                        "discount_price": 162000,
                                        "max_count_for_sale": 10,
                                        "id": 718,
                                        "final_price": 185000
                                    },
                                    "permalink": "دیفن-باخیا",
                                    "box_id": 2,
                                    "url": null,
                                    "name": "دیفن باخیا",
                                    "thumbnail": {
                                        "title": "دیفن باخیا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/thumbnail/11-29-13-65-has-ph.jpg",
                                        "box": 2,
                                        "id": 5192,
                                        "type": "thumbnail"
                                    },
                                    "id": 138
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 5,
                                        "discount_percent": 25,
                                        "title": "کاکتوس اچینو",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 12000,
                                        "max_count_for_sale": 1,
                                        "id": 1755,
                                        "final_price": 16000
                                    },
                                    "permalink": "کاکتوس-اچینو",
                                    "box_id": 2,
                                    "url": null,
                                    "name": "کاکتوس اچینو",
                                    "thumbnail": {
                                        "title": "کاکتوس اچینو",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/2/2020-11-17/thumbnail/22-36-53-90-has-ph.jpg",
                                        "box": 2,
                                        "id": 8651,
                                        "type": "thumbnail"
                                    },
                                    "id": 141
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 2,
                                        "discount_percent": 40,
                                        "title": "کاکتوس اپونتیا سیلندریکا",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 6000,
                                        "max_count_for_sale": 2,
                                        "id": 2025,
                                        "final_price": 10000
                                    },
                                    "permalink": "کاکتوس-اپونتیا-سیلندریکا",
                                    "box_id": 2,
                                    "url": null,
                                    "name": "کاکتوس اپونتیا سیلندریکا",
                                    "thumbnail": {
                                        "title": "کاکتوس اپونتیا سیلندریکا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/2/2020-12-07/thumbnail/13-57-01-27-has-ph.jpg",
                                        "box": 2,
                                        "id": 9284,
                                        "type": "thumbnail"
                                    },
                                    "id": 144
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 10,
                                        "title": "گلدان بونسای سرامیکی ساده",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 48,
                                        "deadline": null,
                                        "discount_price": 35000,
                                        "max_count_for_sale": 1,
                                        "id": 2636,
                                        "final_price": 39000
                                    },
                                    "permalink": "گلدان-بونسای-سرامیکی-ساده",
                                    "box_id": 2,
                                    "url": null,
                                    "name": "گلدان بونسای سرامیکی ساده",
                                    "thumbnail": {
                                        "title": "گلدان بونسای سرامیکی ساده",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/2/2021-02-14/thumbnail/11-35-24-80-has-ph.jpg",
                                        "box": 2,
                                        "id": 10152,
                                        "type": "thumbnail"
                                    },
                                    "id": 148
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 5,
                                        "discount_percent": 17,
                                        "title": "بونسای کاج کونیکا",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 24,
                                        "deadline": null,
                                        "discount_price": 190000,
                                        "max_count_for_sale": 10,
                                        "id": 2635,
                                        "final_price": 230000
                                    },
                                    "permalink": "بونسای-کاج-کونیکا",
                                    "box_id": 2,
                                    "url": null,
                                    "name": "بونسای کاج کونیکا",
                                    "thumbnail": {
                                        "title": "بونسای کاج کونیکا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/2/2021-02-14/thumbnail/09-42-40-03-has-ph.jpg",
                                        "box": 2,
                                        "id": 10144,
                                        "type": "thumbnail"
                                    },
                                    "id": 149
                                }
                            ]
                        },
                        {
                            "id": 3,
                            "priority": 0,
                            "name": "لوازم ورزشی",
                            "permalink": "ورزش",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 10,
                                        "title": "توپ فوتبال دربی استار بلریانت",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 141000,
                                        "max_count_for_sale": 1,
                                        "id": 427,
                                        "final_price": 157000
                                    },
                                    "permalink": "توپ-فوتبال-دربی-استار-بلریانت",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "توپ فوتبال دربی استار بلریانت",
                                    "thumbnail": {
                                        "title": "توپ فوتبال دربی استار بلریانت",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-06-27/thumbnail/14-28-23-38-has-ph.jpg",
                                        "box": 3,
                                        "id": 3464,
                                        "type": "thumbnail"
                                    },
                                    "id": 77
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 10,
                                        "title": "توپ فوتبال آدیداس Tsubasa",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 126000,
                                        "max_count_for_sale": 1,
                                        "id": 442,
                                        "final_price": 140000
                                    },
                                    "permalink": "توپ-فوتبال-آدیداس-tsubasa",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "توپ فوتبال آدیداس Tsubasa",
                                    "thumbnail": {
                                        "title": "توپ فوتبال آدیداس Tsubasa",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-06-28/thumbnail/09-45-50-85-has-ph.jpg",
                                        "box": 3,
                                        "id": 3578,
                                        "type": "thumbnail"
                                    },
                                    "id": 78
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 10,
                                        "title": "طناب ورزشی کراس فیت کد004",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 97000,
                                        "max_count_for_sale": 1,
                                        "id": 340,
                                        "final_price": 108000
                                    },
                                    "permalink": "حذف-3",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "طناب ورزشی کراس فیت کد004",
                                    "thumbnail": {
                                        "title": "طناب ورزشی کراس فیت کد004",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-06-15/thumbnail/12-45-56-39-has-ph.jpg",
                                        "box": 3,
                                        "id": 2701,
                                        "type": "thumbnail"
                                    },
                                    "id": 80
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 0,
                                        "title": "شیکر آندر آرمور 003",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 24,
                                        "deadline": null,
                                        "discount_price": 0,
                                        "max_count_for_sale": 0,
                                        "id": 824,
                                        "final_price": 0
                                    },
                                    "permalink": "شیکر-آندرآرمور-003",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "شیکر آندر آرمور 003",
                                    "thumbnail": {
                                        "title": "شیکر",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-08-09/thumbnail/13-52-51-01-has-ph.jpg",
                                        "box": 3,
                                        "id": 5406,
                                        "type": "thumbnail"
                                    },
                                    "id": 82
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 18,
                                        "title": "حلقه تقویت مچ 6586",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 24,
                                        "deadline": null,
                                        "discount_price": 13000,
                                        "max_count_for_sale": 1,
                                        "id": 829,
                                        "final_price": 16000
                                    },
                                    "permalink": "ادغام-شود-5",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "حلقه تقویت مچ 6586",
                                    "thumbnail": {
                                        "title": "حلقه تقویت مچ",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-08-10/thumbnail/11-20-06-48-has-ph.jpg",
                                        "box": 3,
                                        "id": 5428,
                                        "type": "thumbnail"
                                    },
                                    "id": 84
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 0,
                                        "discount_percent": 12,
                                        "title": "کش پیلاتس لوپ مدل سانلاین اسپرتس",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 92000,
                                        "max_count_for_sale": 1,
                                        "id": 343,
                                        "final_price": 105000
                                    },
                                    "permalink": "کش-پیلاتس-لوپ-مدل-سانلین-اسپرتس",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "کش پیلاتس لوپ مدل سانلاین اسپرتس",
                                    "thumbnail": {
                                        "title": "کش پیلاتس لوپ مدل سانلاین اسپرتس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-06-15/thumbnail/15-13-38-05-has-ph.jpg",
                                        "box": 3,
                                        "id": 2712,
                                        "type": "thumbnail"
                                    },
                                    "id": 86
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 10,
                                        "title": "کش ورزشی بادی هلدر لول GM-1",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 107000,
                                        "max_count_for_sale": 1,
                                        "id": 349,
                                        "final_price": 120000
                                    },
                                    "permalink": "کش-ورزشی-بادی-هلدر",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "کش ورزشی بادی هلدر لول GM-1",
                                    "thumbnail": {
                                        "title": "کش ورزشی بادی هلدر لول GM-1",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-06-16/thumbnail/09-15-35-82-has-ph.jpg",
                                        "box": 3,
                                        "id": 2716,
                                        "type": "thumbnail"
                                    },
                                    "id": 87
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 10,
                                        "title": "طناب ورزشی کراس فیت کد VS20",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 97000,
                                        "max_count_for_sale": 1,
                                        "id": 338,
                                        "final_price": 108000
                                    },
                                    "permalink": "حذفیی",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "طناب ورزشی کراس فیت کد VS20",
                                    "thumbnail": {
                                        "title": "طناب ورزشی کراس فیت کد VS20",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-06-15/thumbnail/08-45-04-54-has-ph.jpg",
                                        "box": 3,
                                        "id": 2692,
                                        "type": "thumbnail"
                                    },
                                    "id": 89
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 14,
                                        "title": "کوله کوهنوردی 70 لیتری بوراک",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 48,
                                        "deadline": null,
                                        "discount_price": 760000,
                                        "max_count_for_sale": 2,
                                        "id": 1350,
                                        "final_price": 890000
                                    },
                                    "permalink": "کوله-کوهنوردی-70-لیتری-بوراک",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "کوله کوهنوردی 70 لیتری بوراک",
                                    "thumbnail": {
                                        "title": "کوله کوهنوردی 70 لیتری بوراک ",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-10-21/thumbnail/09-37-58-89-has-ph.jpg",
                                        "box": 3,
                                        "id": 7592,
                                        "type": "thumbnail"
                                    },
                                    "id": 124
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 1,
                                        "discount_percent": 12,
                                        "title": "کفش ورزشی پیاده روی مدل زد ایکس 8000 طرح آدیداس",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 219000,
                                        "max_count_for_sale": 1,
                                        "id": 1459,
                                        "final_price": 250000
                                    },
                                    "permalink": "کفش-ورزشی-پیاده-روی-مدل-زد-ایکس-8000-طرح-آدیداس",
                                    "box_id": 3,
                                    "url": null,
                                    "name": "کفش ورزشی پیاده روی مدل زد ایکس 8000 طرح آدیداس",
                                    "thumbnail": {
                                        "title": "کفش ورزشی پیاده روی مدل زد ایکس طرح آدیداس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/3/2020-11-03/thumbnail/06-51-03-47-has-ph.jpg",
                                        "box": 3,
                                        "id": 8011,
                                        "type": "thumbnail"
                                    },
                                    "id": 125
                                }
                            ]
                        },
                        {
                            "id": 11,
                            "priority": 0,
                            "name": "عطاری",
                            "permalink": "عطاری",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 25,
                                        "title": " چاشنی قیمه زعفرانی 100 گرم چاشنی قیمه زعفرانی 100 گرم چاشنی قیمه زعفرانی 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 6000,
                                        "max_count_for_sale": 2,
                                        "id": 982,
                                        "final_price": 8000
                                    },
                                    "permalink": "چاشنی-قیمه-زعفرانی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "چاشنی قیمه زعفرانی 100 گرم",
                                    "thumbnail": {
                                        "title": "چاشنی قیمه زعفرانی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-20/thumbnail/10-45-09-12-has-ph.jpg",
                                        "box": 11,
                                        "id": 5850,
                                        "type": "thumbnail"
                                    },
                                    "id": 120
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 18,
                                        "title": "فلوس 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9000,
                                        "max_count_for_sale": 5,
                                        "id": 500,
                                        "final_price": 11000
                                    },
                                    "permalink": "فلوس",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "فلوس 100 گرمی",
                                    "thumbnail": {
                                        "title": "فلوس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/08-58-34-07-has-ph.jpg",
                                        "box": 11,
                                        "id": 6387,
                                        "type": "thumbnail"
                                    },
                                    "id": 121
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 17,
                                        "title": "بذر تربچه 25 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 5750,
                                        "max_count_for_sale": 5,
                                        "id": 1140,
                                        "final_price": 7000
                                    },
                                    "permalink": "بذر-تربچه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "بذر تربچه 25 گرمی",
                                    "thumbnail": {
                                        "title": "بذر تربچه",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-12/thumbnail/07-21-21-60-has-ph.jpg",
                                        "box": 11,
                                        "id": 6783,
                                        "type": "thumbnail"
                                    },
                                    "id": 123
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 25,
                                        "title": "نمک هیمالیا 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 15000,
                                        "max_count_for_sale": 5,
                                        "id": 474,
                                        "final_price": 20000
                                    },
                                    "permalink": "نمک-هیمالیا",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "نمک هیمالیا 100 گرمی",
                                    "thumbnail": {
                                        "title": "نمک هیمالیا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/07-16-24-64-has-ph.jpg",
                                        "box": 11,
                                        "id": 6362,
                                        "type": "thumbnail"
                                    },
                                    "id": 116
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 6,
                                        "title": "آویشن شیرازی 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 28000,
                                        "max_count_for_sale": 2,
                                        "id": 849,
                                        "final_price": 30000
                                    },
                                    "permalink": "آویشن-شیرازی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "آویشن شیرازی 100 گرمی",
                                    "thumbnail": {
                                        "title": "آویشن شیرازی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-24/thumbnail/12-07-36-66-has-ph.jpg",
                                        "box": 11,
                                        "id": 6113,
                                        "type": "thumbnail"
                                    },
                                    "id": 117
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 5,
                                        "title": "عرق معجون چربی خون",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 16000,
                                        "max_count_for_sale": 5,
                                        "id": 1175,
                                        "final_price": 17000
                                    },
                                    "permalink": "زردچوبه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "عرق معجون چربی خون",
                                    "thumbnail": {
                                        "title": "معجون  چربی خون",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-15/thumbnail/10-17-26-81-has-ph.jpg",
                                        "box": 11,
                                        "id": 6938,
                                        "type": "thumbnail"
                                    },
                                    "id": 118
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 8,
                                        "title": "صابون تریاک",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 55000,
                                        "max_count_for_sale": 1,
                                        "id": 1250,
                                        "final_price": 60000
                                    },
                                    "permalink": "صابون-تریاک",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "صابون تریاک",
                                    "thumbnail": {
                                        "title": "صابون تریاک",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-04/thumbnail/14-31-00-95-has-ph.jpg",
                                        "box": 11,
                                        "id": 7358,
                                        "type": "thumbnail"
                                    },
                                    "id": 119
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 16,
                                        "title": "سپستان 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 10000,
                                        "max_count_for_sale": 2,
                                        "id": 1802,
                                        "final_price": 12000
                                    },
                                    "permalink": "سپستان",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "سپستان 100 گرم",
                                    "thumbnail": {
                                        "title": "سپستان",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-27/thumbnail/11-54-52-25-has-ph.jpg",
                                        "box": 11,
                                        "id": 7706,
                                        "type": "thumbnail"
                                    },
                                    "id": 139
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 38,
                                        "title": "گِل سرشور 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9900,
                                        "max_count_for_sale": 2,
                                        "id": 1730,
                                        "final_price": 16000
                                    },
                                    "permalink": "گل-سرشور",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "گِل سرشور 100 گرم",
                                    "thumbnail": {
                                        "title": "گِل سرشور",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-28/thumbnail/13-16-42-72-has-ph.jpg",
                                        "box": 11,
                                        "id": 7763,
                                        "type": "thumbnail"
                                    },
                                    "id": 140
                                }
                            ]
                        },
                        {
                            "id": 12,
                            "priority": 0,
                            "name": "عطاری",
                            "permalink": "عطاری",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 25,
                                        "title": "چاشنی قیمه زعفرانی 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 6000,
                                        "max_count_for_sale": 2,
                                        "id": 982,
                                        "final_price": 8000
                                    },
                                    "permalink": "چاشنی-قیمه-زعفرانی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "چاشنی قیمه زعفرانی 100 گرم",
                                    "thumbnail": {
                                        "title": "چاشنی قیمه زعفرانی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-20/thumbnail/10-45-09-12-has-ph.jpg",
                                        "box": 11,
                                        "id": 5850,
                                        "type": "thumbnail"
                                    },
                                    "id": 120
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 18,
                                        "title": "فلوس 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9000,
                                        "max_count_for_sale": 5,
                                        "id": 500,
                                        "final_price": 11000
                                    },
                                    "permalink": "فلوس",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "فلوس 100 گرمی",
                                    "thumbnail": {
                                        "title": "فلوس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/08-58-34-07-has-ph.jpg",
                                        "box": 11,
                                        "id": 6387,
                                        "type": "thumbnail"
                                    },
                                    "id": 121
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 17,
                                        "title": "بذر تربچه 25 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 5750,
                                        "max_count_for_sale": 5,
                                        "id": 1140,
                                        "final_price": 7000
                                    },
                                    "permalink": "بذر-تربچه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "بذر تربچه 25 گرمی",
                                    "thumbnail": {
                                        "title": "بذر تربچه",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-12/thumbnail/07-21-21-60-has-ph.jpg",
                                        "box": 11,
                                        "id": 6783,
                                        "type": "thumbnail"
                                    },
                                    "id": 123
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 25,
                                        "title": "نمک هیمالیا 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 15000,
                                        "max_count_for_sale": 5,
                                        "id": 474,
                                        "final_price": 20000
                                    },
                                    "permalink": "نمک-هیمالیا",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "نمک هیمالیا 100 گرمی",
                                    "thumbnail": {
                                        "title": "نمک هیمالیا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/07-16-24-64-has-ph.jpg",
                                        "box": 11,
                                        "id": 6362,
                                        "type": "thumbnail"
                                    },
                                    "id": 116
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 6,
                                        "title": "آویشن شیرازی 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 28000,
                                        "max_count_for_sale": 2,
                                        "id": 849,
                                        "final_price": 30000
                                    },
                                    "permalink": "آویشن-شیرازی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "آویشن شیرازی 100 گرمی",
                                    "thumbnail": {
                                        "title": "آویشن شیرازی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-24/thumbnail/12-07-36-66-has-ph.jpg",
                                        "box": 11,
                                        "id": 6113,
                                        "type": "thumbnail"
                                    },
                                    "id": 117
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 5,
                                        "title": "عرق معجون چربی خون",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 16000,
                                        "max_count_for_sale": 5,
                                        "id": 1175,
                                        "final_price": 17000
                                    },
                                    "permalink": "زردچوبه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "عرق معجون چربی خون",
                                    "thumbnail": {
                                        "title": "معجون  چربی خون",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-15/thumbnail/10-17-26-81-has-ph.jpg",
                                        "box": 11,
                                        "id": 6938,
                                        "type": "thumbnail"
                                    },
                                    "id": 118
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 8,
                                        "title": "صابون تریاک",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 55000,
                                        "max_count_for_sale": 1,
                                        "id": 1250,
                                        "final_price": 60000
                                    },
                                    "permalink": "صابون-تریاک",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "صابون تریاک",
                                    "thumbnail": {
                                        "title": "صابون تریاک",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-04/thumbnail/14-31-00-95-has-ph.jpg",
                                        "box": 11,
                                        "id": 7358,
                                        "type": "thumbnail"
                                    },
                                    "id": 119
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 16,
                                        "title": "سپستان 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 10000,
                                        "max_count_for_sale": 2,
                                        "id": 1802,
                                        "final_price": 12000
                                    },
                                    "permalink": "سپستان",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "سپستان 100 گرم",
                                    "thumbnail": {
                                        "title": "سپستان",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-27/thumbnail/11-54-52-25-has-ph.jpg",
                                        "box": 11,
                                        "id": 7706,
                                        "type": "thumbnail"
                                    },
                                    "id": 139
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 38,
                                        "title": "گِل سرشور 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9900,
                                        "max_count_for_sale": 2,
                                        "id": 1730,
                                        "final_price": 16000
                                    },
                                    "permalink": "گل-سرشور",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "گِل سرشور 100 گرم",
                                    "thumbnail": {
                                        "title": "گِل سرشور",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-28/thumbnail/13-16-42-72-has-ph.jpg",
                                        "box": 11,
                                        "id": 7763,
                                        "type": "thumbnail"
                                    },
                                    "id": 140
                                }
                            ]
                        },
                        {
                            "id": 13,
                            "priority": 0,
                            "name": "عطاری",
                            "permalink": "عطاری",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 25,
                                        "title": "چاشنی قیمه زعفرانی 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 6000,
                                        "max_count_for_sale": 2,
                                        "id": 982,
                                        "final_price": 8000
                                    },
                                    "permalink": "چاشنی-قیمه-زعفرانی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "چاشنی قیمه زعفرانی 100 گرم",
                                    "thumbnail": {
                                        "title": "چاشنی قیمه زعفرانی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-20/thumbnail/10-45-09-12-has-ph.jpg",
                                        "box": 11,
                                        "id": 5850,
                                        "type": "thumbnail"
                                    },
                                    "id": 120
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 18,
                                        "title": "فلوس 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9000,
                                        "max_count_for_sale": 5,
                                        "id": 500,
                                        "final_price": 11000
                                    },
                                    "permalink": "فلوس",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "فلوس 100 گرمی",
                                    "thumbnail": {
                                        "title": "فلوس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/08-58-34-07-has-ph.jpg",
                                        "box": 11,
                                        "id": 6387,
                                        "type": "thumbnail"
                                    },
                                    "id": 121
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 17,
                                        "title": "بذر تربچه 25 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 5750,
                                        "max_count_for_sale": 5,
                                        "id": 1140,
                                        "final_price": 7000
                                    },
                                    "permalink": "بذر-تربچه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "بذر تربچه 25 گرمی",
                                    "thumbnail": {
                                        "title": "بذر تربچه",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-12/thumbnail/07-21-21-60-has-ph.jpg",
                                        "box": 11,
                                        "id": 6783,
                                        "type": "thumbnail"
                                    },
                                    "id": 123
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 25,
                                        "title": "نمک هیمالیا 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 15000,
                                        "max_count_for_sale": 5,
                                        "id": 474,
                                        "final_price": 20000
                                    },
                                    "permalink": "نمک-هیمالیا",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "نمک هیمالیا 100 گرمی",
                                    "thumbnail": {
                                        "title": "نمک هیمالیا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/07-16-24-64-has-ph.jpg",
                                        "box": 11,
                                        "id": 6362,
                                        "type": "thumbnail"
                                    },
                                    "id": 116
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 6,
                                        "title": "آویشن شیرازی 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 28000,
                                        "max_count_for_sale": 2,
                                        "id": 849,
                                        "final_price": 30000
                                    },
                                    "permalink": "آویشن-شیرازی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "آویشن شیرازی 100 گرمی",
                                    "thumbnail": {
                                        "title": "آویشن شیرازی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-24/thumbnail/12-07-36-66-has-ph.jpg",
                                        "box": 11,
                                        "id": 6113,
                                        "type": "thumbnail"
                                    },
                                    "id": 117
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 5,
                                        "title": "عرق معجون چربی خون",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 16000,
                                        "max_count_for_sale": 5,
                                        "id": 1175,
                                        "final_price": 17000
                                    },
                                    "permalink": "زردچوبه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "عرق معجون چربی خون",
                                    "thumbnail": {
                                        "title": "معجون  چربی خون",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-15/thumbnail/10-17-26-81-has-ph.jpg",
                                        "box": 11,
                                        "id": 6938,
                                        "type": "thumbnail"
                                    },
                                    "id": 118
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 8,
                                        "title": "صابون تریاک",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 55000,
                                        "max_count_for_sale": 1,
                                        "id": 1250,
                                        "final_price": 60000
                                    },
                                    "permalink": "صابون-تریاک",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "صابون تریاک",
                                    "thumbnail": {
                                        "title": "صابون تریاک",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-04/thumbnail/14-31-00-95-has-ph.jpg",
                                        "box": 11,
                                        "id": 7358,
                                        "type": "thumbnail"
                                    },
                                    "id": 119
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 16,
                                        "title": "سپستان 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 10000,
                                        "max_count_for_sale": 2,
                                        "id": 1802,
                                        "final_price": 12000
                                    },
                                    "permalink": "سپستان",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "سپستان 100 گرم",
                                    "thumbnail": {
                                        "title": "سپستان",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-27/thumbnail/11-54-52-25-has-ph.jpg",
                                        "box": 11,
                                        "id": 7706,
                                        "type": "thumbnail"
                                    },
                                    "id": 139
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 38,
                                        "title": "گِل سرشور 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9900,
                                        "max_count_for_sale": 2,
                                        "id": 1730,
                                        "final_price": 16000
                                    },
                                    "permalink": "گل-سرشور",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "گِل سرشور 100 گرم",
                                    "thumbnail": {
                                        "title": "گِل سرشور",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-28/thumbnail/13-16-42-72-has-ph.jpg",
                                        "box": 11,
                                        "id": 7763,
                                        "type": "thumbnail"
                                    },
                                    "id": 140
                                }
                            ]
                        },
                        {
                            "id": 14,
                            "priority": 0,
                            "name": "عطاری",
                            "permalink": "عطاری",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 25,
                                        "title": "چاشنی قیمه زعفرانی 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 6000,
                                        "max_count_for_sale": 2,
                                        "id": 982,
                                        "final_price": 8000
                                    },
                                    "permalink": "چاشنی-قیمه-زعفرانی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "چاشنی قیمه زعفرانی 100 گرم",
                                    "thumbnail": {
                                        "title": "چاشنی قیمه زعفرانی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-20/thumbnail/10-45-09-12-has-ph.jpg",
                                        "box": 11,
                                        "id": 5850,
                                        "type": "thumbnail"
                                    },
                                    "id": 120
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 18,
                                        "title": "فلوس 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9000,
                                        "max_count_for_sale": 5,
                                        "id": 500,
                                        "final_price": 11000
                                    },
                                    "permalink": "فلوس",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "فلوس 100 گرمی",
                                    "thumbnail": {
                                        "title": "فلوس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/08-58-34-07-has-ph.jpg",
                                        "box": 11,
                                        "id": 6387,
                                        "type": "thumbnail"
                                    },
                                    "id": 121
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 17,
                                        "title": "بذر تربچه 25 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 5750,
                                        "max_count_for_sale": 5,
                                        "id": 1140,
                                        "final_price": 7000
                                    },
                                    "permalink": "بذر-تربچه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "بذر تربچه 25 گرمی",
                                    "thumbnail": {
                                        "title": "بذر تربچه",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-12/thumbnail/07-21-21-60-has-ph.jpg",
                                        "box": 11,
                                        "id": 6783,
                                        "type": "thumbnail"
                                    },
                                    "id": 123
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 25,
                                        "title": "نمک هیمالیا 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 15000,
                                        "max_count_for_sale": 5,
                                        "id": 474,
                                        "final_price": 20000
                                    },
                                    "permalink": "نمک-هیمالیا",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "نمک هیمالیا 100 گرمی",
                                    "thumbnail": {
                                        "title": "نمک هیمالیا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/07-16-24-64-has-ph.jpg",
                                        "box": 11,
                                        "id": 6362,
                                        "type": "thumbnail"
                                    },
                                    "id": 116
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 6,
                                        "title": "آویشن شیرازی 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 28000,
                                        "max_count_for_sale": 2,
                                        "id": 849,
                                        "final_price": 30000
                                    },
                                    "permalink": "آویشن-شیرازی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "آویشن شیرازی 100 گرمی",
                                    "thumbnail": {
                                        "title": "آویشن شیرازی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-24/thumbnail/12-07-36-66-has-ph.jpg",
                                        "box": 11,
                                        "id": 6113,
                                        "type": "thumbnail"
                                    },
                                    "id": 117
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 5,
                                        "title": "عرق معجون چربی خون",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 16000,
                                        "max_count_for_sale": 5,
                                        "id": 1175,
                                        "final_price": 17000
                                    },
                                    "permalink": "زردچوبه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "عرق معجون چربی خون",
                                    "thumbnail": {
                                        "title": "معجون  چربی خون",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-15/thumbnail/10-17-26-81-has-ph.jpg",
                                        "box": 11,
                                        "id": 6938,
                                        "type": "thumbnail"
                                    },
                                    "id": 118
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 8,
                                        "title": "صابون تریاک",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 55000,
                                        "max_count_for_sale": 1,
                                        "id": 1250,
                                        "final_price": 60000
                                    },
                                    "permalink": "صابون-تریاک",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "صابون تریاک",
                                    "thumbnail": {
                                        "title": "صابون تریاک",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-04/thumbnail/14-31-00-95-has-ph.jpg",
                                        "box": 11,
                                        "id": 7358,
                                        "type": "thumbnail"
                                    },
                                    "id": 119
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 16,
                                        "title": "سپستان 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 10000,
                                        "max_count_for_sale": 2,
                                        "id": 1802,
                                        "final_price": 12000
                                    },
                                    "permalink": "سپستان",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "سپستان 100 گرم",
                                    "thumbnail": {
                                        "title": "سپستان",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-27/thumbnail/11-54-52-25-has-ph.jpg",
                                        "box": 11,
                                        "id": 7706,
                                        "type": "thumbnail"
                                    },
                                    "id": 139
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 38,
                                        "title": "گِل سرشور 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9900,
                                        "max_count_for_sale": 2,
                                        "id": 1730,
                                        "final_price": 16000
                                    },
                                    "permalink": "گل-سرشور",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "گِل سرشور 100 گرم",
                                    "thumbnail": {
                                        "title": "گِل سرشور",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-28/thumbnail/13-16-42-72-has-ph.jpg",
                                        "box": 11,
                                        "id": 7763,
                                        "type": "thumbnail"
                                    },
                                    "id": 140
                                }
                            ]
                        },
                        {
                            "id": 15,
                            "priority": 0,
                            "name": "عطاری",
                            "permalink": "عطاری",
                            "special_products": [
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 25,
                                        "title": "چاشنی قیمه زعفرانی 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 6000,
                                        "max_count_for_sale": 2,
                                        "id": 982,
                                        "final_price": 8000
                                    },
                                    "permalink": "چاشنی-قیمه-زعفرانی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "چاشنی قیمه زعفرانی 100 گرم",
                                    "thumbnail": {
                                        "title": "چاشنی قیمه زعفرانی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-20/thumbnail/10-45-09-12-has-ph.jpg",
                                        "box": 11,
                                        "id": 5850,
                                        "type": "thumbnail"
                                    },
                                    "id": 120
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 18,
                                        "title": "فلوس 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9000,
                                        "max_count_for_sale": 5,
                                        "id": 500,
                                        "final_price": 11000
                                    },
                                    "permalink": "فلوس",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "فلوس 100 گرمی",
                                    "thumbnail": {
                                        "title": "فلوس",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/08-58-34-07-has-ph.jpg",
                                        "box": 11,
                                        "id": 6387,
                                        "type": "thumbnail"
                                    },
                                    "id": 121
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 17,
                                        "title": "بذر تربچه 25 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 5750,
                                        "max_count_for_sale": 5,
                                        "id": 1140,
                                        "final_price": 7000
                                    },
                                    "permalink": "بذر-تربچه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "بذر تربچه 25 گرمی",
                                    "thumbnail": {
                                        "title": "بذر تربچه",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-12/thumbnail/07-21-21-60-has-ph.jpg",
                                        "box": 11,
                                        "id": 6783,
                                        "type": "thumbnail"
                                    },
                                    "id": 123
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 25,
                                        "title": "نمک هیمالیا 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 15000,
                                        "max_count_for_sale": 5,
                                        "id": 474,
                                        "final_price": 20000
                                    },
                                    "permalink": "نمک-هیمالیا",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "نمک هیمالیا 100 گرمی",
                                    "thumbnail": {
                                        "title": "نمک هیمالیا",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-28/thumbnail/07-16-24-64-has-ph.jpg",
                                        "box": 11,
                                        "id": 6362,
                                        "type": "thumbnail"
                                    },
                                    "id": 116
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 8,
                                        "discount_percent": 6,
                                        "title": "آویشن شیرازی 100 گرمی",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 28000,
                                        "max_count_for_sale": 2,
                                        "id": 849,
                                        "final_price": 30000
                                    },
                                    "permalink": "آویشن-شیرازی",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "آویشن شیرازی 100 گرمی",
                                    "thumbnail": {
                                        "title": "آویشن شیرازی",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-08-24/thumbnail/12-07-36-66-has-ph.jpg",
                                        "box": 11,
                                        "id": 6113,
                                        "type": "thumbnail"
                                    },
                                    "id": 117
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 10,
                                        "discount_percent": 5,
                                        "title": "عرق معجون چربی خون",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 16000,
                                        "max_count_for_sale": 5,
                                        "id": 1175,
                                        "final_price": 17000
                                    },
                                    "permalink": "زردچوبه",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "عرق معجون چربی خون",
                                    "thumbnail": {
                                        "title": "معجون  چربی خون",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-09-15/thumbnail/10-17-26-81-has-ph.jpg",
                                        "box": 11,
                                        "id": 6938,
                                        "type": "thumbnail"
                                    },
                                    "id": 118
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 3,
                                        "discount_percent": 8,
                                        "title": "صابون تریاک",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 55000,
                                        "max_count_for_sale": 1,
                                        "id": 1250,
                                        "final_price": 60000
                                    },
                                    "permalink": "صابون-تریاک",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "صابون تریاک",
                                    "thumbnail": {
                                        "title": "صابون تریاک",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-04/thumbnail/14-31-00-95-has-ph.jpg",
                                        "box": 11,
                                        "id": 7358,
                                        "type": "thumbnail"
                                    },
                                    "id": 119
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 16,
                                        "title": "سپستان 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 10000,
                                        "max_count_for_sale": 2,
                                        "id": 1802,
                                        "final_price": 12000
                                    },
                                    "permalink": "سپستان",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "سپستان 100 گرم",
                                    "thumbnail": {
                                        "title": "سپستان",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-27/thumbnail/11-54-52-25-has-ph.jpg",
                                        "box": 11,
                                        "id": 7706,
                                        "type": "thumbnail"
                                    },
                                    "id": 139
                                },
                                {
                                    "default_storage": {
                                        "min_count_alert": 6,
                                        "discount_percent": 38,
                                        "title": "گِل سرشور 100 گرم",
                                        "vip_type": null,
                                        "vip_max_count_for_sale": null,
                                        "max_shipping_time": 0,
                                        "deadline": null,
                                        "discount_price": 9900,
                                        "max_count_for_sale": 2,
                                        "id": 1730,
                                        "final_price": 16000
                                    },
                                    "permalink": "گل-سرشور",
                                    "box_id": 11,
                                    "url": null,
                                    "name": "گِل سرشور 100 گرم",
                                    "thumbnail": {
                                        "title": "گِل سرشور",
                                        "image": "https://api.mehrtakhfif.com/media/boxes/11/2020-10-28/thumbnail/13-16-42-72-has-ph.jpg",
                                        "box": 11,
                                        "id": 7763,
                                        "type": "thumbnail"
                                    },
                                    "id": 140
                                }
                            ]
                        },
                    ]
                }
                return new Promise(function (resoleve, reject) {
                    resoleve(res)
                })
            }
            return await axios.get(api,{
                ...params
            }).then((res) => {
                return res.data
            });
        };
        return [api, req]
    }
};

export default site;
