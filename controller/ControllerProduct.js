import _ from 'lodash'
import React from "react";
import base_api, {apiHelper} from './api';
import {
    convertComments,
    convertPagination,
    convertProduct,
    convertProducts,
    convertStorage,
    convertUser,
    keyGenerator,
    paginationParams
} from './converter'
import axios from 'axios'
import {onUserUpdated} from "../middleware/auth";
import rout from "../router";
import {getSafe, Random, sleep, tryIt} from "material-ui-helper";
import {DEBUG} from "../pages/_app";
import {defaultHeader} from "../component/baseSite/axios/AxoisSetup";

export const relatedProductsDefaultStep = 6;
export const commentsDefaultStep = 10;

const cp = {
    Query: {
        products: ({...params} = {}) => {
            const api = base_api.Products.query;

            const request = {
                url: api,
                params: {
                    ...params
                }
            };

            const r = async ({headers, page = 1, step, ...rParams}) =>
                await axios(api, {
                    headers: headers,
                    params: {
                        ...paginationParams(page, step),
                        ...params,
                        ...rParams
                    }
                } || {}, {}).then(res => {
                    return {
                        pagination: convertPagination(res.data.pagination, page),
                        products: convertProducts(res.data.data)
                    }
                });
            return (
                [request && JSON.stringify(request), r]
            )
        },
        getFilters: () => {
            const api = base_api.Products.filterDetail;
            const req = async ({...params}) => {
                return await axios.get(api, {
                    params: {
                        ...params
                    }
                }).then((res) => {
                    return {
                        box: res.data.box,
                        maxPrice: res.data.max_price,
                        minPrice: res.data.min_price,
                        categories: res.data.categories,
                        brands: res.data.brands
                    }
                })
            };
            return [api, req];
        },
        V2: {
            queryConvertor: (query) => {
                return getSafe(() => {
                    const {price, cat, ...params} = query;
                    tryIt(() => {
                        params.min_price = price[0]
                        params.max_price = price[1]
                    })
                    tryIt(() => {
                        params.cat = cat[0]
                    })
                    return params
                })
            },
            search: ({query = ""}) => {
                const api = base_api.Products.search;
                return [
                    keyGenerator(base_api.Products.search + "-v2", {query}),
                    async () => {
                        if (query.length < 2)
                            return
                        if (DEBUG)
                            await sleep(1000)
                        return await axios.get(api, {
                            params: {
                                q: query,
                            }
                        }).then((res) => {
                            const data = res.data
                            // if (!DEBUG) {
                            //     delete data["tags"];
                            //     // delete data["categories"];
                            // }
                            let dataLength = 0;

                            try {
                                const getName = (name) => {
                                    try {
                                        //const re = new RegExp(`\\S*${query}\\S*`, "g");
                                        const re = new RegExp(`${query}`, "g");
                                        let {match, matchIndex} = getSafe(() => {
                                            const m = re.exec(name)
                                            return {
                                                match: m[0],
                                                matchIndex: m.index
                                            }
                                        })
                                        // let match = getSafe(()=>it.name.match(`\\S*${query}\\S*`)[0])

                                        if (match) {
                                            const first = getSafe(() => name.substring(0, matchIndex), "")
                                            const end = getSafe(() => name.substring(first.length + match.length, name.length), "")
                                            return getSafe(() => (
                                                (first ? `${first}` : "") +
                                                `<b>${match}</b>` +
                                                (end ? (end) : "")
                                            ), name)


                                            // return getSafe(()=>(
                                            //     `<b>${first}</b>`+
                                            //     `&nbsp;${match}&nbsp;` +
                                            //     `<b>${end}</b>`
                                            // ),it.name)
                                        }
                                        return name
                                    } catch (e) {
                                    }
                                    return name
                                }
                                _.forEach(data, (item, key) => {

                                    try {
                                        dataLength = dataLength + item.length
                                        _.forEach(item, (it, index) => {
                                            data[key][index].orgName = `${it.name}`
                                            data[key][index].name = getName(it.name)
                                            data[key][index].parent = getName(it.parent)
                                        })
                                    } catch (e) {
                                    }
                                })
                            } catch (e) {
                            }

                            return {
                                data: data,
                                dataLength: dataLength
                            }
                        }).catch((e) => {
                        })
                    }
                ]
            },
            query: (params) => {
                const apiKey = keyGenerator(base_api.Products.query + "-v2", params);
                return [apiKey, async () => {
                    if (DEBUG)
                        await sleep(4000)
                    if (_.isEmpty(params))
                        return {}
                    return await axios.get(base_api.Products.query, {
                        params
                    }).then(res => {
                        return {
                            data: res.data.data
                        }
                    })
                    return {
                        status: `Ok-${Random.randomInteger(0, 20000)}`,
                        apiKey: apiKey
                    }
                }]
            },
            Query2: {
                getKey: (pr) => {
                    if (DEBUG && false)
                        return keyGenerator(base_api.Products.query, {})
                    return keyGenerator(base_api.Products.query, cp.Query.V2.queryConvertor(pr))
                },
                fetcher: async (api) => {
                    if (DEBUG && true)
                        await sleep(2000)
                    return await axios.get(api).then(res => {
                        return {
                            data: res.data.data,
                            pagination: res.data.pagination
                        }
                    })
                }
            },
            getFilters: (pr) => {
                const api = base_api.Products.filterDetail;
                // if (DEBUG)
                //     params.b = "گلکده"
                const apiKey = keyGenerator(api + "-v2", pr)


                const params = cp.Query.V2.queryConvertor(pr)

                const req = async () => {
                    if (_.isEmpty(params))
                        return {}
                    return await axios.get(api, {
                        params
                    }).then(res => {
                        return {
                            status: res.status,
                            ...res.data,
                        }
                    })
                }

                return [apiKey, req];
            },
        }
    },
    SpecialOffer: {
        get: ({...params} = {}) => {
            const api = base_api.Main.Special.specialOffer;
            const req = async ({headers = {}}) => {
                return await axios.get(api, {
                    headers: headers,
                    params: {
                        ...params
                    }
                }).then(res => {
                    const specialOffer = [];
                    _.forEach(res.data.special_offer, (s) => {
                        let val = {
                            id: s.id,
                            name: s.name,
                            labelName: s.label_name,
                            productName: s.product_name,
                            permalink: s.permalink,
                            thumbnail: s.media,
                            url: s.url,
                        };

                        if (s.default_storage) {
                            val = {
                                ...val,
                                ...convertStorage(s.default_storage)
                            }
                        }
                        specialOffer.push(val)
                    });
                    return {
                        specialOffer: specialOffer
                    }
                });
            };
            return [api, req];
        },
    },
    SpecialProducts: {
        get: ({boxes_id = null, ...params} = {}) => {
            const api = base_api.Main.Special.specialProduct;
            const req = async ({headers = {}}) => {

                return await axios.get(api, {
                    headers: headers,
                    params: {
                        boxes_id: boxes_id,
                        ...params
                    }
                }).then(res => {
                    const specialProduct = [];
                    _.forEach(res.data.special_product, (s) => {
                        let val = {
                            id: s.id,
                            name: s.name,
                            labelName: s.label_name,
                            productName: s.product_name,
                            permalink: s.permalink,
                            thumbnail: s.thumbnail,
                            url: s.url,
                        };

                        if (s.default_storage) {
                            val = {
                                ...val,
                                ...convertStorage(s.default_storage)
                            }
                        }
                        specialProduct.push(val)
                    });
                    return {
                        specialProduct: specialProduct
                    }
                });
            };
            return [api, req];
        },
        getAll: ({...params} = {}) => {
            const api = base_api.Main.Special.allSpecialProduct;
            const req = async ({headers = {}}) => {
                return await axios.get(api, {
                    headers: headers,
                    params: {
                        ...params
                    }
                }).then(res => {
                    let newRes = {
                        products: []
                    };
                    _.forEach(res.data.products, (pr) => {
                        newRes.products.push({
                                id: pr.id,
                                name: pr.name,
                                key: pr.permalink,
                                permalink: pr.permalink,
                                specialProduct: convertProducts(pr.special_products)
                            }
                        );
                    });
                    return {
                        products: newRes.products
                    }
                })
            };
            return [api, req];
        }
    },
    Product: {
        get: ({headers, permalink, storageId, includeStorages, ...params} = {}) => {
            const api = base_api.Products.get;
            const apiKey = api + permalink + `-${storageId}`
            const req = async () => {

                return await axios.get(api + `/${encodeURIComponent(permalink)}`, {
                    headers: headers,
                    params: {
                        include_storages: includeStorages,
                        s: storageId,
                        ...params,
                        ...apiHelper.getCacheParam({networkFirst: true})
                    }
                }).then((res) => {
                    const features = tryIt(() => {
                        const features = {
                            features: res.data.features.features || [],
                            group_features: []
                        }
                        _.forEach(res.data.features.group_features, f => {
                            if (f.settings && f.settings.show_title) {
                                features.group_features.push(f)
                                return
                            }
                            features.features = [...features.features, ...f.features]
                        })
                        return features
                    }, res.data.features)
                    return {
                        ...res.data,
                        features: features,
                        product: convertProduct(res.data.product),
                        purchased: res.data.purchased,
                    }
                }).catch(e => {
                    return e
                });
            };
            return [apiKey, req];
        },
        getStorage: ({permalink, storageId, selectedFeatures = [], ...params} = {}) => {
            // if (permalink === undefined)
            //     return
            const api = base_api.Products.features;
            const req = async () => {
                return await axios.get(api + `/${encodeURIComponent(permalink)}`, {
                    params: {
                        select: _.isArray(selectedFeatures) ? selectedFeatures : [],
                        storage_id: _.isEmpty(selectedFeatures) ? storageId : undefined,
                        ...params,
                        // ...apiHelper.getCacheParam({networkFirst: true}),
                    },
                    ...defaultHeader.noCache
                }).then((res) => {
                    return res.data
                });
            };
            let key = api + `-${permalink}`;
            _.forEach(selectedFeatures, (f) => {
                key = key + `-${f}`
            })
            return [key, req];
        },
        getId: async ({permalink}) => {
            return await axios.get(rout.Product.permalink_id + `/${encodeURIComponent(permalink)}`)
        },
        related: ({...params} = {}) => {
            const api = base_api.Products.relatedProducts;

            const request = {
                url: api,
                params: {
                    ...params
                }
            };
            const r = async ({permalink, page = 1, step = relatedProductsDefaultStep, ...rParams}) => await axios({
                url: api + `/${permalink}`,
                params: {
                    ...paginationParams(page, step),
                    ...params,
                    ...rParams
                }
            } || {}, {}).then(res => {
                return {
                    products: convertProducts(res.data.data),
                    pagination: convertPagination(res.data.pagination)
                }
            });
            return (
                [request && JSON.stringify(request), r]
            )
        },
        V2: {
            get: ({productPreview, permalink, initialData,...params}) => {
                const api = (DEBUG || !productPreview) ? base_api.Products.get : base_api.Products.getForAdmin;
                const req = async () => {
                    if (false && DEBUG) {
                        return new Promise(function (resoleve, reject) {
                            const dd = {"product": {"shortlink": "https://mhrt.ir/p/2126", "category": {"parent": null, "permalink": "sport-entertainment", "name": "\u062a\u0641\u0631\u06cc\u062d\u06cc \u0648 \u0648\u0631\u0632\u0634\u06cc", "priority": 0, "type": "service", "id": 391}, "name": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc", "available": false, "address": "\u063a\u0627\u0632\u06cc\u0627\u0646 - \u06a9\u0648\u0686\u0647 \u062e\u0648\u0634 \u067e\u06cc\u06a9", "tags": [], "type": "service", "media": [{"category": 390, "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-10-11/media/09-47-19-76-has-ph.jpg", "title": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc", "type": "media", "id": 11309}, {"category": 390, "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-10-11/media/09-46-33-97-has-ph.jpg", "title": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc \u0646\u0645\u0627\u06cc \u0627\u0633\u062a\u062e\u0631", "type": "media", "id": 11307}, {"category": 390, "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-10-11/media/09-48-08-88-has-ph.jpg", "title": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc \u0646\u0645\u0627\u06cc \u062c\u06a9\u0648\u0632\u06cc \u0648 \u062d\u0648\u0636\u0686\u0647 \u0622\u0628 \u0633\u0631\u062f", "type": "media", "id": 11311}, {"category": 390, "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-10-11/media/09-47-00-26-has-ph.jpg", "title": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc \u0646\u0645\u0627\u06cc \u0627\u0633\u062a\u062e\u0631 \u0648 \u062d\u0648\u0636\u0686\u0647 \u0622\u0628 \u0633\u0631\u062f", "type": "media", "id": 11308}, {"category": 390, "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-10-11/media/09-47-29-09-has-ph.jpg", "title": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc \u0646\u0645\u0627\u06cc \u0633\u0648\u0646\u0627 \u0648 \u062d\u0645\u0627\u0645", "type": "media", "id": 11310}], "brand": null, "id": 2126, "properties": null, "rate": 0, "location": null, "categories": [{"parent": {"parent": null, "permalink": "sport-entertainment", "name": "\u062a\u0641\u0631\u06cc\u062d\u06cc \u0648 \u0648\u0631\u0632\u0634\u06cc", "priority": 0, "type": "service", "id": 391}, "permalink": "\u0627\u0633\u062a\u062e\u0631", "name": "\u0627\u0633\u062a\u062e\u0631", "priority": 0, "type": "product", "id": 379}], "permalink": "\u0627\u0633\u062a\u062e\u0631-\u0645\u0627\u0631\u0627\u0644-\u0648-\u0645\u0627\u0647\u0627\u0646-\u0627\u0646\u0632\u0644\u06cc", "thumbnail": {"category": 390, "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-10-11/thumbnail/09-50-13-66-has-ph.jpg", "title": "\u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc", "type": "thumbnail", "id": 11312}, "states": [], "cities": [{"state": 25, "id": 364, "name": "\u0628\u0646\u062f\u0631\u0627\u0646\u0632\u0644\u064a"}], "disable": false, "details": {"days": "\u0628\u0627 \u0647\u0645\u0627\u0647\u0646\u06af\u06cc", "hours": "\u0622\u0642\u0627\u064a\u0627\u0646 \u0647\u0645\u0647 \u0631\u0648\u0632\u0647 \u0628\u0647 \u062c\u0632 ( \u062f\u0648\u0634\u0646\u0628\u0647 \u0648 \u062c\u0645\u0639\u0647 ) \u0627\u0632 15 \u062a\u0627 23:30 - \u062c\u0645\u0639\u0647 \u0647\u0627 \u0627\u0632 20 \u062a\u0627 23:30\n\u0628\u0627\u0646\u0648\u0627\u0646 \u0631\u0648\u0632\u0647\u0627\u06cc \u064a\u06a9\u0634\u0646\u0628\u0647, \u0633\u0647 \u0634\u0646\u0628\u0647, \u0686\u0647\u0627\u0631\u0634\u0646\u0628\u0647 \u0627\u0632 9 \u062a\u0627 13 - \u062f\u0648\u0634\u0646\u0628\u0647 \u0648 \u062c\u0645\u0639\u0647 \u0627\u0632 9 \u062a\u0627 19\n", "phone": ["01344424747"]}, "short_description": "\u062e\u0631\u06cc\u062f \u0627\u06cc\u0646\u062a\u0631\u0646\u062a\u06cc \u0628\u0644\u06cc\u0637 \u0627\u0633\u062a\u062e\u0631 \u0645\u0627\u0631\u0627\u0644 \u0648 \u0645\u0627\u0647\u0627\u0646 \u0627\u0646\u0632\u0644\u06cc \u0628\u0627 \u062a\u062e\u0641\u06cc\u0641 \u0648 \u0635\u062f\u0648\u0631 \u0641\u0648\u0631\u06cc \u0628\u0644\u06cc\u0637 \u0628\u0647 \u0635\u0648\u0631\u062a \u0627\u0644\u06a9\u062a\u0631\u0648\u0646\u06cc\u06a9\u06cc \u0648 \u067e\u06cc\u0627\u0645\u06a9\u06cc \u0628\u062f\u0648\u0646 \u0646\u06cc\u0627\u0632 \u0628\u0647 \u0686\u0627\u067e \u0627\u0632 \u0641\u0631\u0648\u0634\u06af\u0627\u0647 \u0622\u0646\u0644\u0627\u06cc\u0646 \u0645\u0647\u0631\u062a\u062e\u0641\u06cc\u0641", "colors": [], "house": null, "description": {"items": []}, "review_count": 0, "default_storage": null, "short_address": "\u0627\u0646\u0632\u0644\u06cc"}, "purchased": false, "features": {"group_features": [{"name": "\u0627\u0645\u06a9\u0627\u0646\u0627\u062a", "priority": 0, "settings": {"show_title": true}, "id": 54, "features": [{"feature_settings": {"ui": {"hideName": true, "showGrid": true}, "hideName": true, "showGrid": true}, "feature_groups": [54], "feature_value": [{"priority": 3, "settings": {}, "id": 2203, "name": "\u0633\u0648\u0646\u0627\u06cc \u0628\u062e\u0627\u0631"}, {"priority": 5, "settings": {}, "id": 2205, "name": "\u0633\u0648\u0646\u0627\u06cc \u062e\u0634\u06a9"}, {"priority": 7, "settings": {}, "id": 2207, "name": "\u062d\u0648\u0636\u0686\u0647 \u0622\u0628 \u0633\u0631\u062f"}, {"priority": 11, "settings": {}, "id": 2211, "name": "\u062c\u06a9\u0648\u0632\u06cc"}, {"priority": 10, "settings": {}, "id": 2210, "name": "\u0627\u0633\u062a\u062e\u0631 \u06a9\u0648\u062f\u06a9 \u0646\u062f\u0627\u0631\u062f"}, {"priority": 13, "settings": {}, "id": 2213, "name": "\u0645\u0627\u0633\u0627\u0698\u0648\u0631"}, {"priority": 20, "settings": {}, "id": 2221, "name": "\u067e\u0627\u0631\u06a9\u06cc\u0646\u06af \u0646\u062f\u0627\u0631\u062f"}, {"priority": 23, "settings": {}, "id": 2222, "name": "\u0628\u0648\u0641\u0647"}, {"priority": 22, "settings": {}, "id": 2752, "name": "\u062c\u0627\u06cc \u067e\u0627\u0631\u06a9 \u0622\u0633\u0627\u0646"}, {"priority": 25, "settings": {}, "id": 2224, "name": "\u06a9\u0645\u062f \u0644\u0628\u0627\u0633"}, {"priority": 46, "settings": {}, "id": 2247, "name": "\u0641\u0631\u0648\u0634\u06af\u0627\u0647 \u067e\u0648\u0634\u0627\u06a9"}, {"priority": 34, "settings": {}, "id": 2233, "name": "\u06a9\u0627\u0631\u062a \u062e\u0648\u0627\u0646"}], "priority": 0, "id": 31364, "feature": "\u0627\u0645\u06a9\u0627\u0646\u0627\u062a \u0627\u0633\u062a\u062e\u0631"}]}, {"name": "\u0645\u0634\u062e\u0635\u0627\u062a", "priority": 2, "settings": {"show_title": true}, "id": 47, "features": [{"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2848, "name": "7*15/5 \u0645\u062a\u0631"}], "priority": 13, "id": 31377, "feature": "\u0627\u0628\u0639\u0627\u062f \u0627\u0633\u062a\u062e\u0631"}, {"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2849, "name": "1/20 \u062a\u0627 2/20 \u0645\u062a\u0631"}], "priority": 14, "id": 31378, "feature": "\u0639\u0645\u0642 \u0627\u0633\u062a\u062e\u0631"}, {"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2850, "name": "14 \u0645\u062a\u0631 \u0645\u0631\u0628\u0639"}], "priority": 15, "id": 31379, "feature": "\u0627\u0628\u0639\u0627\u062f \u0633\u0648\u0646\u0627 \u062e\u0634\u06a9"}, {"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2851, "name": "12 \u0645\u062a\u0631 \u0645\u0631\u0628\u0639"}], "priority": 16, "id": 31380, "feature": "\u0627\u0628\u0639\u0627\u062f \u0633\u0648\u0646\u0627 \u0628\u062e\u0627\u0631"}, {"feature_settings": {"ui": {}}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2852, "name": "5 \u0645\u062a\u0631 \u0645\u0631\u0628\u0639"}], "priority": 17, "id": 31381, "feature": "\u0627\u0628\u0639\u0627\u062f \u062c\u06a9\u0648\u0632\u06cc"}, {"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2853, "name": "3 \u0639\u062f\u062f"}], "priority": 18, "id": 31382, "feature": "\u062a\u0639\u062f\u0627\u062f \u062f\u0648\u0634"}, {"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2854, "name": "50 \u0639\u062f\u062f"}], "priority": 19, "id": 31383, "feature": "\u062a\u0639\u062f\u0627\u062f \u06a9\u0645\u062f"}, {"feature_settings": {}, "feature_groups": [47], "feature_value": [{"priority": 0, "settings": {}, "id": 2855, "name": "50 \u0646\u0641\u0631"}], "priority": 20, "id": 31384, "feature": "\u0638\u0631\u0641\u06cc\u062a \u0627\u0633\u062a\u062e\u0631"}]}, {"name": "\u0634\u0631\u0627\u06cc\u0637 \u0627\u0633\u062a\u0641\u0627\u062f\u0647", "priority": 3, "settings": {"show_title": true}, "id": 48, "features": [{"feature_settings": {"ui": {"hideName": false}, "hideName": true}, "feature_groups": [49, 48, 41], "feature_value": [{"priority": 0, "settings": {}, "id": 2847, "name": "\u0632\u06cc\u06312 \u0633\u0627\u0644 \u0628\u0627 \u0647\u0631 \u062c\u0646\u0633\u06cc\u062a\u06cc \u0645\u062c\u0627\u0632 \u0646\u06cc\u0633\u062a"}], "priority": 12, "id": 31376, "feature": "\u0647\u0645\u0631\u0627\u0647 \u0622\u0648\u0631\u062f\u0646 \u0641\u0631\u0632\u0646\u062f\u0627\u0646 \u062e\u0631\u062f\u0633\u0627\u0644"}]}], "features": []}, "wish": false, "notify": false}

                            const {product, ...data} = dd
                            resoleve({
                                ...product, ...data
                            })
                            return;
                        })
                    }

                    return await axios.get(api + `/${encodeURIComponent(permalink)}`,{
                        ...params
                    }).then((res) => {
                        return tryIt(() => {
                            const {product, ...data} = res.data
                            return {
                                ...data,
                                ...product
                            }
                        }, res)
                    })
                };
                return [
                    keyGenerator(api, {productPreview, permalink: permalink}),
                    req,
                    {
                        initialData: initialData,
                        revalidateOnFocus: DEBUG,
                        dedupingInterval: 480000,
                    }
                ]
            },
            getStorage: ({permalink, storageId, selectedFeatures = [], initialData,...params}) => {
                const api = base_api.Products.features;
                const req = async () => {
                    if (false && DEBUG) {
                        return new Promise(function (resolve) {
                            const data = {
                                "features": [{
                                    "id": 40,
                                    "name": "\u0633\u0627\u06cc\u0632 \u06af\u0644",
                                    "values": [{
                                        "id": 5708,
                                        "settings": {},
                                        "name": "\u0628\u0632\u0631\u06af",
                                        "available": true,
                                        "selectable": true
                                    }, {
                                        "id": 5709,
                                        "settings": {},
                                        "name": "\u06a9\u0648\u0686\u06a9",
                                        "available": false,
                                        "selectable": false
                                    }],
                                    "selected": 5708,
                                    "settings": {}
                                }],
                                "storage": {
                                    "gender": null,
                                    "id": 1755,
                                    "priority": 0,
                                    "vip_max_count_for_sale": null,
                                    "least_booking_time": -1,
                                    "discount_price": 12000,
                                    "accessories": [],
                                    "disable": false,
                                    "discount_percent": 25,
                                    "max_count_for_sale": 1,
                                    "max_shipping_time": 0,
                                    "min_count_alert": 5,
                                    "vip_type": null,
                                    "shipping_cost": null,
                                    "booking_cost": -1,
                                    "title": "\u0627\u0686\u06cc\u0646\u0648 \u0628\u0632\u0631\u06af",
                                    "final_price": 16000,
                                    "invoice_title": "\u0627\u0686\u06cc\u0646\u0648 \u0628\u0632\u0631\u06af",
                                    "deadline": null,
                                    "default": true
                                }
                            }
                            const {storage,features} = data;
                            resolve({
                                storage,
                                features,
                                is_service: _.isArray(storage),
                            })
                        })
                    }

                    if (DEBUG)
                        await sleep(1000)

                    return await axios.get(api + `/${encodeURIComponent(permalink)}`, {
                        ...params,
                        params: {
                            select: _.isArray(selectedFeatures) ? selectedFeatures : [],
                            storage_id: _.isEmpty(selectedFeatures) ? storageId : undefined,
                        },
                        ...defaultHeader.noCache
                    }).then((res) => {
                        const {storage, features} = res.data;

                        return {
                            storage,
                            features,
                            is_service: _.isArray(storage),
                        }
                    });
                };

                return [
                    keyGenerator(api, {storageId, permalink, selectedFeatures}),
                    req,
                    {
                        initialData: initialData,
                        revalidateOnFocus: true,
                        dedupingInterval: 480000,
                    }
                ]
            },
            getStorageDedupingInterval: 480000,
        }
    },
    Comments: {
        get: ({...params} = {}) => {
            const api = base_api.Products.comments;

            const request = {
                url: api,
                params: {
                    ...params
                }
            };

            const r = async ({permalink, page = 1, step = commentsDefaultStep, ...rParams}) => await axios({
                url: api,
                params: {
                    prp: permalink,
                    ...paginationParams(page, step),
                    ...params,
                    ...rParams
                }
            } || {}, {}).then(res => {
                return {
                    comments: convertComments(res.data.data),
                    pagination: convertPagination(res.data.pagination, page)
                }
            });
            return (
                [request && JSON.stringify(request), r]
            )
        },
        getPointsAndComments: ({...params} = {}) => cp.Comments.get({type: 2, ...params}),
        getQuestionAndAnswerComments: ({...params} = {}) => cp.Comments.get({type: 1, ...params}),
        getCommentReplays: ({...params} = {}) => {
            const api = base_api.Products.comments;

            const request = {
                url: api,
                params: {
                    ...params
                }
            };

            const r = async ({comment_id, type = null, page, step = commentsDefaultStep, ...params}) => await axios({
                url: api,
                params: {
                    comment_id: comment_id,
                    type: null,
                    ...paginationParams(page, step),
                    ...params,
                }
            } || {}, {}).then(res => {
                return {
                    status: 200,
                    comments: convertComments(res.data.data),
                    pagination: convertPagination(res.data.pagination, page)
                }
            });
            return (
                [request && JSON.stringify(request), r]
            );
        },
        set: async ({
                        type,
                        comment_id,
                        productPermalink,
                        rate,
                        satisfied,
                        postParams,
                        text,
                        firstName = undefined,
                        lastName = undefined,
                        ...params
                    }) => {
            const api = base_api.Products.comments;

            const p = {
                product_permalink: productPermalink,
                reply_to_id: comment_id,
                type: type,
                text: text,
                first_name: firstName,
                last_name: lastName,
                ...postParams,
                params: {
                    ...params
                }
            };

            if (type === 2) {
                p.rate = rate;
                p.satisfied = satisfied;
            }

            return await axios.post(api, p).then(res => {
                if (res.data.user)
                    onUserUpdated(dispatch, {user: convertUser(res.data.user)});
                return res
            })
        },
        setPointsAndComments: async ({
                                         productPermalink,
                                         rate,
                                         satisfied,
                                         postParams,
                                         text,
                                         firstName = undefined,
                                         lastName = undefined,
                                         ...params
                                     }) => {
            return cp.Comments.set({
                type: 2,
                productPermalink,
                rate,
                satisfied,
                postParams,
                text,
                firstName,
                lastName, ...params
            })
        },
        setQuestionAndAnswerComments: async ({
                                                 productPermalink,
                                                 text,
                                                 firstName = undefined,
                                                 lastName = undefined,
                                                 postParams,
                                                 ...params
                                             }) => {
            return cp.Comments.set({
                type: 1,
                productPermalink,
                postParams,
                text,
                firstName,
                lastName, ...params
            })
        },
        setReplayComment: async ({
                                     comment_id,
                                     text,
                                     firstName = undefined,
                                     lastName = undefined,
                                     postParams,
                                     ...params
                                 }) => {
            return cp.Comments.set({comment_id, postParams, text, firstName, lastName, type: 1, ...params})
        }

    },
};

export default cp;

