export const productType = {
    SERVICE: 'service',
    PRODUCT: 'product'
};


export const productDescription = {
    description:{
        key:"description",
        label:"توضیحات",
    },
    usage_condition:{
        key:"usage_condition",
        label:"شرایط استفاده"
    },
    how_to_use:{
        key:"how_to_use",
        label:"طریقه مصرف"
    },
    keeping_maintenance:{
        key:"keeping_maintenance",
        label:"شرایط نگهداری"
    },
    regulation:{
        key:"regulation",
        label:"مقررات"
    },
}


export const bookingType ={
    datetime:{
        key:'datetime',
        label:"تاریخ و ساعت"
    }
}
