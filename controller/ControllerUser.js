import _ from "lodash";
import React from "react";
import {lang} from "../repository";
import JDate from "../utils/JDate";
import {
    convertAddress,
    convertAddresses,
    convertFeatures,
    convertInvoice,
    convertInvoices,
    convertInvoiceStatus,
    convertPagination,
    convertProduct,
    convertProducts,
    convertUser,
    gender,
    generateSummary,
    keyGenerator,
    paginationParams,
} from "./converter";
import axios from 'axios';
import base_api, {apiHelper} from './api';
import Actions from "../Actions";
import Utils, {UtilsDate} from "../utils/Utils";
import {bookingType} from "./type";
import {AuthStatus} from "../pages/login";
import {getSafe, gLog, sleep, tryIt} from "material-ui-helper";
import {defaultHeader} from "../component/baseSite/axios/AxoisSetup";
import {DEBUG, DEBUG_AUTH} from "../pages/_app";

export const wishlistDefaultStep = 6;
export const ordersDefaultStep = 7;


export const testUser = {
    username: "09377467414",
    newUsername: "09377467415",
    password: "123456",
    passwordNewUser: "",
    code: "12345",
    isAdmin: true
}


const LOGIN_DEBUG = false

const cu = {
    Auth: {
        firstStep: async ({username, postParams, ...params}) => {
            if (LOGIN_DEBUG) {
                await sleep(2000)
                if (username === testUser.username) {
                    return {
                        data: {
                            status: AuthStatus.REGISTERED_USER
                        }
                    }
                }
                if (username === testUser.newUsername) {
                    return {
                        data: {
                            status: AuthStatus.GUEST_USER
                        }
                    }
                }
                throw ""
            }
            return await axios.post(base_api.User.Auth.login, {
                username: username,
                ...postParams,
                params: {
                    ...params
                }
            })
        },
        login: async ({username, password, code, postParams, ...params}) => {
            if (LOGIN_DEBUG) {
                await sleep(2000)
                let u = {}
                if (username === testUser.username) {
                    u = {
                        username: testUser.username,
                        password: testUser.password,
                        code: testUser.code
                    }
                } else if (username === testUser.newUsername) {
                    u = {
                        username: testUser.newUsername,
                        password: testUser.passwordNewUser,
                        code: testUser.code
                    }
                } else {
                }


                if ((username === u.username) && ((code && code === u.code) || (password && password === u.password))) {
                    if (u.password === "") {
                        return {
                            data: {
                                status: AuthStatus.SET_PASSWORD_REQUIRED
                            }
                        }
                    }
                    if (username === testUser.username && testUser.isAdmin) {
                        return {
                            data: {
                                status: AuthStatus.OTP_REQUIRED
                            }
                        }
                    }
                    return {}
                }
                throw ""
            }
            return await axios.post(base_api.User.Auth.login, {
                username: username,
                password: password,
                code: code,
                ...postParams,
                params: params
            })
        },
        sendCode: async ({username}) => {
            if (LOGIN_DEBUG) {
                await sleep(2000)
                gLog("Auth => resendCode => code sent")
                return {
                    data: {
                        resend_timeout: _.toInteger((Date.now() + 120000) / 1000),
                        activation_expire: _.toInteger((Date.now() + 120000) / 1000)
                    }
                }
            }
            return await axios.post(base_api.User.Auth.sendCode, {
                username: username
            })
        },
        setPassword: async ({username, password, ...params}) => {
            if (LOGIN_DEBUG) {
                if (username === testUser.username) {
                    testUser.password = password
                    return {}
                }
                testUser.passwordNewUser = password
                return {}
            }
            return await axios.put(base_api.User.Auth.setPassword, {
                password,
                params: {
                    ...params
                }
            })
        }
    },
    User: {
        login: async ({username, password, postParams = {}, ...params}) => {
            return await axios.post(base_api.User.Auth.login, {
                username: username,
                password: password,
                ...postParams,
                params: {
                    ...params
                }
            }).then((res) => {
                if (res.status === 200)
                    return {
                        status: res.status,
                        data: {
                            user: convertUser(res.data.user),
                            newBasketCount: res.data.basket_count,
                            basket_count: res.data.basket_count
                        }
                    }
                return res
            })
        },
        verifyPhone: async (dispatch, {code, newPassword, directTo, save = true, postParams = {}, ...params}) => {
            return await axios.post(base_api.User.Auth.verifyMobile, {
                code: code,
                new_password: newPassword,
                ...postParams,
                params: {
                    ...params
                }
            }).then((res) => {
                if (res.status === 200)
                    return {
                        status: res.status,
                        data: {
                            user: convertUser(res.data.user),
                            newBasketCount: res.data.basket_count,
                            basket_count: res.data.basket_count
                        }
                    }
                return res
            })
        },
        resetPassword: async (dispatch, {code, directTo, save = true, postParams = {}, ...params}) => {
            return await axios.post(base_api.User.Auth.verifyMobile, {
                code: code,
                ...postParams,
                params: {
                    ...params
                }
            }).then((res) =>
                loginSuccessData(dispatch, {
                    res: res,
                    directTo: directTo,
                    save: save
                }))
        },
        resendVerifyPhoneCode: async ({...params} = {}) => {
            return await axios.post(base_api.User.Auth.sendCode, {
                params: {
                    ...params
                }
            })
        },
        setPassword: async ({password, ...params}) => {
            return await axios.put(base_api.User.Auth.setPassword, {
                password,
                params: {
                    ...params
                }
            }).then(res => {
                if (res.status === 200)
                    return {
                        status: res.status,
                        user: convertUser(res.data.user),
                        newBasketCount: res.data.basket_count
                    }
                return res;
            })
        },
        logout: async ({postParams = {}, ...params} = {}) => {
            axios.post(base_api.User.Auth.logout, {
                ...postParams,
                params: {
                    ...params
                }
            }).then().catch().then(() => {
            })
        },
        add: (dispatch, {user}) => {
            // dispatch(update(user))
        },
        Profile: {
            V2: {
                get: () => {
                    const apiKey = base_api.User.Profile.main;
                    const req = async () => {
                        if (DEBUG_AUTH&&false) {
                            return new Promise(function (resoleve, reject) {
                                const res = {
                                    data:{
                                        "user": {
                                            "first_name": "\u0645\u0647\u062f\u06cc",
                                            "default_address": {
                                                "phone": "09377467414",
                                                "id": 43,
                                                "address": "\u0631\u0634\u062a\u060c\u0645\u06cc\u062f\u0627\u0646 \u0642\u0644\u06cc\u067e\u0648\u0631\u060c \u0628\u0639\u062f \u0627\u0632 \u0628\u06cc\u0645\u0627\u0631\u0633\u062a\u0627\u0646 \u067e\u0627\u0631\u0633\u060c \u06a9 \u067e\u0627\u0631\u0633 \u06cc\u06a9\u060c \u062f\u0631\u0641\u06a9 1\u060c \u062f\u0631\u0641\u06a9 3\u060c \u062c\u0646\u0628 \u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u067e\u062f\u0631\u060c \u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u0622\u0631\u06cc\u0646\u060c \u0637 2",
                                                "location": {"lat": 37.29787918478283, "lng": 49.5749315790919},
                                                "name": "\u0645\u0647\u062f\u06cc \u0645\u0631\u0627\u062f\u06cc",
                                                "postal_code": "4158814355",
                                                "state": {"name": "\u06af\u06cc\u0644\u0627\u0646", "id": 25},
                                                "city": {"name": "\u0631\u0634\u062a", "state": 25, "id": 366}
                                            },
                                            "last_name": "\u0645\u0631\u0627\u062f\u06cc",
                                            "email": "food@mehrtakhfif.com",
                                            "id": 10,
                                            "meli_code": "2580839364",
                                            "shaba": "IR840570150180011692681102",
                                            "vip_type": null,
                                            "username": "09377467414",
                                            "name": "\u0645\u0647\u062f\u06cc \u0645\u0631\u0627\u062f\u06cc",
                                            "gender": true,
                                            "birthday": 810172800,
                                            "is_staff": true,
                                            "roll": "admin"
                                        }
                                    }
                                };
                                const u = res.data.user
                                res.data.user = {
                                    ...u,
                                    isActiveData: (u.first_name && u.last_name && u.meli_code && u.username),
                                    gender: gender(u?.gender),
                                    jBirthday: getSafe(() => u.birthday ? JDate.timeStampToJalali(u.birthday).format('jYYYY/jM/jD') : undefined),
                                }
                                resoleve(res)
                            })
                        }


                        return await axios.get(base_api.User.Profile.main).then(res => {
                            const u = res.data.user
                            res.data.user = {
                                ...u,
                                isActiveData: (u.first_name && u.last_name && u.meli_code && u.username),
                                gender: gender(u?.gender),
                                jBirthday: getSafe(() => u.birthday ? JDate.timeStampToJalali(u.birthday).format('jYYYY/jM/jD') : undefined),
                            }
                            return res
                        })
                    }
                    return [apiKey, req, {
                        refreshInterval: UtilsDate.minuteToMillisecond(2),
                        dedupingInterval: 24000,
                    }]
                }
            },
            get2: async (dispatch, {save = true, ...params} = {}) => {
                return await axios.get(base_api.User.Profile.main, {
                    params: {
                        ...params,
                        ...apiHelper.getCacheParam({networkFirst: true})
                    }
                }).then((res) => {
                    const data = {
                        user: convertUser(res.data.user)
                    };
                    const dispatcher = () => {
                        // dispatch(update(data.user));
                    };
                    return {
                        ...data,
                        dispatcher: dispatcher
                    }
                })
            },
            get: (dispatcher, {save = true, ...params} = {save: true}) => {
                const api = base_api.User.Profile.main;
                const isLogin = Actions.User.isLogin()
                const apiKey = api + `isLogin-${isLogin ? "true" : "false"}`;
                const req = async () => {
                    if (DEBUG_AUTH) {
                        return new Promise(function (resoleve, reject) {
                            const res = {
                                data:{
                                    "user": {
                                        "first_name": "\u0645\u0647\u062f\u06cc",
                                        "default_address": {
                                            "phone": "09377467414",
                                            "id": 43,
                                            "address": "\u0631\u0634\u062a\u060c\u0645\u06cc\u062f\u0627\u0646 \u0642\u0644\u06cc\u067e\u0648\u0631\u060c \u0628\u0639\u062f \u0627\u0632 \u0628\u06cc\u0645\u0627\u0631\u0633\u062a\u0627\u0646 \u067e\u0627\u0631\u0633\u060c \u06a9 \u067e\u0627\u0631\u0633 \u06cc\u06a9\u060c \u062f\u0631\u0641\u06a9 1\u060c \u062f\u0631\u0641\u06a9 3\u060c \u062c\u0646\u0628 \u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u067e\u062f\u0631\u060c \u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u0622\u0631\u06cc\u0646\u060c \u0637 2",
                                            "location": {"lat": 37.29787918478283, "lng": 49.5749315790919},
                                            "name": "\u0645\u0647\u062f\u06cc \u0645\u0631\u0627\u062f\u06cc",
                                            "postal_code": "4158814355",
                                            "state": {"name": "\u06af\u06cc\u0644\u0627\u0646", "id": 25},
                                            "city": {"name": "\u0631\u0634\u062a", "state": 25, "id": 366}
                                        },
                                        "last_name": "\u0645\u0631\u0627\u062f\u06cc",
                                        "email": "food@mehrtakhfif.com",
                                        "id": 10,
                                        "meli_code": "2580839364",
                                        "shaba": "IR840570150180011692681102",
                                        "vip_type": null,
                                        "username": "09377467414",
                                        "name": "\u0645\u0647\u062f\u06cc \u0645\u0631\u0627\u062f\u06cc",
                                        "gender": true,
                                        "birthday": 810172800,
                                        "is_staff": true,
                                        "roll": "admin"
                                    }
                                }
                            };
                            const data = {
                                user: convertUser(res.data.user)
                            };
                            resoleve({
                                ...data,
                            })
                        })
                    }


                    if (!isLogin)
                        return {}
                    return await axios.get(base_api.User.Profile.main, {
                        params: {
                            ...params,
                            ...apiHelper.getCacheParam({networkFirst: true})
                        },
                        ...defaultHeader.noCache
                    }).then((res) => {
                        const data = {
                            user: convertUser(res.data.user)
                        };
                        if (dispatcher && save) {
                            dispatcher(data.user)
                        }
                        return {
                            ...data,
                        }
                    })
                };
                return [apiKey, req];
            },
            uploadAvatar: async ({formData, save = true, ...params}) => {
                const config = {
                    headers: {'content-type': 'multipart/form-data'},
                    params: {
                        ...params
                    }
                };
                return await axios.post(base_api.User.Profile.avatar, formData, config).then((res) => {
                    const data = {
                        avatarUrl: res.data.url
                    };
                    // const dispatcher = () => {
                    // dispatch(updateAvatar(data.avatarUrl));
                    // };
                    // if (dispatch && save) {
                    //     dispatcher();
                    // }
                    return {
                        status: 200,
                        data: {
                            ...data,
                        }
                    }
                })
            },
            removeAvatar: async ({save = true, ...params} = {save: true}) => {
                return await axios.delete(base_api.User.Profile.avatar, {
                    params: {
                        ...params
                    }
                }).then((res) => {
                    const dispatcher = () => {
                        // dispatch(updateAvatar(""));
                    };
                    // if (dispatch && save) {
                    //     dispatcher();
                    // }
                    return {
                        status: res.status,
                        data: {
                            dispatcher: dispatcher
                        }
                    }
                })
            },
            update: async ({
                               firstName = null,
                               lastName = null,
                               email = null,
                               meliCode = null,
                               gender = null,
                               birthday = null,
                               shaba = null,
                               subscribe = 1,
                               save = true,
                               postParams = {},
                               ...params
                           }) => {
                return await axios.put(base_api.User.Profile.main, {
                        first_name: firstName,
                        last_name: lastName,
                        email: email,
                        meli_code: meliCode,
                        shaba: shaba,
                        birthday: birthday,
                        gender: gender,
                        subscribe: subscribe,
                        ...postParams,
                    },
                    {
                        params: {
                            ...params
                        }
                    }).then((res) => {
                    const user = convertUser(res.data.user);

                    return {
                        status: 200,
                        user: user,
                    }
                }).catch(() => {
                    return {
                        status: 500,
                    }
                })
            },
            Wishlist: {
                V2: {
                    Get: {
                        getKey: () => {
                            if (DEBUG && false)
                                return keyGenerator(base_api.User.Profile.wishlist, {})
                            return keyGenerator(base_api.User.Profile.wishlist)
                        },
                        fetcher: async (api) => {
                            if (DEBUG && true)
                                await sleep(2000)

                            return await axios.get(api).then(res => {
                                const data = [];
                                _.forEach(res.data.data, (item, index) => {
                                    data.push({
                                        id: item.id,
                                        type: item.type,
                                        product: convertProduct(item.product),
                                        notify: item.notify,
                                    })
                                })
                                return {
                                    status: 200,
                                    data: data,
                                    pagination: res.data.pagination
                                }
                            })
                        }
                    },
                },
                get: ({...params} = {}) => {
                    const api = base_api.User.Profile.wishlist;
                    const request = {
                        url: api,
                        params: {
                            ...params
                        }
                    };

                    const r = async ({page, step = wishlistDefaultStep, ...rParams}) => await axios({
                        url: api,
                        params: {
                            ...paginationParams(page, step),
                            ...params,
                            ...rParams
                        },
                        ...defaultHeader.noCache
                    } || {}, {}).then(res => {
                        const data = [];
                        _.forEach(res.data.data, (item, index) => {
                            data.push({
                                id: item.id,
                                type: item.type,
                                product: convertProduct(item.product),
                                notify: item.notify,
                            })
                        })
                        return {
                            status: 200,
                            data: data,
                            pagination: convertPagination(res.data.pagination)
                        }
                    });
                    return (
                        [request && JSON.stringify(request), r]
                    );
                },
                update: async ({productId, wish, notify, ...params} = {}) => {
                    return await axios.post(base_api.User.Profile.wishlist, {
                        product_id: productId,
                        wish: wish,
                        notify: notify,
                    })
                },
                remove: async ({wishlist_id, ...params} = {}) => {
                    return await axios.delete(base_api.User.Profile.wishlist, {
                        params: {
                            wishlist_id: wishlist_id,
                            ...params
                        }
                    })
                },

            },
            Comment: {
                V2: {
                    Get: {
                        getKey: () => {
                            return keyGenerator(base_api.User.Profile.comments)
                        },
                        fetcher: async (api) => {
                            if (DEBUG && true)
                                await sleep(2000)

                            return await axios.get(api).then(res => {

                                const data = [];
                                _.forEach(res.data.data, (item, index) => {
                                    data.push({
                                        id: item.id,
                                        approved: item.approved,
                                        approvedLabel: lang.get(item.approved === null ? 'waiting_for_accept' : item.approved ? 'accepted' : 'not_accepted'),
                                        rate: item.rate,
                                        type: item.type,
                                        replyTo: item.reply_to,
                                        text: item.text,
                                        product: convertProduct(item.product),
                                    })
                                });
                                return {
                                    status: 200,
                                    data: data,
                                    pagination: res.data.pagination
                                }
                            })
                        }
                    },
                },
                get: ({...params} = {}) => {
                    const api = base_api.User.Profile.comments;
                    const request = {
                        url: api,
                        params: {
                            ...params
                        }
                    };

                    const r = async ({page, step = wishlistDefaultStep, ...rParams}) => await axios({
                        url: api,
                        params: {
                            ...paginationParams(page, step),
                            ...params,
                            ...rParams
                        },
                        ...defaultHeader.noCache
                    } || {}, {}).then(res => {
                        const data = [];
                        _.forEach(res.data.data, (item, index) => {
                            data.push({
                                id: item.id,
                                approved: item.approved,
                                approvedLabel: lang.get(item.approved === null ? 'waiting_for_accept' : item.approved ? 'accepted' : 'not_accepted'),
                                rate: item.rate,
                                type: item.type,
                                replyTo: item.reply_to,
                                text: item.text,
                                product: convertProduct(item.product),
                            })
                        });
                        return {
                            status: 200,
                            data: data,
                            pagination: convertPagination(res.data.pagination)
                        }
                    });
                    return (
                        [request && JSON.stringify(request), r]
                    );
                },
                remove: async ({comment_id, ...params}) => {
                    return await axios.delete(base_api.User.Profile.comments, {
                        params: {
                            comment_id: comment_id,
                            ...params
                        }
                    })
                }
            },
            Orders: {
                V2: {
                    Get: {
                        getKey: () => {
                            if (DEBUG && false)
                                return keyGenerator(base_api.User.Profile.orders, {})
                            return keyGenerator(base_api.User.Profile.orders)
                        },
                        fetcher: async (api) => {
                            if (DEBUG && true)
                                await sleep(2000)

                            return await axios.get(api).then(res => {

                                const data = [];
                                _.forEach(res.data.data, order => {
                                    const p = {
                                        ...convertInvoiceStatus(order.status)
                                    };

                                    data.push({
                                        id: order.id,
                                        order_id: "MT-" + order.id,
                                        amount: order.amount,
                                        finalPrice: order.final_price,
                                        createdAt: order.created_at,
                                        jalaliCreatedAt: JDate.timeStampToJalali(order.created_at),
                                        statusText: order.status,
                                        status: order.status,
                                        ...p
                                    })
                                });

                                return {
                                    status: res.status,
                                    data: data,
                                    pagination: res.data.pagination
                                }
                            })
                        }
                    },
                },
                get: ({...params} = {}) => {
                    const api = base_api.User.Profile.orders;

                    const request = {
                        url: api,
                        params: {
                            ...params
                        }
                    };

                    const r = async ({page = 1, step = 5, ...rParams}) => await axios({
                        url: api,
                        params: {
                            ...paginationParams(page, step),
                            ...params,
                            ...rParams
                        },
                        ...defaultHeader.noCache
                    } || {}, {}).then(res => {
                        const data = [];
                        _.forEach(res.data.data, order => {
                            const p = {
                                ...convertInvoiceStatus(order.status)
                            };

                            data.push({
                                id: order.id,
                                order_id: "MT-" + order.id,
                                amount: order.amount,
                                finalPrice: order.final_price,
                                createdAt: order.created_at,
                                jalaliCreatedAt: JDate.timeStampToJalali(order.created_at),
                                statusText: order.status,
                                status: order.status,
                                ...p
                            })
                        });
                        return {
                            status: res.status,
                            data: data,
                            pagination: convertPagination(res.data.pagination, page)
                        }
                    });
                    return (
                        [request && JSON.stringify(request), r]
                    )
                },
                Invoice: {
                    get: async ({invoice_id, ...params}) => {
                        return await axios.get(base_api.User.Profile.orders, {
                            params: {
                                id: invoice_id,
                                ...params
                            },
                            ...defaultHeader.noCache
                        }).then((res => {
                            // if (DEBUG)
                            //     res = {data:{"data": {"details": {}, "invoice_discount": 18000, "address": null, "storages": [{"details": null, "id": 451, "discount_file": "https://mhrt.ir/E3QkSB", "count": 1, "total_price": 20000, "discount_price_without_tax": 2000, "unit_price": 2000, "invoice_id": 513, "amer": "\u062a\u0641\u0631\u06cc\u062d\u06cc \u0648 \u0648\u0631\u0632\u0634\u06cc", "final_price": 20000, "features": {}, "tax": 0, "discount": 18000, "discount_price": 2000, "product": {"available": true, "disable": false, "thumbnail": {"id": 10790, "box": 18, "title": "\u0628\u0627\u0634\u06af\u0627\u0647 \u0628\u062f\u0646\u0633\u0627\u0632\u06cc \u0637\u0627\u0647\u0627", "image": "https://api.mehrtakhfif.com/media/boxes/18/2021-06-01/thumbnail/15-59-43-29-has-ph.jpg", "type": "thumbnail"}, "id": 2032, "permalink": "\u0628\u0627\u0634\u06af\u0627\u0647-\u0628\u062f\u0646\u0633\u0627\u0632\u06cc-\u0637\u0627\u0647\u0627", "name": "\u0628\u0627\u0634\u06af\u0627\u0647 \u0628\u062f\u0646\u0633\u0627\u0632\u06cc \u0637\u0627\u0647\u0627", "colors": [], "type": "service", "invoice_description": ""}, "purchase_date": 1625919815.461814, "storage": {"id": 2699, "invoice_title": "undefined", "title": "\u0628\u0627\u0634\u06af\u0627\u0647 \u0628\u062f\u0646\u0633\u0627\u0632\u06cc \u063a\u0644\u0627\u0645\u06cc"}}], "start_date": null, "payment_url": null, "status": "payed", "id": 513, "payed_at": 1625919815.461814, "amount": 2000, "final_price": 20000, "invoice": "https://api.mehrtakhfif.com/invoice_detail/513", "booking_type": "unbookable", "end_date": null, "created_at": 1625919751.743699, "expire": 1625921551.74303}}}

                            const {data} = res.data;

                            const props = {
                                ...convertInvoiceStatus(data.status)
                            };
                            props.invoice = data.invoice;
                            props.amount = data.amount;
                            props.finalPrice = data.final_price;

                            const products = convertInvoices(data.storages);
                            props.productCount = products.length;
                            props.products = {
                                services: [],
                                products: []
                            };
                            _.forEach(products, (p) => {
                                if (p.product.isService) {
                                    props.products.services.push(p);
                                    return;
                                }
                                props.products.products.push(p);
                            });
                            props.address = convertAddress(data.address);

                            return {
                                ...data,
                                booking_type: bookingType[data.booking_type],
                                id: data.id,
                                status: res.status,
                                ...props
                            }
                        }))
                    },
                    getProductInvoice: ({product_id, ...params}) => {
                        const api = base_api.User.Profile.orderProduct + `/${product_id}`;
                        const req = async () => {
                            return await axios.get(api, {
                                params: {
                                    id: product_id,
                                    ...params
                                }
                            }).then((res) => {
                                return {
                                    status: res.status,
                                    data: {
                                        invoice: convertInvoice(res.data)
                                    }
                                }
                            })
                        };
                        return [api, req];
                    }
                }
            },
            Address:{
                GetAddresses: {
                    getKey: () => {
                        return keyGenerator(base_api.User.Profile.address)
                    },
                    fetcher: async (api) => {
                        return await axios.get(api).then(res => {
                            return {
                                status: 200,
                                data: res.data,
                                pagination: res.data.pagination
                            }
                        })
                    }
                },
                get:()=>{
                    const apiKey = base_api.User.Profile.address;
                    const req = async () => {


                        if (DEBUG_AUTH){
                            return new Promise(function (resoleve, reject) {
                                const res = {
                                    "address": {
                                        "location": {
                                            "lat": 37.29787918478283,
                                            "lng": 49.5749315790919
                                        },
                                        "id": 43,
                                        "phone": "09377467414",
                                        "postal_code": "4158814355",
                                        "state": {
                                            "id": 25,
                                            "name": "گیلان"
                                        },
                                        "name": "مهدی مرادی",
                                        "address": "میدان قلیپور، بعد از بیمارستان پارس، ک پارس یک، درفک 1، درفک 3، جنب ساختمان پدر، ساختمان آرین، ط 2",
                                        "city": {
                                            "id": 366,
                                            "name": "رشت",
                                            "state": 25
                                        }
                                    }
                                }
                                resoleve(res)
                            })
                        }






                        return await axios.get(apiKey,{
                            params:{
                                default:true
                            }
                        }).then(res => {
                            return res
                        })
                    }
                    return [apiKey, req, {}]
                }
            },
            Addresses: {
                get: ({all = false, page, step = 16, ...params}) => {
                    const api = base_api.User.Profile.address;
                    const req = async () => {
                        return await axios.get(api, {
                            params: {
                                all: all,
                                ...paginationParams(page, step),
                                ...params,
                                ...apiHelper.getCacheParam({networkFirst: true})
                            },
                            ...defaultHeader.noCache
                        }).then((res) => {

                            return {
                                status: res.status,
                                data: convertAddresses(res.data.addresses.data),
                                pagination: convertPagination(res.data.addresses.pagination, page),
                                defaultAddress: convertAddress(res.data.default_address)
                            }
                        })
                    };
                    return [api, req];
                },
                add: async (
                    {
                        name,
                        state_id,
                        city_id,
                        phone,
                        postalCode,
                        address,
                        location = null,
                        setDefault = false,
                        postParams = {},
                        ...params
                    }) => {
                    return await axios.post(base_api.User.Profile.address, {
                        name: name,
                        state_id: state_id,
                        city_id: city_id,
                        phone: phone,
                        postal_code: postalCode,
                        address: address,
                        location: apiHelper.convertLocation(location),
                        set_default: setDefault,
                        ...postParams,
                    }, {
                        params: {
                            ...params
                        }
                    }).then((res) => {
                        return {
                            status: res.status,
                            data: convertAddress(res.data),
                        }
                    })
                },
                update: async (
                    {
                        id,
                        name,
                        state_id,
                        city_id,
                        phone,
                        postalCode,
                        address,
                        location = null,
                        setDefault = false,
                        postParams = {},
                        ...params
                    }) => {
                    return await axios.put(base_api.User.Profile.address, {
                            id: id,
                            name: name,
                            state_id: state_id,
                            city_id: city_id,
                            phone: phone,
                            postal_code: postalCode,
                            address: address,
                            set_default: setDefault,
                            location: apiHelper.convertLocation(location),
                            ...postParams
                        },
                        {
                            params: {
                                ...params
                            }
                        }).then((res) => {
                        return {
                            status: res.status,
                            data: convertAddress(res.data),
                        }
                    })
                },
                remove: async ({address_id, ...params}) => {
                    return await axios.delete(base_api.User.Profile.address, {
                        params: {
                            id: address_id,
                            ...params
                        }
                    }).then((res) => {
                        return {
                            status: res.status,
                            data: res.data.data,
                        }
                    })
                },
                setDefault: async ({address_id, ...params}) => {
                    return await axios.patch(base_api.User.Profile.address, {
                        id: address_id,
                        ...params
                    });
                }
            },
        },
        PrivacyPolicy: {
            get: async (params) => {
                return await axios.get(base_api.User.Auth.privacyPolicy, {
                    params: {
                        ...params
                    },
                    ...defaultHeader.noCache
                })
            },
            accept: async (params) => {
                return await axios.put(base_api.User.Auth.privacyPolicy, {
                    params: {
                        ...params
                    }
                })
            },
        },
        addDevice: ({token}) => {
            axios.post(base_api.User.Global.addDevice, {
                token: token,
                device_id: Utils.uniqId()
            }).finally(() => {
            })
        }
    },
    Basket: {
        V2: {
            get: (props = {}) => {
                const api = base_api.User.Shopping.basket;
                const req = async () => {
                    if (false && DEBUG) {
                        const res = {
                            data: {
                                "basket": {
                                    "description": null, "products": [{
                                        "discount_percent": 6,
                                        "features": [{
                                            "priority": 2,
                                            "feature_value": "\u0646\u0627\u0631\u0646\u062c\u06cc",
                                            "feature_settings": {},
                                            "id": 628,
                                            "feature_groups": [],
                                            "feature": "\u0631\u0646\u06af \u0628\u0646\u062f\u06cc"
                                        }],
                                        "id": 224,
                                        "item_discount_price": 315000,
                                        "product": {
                                            "permalink": "servicetest100",
                                            "rate": 0,
                                            "default_storage": {
                                                "discount_percent": 6,
                                                "title": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 Adidas \u0645\u062f\u0644 MTS-1",
                                                "id": 852,
                                                "deadline": null,
                                                "max_count_for_sale": 3,
                                                "vip_type": null,
                                                "min_count_alert": 0,
                                                "discount_price": 315000,
                                                "vip_max_count_for_sale": null,
                                                "max_shipping_time": 0,
                                                "final_price": 338000
                                            },
                                            "id": 1285,
                                            "name": "\u0633\u0631\u0648\u06cc\u0633 \u062a\u0633\u062a 100",
                                            "disable": false,
                                            "thumbnail": {
                                                "image": "http://api.mt.com/media/boxes/3/2020-05-25/thumbnail/13-29-40-59-has-ph.jpg",
                                                "box": 3,
                                                "id": 1186,
                                                "type": "thumbnail",
                                                "title": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 \u0645\u062f\u0644 MTS-1 Aiddas"
                                            },
                                            "colors": []
                                        },
                                        "accessories": null,
                                        "amer": "\u0644\u0648\u0627\u0632\u0645 \u0648\u0631\u0632\u0634\u06cc",
                                        "discount_price": 630000,
                                        "final_price": 676000,
                                        "count": 2,
                                        "item_final_price": 338000
                                    }, {
                                        "discount_percent": 33,
                                        "features": [],
                                        "id": 226,
                                        "item_discount_price": 100000,
                                        "product": {
                                            "permalink": "\u062f\u06cc\u0641\u0646-\u0628\u0627\u062e\u06cc\u0627-ng25",
                                            "rate": 0,
                                            "default_storage": {
                                                "discount_percent": 33,
                                                "title": "\u062f\u06cc\u0641\u0646 \u0628\u0627\u062e\u06cc\u0627",
                                                "id": 718,
                                                "deadline": null,
                                                "max_count_for_sale": 10,
                                                "vip_type": null,
                                                "min_count_alert": 0,
                                                "discount_price": 100000,
                                                "vip_max_count_for_sale": null,
                                                "max_shipping_time": 0,
                                                "final_price": 150000
                                            },
                                            "id": 1116,
                                            "name": "\u062f\u06cc\u0641\u0646 \u0628\u0627\u062e\u06cc\u0627",
                                            "disable": false,
                                            "thumbnail": {
                                                "image": "http://api.mt.com/media/boxes/2/2020-07-31/thumbnail/11-29-13-65-has-ph.jpg",
                                                "box": 2,
                                                "id": 5192,
                                                "type": "thumbnail",
                                                "title": "\u062f\u06cc\u0641\u0646 \u0628\u0627\u062e\u06cc\u0627"
                                            },
                                            "colors": []
                                        },
                                        "accessories": null,
                                        "discount_price": 100000,
                                        "final_price": 150000,
                                        "count": 1,
                                        "item_final_price": 150000
                                    }, {
                                        "discount_percent": 7,
                                        "features": [],
                                        "id": 227,
                                        "item_discount_price": 12000,
                                        "product": {
                                            "permalink": "\u06af\u0644-\u0627\u0631\u06a9\u06cc\u062f\u0647",
                                            "rate": 0,
                                            "default_storage": {
                                                "discount_percent": 7,
                                                "title": "\u06af\u0644 \u0627\u0631\u06a9\u06cc\u062f\u0647",
                                                "id": 566,
                                                "deadline": null,
                                                "max_count_for_sale": 2,
                                                "vip_type": null,
                                                "min_count_alert": 0,
                                                "discount_price": 12000,
                                                "vip_max_count_for_sale": null,
                                                "max_shipping_time": 0,
                                                "final_price": 13000
                                            },
                                            "id": 909,
                                            "name": "\u06af\u0644 \u0627\u0631\u06a9\u06cc\u062f\u0647",
                                            "disable": false,
                                            "thumbnail": {
                                                "image": "http://api.mt.com/media/boxes/2/2020-07-04/thumbnail/12-40-14-87-has-ph.jpg",
                                                "box": 2,
                                                "id": 4029,
                                                "type": "thumbnail",
                                                "title": "\u06af\u0644 \u0627\u0631\u06a9\u06cc\u062f\u0647"
                                            },
                                            "colors": []
                                        },
                                        "accessories": null,
                                        "discount_price": 12000,
                                        "final_price": 13000,
                                        "count": 1,
                                        "item_final_price": 13000
                                    }, {
                                        "discount_percent": 55,
                                        "features": [{
                                            "priority": 0,
                                            "feature_value": "\u0632\u0631\u062f ",
                                            "feature_settings": {},
                                            "id": 377,
                                            "feature_groups": [],
                                            "feature": "1 \u0631\u0646\u06af \u06a9\u0641\u0634"
                                        }, {
                                            "priority": 6,
                                            "feature_value": "40",
                                            "feature_settings": {},
                                            "id": 633,
                                            "feature_groups": [],
                                            "feature": "1 \u0633\u0627\u06cc\u0632 \u06a9\u0641\u0634"
                                        }],
                                        "id": 228,
                                        "item_discount_price": 4000,
                                        "product": {
                                            "permalink": "\u0628\u0627\u0632\u06cc-\u0641\u06a9\u0631\u06cc-\u0648\u0633\u062a\u0631\u0648\u0633-game-of-thrones",
                                            "rate": 0,
                                            "default_storage": {
                                                "discount_percent": 55,
                                                "title": "\u0627\u0646\u0628\u0627\u0631 \u0641\u06cc\u0686\u0631 2",
                                                "id": 779,
                                                "deadline": null,
                                                "max_count_for_sale": 5,
                                                "vip_type": null,
                                                "min_count_alert": 0,
                                                "discount_price": 4000,
                                                "vip_max_count_for_sale": null,
                                                "max_shipping_time": 0,
                                                "final_price": 9000
                                            },
                                            "id": 61,
                                            "name": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 Adidas \u0645\u062f\u0644 MTS-1",
                                            "disable": false,
                                            "thumbnail": {
                                                "image": "http://api.mt.com/media/boxes/3/2020-05-25/thumbnail/13-29-40-59-has-ph.jpg",
                                                "box": 3,
                                                "id": 1186,
                                                "type": "thumbnail",
                                                "title": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 \u0645\u062f\u0644 MTS-1 Aiddas"
                                            },
                                            "colors": []
                                        },
                                        "accessories": null,
                                        "discount_price": 4000,
                                        "final_price": 9000,
                                        "count": 1,
                                        "item_final_price": 9000
                                    }], "id": 115
                                },
                                "summary": {
                                    "total_price": 868000,
                                    "discount_price": 766000,
                                    "profit": 102000,
                                    "shipping_cost": 20000,
                                    "tax": 0,
                                    "final_price": 0,
                                    "max_shipping_time": 0,
                                    "invoice_discount": 102000
                                },
                                "address_required": true,
                                "deleted": [],
                                "changed": [],
                                "active_invoice": []
                            }
                        }
                        return new Promise(function (resoleve, reject) {
                            setTimeout(function () {
                                resoleve(res);
                            }, 2000);
                        })
                        return
                    }


                    return await axios.get(api, {
                        params: props
                    }).then(res => {
                        return {
                            status: res?.status,
                            data: res?.data
                        }
                    })
                }
                return [
                    keyGenerator(api + "-V2", props),
                    req,
                    {
                        revalidateOnFocus: true,
                        refreshInterval: 0,
                        dedupingInterval: 120000,
                    }
                ]
            },
            add: async ({basket_id = null, storage_id, count, postParams = {}, ...params}) => {
                return await cu.Basket.V2.multiAdd({basket_id: basket_id, products: [{storage_id: storage_id, count: count}]})
            },
            multiAdd: async ({basket_id = null, products, postParams = {}, ...params}) => {
                return await axios.post(base_api.User.Shopping.basket, {
                    basket_id: basket_id,
                    products: products,
                    ...postParams,
                }, {
                    params: {
                        ...params
                    }
                })
            },
        },
        get: ({...params} = {}) => {
            const ap = base_api.User.Shopping.basket;
            const req = async () => {

                if (false && DEBUG) {
                    const res = {
                        data: {
                            "basket": {
                                "description": null, "products": [{
                                    "discount_percent": 6,
                                    "features": [{
                                        "priority": 2,
                                        "feature_value": "\u0646\u0627\u0631\u0646\u062c\u06cc",
                                        "feature_settings": {},
                                        "id": 628,
                                        "feature_groups": [],
                                        "feature": "\u0631\u0646\u06af \u0628\u0646\u062f\u06cc"
                                    }],
                                    "id": 224,
                                    "item_discount_price": 315000,
                                    "product": {
                                        "permalink": "servicetest100",
                                        "rate": 0,
                                        "default_storage": {
                                            "discount_percent": 6,
                                            "title": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 Adidas \u0645\u062f\u0644 MTS-1",
                                            "id": 852,
                                            "deadline": null,
                                            "max_count_for_sale": 3,
                                            "vip_type": null,
                                            "min_count_alert": 0,
                                            "discount_price": 315000,
                                            "vip_max_count_for_sale": null,
                                            "max_shipping_time": 0,
                                            "final_price": 338000
                                        },
                                        "id": 1285,
                                        "name": "\u0633\u0631\u0648\u06cc\u0633 \u062a\u0633\u062a 100",
                                        "disable": false,
                                        "thumbnail": {
                                            "image": "http://api.mt.com/media/boxes/3/2020-05-25/thumbnail/13-29-40-59-has-ph.jpg",
                                            "box": 3,
                                            "id": 1186,
                                            "type": "thumbnail",
                                            "title": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 \u0645\u062f\u0644 MTS-1 Aiddas"
                                        },
                                        "colors": []
                                    },
                                    "accessories": null,
                                    "amer": "\u0644\u0648\u0627\u0632\u0645 \u0648\u0631\u0632\u0634\u06cc",
                                    "discount_price": 630000,
                                    "final_price": 676000,
                                    "count": 2,
                                    "item_final_price": 338000
                                }, {
                                    "discount_percent": 33,
                                    "features": [],
                                    "id": 226,
                                    "item_discount_price": 100000,
                                    "product": {
                                        "permalink": "\u062f\u06cc\u0641\u0646-\u0628\u0627\u062e\u06cc\u0627-ng25",
                                        "rate": 0,
                                        "default_storage": {
                                            "discount_percent": 33,
                                            "title": "\u062f\u06cc\u0641\u0646 \u0628\u0627\u062e\u06cc\u0627",
                                            "id": 718,
                                            "deadline": null,
                                            "max_count_for_sale": 10,
                                            "vip_type": null,
                                            "min_count_alert": 0,
                                            "discount_price": 100000,
                                            "vip_max_count_for_sale": null,
                                            "max_shipping_time": 0,
                                            "final_price": 150000
                                        },
                                        "id": 1116,
                                        "name": "\u062f\u06cc\u0641\u0646 \u0628\u0627\u062e\u06cc\u0627",
                                        "disable": false,
                                        "thumbnail": {
                                            "image": "http://api.mt.com/media/boxes/2/2020-07-31/thumbnail/11-29-13-65-has-ph.jpg",
                                            "box": 2,
                                            "id": 5192,
                                            "type": "thumbnail",
                                            "title": "\u062f\u06cc\u0641\u0646 \u0628\u0627\u062e\u06cc\u0627"
                                        },
                                        "colors": []
                                    },
                                    "accessories": null,
                                    "discount_price": 100000,
                                    "final_price": 150000,
                                    "count": 1,
                                    "item_final_price": 150000
                                }, {
                                    "discount_percent": 7,
                                    "features": [],
                                    "id": 227,
                                    "item_discount_price": 12000,
                                    "product": {
                                        "permalink": "\u06af\u0644-\u0627\u0631\u06a9\u06cc\u062f\u0647",
                                        "rate": 0,
                                        "default_storage": {
                                            "discount_percent": 7,
                                            "title": "\u06af\u0644 \u0627\u0631\u06a9\u06cc\u062f\u0647",
                                            "id": 566,
                                            "deadline": null,
                                            "max_count_for_sale": 2,
                                            "vip_type": null,
                                            "min_count_alert": 0,
                                            "discount_price": 12000,
                                            "vip_max_count_for_sale": null,
                                            "max_shipping_time": 0,
                                            "final_price": 13000
                                        },
                                        "id": 909,
                                        "name": "\u06af\u0644 \u0627\u0631\u06a9\u06cc\u062f\u0647",
                                        "disable": false,
                                        "thumbnail": {
                                            "image": "http://api.mt.com/media/boxes/2/2020-07-04/thumbnail/12-40-14-87-has-ph.jpg",
                                            "box": 2,
                                            "id": 4029,
                                            "type": "thumbnail",
                                            "title": "\u06af\u0644 \u0627\u0631\u06a9\u06cc\u062f\u0647"
                                        },
                                        "colors": []
                                    },
                                    "accessories": null,
                                    "discount_price": 12000,
                                    "final_price": 13000,
                                    "count": 1,
                                    "item_final_price": 13000
                                }, {
                                    "discount_percent": 55,
                                    "features": [{
                                        "priority": 0,
                                        "feature_value": "\u0632\u0631\u062f ",
                                        "feature_settings": {},
                                        "id": 377,
                                        "feature_groups": [],
                                        "feature": "1 \u0631\u0646\u06af \u06a9\u0641\u0634"
                                    }, {
                                        "priority": 6,
                                        "feature_value": "40",
                                        "feature_settings": {},
                                        "id": 633,
                                        "feature_groups": [],
                                        "feature": "1 \u0633\u0627\u06cc\u0632 \u06a9\u0641\u0634"
                                    }],
                                    "id": 228,
                                    "item_discount_price": 4000,
                                    "product": {
                                        "permalink": "\u0628\u0627\u0632\u06cc-\u0641\u06a9\u0631\u06cc-\u0648\u0633\u062a\u0631\u0648\u0633-game-of-thrones",
                                        "rate": 0,
                                        "default_storage": {
                                            "discount_percent": 55,
                                            "title": "\u0627\u0646\u0628\u0627\u0631 \u0641\u06cc\u0686\u0631 2",
                                            "id": 779,
                                            "deadline": null,
                                            "max_count_for_sale": 5,
                                            "vip_type": null,
                                            "min_count_alert": 0,
                                            "discount_price": 4000,
                                            "vip_max_count_for_sale": null,
                                            "max_shipping_time": 0,
                                            "final_price": 9000
                                        },
                                        "id": 61,
                                        "name": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 Adidas \u0645\u062f\u0644 MTS-1",
                                        "disable": false,
                                        "thumbnail": {
                                            "image": "http://api.mt.com/media/boxes/3/2020-05-25/thumbnail/13-29-40-59-has-ph.jpg",
                                            "box": 3,
                                            "id": 1186,
                                            "type": "thumbnail",
                                            "title": "\u06a9\u0641\u0634 \u0648\u0631\u0632\u0634\u06cc \u0632\u0646\u0627\u0646\u0647 \u0645\u062f\u0644 MTS-1 Aiddas"
                                        },
                                        "colors": []
                                    },
                                    "accessories": null,
                                    "discount_price": 4000,
                                    "final_price": 9000,
                                    "count": 1,
                                    "item_final_price": 9000
                                }], "id": 115
                            },
                            "summary": {
                                "total_price": 868000,
                                "discount_price": 766000,
                                "profit": 102000,
                                "shipping_cost": 20000,
                                "tax": 0,
                                "final_price": 0,
                                "max_shipping_time": 0,
                                "invoice_discount": 102000
                            },
                            "address_required": true,
                            "deleted": [],
                            "changed": [],
                            "active_invoice": []
                        }
                    }
                    return await new Promise(function (resoleve, reject) {
                        gLog("asfkjskajfkjaskjf res", res)
                        setTimeout(function () {
                            const newData = [];
                            _.forEach(res.data.basket.products, (p) => {
                                const product = convertProduct(p.product);
                                product.features = convertFeatures(p.features);
                                tryIt(() => {
                                    newData.push({
                                        id: p.id,
                                        count: p.count,
                                        itemFinalPrice: p.item_final_price,
                                        finalPrice: p.final_price,
                                        itemDiscountPrice: p.item_discount_price,
                                        discountPrice: p.discount_price,
                                        discountPercent: p.discount_percent,
                                        product: product
                                    })
                                })
                            });
                            gLog("asfkjskajfkjaskjf newData", newData)
                            gLog("asfkjskajfkjaskjf finish", {
                                status: 200,
                                data: {
                                    data: [...newData],
                                    basket_id: res.data.basket.id,
                                    addressRequired: res.data.address_required,
                                    active_invoice: res.data.active_invoice || [],
                                    ...generateSummary({
                                        max_shipping_time: res.data.summary.max_shipping_time,
                                        totalProfit: res.data.summary.profit,
                                        shippingCost: res.data.summary.shipping_cost,
                                        totalPrice: res.data.summary.total_price,
                                        discountPrice: res.data.summary.discount_price,
                                    }),
                                }
                            })
                            resoleve({
                                status: 200,
                                data: {
                                    data: [...newData],
                                    basket_id: res.data.basket.id,
                                    addressRequired: res.data.address_required,
                                    active_invoice: res.data.active_invoice || [],
                                    ...generateSummary({
                                        max_shipping_time: res.data.summary.max_shipping_time,
                                        totalProfit: res.data.summary.profit,
                                        shippingCost: res.data.summary.shipping_cost,
                                        totalPrice: res.data.summary.total_price,
                                        discountPrice: res.data.summary.discount_price,
                                    }),
                                }
                            });
                        }, 2000);
                    })
                }


                return await axios.get(ap, {
                    params: {
                        ...params,
                        ...apiHelper.getCacheParam({networkFirst: true}),
                    },
                    ...defaultHeader.noCache
                }).then(res => {
                    const newData = [];
                    _.forEach(res.data.basket.products, (p) => {
                        const product = convertProduct(p.product);
                        product.features = convertFeatures(p.features);
                        tryIt(() => {
                            newData.push({
                                id: p.id,
                                count: p.count,
                                itemFinalPrice: p.item_final_price,
                                finalPrice: p.final_price,
                                itemDiscountPrice: p.item_discount_price,
                                discountPrice: p.discount_price,
                                discountPercent: p.discount_percent,
                                product: product
                            })
                        })
                    });
                    return {
                        status: 200,
                        data: {
                            data: [...newData],
                            basket_id: res.data.basket.id,
                            addressRequired: res.data.address_required,
                            active_invoice: res.data.active_invoice || [],
                            ...generateSummary({
                                max_shipping_time: res.data.summary.max_shipping_time,
                                totalProfit: res.data.summary.profit,
                                shippingCost: res.data.summary.shipping_cost,
                                totalPrice: res.data.summary.total_price,
                                discountPrice: res.data.summary.discount_price,
                            }),
                        }
                    }
                });
            };
            return [ap, req];
        },
        add: async ({basket_id = null, storage_id, count, features, override = true, postParams = {}, ...params}) => {
            return cu.Basket.multiAdd({
                basket_id: basket_id,
                products: [apiHelper.createProductItem({storage_id: storage_id, count: count, features})],
                override: override,
                postParams: postParams,
                ...params
            })
        },
        multiAdd: async ({basket_id = null, products, override = true, postParams = {}, ...params}) => {

            return await axios.post(base_api.User.Shopping.basket, {
                basket_id: basket_id,
                products: products,
                override: override,
                ...postParams,
            }, {
                params: {
                    ...params
                }
            })
        },
        addWithBasketSummaryResponse: async ({
                                                 basket_id,
                                                 basketproduct_id,
                                                 storage_id,
                                                 count
                                             }, props = null) => {
            return await axios.patch(base_api.User.Shopping.basket, {
                basket_id: basket_id,
                basket_product_id: basketproduct_id,
                storage_id: storage_id,
                count: count
            }).then(res => {
                return {
                    status: 200,
                    data: {
                        ...generateSummary({
                            totalProfit: res.data.summary.profit,
                            shippingCost: res.data.summary.shopping_cost,
                            totalPrice: res.data.summary.total_price,
                            discountPrice: res.data.summary.discount_price,
                        })
                    }
                }
            });
        },
        delete: async ({basket_id, basketproduct_id, storage_id, ...params}) => {
            return await axios.delete(base_api.User.Shopping.basket, {
                params: {
                    basket_id: basket_id,
                    basket_product_id: basketproduct_id,
                    storage_id: storage_id,
                    summary: true,
                    ...params
                }
            }).then((res) => {
                if (_.isEmpty(res.data.basket)) {
                    return {
                        data: {
                            products: [],
                            ...generateSummary({
                                totalProfit: 0,
                                shippingCost: -1,
                                totalPrice: 0,
                                discountPrice: 0,
                            })
                        }
                    }
                }

                return {
                    data: {
                        products: convertProducts(res.data.basket.products),
                        ...generateSummary({
                            totalProfit: res.data.summary.profit,
                            shippingCost: res.data.summary.shopping_cost,
                            totalPrice: res.data.summary.total_price,
                            discountPrice: res.data.summary.discount_price,
                        })
                    }
                }
            })
        },
        productsSummery: async ({products = null}) => {
            alert("checkBug");
            // if (!products && !userIsLogin()) {
            //     return {}
            // }
            if (!products) {
                // request to server
            }
            let totalProfit = 0;
            let totalAmount = 0;
            let shippingCost = 0;
            await _.forEach(products, item => {
                const count = item.count;
                const product = item.product;
                totalAmount = totalAmount + (_.toNumber(product.final_price) * count);
                //discount
                totalProfit = totalProfit + ((_.toNumber(product.final_price) - _.toNumber(product.discount_price)) * count);
            });
            return {
                status: 200,
                data: {
                    ...generateSummary({
                        totalProfit: totalProfit,
                        shippingCost: shippingCost,
                        totalPrice: totalAmount,
                        discountPrice: totalAmount - totalProfit,
                    }).summary,
                }
            }
        },
        payment: ({basket_id, ipg_id}) => {
            const api = base_api.User.Shopping.payment + `/${basket_id}`;
            const req = async () => {
                return await axios.get(api, {
                    params: {
                        ipg_id: ipg_id,
                        ...apiHelper.getCacheParam({networkOnly: true})
                    },
                    ...defaultHeader.noCache
                }).then((res) => {
                    return {
                        states: res.status,
                        data: {
                            url: res.data.url,
                            redirect: () => {
                                window.location.replace(res.data.url)
                            }
                        }
                    }
                })
            };
            return [api, req];
        },
        findStorageIndex: async ({newStorage, features, ...params}) => {
            const basket = Actions.Basket.main();
            return await axios.patch(base_api.Products.get, {
                basket: basket.data || [],
                product: {
                    storage_id: newStorage,
                    features: features
                },
            }, {
                params: {
                    ...params
                }
            })
        },
        setDiscountCode: async ({code}) => {
            return await axios.post(base_api.User.Shopping.discountCode, {
                code: code
            })
        },
        Booking: {
            Datetime: {
                add: async ({storageId, date, count, cart_postal_text = {}}) => {
                    return await axios.post(base_api.User.Shopping.booking, {
                        storage_id: storageId,
                        start_date: date,
                        type: "datetime",
                        cart_postal_text: cart_postal_text
                    }).then(res => {
                        return {
                            ...res,
                            data: {
                                ...res.data.data,
                                address: convertAddress(res.data.data.address)
                            }
                        }
                    })
                }
            }
        }
    },
    Shopping: {
        edit: async ({invoiceId}) => {
            return await axios.patch(base_api.User.Shopping.editInvoice + `/${invoiceId}`)
        },
        payment: async ({basket_id, ipg_id, postParams = {}, ...params}) => {
            return await axios.get(base_api.User.Shopping.payment + `/${basket_id}`, {
                ipg: ipg_id,
                ...postParams,
                ...defaultHeader.noCache
            }, {
                params: {
                    ...params
                },
                ...defaultHeader.noCache
            }).then((res) => {
                return {
                    state: 200,
                    redirectUrl: res.data.url
                }
            })
        },
        getIpgLink: async ({paymentUrl}) => {
            return await axios.get(paymentUrl)
        }
    }
};

export default cu;

function loginSuccessData({res}) {
    let data = {};
    let dispatcher = undefined;
    try {
        const {user, basket_count} = res.data;
        data.user = convertUser(user);
        data.basketCount = basket_count;
        // dispatcher = () => {
        //     dispatch(login(data, directTo));
        // };
        // if (save && dispatch)
        //     dispatcher();
    } catch (e) {
        if (res.data.resend_timeout) {
            data = {
                code: res.data.code,
                timeout: res.data.timeout,
                resendTimeout: res.data.resend_timeout,

            }
        } else {
            data = res.data
        }
    }
    return {
        status: res.status,
        data: {
            ...data,
            dispatcher: dispatcher
        }
    }
}





