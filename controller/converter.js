import _ from 'lodash';
import rout from '../router'
import momentJalaali from "moment-jalaali";
import JDate from "../utils/JDate";
import {lang} from "../repository";
import {Phone, PhoneIphone} from "@material-ui/icons";
import React from "react";
import {cyan, green, grey, red} from "@material-ui/core/colors";
import {productType} from "./type";
import {DEBUG} from "../pages/_app";
import {getSafe, sleep} from "material-ui-helper";


//region Product

export const convertProducts = (products) => {
    const list = [];
    _.forEach(products, (product) => {
        list.push(convertProduct(product))
    });
    return list
};

export const convertProduct = (p) => {
    if (!p)
        return

    let val = {
        id: p.id,
        name: p.name,
        permalink: p.permalink,
        thumbnail: {
            ...p.thumbnail,
            title: p.title || p.name,
        },
        disable: p.disable,
        deadline: _.toInteger(p.deadline),
        location: p.location,
        description: p.description,
        media: p.media,
        shortDescription: getSafe(() => p.short_description.replace(/(<([^>]+)>)/gi, ""), ""),
        type: p.type,
        isService: p.type === productType.SERVICE,
        box: p.box,
        category: p.category,
        breadcrumbs: [],
        shortAddress: p.short_address,
        address: p.address,
        tag: p.tag,
        gender: p.gender,
        brand: p.brand,
        rate: DEBUG ? 3.5 : p.rate ? p.rate / 2 : undefined,
        wishlist: DEBUG
    };

    if (p.box) {
        val.breadcrumbs.push({
            id: p.box.id,
            name: p.box.name,
            link: rout.Product.Search.as({
                category: p.box.permalink
            }),
        })
    }

    if (p.category) {
        let category = p.category;
        while (category) {
            val.breadcrumbs.push({
                id: category.id,
                name: category.name,
                link: rout.Product.Search.as({
                    category: category.permalink
                }),
            });
            category = category.child;
        }
    }

    if (!_.isEmpty(val.breadcrumbs)) {
        val.breadcrumbs.push({
            id: p.id,
            name: p.name
        })
    }

    if (p.default_storage) {
        try {
            val = {
                ...val,
                defaultStorage: convertStorage(p.default_storage),
            };
        } catch (e) {
        }
    }

    if (p.storages) {
        try {
            val = {
                ...val,
                storages: _.sortBy(convertStorages(p.storages), [function (o) {
                    return o.default === false;
                }])
            };
            if (!p.default_storage && !_.isEmpty(val.storages)) {
                val = {
                    ...val,
                    defaultStorage: val.storages[0]
                }
            }
        } catch (e) {
        }
    }

    if (val.defaultStorage) {
        val = {
            ...val,
            ...val.defaultStorage
        }
    }

    if (p.properties) {
        val = {
            ...val,
            properties: {
                usageCondition: convertProperties(p.properties.usage_condition),
                property: convertProperties(p.properties.property)
            }
        }
    }

    if (p.details) {
        const phones = [];
        _.forEach(p.details.phones, (p) => {
            phones.push({
                ...p,
                icon: p.type === "phone" ? <PhoneIphone/> : <Phone/>
            })
        });
        val = {
            ...val,
            details: {
                text: p.details.text,
                phones: phones,
                servingDays: p.details.serving_days || p.details.servingDays,
                servingHours: p.details.serving_hours || p.details.servingHours
            }
        }
    }


    val.disable = p.disable;

    return val
};

//region Storage
export function convertStorages(storages) {
    const list = [];
    _.forEach(storages, (st) => {
        list.push(convertStorage(st))
    });
    return list
}

export function convertStorage(st) {
    const isVip = Boolean(st.vip_type);
    const oDiscountPercent = st.discount_percent === 0 ? undefined : st.discount_percent;
    const vipDiscountPercent = st.discount_vip_percent === 0 ? undefined : st.discount_vip_percent;
    // if (DEBUG)
    //     st.max_shipping_time = Math.floor(Math.random() * 100)


    return {
        storage_id: st.id,
        storageTitle: st.title,
        default: st.default,
        disable: st.disable,
        transportationPrice: st.transportation_price,
        vipType: st.vip_type,
        isVip: isVip,
        deadline: st.deadline,
        priority: st.priority,
        jDeadline: st.deadline ? momentJalaali(new Date(st.deadline * 1000)).format('jYYYY/jM/jD') : undefined,
        finalPrice: st.final_price,
        features: convertFeatures(st.features),
        max_shipping_time: (st.max_shipping_time >= 24) ? Math.ceil(st.max_shipping_time / 24) : st.max_shipping_time,
        org_max_shipping_time: st.max_shipping_time,
        shippingTimeIsHours: st.max_shipping_time < 24,

        oMaxCountForSale: st.max_count_for_sale,
        vipMaxCountForSale: st.vip_max_count_for_sale,
        maxCountForSale: (isVip && st.vip_max_count_for_sale < st.max_count_for_sale) ? st.vip_max_count_for_sale : st.max_count_for_sale,

        oDiscountPrice: st.discount_price,
        vipDiscountPrice: st.discount_vip_price,
        discountPrice: (isVip && st.discount_vip_price < st.discount_price) ? st.discount_vip_price : st.discount_price,

        oDiscountPercent: oDiscountPercent,
        vipDiscountPercent: vipDiscountPercent,
        discountPercent: (isVip && vipDiscountPercent > oDiscountPercent) ? vipDiscountPercent : oDiscountPercent,

        booking_cost: st.booking_cost,
        least_booking_time: st.least_booking_time,
    }
}

//endregion Storage

//region Property
export const convertProperties = (properties) => {

    const high = [];
    const medium = [];
    const low = [];
    _.forEach(properties, (p) => {
        switch (p.priority) {
            case 'high':
                high.push({
                    isHigh: true,
                    ...p
                });
                break;
            case 'medium':
                medium.push({
                    isMedium: true,
                    ...p
                });
                break;
            default:
                low.push({
                    isLow: true,
                    ...p
                });
                break;
        }
    });
    return [
        ...high,
        ...medium,
        ...low
    ]
};
//endregion Property

//region Comment
export function convertComments(comments) {
    const list = [];
    _.forEach(comments, (cm) => {
        list.push(convertComment(cm))
    });
    return list
}

export function convertComment(cm) {
    const purchaseAt = cm.purchase_at ? JDate.timeStampToJalali(cm.purchase_at) : undefined;
    const createdAt = JDate.timeStampToJalali(cm.created_at);
    return {
        id: cm.id,
        text: cm.text,
        user: cm.user,
        rate: cm.rate,
        approved: cm.approved,
        replyCount: cm.reply_count,
        type: cm.type,
        satisfied: cm.satisfied,
        firstReply: cm.first_reply ? convertComment(cm.first_reply) : undefined,
        purchaseAt: purchaseAt ? purchaseAt.format('jYYYY/jM/jD HH:mm') : undefined,
        purchaseAtTimeStamp: cm.purchase_at,
        createdAt: createdAt.format('jD  jMMMM  jYYYY'),
        createdAtTimeStamp: cm.created_at,
        commentTimeAfterPurchase: {
            days: createdAt.diff(purchaseAt, 'days'),
            hours: createdAt.diff(purchaseAt, 'hours'),
        }
    }
}

//endregion Comment

//region Feature
export function convertFeatures(features) {
    const list = [];
    _.forEach(features, (f) => {
        list.push(convertFeature(f))
    });
    return list
}

export function convertFeature(f) {
    return f
}

//endregion Feature
//endregion Product

//region generateSummary

/**
 * @param {number}  totalProfit
 * @param {number}  shippingCost
 * @param {number}  totalPrice
 * @param {number}  discountPrice
 * @param {number}  max_shipping_time
 * @returns {{summary: {shippingCost: number, totalProfit: number, totalPrice: number, discountPrice: number}}}
 */
export const generateSummary = ({totalProfit, shippingCost = -1, totalPrice, max_shipping_time = -1, discountPrice} = {
    totalProfit: 0,
    shippingCost: -1,
    totalPrice: 0,
    discountPrice: 0,
    max_shipping_time: -1
}) => {
    return {
        summary: {
            totalProfit: totalProfit,
            shippingCost: shippingCost,
            totalPrice: totalPrice,
            discountPrice: discountPrice,
            max_shipping_time: max_shipping_time
        }
    }
}
//endregion generateSummary


//region user
export const InvoiceStatus = {
    CANCELED: 'canceled',
    PAYED: 'payed',
    REJECTED: 'rejected',
    PENDING: 'pending',
    SENT: 'sent',
};

export function convertUser(us) {
    if (!us)
        return {}
    const firstName = (us.first_name) ? us.first_name : '';
    const lastName = us.last_name ? us.last_name : '';
    const user = {
        id: us.id,
        username: us.username,
        firstName: firstName,
        lastName: lastName,
        fullName: (_.isEmpty(firstName) && _.isEmpty(lastName) ? '' : firstName + " " + lastName),
        phone: us.username,
        email: us.email || '',
        walletMoney: us.walletMoney,
        meliCode: us.meli_code ? us.meli_code : '',
        gender: gender(us.gender),
        birthday: us.birthday,
        jBirthday: us.birthday ? JDate.timeStampToJalali(us.birthday).format('jYYYY/jM/jD') : undefined,
        avatar: us.avatar,
        shaba: us.shaba ? us.shaba : '',
        isActiveData: (!_.isEmpty(firstName) && !_.isEmpty(lastName) && !_.isEmpty(us.meli_code)),
        roll: us.roll
    };
    if (us.is_staff) {
        user.isStaff = true
    }
    return user
}


export function convertAddresses(addresses) {
    const list = [];
    _.forEach(addresses, (ad) => {
        list.push(convertAddress(ad))
    });
    return list;
}

export function convertAddress(ad) {
    let state = null;
    try {
        _.forEach(states, (st) => {
            if (st.id === ad.city.state) {
                state = st;
                return true;
            }
        });
    } catch (e) {
    }
    if (!state)
        return undefined

    return {
        id: ad.id,
        postalCode: ad.postal_code,
        phone: ad.phone,
        name: ad.name,
        address: ad.address,
        fullAddress: ad.city ? (state.label + ' - ' + ad.city.name + ' - ' + ad.address) : "",
        state: state,
        city: {
            ...ad.city,
            label: ad.city.name
        },
        location: ad.location
    }
}

export function convertInvoices(invoices) {
    const list = [];
    _.forEach(invoices, (i) => {
        list.push(convertInvoice(i))
    });
    return list
}

export function convertInvoice(invoice) {

    return {
        ...invoice,
        id: invoice.id,
        invoice_id: invoice.invoice_id,
        storage: {
            id: invoice.storage.id,
            title: invoice.storage.title,
            invoiceTitle: invoice.storage.invoice_title || invoice.storage.title,
            invoiceDescription: invoice.storage.invoice_description,
        },
        product: {
            name: invoice.product.name,
            permalink: invoice.product.permalink,
            code: invoice.product.code,
            thumbnail: invoice.product.thumbnail,
            type: invoice.product.type,
            isService: invoice.product.type === productType.SERVICE,
            invoiceDescription: invoice.product.invoice_description,
        },
        box: invoice.amer,
        finalPrice: invoice.final_price,
        discountPercent: invoice.discount_percent,
        count: invoice.count,
        unitPrice: invoice.unit_price,
        discountPrice: invoice.amount || invoice.discount_price,
        amount: invoice.amount || invoice.discount_price,
        purchaseDate: invoice.purchase_date,
        jPurchaseDate: invoice.purchase_date ? momentJalaali(new Date(invoice.purchase_date * 1000)).format('jYYYY/jM/jD') : undefined
    }
}


export function convertInvoiceStatus(status) {

    const p = {};
    switch (status) {
        case InvoiceStatus.CANCELED: {
            p.isCanceled = true;
            p.statusLabel = lang.get('payment_canceled');
            p.statusColor = grey[300];
            break
        }
        case InvoiceStatus.PAYED: {
            p.isPayed = true;
            p.statusLabel = lang.get('payment_payed');
            p.statusColor = green[300];
            break
        }
        case InvoiceStatus.REJECTED: {
            p.isRejected = true;
            p.statusLabel = lang.get('rejected');
            p.statusColor = red[300];
            break
        }
        case InvoiceStatus.PENDING: {
            p.isPending = true;
            p.statusLabel = lang.get('pending');
            p.statusColor = cyan[300];
            break
        }
        case InvoiceStatus.SENT: {
            p.isSent = true;
            p.statusLabel = lang.get('sent');
            p.statusColor = green[600];
            break
        }
        default: {
        }
    }

    return p;
}

//endregion user

//region Global

export const keyGenerator = (api,pr) => {
    //Sort Object by key
    const params = _(pr).toPairs().sortBy(0).fromPairs().value()
    return getSafe(() => {
        let ap = api+"?"
        _.forEach(params, (c, k) => {
            if (_.isString(c) || _.isNumber(c))
                ap = ap + `${k}=${c}&`
            else
                getSafe(() => {
                    if (_.isArray(c)) c = _.sortBy(c)
                    else c = _(c).toPairs().sortBy(0).fromPairs().value()
                    c = _.uniq(c)
                    _.forEach(c, v => {
                        ap = ap + `${k}=${v}&`
                    })
                })
        })
        ap = ap.substring(0, ap.length - 1)
        return ap
    })
}
export const paginationParams = (page, step) => {
    return {
        s: step,
        p: page
    }
};

export const convertPagination = (pagination, activePage) => {
    return {
        lastPage: pagination.last_page,
        count: pagination.count,
        page: activePage ? activePage : undefined,
        hasMoreItems: activePage ? activePage + 1 < pagination.last_page : undefined
    }
};

export const states = [
    {
        "label": "یزد",
        "id": 31
    },
    {
        "label": "همدان",
        "id": 30
    },
    {
        "label": "هرمزگان",
        "id": 29
    },
    {
        "label": "مرکزی",
        "id": 28
    },
    {
        "label": "مازندران",
        "id": 27
    },
    {
        "label": "لرستان",
        "id": 26
    },
    {
        "label": "گیلان",
        "id": 25
    },
    {
        "label": "گلستان",
        "id": 24
    },
    {
        "label": "کهکیلویه و بویراحمد",
        "id": 23
    },
    {
        "label": "کرمانشاه",
        "id": 22
    },
    {
        "label": "کرمان",
        "id": 21
    },
    {
        "label": "کردستان",
        "id": 20
    },
    {
        "label": "قم",
        "id": 19
    },
    {
        "label": "قزوین",
        "id": 18
    },
    {
        "label": "فارس",
        "id": 17
    },
    {
        "label": "سیستان و بلوچستان",
        "id": 16
    },
    {
        "label": "سمنان",
        "id": 15
    },
    {
        "label": "زنجان",
        "id": 14
    },
    {
        "label": "خوزستان",
        "id": 13
    },
    {
        "label": "خراسان شمالی",
        "id": 12
    },
    {
        "label": "خراسان رضوی",
        "id": 11
    },
    {
        "label": "خراسان جنوبی",
        "id": 10
    },
    {
        "label": "چهارمحال بختیاری",
        "id": 9
    },
    {
        "label": "تهران",
        "id": 8
    },
    {
        "label": "بوشهر",
        "id": 7
    },
    {
        "label": "ایلام",
        "id": 6
    },
    {
        "label": "البرز",
        "id": 5
    },
    {
        "label": "اصفهان",
        "id": 4
    },
    {
        "label": "اردبیل",
        "id": 3
    },
    {
        "label": "آذربایجان غربی",
        "id": 2
    },
    {
        "label": "آذربایجان شرقی",
        "id": 1
    }
];

// export const states = [
//     {
//         "label": "گیلان",
//         "id": 25
//     },
//     {
//         "label": "تهران",
//         "id": 8
//     },
// ];


export const gender = (gender) => {
    if (gender === null)
        return {
            value: gender,
            label: lang.get("gender_none"),
        };
    return {
        value: gender,
        label: lang.get(gender ? "gender_man" : 'gender_woman'),
    }
};


export const defaultIcon = 'default.svg';

export function convertIconUrl(iconName) {
    if (!iconName)
        iconName = defaultIcon;
    return `${process.env.REACT_APP_API}/media/icon/${iconName}`
}

export function convertIconName(iconName) {
    if (!iconName)
        iconName = defaultIcon;
    return _.startCase(iconName.replaceAll(/.+?\\(?=\w+)|\.\w+$|$/g, ""))
}

//endregion Global
