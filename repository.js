import GAnalytics from './utils/GAnalytics'
import Language from "./language/Language";
import {amber, green, grey, red} from "@material-ui/core/colors";
import createPalette from "@material-ui/core/styles/createPalette";
import moment from "moment-jalaali";
import mediaQuery from 'css-mediaquery';
import LocalStorageUtils from "./LocalStorageUtils";
import cookies from "./cookies";
import {tryIt} from "material-ui-helper";
import {createTheme} from "@material-ui/core";

export const siteBackground = "#fff";

export function dir() {
    return 'rtl'
}

export const siteRout = process.env.REACT_APP_SITE_ROUT;
export const isServer = () => !process.browser;
export const isClient = () => process.browser;
export const canUseDOM = !!(
    typeof window !== 'undefined' &&
    typeof window.document !== 'undefined' &&
    typeof window.document.createElement !== 'undefined'
);
export const siteLang = 'fa';
export const lang = new Language(siteLang);
export const cookieVersion = '2';
export const localStorageVersion = '2';

export const icons={
    mtChevronRight:"'\\e914'",
    mtChevronLeft:"'\\e913'"
}

export function checkCookieVersion() {
    const ck = LocalStorageUtils.get("cookieVersion", null);
    if (ck === cookieVersion)
        return;
    cookies.removeAll();
    LocalStorageUtils.set("cookieVersion", cookieVersion);
}

export function checkLocalStorageVersion() {
    const ck = LocalStorageUtils.get("localStorageVersion", null);
    if (ck === localStorageVersion)
        return;
    LocalStorageUtils.removeAll();
    LocalStorageUtils.set("localStorageVersion", localStorageVersion);
    LocalStorageUtils.set("cookieVersion", cookieVersion);
}


moment.loadPersian([]);

const ssrMatchMedia = query => ({
    matches: mediaQuery.match(query, {
        // The estimated CSS width of the browser.
        width: 800,
    }),
});

export let theme = createTheme({
    direction: dir(),
    palette: createPalette({
        primary: {
            light: "#ff527b",
            main: "#ff527b",
            dark: "#ff527b"
        },
        secondary: {
            light: "#3278c9",
            main: "#3278c9",
            dark: "#3278c9"
        }
    }),
    status: {},
    props: {
        MuiCard: {
            elevation: 2
        },
        MuiTooltip: {},
        MuiUseMediaQuery: {ssrMatchMedia},
    },
    typography: {
        htmlFontSize: 20,
    },
});

//region fontSize
theme.typography.h1 = {
    ...theme.typography.h1,
    fontWeight: 300,
    color: grey[900],
    fontSize: '3rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '3.8rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '4rem',
    },
};
theme.typography.h2 = {
    ...theme.typography.h2,
    fontWeight: 300,
    color: grey[900],
    fontSize: '2.2rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '2.3rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '2rem',
    },
};
theme.typography.h3 = {
    ...theme.typography.h3,
    fontWeight: 300,
    color: grey[900],
    fontSize: '1.9rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '2rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '1.8rem',
    },
};
theme.typography.h4 = {
    ...theme.typography.h4,
    fontWeight: 300,
    color: grey[900],
    fontSize: '1.65rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.8rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '1.6rem',
    },
};
theme.typography.h5 = {
    ...theme.typography.h5,
    fontWeight: 300,
    color: grey[900],
    fontSize: '1.2rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.4rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '1.05rem !important',
    },
};
theme.typography.h6 = {
    ...theme.typography.h6,
    fontWeight: 300,
    color: grey[900],
    fontSize: '1rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.1rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.8rem !important',
    },
};
theme.typography.subtitle1 = {
    ...theme.typography.body1,
    fontSize: '0.9rem',
    fontWeight: 400,
    color: grey[800],
    [theme.breakpoints.up('lg')]: {
        fontSize: '1rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.7rem !important',
    },
};
theme.typography.subtitle2 = {
    ...theme.typography.body2,
    fontWeight: 400,
    color: grey[800],
    fontSize: '0.76rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.9rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.7rem !important',
    },
};

theme.typography.body1 = {
    ...theme.typography.body1,
    fontSize: '0.8rem',
    fontWeight: 300,
    color: grey[800],
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.9rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.75rem',
    },
};
theme.typography.body2 = {
    ...theme.typography.body2,
    fontWeight: 300,
    color: grey[800],
    fontSize: '0.71rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.8rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.7rem',
    },
};
theme.typography.overline = {
    ...theme.typography.overline,
    fontWeight: 200,
    color: grey[800],
    fontSize: '0.65rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.71rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.6rem',
    },
};
// theme.typography.button = {
//     ...theme.typography.button,
//     fontWeight: 200,
//     color: grey[800],
//     fontSize: '0.8rem',
//     [theme.breakpoints.up('md')]: {
//         fontSize: '1rem',
//     }
// };
theme.typography.caption = {
    ...theme.typography.caption,
    fontWeight: 200,
    color: grey[800],
    fontSize: '0.6rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.68rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.55rem',
    },
};

//endregion

export const media = {
    promotedCategories:{
        width: 600,
        height: 600,
    },
    category: {
        width: 800,
        height: 500,
    },
    thumbnail: {
        width: 600,
        height: 372,
        container: {
            width: 600,
            height: 372,
        },
    },
    media: {
        width: 1280,
        height: 794
    },
    slider: {
        width: 1919,
        height: 501,
        XS: {
            width: 326,
            height: 285
        }
    },
    ads:{
        1:{
            width: 1760,
            height: 400,
            sm:{
                width: 328,
                height: 184,
            }
        },
        2:{
            width: 864,
            height: 320,
            sm:{
                width: 156,
                height: 200,
            }
        },
        4:{
            width: 422,
            height: 320,
            sm:{
                width: 156,
                height: 112,
            }
        },
    },
    convertor: (
        src,
        {
            width,
            height,
            cropWidth,
            cropHeight,
            quality = 80
        }) => {
        return tryIt(() => {
            if (!src)
                return src
            let mediaRout = 'https://api.mehrtakhfif.com/media/';
            if (!src.includes(mediaRout))
                return src
            let newSrc = src.replace(mediaRout, "");

            if (newSrc === src) {
                // mediaRout = 'http://api.mt.com/media/';
                newSrc = src.replace('http://api.mt.com/media/', "");
            }

            let query = "";
            if (width)
                query = query + `resize_${width}x${height || width}&`
            if (cropWidth && cropHeight)
                query = query + `crop_${cropWidth}x${cropHeight}&`
            if (quality)
                query = query + `quality_${quality}&`
            if (query)
                query = query.slice(0, -1) + "/"
            return mediaRout + query + newSrc
        }, src)
    }
}

//region colors
export const colors = {
    hc9c9c9: "#c9c9c9",
    primary: {
        light: red[500],
        main: red["A700"],
        dark: red[900]
    },
    secondary: {
        light: "#6ea6fd",
        main: "#3278C9",
        dark: "#004d98"
    },
    success: {
        light: green[500],
        main: green["A700"],
        dark: green[900]
    },
    danger: {
        light: red[500],
        main: red["A700"],
        dark: red[900]
    },
    info: {
        light: amber[400],
        main: green["A700"],
        dark: green[700]
    },
    backgroundColor: {
        light: grey["50"],
        main: grey["100"],
        dark: grey["400"],
        darker: grey["600"],
        hf8f8f8: "#f8f8f8",
        heff0ef: "#eff0ef",
        hffffff: "#ffffff",
        ha9a9a9: "#a9a9a9",
        hf2d7dd80: "#f2d7dd80",
    },
    textColor: {
        ha9a9a9: "#a9a9a9",
        h9e9e9e: "#9e9e9e",
        h8d8d8d: "#8d8d8d",
        h757575: "#757575",
        h404040: "#404040",
        h000000: "#000000",
        hffffff: "#ffffff",
        he4254a: "#e4254a",
        h00aa5b: "#00aa5b",
        h3278C9: "#3278C9"
    },
    borderColor: {
        h000000: "#000000",
        hf8f8f8: "#f8f8f8",
        heff0ef: "#eff0ef",
        hebebeb: "#ebebeb",
        he5e5e5: "#e5e5e5",
        hc9c9c9: "#c9c9c9",
        hc5c5c5: "#c5c5c5",
        h9e9e9e: "#9e9e9e",
        ha9a9a9: "#a9a9a9",
        h75757: "#757575",
    }
};

//endregion

export function getPageTitle(title) {
    return (
        lang.get("mehr_takhfif") + (title ? " | " + title : "")
    )
}

export function setPageTitle({title = "", withSiteName = true} = {}) {
    if (withSiteName) {
        title = getPageTitle(title)
    }
    document.title = title;
}

export const GA = new GAnalytics();

