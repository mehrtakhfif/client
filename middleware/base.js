import {isClient} from "../repository";

export const listeners = {
    dispatch: 'dispatch',
};

export const redirectTo = (ctx, href = '/') => {
    if (isClient()) {
        // Router.push(href)
        window.location.href = href
        return
    }
    ctx.res.writeHead(302, {Location: href});
    return ctx.res.end()
};
