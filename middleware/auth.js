import React from "react";
import Actions from "../Actions";
import actions, {userIsLogin} from "../Actions";
import ControllerUser from "../controller/ControllerUser";
import * as Sentry from '@sentry/browser';


export const listeners = {
    onLogin: 'login',
    onLogout: 'logout',
};

//region Helper

export const createDefaultInitialProps = async (WrappedComponent, ctx) => {
    const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx));

    let newDataInjected = {
        user: Actions.User.main({ctx}),
        userIsLogin: Actions.User.isLogin({ctx}),
        basket: Actions.Basket.main({ctx: ctx}),
    };
    if (componentProps && componentProps.cookie) {
        newDataInjected = {
            ...componentProps.dataInjected,
            ...newDataInjected,
        }
    }

    return {
        ...componentProps,
        dataInjected: newDataInjected
    }
};

const createWrapper = (WrappedComponent) => {

    return props => {
        return <WrappedComponent {...props} />
    };
};

export const login = async (dispatch, {username, password, directTo}) => {
    return await ControllerUser.User.login({
        username: username,
        password: password,
    }).then((res) => {
        // if (res.status === 200) {
        //     const basket = getBasket();
        //     if (basket && basket.count > 0) {
        //         try {
        //             const list = [];
        //             _.forEach(basket.data, (p) => {
        //                 list.push(apiHelper.createProductItem({
        //                     storage_id: p.storage_id,
        //                     count: p.count,
        //                     features: p.features
        //                 }))
        //             });
        //             ControllerUser.Basket.multiAdd({products: list}).then(bRes => {
        //                 onUserUpdated(dispatch, {user: res.user});
        //                 setTimeout(() => {
        //                     actions.Basket.checkLoginUserBasketCount(dispatch)
        //                 }, 1000)
        //                 removeLocalBasket(dispatch);
        //             });
        //         } catch (e) {
        //             onUserUpdated(dispatch, {user: res.user});
        //             setTimeout(() => {
        //                 actions.Basket.checkLoginUserBasketCount(dispatch)
        //             }, 1000)
        //             removeLocalBasket(dispatch);
        //         }
        //         tryIt(() => {
        //             GA.User.Auth.login({user_id: res.user.id});
        //         })
        //         return res
        //     } else {
        //         onUserUpdated(dispatch, {user: res.user});
        //         tryIt(() => {
        //             GA.User.Auth.login({user_id: res.user.id});
        //         })
        //         return res
        //     }
        // }
        return res
    });
    // UtilsRouter.goTo({routUrl:'/'});
};

export const onUserUpdated = ( {user}) => {
    // window.localStorage.setItem(baseListeners.dispatch, Utils.stringifyJon({
    //     dispatcher: dispatcher
    // }));
    // window.localStorage.setItem(listeners.onLogin, Date.now());
};

export const logout = () => {
    ControllerUser.User.logout().then(res => {
    });
 
    actions.Basket.updateCount({count: 0})
    // window.localStorage.setItem(baseListeners.dispatch, Utils.stringifyJon({
    //     dispatcher: dispatcher
    // }));
    window.localStorage.setItem(listeners.onLogout, Date.now());
};

export const update = async ({
    firstName = null,
    lastName = null,
    email = null,
    meliCode = null,
    gender = null,
    birthday = null,
    shaba = null,
    subscribe = 1,
}) => {
    if (!userIsLogin())
        return {};
    return await ControllerUser.User.Profile.update({
        firstName: firstName,
        lastName: lastName,
        email: email,
        meliCode: meliCode,
        gender: gender,
        birthday: birthday,
        shaba: shaba,
        subscribe: subscribe,
    }).then((res) => {
        onUserUpdated( {user: res.user});
        return res
    });
};

export const getUpdateUser = async (dispatch, {debugMode = false} = {debugMode: false}) => {
    if (!debugMode && !userIsLogin())
        return {};
    return await ControllerUser.User.Profile.get()[1]().then((res => {
        try {
            Sentry.setUser({"userId": res.user.id});
        } catch (e) {
        }
        onUserUpdated(dispatch, {user: res.user});
        return res
    }))
};

//endregion Helper

//region Container

export const withAuthSync = WrappedComponent => {
    const Wrapper = createWrapper(WrappedComponent);
    Wrapper.getInitialProps = async ctx => await createDefaultInitialProps(WrappedComponent, ctx);
    return Wrapper
};

//region PrivateRout
const createPrivateRoutWrapper = (WrappedComponent) => {
    return props => {
        // const syncLogout = event => {
        //     if (event.key === listeners.onLogout && isClient()) {
        //         Router.push(rout.User.Auth.Login.rout)
        //     }
        // };
        //
        // useEffect(() => {
        //     if (!Actions.User.isLogin() && isClient()) {
        //         Router.push(rout.User.Auth.Login.rout)
        //     }
        //     window.addEventListener('storage', syncLogout);
        //     return () => {
        //         window.removeEventListener('storage', syncLogout);
        //         window.localStorage.removeItem('logout')
        //     }
        // }, []);

        return (
            <WrappedComponent {...props} />
        )
    };
};

export const privateWithAuthSync = WrappedComponent => {
    const Wrapper = createPrivateRoutWrapper(WrappedComponent);

    // Wrapper.getInitialProps = async ctx => {
    //     const token = Actions.User.isLogin({ctx});
    //     if (!token && isServer()) {
    //         try {
    //             return await redirectTo(ctx, rout.User.Auth.Login.rout)
    //         } catch (e) {
    //             gcError("withAuthSync error", e)
    //         }
    //     } else {
    //         return createDefaultInitialProps(WrappedComponent, ctx)
    //     }
    // };
    return Wrapper
};
//endregion PrivateRout

//region logoutRequire

const createLogoutRequireWrapper = (WrappedComponent) => {
    return props => {
        // const syncLogout = event => {
        //     if (event.key === listeners.onLogin && isClient()) {
        //         Router.push(rout.Main.home)
        //     }
        // };
        //
        // useEffect(() => {
        //     if (Actions.User.isLogin() && isClient()) {
        //         Router.push(rout.Main.home);
        //     }
        //     window.addEventListener('storage', syncLogout);
        //     return () => {
        //         window.removeEventListener('storage', syncLogout);
        //         window.localStorage.removeItem('login')
        //     }
        // }, []);
        return (
            <createWrapper>
                <WrappedComponent {...props} />
            </createWrapper>
        )
    };
}

export const logoutRequiredWithAuthSync = WrappedComponent => {
    const Wrapper = createLogoutRequireWrapper(WrappedComponent);
    // Wrapper.getInitialProps = async ctx => {
    //     const token = Actions.User.isLogin({ctx});
    //     if (token) {
    //         try {
    //             return await redirectTo(ctx, rout.Main.home)
    //         } catch (e) {
    //             gcError("withAuthSync error", e)
    //         }
    //     } else {
    //         return createDefaultInitialProps(WrappedComponent, ctx)
    //     }
    // };
    return Wrapper
};
//endregion logoutRequire

//endregion Container
