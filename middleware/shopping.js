import ControllerUser from "../controller/ControllerUser";


export const listeners = {
    onNewProductAdded: 'new-product-added',
    onProductUpdated: 'product-updated',
    onBasketCountUpdated: 'product-basket-count-updated',
    onBasketChange: 'basket-change',
};

export const addProduct = async ({basket_id = null, storage_id, count, features, index}) => {


    return await ControllerUser.Basket.findStorageIndex({
        newStorage: storage_id,
        features: features
    }).then(res => {
        // localStorageUpdate(dispatch, {index: res.data.index, storage_id: storage_id, features: features, count: count});
        return res
    });
};

export const addProduct2 = async ( {storage_id, count = 1}) => {
    return await ControllerUser.Basket.add2({storage_id, count}).then(() => {
    })
}

function localStorageUpdate({index, storage_id, features, count}) {
    // const dispatcher = reduxAddProduct({storage_id: storage_id, count: count, features: features, index: index});
    // try {
    //     dispatch(dispatcher);
    // } catch (e) {
    // }
    // window.localStorage.setItem(baseListeners.dispatch, Utils.stringifyJon({
    //     dispatcher: dispatcher
    // }));
    // window.localStorage.setItem(listeners.onNewProductAdded, "localStorageUpdate-"+Date.now());
    // window.localStorage.setItem(listeners.onBasketChange, "localStorageUpdate-"+Date.now());
}


export const updateProduct = async ({index, basket_id = null, basketproduct_id = null, storage_id, count, features = []}) => {
    return await ControllerUser.Basket.addWithBasketSummaryResponse( {
        basket_id,
        basketproduct_id,
        storage_id,
        count
    }).then((res) => {
        window.localStorage.setItem(listeners.onProductUpdated, "updateProduct-" + Date.now());
        window.localStorage.setItem(listeners.onBasketChange, "updateProduct-" + Date.now());
        return res
    })
};

export const updateBasketCount = ( {count}) => {
    // const dispatcher = reduxUpdateBasketCount({count: count});
    // dispatch(dispatcher);
    // window.localStorage.setItem(baseListeners.dispatch, Utils.stringifyJon({
    //     dispatcher: dispatcher
    // }));
    // window.localStorage.setItem(listeners.onBasketCountUpdated, count);
    // window.localStorage.setItem(listeners.onBasketChange, "updateBasketCount-" + Date.now());
};

export const removeProduct = async ( {basket_id = null, basketproduct_id, storage_id, features = []}) => {
    return ControllerUser.Basket.delete({
        basket_id: basket_id,
        basketproduct_id: basketproduct_id,
        storage_id: storage_id
    }).then(res => {
        // updateBasketCount(dispatch, {count: res.data.products.length});
        return res;
    })
    return
    // if (userIsLogin()) {
    // }
    // const dispatcher = reduxRemoveProduct({storage_id: storage_id, features: features});
    // dispatch(dispatcher);
    // window.localStorage.setItem(baseListeners.dispatch, Utils.stringifyJon({
    //     dispatcher: dispatcher
    // }));
    // window.localStorage.setItem(listeners.onBasketChange, "removeProduct-" + Date.now());
    // return await ControllerUser.Basket.get()
};

export const removeLocalBasket = (dispatch, {defaultCount = null} = {}) => {
    // const dispatcher = reduxRemoveLocalBasket({defaultCount: defaultCount});
    // dispatch(dispatcher);
    // window.localStorage.setItem(baseListeners.dispatch, Utils.stringifyJon({
    //     dispatcher: dispatcher
    // }));

    window.localStorage.setItem(listeners.onBasketChange, "removeLocalBasket-" + Date.now());
};
