import Moment from 'moment'
import momentJalaali from "moment-jalaali";
import {extendMoment} from 'moment-range';
import _ from 'lodash'

const moment = extendMoment(Moment);
export default (() => {
    // Create the methods object
    const methods = {};
    methods.timeStampToDate = (timeStamp = Date.now() / 1000) => {
        return new Date(timeStamp * 1000);
    };

    methods.timeStampToJalali = (timeStamp = Date.now() / 1000) => {
        return momentJalaali(methods.timeStampToDate(timeStamp));
    };

    methods.timeStampToJalaliByFormat = (dateString, format = "jYYYY/jM/jD") => {
        return momentJalaali(dateString, format);
    };

    methods.liveJalali = () => {
        return momentJalaali();
    };

    methods.format = (moment = undefined, format = 'jYYYY/jM/jD') => {
        if (!moment) {
            moment = methods.timeStampToJalali()
        }
        return moment.format(format)
    };

    methods.formatEn = (moment, format = 'YYYY/M/D') => {
        return moment.format(format)
    };

    methods.timestampFormat = (timeStamp, format = 'jYYYY/jM/jD') => {
        return methods.format(momentJalaali(methods.timeStampToDate(timeStamp)), format)
    };

    methods.inRange = (day, startDate, endDate) => {
        const range = moment.range(methods.formatEn(startDate), methods.formatEn(endDate));
        return range.contains(day)
    };

    methods.daysInMonth = (moment) => {
        let firstDay = _.toInteger(moment.startOf('jMonth').format('jD'));
        let endDay = _.toInteger(moment.endOf('jMonth').format('jD'));
        return endDay - firstDay + 1;
    };

    return methods;
})();
