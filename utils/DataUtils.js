import _ from 'lodash';

export default (() => {
    const methods = {};
    methods.TableFilter = () => {
        const tfm = {};
        tfm.filterItemChange = ({filters, item, value}) => {
            const dIndex = _.findIndex(filters, function (o) {
                return o.id === item.id
            });
            if (dIndex === -1)
                return filters;
            const d = filters[dIndex];
            let val = value;
            if (val === null) {
                const t = item.type;
                const ft = filterType;
                val = t === ft.bool ? ft.defaultValue ? ft.defaultValue : false : t === ft.text ? '' : t === ft.multi ? [] : null;
            }
            d.value = val;
            return filters;
        };
        tfm.resetFilter = ({filters}) => {
            const newData = [];
            const ft = filterType;
            _.forEach(filters, (fi) => {
                let newItem = null;

                if (fi.defaultValue) {

                    newItem = {
                        ...fi,
                        value: fi.defaultValue,
                    }
                }

                if (newItem === null) {
                    switch (fi.type) {
                        case ft.bool: {
                            newItem = {
                                ...fi,
                                value: false
                            };
                            break;
                        }
                        case ft.text: {
                            newItem = {
                                ...fi,
                                value: ''
                            };
                            break;
                        }
                        case ft.multi: {
                            newItem = {
                                ...fi,
                                value: []
                            };
                            break;
                        }
                        case ft.single: {
                            newItem = {
                                ...fi,
                                value: null
                            };
                            break;
                        }
                        default: {

                        }
                    }
                }
                if (newItem !== null)
                    newData.push(newItem);
            })
            return newData;
        };
        tfm.convertForSendToServer = ({filters}) => {
            const ft = filterType;
            const fList = {};
            _.forEach(filters, (fi) => {
                switch (fi.type) {
                    case ft.bool: {
                        if (fi.value === null)
                            return;
                        fList[fi.id] = fi.value
                        break
                    }
                    case ft.text: {
                        if (!fi.value || !_.toString(fi.value))
                            break;
                        fList[fi.id] = fi.value;
                        break
                    }
                    case ft.single: {
                        if (!fi.value || !fi.value.id)
                            break;
                        fList[fi.id] = fi.value.id;
                        break
                    }
                    case ft.multi: {
                        if (!fi.value || !_.isArray(fi.value) || _.isEmpty(fi.value))
                            break;
                        const list = [];
                        _.forEach(fi.value, (v) => {
                            if (!v.id)
                                return;
                            list.push(v.id)
                        });
                        fList[fi.id] = list;
                        break
                    }
                    default: {
                        break
                    }
                }
            })

            return fList;
        };
        return tfm;
    }

    return methods;
});

export const filterType = {
    bool: 'bool',
    text: 'text',
    multi: 'multi',
    single: 'single',
}
