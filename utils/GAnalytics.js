import ReactGA from "react-ga";
import {DEBUG} from "../pages/_app";
import * as Sentry from '@sentry/browser';

export const gAnalyticsCode = process.env.REACT_APP_G_ANALYTICS;
export default class {
    constructor() {
        this.user_id = 0;
    }

    init({user_id = 0, ...props}) {
        if (DEBUG)
            return
        this.user_id = user_id;
        ReactGA.initialize(gAnalyticsCode, {
            debug: DEBUG,
            gaOptions: {
                user_id: user_id,
                ...props
            }
        });
    }

    initializePage({pageRout}) {
        if (DEBUG)
            return
        ReactGA.pageview(pageRout);
    }


    /**
     * Event - Add custom tracking event.
     * @param {string} category
     * @param {string} action
     * @param {null|string} label
     * @param {null|number} value
     * @param props
     */
    event({category, action, label = null, value = null, ...props}) {
        if (DEBUG)
            return
        ReactGA.event({
            user_id: this.user_id,
            category: category,
            action: action,
            label: label,
            value: value,
            ...props
        });
    }

    User = {
        main: ({action, label = null, value = null, ...props}) => {
            this.event({category: "User", action: action, ...props})
        },

        Auth: {
            login: ({user_id = null, value = null, ...props}) => {
                this.User.main({action: 'user login', label: `user_id-${user_id}`, value: user_id, ...props})
            },
            logout: ({user_id = null, value = null, ...props}) => {
                this.User.main({action: 'user logout', label: `user_id-${user_id}`, value: user_id, ...props})
            },

            addPassword: ({phone = null, value = null, ...props} = {}) => {
                this.User.main({action: 'user add password', label: phone, value: value, ...props})
            },
            activeCodeSent: ({phone = null, value = null, ...props} = {}) => {
                this.User.main({action: 'user active code sent', label: phone, value: value, ...props})
            },
            resendCode: ({phone = null, value = null, ...props} = {}) => {
                this.User.main({action: 'user resend code', label: phone, value: value, ...props})
            },

            Error: {
                resendCodeTimeout: ({phone = null, value = null, ...props} = {}) => {
                    this.User.main({action: 'user resend code timeout error', label: phone, value: value, ...props})
                },
                ban: ({phone = null, value = null, ...props} = {}) => {
                    this.User.main({action: 'user is ban', label: phone, value: value, ...props})
                },
                verifyMobileError: ({phone = null, value = null, ...props} = {}) => {
                    this.User.main({action: 'user verify mobile Error', label: phone, value: value, ...props})
                },
                loginError: ({phone = null, value = null, ...props} = {}) => {
                    this.User.main({action: 'user login Error', label: phone, value: value, ...props})
                },
            }
        },

        Basket: {
            main: ({action, label = null, value = null, ...props}) => {
                this.event({category: "User-Basket", action: action, ...props})
            },
            addToBasket: ({storage_id, fail = false, ...props}) => {
                let action = this.user_id > 0 ? 'add product to basket' : 'add product to localBasket';
                if (fail)
                    action = this.user_id > 0 ? 'fail add product to basket' : 'fail add product to localBasket';
                this.User.Basket.main({
                    action: action
                    , label: `user-id:${this.user_id}`,
                    value: storage_id,
                    ...props
                })
            }
        }
    };

    Product = {
        main: ({action, label = null, value = null, ...props}) => {
            this.event({category: "Product", action: action, ...props})
        },

        Visit: {
            box: ({boxPermalink, filters = null, fail = false, ...props}) => {
                let action = `visit ${boxPermalink}`;
                if (fail)
                    action = `fail visit ${boxPermalink}`;
                this.Product.main({action: action, label: filters});
            },
            single: ({productPermalink, product_id = null, fail = false, ...props}) => {
                let action = 'visit single page';
                if (fail)
                    action = 'fail visit single page';
                this.Product.main({action: action, label: productPermalink, value: product_id});
            },
        }
    };

    Error = {
        P404: () => {
            this.Error.ReqError(404)
        },
        ReqError: (statusCode = 0,fatal=true) => {
try {
            ReactGA.exception({
                description: `ERROR ${statusCode}: ${window.location.pathname}`,
                fatal: fatal
            });
            Sentry.captureException(new Error(`ReqError => status:${statusCode} location:${window.location.pathname}`));
}catch (e) {
}
        },
        exception: (from, e) => {
            try {
                ReactGA.exception({
                    description: `exception: ${from} e: ${e}`,
                    fatal: true
                });
                Sentry.captureException(new Error(`exception => from:${from} e:${e}`));
            }catch (e) {
            }
        }
    }

}
