import {DEBUG} from "../pages/_app";

function baseLog(type = "log", m) {
    try {
        if (DEBUG)
            console[type](...m)
    } catch (e) {
    }
}

export function gcLogOrg(m1, m2) {
    baseLog("log", [m1, m2])
}

export function gcLog(m1, m2 = "!~~!#@~") {
    let m = [m1]
    if (m2 !== "!~~!#@~")
        m.push(m2)
    baseLog("log", m)
}

export function gcRedLog(m1, m2) {
    console.log("%c" + m1, "color: red", m2)
}

export function gcError(m1, m2) {
    console.error(m1, m2)
}

export function gcInfo(m1, m2) {
    console.info(m1, m2)
}

String.prototype.replaceAll = function (regex, to) {
    return this.replace(regex, to)
};
String.prototype.trimAll = function () {
    return this.replaceAll(/ /g, "").trim()
};



