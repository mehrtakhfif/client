import jwt from "jsonwebtoken";
import md5 from 'md5'
import _ from 'lodash'
import HTMLParser from 'html-react-parser'
import SHA from 'js-sha3';
import Router from "next/router";
import QueryString from "query-string";
import {isClient} from "../repository";


export const UtilsKeyboardKey = {
    ENTER_KEY: 13
};

export const privateKeys = {
    uniqueHSACode: "5jkLTfuPk*HzIddPWKUZnhS6h9px@gDZiWVt615Oq1r&9A^2Y$DD!ydiBa%BUG#bpTXX^V7XWpq",
    uniqueCookieCode: "quL4rXp4MXs#cVhOZzNYy6uMo%e3fFoYX6giJvh@$qRiopXHPceL1%TJCGB3eOdXp$VQUIa7K@k",
};

export const UtilsEncrypt = {
    HSA: {
        encode: function (key, value) {
            return jwt.sign(value, key + privateKeys.uniqueHSACode, {algorithm: 'HS256'});
        },
        decode: function (key, value) {
            return jwt.verify(value, key + privateKeys.uniqueHSACode)
        }
    },
    SHA: {
        S224: function (value) {
            return SHA.sha3_224(value);
        }
    },
    MD5: function (val) {
        return md5(val)
    }
};

export const UtilsImage = {
    getImageBrightness: function (imageSrc, callback) {
        try {
            const fuzzy = 0.1;
            const img = document.createElement("img");
            img.src = imageSrc;
            img.style.display = "none";
            img.crossOrigin = "Anonymous";
            document.body.appendChild(img);
            img.onload = function () {
                // create canvas
                const canvas = document.createElement("canvas");
                canvas.width = this.width;
                canvas.height = this.height;

                const ctx = canvas.getContext("2d");
                ctx.drawImage(this, 0, 0);

                const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                const data = imageData.data;
                let r, g, b, max_rgb;
                let light = 0, dark = 0;

                for (let x = 0, len = data.length; x < len; x += 4) {
                    r = data[x];
                    g = data[x + 1];
                    b = data[x + 2];

                    max_rgb = Math.max(Math.max(r, g), b);
                    if (max_rgb < 128)
                        dark++;
                    else
                        light++;
                }

                const dl_diff = ((light - dark) / (this.width * this.height));
                if (dl_diff + fuzzy < 0)
                    callback(true); /* Dark. */
                else
                    callback(false);  /* Not dark. */
            }
        } catch (err) {

        }
    }
};

export const UtilsDataConverter = {
    specialProductConvertToProduct: function (props) {
        if (props === undefined)
            return;
        if (_.isArray(props)) {
            let newProps = [];
            try {
                _.forEach(props, function (product) {
                    newProps.push(UtilsDataConverter.specialProductConvertToProduct(product));
                });
                return newProps;
            } catch (err) {
                return []
            }
        }
        try {
            let item = props.storage;
            if (props.media && props.media.type === 'image') {
                item.product.thumbnail = props.media.file;
            }
            if (!_.isEmpty(props.title)) {
                item.product.name = props.title;
            }
            return item
        } catch (err) {
            return null
        }
    },
};

export const UtilsDate = {
    minuteToSecond: (min) => {
        return min * 60
    },
    secondToMillisecond: (second) => {
        return second * 1000
    },
    minuteToMillisecond: (min) => {
        return UtilsDate.secondToMillisecond(UtilsDate.minuteToSecond(min))
    },
    millisecondToSecond: (ms) => {
        return ms / 1000
    },
    secondToMinute: (second) => {
        return second / 60
    },
    millisecondToMinute: (ms) => {
        return UtilsDate.secondToMinute(UtilsDate.millisecondToSecond(ms))
    },
};

export const UtilsFormat = {
    numberToMoney: (props) => {
        if (!props && props !== 0)
            return;
        return props.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        // return true
    },
    phoneNumberWithSpace: (n, splitter = " ") => {
        try {
            if (n.length < 4) {
                throw ""
            }
            return n.substring(0, 4) + splitter + n.substring(4, 7) + splitter + n.substring(7, 11)
        } catch (e) {
            return n
        }
    }
};

export const UtilsConverter = {

    addRem: (rem, rem2) => {
        const match1 = rem.toString().match(/\d+/g);
        const match2 = rem2.toString().match(/\d+/g);
        const num1 = parseFloat(match1[0] + ((match1[1]) ? '.' + match1[1] : ''));
        const num2 = parseFloat(match2[0] + ((match2[1]) ? '.' + match2[1] : ''));
        return (num1 * num2) + 'rem'
    }
};

export const UtilsRouter = {
    redirectRout: (props, defaultRout) => {
        if (!(props && ((props.location && props.location.state && props.location.state.from) || _.isString(props)))) {
            return {
                from: {
                    pathname: defaultRout
                }
            }
        }
        return {
            from: _.isString(props) ? {pathname: props} : props.location.state.from
        }
    },
    redirectRoutByWindow: (defaultRout) => {
        if (!window)
            return;
        try {
            return {
                from: {
                    pathname: window.location.pathname,
                    search: window.location.search
                }
            }
        } catch (e) {
            return {
                from: {
                    pathname: defaultRout
                }
            }
        }
        // return {
        //     from: _.isString(props) ? {pathname: props} : props.location.state.from
        // }
    },
    goTo: ({routUrl, props = {}}) => {
        try {
            return Router.push(routUrl)
        } catch (e) {
        }
    }
};

export const UtilsStyle = {
    transition: (duration = 500) => {
        return {
            'WebkitTransition': 'all ' + duration + 'ms ease',
            'MozTransition': 'all ' + duration + 'ms ease',
            'MsTransition': 'all ' + duration + 'ms ease',
            'OTransition': 'all ' + duration + 'ms ease',
            transition: 'all ' + duration + 'ms ease'
        }
    },
    borderRadius: (radius) => {
        return {
            borderRadius: radius,
            WebkitBorderRadius: radius,
            MozBorderRadius: radius,
        }
    },
    widthFitContent: () => {
        return {
            width: 'max-content',
            whiteSpace: 'nowrap'
        }
    },
    disableTextSelection: () => {
        return {
            WebkitTouchCallout: 'none',
            WebkitUserSelect: 'none',
            KhtmlUserSelect: 'none',
            MozUserSelect: 'none',
            MsUserSelect: 'none',
            userSelect: 'none'
        }
    }
};

export const ScrollToRef = (ref) => {
    if (!window)
        return;
    window.scrollTo(0, ref.current.offsetTop - 250)
};


export const UtilsParser = {
    html: (string) => {
        try {
            return HTMLParser(string)
        } catch {
            return string
        }
    }
};

export function _delete(obj, prop) {
    if (_.isObject(obj)) {
        let newObj = {};
        _.forEach(obj, function (objItem, key) {
            if (_.isString(prop)) {
                if (prop !== key) {
                    newObj[key] = objItem;
                }
                return
            }
            if (_.isArray(prop)) {
                _.forEach(prop, function (prop) {
                    if (prop !== key) {
                        newObj[key] = prop;
                    }
                });
                return
            }
            newObj[key] = prop;
        });
        return newObj;
    }
    return obj
}


export const SSRUtils = {
    redirect: (res, redirectTo = '/p404') => {
        if (!res)
            return;
        res.writeHead(301, {
            Location: redirectTo
        });
        res.end();
    }
}


export const showNotification = ({title, body, icon = "https://api.mehrtakhfif.com/static/mtIcon.png"}) => {

    const options = {
        body: body,
        icon: icon,
        dir: "rtl"
    };
    new Notification(title, options)
}


export const Utils = {
    getQueryFromUrl: url => {
        if (_.isObject(url) && url.history && url.history.state) {
            url = url.history.state.as || url.history.state;
        }
        if (!_.isString(url))
            return {};
        const a = url.match("([^\\?]+)\\?(.*)")
        if (!a || _.isEmpty(a) || !_.isArray(a) || !a[2])
            return {};
        return QueryString.parse('?' + a[2])
    },
    getQueryFromWindows: () => {
        if (!isClient())
            return {};
        return Utils.getQueryFromUrl(window)
    },
    copyToClipboard: str => {
        const el = document.createElement('textarea');  // Create a <textarea> element
        el.value = str;                                 // Set its value to the string that you want copied
        el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
        el.style.position = 'absolute';
        el.style.left = '-9999px';                      // Move outside the screen to make it invisible
        document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
        const selected =
            document.getSelection().rangeCount > 0        // Check if there is any content selected previously
                ? document.getSelection().getRangeAt(0)     // Store selection if found
                : false;                                    // Mark as false to know no selection existed before
        el.select();                                    // Select the <textarea> content
        document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
        document.body.removeChild(el);                  // Remove the <textarea> element
        if (selected) {                                 // If a selection existed before copying
            document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
            document.getSelection().addRange(selected);   // Restore the original selection
        }
    },
    disableScrolling: () => {
        document.getElementsByTagName("html")[0].style.overflow = 'hidden'
    },
    enableScrolling: () => {
        document.getElementsByTagName("html")[0].style.overflow = 'auto'
    },
    isIos: () => {
        var iDevices = [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod'
        ];

        if (!!navigator.platform) {
            while (iDevices.length) {
                if (navigator.platform === iDevices.pop()) {
                    return true;
                }
            }
        }

        return false;
    },
    isAndroid: () => {
        return navigator.userAgent.toLowerCase().indexOf("android") > -1;
    },
    insert: (arr, index, newItem) => [
        ...arr.slice(0, index),
        newItem,
        ...arr.slice(index)
    ],
    stringifyJon: (obj) => {
        return JSON.stringify(obj, function (key, value) {
            if (typeof value === "function") {
                return "/Function(" + value.toString() + ")/";
            }
            return value;
        });
    },
    parsJson: (json) => {
        return JSON.parse(json, function (key, value) {
            if (typeof value === "string" &&
                value.startsWith("/Function(") &&
                value.endsWith(")/")) {
                value = value.substring(10, value.length - 2);
                return eval("(" + value + ")");
            }
            return value;
        });
    },
    uniqId: () => {
        const navigator_info = window.navigator;
        const screen_info = window.screen;
        let uid = navigator_info.mimeTypes.length;
        uid += navigator_info.userAgent.replace(/\D+/g, '');
        uid += navigator_info.plugins.length;
        uid += screen_info.height || '';
        uid += screen_info.width || '';
        uid += screen_info.pixelDepth || '';
        return uid
    }
};
export default Utils;
