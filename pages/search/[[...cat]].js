import React, {useCallback, useEffect, useMemo, useState} from "react";
import {isClient} from "../../repository";
import {Box, getSafe, HiddenLgUp, HiddenMdDown, tryIt} from "material-ui-helper";
import {useRouter} from "next/router";
import _ from "lodash"
import rout from "../../router";
import SearchFilter from "../../component/search/filter/SearchFilter";
import ControllerProduct from "../../controller/ControllerProduct";
import useSWR, {mutate} from "swr";
import ActiveFilters from "../../component/search/filter/activeFilters/ActiveFilters";
import ResultContainer from "../../component/search/result/ResultContainer";
import BasePage from "../../component/base/BasePage";
import ResultIsEmpty from "../../component/search/result/resultIsEmpty/ResultIsEmpty";
import SmHeader from "../../component/search/filter/smHeader/SmHeader";
import SmFilter from "../../component/search/smFilter/SmFilter";
import FilterDataContext from "../../component/search/context/FilterDataContext";
import ParamsContext from "../../component/search/context/ParamsContext";
import Seo from "../../component/search/Seo";
import CustomHeader, {HeaderItem} from "../../component/header/customHeader/CustomHeader";
import {DEBUG} from "../_app";
import Ordering from "../../component/search/filter/Ordering";
import {Category} from "@material-ui/icons";
import CategoryDetails from "../../component/search/CategoryDetails";
import ShowMore from "../../component/base/ShowMore";
import Typography from "../../component/base/Typography";
import {UtilsParser} from "../../utils/Utils";

/*
 **Add new filter helper**
 Add key to filterItemsKey
 Add name to filterItemName
 Add key to rout.Product.Search
 Add component to SearchFilter.js
 Check ActiveFilter.js
*/

export const filterItemsKey = {
    brands: "brands",
    colors: "colors",
    price: "price",
    ordering: "o",
    cat: "cat",
    query: "q",
    type: "type"
}
export const filterItemName = {
    cat: "category",
    b: "filter_box_title",
    colors: "filter_colors_title",
    brands: "filter_brands_title",
    price: "pricing_range",
    q: "search",
    o: "ordering",
    type: "type"
}

export const orderingItems = {
    newest: {
        key: "newest",
        title: "most_newest"
    },
    price: {
        key: "price",
        title: "most_cheapest"
    },
    '-price': {
        key: "-price",
        title: "most_expensive"
    },
    best_seller: {
        key: "best_seller",
        title: "most_bestseller"
    },
    // popular: {
    //     key: "popular",
    //     title: "most_popular"
    // },
    discount: {
        key: "discount",
        title: "most_discount"
    }
}

export const typeItems = {
    product: "product",
    service: "service"
}

export const defaultOrderingItem = orderingItems.newest;

function Search({initialData, initialFilterData, ...params}) {
    const {query} = useRouter();
    const [loading, setLoading] = useState(false);
    const [filterDataBackup, setFilterDataBackup] = useState(undefined);

    if (_.isEmpty(query)) {
        return (
            <BasePage
                name="Search">
                <Box
                    pt={3}
                    px={{
                        xs: 2,
                        md: 10
                    }}>
                    <ResultIsEmpty/>
                </Box>
            </BasePage>)
    }

    //region filterData
    const [filterKey, filterReq] = useMemo(() => ControllerProduct.Query.V2.getFilters(query), [query])

    const {data: filterData, error: filterDataError} = useSWR(filterKey, filterReq, {
        initialData: initialFilterData,
        revalidateOnFocus: false,
        dedupingInterval: 480000,// 8 Min,
        onSuccess: () => {
            setLoading(false)
        }
    })
    //endregion filterData

    //region useEffect
    useEffect(() => {
        if (filterData)
            setFilterDataBackup(filterData)
    }, [filterData]);
    //endregion useEffect

    //region handler
    const handleLoadingStart = useCallback(() => {
        setLoading(true)
    }, [loading, setLoading]);
    const handleLoadingFinish = useCallback(() => {
        setLoading(false)
    }, [loading, setLoading]);
    const mutateFilterData = useCallback(() => {
        return mutate(filterKey)
    }, [filterKey]);
    //endregion handler

    return (
        <BasePage
            name="Search"
            setting={{
                header: {
                    showSm: false
                },
                footer: {
                    showSm: false,
                }
            }}>
            <FilterDataContext.Provider
                value={[
                    filterData || filterDataBackup,
                    filterDataError,
                    mutateFilterData
                ]}>
                <ParamsContext.Provider
                    value={{
                        loading: [loading, handleLoadingStart, handleLoadingFinish, setLoading],
                    }}>
                    <Box flexDirection={'column'} pb={5}>
                        <HiddenLgUp>
                            <CustomHeader
                                title={DEBUG ? "متن تستی" : undefined}
                                menus={[
                                    HeaderItem.Search,
                                    HeaderItem.Basket
                                ]}
                                backable={true}/>
                        </HiddenLgUp>
                        <Seo/>
                        <SmHeader/>
                        <Box
                            pt={{
                                xs: 1,
                                lg: 3
                            }}
                            px={{
                                xs: 0,
                                sm: 2,
                                lg: 10
                            }}>
                            <HiddenMdDown>
                                <SearchFilter/>
                            </HiddenMdDown>
                            <Box flex={1} pt={1} flexDirectionColumn={true}>
                                <ActiveFilters/>
                                <Ordering/>
                                <ResultContainer initialData={initialData}/>
                            </Box>
                        </Box>
                        <SmFilter/>{filterData?.description &&
                    <Box
                        pt={{xs:4,md:10,}}
                        pb={{md:5,xs:2}}
                        px={{md:30,xs:1}}
                        flexDirectionColumn
                    >
                        {/*<Typography variant={'h3'}>{filterData?.breadcrumb.slice(-1)[0]?.name}</Typography>*/}
                        <ShowMore acceptableHeight={200}>
                            <Typography variant={'body'} style={{lineHeight: '24px',display:'block !important'}}>
                                {UtilsParser.html(filterData?.description)}
                            </Typography>
                        </ShowMore>
                    </Box>
                    }
                    </Box>
                </ParamsContext.Provider>
            </FilterDataContext.Provider>
        </BasePage>
    )
}

Search.getInitialProps = async function ({query, ...ctx}) {
    // const {query, asPath, router} = ctx
    if (isClient()) {
        return __NEXT_DATA__.props;
    }

    try {
        if (DEBUG)
            throw ""
        const data = await Promise.all([
            ControllerProduct.Query.V2.query(query)[1](),
            ControllerProduct.Query.V2.getFilters(query)[1](),
        ])

        return {initialData: data[0], initialFilterData: data[1]}
    } catch (e) {
        return {}
    }
};

export default Search;

//region onFilterChange

export function onFilterChange(
    router,
    {
        category,
        filterKey,
        value,
        remove = false
    }, qu) {

    setTimeout(() => {
        const query = qu || _.cloneDeep(router.query) || {};
        if (filterKey) {
            const generateQuery = (value) => {
                value = _.toString(value);
                query[filterKey] = tryIt(() => {
                    if (!query[filterKey])
                        throw ""
                    const p = _.cloneDeep(query)
                    if (_.isArray(p[filterKey])) {
                        if (remove) {
                            p[filterKey] = p[filterKey].filter(i => i !== value)
                        } else {
                            p[filterKey].push(value)
                        }
                        return _.uniq(p[filterKey])
                    }
                    if (remove)
                        return []
                    return _.uniq([p[filterKey], value])
                }, remove ? undefined : [value])
            }

            if (_.isArray(value)) {
                _.forEach(value, val => {
                    generateQuery(val)
                })
            } else {
                generateQuery(value)
            }

        }

        const params = {}
        _.forEach(query, (val, key) => {
            if (_.isEmpty(key) || _.isEmpty(val))
                return
            params[key] = val
        })


        switch (filterKey) {
            case filterItemsKey.price: {
                if (remove) {
                    delete params[filterItemsKey.price];
                    break;
                }
                params[filterItemsKey.price] = value
                break;
            }
        }

        const cat = getSafe(() => {
            if (_.isUndefined(category)) {
                throw ""
            }
            if (_.isNull(category))
                return ""
            return `${category}`
        }, query[filterItemsKey.cat] ? `${query[filterItemsKey.cat]}` : "")
        delete params[filterItemsKey.cat];


        router.push(...rout.Product.Search.create({
            ...params,
            category: cat
        }))
    }, 400)
}


//endregion onFilterChange
