import React, {useState} from "react";
import DebugRout from "../lib/wrappedcomponent/DebugRout";
import BasePage from "../component/base/BasePage";
import {Box, Button, DefaultTextField} from "material-ui-helper";
import axios from "axios";


export default function () {
    const [url, setUrl] = useState("")
    const [data, setData] = useState({})


    return (
        <DebugRout>
            <BasePage
                name="cross"
                setting={{
                    header: {
                        showLg: false,
                        showSm: false
                    },
                    footer: {
                        showLg: false,
                        showSm: false
                    },
                }}>
                <Box dir={'ltr'} px={5} py={2}>
                    <DefaultTextField
                        name={"url"}
                        label={"url"}
                        variant={"outlined"}
                        onChange={(e, url) => {
                            setUrl(url)
                        }}/>
                    <Box pr={2}>
                        <Button
                            onClick={() => {
                                axios.get(url).then((res)=>setData(res.data))
                            }}>
                            Request
                        </Button>
                    </Box>
                </Box>
                <Box dir={'ltr'}>
                    {JSON.stringify(data)}
                </Box>
            </BasePage>
        </DebugRout>
    )
}