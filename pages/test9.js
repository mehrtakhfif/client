import React, {useEffect, useState} from "react";
import {Box, Button} from "material-ui-helper";
import Img from "../component/base/Img";


const style = {
    objectFit: "contain",
    width: "100%",
    height: "auto",
    zIndex: 5
}

export default function Test9() {

    const [show, setShow] = useState(0)

    useEffect(() => {

    }, [])

    useEffect(()=>{
        if (show>=2)
            return
        setTimeout(() => {
            setShow(show=>show+1)
        }, 3000)
    },[show])

    return (
        <Box flexDirection={"column"}>
            <Box width={"25%"}>
                <Button onClick={() => {
                }}>
                    cahnge
                </Button>
            </Box>
            <Box width={"100%"}>
                <Box width={"25vw"} height={200}>
                    content
                </Box>
                <Box width={"75vw"} flexWrap={"wrap"} alignItems={"start"}>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                    <Item show={show}/>
                </Box>
            </Box>
        </Box>
    )
}

function Item({show}){
    return(
        <Box width={"18.75vw"} p={1}>
            <Img
                alt="1"
                vw={17.75}
                imageWidth={600}
                imageHeight={372}
                src={show>=2 ? "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/thumbnail/11-29-13-65-has-ph.jpg" : undefined}
                placeholderSrc={show >=1?"https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/thumbnail/11-29-13-65-ph.jpg":undefined}/>
        </Box>
    )
}