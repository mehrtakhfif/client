import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import {useRouter} from "next/router";
import rout, {routIsExact} from "../../router";
import {SmHidden} from "../../component/header/Header";
import {UtilsStyle} from "../../utils/Utils";
import StickyBox from "react-sticky-box";
import ProfileMenu from "../../component/profile/ProfileMenu";
import ProfileCm from "../../component/profile/Profile";
import AllOrder from "../../component/profile/AllOrder";
import Wishlist from "../../component/profile/Wishlist";
import CommentAndReview from "../../component/profile/CommentAndReview";
import Addresses from "../../component/profile/Addresses";
import UserDetails from "../../component/profile/UserDetails";
import {userIsLogin} from "../../Actions";
import {getPageTitle, lang} from "../../repository";
import Head from "next/head";
import LoginRequiredRout from "../../lib/wrappedcomponent/LoginRequiredRout";
import ErrorBoundary from "../../component/base/ErrorBoundary";
import PleaseWait from "../../component/base/loading/PleaseWait";
import BasePage from "../../component/base/BasePage";
import {useUserContext} from "../../context/UserContext";


function Profile({...props}) {

    return (
        <LoginRequiredRout>
            <BasePage
                name="profile">
                <ProfilePage/>
            </BasePage>
        </LoginRequiredRout>
    )
}

export default React.memo(Profile)


function ProfilePage({...props}) {
    const router = useRouter();
    const page = router.query.page;
    const isLogin = userIsLogin();
    const [user,refreshUser] = useUserContext();

    //region routChecker
    function isProfilePage() {
        return routIsExact(page[0], rout.User.Profile.Main.Params.Profile.param);
    }

    function isAllOrderPage() {
        return routIsExact(page[0], rout.User.Profile.Main.Params.AllOrder.param);
    }

    function isWishlistPage() {
        return routIsExact(page[0], rout.User.Profile.Main.Params.Wishlist.param);
    }

    function isCommentsAndReviewPage() {
        return routIsExact(page[0], rout.User.Profile.Main.Params.CommentsAndReview.param);
    }

    function isAddressesPage() {
        return routIsExact(page[0], rout.User.Profile.Main.Params.Addresses.param);
    }

    function isUserInfoPage() {
        return routIsExact(page[0], rout.User.Profile.Main.Params.UserInfo.param);
    }

    //endregion routChecker

    function a11yProps(index) {
        return {
            id: `scrollable-auto-tab-${index}`,
            'aria-controls': `scrollable-auto-tabpanel-${index}`,
        };
    }

    useEffect(() => {
        if (user?.username)
            return
        refreshUser()
        // getUpdateUser(dispatch).finally(() => {
        // })
    }, [isLogin])


    return (
        <LoginRequiredRout>
            <Box display={'flex'} mt={5} mb={3}
                 flexDirection={{
                     xs: 'column',
                     md: 'row'
                 }}>
                <Head>
                    <title>{getPageTitle(lang.get("profile"))}</title>
                    {/*<description>ویرایش پروفایل</description>*/}
                </Head>
                {(userIsLogin() && user && user.username) ?
                    <React.Fragment>
                        <SmHidden mdDown={true}>
                            <Box
                                px={2}
                                style={{
                                    ...UtilsStyle.widthFitContent()
                                }}>
                                <StickyBox offsetTop={150} offsetBottom={10}>
                                    <ProfileMenu
                                        user={user}
                                        activeProfileKey={page[0]}/>
                                </StickyBox>
                            </Box>
                        </SmHidden>
                        <Box
                            flex={{
                                xs: 'unset',
                                md: 1
                            }}
                            width={{
                                xs: 1,
                                md: 'auto'
                            }}
                            pt={{
                                xs: 2,
                                md: 0
                            }}
                            pr={2}
                            pl={{
                                xs: 2,
                                md: 3
                            }}>
                            <React.Fragment>
                                {isAllOrderPage() ?
                                    <AllOrder {...props}/> :
                                    isWishlistPage() ?
                                        <Wishlist/> :
                                        isCommentsAndReviewPage() ?
                                            <CommentAndReview/> :
                                            isAddressesPage() ?
                                                <Addresses/> :
                                                isUserInfoPage() ?
                                                    <UserDetails user={user}/> :
                                                    <ErrorBoundary>
                                                        <ProfileCm user={user}/>
                                                    </ErrorBoundary>
                                }
                            </React.Fragment>
                        </Box>
                    </React.Fragment> :
                    <PleaseWait/>}
            </Box>
        </LoginRequiredRout>
    )
}

