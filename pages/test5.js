import React, {useEffect} from "react";
import {Box, useState, UtilsStyle} from "material-ui-helper";
import Img from "../component/base/Img";
import {lang, media} from "../repository";
import {useRouter} from "next/router";
import Link from "../component/base/link/Link";
import ButtonLink from "../component/base/link/ButtonLink";
import Typography from "../component/base/Typography";
import _ from "lodash";
import moment from "moment";
import {useTheme} from "@material-ui/core";
import {containerSpace} from "../component/homeV2/specialOffer/SpecialOffer";
import Card from "@material-ui/core/Card";
import {Swiper, SwiperSlide} from "swiper/react";
import {grey} from "@material-ui/core/colors";
import {UtilsFormat} from "../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import Icon from "../component/base/Icon";
import Hidden from "@material-ui/core/Hidden";
import HomeSlider from "../component/homeV2/HomeSlider";
import BaseSlider from "../component/base/slider/Slider"
import DebugRout from "../lib/wrappedcomponent/DebugRout";

export default function Test5() {


    return (
        <DebugRout>
            <Box width={1} flexDirection={'column'}>
                <HomeSlider/>
                <SS/>
            </Box>
        </DebugRout>
    )
}


function SS({
                label = lang.get("special_discounts_today"),
                seeMoreLabel = lang.get("see_all"),
                seeMoreLink,
                ...props
            }) {

    const router = useRouter();

    return (
        <React.Fragment>
            <Hidden mdDown={true}>
                <Box
                    width={1}
                    display={'flex'}
                    flexDirection={'column'}
                    minHeight={400}
                    alignItems={'center'}
                    style={{
                        background: `url(/drawable/image/specialOffer.jpg)`,
                        backgroundRepeat: 'no-repeat'
                    }}>
                    <Box display={'flex'} width={1}
                         pt={'2.667vw'}
                         px={'7.5vw'}
                         alignItems={'center'}>
                        <Box width={1 / 3}>
                            <Link
                                href={seeMoreLink} hoverColor={'#fff'}
                                pb={3} variant={"h4"} color={'#fff'}
                                style={{
                                    ...UtilsStyle.widthFitContent()
                                }}>
                                {lang.get("special_discounts_today")}
                            </Link>
                            {seeMoreLink &&
                            <ButtonLink
                                variant={'outlined'}
                                href={seeMoreLink}
                                prefetch={true}
                                style={{
                                    borderColor: "#fff",
                                }}>
                                <Typography py={0.5} variant={"body2"} color={"#fff"}>
                                    {lang.get("see_all")}
                                </Typography>
                            </ButtonLink>}
                        </Box>
                        <Box width={2 / 3} pl={'4.167vw'}>
                            <Counter/>
                        </Box>
                    </Box>
                    <Slider/>
                </Box>
            </Hidden>
        </React.Fragment>
    )
}


function Counter({...props}) {

    const [timer, setTimer] = useState({
        hours: undefined,
        minutes: undefined,
        seconds: undefined
    })


    function serializeTime(val) {
        const v = _.toString(val);
        const a = v.length === 1 ? '0' + v : v;
        return a > 0 ? a : '00'
    }

    useEffect(() => {
        const interval = setInterval(() => {
            const du = moment.duration((1611070001 - moment().unix()) * 1000 - interval, 'milliseconds')
            setTimer({
                hours: du.hours(),
                minutes: du.minutes(),
                seconds: du.seconds()
            })
        }, 1000)
        return () => {
            clearInterval(interval)
        }
    }, [])

    return (
        <Box display={'flex'} justifyContent={'flex-end'} dir={'ltr'}>
            {
                !_.isUndefined(timer.hours) &&
                <React.Fragment>
                    <Box display={'flex'} justifyContent={'center'} minWidth={80} fontSize={57} fontWeight={500}
                         color={'#fff'}>
                        {serializeTime(timer.hours)}
                    </Box>
                    <Box px={1} fontSize={57} fontWeight={500} color={'#fff'}>
                        :
                    </Box>
                    <Box display={'flex'} justifyContent={'center'} minWidth={80} fontSize={57} fontWeight={500}
                         color={'#fff'}>
                        {serializeTime(timer.minutes)}
                    </Box>
                    <Box px={1} fontSize={57} fontWeight={500} color={'#fff'}>
                        :
                    </Box>
                    <Box display={'flex'} justifyContent={'center'} minWidth={80} fontSize={57} fontWeight={500}
                         color={'#fff'}>
                        {serializeTime(timer.seconds)}
                    </Box>
                </React.Fragment>
            }
        </Box>
    )
}

export const productData = [
    {
        id: 1,
        title: "تیشرت‌های باشگاهی و ملی",
        link: '/test',
        items: [
            {
                id: 1,
                title: 'گوشی موبایل سامسونگ مدل Galaxy A01 SM-A015F/DS دو سیم کارت ظرفیت 16 گیگابایت گوشی موبایل سامسونگ مدل Galaxy A01 SM-A015F/DS دو سیم کارت ظرفیت 16 گیگابایت',
                thumbnail: {
                    title: 'پولوشرت منچوستر',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/2.jpg`,
                    square: true
                },
                final_price: 80000000,
                discount_price: 67000000,
                discount_percent: 30,
                permalink: 'cat-1',
            },
            {
                id: 2,
                title: 'پولوشرت یونتوس',
                thumbnail: {
                    title: 'پولوشرت یونتوس',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/1.jpg`,
                    square: true
                },
                final_price: 70000,
                discount_price: 62000,
                discount_percent: 23,
                permalink: 'cat-2',
            },
            {
                id: 3,
                title: 'بهار نارنج',
                thumbnail: {
                    title: 'بهار نارنج',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/3.jpg`,
                    square: true
                },
                final_price: 40000,
                discount_price: 40000,
                discount_percent: 0,
                permalink: 'cat-3',
            },
            {
                id: 4,
                title: 'دسته گل رز',
                thumbnail: {
                    title: 'دسته گل رز',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/4.jpg`,
                    square: true
                },
                final_price: 5000000,
                discount_price: 4000000,
                discount_percent: 2,
                permalink: 'cat-4',
            },
            {
                id: 5,
                title: 'کیف 1',
                thumbnail: {
                    title: 'کیف 1',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/5.jpg`,
                    square: true
                },
                final_price: 180000,
                discount_price: 160000,
                discount_percent: 10,
                permalink: 'cat-5',
            },
            {
                id: 6,
                title: 'کیف 2',
                thumbnail: {
                    title: 'کیف 2',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/6.jpg`,
                    square: true
                },
                final_price: 160000,
                discount_price: 160000,
                discount_percent: 0,
                permalink: 'cat-6',
            },
            {
                id: 7,
                title: 'کیف 3',
                thumbnail: {
                    title: 'کیف 3',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/7.jpg`,
                    square: true
                },
                final_price: 3200000,
                discount_price: 2900000,
                discount_percent: 21,
                permalink: 'cat-7',
            },
            {
                id: 8,
                title: 'کیف 4',
                thumbnail: {
                    title: 'کیف 4',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/8.jpg`,
                    square: true
                },
                final_price: 4800000,
                discount_price: 4300000,
                discount_percent: 41,
                permalink: 'cat-8',
            },
        ]
    },
    {
        id: 2,
        title: "ویلا",
        link: '/test2',
        items: [
            {
                id: 1,
                title: 'گوشی موبایل سامسونگ مدل Galaxy A01 SM-A015F/DS دو سیم کارت ظرفیت 16 گیگابایت گوشی موبایل سامسونگ مدل Galaxy A01 SM-A015F/DS دو سیم کارت ظرفیت 16 گیگابایت',
                thumbnail: {
                    title: 'پولوشرت منچوستر',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f1.jpg`,
                    square: false
                },
                final_price: 80000000,
                discount_price: 67000000,
                discount_percent: 30,
                permalink: 'cat-1',
            },
            {
                id: 2,
                title: 'پولوشرت یونتوس',
                thumbnail: {
                    title: 'پولوشرت یونتوس',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f2.jpg`,
                    square: false
                },
                final_price: 70000,
                discount_price: 62000,
                discount_percent: 23,
                permalink: 'cat-2',
            },
            {
                id: 3,
                title: 'بهار نارنج',
                thumbnail: {
                    title: 'بهار نارنج',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f3.jpg`,
                    square: false
                },
                final_price: 40000,
                discount_price: 40000,
                discount_percent: 0,
                permalink: 'cat-3',
            },
            {
                id: 4,
                title: 'دسته گل رز',
                thumbnail: {
                    title: 'دسته گل رز',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f4.jpg`,
                    square: false
                },
                final_price: 5000000,
                discount_price: 4000000,
                discount_percent: 2,
                permalink: 'cat-4',
            },
            {
                id: 5,
                title: 'کیف 1',
                thumbnail: {
                    title: 'کیف 1',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f5.jpg`,
                    square: false
                },
                final_price: 180000,
                discount_price: 160000,
                discount_percent: 10,
                permalink: 'cat-5',
            },
            {
                id: 6,
                title: 'کیف 2',
                thumbnail: {
                    title: 'کیف 2',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f6.jpg`,
                    square: false
                },
                final_price: 160000,
                discount_price: 160000,
                discount_percent: 0,
                permalink: 'cat-6',
            },
            {
                id: 7,
                title: 'کیف 3',
                thumbnail: {
                    title: 'کیف 3',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f7.jpg`,
                    square: false
                },
                final_price: 3200000,
                discount_price: 2900000,
                discount_percent: 21,
                permalink: 'cat-7',
            },
            {
                id: 8,
                title: 'کیف 4',
                thumbnail: {
                    title: 'کیف 4',
                    image: `${process.env.REACT_APP_API}/media/redesign/thumbnail/f8.jpg`,
                    square: false
                },
                final_price: 4800000,
                discount_price: 4300000,
                discount_percent: 41,
                permalink: 'cat-8',
            },
        ]
    },
]

function Slider({...props}) {
    const theme = useTheme()
    return (
        <Box width={1} flexDirection={'column'}>
            <Box px={containerSpace}>
                <Box component={Card}
                     elevation={3}
                     width={1}
                     pt={'6.146vw'}
                     pb={'3.5vw'}>
                    <BaseSlider
                        scrollbar={true}
                        sliderKey={"test"}
                        items={productData[0].items}
                        breakpoints={{
                            0: {
                                slidesPerView: 2,
                                freeMode: true,
                            },
                            678: {
                                slidesPerView: 3,
                            },
                            1280: {
                                slidesPerView: 3.5,
                                slidesPerGroup: 3,
                                navigation: {
                                    nextEl: '.special-product-slider-next',
                                    prevEl: '.special-product-slider-prev',
                                }
                            },
                        }}
                        style={{
                            paddingBottom: '2.8vw'
                        }}
                        render={(pr, i) => {
                            const iPlus = i + 1;
                            return (
                                <Link href={`/${pr.id}`} hasHover={false}>
                                    <ProductCard
                                        item={pr}
                                        style={{
                                            borderLeft: (iPlus % 4 !== 0 && iPlus < productData.length) ? `1px solid ${grey[200]}` : undefined
                                        }}/>
                                </Link>
                            )
                        }}/>
                </Box>
            </Box>
            <Box pt={8} width={1}>
                <Box display={'flex'} flexDirection={'column'} position={'relative'}
                     px={containerSpace}
                     width={1}
                     className={'base-swiper-slider'}>
                    <Box
                        component={Card}
                        elevation={3}
                        width={1}
                        pt={'6.146vw'}
                        pb={'3.5vw'}>
                        <Swiper
                            scrollbar={{draggable: true}}
                            breakpoints={{
                                0: {
                                    slidesPerView: 2,
                                    freeMode: true,
                                },
                                678: {
                                    slidesPerView: 3,
                                },
                                1280: {
                                    slidesPerView: 3.5,
                                    slidesPerGroup: 3,
                                    navigation: {
                                        nextEl: '.special-product-slider-next',
                                        prevEl: '.special-product-slider-prev',
                                    }
                                },
                            }}
                            onSwiper={(swiper) => console.log(swiper)}
                            onSlideChange={(e) => console.log('slide change', e)}
                            style={{
                                paddingBottom: '2.8vw'
                            }}>
                            {productData[0].items.map((pr, i) => {
                                const iPlus = i + 1;
                                return (
                                    <SwiperSlide key={pr.id}>
                                        <Link href={`/${pr.id}`} hasHover={false}>
                                            <ProductCard
                                                item={pr}
                                                style={{
                                                    borderLeft: (iPlus % 4 !== 0 && iPlus < productData.length) ? `1px solid ${grey[200]}` : undefined
                                                }}/>
                                        </Link>
                                    </SwiperSlide>
                                )
                            })}
                        </Swiper>
                    </Box>
                    <NextButton uniqClass={'special-product-slider-next'}
                                style={{
                                    left: '16px',
                                }}/>
                    <PrevButton uniqClass={'special-product-slider-prev'}
                                style={{
                                    right: '16px',
                                }}/>
                </Box>
            </Box>
        </Box>
    )
}


function ProductCard({item: it, ...props}) {
    const theme = useTheme();

    return (
        <Box display={'flex'}
             flexDirection={'column'}
             {...props}>
            <Box
                px={it.thumbnail.square ? 10 : 2}
                py={2}>
                <Img
                    alt={it.thumbnail.title}
                    src={"/drawable/image/thumbnail.jpg"}
                    imageWidth={media.thumbnail.width}
                    imageHeight={media.thumbnail.height}
                />
            </Box>
            <Box px={2} display={'flex'} flexDirection={'column'}>
                <Typography component={'h6'} fontWeight={400}
                            variant={'subtitle2'}
                            pb={1.25}
                            minHeight={55}>
                    {it.title.length <= 20 ? it.title : it.title.slice(0, 20) + "..."}
                </Typography>
                <Box
                    alignItems={'center'}
                    display={'flex'}>
                    {
                        Boolean(it.discount_percent && it.discount_percent > 0) &&
                        <Typography
                            variant={'body1'}
                            px='0.729vw'
                            py={0.5}
                            color={'#fff'}
                            dir={'ltr'}
                            justifyContent={'center'}
                            style={{
                                minWidth: 38,
                                backgroundColor: theme.palette.primary.main,
                                ...UtilsStyle.borderRadius(3)
                            }}>
                            % {it.discount_percent}
                        </Typography>
                    }
                    <Box display={'flex'} flex={1} pl={0.1} dir={'ltr'} alignItems={'center'} flexWrap={'wrap'}>
                        <Typography variant={'subtitle1'} fontWeight={600} direction={'ltr'} alignItems={'center'}>
                            <Typography component={'span'} color={'#616161'} variant={'body2'} pl={0.5}>
                                {lang.get("toman")}
                            </Typography>
                            {UtilsFormat.numberToMoney(it.discount_price)}
                        </Typography>
                        {
                            it.final_price > it.discount_price &&
                            <Typography color={'#9E9E9E'} pr={1} component={'del'} variant={'body2'} direction={'ltr'}>
                                {UtilsFormat.numberToMoney(it.final_price)}
                            </Typography>
                        }
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}


function NextButton({uniqClass, ...props}) {


    return (
        <ButtonBase
            className={`${uniqClass} slider-next slider-arrows transition400`}
            {...props}
            style={{
                left: 0,
                ...props.style
            }}>
            <Icon className="fal fa-chevron-left"
                  style={{
                      color: "#000"
                  }}/>
        </ButtonBase>
    )

}

function PrevButton({uniqClass, ...props}) {
    return (
        <ButtonBase
            className={`${uniqClass} slider-prev slider-arrows transition400`}
            {...props}>
            <Icon className="fal fa-chevron-right"
                  style={{
                      color: "#000"
                  }}/>
        </ButtonBase>
    )
}


