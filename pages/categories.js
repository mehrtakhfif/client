import React from "react";
import MobileAllCategories from "../component/category/MobileAllCategories";


function Categories(props) {
    return <MobileAllCategories {...props}/>
}

export default React.memo(Categories)