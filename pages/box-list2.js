import Box from "@material-ui/core/Box";
import React, {useEffect, useState} from "react";
import ControllerSite from "../controller/ControllerSite";
import useSWR, {mutate} from "swr";
import Card from "@material-ui/core/Card";
import BaseButton from "../component/base/button/BaseButton";
import Typography from "../component/base/Typography";
import PleaseWait from "../component/base/loading/PleaseWait";
import _ from "lodash";
import StickyBox from "react-sticky-box";
import {headerOffset} from "../component/header/HeaderLg";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import {ArrowBack, BugReportOutlined, NavigateBefore} from "@material-ui/icons";
import Link from "../component/base/link/Link";
import {grey, red} from "@material-ui/core/colors";
import Img from "../component/base/oldImg/Img";
import {useTheme} from "@material-ui/core";
import ButtonBase from "@material-ui/core/ButtonBase";
import ComponentError from "../component/base/ComponentError";
import {SmHidden, SmShow} from "../component/header/Header";
import {lang} from "../repository"
import {useRouter} from "next/router";
import rout from "../router";
import Tooltip from "../component/base/Tooltip";
import {UtilsStyle} from "../utils/Utils";
import DebugRout from "../lib/wrappedcomponent/DebugRout";
import BasePage from "../component/base/BasePage";

export default function BoxList2({...props}) {
    const [boxPermalink, setBoxPermalink] = useState(undefined);
    const [selectedCat, setSelectedCat] = useState([]);
    const [breadcrumb, setBreadcrumb] = useState(null)

    const boxApiKey = "box-list";
    const {data: boxData, error: boxError} = useSWR(boxApiKey, () => {
        return ControllerSite.getBoxOrCategories({boxPermalink: boxPermalink})
    });
    const catApiKey = "cat-list-" + boxPermalink ? boxPermalink : "";
    const {data: catData, error: catError} = useSWR(catApiKey, () => {
        if (!boxPermalink)
            return undefined
        return ControllerSite.getBoxOrCategories({boxPermalink: boxPermalink})
    });

    useEffect(() => {
        try {
            if (boxPermalink) {
                setSelectedCat([])
            }
        } catch (e) {
        }
    }, [boxPermalink]);

    useEffect(() => {
        try {
            if (!boxPermalink || !(catData && catData.data)) {
                throw "boxPermalink undefined "
            }

            const el = !_.isEmpty(selectedCat) ?
                (<React.Fragment>
                    <Link color={grey[800]}
                          hoverColor={grey[500]}
                          onClick={() => {
                              setSelectedCat([])
                          }}>
                        {catData.data.box.name}
                    </Link>
                    {
                        selectedCat.map((cat, i) => (
                            <React.Fragment key={i}>
                                {(cat.length > i + 1) ?
                                    <Link color={grey[800]}
                                          href={"#"}
                                          hoverColor={grey[500]}
                                          onClick={() => {
                                          }}>
                                        {cat.name}
                                    </Link> :
                                    <Typography>{cat.name}</Typography>}
                            </React.Fragment>
                        ))}
                </React.Fragment>) :
                (
                    <Link color={grey[800]}
                          hoverColor={grey[500]}
                          onClick={() => {
                          }}>
                        {catData.data.box.name}
                    </Link>
                )

            setBreadcrumb(<React.Fragment>
                <SmShow>
                    <Breadcrumbs separator={<NavigateBefore fontSize="small"/>} aria-label="breadcrumb">
                        <Link color={grey[800]}
                              hoverColor={grey[500]}
                              onClick={() => {
                                  setBoxPermalink(undefined)
                                  setSelectedCat([])
                              }}>
                            {lang.get("all")}
                        </Link>
                        {el}
                    </Breadcrumbs>
                </SmShow>
                <SmHidden>
                    <Breadcrumbs separator={<NavigateBefore fontSize="small"/>} aria-label="breadcrumb">
                        {el}
                    </Breadcrumbs>
                </SmHidden>
            </React.Fragment>)
        } catch (e) {
            return setBreadcrumb(null);
        }
    }, [boxPermalink, catData, selectedCat])

    function handleBoxSelect(b) {
        setBoxPermalink(b.permalink);
    }

    function handleCatSelect(cat) {
        setSelectedCat([...selectedCat, cat])
    }

    return (
        <DebugRout>
            <BasePage
                name="box-list2">
                <Box mt={2} display={'flex'} flexDirection={'column'} px={{
                    xs: 0,
                    md: 2
                }}>
                    {!boxError ? boxData ?
                        <React.Fragment display={'flex'}>
                            {!catError ?
                                <React.Fragment>
                                    <SmHidden>
                                        <LgComponent breadcrumbsComponent={breadcrumb}
                                                     boxPermalink={boxPermalink}
                                                     catData={catData}
                                                     selectedCat={selectedCat}
                                                     boxData={boxData}
                                                     onBoxSelected={handleBoxSelect}
                                                     onCatSelected={handleCatSelect}/>
                                    </SmHidden>
                                    <SmShow>
                                        <SmComponent
                                            breadcrumbsComponent={breadcrumb}
                                            boxPermalink={boxPermalink}
                                            catData={catData}
                                            selectedCat={selectedCat}
                                            boxData={boxData}
                                            onBoxSelected={handleBoxSelect}
                                            onCatSelected={handleCatSelect}/>
                                    </SmShow>
                                </React.Fragment> :
                                <ComponentError tryAgainFun={() => mutate(catApiKey)}/>}
                        </React.Fragment> :
                        <Box>
                            <PleaseWait/>
                        </Box> :
                        <ComponentError tryAgainFun={() => mutate(boxApiKey)}/>
                    }
                </Box>
            </BasePage>
        </DebugRout>
    )
}


function LgComponent({
                         breadcrumbsComponent,
                         boxPermalink,
                         boxData,
                         catData,
                         selectedCat,
                         onBoxSelected,
                         onCatSelected
                     }) {

    useEffect(() => {
        try {
            if (boxPermalink)
                throw "boxPermalink has added"
            onBoxSelected(boxData.data.boxes[0])
        } catch (e) {
        }
    }, [boxData])
    return (
        <Box display={'flex'}>
            <Box maxWidth={1 / 4}>
                <StickyBox offsetTop={60} offsetBottom={10}>
                    <Box display={'flex'} flexDirection={'column'}>
                        {
                            _.isArray(boxData.data.boxes) && boxData.data.boxes.map((b) => (
                                <Box width={1} key={b.id} py={1}>
                                    <BaseButton
                                        onClick={() => {
                                            onBoxSelected(b)
                                        }}
                                        style={{
                                            width: "100%",
                                            padding: 0,
                                        }}>
                                        <Box component={Card} width={1} height={70} display={'flex'}
                                             alignItems={'center'}
                                             justifyConetnt={'center'}>
                                            <Box
                                                width={1 / 3}
                                                minWidth={110}
                                                height={1}
                                                style={{
                                                    background: `linear-gradient(90deg,#ffffff, #ffffffa1, transparent), url(${(b.media) ? b.media.image : "https://api.mehrtakhfif.com/media/def_cat.jpg"}) no-repeat center`,
                                                    backgroundSize: 'cover',
                                                }}/>
                                            <Typography variant={"h6"} pr={3} pl={2} py={1} flex={1}
                                                        alignItems={'center'} justifyContent={'center'}>
                                                {b.name}
                                            </Typography>
                                        </Box>
                                    </BaseButton>
                                </Box>
                            ))
                        }
                    </Box>
                </StickyBox>
            </Box>
            <Box flex={1}>
                <StickyBox offsetTop={headerOffset} offsetBottom={10}>
                    {(catData && catData.data) ?
                        (
                            <Box width={1} display={'flex'}
                                 px={{
                                     xs: 1,
                                     md: 2
                                 }}
                                 flexWrap={'wrap'}>
                                <Box width={1} pl={2}>
                                    <Box>
                                        {breadcrumbsComponent}
                                    </Box>
                                </Box>
                                {
                                    catData.data.categories && (!_.isEmpty(selectedCat) ? selectedCat[selectedCat.length - 1].child : catData.data.categories).map((cat) => (
                                        <CatItem item={cat}
                                                 key={cat.id}
                                                 onClick={() => {
                                                     onCatSelected(cat)
                                                 }}/>
                                    ))
                                }
                            </Box>) : <Box px={2} py={2}>
                            <PleaseWait/>
                        </Box>}
                </StickyBox>
            </Box>
        </Box>
    )
}

function SmComponent({
                         breadcrumbsComponent,
                         boxPermalink,
                         boxData,
                         catData,
                         selectedCat,
                         onBoxSelected,
                         onCatSelected
                     }) {

    return (
        <Box display={'flex'} flexWrap={'wrap'} p={2}>
            <Box width={1} pb={2} pl={2}>
                <Box>
                    {breadcrumbsComponent}
                </Box>
            </Box>
            {
                !boxPermalink ?
                    boxData.data.boxes && boxData.data.boxes.map(b => (
                        <CatItem key={`b-${b.id}`} isBox={true} item={b}
                                 onClick={() => {
                                     onBoxSelected(b)
                                 }}/>
                    )) :
                    catData ? catData.data.categories && (!_.isEmpty(selectedCat) ? selectedCat[selectedCat.length - 1].child : catData.data.categories).map((cat) => (
                        <CatItem item={cat}
                                 boxPermalink={boxPermalink}
                                 key={cat.id}
                                 onClick={() => {
                                     onCatSelected(cat)
                                 }}/>
                    )) :
                        <PleaseWait/>
            }
        </Box>
    )
}


function CatItem({boxPermalink, isBox = false, item, onClick}) {
    const theme = useTheme();
    const router = useRouter();

    function goToFilter() {
        // const ro = rout.Product.Filter.create({category:item.permalink});
        // router.push(ro.rout,rout.as)
    }

    const bt = (
        <ButtonBase
            onClick={() => {
                if (!isBox && _.isEmpty(item.child)) {
                    goToFilter()
                    return
                }
                onClick()
            }}
            style={{
                padding: 0
            }}>
            <Box display={'flex'}
                 component={Card}
                 flexDirection={'column'}
                 width={1}
                 style={{
                     position: 'relative'
                 }}>
                {(item.disable) &&
                <Tooltip title={"پیش‌نمایش"}>
                    <BugReportOutlined
                        style={{
                            position: "absolute",
                            top: 5,
                            left: 5,
                            color: red[400],
                            zIndex: 999,
                            background: 'rgba(255, 255, 255, 0.83)',
                            ...UtilsStyle.borderRadius(5)
                        }}/>
                </Tooltip>
                }
                <Img src={item.media ? item.media.image : "https://api.mehrtakhfif.com/media/def_cat.jpg"}/>
                <Box
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        zIndex: 3,
                        background: "linear-gradient(360deg, rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.7) 19%,rgba(255, 255, 255, 0.4) 26%, rgba(255, 255, 255, 0.3) 30%, rgba(255, 255, 255, 0) 100%)"
                    }}/>
                <Box
                    display={'flex'}
                    px={1}
                    pb={0.3}
                    style={{
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        right: 0,
                        zIndex: 3,
                    }}>
                    <Typography variant={"h6"} alignItems={'center'} justifyContent={'center'} style={{flex: 1}}>
                        {item.name}
                    </Typography>
                    {!_.isEmpty(item.child) &&
                    <BaseButton variant={"text"}
                                onClick={(e) => {
                                    onClick()
                                    e.stopPropagation();
                                }}>
                        <Typography variant={"body1"} alignItems={'center'}>
                            نمایش محصولات
                            <ArrowBack style={{
                                paddingRight: theme.spacing(0.5)
                            }}/>
                        </Typography>
                    </BaseButton>}
                </Box>
            </Box>
        </ButtonBase>
    )

    return (
        <Box p={{
            xs: 0.5,
            md: 2
        }}
             width={{
                 xs: 1 / 2,
                 md: 1 / 3
             }}>
            {(!isBox && _.isEmpty(item.child)) ?
                <Link href={rout.Product.Search.as({category: item.permalink})}>
                    {bt}
                </Link> :
                bt
            }
        </Box>
    )
}
