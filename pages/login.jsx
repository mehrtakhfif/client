import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Box, Dialog, gLog, HiddenLgUp, HiddenMdDown, tryIt} from "material-ui-helper";
import _ from "lodash"
import FirstStep from "../component/login/FirstStep";
import {Card} from "@material-ui/core";
import Password from "../component/login/Password";
import {DEBUG} from "./_app";
import {colors} from "../repository";
import {useRouter} from "next/router";
import router from "../router";
import PleaseWait from "../component/base/loading/PleaseWait";
import LogoutRequiredRout from "../lib/wrappedcomponent/LogoutRequiredRout";
import VerifyCode from "../component/login/VerifyCode";
import {useIsLoginContext} from "../context/IsLoginContextContainer";
import {useBrowserHistoriesContext} from "../context/BrowserHistoriesContext";
import {NextSeo} from "next-seo";

export const AuthStatus = {
    FIRST_STEP: 'first_step',
    REGISTERED_USER: "registered_user",
    GUEST_USER: "guest_user",
    LOGIN_WITH_OTP: "login_with_otp",
    FORGOT_PASSWORD: "forgot_password",
    SET_PASSWORD_REQUIRED: "set_password_required",
    PASSWORD_REQUIRED: "password_required",
    OTP_REQUIRED: "otp_required",
    SET_NEW_PASSWORD: "set_new_password",
    VERIFY_CODE_GUST_USER: "verify_code_gust_user",
    SET_NEW_USER_PASSWORD: "set_new_user_password",
    PLEASE_WAIT: "please_wait",
}

export default function Login({open,containerStyle={},onFreeze, onClose, ...props}) {

    const isLogin = useIsLoginContext();

    const [autoChangeRout, setAutoChangeRout] = useState(true)

    //region handler
    const handleAutoChangeRout = useCallback((val) => {
        setAutoChangeRout(val)
        tryIt(() => onFreeze(!val))
    }, [])
    //endregion handler

    const dialogProps = {
        maxWidth: "xs",
        onBackdropClick: onClose,
        fullWidth: true,
        open: open
    }

    return (
        _.isBoolean(open) ?
            (!isLogin || !autoChangeRout) ?
                <React.Fragment>
                    <HiddenMdDown>
                        <Dialog {...dialogProps}>
                            <LoginCm isDialog={true} onClose={onClose} onChangeAutoChangeRout={handleAutoChangeRout}/>
                        </Dialog>
                    </HiddenMdDown>
                    <HiddenLgUp>
                        <Dialog fullScreen={true} rootProps={{justifyContent: "center",height:"100%"}} {...dialogProps}>
                            <LoginCm
                                isDialog={true}
                                containerStyle={{
                                    border:"unset",
                                    ...containerStyle
                                }}
                                onClose={onClose}
                                onChangeAutoChangeRout={handleAutoChangeRout}/>
                        </Dialog>
                    </HiddenLgUp>
                </React.Fragment>
                :
                <React.Fragment/> :
            <LogoutRequiredRout autoChangeRout={autoChangeRout}>
                <NextSeo noindex={true} nofollow={true} />
                <LoginCm containerStyle={containerStyle} onChangeAutoChangeRout={handleAutoChangeRout} py={10}/>
            </LogoutRequiredRout>
    )
}

function LoginCm({isDialog = false, onClose, containerStyle={}, onChangeAutoChangeRout, ...props}) {
    const rout = useRouter();
    const [username, setUsername] = useState("");
    const [status, setStatus] = useState(AuthStatus.FIRST_STEP);
    const [response, setResponse] = useState({});
    const [__,{onBack}]=useBrowserHistoriesContext();

    //region styleProps

    const containerProps = React.useMemo(() => {
        return {
            border: `1px solid ${colors.borderColor.heff0ef}`,
            ...containerStyle
        }
    }, [])

    const innerContainerProps = React.useMemo(() => {
        return {
            width: 1,
            center: true,
            flexDirectionColumn: true,
            py: 12,
            px: {
                xs: 1,
                sm: 4,
                lg: 8
            },
            style: {
                position: "relative",
            }
        }
    }, [])

    //endregion styleProps

    //region useEffect

    useEffect(() => {
        tryIt(() => document.getElementById("login-container").scrollIntoView())
    }, [status])

    useEffect(() => {
        tryIt(() => {
            if (window.location.pathname === router.User.Auth.Login.rout) rout.prefetch(router.Main.home)
        })
    }, [])

    //endregion useEffect

    //region handler
    const passwordLayout = useMemo(() => {
        return status === AuthStatus.REGISTERED_USER || status === AuthStatus.PASSWORD_REQUIRED || status === AuthStatus.SET_NEW_PASSWORD || status === AuthStatus.SET_NEW_USER_PASSWORD || status === AuthStatus.SET_PASSWORD_REQUIRED
    }, [status])
    const verifyCodeLayout = useMemo(() => {
        const show = status === AuthStatus.GUEST_USER || status === AuthStatus.LOGIN_WITH_OTP || status === AuthStatus.FORGOT_PASSWORD || status === AuthStatus.VERIFY_CODE_GUST_USER
        if (show) {
            onChangeAutoChangeRout(false)
        }
        return show
    }, [status])

    const goContinue = useCallback(() => {
        tryIt(() => {
            onChangeAutoChangeRout(true)
            if (window.location.pathname === router.User.Auth.Login.rout) {
                // dispatch(headerFooterVisibility({show: true}))
                setStatus(AuthStatus.PLEASE_WAIT)
                rout.push(router.Main.home)
                return;
            }
        })
    }, [])

    const handleBackClick = useCallback((e) => {
        tryIt(() => e.stopPropagation())
        setStatus(AuthStatus.FIRST_STEP)
    }, [])

    const handleFirstStepSubmit = useCallback(({status: st, username}) => {
        if (!st) {
            // dispatch(headerFooterVisibility({show: true}))
            if (isDialog) {
                tryIt(() => onClose())
                return
            }
            tryIt(()=>onBack())
            return;
        }

        setUsername(username);
        setStatus(st === AuthStatus.OTP_REQUIRED ? AuthStatus.LOGIN_WITH_OTP : st)
        if (st === AuthStatus.GUEST_USER || st === AuthStatus.OTP_REQUIRED) {
            //If gust user => after verify code not signedUp
            onChangeAutoChangeRout(false)
        }
    }, []);

    const handlePasswordSubmit = useCallback(({res, status: st}) => {
        if (st === AuthStatus.FORGOT_PASSWORD) {
            //if user forgot password clicked send verifyCode and autoRoutChange off
            setStatus(AuthStatus.FORGOT_PASSWORD)
            onChangeAutoChangeRout(false)
            return
        }
        if (st === AuthStatus.LOGIN_WITH_OTP) {
            //Login with Otp and finish
            setStatus(AuthStatus.LOGIN_WITH_OTP)
            return;
        }
        try {
            if (res.data.status === AuthStatus.OTP_REQUIRED) {
                setStatus(AuthStatus.LOGIN_WITH_OTP)
                return;
            }
        } catch (e) {

        }
        if (status === AuthStatus.SET_NEW_PASSWORD) {
            //if user password changed autoRoutChange on
            onChangeAutoChangeRout(true)
        }

        goContinue()
    }, [status]);

    const handleVerifyCodeSubmit = useCallback(({res, status: st}) => {
        if (st === AuthStatus.FIRST_STEP) {
            setStatus(AuthStatus.FIRST_STEP)
            return;
        }

        if (status === AuthStatus.GUEST_USER) {
            //set password for NewUser
            setStatus(AuthStatus.SET_NEW_USER_PASSWORD)
            return;
        }
        if (status === AuthStatus.FORGOT_PASSWORD) {
            //if user verify code successful => go to set new password
            setStatus(AuthStatus.SET_NEW_PASSWORD)
            return
        }
        try {
            if (res.data.status === AuthStatus.SET_PASSWORD_REQUIRED || res.data.status === AuthStatus.PASSWORD_REQUIRED) {
                setStatus(res.data.status)
                return;
            }
        } catch (e) {
        }
        //user logged in and continua state === LOGIN_WITH_OTP
        goContinue()
    }, [status]);

    //endregion handler

    return (
        <Box
            width={!isDialog ? "100vw" : undefined}
            height={!isDialog ? "100vh" : undefined}
            center={true}>
            <Box
                id={"login-container"}
                px={{
                    xs:3,
                    md:0
                }}
                component={Card}
                elevation={0}
                width={isDialog ? 1 : {
                    xs: 11 / 12,
                    sm: 9 / 12,
                    md: 4 / 12,
                    lg: 4 / 12,
                    xl: 4 / 12,
                }}
                center={true}
                style={containerProps}>
                {
                    status === AuthStatus.FIRST_STEP ?
                        <FirstStep
                            {...innerContainerProps}
                            onSubmitted={handleFirstStepSubmit}/> :
                        (passwordLayout) ?
                            <Password
                                {...innerContainerProps}
                                onBackClick={handleBackClick}
                                username={username}
                                status={status}
                                onSubmitted={handlePasswordSubmit}/> :
                            (verifyCodeLayout) ?
                                <VerifyCode
                                    {...innerContainerProps}
                                    status={status}
                                    username={username}
                                    response={response}
                                    onBackClick={handleBackClick}
                                    onSubmitted={handleVerifyCodeSubmit}/> :
                                status === AuthStatus.PLEASE_WAIT ?
                                    DEBUG ? <PleaseWait/> : <React.Fragment/> :
                                    <React.Fragment/>
                }
            </Box>
        </Box>
    )
}
