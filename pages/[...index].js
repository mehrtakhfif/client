import React from "react";
import Box from "@material-ui/core/Box";
import {useRouter} from "next/router";
import SiteDetails from "../component/siteDetails/SiteDetails";
import rout from "../router";
import {Error} from "./_error";


export default function Index({...props}) {
    const router = useRouter();

    function getIndex1() {
        try {
            const i = router.query.index[0];
            switch (i) {
                case rout.Main.AboutUs.key:
                case rout.Main.ContactUs.key:
                case rout.Main.PrivacyPolicy.key:
                    return <SiteDetails page={i}/>
                default:
                    throw ""
            }
        } catch (e) {
            return <Error/>
        }
    }

    return (
        <Box display={'flex'} flexDirection={'column'}>
            {getIndex1}
        </Box>
    )
}


