import React from "react";
import DebugRout from "../lib/wrappedcomponent/DebugRout";
import {Box, DefaultTextField} from "material-ui-helper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

function Page({...props}) {

    const handleClose = (e)=>{
        e.stopPropagation();

    }

    return (
        <DebugRout>
            <Box py={2}>
                <DefaultTextField
                    name={"search"}
                    variant={"outlined"}/>
            </Box>
            <MenuList>
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
                <MenuItem onClick={handleClose}>Logout</MenuItem>
            </MenuList>
        </DebugRout>
    )
}

export default Page
