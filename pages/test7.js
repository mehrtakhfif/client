import React, {useMemo} from "react";
import DebugRout from "../lib/wrappedcomponent/DebugRout";
import ControllerProduct from "../controller/ControllerProduct";
import {Box, Typography} from "material-ui-helper";
import useSWRInfinite from "../lib/useSWRInfinite";


export default function Test7() {

    const {getKey, fetcher} = ControllerProduct.Query.V2.Query2
    const api = useMemo(() => getKey({q: "وست"}), [])
    const {
        data,
        error,
        page,
        mutate,
        setNextPage,
        onClear,
        isEmpty,
        isLoadingMore,
        isReachingEnd,
        isRefreshing,
    } = useSWRInfinite(api, fetcher, {
        initialData: [
            {name: "tesssssssssst"},
            {name: "tesssssssssstsafkjaskf"},
            {name: "tesssssssssstgoaskoasogj"},
            {name: "tesssssssssstaiowrijawijf"},
        ]
    })


    return (
        <DebugRout>
            <Box flexDirectionColumn={true}>
                <Typography pt={1} variant={"body1"}>
                    showing {page} page(s) of {isLoadingMore ? '...' : data.length}{' '}
                    issue(s){' '}
                </Typography>
                <Box p={2}>
                    <button
                        disabled={isLoadingMore || isReachingEnd}
                        onClick={setNextPage}>
                        {isLoadingMore
                            ? 'loading...'
                            : isReachingEnd
                                ? 'no more issues'
                                : 'load more'}
                    </button>
                    <button disabled={isRefreshing} onClick={() => mutate()}>
                        {isRefreshing ? 'refreshing...' : 'refresh'}
                    </button>
                    <button disabled={!page} onClick={onClear}>
                        clear
                    </button>
                </Box>
                <Typography variant={"h6"}>
                    {isEmpty ? <p>Yay, no issues found.</p> : <React.Fragment/>}
                </Typography>
                {data?.map((issue, index) => {
                    return (
                        <Typography variant={"h6"} py={1} key={issue?.id} style={{margin: '6px 0'}}>
                            {index + 1} - {issue?.name}
                        </Typography>
                    )
                })}
            </Box>
        </DebugRout>
    )
}
