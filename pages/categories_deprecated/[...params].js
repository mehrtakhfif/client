import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {useRouter} from "next/router";
import useSWR, {mutate} from "swr";
import ControllerSite from "../../controller/ControllerSite";
import PleaseWait from "../../component/base/loading/PleaseWait";
import ComponentError from "../../component/base/ComponentError";
import {SmHidden, SmShow} from "../../component/header/Header";
import rout from "../../router"
import _ from "lodash";
import Link from "../../component/base/link/Link";
import {grey} from "@material-ui/core/colors";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import {ArrowBack, NavigateBefore} from "@material-ui/icons";
import {lang, media} from "../../repository";
import {makeStyles, useTheme} from "@material-ui/core";
import ButtonBase from "@material-ui/core/ButtonBase";
import Card from "@material-ui/core/Card";
import {UtilsStyle} from "../../utils/Utils";
import BaseButton from "../../component/base/button/BaseButton";
import StickyBox from "react-sticky-box";
import {headerOffset} from "../../component/header/HeaderLg";
import DebugBox from "../../component/base/DebugBox";
import {getSafe, Img, Typography} from "material-ui-helper";
import BasePage from "../../component/base/BasePage";

export default function Categories({...props}) {
    const router = useRouter()
    const [breadcrumb, setBreadcrumb] = useState(null);
    const [selectedCat, setSelectedCat] = useState([]);

    const dBox = ControllerSite.getBoxOrCategories();
    const {data: boxData, error: boxError} = useSWR(...dBox);
    const dCat = ControllerSite.getBoxOrCategories({boxPermalink: router.query.params[0]});
    const {data: catData, error: catError} = useSWR(...dCat);

    useEffect(() => {
        try {
            const pr = router.query.params;
            if (_.isEmpty(pr) || !catData || _.isEmpty(catData.data.categories)) {
                throw "boxPermalink undefined"
            }
            const cats = [];
            _.forEach(pr, (pr, i) => {
                if (i === 0)
                    return
                const cat = _.find(catData.data.categories, (c) => c.permalink === pr)
                if (!cat)
                    return;
                cats.push(cat)
            })

            setBreadcrumb(
                <React.Fragment>
                    <SmShow>
                        <Breadcrumbs separator={<NavigateBefore fontSize="small"/>} aria-label="breadcrumb">
                            <Typography
                                color={grey[800]}
                                onClick={() => {
                                    const r = rout.Main.BoxList.create({})
                                    router.push(r.rout, r.as)
                                }}>
                                {lang.get("all")}
                            </Typography>
                            <Link color={grey[800]}
                                  hoverColor={grey[500]}
                                  href={rout.Main.BoxList.create({boxPermalink: catData.data.box.permalink})}>
                                {catData.data.box.name}
                            </Link>
                            {
                                cats.map((cat, i) => (
                                    <React.Fragment key={i}>
                                        {(cat.length > i + 1) ?
                                            <Link color={grey[800]}
                                                  href={"#"}
                                                  hoverColor={grey[500]}
                                                  onClick={() => {
                                                  }}>
                                                {cat.name}
                                            </Link> :
                                            <Typography>{cat.name}</Typography>}
                                    </React.Fragment>
                                ))}
                        </Breadcrumbs>
                    </SmShow>
                    <SmHidden>
                        <Breadcrumbs separator={<NavigateBefore fontSize="small"/>} aria-label="breadcrumb">
                            <Link color={grey[800]}
                                  hoverColor={grey[500]}
                                  href={rout.Main.BoxList.create({boxPermalink: catData.data.box.permalink})}>
                                {catData.data.box.name}
                            </Link>
                            {
                                cats.map((cat, i) => (
                                    <React.Fragment key={i}>
                                        {(cat.length > i + 1) ?
                                            <Link color={grey[800]}
                                                  href={"#"}
                                                  hoverColor={grey[500]}
                                                  onClick={() => {
                                                  }}>
                                                {cat.name}
                                            </Link> :
                                            <Typography>{cat.name}</Typography>}
                                    </React.Fragment>
                                ))}
                        </Breadcrumbs>
                    </SmHidden>
                </React.Fragment>)
        } catch (e) {
            return setBreadcrumb(null);
        }
    }, [router.query.params])


    useEffect(() => {
        const param = router.query.params;
        if (!catData)
            return;
        const lastCat = param[param.length - 1];
        const cat = []
        const newParam = _.cloneDeep(param)
        newParam.shift()
        _.forEach(newParam, pr => {
            if (_.isEmpty(cat)) {
                const p = _.find(catData.data.categories, (cat) => {
                    return cat.permalink === pr
                })
                if (!p) {
                    return
                }
                cat.push(p)
                return
            }
            const p = _.find(cat[cat.length - 1].child, (c) => c.permalink === pr)
            if (!p)
                return false;
            cat.push(p)
        })
        setSelectedCat([...cat])
    }, [router.query.params, catData])

    function onBoxSelect(b) {
        const r = rout.Main.BoxList.create({boxPermalink: b.permalink})
        router.push(r.rout, r.as)
    }

    return (
        <BasePage
            name="categories_deprecated">
            {
                !boxError ?
                    boxData ?
                        <Box width={1}>
                            <SmHidden>
                                <LgComponent
                                    boxData={boxData}
                                    catData={catData}
                                    selectedCat={selectedCat}
                                    breadcrumbsComponent={breadcrumb}
                                    onBoxSelect={onBoxSelect}/>
                            </SmHidden>
                            <SmShow>
                                <SmComponent
                                    boxData={boxData}
                                    catData={catData}
                                    selectedCat={selectedCat}
                                    breadcrumbsComponent={breadcrumb}
                                    onBoxSelect={onBoxSelect}/>
                            </SmShow>
                        </Box> :
                        <PleaseWait/> :
                    <ComponentError tryAgainFun={() => mutate(dBox[0])}/>
            }
        </BasePage>

    )
}


function LgComponent({boxData, catData, selectedCat = [], breadcrumbsComponent, onBoxSelect}) {
    const router = useRouter()
    useEffect(() => {
        const param = router.query.params;
        if (param[0] === "any") {
            const r = rout.Main.BoxList.create({boxPermalink: boxData.data.boxes[0].permalink})
            router.push(r.rout, r.as)
            return
        }
        // if (!catData)
        //     return;
        // const lastCat = param[param.length - 1];
        // const cat = []
        // const newParam = _.cloneDeep(param)
        // newParam.shift()
        // gcLog(`skfjksajf param :DDD`,newParam)
        // _.forEach(newParam, pr => {
        //     if (_.isEmpty(cat)) {
        //         const p = _.find(catData.data.categories_deprecated, (cat) => {
        //             gcLog(`skfjksajf param cat`,cat)
        //             gcLog(`skfjksajf param pr`,pr)
        //             gcLog(`skfjksajf param ${cat.permalink} === ${pr}`)
        //             return cat.permalink === pr
        //         })
        //         gcLog("skfjksajf param",p)
        //         if (!p) {
        //             return
        //         }
        //         cat.push(p)
        //         return
        //     }
        //     const p = _.find(cat[cat.length-1].child,(c)=>c.permalink === pr)
        //     if (!p)
        //         return false;
        //     cat.push(p)
        // })
        // gcLog("skfjksajf param",cat)
    }, [router.query.params])

    return (
        <Box width={1} display={'flex'} mt={4}>
            <Box maxWidth={1 / 4}>
                <StickyBox offsetTop={60} offsetBottom={10}>
                    <Box display={'flex'} flexDirection={'column'}>
                        {
                            _.isArray(boxData.data.boxes) && boxData.data.boxes.map((b) => (
                                <Box width={1} key={b.id} py={1}>
                                    <BaseButton
                                        onClick={() => onBoxSelect(b)}
                                        style={{
                                            width: "100%",
                                            padding: 0,
                                        }}>
                                        <DebugBox debug={b.disable} component={Card} width={1} height={70}
                                                  display={'flex'}
                                                  alignItems={'center'}
                                                  justifyConetnt={'center'}>
                                            <Box
                                                width={1 / 3}
                                                minWidth={110}
                                                height={1}
                                                style={{
                                                    background: `linear-gradient(90deg,#ffffff, #ffffff3b,transparent, transparent), url(${(b.media) ? b.media.image : "https://api.mehrtakhfif.com/media/def_cat.jpg"}) no-repeat center`,
                                                    backgroundSize: 'cover',
                                                }}/>
                                            <Typography variant={"body2"} pr={3} pl={2} py={1} flex={1}
                                                        alignItems={'center'} justifyContent={'center'}>
                                                {b.name}
                                            </Typography>
                                        </DebugBox>
                                    </BaseButton>
                                </Box>
                            ))
                        }
                    </Box>
                </StickyBox>
            </Box>
            <Box flex={1}>
                <StickyBox offsetTop={headerOffset} offsetBottom={10}>
                    {(catData && catData.data) ?
                        (
                            <Box width={1} display={'flex'}
                                 px={{
                                     xs: 1,
                                     md: 2
                                 }}
                                 flexWrap={'wrap'}>
                                <Box width={1} pl={2}>
                                    <Box>
                                        {breadcrumbsComponent}
                                    </Box>
                                </Box>
                                {
                                    catData.data.categories && (!_.isEmpty(selectedCat) ? selectedCat[selectedCat.length - 1].child : catData.data.categories).map((cat) => (
                                        <CatItem item={cat}
                                                 key={cat.id}
                                                 onClick={() => {
                                                     const pr = router.query.params
                                                     const catPr = [];
                                                     _.forEach(pr, (p, i) => {
                                                         if (i === 0)
                                                             return
                                                         catPr.push(p)
                                                     })
                                                     catPr.push(cat.permalink)
                                                     const r = rout.Main.BoxList.create({
                                                         boxPermalink: pr[0],
                                                         catsPermalink: catPr
                                                     })
                                                     router.push(r.rout, r.as)
                                                 }}/>
                                    ))
                                }
                            </Box>) : <Box px={2} py={2}>
                            <PleaseWait/>
                        </Box>}
                </StickyBox>
            </Box>
        </Box>
    )
}

function SmComponent({boxData, catData, selectedCat = [], breadcrumbsComponent, onBoxSelect}) {
    const router = useRouter();
    const param = router.query.params;

    return (
        <Box width={1} display={'flex'} mt={2} flexWrap={"wrap"}>
            {
                (!_.isEmpty(param) && param[0] === "any") &&
                _.isArray(boxData.data.boxes) && boxData.data.boxes.map(b => (
                    <CatItem key={`b-${b.id}`} isBox={true} item={b}
                             onClick={() => onBoxSelect(b)}/>
                ))
            }
            {(catData && catData.data) ?
                (
                    <Box width={1} display={'flex'}
                         px={{
                             xs: 1,
                             md: 2
                         }}
                         flexWrap={'wrap'}>
                        <Box width={1} pl={2} pb={2}>
                            <Box>
                                {breadcrumbsComponent}
                            </Box>
                        </Box>
                        {
                            catData.data.categories && (!_.isEmpty(selectedCat) ? selectedCat[selectedCat.length - 1].child : catData.data.categories).map((cat) => (
                                <CatItem item={cat}
                                         key={cat.id}
                                         onClick={() => {
                                             const pr = router.query.params
                                             const catPr = [];
                                             _.forEach(pr, (p, i) => {
                                                 if (i === 0)
                                                     return
                                                 catPr.push(p)
                                             })
                                             catPr.push(cat.permalink)
                                             const r = rout.Main.BoxList.create({
                                                 boxPermalink: pr[0],
                                                 catsPermalink: catPr
                                             })
                                             router.push(r.rout, r.as)
                                         }}/>
                            ))
                        }
                    </Box>) :
                <Box px={2} py={2}>
                    <PleaseWait/>
                </Box>}
        </Box>
    )
}


const useStyles = makeStyles(theme => ({
    categoriesImg: {
        ...UtilsStyle.transition(500),
        '&:hover': {
            MozTransform: "scale(1.04) rotate(1deg)",
            WebkitTransform: 'scale(1.04) rotate(1deg)',
            OTransform: "scale(1.04) rotate(1deg)",
            MsTransform: 'scale(1.04) rotate(1deg)',
            transform: 'scale(1.04) rotate(1deg)',
            ...UtilsStyle.transition(500)
        }
    }
}));

function CatItem({isBox = false, item, onClick}) {
    const theme = useTheme();
    const router = useRouter();
    const classes = useStyles();

    function goToFilter() {
        // const ro = rout.Product.Filter.create({boxPermalink:  router.query.params[0], category: [item.permalink]});
        // router.push(ro.rout,rout.as)
    }


    const bt = (
        <ButtonBase
            onClick={() => {
                if (!isBox && _.isEmpty(item.child)) {
                    goToFilter()
                    return
                }
                onClick()
            }}
            style={{
                width: '100%',
                padding: 0,
            }}>
            <Box display={'flex'}
                 component={Card}
                 flexDirection={'column'}
                 width={1}
                 style={{
                     position: 'relative'
                 }}>
                <Box width={1}>
                    <Img
                        className={classes.categoriesImg}
                        alt={getSafe(() => item.media.title, "")}
                        src={getSafe(() => item.media.image, "")}
                        renderTimeout={2000}
                        backupSrc={"https://api.mehrtakhfif.com/media/def_cat.jpg"}
                        imageWidth={media.category.width}
                        imageHeight={media.category.height}
                        minHeight={{xs: 80, md: 200}}/>
                </Box>
                <Box
                    display={'flex'}
                    px={1}
                    py={0.3}
                    style={{
                        backgroundColor: 'rgb(240, 240, 240)',
                        zIndex: 1
                    }}>
                    <Typography variant={"body1"}
                                alignItems={'center'}
                                justifyContent={'center'} style={{flex: 1}}>
                        {item.name}
                    </Typography>
                    {!_.isEmpty(item.child) &&
                    <BaseButton variant={"text"}
                                onClick={(e) => {
                                    onClick()
                                    e.stopPropagation();
                                }}>
                        <Typography
                            variant={"body1"}
                            alignItems={'center'}>
                            نمایش محصولات
                            <ArrowBack style={{
                                paddingRight: theme.spacing(0.5)
                            }}/>
                        </Typography>
                    </BaseButton>}
                </Box>
            </Box>
        </ButtonBase>
    )

    return (
        <Box
            p={{
                xs: 0.5,
                md: 2
            }}
            debug={item.disable}
            width={{
                xs: 1 / 2,
                md: 1 / 3
            }}>
            {(!isBox && _.isEmpty(item.child)) ?
                <Link hasHover={false} hasHoverBase={false}
                      href={rout.Product.Search.create({
                          category: item.permalink
                      })}>
                    {bt}
                </Link> :
                bt
            }
        </Box>
    )
}
