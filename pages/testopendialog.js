import React, {useEffect, useState} from 'react';
import DebugRout from "../lib/wrappedcomponent/DebugRout";
import {Box, Button, Dialog, Typography} from "material-ui-helper";
import {useRouter} from "next/router";

export default function Grouped() {
    const rout = useRouter()
    const [open, setOpen] = useState(false)

    const handleOpenClick = () => {
        history.pushState({
            dialog: 1
        }, null, location.href)
        handleOpenDialog()
    }

    const handleOpenDialog = ()=>{
        setOpen(true)
    }

    const handleCloseDialog = ()=>{
        setOpen(false)
    }



    useEffect(() => {
        window.onpopstate = function (event) {
            if (!event.state || event.state.dialog === 1) {
                handleOpenDialog()
                return
            }
            handleCloseDialog()
        };
    }, [])


    return (
        <DebugRout>
            <Box width={1} my={10} px={2} center={true}>
                <Box width={0.5}>
                    <Button onClick={handleOpenClick}>
                        openDialog
                    </Button>
                </Box>
                <Dialog open={open} onClose={() => rout.back()}>
                    <Typography variant={"h1"} py={10} px={3}>
                        Dialog
                    </Typography>
                </Dialog>
            </Box>
        </DebugRout>
    );
}
