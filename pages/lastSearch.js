import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {lang} from "../repository";
import rout from '../router';
import SearchBar from "../component/SearchBar";
import ControllerSite from "../controller/ControllerSite";
import _ from "lodash";
import Container from "@material-ui/core/Container";
import Img from "../component/base/oldImg/Img";
import {UtilsStyle} from "../utils/Utils";
import Card from "@material-ui/core/Card";
import Link from "../component/base/link/Link";
import {useRouter} from "next/router";
import {getPageTitle, setPageTitle} from "../headUtils";
import Head from "next/head";
import useSWR, {mutate} from "swr";

import ComponentError from "../component/base/ComponentError";
import Typography from "../component/base/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import AdminRout from "../lib/wrappedcomponent/AdminRout";
import BasePage from "../component/base/BasePage";

export default function LastSearch({...props}) {
    const router = useRouter();
    const [value, setValue] = useState(router.query.q ? router.query.q : "");
    const apiKey = `search-${value}`;
    const {data, error} = useSWR(apiKey, () => {
        if (!value)
            return [];
        return ControllerSite.Search.get({text: value}).then(res => {
            return res.data;
        })
    }, {
        dedupingInterval: 60000 * 5
    });


    useEffect(() => {
        try {
            router.push(...SearchRoutCreator({qParam: value}))
            // props.history.push(SearchRoutCreator({qParam: state.value}));
            const pt = !_.isEmpty(value) ? (': ' + value) : '';
            setPageTitle({title: lang.get("search") + pt});
        } catch (e) {
        }
    }, [value]);


    return (
        <AdminRout>
            <BasePage
                name="lastSearch">
            <Box my={{xs: 7, md: 5}}>
                <Head>
                    <title>{getPageTitle(lang.get("search"))}</title>
                </Head>
                <Box mx={3}>
                    <SearchBar
                        value={value}
                        openList={false}
                        name={"search-page"}
                        loadingReq={!data}
                        onChange={(text) => {
                            setValue(text)
                        }}/>
                </Box>
                <Container
                    maxWidth="xl">
                    <Box display='flex'
                         flexWrap='wrap'
                         alignItems='stretch'
                         width='100%' py={2}>
                        {
                            !_.isObject(data) ?
                                <Box p={2} mt={4}>
                                    <Typography variant={"h6"} width={1} py={3} px={2} justifyContent='center'
                                                alignItems='center'>
                                        {lang.get(_.isEmpty(value) ? "enter_part_of_the_product_name" : "products_not_found")}
                                    </Typography>
                                </Box>
                                :
                                !error ? data &&
                                    <Box display={'flex'} flexDirection={'column'}>
                                        {data.map(d => <SearchItem key={d.id} data={d}/>)}
                                    </Box> :
                                    <ComponentError
                                        tryAgainFun={() => {
                                            mutate(apiKey)
                                        }}/>
                        }
                    </Box>
                </Container>
            </Box>
            </BasePage>
        </AdminRout>
    )
}

function SearchItem({data, ...params}) {
    return (
        <Box py={1}>
            <Box component={Card} {...params}>
                {data ?
                    <Link href={data.rout || "#"}>
                        <ButtonBase style={{padding: 0}}>
                            <Box py={1} px={1} display='flex' alignItems='center'>
                                <Box width='30%'>
                                    <Img src={data.thumbnail}
                                         alt={data.name}
                                         imgStyle={{
                                             width: 150,
                                             maxWidth: '100%',
                                             ...UtilsStyle.borderRadius(7),
                                         }}/>
                                </Box>
                                <Typography px={2} component={"h6"} variant={'h6'} style={{width: '70%'}}>
                                    {data.name}
                                </Typography>
                            </Box>
                        </ButtonBase>
                    </Link> :
                    <Box>
                        placeHolder
                    </Box>}
            </Box>
        </Box>
    )
}

export function SearchRoutCreator({qParam = ""}) {
    return rout.Product.Search.create({q: qParam})
}
