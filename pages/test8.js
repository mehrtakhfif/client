import React, {useLayoutEffect} from "react";
import DebugRout from "../lib/wrappedcomponent/DebugRout";
import {UtilsParser} from "../utils/Utils";
import {useRouter} from "next/router";
import Box from "@material-ui/core/Box";


const key = "test";
const html = '<p style="text-align:center;"><strong>برای خرید گلدان مناسب این گیاه میتوانید از طریق </strong><a href="/product/2099"><span style="color: rgb(44,130,201);"><strong>این لینک</strong></span></a> <strong>اقدام کنید</strong></p><p><span style="font-size: 16px;">بونسای کاج کونیکا از درختان همیشه سبز است که به دلیل برگ های سوزنی زیبایی که دارد، بسیار محبوب واقع شده است. رشد نسبتا کندی دارد و از درختان مقاوم به تنش های محیطی است. شکل تاج آن به صورت مخروطی است و برگ های سوزنی آن کامل در هم فرو رفته اند و شکل ظاهری توپری را به درختچه می بخشند. کاج کونیکا نماد دعوت به صلح و آرامش است. هدیه دادن بونسای کاج به معنی استواری، پایداری، و آرزوی عمر طولانی برای افراد است. بونسای کاج کونیکا بسیار قابل توجه است و افراد زیادی علاقه مند به نگهداری از آن در منازل خود هستند.</span></p>';

export default function Test8({sectionKey = key}) {
    const router = useRouter()


    useLayoutEffect(() => {
        try {
            const section = document.getElementById(sectionKey);
            const aList = section.getElementsByTagName("a")

            _.forEach(aList, el => {
                el.onclick = (e) => {
                    e.preventDefault()
                    const href = el.getAttribute("href");
                    router.push(href)
                }
            })
        } catch (e) {
        }
    }, [])


    return (
        <DebugRout>
            <Box p={5} id={sectionKey} display={'block'}>
                {UtilsParser.html(html)}
            </Box>
        </DebugRout>
    )
}
