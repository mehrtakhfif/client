import React, {useEffect, useState} from 'react'
import {GA, lang, theme} from "../repository";
import rout from "../router"
import Placeholder from "../component/base/Placeholder";
import ControllerUser from "../controller/ControllerUser";
import {listeners as shoppingMiddlewareListeners, removeProduct, updateProduct} from "../middleware/shopping";
import useSWR, {mutate} from "swr";
import ComponentError from "../component/base/ComponentError";
import PleaseWait from "../component/base/loading/PleaseWait";
import {useRouter} from "next/router";
import Box from "@material-ui/core/Box";
import {Card} from "@material-ui/core";
import Img from "../component/base/oldImg/Img";
import ButtonLink from "../component/base/link/ButtonLink";
import {cyan, grey} from "@material-ui/core/colors";
import Hidden from "@material-ui/core/Hidden";
import {smFooterHeight} from "../component/footer/FooterSm";
import CheckoutSummary, {GoToNextLevelButton} from "../component/basket/CheckoutSummary";
import Container from "@material-ui/core/Container";
import ProductsContainerHeader from "../component/home/product/productsContainer/ProductsContainerHeader";
import BasketItem from "../component/basket/main/BasketItem";
import ShoppingDetailsCard from "../component/basket/ShoppingDetalisCard";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import BaseButton from "../component/base/button/BaseButton";
import Typography from '../component/base/Typography'
import Head from "next/head";
import {getPageTitle} from "../headUtils";
import ActiveInvoices from "../component/basket/ActiveInvoices";
import BasePage from "../component/base/BasePage";
import {useIsLoginContext} from "../context/IsLoginContextContainer";
import {useLoginDialogContext} from "../context/LoginDialogContextContainer";
import DebugRout from "../lib/wrappedcomponent/DebugRout";

function Basket(props) {
    const router = useRouter();
    const isLogin = useIsLoginContext();
    const [__,setLoginDialog]= useLoginDialogContext()


    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Basket.rout});
        const syncLogout = event => {
            if (event.key === shoppingMiddlewareListeners.onBasketChange) {
                mutate(d[0])
            }
        };
        window.addEventListener('storage', syncLogout);
        router.prefetch(rout.User.Basket.Shopping.rout);
        return () => {
            window.removeEventListener('storage', syncLogout);
        }
    }, []);

    const d = ControllerUser.Basket.get();
    const {data: da, error} = useSWR(...d, {
        dedupingInterval: 8000,
        onSuccess: () => unDisable(),
        onError: () => unDisable()
    });

    const {data} = da ? da : {};
    const [removeItem, setRemoveItem] = useState({
        id: null,
        storage_id: null
    });
    const [newState, setNewState] = useState({
        disable: false
    });

    function resetData() {
        console.log("saklfjkjaskjfkasjkjfa resetData")
        mutate(d[0]);
    }

    //region function
    function onNextButtonClick() {
        if (!isLogin) {
            setLoginDialog(true)
            return
        }
        const r = rout.User.Basket.Shopping.Params.Details.create();
        router.push(r.rout, r.as);
    }

    //region itemChangeCountHandler
    function onItemChangeCount({index, id, storage_id, count}) {
        disable();
        updateProduct({
            index: index,
            basket_id: data.basket_id,
            basketproduct_id: id,
            storage_id: storage_id,
            count: count
        }).then(res => {
            resetData()
        }).catch(() => {
            unDisable()
        })
    }


    //endregion itemChangeCountHandler

    //region itemRemoveHandler
    function onItemRemoveHandler({id, storage_id}) {
        disable();
        setRemoveItem({
            id: id,
            storage_id: storage_id
        })
    }

    function requestRemoveItem() {
        const id = removeItem.id;
        const storage_id = removeItem.storage_id;
        setRemoveItem({
            id: null,
            storage_id: null
        });
        removeProduct({basket_id: data.basket_id, basketproduct_id: id, storage_id: storage_id}).then(res => {
            resetData()
        }).catch(() => {
            unDisable()
        })
    }

    function cancelRemoveItem() {
        unDisable();
        setRemoveItem({
            id: null,
            storage_id: null
        })
    }

    //endregion itemRemoveHandler
    function disable() {
        setNewState({
            ...newState,
            disable: true
        });
        // const newDisableProducts = newState.products.disableProducts;
        // newDisableProducts.push(storage_id);
        // setNewState({
        //     ...newState,
        //     products: {
        //         ...newState.products,
        //         disableProducts: newDisableProducts
        //     },
        //     summary: {
        //         ...newState.summary,
        //         disable: true
        //     }
        // });
    }

    /**
     * @description disable all loading state
     * @param storage_id
     * @param getObject
     * @returns {{summary: {disable: boolean}, addressRequired: *, goToShoppingSteps: boolean, products: {data: *, disableProducts: []}}|{summary: ({disable: boolean}|{disable: boolean}), products: ({data: *, disableProducts: []}|{disableProducts: *})}} newState Object
     */
    function unDisable() {
        setNewState({
            ...newState,
            disable: false
        })
        // const newDisableProducts = state.products.disableProducts;
        // _.remove(newDisableProducts, function (n) {
        //     return n === storage_id;
        // });
        // const newState = {
        //     ...state,
        //     products: {
        //         ...state.products,
        //         disableProducts: newDisableProducts
        //     },
        //     summary: {
        //         ...state.summary,
        //         disable: !_.isEmpty(newDisableProducts)
        //     }
        // };
        //
        // if (getObject)
        //     return newState;
        //
        // setState({
        //     ...newState
        // });
    }

    //endregion function

    return (
        <DebugRout>

            <BasePage
                name="basket2"
                setting={{
                    header: {
                        showSm: false
                    }
                }}>
                <Head>
                    <title>{getPageTitle(lang.get("basket"))}</title>
                </Head>
                {error ?
                    <ComponentError tryAgainFun={resetData}/> :
                    data ?
                        <React.Fragment>
                            <ActiveInvoices activeInvoice={data.active_invoice}/>
                            {(!data.data || data.data.length === 0) ?
                                <Box my={8} mx={5}>
                                    <Box component={Card} width={1} p={2} display={'flex'} flexDirection='column'
                                         justifyContent={'center'}
                                         alignItems={'center'}>
                                        <Img mt={4} src={'/drawable/svg/basket.svg'}
                                             alt={'Basket Image'}
                                             showSkeleton={false}
                                             style={{
                                                 width: '100%',
                                                 textAlign: 'center',
                                                 maxWidth: 150,
                                             }}/>
                                        <Typography variant="h6" mb={4} mt={2}>
                                            {lang.get("basket_is_empty")}!
                                        </Typography>
                                        <Typography variant="h6" mb={1.5} fontWeight={600}>
                                            {lang.get("me_link_to_show_more_product")}
                                        </Typography>
                                        <Box display={'flex'} alignItems={'center'}>
                                            <ButtonLink href={rout.Main.home}
                                                        fontWeight={500}
                                                        variant={'h6'}
                                                        color={cyan[500]}>
                                                {lang.get('discounts_and_offers')}
                                            </ButtonLink>
                                            |
                                            <ButtonLink href={rout.Main.home} fontWeight={500} variant={'h6'}
                                                        color={cyan[500]}>
                                                {lang.get('best_selling_products')}
                                            </ButtonLink>
                                        </Box>
                                    </Box>
                                </Box>
                                :
                                <>
                                    <Hidden mdUp>
                                        <CheckoutSummary
                                            totalProfit={data.summary.totalProfit}
                                            totalAmount={data.summary.totalPrice}
                                            amountPayable={data.summary.discountPrice}
                                            shippingCost={data.summary.shippingCost}
                                            disable={newState.disable}
                                            onButtonClick={onNextButtonClick}
                                            style={{
                                                backgroundColor: '#fff',
                                                paddingLeft: theme.spacing(5),
                                                paddingTop: theme.spacing(2),
                                                paddingBottom: theme.spacing(2)
                                            }}/>
                                        <Box
                                            width={1}
                                            display='flex'
                                            justifyContent='center'
                                            style={{
                                                position: 'fixed',
                                                bottom: smFooterHeight + 5,
                                                zIndex: theme.zIndex.appBar - 20,
                                            }}>
                                            <GoToNextLevelButton
                                                onClick={onNextButtonClick}
                                                disabled={newState.disable}
                                                buttonText={lang.get('continue_ordering')}
                                                style={{
                                                    width: '95%'
                                                }}/>
                                        </Box>
                                    </Hidden>
                                    <Container maxWidth="xl"
                                               style={{
                                                   marginTop: theme.spacing(2)
                                               }}>
                                        <Hidden mdDown>
                                            <ProductsContainerHeader
                                                style={{
                                                    width: '%100',
                                                    marginTop: theme.spacing(5),
                                                    marginBottom: theme.spacing(2)
                                                }}>
                                                {lang.get('basket')}
                                            </ProductsContainerHeader>
                                        </Hidden>
                                        <Box display='flex' pb={`${(smFooterHeight * 2) + 5}px`}>
                                            <Box
                                                display={{
                                                    lg: 'block !important',
                                                    xs: 'flex'
                                                }}
                                                flexWrap='wrap'
                                                justifyContent='center'
                                                width={{
                                                    xs: '100%',
                                                    md: '70%'
                                                }}>
                                                {
                                                    data.data.map(({
                                                                       id,
                                                                       product,
                                                                       count,
                                                                       discountPrice,
                                                                       finalPrice,
                                                                       discountPercent,
                                                                       ...s
                                                                   }, index) => (
                                                        <Box
                                                            key={index}
                                                            width={{
                                                                xs: '100%',
                                                                sm: '50%',
                                                                lg: '100%'
                                                            }}>
                                                            <BasketItem
                                                                label={product.storageTitle}
                                                                permalink={product.permalink}
                                                                finalPrice={finalPrice}
                                                                product={product}
                                                                discountPrice={discountPrice}
                                                                discountPercent={discountPercent}
                                                                shortDescription={product.shortDescription}
                                                                maxCountForSale={product.maxCountForSale}
                                                                disable={newState.disable}
                                                                imgSrc={product.thumbnail}
                                                                count={count}
                                                                onItemCountChange={count => {
                                                                    onItemChangeCount({
                                                                        index: index,
                                                                        id: id,
                                                                        storage_id: product.storage_id,
                                                                        count: count
                                                                    })
                                                                }}
                                                                onItemRemoveHandler={() => onItemRemoveHandler({
                                                                    id: id,
                                                                    storage_id: product.storage_id
                                                                })}
                                                                style={{
                                                                    marginBottom: theme.spacing(1.5),
                                                                    marginRight: theme.spacing(1),
                                                                    marginLeft: theme.spacing(1)
                                                                }}/>
                                                        </Box>
                                                    ))
                                                }
                                            </Box>
                                            <ShoppingDetailsCard
                                                totalProfit={data.summary.totalProfit}
                                                totalAmount={data.summary.totalPrice}
                                                amountPayable={data.summary.discountPrice}
                                                shippingCost={data.summary.shippingCost}
                                                max_shipping_time={data.summary.max_shipping_time}
                                                disable={newState.disable}
                                                onButtonClick={(props) => {
                                                    onNextButtonClick(props)
                                                }}/>
                                        </Box>
                                    </Container>
                                    <Dialog
                                        open={removeItem.storage_id ? true : false}
                                        onClose={() => {
                                            cancelRemoveItem()
                                        }}
                                        aria-labelledby="address_remove_alert-dialog-title"
                                        aria-describedby="address_remove_alert-dialog-description">
                                        <DialogTitle
                                            style={{
                                                paddingRight: theme.spacing(3),
                                                paddingLeft: theme.spacing(3),
                                                paddingBottom: theme.spacing(3)
                                            }}>{lang.get("me_remove_product_on_basket")}</DialogTitle>
                                        <DialogActions>
                                            <BaseButton
                                                onClick={cancelRemoveItem}
                                                autoFocus
                                                style={{
                                                    backgroundColor: cyan[400],
                                                    color: '#fff',
                                                    marginLeft: theme.spacing(1.5)
                                                }}>
                                                {lang.get("no")}
                                            </BaseButton>
                                            <BaseButton
                                                size={"small"}
                                                variant={"text"}
                                                onClick={requestRemoveItem}
                                                style={{
                                                    color: grey[800]
                                                }}>
                                                {lang.get("yes")}
                                            </BaseButton>
                                        </DialogActions>
                                    </Dialog>
                                </>}
                        </React.Fragment>
                        : <PleaseWait/>}
                <Placeholder/>
            </BasePage>
        </DebugRout>
    )
}

export default Basket;
