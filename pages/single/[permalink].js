import React, {useEffect} from "react";
import {useRouter} from "next/router";
import rout from "../../router";


export default function Single() {

    const {query,...router} = useRouter();
    const {permalink} = query || {};

    useEffect(() => {
        router.push(rout.Product.SingleV2.as(permalink))
    }, [])


    return <React.Fragment/>
}