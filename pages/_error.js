import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {GA} from "../repository";
import Lottie from 'react-lottie';
import p404Animation from '../public/drawable/json/animation/p404.json'
import Typography from "../component/base/Typography";
import {HomeOutlined} from "@material-ui/icons";
import {useTheme} from "@material-ui/core";
import BaseButton from "../component/base/button/BaseButton";
import {useRouter} from "next/router";
import rout from "../router";
import {Head} from "next/document";
import {NextSeo} from "next-seo";

export default function _error({statusCode, ...props}) {
    return (<Error/>)
}

export function Error({statusCode}) {
    const theme = useTheme()
    const router = useRouter();
    const [state, setState] = useState({
        isStopped: false,
        isPaused: false
    });

    useEffect(() => {
        if (statusCode) {
            GA.Error.ReqError(statusCode)
            return
        }
        GA.Error.P404();
    }, [])

    const buttonStyle = {
        display: 'block',
        margin: '10px auto'
    };

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: p404Animation,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <React.Fragment>
            <NextSeo noindex={true} nofollow={true} />
            <Box
                py={5}
                px={3}
                display={'flex'}
                flexDirection={{
                    xs: 'column',
                    md: 'row'
                }}
                width={1}>
                <Box width={{
                    xs: 1,
                    md: 1 / 2
                }}>
                    <Lottie options={defaultOptions}
                            height={"auto"}
                            width={"100%"}
                            isStopped={state.isStopped}/>
                </Box>
                <Box flex={1} display={'flex'} flexDirection={'column'} alignItems={'center'} justifyContent={'center'}>
                    <Typography variant={'h2'} smVariant={"h4"} component={'h3'} py={2} px={1} fontWeight={500}
                                style={{textAlign: 'center'}}>
                        صفحه مورد نظر پیدا نشد
                    </Typography>
                    <Box display={'flex'} alignItems={'center'} pt={2}>
                        <Box px={1}>
                            <BaseButton
                                onClick={() => {
                                    router.push(rout.Main.home)
                                }}
                                style={{
                                    backgroundColor: "#01DF6C",
                                }}>
                                <Typography variant={'h6'} alignItems={"center"} color={'#fff'}>
                                    <HomeOutlined
                                        style={{
                                            color: '#fff',
                                            marginLeft: theme.spacing(1)
                                        }}/>
                                    رفتن به صفحه اصلی
                                </Typography>
                            </BaseButton>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </React.Fragment>
    )
}
