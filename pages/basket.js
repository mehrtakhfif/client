import React from "react";
import BasePage from "../component/base/BasePage";
import {NextSeo} from "next-seo";
import {colors, lang} from "../repository";
import {Box, gLog, HiddenMdUp, UtilsStyle} from "material-ui-helper";
import {useLoginDialogContext} from "../context/LoginDialogContextContainer";
import ControllerUser from "../controller/ControllerUser";
import useSWR from "swr";
import BasketIsEmpty from "../component/basketV2/BasketIsEmpty";
import _ from "lodash";
import BasketItemsContainer from "../component/basketV2/basketItems/BasketItemsContainer";
import BasketPanel from "../component/basketV2/basketPanel/BasketPanel";
import BasketDetail from "../component/basketV2/basketPanel/lgBasketPanel/basketDetail/BasketDetail";
import ActiveInvoices from "../component/basketV2/ActiveInvoices";
import {Head} from "next/document";


function BasketBase() {
    const d = ControllerUser.Basket.V2.get();
    const {data, error} = useSWR(...d)

    return (
        <BasePage
            name="basket"
            setting={{
                header: {
                    showSm: false
                }
            }}>
            <NextSeo
                title={lang.get("basket")}
                noindex={true}
                nofollow={true}
                openGraph={{
                    title: lang.get("basket")
                }}/>
            {
                data?.data?.active_invoice &&
                <ActiveInvoices activeInvoice={data?.data?.active_invoice}/>
            }
            <Basket data={data?.data || {}}/>
        </BasePage>
    )
}

export default React.memo(BasketBase)

function Basket({data}) {
    const [__, setLoginDialog] = useLoginDialogContext()

    const {basket, summary} = data || {};
    const {id: basketId, products} = basket || {};

    if (data && _.isEmpty(products))
        return (<BasketIsEmpty/>)


    return (
        <React.Fragment>
            <Box
                pt={{
                    xs: 3.5,
                    md: 0
                }}
                px={{
                    xs: 0,
                    md: 5,
                    lg: 10,
                }}
                flexDirection={{
                    xs: "column",
                    md: 'row'
                }}
                style={{position: 'relative'}}>
                <HiddenMdUp>
                    <Box
                        width={1}
                        px={{
                            xs: 2,
                            md: 0
                        }}>
                        <Box
                            width={1}
                            flexDirection={'column'}
                            p={2}
                            style={{
                                backgroundColor: colors.backgroundColor.hf8f8f8,
                                ...UtilsStyle.borderRadius(5)
                            }}>
                            <BasketDetail
                                summary={summary}/>
                        </Box>
                    </Box>
                </HiddenMdUp>
                <BasketItemsContainer data={products}/>
                <BasketPanel summary={summary}/>
            </Box>
        </React.Fragment>
    )
}