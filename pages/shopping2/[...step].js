import React from "react";
import BasePage from "../../component/base/BasePage";
import LoginRequiredRout from "../../lib/wrappedcomponent/LoginRequiredRout";
import {useRouter} from "next/router";
import rout from "../../router";
import {Box} from "material-ui-helper";
import Steps from "../../component/shipping/steps/Steps";
import DetailsStep from "../../component/shipping/steps/detailsStep/DetailsStep";
import Header from "../../component/shipping/Header";


export default function Shipping() {
    const router = useRouter();
    const step = router?.query?.step?.[0] || [];


    const isDetails = step === rout.User.Basket.Shopping.Params.Details.param;
    const isPayment = step === rout.User.Basket.Shopping.Params.Payment.param;
    const isInvoice = step === rout.User.Basket.Shopping.Params.Invoice.param;
    const isFail = step === rout.User.Basket.Shopping.Params.Fail.param;

    return (
        <LoginRequiredRout>
            <BasePage
                name="shopping2"
                setting={{
                    header: {
                        showLg: false,
                    },
                    footer: {
                        showLg: false
                    }
                }}>
                <Header/>
                <Box px={10} pt={5} flexDirection={'column'}>
                    <Steps/>
                    <Box width={1} pt={13}>
                        <Box
                            pr={{
                                xs: 0,
                                md: 3.3
                            }}
                            flexDirection={'column'}
                            flex={1}>
                            {
                                isDetails ?
                                    <DetailsStep/> :
                                    <React.Fragment/>
                            }
                        </Box>
                        <Box width={0.27} pl={3.3}>
                        </Box>
                    </Box>
                </Box>
            </BasePage>
        </LoginRequiredRout>
    )
}