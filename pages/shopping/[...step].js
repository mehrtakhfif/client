import React, {useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import {useRouter} from "next/router";
import ControllerUser from "../../controller/ControllerUser";
import useSWR, {mutate} from "swr";
import ControllerSite from "../../controller/ControllerSite";
import {lang, theme} from "../../repository";
import rout, {routIsExact} from "../../router";
import {listeners as shoppingMiddlewareListeners} from "../../middleware/shopping";
import {useSnackbar} from "notistack";
import CheckoutSummary, {GoToNextLevelButton} from "../../component/basket/CheckoutSummary";
import ComponentError from "../../component/base/ComponentError";
import PleaseWait from "../../component/base/loading/PleaseWait";
import Steppers from "../../component/basket/shoppingSteps/Steppers";
import Placeholder from "../../component/base/Placeholder";
import Container from "@material-ui/core/Container";
import ShoppingDetailsCard from "../../component/basket/ShoppingDetalisCard";
import BasketSummery from "../../component/basket/shoppingSteps/BasketSummary";
import AddressPage from "../../component/basket/shoppingSteps/addressPage/AddressPage";
import PaymentPage from "../../component/basket/shoppingSteps/paymentPage/PaymentPage";
import InvoicePage from "../../component/basket/shoppingSteps/invoicePage/InvoicePage";
import {SmShow} from "../../component/header/Header";
import {smFooterHeight} from "../../component/footer/FooterSm";
import Button from "@material-ui/core/Button";
import {update} from "../../middleware/auth";
import _ from 'lodash'
import FailPage from "../../component/basket/shoppingSteps/failPage/FailPage";
import Typography from "../../component/base/Typography";
import Link from "../../component/base/link/Link";
import Head from "next/head";
import {getPageTitle} from "../../headUtils";
import {DEBUG} from "../_app";
import {redirectTo} from "../../middleware/base";
import LoginRequiredRout from "../../lib/wrappedcomponent/LoginRequiredRout";
import {getSafe, tryIt} from "material-ui-helper";
import ErrorBoundary from "../../component/base/ErrorBoundary";
import BasePage from "../../component/base/BasePage";
import {useUserContext} from "../../context/UserContext";
import {useIsLoginContext} from "../../context/IsLoginContextContainer";
import {useBackdropContext} from "../../context/BackdropContext";


export default function Shopping({...props}) {
    return (
        <LoginRequiredRout>
            <BasePage
                name="shopping"
                setting={{
                    header: {
                        showSm: false
                    }
                }}>
                <ShoppingCm {...props}/>
            </BasePage>
        </LoginRequiredRout>
    )
}

function ShoppingCm({...props}) {
    const router = useRouter();
    const step = router.query.step || [];
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [backdrop,setBackDrop] = useBackdropContext()
    const [user] = useUserContext();
    const isLogin = useIsLoginContext()
    const personalDataRef = useRef();
    const [state, setState] = useState({
        payment: {
            username: user?.username,
            firstName: user?.first_name,
            lastName: user?.last_name,
            meliCode: user?.meli_code,
            email: user?.email || "",
            activeIpg: null,
            hasError: true,
            disable: false,
        },
        summaryDisable: false,
    });

    useEffect(() => {
        if (!user || (_.isObject(user) && !user.username))
            return;
        setState({
            ...state,
            payment: {
                ...state.payment,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                meliCode: user.meliCode,
                email: user.email ? user.email : "",
            }
        })
    }, [user])
    const d = ControllerUser.Basket.get();
    const {data: da, error} = useSWR(...d, {
        onSuccess: (res) => {
            unDisable()
        },
        onError: () => {
            unDisable()
        }
    });

    const {data} = da ? da : {};

    const dIpg = ControllerSite.IPG.get();
    const {data: dataIpg, error: errorIpg} = useSWR(...dIpg);

    useEffect(() => {
        const syncLogout = event => {
            if (event.key === shoppingMiddlewareListeners.onBasketChange) {
                mutate(d[0]);
                mutate(dIpg[0]);
            }
        };
        window.addEventListener('storage', syncLogout);

        return () => {
            window.removeEventListener('storage', syncLogout);
        }
    }, []);

    useEffect(() => {
        if (data && _.isArray(data.data)) {
            if (_.isEmpty(data.data)) {
                redirectTo(undefined, rout.User.Basket.rout)
                return
            }
            if (!data.addressRequired) {
                onNextStepButtonClick()
            }
        }
    }, [data])


    // if (data) {
    //     data.addressRequired = true
    // }

    function disable() {

        setState({
            ...state,
            payment: {
                ...state.payment,
                disable: true
            },
            summaryDisable: true
        })

    }

    function unDisable() {

        setTimeout(() => {

            setState({
                ...state,
                payment: {
                    ...state.payment,
                    disable: false
                },
                summaryDisable: false
            })

        }, [1000])
    }

    function isDetailsPage() {
        return routIsExact(step[0], rout.User.Basket.Shopping.Params.Details.param);
    }

    function isPaymentPage() {
        return routIsExact(step[0], rout.User.Basket.Shopping.Params.Payment.param);
    }

    function isInvoicePage() {
        return routIsExact(step[0], rout.User.Basket.Shopping.Params.Invoice.param);
    }

    function isFailPage() {
        return routIsExact(step[0], rout.User.Basket.Shopping.Params.Fail.param);
    }

    function getActiveStep() {
        if (!data)
            return 0

        if (isDetailsPage())
            return 0;

        if (isPaymentPage())
            return data.addressRequired ? 1 : 0;

        return data.addressRequired ? 2 : 1
    }

    const isLastStep = tryIt(()=>{
        return window.location.pathname === '/shopping/payment'
    },false)

    const nextStepButtonText = lang.get(isLastStep ? 'payment' : getActiveStep() === 2 ? 'fallow_order' : ((isLogin) ? 'continue_ordering' : "login_first_and_pay"));

    function onNextStepButtonClick() {
        if (isDetailsPage()) {
            const r = rout.User.Basket.Shopping.Params.Payment.create();

            router.push(r.rout, r.as);
            return
        }

        if (!user.isActiveData) {
            try {
                let errorText = undefined;
                if (personalDataRef.current && personalDataRef.current.hasError()) {
                    errorText = lang.get("er_complete_profile");
                }


                const {email, firstName, lastName, meliCode} = personalDataRef.current.serialize();
                if (meliCode.length < 10) {
                    errorText = "کدملی را درست وارد کنید.";
                }


                if (errorText) {
                    unDisable();
                    enqueueSnackbar(errorText,
                        {
                            variant: "error",
                            action: (key) => (
                                <React.Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </React.Fragment>
                            )
                        });
                    return;
                }
                disable();

                update({
                    firstName: firstName,
                    lastName: lastName,
                    meliCode: meliCode,
                    email: email
                }).then((res) => {


                    unDisable();
                    requestToPay();

                }).catch(() => {

                    unDisable();

                    enqueueSnackbar(lang.get("er_problem"),
                        {
                            variant: "error",
                            action: (key) => (
                                <React.Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </React.Fragment>
                            )
                        });
                })
            } catch (e) {

                unDisable();

                enqueueSnackbar("please_try_again",
                    {
                        variant: "warning",
                        action: (key) => (
                            <React.Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </React.Fragment>
                        )
                    });
            }
        } else {
            disable();
            requestToPay();
        }
    }

    function requestToPay() {

        enqueueSnackbar(lang.get("me_forwarding_to_payment"),
            {
                variant: "success",
                action: (key) => (
                    <React.Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </React.Fragment>
                )
            });

        setBackDrop(true)
        disable()
        ControllerUser.Shopping.payment({
            basket_id: data.basket_id,
            ipg_id: state.payment.activeIpg || dataIpg.ipg.default
        }).then((res) => {
            unDisable();

            window.location.href = res.redirectUrl;


        }).catch(() => {
            setBackDrop(false)
            enqueueSnackbar(lang.get("er_problem_to_forward_payment"),
                {
                    variant: "error",
                    action: (key) => (
                        <React.Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </React.Fragment>
                    )
                });
            unDisable()
        })
    }

    return (
        <Box>
            <Head>
                <title>{getPageTitle(lang.get("basket"))}</title>
            </Head>
            {error ?
                <React.Fragment>
                    <ComponentError
                        tryAgainFun={() => {
                            mutate(d[0])
                        }}/>
                </React.Fragment>
                :
                getSafe(() => (data && state.payment.username && data.summary), false) ?
                    <ErrorBoundary elKey={"shipping/[...step].js"}>
                        <SmShow>
                            <CheckoutSummary
                                totalProfit={data.summary.totalProfit}
                                totalAmount={data.summary.totalPrice}
                                amountPayable={data.summary.discountPrice}
                                shippingCost={data.summary.shippingCost}
                                disable={state.summaryDisable || state.payment.disable}
                                buttonText={nextStepButtonText}
                                onButtonClick={onNextStepButtonClick}
                                style={{
                                    backgroundColor: '#fff',
                                    paddingLeft: theme.spacing(5),
                                    paddingTop: theme.spacing(2),
                                    paddingBottom: theme.spacing(2)
                                }}/>
                            <Box
                                width={1}
                                display='flex'
                                justifyContent='center'
                                style={{
                                    position: 'fixed',
                                    bottom: smFooterHeight + 5,
                                    zIndex: 3
                                }}>
                                <GoToNextLevelButton
                                    onClick={onNextStepButtonClick}
                                    disabled={state.summaryDisable || state.payment.disable}
                                    buttonText={nextStepButtonText}
                                    style={{
                                        width: '95%'
                                    }}/>
                            </Box>
                        </SmShow>
                        <Box
                            display="flex"
                            justifyContent="center"
                            width={1}
                            mt={4}>
                            <Steppers addressRequired={data.addressRequired} activeStep={getActiveStep()}/>
                        </Box>
                        <Container maxWidth="xl"
                                   style={{
                                       marginTop: theme.spacing(2),
                                       marginBottom: theme.spacing(3)
                                   }}>
                            <Box display='flex' width={1}>
                                <Box
                                    display='flex'
                                    flexWrap='wrap'
                                    flexDirection='column'
                                    width={1}
                                    maxWidth={{
                                        xs: 1,
                                        lg: isInvoicePage() ? 1 : 0.75
                                    }}
                                    flex={1}>
                                    {
                                        (isDetailsPage()) ?
                                            (data?.addressRequired ?
                                                <AddressPage
                                                    onChangeAddress={(val) => {
                                                        mutate(d[0])
                                                    }}/> :
                                                <PleaseWait/>)
                                            : (isPaymentPage()) ?
                                                errorIpg ?
                                                    <ComponentError
                                                        tryAgainFun={() => {
                                                            mutate(dIpg[0])
                                                        }}/> :
                                                    dataIpg ?
                                                        <PaymentPage
                                                            firstName={state.payment.firstName}
                                                            lastName={state.payment.lastName}
                                                            meliCode={state.payment.meliCode}
                                                            email={state.payment.email}
                                                            disable={state.payment.disable}
                                                            personalDataIsActive={user.isActiveData}
                                                            ipgs={dataIpg.ipg.data}
                                                            activeIpg={state.payment.activeIpg || dataIpg.ipg.default}
                                                            onChangeIpg={(val) => {
                                                                setState({
                                                                    ...state,
                                                                    payment: {
                                                                        ...state.payment,
                                                                        activeIpg: _.toNumber(val)
                                                                    }
                                                                })
                                                            }}
                                                            onDiscountCodeSet={() => {
                                                                mutate(d[0])
                                                            }}
                                                            hasError={state.payment.hasError}
                                                            personalDataRef={personalDataRef}
                                                            onChangePersonalData={({
                                                                                       firstName,
                                                                                       lastName,
                                                                                       meliCode,
                                                                                       email,
                                                                                       ...props
                                                                                   }) => {
                                                                setState({
                                                                    ...state,
                                                                    payment: {
                                                                        ...state.payment,
                                                                        firstName: firstName !== undefined ? firstName : state.payment.firstName,
                                                                        lastName: lastName !== undefined ? lastName : state.payment.lastName,
                                                                        meliCode: meliCode !== undefined ? meliCode : state.payment.meliCode,
                                                                        email: email !== undefined ? email : state.payment.email
                                                                    }
                                                                });
                                                            }}/>
                                                        :
                                                        <PleaseWait/>
                                                :
                                                isInvoicePage() ?
                                                    <InvoicePage {...props}/> :
                                                    isFailPage() ?
                                                        <FailPage/> :
                                                        <Typography variant={'h5'} p={3}>
                                                            بازگشت به
                                                            <Link variant={'h5'} fontWeight={600}
                                                                  href={rout.User.Basket.rout}>
                                                                {lang.get("basket")}
                                                            </Link>
                                                        </Typography>
                                    }
                                    <Box display='flex' width={1}>
                                        <BasketSummery products={data?.data}/>
                                    </Box>
                                </Box>
                                {
                                    !isInvoicePage() &&
                                    <ShoppingDetailsCard
                                        totalProfit={data.summary.totalProfit}
                                        totalAmount={data.summary.totalPrice}
                                        amountPayable={data.summary.discountPrice}
                                        shippingCost={data.summary.shippingCost}
                                        max_shipping_time={data.summary.max_shipping_time}
                                        disable={state.summaryDisable || state.payment.disable}
                                        buttonText={nextStepButtonText}
                                        onButtonClick={onNextStepButtonClick}/>
                                }
                            </Box>
                        </Container>
                    </ErrorBoundary>
                    :
                    <React.Fragment>
                        <PleaseWait/>
                        {
                            DEBUG &&
                            <React.Fragment>Shopping</React.Fragment>
                        }
                    </React.Fragment>
            }
            <Placeholder/>
        </Box>
    )
}

