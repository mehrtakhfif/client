import React, {useEffect, useState} from 'react';
import Box from "@material-ui/core/Box";
import {useRouter} from "next/router";
import useSWR, {mutate} from "swr";
import ControllerProduct from "../../controller/ControllerProduct";
import Hidden from "@material-ui/core/Hidden";
import ComponentError from "../../component/base/ComponentError";
import Skeleton from "@material-ui/lab/Skeleton";
import StickyBox from "react-sticky-box";
import FilterContainer from "../../component/boxPage/FilterContainer";
import _ from 'lodash';
import Placeholder from "../../component/base/Placeholder";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "../../component/base/link/Link";
import {grey} from "@material-ui/core/colors";
import Paper from "@material-ui/core/Paper";
import MobileFilterContainer from "../../component/boxPage/MobileFilterContainer";
import {isClient, lang} from "../../repository";
import {SSRUtils, Utils} from "../../utils/Utils";
import {getPageTitle} from "../../headUtils";
import Head from "next/head";
import {SmHidden} from "../../component/header/Header";
import ProductsList from "../../component/filter/ProductsList";
import {DEBUG} from "../_app";
import {tryIt} from "material-ui-helper";
import rout from "../../router";
import BasePage from "../../component/base/BasePage";
import DebugRout from "../../lib/wrappedcomponent/DebugRout";


export const orderByItems = [
    {
        label: 'most_newest',
        value: ''
    },
    {
        label: 'most_cheapest',
        value: 'cheap'
    },
    {
        label: 'most_expensive',
        value: 'expensive'
    },
    {
        label: 'most_bestseller',
        value: 'best_seller'
    },
    {
        label: 'most_popular',
        value: 'popular'
    },
    {
        label: 'most_discount',
        value: 'discount'
    },
];

export const defaultOrder = orderByItems[0];

export function getFilterProps(query) {
    if (!query)
        return {
            b: ''
        };

    if (query && query.history && query.history.state) {
        query = Utils.getQueryFromUrl(query.history.state.as);
    }


    const filter = {
        b: ''
    };
    const box = query.b ? query.b : undefined;
    const q = query.q ? query.q : undefined;
    const orderBy = query.order ? query.order : defaultOrder;
    const category = query.cat || '';
    const brand = query.br ? _.isString(query.br) ? [query.br] : _.isArray(query.br) ? query.br : [] : [];
    const priceFilter = {
        minPrice: _.toInteger(query.min_price),
        maxPrice: _.toInteger(query.max_price),
    };

    if (box) {
        filter.b = box
    }
    if (q) {
        filter.q = q
    }
    if (orderBy && orderBy.value) {
        filter.o = orderBy.value
    }
    if (category) {
        filter.cat = category
    }
    if (!_.isEmpty(brand)) {
        filter.brand = brand
    }

    if (priceFilter && priceFilter.maxPrice > 0) {
        filter.min_price = priceFilter.minPrice;
        filter.max_price = priceFilter.maxPrice;
    }

    return filter
}

function FilterProducts(props){
    return <DebugRout>
        <FilterProductsBase {...props}/>
    </DebugRout>
}
function FilterProductsBase({productsListInitialProps, asProps, ...props}) {


    //region states
    const {query, ...router} = useRouter();

    const [box, setBox] = useState(query.params[0]);
    const [cat, setCat] = useState(query.params[1]);
    const [orderBy, setOrderBy] = useState(getInitOrder());
    const [queryString, setQueryString] = useState(query.query);
    const [brand, setBrand] = useState(query.br || []);
    const [tags, setTags] = useState(query.tg || []);
    const [availableProduct, setAvailableProduct] = useState(true);
    const [pricing, setPricing] = useState({
        minp: query.minp ? _.toInteger(query.minp) : 0,
        maxp: query.maxp ? _.toInteger(query.maxp) : 0
    });

    // useEffect(() => {
    //     // setBox(query.params[0])
    // }, [query.params[0]]);


    const filterApiKey = `filter-filterQuery-box${box}-cat${cat}`;
    const {data: filterData, error: filterError} = useSWR(filterApiKey, () => {
        const params = {};
        if (box !== "all") {
            params.b = box
        }
        if (cat) {
            params.cat = cat
        }
        return ControllerProduct.Query.getFilters()[1](params)
    });

    //endregion states

    function getInitOrder() {
        const i = _.findIndex(orderByItems, (o) => {
            return query.o === o.label
        });
        return (i !== -1) ? orderByItems[i] : defaultOrder
    }

    //region func
    useEffect(() => {
        tryIt(() => {
            const r = rout.Product.Search.create({
                q: queryString,
                o: orderBy?.label,
                price: [pricing.minp, pricing.maxp]
            })

            if (r.as === decodeURI(window.location.pathname + window.location.search))
                return;
            router.push(r.as)
            return
            let url = `/filter/${box}${cat ? `/${cat}` : ''}`;
            url = url + "?";
            if (!_.isEmpty(queryString)) {
                url = url + `query=${queryString}&`
            }
            if (orderBy && orderBy !== defaultOrder) {
                url = url + `o=${orderBy.label}&`
            }
            if (availableProduct) {
                // url = url + `av=t&`
            }
            if (!_.isEmpty(brand)) {
                _.forEach(brand, (b) => {
                    url = url + `br=${b}&`
                })
            }
            if (pricing.maxp && pricing.maxp > 0) {
                url = url + `minp=${pricing.minp}&`;
                url = url + `maxp=${pricing.maxp}&`;
            }
            url = url.substring(0, url.length - 1)
            if (decodeURI(window.location.pathname) !== url)
                router.push("/filter/[...params]", url);
        })

    }, [query, box, cat, queryString, orderBy, availableProduct, brand, pricing]);


    function updateFilter({
                              orderBy,
                              queryString,
                              categoryPermalink,
                              minPrice,
                              maxPrice,
                              availableProduct,
                              brand: brandProps,
                          }) {

        if (categoryPermalink)
            setCat(categoryPermalink);
        if (queryString)
            setQueryString(queryString);
        if (minPrice && maxPrice) {
            setPricing({
                minp: minPrice,
                maxp: maxPrice
            });
        }
        if (_.isBoolean(availableProduct)) {
            setAvailableProduct(availableProduct)
        }
        if (orderBy) {
            setOrderBy(orderBy)
        }
        if (brandProps) {
            const {item, checked} = brandProps;
            if (checked) {
                setBrand([
                    ...brand,
                    _.toString(item.id)
                ])
            } else {
                const newBrand = [...brand];
                const id = _.toString(item.id);
                _.remove(newBrand, (b) => {
                    return b === id
                });
                setBrand(newBrand);
            }
        }
    }

    function filterRemoveHandler(key, data) {
        switch (key) {
            case 'queryString': {
                // setQueryString('');
                break;
            }
            case 'orderBy':
                setOrderBy(defaultOrder);
                break;
            case 'categoryPermalink':
                setCat('');
                break;
            case 'minMax':
                setPricing({
                    maxp: 0,
                    minp: 0
                });
                break;
            case 'brand':
                const bra = brand;
                _.remove(brand, b => b === data);
                setBrand([...bra.sort()]);
                break;
            default:
                break
        }
    }

    function breadcrumbsHandler(event) {
        event.preventDefault();
        alert('You clicked a breadcrumb.');
    }

    //endregion func

    const filterProps = {
        queryString: "",
        categoryPermalink: cat,
        minPrice: pricing.minp,
        maxPrice: pricing.maxp,
        orderBy: orderBy !== defaultOrder ? orderBy : '',
        brands: brand,
    };

    return (
        <BasePage
            name="filter">
            <Head>
                <title>{getPageTitle(lang.get("search_product"))}</title>
            </Head>
            <Box display='flex' mt={4} mb={4}>
                <SmHidden>
                    <Box width='25%'>
                        {
                            filterError ?
                                <ComponentError tryAgainFun={() => mutate(filterApiKey)}/> :
                                filterData ?
                                    <StickyBox offsetTop={150} offsetBottom={10}>
                                        <FilterContainer
                                            boxKey={box}
                                            filter={filterProps}
                                            categories={filterData.categories}
                                            queryString={queryString}
                                            onCategoryClick={(permalink) => {
                                                updateFilter({
                                                    categoryPermalink: permalink
                                                })
                                            }}
                                            brands={filterData.brands}
                                            onBrandClick={(props) => {
                                                updateFilter({
                                                    brand: props
                                                })
                                            }}
                                            onQueryStringChange={(text) => {
                                                updateFilter({
                                                    queryString: text
                                                })
                                            }}
                                            minPrice={filterData.minPrice}
                                            maxPrice={filterData.maxPrice}
                                            availableProduct={availableProduct}
                                            onAvailableProductChange={() => {

                                            }}
                                            onPriceRangeFilter={({minPrice, maxPrice}) => {
                                                updateFilter({
                                                    minPrice: minPrice,
                                                    maxPrice: maxPrice
                                                })
                                            }}
                                            onFilterRemove={filterRemoveHandler}/>
                                    </StickyBox>
                                    :
                                    <Box>
                                        <Skeleton width='95%' height={70}/>
                                        <Skeleton width='95%' height={200}/>
                                        <Skeleton width='95%' height={70}/>
                                        <Skeleton width='95%' height={70}/>
                                    </Box>
                        }
                    </Box>
                </SmHidden>
                <Box
                    flex={1}
                    mx={{
                        xs: 0,
                        md: 1
                    }}>
                    {
                        DEBUG &&
                        <Hidden smDown>
                            <Box width='100%' mb={1}>
                                <Breadcrumbs aria-label="breadcrumb">
                                    <Link color={grey[800]} href="#" onClick={breadcrumbsHandler}>
                                        صفحه اصلی
                                    </Link>
                                    <Link color={grey[800]} href="#" onClick={breadcrumbsHandler}>
                                        همونجا
                                    </Link>
                                    <Box m={0} fontWeight="fontWeightRegular" fontSize='body1.fontSize'
                                         component='h3'
                                         style={{
                                             color: grey[800]
                                         }}>باکس 1</Box>
                                </Breadcrumbs>
                            </Box>
                        </Hidden>
                    }
                    <Paper>
                        <ProductsList
                            initialData={productsListInitialProps}
                            filterProps={{box, cat, availableProduct, orderBy, queryString, brand, pricing}}
                            filter={{
                                orderBy: orderBy
                            }}
                            availableProduct={availableProduct}
                            onAvailableProductChange={(av) => {
                                updateFilter({availableProduct: av})
                            }}
                            onChangeOrder={(key) => updateFilter({orderBy: key})}/>
                        {(filterData) &&
                        <Hidden lgUp>
                            <MobileFilterContainer activeOrder={orderBy}
                                                   onOrderByClick={(key) => updateFilter({orderBy: key})}>
                                <FilterContainer boxKey={box}
                                                 openFilters={false}
                                                 filter={filterProps}
                                                 categories={filterData.categories}
                                                 queryString={queryString}
                                                 onQueryStringChange={(text) => {
                                                     updateFilter({
                                                         queryString: text
                                                     })
                                                 }}
                                                 onCategoryClick={(permalink) => {
                                                     updateFilter({
                                                         categoryPermalink: permalink
                                                     })
                                                 }}
                                                 onBrandClick={(props) => {
                                                     updateFilter({
                                                         brand: props
                                                     })
                                                 }}
                                                 brands={filterData.brands}
                                                 minPrice={filterData.minPrice}
                                                 maxPrice={filterData.maxPrice}
                                                 onPriceRangeFilter={({minPrice, maxPrice}) => {
                                                     updateFilter({
                                                         minPrice: minPrice,
                                                         maxPrice: maxPrice
                                                     })
                                                 }}
                                                 onFilterRemove={filterRemoveHandler}/>
                            </MobileFilterContainer>
                        </Hidden>}
                    </Paper>
                </Box>
            </Box>
            <Placeholder/>
        </BasePage>
    )
}

FilterProducts.getInitialProps = async function ({res, asPath, query, ...p}) {
    if (isClient()) {
        return __NEXT_DATA__.props.pageProps;
    }

    const params = {};

    if (query.params[0] !== "all") {
        params.box = query.params[0];
    }
    if (query.params[1]) {
        params.cat = query.params[1];
    }
    if (query.o) {
        const i = _.findIndex(orderByItems, (o) => {
            o.label === query.o
        });
        if (i !== -1)
            params.o = orderByItems[i].value;
    }
    if (query.query) {
        params.q = query.query;
    }
    if (query.av) {
        params.av = query.av;
    }
    if (!_.isEmpty(query.br)) {
        params.brand = query.br
    }
    if (query.maxp && _.toInteger(query.maxp) > 0 && query.minp && _.toInteger(query.minp) > 0) {
        params.min_price = query.minp;
        params.max_price = query.maxp;
    }

    const data = ControllerProduct.Query.products()[1]({
        page: 1,
        ...params
    });

    if (!data)
        SSRUtils.redirect(res);

    return {
        asProps: Utils.getQueryFromUrl(asPath),
        productsListInitialProps: data.initialData
    }
};

// FilterProducts.whydidyourender = true
export default FilterProducts;
