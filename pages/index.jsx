import React, {useRef} from "react";
import {Box} from "material-ui-helper";
import HomeSlider from "../component/homeV2/HomeSlider";
import BasePage from "../component/base/BasePage";
import {isClient} from "../repository";
import ControllerSite from "../controller/ControllerSite";
import {DEBUG} from "./_app";
import dynamic from "next/dynamic";
import useInView from "react-cool-inview";


const SpecialOffer = dynamic(() => import('../component/homeV2/specialOffer/SpecialOffer'))
const PromotedCategories = dynamic(() => import('../component/homeV2/promotedCategories/PromotedCategories'))
const SpecialProduct = dynamic(() => import('../component/homeV2/specialProduct/SpecialProduct'))

//TODO: SEO Check
function Index({
                   initialHomeSlider,
                   initialLimitedSpecialProduct,
                   initialPromotedCategories,
                   initialAds,
                   initialSpecialProduct,
                   ...props
               }) {
    const {observe: specialOfferOb, inView: specialOfferInView} = useInView({
        onEnter: ({unobserve}) => unobserve(), // only run once
    });
    const {observe: promotedCategoriesOb, inView: promotedCategoriesInView} = useInView({
        onEnter: ({unobserve}) => unobserve(), // only run once
    });
    const {observe: specialProductOb, inView: specialProductInView} = useInView({
        onEnter: ({unobserve}) => unobserve(), // only run once
    });

    return (
        <BasePage
            name="index">
            <Box
                width={1}
                pt={{
                    xs: 1,
                    md: 4
                }}
                flexDirection={"column"}>
                <HomeSlider initialData={initialHomeSlider}/>
                <div ref={specialOfferOb}>
                    {specialOfferInView && <SpecialOffer initialData={initialLimitedSpecialProduct}/>}
                </div>
                <div ref={promotedCategoriesOb}>
                    {promotedCategoriesInView&& <PromotedCategories initialData={initialPromotedCategories}/>}
                </div>
                <div ref={specialProductOb}>
                    {specialProductInView && <SpecialProduct initialAdsData={initialAds} initialSpecialProductData={initialSpecialProduct}/>}
                </div>
            </Box>
        </BasePage>
    )
}


// export async function getServerSideProps({query, req, ...ctx}) {
//     const headers = req.headers;
//     try {
//         const data = await Promise.all([
//             ControllerSite.Slider.getHomeSlider({headers: headers})[1](),
//             ControllerSite.limitedSpecialProduct({headers: headers})[1](),
//             ControllerSite.promotedCategories({headers: headers})[1](),
//             ControllerSite.ADS.getHome({headers: headers})[1](),
//         ])
//         return {
//             props: {
//                 initialHomeSlider: data[0],
//                 initialLimitedSpecialProduct: data[1],
//                 initialPromotedCategories: data[2],
//                 initialAds: data[3],
//                 initialSpecialProduct: null,
//             }
//         }
//     } catch (e) {
//         return {
//             props: {}
//         }
//     }
// }


Index.getInitialProps = async function ({query, req, ...ctx}) {
    try {
        if (!req)
            throw ""
        const headers = req.headers;
        const data = await Promise.all([
            ControllerSite.Slider.getHomeSlider({headers: headers})[1](),
            ControllerSite.limitedSpecialProduct({headers: headers})[1](),
            ControllerSite.promotedCategories({headers: headers})[1](),
            ControllerSite.ADS.getHome({headers: headers})[1](),
            // ControllerSite.specialProduct({headers: headers})[1](),
        ])
        return {
            initialHomeSlider: data[0],
            initialLimitedSpecialProduct: data[1],
            initialPromotedCategories: data[2],
            initialAds: data[3],
            initialSpecialProduct: undefined,
        }
    } catch (e) {
        return {}
    }
};


export default Index