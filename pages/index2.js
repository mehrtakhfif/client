import React, {useEffect} from 'react';
import {GA, isClient, isServer} from "../repository";
import rout from '../router';
import useSWR from 'swr'
import ControllerProduct from "../controller/ControllerProduct";
import Placeholder from "../component/base/Placeholder";
import TopSliderColumn from "../component/home/TopSliderColumn";
import ControllerSite from "../controller/ControllerSite";
import Box from "@material-ui/core/Box";
import BoxItemSlider from "../component/home/product/box/BoxItemSlider";
import Link from "../component/base/link/Link";
import Img from "../component/base/oldImg/Img";
import _ from 'lodash'
import {withAuthSync} from "../middleware/auth";
import ServerRender from "../component/base/ServerRender";
import {SmHidden, SmShow} from "../component/header/Header";
import {UtilsStyle} from "../utils/Utils";
import ErrorBoundary from "../component/base/ErrorBoundary";
import {NextSeo} from "next-seo";
import BasePage from "../component/base/BasePage";
import {DEBUG} from "./_app";

function Home({specialProducts, allSpecialProducts, ...props}) {
    // const d = ControllerProduct.SpecialProducts.get();
    // const {data: spData, error} = useSWR(...d);

    useEffect(() => {
        GA.initializePage({pageRout: rout.Main.home});
    }, []);

    return (
        <BasePage
            name="index2"
            setting={{
                header: {
                    showLg:true,
                    showSm: true
                },
                footer: {
                    showLg:true,
                    showSm: true
                }
            }}>
            <NextSeo
                title={"فروشگاه اینترنتی همیشه تخفیف"}
                noindex={false}
                nofollow={false}
                openGraph={{
                    title: "مهرتخفیف فروشگاه اینترنتی همیشه تخفیف",
                }}/>
            <ErrorBoundary elKey={"Index::TopSliderColumn"}>
                <TopSliderColumn/>
            </ErrorBoundary>
            {/*{*/}
            {/*    (spData && spData.specialProduct) &&*/}
            {/*    <ErrorBoundary elKey={"Index::SpecialProductsColumn"}>*/}
            {/*        <Box mb={{*/}
            {/*            xs: 1,*/}
            {/*            md: 3*/}
            {/*        }}>*/}
            {/*            <SpecialProductsColumn data={spData.specialProduct} initialData={specialProducts}/>*/}
            {/*        </Box>*/}
            {/*    </ErrorBoundary>*/}
            {/*}*/}

            <ErrorBoundary elKey={"Index::BoxesProducts"}>
                <BoxesProducts data={allSpecialProducts}/>
            </ErrorBoundary>
            {/*<Placeholder/>*/}
        </BasePage>
    )
}

function BoxesProducts({data: initialData, ...props}) {
    const {data, error} = useSWR(...ControllerProduct.SpecialProducts.getAll());
    const {data: adsData} = useSWR(...ControllerSite.ADS.getHome({}));
    //Todo: Ads Link


    return (
        <React.Fragment>
            {(initialData && initialData.products && isServer()) && <ServerRender>
                {initialData.products.map((product, index) => (
                    <Box key={index}>
                        <a href={rout.Product.Search.as({category: product.permalink})}>
                            <h3>{product.name}</h3>
                        </a>
                        {!_.isEmpty(product.specialProduct) &&
                        <React.Fragment>
                            {product.specialProduct.map((p, i) => {
                                const link = rout.Product.SingleV2.as(p?.permalink);
                                return (
                                    <Box key={`${index}-${i}`}>
                                        <a href={link}><img src={p.thumbnail.image} alt={p.thumbnail.title}/></a>
                                        <a href={link}><h4>{p.name}</h4></a>
                                    </Box>
                                )
                            })}
                        </React.Fragment>}
                    </Box>
                ))}
            </ServerRender>}
            {(data && data.products) && (
                data.products.map((product, index) => {
                    return (
                        !_.isEmpty(product.specialProduct) &&
                        <Box
                            my={1}
                            key={product.id}
                            display='flex'
                            flexDirection='column'>
                            <BoxItemSlider data={product}/>
                            {data.products.length !== index + 1 &&
                            (
                                <React.Fragment>
                                    <SmShow>
                                        {adsData && adsData.ads && (() => {
                                            const adsIndex = adsData.ads[index] ? index : 0;
                                            return (
                                                adsData.ads[adsIndex] ?
                                                    <Box width={1}
                                                         display='flex'
                                                         my={1.5}
                                                         px={2}
                                                         height='auto'>
                                                        <Box width={1} pr={1}>
                                                            <a
                                                                no_hover="true"
                                                                target={'_blank'}
                                                                href={adsData.ads[adsIndex].url ? adsData.ads[adsIndex].url : '#'}>
                                                                <Img
                                                                    src={adsData.ads[adsIndex].media.image}
                                                                    alt={adsData.ads[adsIndex].media.title}
                                                                    style={{
                                                                        width: '100%',
                                                                        height: '100%'
                                                                    }}
                                                                    imgStyle={{
                                                                        width: '100%',
                                                                        height: '100%',
                                                                        ...UtilsStyle.borderRadius(5)
                                                                    }}/>
                                                            </a>
                                                        </Box>
                                                    </Box>
                                                    :
                                                    <React.Fragment/>
                                            )
                                        })()}
                                    </SmShow>
                                    <SmHidden>
                                        {adsData && adsData.ads && (() => {
                                            const adsIndex = adsData.ads[index * 2] ? index * 2 : 0;
                                            const adsIndex2 = adsData.ads[adsIndex + 1] ? adsIndex + 1 : adsIndex;
                                            return (
                                                adsData.ads[adsIndex] ?
                                                    <Box width={1}
                                                         display='flex'
                                                         my={1.5}
                                                         px={2}
                                                         height='auto'>
                                                        <Box width='50%' pr={1}>
                                                            <Link hasHoverBase={false} hasHover={false}
                                                                  href={adsData.ads[adsIndex].url || '#'}>
                                                                <Img src={adsData.ads[adsIndex].media.image}
                                                                     alt={adsData.ads[adsIndex].media.title}
                                                                     style={{
                                                                         width: '100%',
                                                                         height: '100%'
                                                                     }}
                                                                     imgStyle={{
                                                                         width: '100%',
                                                                         height: '100%',
                                                                         ...UtilsStyle.borderRadius(5)
                                                                     }}/>
                                                            </Link>
                                                        </Box>
                                                        {
                                                            adsData.ads[adsIndex2] &&
                                                            <Box width='50%' pl={1}>
                                                                <a
                                                                    no_hover="true"
                                                                    target={'_blank'}
                                                                    href={adsData.ads[adsIndex2].url ? adsData.ads[adsIndex2].url : '#'}>
                                                                    <Img
                                                                        src={adsData.ads[adsIndex2].media.image}
                                                                        alt={adsData.ads[adsIndex2].media.title}
                                                                        style={{
                                                                            width: '100%',
                                                                            height: '100%'
                                                                        }}
                                                                        imgStyle={{
                                                                            width: '100%',
                                                                            height: '100%',
                                                                            ...UtilsStyle.borderRadius(5)
                                                                        }}/>
                                                                </a>
                                                            </Box>}
                                                    </Box>
                                                    :
                                                    <React.Fragment/>
                                            )
                                        })()}
                                    </SmHidden>
                                </React.Fragment>
                            )
                            }
                        </Box>
                    )
                })
            )}
            <Placeholder/>
        </React.Fragment>
    )
}

Home.getInitialProps = async function (ctx) {
    if (isClient()) {
        return __NEXT_DATA__.props.pageProps;
    }

    if(DEBUG)
        return {}

    try {
        const data = await Promise.all([
            ControllerProduct.SpecialProducts.get()[1]({
                headers: (ctx.req) ? {Cookie: ctx.req.headers.cookie} : undefined
            }),
            ControllerProduct.SpecialProducts.getAll()[1]({
                headers: (ctx.req) ? {Cookie: ctx.req.headers.cookie} : undefined
            }),
        ])

        return {
            specialProducts: data[0],
            allSpecialProducts: data[1],
        }
    } catch {
    }
};

export default withAuthSync(Home);
