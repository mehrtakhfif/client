import React, {useCallback, useEffect, useMemo, useState as useStateMaterialUi} from "react";
import {useRouter} from "next/router";
import {Box, getSafe, gLog, HiddenLgUp, tryIt, useState} from "material-ui-helper";
import useSWR, {mutate} from "swr";
import ControllerProduct from "../../controller/ControllerProduct";
import StickyBox from "../../component/base/StickyBox";
import Gallery from "../../component/productV2/gallery/Gallery";
import Breadcrumbs from "../../component/productV2/Breadcrumbs";
import BasePage from "../../component/base/BasePage";
import CustomHeader, {HeaderItem} from "../../component/header/customHeader/CustomHeader";
import ActiveStorageContext, {defaultActiveStorageContext} from "../../component/productV2/context/ActiveStorageContext";
import _ from "lodash";
import ProductDetails from "../../component/productV2/storagePanel/storages/base/productDetails/ProductDetails";
import ProductDataContext from "../../component/productV2/context/ProductDataContext";
import StickyContent from "../../component/productV2/StickyContent/StickyContent";
import StorageDataContext from "../../component/productV2/context/StorageDataContext";
import OrderCountContext from "../../component/productV2/context/OrderCountContext";
import LeftStickyPanel from "../../component/productV2/LeftStickyPanel";
import Wish from "../../component/productV2/base/Wish";
import {DEBUG} from "../_app";
import ProductParamsContext from "../../component/productV2/context/ProductParamsContext";
import rout from "../../router";
import Storages from "../../component/productV2/storagePanel/storages/Storages";
import ControllerUser from "../../controller/ControllerUser";
import Share from "../../component/productV2/storagePanel/Share";
import ActiveFeatureContext from "../../component/productV2/context/ActiveFeatureContext";
import Seo from "../../component/productV2/seo/Seo";
import ProductNotFound from "../../component/single/ProductNotFound";
import ControllerSite from "../../controller/ControllerSite";

//Todo: Add Service address details and map. Add Related Product

function Product({initialData, storageInitialData}) {
    const {query, ...props} = useRouter()
    const [loading, setLoading] = useStateMaterialUi(false)
    const permalink = query[rout.Product.SingleV2.Params.permalink]
    const productPreview = query[rout.Product.SingleV2.Params.productPreview]

    const [activeStorage, setActiveStorage] = useState(defaultActiveStorageContext)
    const [firstFeature, setFirstFeature] = useState({})
    const [activeFeature, setActiveFeature] = useState({})
    const [orderCount, setOrderCount] = useState(1, {
        validator: (value, setter) => {
            if (value <= 0)
                return
            setter(value)
        }
    });
    const [storageCatch, setStorageCatch] = useState(undefined);


    useEffect(() => {
        if (!firstFeature)
            setFirstFeature({})
    }, [permalink])

    //region ProductData
    const [key, req, swrProps] = ControllerProduct.Product.V2.get({productPreview, permalink, initialData})

    const {data, error} = useSWR(key, req, swrProps)

    const {id, media, wish} = data || {};
    //endregion ProductData

    //region StorageData
    const selectedFeatures = useMemo(() => {
        return getSafe(() => {
            if (_.isEqual(activeFeature, firstFeature))
                return
            let list = _.values(activeFeature);
            list = _.sortBy(list)
            return list;
        })
    }, [activeFeature, firstFeature])

    const [storageKey, storageReq, swrStorageProps] = ControllerProduct.Product.V2.getStorage({
        permalink,
        selectedFeatures: selectedFeatures,
        initialData: getSafe(() => {
            if (_.isEmpty(activeFeature))
                return storageInitialData
            _.forEach(storageInitialData.features, ({id, selected}) => {
                if (selected !== activeFeature[id])
                    throw ""
            })
            return storageInitialData;
        }, undefined),
    })

    const {data: storageData, error: errorDataStorage} = useSWR(storageKey, storageReq, swrStorageProps)
    const {is_service, storage} = storageData || {};

    const handleChangeActiveStorage = useCallback((activeStorage) => {
        if (loading)
            return
        setActiveStorage(activeStorage)
    }, [activeStorage, loading])

    const handleChangeActiveFeature = (newFeatureItem, feature) => {
        if (loading || !newFeatureItem || !feature)
            return
        const newActiveFeature = _.cloneDeep(activeFeature)
        newActiveFeature[_.toString(feature.id)] = newFeatureItem.id;
        setActiveFeature({...newActiveFeature})
    }

    // const handleChangeActiveFeature = useCallback((newFeatureItem, feature) => {
    // }, [activeFeature, setActiveFeature, firstFeature, loading])

    useEffect(() => {
        if (_.isEmpty(storageData))
            return
        //region activeStorage
        tryIt(() => {
            if (is_service) {
                if (activeStorage && _.findIndex(storage, st => st.id === activeStorage?.id) !== -1) {
                    return;
                }

                const ac = _.find(storage, st => {
                    return st.max_count_for_sale > 0
                })
                handleChangeActiveStorage(ac || storage)
                return;
            }

            if (activeStorage?.id === storage?.id) {
                return;
            }
            handleChangeActiveStorage(storage)
        })
        //endregion activeStorage
        //region feature
        tryIt(() => {
            if (_.isEmpty(storageData))
                return
            let selectedFeature = {};
            _.forEach(storageData?.features, ({id, selected}) => {
                if (!selected)
                    return
                selectedFeature[_.toString(id)] = selected;
            })
            if (_.isEmpty(firstFeature)) {
                setFirstFeature(_.cloneDeep(selectedFeature))
            }
            setActiveFeature(selectedFeature)
        })
        //endregion feature
        if (!_.isEmpty(storageData))
            setStorageCatch(storageData)
    }, [storageData]);

    //endregion StorageData

    const handleSubmit = useCallback(() => {
        setLoading(true)
        ControllerUser.Basket.V2.add({
            storage_id: is_service ? activeStorage?.id : storage?.id,
            count: orderCount
        }).finally(() => {
            setLoading(false)
        })
    }, [loading, orderCount, setLoading, activeStorage])

    const mutateProductData = useCallback(() => {
        mutate(key)
    }, [key])

    const mutateStorageData = useCallback(() => {
        mutate(storageKey)
    }, [storageKey])


    return (
        <BasePage
            name="product"
            setting={{
                header: {
                    showSm: false
                },
                footer: {
                    showSm: false
                }
            }}>
            <ProductDataContext.Provider value={[data, mutateProductData, error]}>
                <StorageDataContext.Provider
                    value={[storageData || storageCatch, mutateStorageData, !Boolean(storageData)]}>
                    <ActiveStorageContext.Provider value={[activeStorage, handleChangeActiveStorage]}>
                        <ActiveFeatureContext.Provider value={[activeFeature, handleChangeActiveFeature]}>
                            <OrderCountContext.Provider value={[orderCount, setOrderCount]}>
                                <ProductParamsContext.Provider
                                    value={{
                                        loading: (loading || _.isEmpty(storage)),
                                        onSubmit: handleSubmit
                                    }}>
                                    <Seo/>
                                    <Box
                                        id={"product-container"}
                                        width={1}
                                        pt={{
                                            xs: 1,
                                            md: 8
                                        }}
                                        flexDirectionColumn={true}>
                                        {
                                            error?.response?.status >= 400 ?
                                                <ProductNotFound/> :
                                                <React.Fragment>
                                                    <Breadcrumbs/>
                                                    <HiddenLgUp>
                                                        <CustomHeader
                                                            menus={[
                                                                HeaderItem.Search,
                                                                HeaderItem.Basket,
                                                                <Wish/>,
                                                                <Share/>
                                                            ]}
                                                            closable={true}/>
                                                    </HiddenLgUp>
                                                    <Box width={1}>
                                                        <StickyBox
                                                            width={{
                                                                xs: "100%",
                                                                lg: "68%"
                                                            }}>
                                                            <Gallery data={media}/>
                                                            <ProductDetails/>
                                                            <StickyContent productId={id}/>
                                                        </StickyBox>
                                                        <LeftStickyPanel/>
                                                    </Box>
                                                    <HiddenLgUp>
                                                        <Storages/>
                                                    </HiddenLgUp>
                                                </React.Fragment>
                                        }
                                    </Box>
                                </ProductParamsContext.Provider>
                            </OrderCountContext.Provider>
                        </ActiveFeatureContext.Provider>
                    </ActiveStorageContext.Provider>
                </StorageDataContext.Provider>
            </ProductDataContext.Provider>
        </BasePage>
    )
}

// export async function getServerSideProps({query,req, ...context}) {
//     try {
//         const permalink = query[rout.Product.SingleV2.Params.permalink];
//
//         const headers = req.headers;
//         const [__, reqProduct] = ControllerProduct.Product.V2.get({permalink,headers})
//
//         const [___, reqStorage] = ControllerProduct.Product.V2.getStorage({
//             permalink,
//             headers,
//         })
//         const data = await Promise.all([
//             reqProduct(),
//             reqStorage(),
//         ])
//
//         return {
//             props: {
//                 initialData: data[0],
//                 storageInitialData: data[1],
//             }
//         }
//     } catch (e) {
//         return {
//             props: {}
//         }
//     }
// }

Product.getInitialProps = async function ({query,req, ...context}) {
    try {
        if (!req)
            throw ""

        const permalink = query[rout.Product.SingleV2.Params.permalink];

        const [__, reqProduct] = ControllerProduct.Product.V2.get({permalink})

        const [___, reqStorage] = ControllerProduct.Product.V2.getStorage({
            permalink,
        })

        const data = await Promise.all([
            reqProduct(),
            reqStorage(),
        ])


        return {
            initialData: data[0],
            storageInitialData: data[1],
        }
    } catch (e) {
        return {}
    }
}

export default Product;