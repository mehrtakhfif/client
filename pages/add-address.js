import React, {Fragment, useEffect, useRef} from 'react';
import Box from "@material-ui/core/Box";
import ProductsContainerHeader from "../component/home/product/productsContainer/ProductsContainerHeader";
import Container from "@material-ui/core/Container";
import {useTheme} from "@material-ui/core";
import {GA, lang} from "../repository";
import rout from '../router';
import {makeStyles} from "@material-ui/styles";
import PropTypes from 'prop-types';
import Hidden from "@material-ui/core/Hidden";
import _ from 'lodash'
import Button from "@material-ui/core/Button";
import NewTextField from "../component/base/textField/TextField";
import SuccessButton from "../component/base/button/buttonVariant/SuccessButton";
import {useSnackbar} from "notistack";
import {blue, red} from "@material-ui/core/colors";
import BaseButton from "../component/base/button/BaseButton";
import NewFormControl from "../component/base/formController/NewFormController";
import Map from "../component/addAddress/Map";
import AutoFill from "../component/base/autoFill/AutoFill";
import PostalCodeTextField from "../component/base/textField/PostalCodeTextField";
import {UtilsStyle} from "../utils/Utils";
import ControllerUser from '../controller/ControllerUser'
import {states} from '../controller/converter';
import ControllerSite from "../controller/ControllerSite";
import {useRouter} from "next/router";
import Head from "next/head";
import {getPageTitle} from "../headUtils";
import TextFieldContainer, {errorList} from "../component/base/textField/TextFieldContainer";
import PhoneTextField from "../component/base/textField/PhoneTextField";
import MultiTextField from "../component/base/textField/MultiTextField";
import LoginRequiredRout from "../lib/wrappedcomponent/LoginRequiredRout";
import BasePage from "../component/base/BasePage";
import {useIsLoginContext} from "../context/IsLoginContextContainer";
import {useUserContext} from "../context/UserContext";


const useStyles = makeStyles(theme => ({
    inputContainer: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    input: {
        width: '100%',
        marginLeft: theme.spacing(5),
        marginRight: theme.spacing(5),
        [theme.breakpoints.down('md')]: {
            marginLeft: theme.spacing(4),
            paddingLeft: 0
        },
        [theme.breakpoints.down('sm')]: {
            marginRight: theme.spacing(4),
        },
        [theme.breakpoints.down('xs')]: {
            marginRight: 0,
            marginLeft: 0
        }
    },
    mapContainer: {
        [theme.breakpoints.down('sm')]: {
            zIndex: 9999,
            position: 'fixed',
            overflow: 'hidden',
            height: '100%',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0
        },
    },
    mapSubmitBtn: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        ...UtilsStyle.borderRadius(0),
        [theme.breakpoints.down('xs')]: {
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
        }
    },
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 3,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
    formButton: {
        paddingRight: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        [theme.breakpoints.down('sm')]: {
            paddingRight: theme.spacing(1),
            paddingLeft: theme.spacing(1),
        },
        [theme.breakpoints.down('xs')]: {
            paddingTop: theme.spacing(1.5),
            paddingBottom: theme.spacing(1.5),
            marginBottom: theme.spacing(2),
            width: '100% !important'
        }
    }
}));


export default function AddAddress({...props}) {

    return (
        <LoginRequiredRout>
            <AddAddressCm {...props}/>
        </LoginRequiredRout>
    )
}

function AddAddressCm({address, onFinish, onClose, directBack = true, ...props}) {
    const router = useRouter();
    const isLogin = useIsLoginContext();
    const user = useUserContext();
    useEffect(() => {
        GA.initializePage({pageRout: rout.User.Profile.AddAddress.rout});
    }, []);

    //region variable
    const theme = useTheme()
    const classes = useStyles();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = React.useState({
        mapVisibilityInMD: false,
        locationSelected: false,
        locationSelectedFirst: false,
        disable: false,
    });
    const [location, setLocation] = React.useState({
        state: address ? address.state : null,
        city: address ? {
            ...address.city,
            label: address.city.name
        } : null,
    });
    const [map, setMap] = React.useState({
        centerLocation: null,
        targetLocation: {
            lat: (address && address.location && address.location.lat) ? address.location.lat : 37.28049,
            lng: (address && address.location && address.location.lng) ? address.location.lng : 49.59051,
        }
    });
    const [inputs, setInputs] = React.useState({
        name: {
            text: address ? address.name : (user?.fullName || "")
        },
        mobileNumber: {
            text: address ? address.phone : (user?.phone || "")
        },
        address: {
            text: address ? address.address : ""
        },
        postalCode: {
            text: address ? address.postalCode : ""
        }
    });

    const textInputRef = useRef();

    useEffect(() => {
        try {
            if (state.locationSelectedFirst) {
                setState({
                    ...state,
                    locationSelectedFirst: false
                });
                window.scrollTo(0, document.body.scrollHeight)
            }
        } catch (e) {
        }
    }, [state]);
    //endregion variable

    //region function
    function onStateSelected(item) {
        if (item === location.state.activeItem) {
            return
        }
        setLocation({
            ...location,
            state: {
                ...location.state,
                activeItem: item
            },
            city: {
                items: [],
                activeItem: null
            }
        });
        ControllerSite.getCities({state_id: item.id})
            .then(res => {
                const newCities = [];
                _.each(res.data.cities, function (value) {
                    newCities.push({
                        label: value.name,
                        ...value
                    })
                });
                setLocation({
                    ...location,
                    state: {
                        ...location.state,
                        activeItem: item
                    },
                    city: {
                        ...location.city,
                        items: newCities,
                    }
                })
            })
            .catch(function (error) {
                setLocation({
                    ...location,
                    state: {
                        ...location.state,
                        activeItem: null
                    },
                    city: {
                        ...location.city,
                        items: null,
                    }
                })
            })
    }

    function onAddAddressHandler() {
        setState({
            ...state,
            disable: true
        });
        try {
            const hasError = textInputRef.current.hasError() || !(location.state && location.state.id) || !(location.city && location.city.id);

            if (hasError) {
                enqueueSnackbar(lang.get("er_fill_all_inputs"),
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                setState({
                    ...state,
                    disable: false
                });
                return
            }

            const i = textInputRef.current.serialize();
            const props = {
                name: i.name,
                address: i.address,
                postalCode: i.postalCode,
                phone: i.mobile,
                state_id: location.state.id,
                city_id: location.city.id,
                location: map.centerLocation
            }

            if (address) {
                ControllerUser.User.Profile.Addresses.update({
                    id: address.id,
                    ...props
                }).then((res) => {
                    addSuccessful(res)
                }).catch(addError);
                return;
            }
            ControllerUser.User.Profile.Addresses.add({
                ...props
            }).then(addSuccessful).catch(addError);

        } catch (e) {
            addError()
        }
    }

    function addSuccessful(res) {
        enqueueSnackbar(lang.get("me_add_address_successful"),
            {
                variant: "success",
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
        if (onFinish) {
            onFinish();
        } else
            router.push((router.query && router.query.R) ? router.query.R : rout.User.Profile.Main.rout);

        setState({
            ...state,
            disable: false,
        });
    }

    function addError(err) {
        setState({
            ...state,
            disable: false
        });
        enqueueSnackbar(lang.get("er_error"),
            {
                variant: "error",
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
        setState({
            ...state,
            disable: false
        });
    }

    //endregion function

    return (
        <BasePage
            name="add-address">
            <Box>
                <Head>
                    <title>{getPageTitle(lang.get("add_address"))}</title>
                </Head>
                <Container
                    maxWidth="xl"
                    style={{
                        marginTop: theme.spacing(3)
                    }}>
                    <ProductsContainerHeader
                        style={{
                            margin: 0
                        }}
                        container_style={{
                            paddingTop: theme.spacing(2),
                            paddingBottom: theme.spacing(2)
                        }}
                        borderColor="#a56">
                        {lang.get('transferee_specifications')}
                    </ProductsContainerHeader>
                    <Box display='flex'>
                        <Hidden smDown={state.mapVisibilityInMD}>
                            <NewFormControl display='flex'
                                            flexWrap='wrap'
                                            width={{
                                                md: '60% !important',
                                                xs: '100%'
                                            }}
                                            innerref={textInputRef}>
                                <ItemContainer
                                    width={['100%', '50%', '50%']}>
                                    <Box width={'80%'} alignItems={'center'} justifyContent={'center'}>
                                        <TextFieldContainer
                                            name={"name"}
                                            defaultValue={inputs.name.text}
                                            render={(ref, {
                                                name: inputName,
                                                initialize,
                                                valid,
                                                errorIndex,
                                                setValue,
                                                inputProps,
                                                style,
                                                props
                                            }) => {
                                                return (
                                                    <NewTextField
                                                        {...props}
                                                        error={!valid}
                                                        name={inputName}
                                                        helperText={errorList[errorIndex]}
                                                        inputRef={ref}
                                                        fullWidth
                                                        required={true}
                                                        label={lang.get('full_name_transferee')}
                                                        style={{
                                                            ...style,
                                                            minWidth: '90%'
                                                        }}/>
                                                )
                                            }}/>
                                    </Box>
                                </ItemContainer>
                                <ItemContainer>
                                    <Box width={'80%'} alignItems={'center'} justifyContent={'center'}>
                                        <PhoneTextField
                                            variant={"standard"}
                                            name={"mobile"}
                                            required={true}
                                            defaultValue={inputs.mobileNumber.text}/>
                                    </Box>
                                </ItemContainer>
                                <Box
                                    display='flex'
                                    flexDirection={{
                                        sm: 'row !important',
                                        xs: 'column'
                                    }}
                                    width={1}>
                                    <SelectAddress
                                        defaultState={location.state}
                                        defaultCity={location.city}
                                        onChange={(val) => {
                                            setLocation(val)
                                        }}/>
                                </Box>
                                <Box display='flex'
                                     justifyContent={['center', 'start', 'center']}
                                     width={1}
                                     mr={1}
                                     ml={1}
                                     mt={2}
                                     mb={1}>
                                    <Box width={'80%'} alignItems={'center'} justifyContent={'center'}>
                                        <MultiTextField
                                            name={"address"}
                                            defaultValue={inputs.address.text}
                                            variant={"standard"}
                                            isRequired
                                            placeholder={lang.get('post_address_placeholder')}
                                            label={lang.get("post_address")}/>
                                    </Box>
                                </Box>
                                <ItemContainer justifyContent={'center'}>
                                    <Box width={'80%'} alignItems={'center'} justifyContent={'center'}>
                                        <PostalCodeTextField
                                            name={"postalCode"}
                                            variant={"standard"}
                                            defaultValue={inputs.postalCode.text}
                                            isRequired/>
                                    </Box>
                                </ItemContainer>
                                <ItemContainer>
                                    <Box
                                        className={classes.input}
                                        ml={10}
                                        width='80%'
                                        display='flex'
                                        style={{
                                            marginRight: 0
                                        }}
                                        justifyContent='flex-end'
                                        flexDirection={{
                                            sm: 'row !important',
                                            xs: 'column'
                                        }}>
                                        <Hidden mdUp>
                                            <BaseButton
                                                className={classes.formButton}
                                                disabled={state.disable}
                                                onClick={() => {
                                                    setState({
                                                        ...state,
                                                        mapVisibilityInMD: true
                                                    })
                                                }}
                                                style={{
                                                    marginLeft: theme.spacing(2),
                                                    backgroundColor: blue[500],
                                                    color: "#fff",
                                                }}>
                                                {lang.get(!state.locationSelected ? 'select_location_on_map' : 'edit_location')}
                                            </BaseButton>
                                        </Hidden>
                                        <Box display={'flex'}>
                                            {
                                                onClose &&
                                                <Box width={'40%'} pr={1}>
                                                    <BaseButton
                                                        variant={"outlined"}
                                                        className={classes.formButton}
                                                        disabled={state.disable}
                                                        onClick={onClose}
                                                        style={{
                                                            marginLeft: theme.spacing(2),
                                                        }}>
                                                        {"لغو"}
                                                    </BaseButton>
                                                </Box>
                                            }
                                            <SuccessButton
                                                className={classes.formButton}
                                                loading={state.disable}
                                                onClick={onAddAddressHandler}
                                                style={{
                                                    flex: 1
                                                }}>
                                                {lang.get('address_registration_and_sending')}
                                            </SuccessButton>
                                        </Box>
                                    </Box>
                                </ItemContainer>
                            </NewFormControl>
                        </Hidden>
                        <Hidden smDown={!state.mapVisibilityInMD}>
                            <Box display='flex'
                                 boxShadow={3}
                                 className={classes.mapContainer}
                                 flexDirection='column'
                                 width={{
                                     md: '40% !important',
                                     xs: '100%'
                                 }}>
                                <Map
                                    centerLocation={map.centerLocation}
                                    targetLocation={map.targetLocation}
                                    onMoveEnd={(center) => {
                                        setMap({
                                            ...map,
                                            centerLocation: center
                                        })
                                    }}
                                    style={{
                                        zIndex: 2
                                    }}/>
                                <Hidden mdUp>
                                    <FullScreenMap
                                        onReturnClick={() => {
                                            setState({
                                                ...state,
                                                mapVisibilityInMD: false,
                                                locationSelectedFirst: true,
                                            })
                                        }}
                                        onSubmitClick={(val) => {
                                            setMap({
                                                ...map,
                                                centerLocation: val
                                            });
                                            setState({
                                                ...state,
                                                mapVisibilityInMD: false,
                                                locationSelected: true,
                                                locationSelectedFirst: true,
                                            })
                                        }}/>
                                </Hidden>
                            </Box>
                        </Hidden>
                    </Box>
                </Container>
            </Box>
        </BasePage>
    )
}


//region Component
//region ItemContainer
function ItemContainer(props) {
    return (
        <Box display='flex'
             justifyContent={['center', 'start', 'center']}
             alignItems={'center'}
             width={['100%', '50%', '50%']}
             mt={2}
             mb={1}
             {...props}>
            {props.children}
        </Box>
    )
}

//endregion ItemContainer
//region SelectAddress
const stateOnlyGuilan = true

export function SelectAddress({defaultState, defaultCity, onChange, ...props}) {
    const classes = useStyles();
    /**
     * city item if null => not have active state
     * city item if [].length => select new active state and state AutoFill disabled until fetch cites
     * city item if [...] => cites fetched and selectable and state AutoFill achieved
     */
    const [location, setLocation] = React.useState({
        first: 0,
        state: {
            activeItem: undefined
        },
        city: {
            items: null,
            activeItem: null
        }
    });

    useEffect(() => {
        onChange({
            state: location.state.activeItem,
            city: location.city.activeItem
        })
    }, [location]);

    useEffect(() => {
        if (location.state.activeItem === null && defaultState) {
            onStateSelected(defaultState, defaultCity);
        }
    }, [location, defaultCity])


    function onStateSelected(item, defaultCity) {
        if (item === location.state.activeItem) {
            return
        }
        setLocation({
            ...location,
            state: {
                ...location.state,
                activeItem: item
            },
            city: {
                items: [],
                activeItem: null
            }
        });

        ControllerSite.getCities()[1]({state_id: item.id})
            .then(res => {
                const newCities = [];
                _.each(res.data.cities, function (value) {
                    newCities.push({
                        label: value.name,
                        ...value
                    })
                });
                setLocation({
                    ...location,
                    state: {
                        ...location.state,
                        activeItem: item
                    },
                    city: {
                        items: newCities,
                        activeItem: defaultCity
                    }
                });
            })
            .catch(function (error) {
                setLocation({
                    ...location,
                    state: {
                        ...location.state,
                        activeItem: null
                    },
                    city: {
                        ...location.city,
                        items: null,
                    }
                })
            })
    }

    return (
        <>
            <ItemContainer {...((_.isEmpty(location.state.activeItem)) ? {haserror: ""} : null)}>
                <AutoFill className={classes.input}
                          inputId='stateInput'
                          items={states}
                          disabled={(location.city.items ? location.city.items.length === 0 : false) || props.disable}
                          activeItem={location.state.activeItem}
                          onActiveItemChanged={(item) => onStateSelected(item, null)}
                          title={lang.get('state')}
                          placeholder={lang.get('search_state')}/>
            </ItemContainer>
            <ItemContainer {...((_.isEmpty(location.city.activeItem)) ? {haserror: ""} : null)}>
                <AutoFill className={classes.input}
                          inputId='cityInput'
                          items={location.city.items ? location.city.items : []}
                          activeItem={location.city.activeItem}
                          disabled={!location.state.activeItem || _.isEmpty(location.city.items) || props.disable}
                          loading={location.state.activeItem && _.isEmpty(location.city.items)}
                          onActiveItemChanged={(item) => {
                              setLocation({
                                  ...location,
                                  city: {
                                      ...location.city,
                                      activeItem: item
                                  }
                              })
                          }}
                          title={lang.get('city')}
                          placeholder={
                              !location.state.activeItem ?
                                  lang.get("er_first_select_state") :
                                  _.isEmpty(location.city.items) ?
                                      lang.get('please_wait') :
                                      lang.get('search_city')}/>
            </ItemContainer>
        </>
    )
}

SelectAddress.propTypes = {
    defaultState: PropTypes.object,
    defaultCity: PropTypes.object,
    onChange: PropTypes.func.isRequired
};
//endregion SelectAddress
//region FullScreenMap
const useMapStyle = makeStyles(theme => ({
    mapContainer: {
        position: 'fixed',
        overflow: 'hidden',
        height: '100%',
        width: '100% !important',
        zIndex: 999,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    mapSubmitBtn: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        ...UtilsStyle.borderRadius(5),
    },
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 3,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
}));

export function FullScreenMap(props) {
    const {firstLocation, onSubmitClick, onReturnClick} = props;
    const classes = useMapStyle(props);
    const defultLocation = firstLocation ? firstLocation : {
        lat: 37.28049,
        lng: 49.59051,
    };
    const [map, setMap] = React.useState({
        centerLocation: defultLocation,
    });
    return (
        <Box display='flex'
             boxShadow={3}
             className={classes.mapContainer}
             flexDirection='column'
             width={{
                 md: '40% !important',
                 xs: '100%'
             }}>
            <Map
                centerLocation={map.centerLocation}
                targetLocation={defultLocation}
                onMoveEnd={(center) => {
                    setMap({
                        ...map,
                        centerLocation: center
                    })
                }}
                style={{
                    zIndex: 2
                }}/>
            <Box className={classes.mapSubmitBtnContainer} display='flex'>
                <Box width="70%">
                    <BaseButton
                        disabled={false}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={() => onSubmitClick(map.centerLocation)}
                        style={{
                            backgroundColor: blue[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('submit_location')}
                    </BaseButton>
                </Box>
                <Box width='30%'>
                    <BaseButton
                        disabled={false}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={onReturnClick}
                        style={{
                            backgroundColor: red[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('return')}
                    </BaseButton>
                </Box>
            </Box>
        </Box>
    )
}

FullScreenMap.propTypes = {
    firstLocation: PropTypes.object,
    onSubmitClick: PropTypes.func.isRequired,
    onReturnClick: PropTypes.func.isRequired
};

//endregion FullScreenMap
//endregion


