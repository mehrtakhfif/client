import "../wdyr"
import React, {useLayoutEffect, useState} from 'react';
import {SnackbarProvider} from "notistack";
import BaseSite from "../component/baseSite/BaseSite";
import {ThemeProvider} from 'styled-components'
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import {checkCookieVersion, checkLocalStorageVersion, dir, lang, siteRout} from '../repository'
import {SWRConfig} from "swr";
import LazyLoad from "vanilla-lazyload";
import ReactGA from "react-ga";
import {gAnalyticsCode} from "../utils/GAnalytics";
import rtl from 'jss-rtl';
import {jssPreset, StylesProvider} from '@material-ui/core/styles';
import {create} from 'jss';
import "../utils/ObjectUtils"
import * as Sentry from '@sentry/browser';
import moment from "moment-jalaali";
import Head from 'next/head'
import createPalette from "@material-ui/core/styles/createPalette";
import mediaQuery from 'css-mediaquery';
import axios from "axios";
import {register, unregister} from 'next-offline/runtime'
import {getSafe, tryIt} from "material-ui-helper";
import parser from 'ua-parser-js';
import _ from "lodash"
import Init from "../component/baseSite/Init";
import BaseContextContainer from "../context/helper/BaseContextContainer";
import {DefaultSeo} from 'next-seo';
import 'material-ui-helper/dist/index.css'
import 'smart-image-lazyload/dist/index.css'
import SwiperCore, {A11y, Autoplay, Navigation, Pagination, Scrollbar} from "swiper";
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/navigation/navigation.scss';
//region config


//region DEBUGConst
export const DEBUG = (false || ((!process.env.REACT_APP_DEBUG || process.env.REACT_APP_DEBUG === "true") && false));
export const IS_DEV = process.env.REACT_APP_IS_DEV;
export const IS_STAGING = process.env.REACT_APP_IS_STAGING;
export const IS_PROD = process.env.REACT_APP_IS_PROD;
export const DEBUG_AUTH = (DEBUG && false);
export const DEBUG_LAYOUT = (DEBUG && false);
//endregion DEBUGConst

//region theme

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, rtl()]});

const primary = {
    light: "#ffb4b4",
    main: "#ff527b",
    dark: "#ff003d"
}
const secondary = {
    light: "#6ea6fd",
    main: "#3278C9",
    dark: "#004d98"
}
const ty = 0;
const tyMd = 1.15;
const tySm = 2.00;
const tyXs = 4.14815;

const defSpaceXs = 2.2222;
export const spacing = {
    xs: {
        s4: `${defSpaceXs * 0.5}vw`,
        s8: `${defSpaceXs}vw`,
        s12: `${defSpaceXs * 1.5}vw`,
        s16: `${defSpaceXs * 2}vw`,
        s24: `${defSpaceXs * 3}vw`,
        s32: `${defSpaceXs * 4}vw`,
        convert: (px) => {
            return `${defSpaceXs * (px / 8)}vw`
        }
    }
}
const spacingBase = {
    xl: 0.417,
    lg: 0.417,
    md: 0.714,
    sm: 1.025,
    xs: 2.2225
}


function useWindowSize() {
    const [size, setSize] = useState([0, 0]);
    useLayoutEffect(() => {
        function updateSize() {
            tryIt(() => setSize([window.innerWidth, window.innerHeight]))
        }

        window.addEventListener('resize', _.debounce(function () {
            updateSize()
        }, 2000));
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
}


// function spaceGenerate(size){
//     return `${spacing * size}vw`
// }

const getTheme = ({deviceType, width = 1024}) => {
    const ssrMatchMedia = query => ({
        matches: mediaQuery.match(query, {
            width: deviceType === 'mobile' ? '0px' : '1024px',
        }),
    });

    const defaultTheme = createTheme({
        props: {
            MuiUseMediaQuery: {ssrMatchMedia},
        },
    })

    const isXl = 1536 <= width;
    const isLg = 1200 <= width;
    const isMd = 900 <= width;
    const isSm = 600 <= width;
    const isXs = 0 <= width;

    const spacing = getSafe(() => {
        if (isXl) {
            // return 0.417
        }
        if (isLg) {
            return spacingBase.lg
        }
        if (isMd) {
            return spacingBase.md
        }
        if (isSm) {
            return spacingBase.sm
        }
        if (isXs) {
            return spacingBase.xs
        }
        return deviceType === 'mobile' ? spacingBase.sm : spacingBase.lg
    })

    return createTheme({
        direction: dir(),
        spacing: factor => `${spacing * factor}vw`,
        factor: factor => spacing * factor,
        palette: createPalette({
            primary: {
                light: primary.light,
                main: primary.main,
                dark: primary.dark
            },
            secondary: {
                light: secondary.light,
                main: secondary.main,
                dark: secondary.dark
            },
            error: {
                light: "#dd4a66",
                main: "#E4254A",
                dark: "#e20631",
            },
            divider: '#E9E9E9'
        }),
        status: {},
        props: {
            MuiCard: {
                elevation: 2
            },
            MuiTooltip: {},
            MuiUseMediaQuery: {ssrMatchMedia},
            MuiTypography: {
                style: {
                    margin: 'unset'
                }
            }
        },
        typography: {
            h1: {
                fontSize: `${2.917 + ty}vw`,//56px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${2.917 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${2.917 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${2.917 * tyXs}vw`,
                },
            },
            h2: {
                fontSize: `${2.083 + ty}vw`,//40px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${2.083 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${2.083 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${2.083 * tyXs}vw`,
                },
            },
            h3: {
                fontSize: `${1.667 + ty}vw`,//32px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.667 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.667 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.667 * tyXs}vw`,
                },
            },
            h4: {
                fontSize: `${1.5625 + ty}vw`,//30px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.5625 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.5625 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.5625 * tyXs}vw`,
                },
            },
            h5: {
                fontSize: `${1.458 + ty}vw`,//28px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.458 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.458 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.458 * tyXs}vw`,
                },

            },
            h6: {
                fontSize: `${1.354 + ty}vw`,//26px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.354 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.354 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.354 * tyXs}vw`,
                },
            },
            subtitle1: {
                fontSize: `${1.25 + ty}vw`,//24px
                fontWeight: 300,
                lineHeight:1.9,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.25 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.25 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.25 * tyXs}vw`,
                },
            },
            body1: {
                fontSize: `${1.146 + ty}vw`,//22px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.146 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.146 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.146 * tyXs}vw`,
                },
            },
            subtitle2: {
                fontSize: `${1.042 + ty}vw`,//20px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.042 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.042 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.042 * tyXs}vw`,
                },
            },
            body2: {
                fontSize: `${0.9375 + ty}vw`,//18px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${0.9375 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${0.9375 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${0.9375 * tyXs}vw`,
                },
            },
            caption: {
                fontSize: `${0.833 + ty}vw`,//16px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${0.833 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${0.833 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${0.833 * tyXs}vw`,
                },
            },
        },
        shadows: [
            'none',
            '0 .125rem .25rem rgba(0,0,0,.075)',
            '0 .5rem 1rem rgba(0,0,0,.06)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',


            '0 .5rem 1rem rgba(0,0,0,.15)',


            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
        ],
        overrides: {
            MuiTextField: {
                root: {
                    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                        borderColor: secondary.light,
                    }
                }
            }


        }
    })
}
//endregion theme

//region MoreProps

SwiperCore.use([Autoplay,Navigation, Pagination, Scrollbar, A11y]);


export const globalProps={
    phone:"+981391003033",
    email:"info@Mehrtakhfif.com"
}

moment.loadPersian({dialect: 'persian-modern'})

if (!DEBUG)
    Sentry.init({dsn: "https://a2a10ee8c49d4063a8809961571ae64a@o401733.ingest.sentry.io/5272416"});

export const lazyloadConfig = {
    elements_selector: ".lazy"
};
let L;
//endregion MoreProps

//endregion config

function MyApp({Component: Cm, deviceType, pageProps, ...props}) {
    const [width] = useWindowSize()

    const theme = React.useMemo(() => {
        return getTheme({deviceType, width})
    }, [width]);

    React.useEffect(() => {
        register()

        //region LazyLoad
        if (!document.lazyLoadInstance) {
            document.lazyLoadInstance = new LazyLoad(lazyloadConfig);
        }
        //endregion LazyLoad

        //region leaflet
        L = require('leaflet');
        L.Marker.prototype.options.icon = L.icon({
            iconUrl: '/drawable/mapImage/marker-icon.svg',
            className: 'leaflet-custom-marker',
            iconSize: [40, 40],
            iconAnchor: [7, -7],
        });
        //endregion leaflet

        ReactGA.initialize(gAnalyticsCode, {
            debug: DEBUG,
        });

        checkCookieVersion();
        checkLocalStorageVersion();

        //region jssStyles
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
        //endregion jssStyles

        return () => {
            unregister()
        }
    }, []);

    const DEFAULT_SEO =  {
        titleTemplate: `%s | ${lang.get("mehr_takhfif")}`,
        defaultTitle: `${lang.get("mehr_takhfif")} فروشگاه اینترنتی همیشه تخفیف`,
        description: 'فروشگاهی که کالا و خدمات رو با تخفیف در دسترس کاربران قرار میده و از هر خرید، بخشی از بهای محصول رو مستقیما به حساب موسسات حامی زنان سرپرست خانوار، واریز میکنه.',
        noindex: true,
        nofollow: true,
        openGraph: {
            type: 'website',
            locale: 'fa_ir',
            url: siteRout,
            title: lang.get("mehr_takhfif_site"),
            description: '',
            site_name: 'MehrTakhfif.com',
        },
    }

    return (
        <React.Fragment>
            <Head>
                <title>فروشگاه اینترنتی مهرتخفیف</title>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'"/>
                <meta name="msapplication-TileColor" content={primary.main}/>
                <meta name="theme-color" content={primary.main}/>
                <meta name="apple-mobile-web-app-status-bar" content={primary.main}/>
            </Head>
            <DefaultSeo {...DEFAULT_SEO}/>
            <StylesProvider jss={jss}>
                <MuiThemeProvider theme={theme}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline/>
                        <SnackbarProvider
                            maxSnack={3}
                            autoHideDuration={4000}
                            preventDuplicate={true}>
                            <SWRConfig
                                value={{
                                    refreshInterval: 0,
                                    revalidateOnFocus: false,
                                    dedupingInterval: 60000,
                                    shouldRetryOnError: false,
                                }}>
                                <Init/>
                                <BaseContextContainer>
                                    {
                                        DEBUG_LAYOUT ?
                                            <Cm/> :
                                            <BaseSite
                                                pageProps={pageProps}>
                                                <Cm/>
                                            </BaseSite>
                                    }
                                </BaseContextContainer>
                            </SWRConfig>
                        </SnackbarProvider>
                    </ThemeProvider>
                </MuiThemeProvider>
            </StylesProvider>
        </React.Fragment>
    );
}

MyApp.getInitialProps = async function ({Component, ctx, req}) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    if (ctx.req) {
        axios.defaults.headers.get.Cookie = ctx.req.headers.cookie;
    }
    const deviceType = getSafe(() => parser(req.headers['user-agent']).device.type || 'desktop', 'desktop');

    //Anything returned here can be accessed by the client
    return {pageProps: pageProps, deviceType};
};

export default MyApp;
