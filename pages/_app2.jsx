import "../scripts/wdyr"
import React from 'react';
import {SnackbarProvider} from "notistack";
import BaseSite from "../component/baseSite/BaseSite";
import {ThemeProvider} from 'styled-components'
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import {checkCookieVersion, checkLocalStorageVersion, dir} from '../repository'
import {SWRConfig} from "swr";
import LazyLoad from "vanilla-lazyload";
import ReactGA from "react-ga";
import {gAnalyticsCode} from "../utils/GAnalytics";
import rtl from 'jss-rtl';
import {jssPreset, StylesProvider} from '@material-ui/core/styles';
import {create} from 'jss';
import "../utils/ObjectUtils"
import * as Sentry from '@sentry/browser';
import moment from "moment-jalaali";
import Head from 'next/head'
import createPalette from "@material-ui/core/styles/createPalette";
import mediaQuery from 'css-mediaquery';
import axios from "axios";
import 'material-ui-helper/dist/index.css'
import {register, unregister} from 'next-offline/runtime'
import {getSafe} from "material-ui-helper";
import parser from 'ua-parser-js';

//region config

//region DEBUGCont
export const DEBUG = (false || ((!process.env.REACT_APP_DEBUG || process.env.REACT_APP_DEBUG === "true") && true));
export const DEBUG_AUTH = (DEBUG && false);
export const DEBUG_LAYOUT = (DEBUG && false);
//endregion DEBUGCont

//region theme

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, rtl()]});

const primary = {
    light: "#ffb4b4",
    main: "#ff527b",
    dark: "#ff003d"
}
const secondary = {
    light: "#6ea6fd",
    main: "#3278C9",
    dark: "#004d98"
}
const ty = 0;
const tyMd = 1.15;
const tySm = 2.00;
const tyXs = 3.2821;

const defSpaceXs = 2.2222;
export const spacing = {
    xs: {
        s4: `${defSpaceXs * 0.5}vw`,
        s8: `${defSpaceXs}vw`,
        s12: `${defSpaceXs * 1.5}vw`,
        s16: `${defSpaceXs * 2}vw`,
        s24: `${defSpaceXs * 3}vw`,
        s32: `${defSpaceXs * 4}vw`,
        convert: (px) => {
            return `${defSpaceXs * (px / 8)}vw`
        }
    }
}
const spacingBase = {
    xl: 0.417,
    lg: 0.417,
    md: 0.714,
    sm: 1.025,
    xs: 2.2222
}
const getTheme = ({ deviceType}) => {

    const ssrMatchMedia = query => ({
        matches: mediaQuery.match(query, {
            width: deviceType === 'mobile' ? '0px' : '1024px',
        }),
    });

    const defaultTheme = createTheme({
        props: {
            MuiUseMediaQuery: {ssrMatchMedia},
        },
    })

    const isXl = defaultTheme.breakpoints.up('xl');
    const isLg = defaultTheme.breakpoints.up('lg');
    const isMd = defaultTheme.breakpoints.up('md');
    const isSm = defaultTheme.breakpoints.up('sm');
    const isXs = defaultTheme.breakpoints.up('xs');


    const spacing = getSafe(() => {
        if (isXl) {
            // return 0.417
        }
        if (isLg) {
            return spacingBase.lg
        }
        if (isMd) {
            return spacingBase.md
        }
        if (isSm) {
            return spacingBase.sm
        }
        if (isXs) {
            return spacingBase.xs
        }
        return deviceType === 'mobile' ? spacingBase.sm : spacingBase.lg
    })
    return createTheme({
        direction: dir(),
        spacing: factor => `${spacing * factor}vw`,
        palette: createPalette({
            primary: {
                light: primary.light,
                main: primary.main,
                dark: primary.dark
            },
            secondary: {
                light: secondary.light,
                main: secondary.main,
                dark: secondary.dark
            },
            error: {
                light: "#dd4a66",
                main: "#E4254A",
                dark: "#e20631",
            },
            divider: '#E9E9E9'
        }),
        status: {},
        props: {
            MuiCard: {
                elevation: 2
            },
            MuiTooltip: {},
            MuiUseMediaQuery: {ssrMatchMedia},
            MuiTypography: {
                style: {
                    margin: 'unset'
                }
            }
        },
        typography: {
            h1: {
                fontSize: `${2.917 + ty}vw`,//56px
                fontWeight: 300,
                [defaultTheme.breakpoints.up('md')]: {
                    fontSize: `${2.917 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.up('sm')]: {
                    fontSize: `${2.917 * tySm}vw`,
                },
                [defaultTheme.breakpoints.up('xs')]: {
                    fontSize: `${2.917 * tyXs}vw`,
                },
            },
            h2: {
                fontSize: `${2.083 + ty}vw`,//40px
                fontWeight: 300,
                [defaultTheme.breakpoints.up('md')]: {
                    fontSize: `${2.083 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.up('sm')]: {
                    fontSize: `${2.083 * tySm}vw`,
                },
                [defaultTheme.breakpoints.up('xs')]: {
                    fontSize: `${2.083 * tyXs}vw`,
                },
            },
            h3: {
                fontSize: `${1.667 + ty}vw`,//32px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.667 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.667 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.667 * tyXs}vw`,
                },
            },
            h4: {
                fontSize: `${1.5625 + ty}vw`,//30px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.5625 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.5625 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.5625 * tyXs}vw`,
                },
            },
            h5: {
                fontSize: `${1.458 + ty}vw`,//28px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.458 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.458 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.458 * tyXs}vw`,
                },

            },
            h6: {
                fontSize: `${1.354 + ty}vw`,//26px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.354 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.354 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.354 * tyXs}vw`,
                },
            },
            subtitle1: {
                fontSize: `${1.25 + ty}vw`,//24px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.25 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.25 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.25 * tyXs}vw`,
                },
            },
            body1: {
                fontSize: `${1.146 + ty}vw`,//22px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.146 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.146 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.146 * tyXs}vw`,
                },
            },
            subtitle2: {
                fontSize: `${1.042 + ty}vw`,//20px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${1.042 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${1.042 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${1.042 * tyXs}vw`,
                },
            },
            body2: {
                fontSize: `${0.9375 + ty}vw`,//18px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${0.9375 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${0.9375 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${0.9375 * tyXs}vw`,
                },
            },
            caption: {
                fontSize: `${0.833 + ty}vw`,//16px
                fontWeight: 300,
                [defaultTheme.breakpoints.down('md')]: {
                    fontSize: `${0.833 * tyMd}vw`,
                },
                [defaultTheme.breakpoints.down('sm')]: {
                    fontSize: `${0.833 * tySm}vw`,
                },
                [defaultTheme.breakpoints.down('xs')]: {
                    fontSize: `${0.833 * tyXs}vw`,
                },
            },
        },
        shadows: [
            'none',
            '0 .125rem .25rem rgba(0,0,0,.075)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',
            '0 0 20px 0 rgba(0,0,0,0.2)',


            '0 .5rem 1rem rgba(0,0,0,.15)',


            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
            '0 .5rem 1rem rgba(0,0,0,.15)',
        ],
        overrides: {
            MuiTextField: {
                root: {
                    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                        borderColor: secondary.light,
                    }
                }
            }


        }
    })
}
//endregion theme

//region MoreProps

moment.loadPersian({dialect: 'persian-modern'})

if (!DEBUG)
    Sentry.init({dsn: "https://a2a10ee8c49d4063a8809961571ae64a@o401733.ingest.sentry.io/5272416"});

export const lazyloadConfig = {
    elements_selector: ".lazy"
};
let L;
//endregion MoreProps

//endregion config

function MyApp({Component: Cm, deviceType, ...props}) {
    const theme = React.useMemo(() => {
        return getTheme(deviceType)
    }, []);

    React.useEffect(() => {
        register()


        //region LazyLoad
        if (!document.lazyLoadInstance) {
            document.lazyLoadInstance = new LazyLoad(lazyloadConfig);
        }
        //endregion LazyLoad

        //region leaflet
        L = require('leaflet');
        L.Marker.prototype.options.icon = L.icon({
            iconUrl: '/drawable/mapImage/marker-icon.png',
        });
        //endregion leaflet

        ReactGA.initialize(gAnalyticsCode, {
            debug: DEBUG,
        });

        checkCookieVersion();
        checkLocalStorageVersion();

        //region jssStyles
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
        //endregion jssStyles

        return () => {
            unregister()
        }
    }, []);

    return (
        <React.Fragment>
            <Head>
                <title>فروشگاه اینترنتی مهرتخفیف</title>
                <meta name="viewport"
                      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'"/>

                <meta name="msapplication-TileColor" content={primary.main}/>
                <meta name="theme-color" content={primary.main}/>
                <meta name="apple-mobile-web-app-status-bar" content={primary.main}/>
                <meta name="description"
                      content="با خرید از مهرتخفیف در کنار تخفیف‌های واقعی و جذاب، از هر خرید شما 5 درصد به خیریه اهدا می‌شود."
                      key={"meta-description"}/>
            </Head>
            <StylesProvider jss={jss}>
                <MuiThemeProvider theme={theme}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline/>
                            <SnackbarProvider maxSnack={3}
                                              autoHideDuration={4000}
                                              preventDuplicate={true}>
                                <SWRConfig
                                    value={{
                                        refreshInterval: 0,
                                        revalidateOnFocus: true,
                                        dedupingInterval: 2000,
                                        shouldRetryOnError: true,
                                        errorRetryInterval: 15000,
                                        errorRetryCount: 2
                                    }}>
                                    {
                                        DEBUG_LAYOUT ?
                                            <Cm/> :
                                            <BaseSite  {...props}>
                                                <Cm/>
                                            </BaseSite>
                                    }
                                </SWRConfig>
                            </SnackbarProvider>
                    </ThemeProvider>
                </MuiThemeProvider>
            </StylesProvider>
        </React.Fragment>
    );
}

MyApp.getInitialProps = async function ({Component, ctx, req}) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    if (ctx.req) {
        axios.defaults.headers.get.Cookie = ctx.req.headers.cookie;
    }
    const deviceType = getSafe(() => parser(req.headers['user-agent']).device.type || 'desktop', 'desktop');

    //Anything returned here can be accessed by the client
    return {pageProps: pageProps, deviceType};
};

