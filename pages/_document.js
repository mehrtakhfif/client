import React from 'react'
import {ServerStyleSheet as StyledComponentSheets} from 'styled-components'
import {ServerStyleSheets as MaterialUiServerStyleSheets} from '@material-ui/styles'
import Document, {Head, Html, Main, NextScript} from 'next/document';
import {ServerStyleSheets} from '@material-ui/core/styles';
import {colors, dir} from '../repository';

export default class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const styledComponentSheet = new StyledComponentSheets();
        const materialUiSheets = new MaterialUiServerStyleSheets();
        const originalRenderPage = ctx.renderPage;
        try {
            ctx.renderPage = () =>
                originalRenderPage({
                    enhanceApp: App => props =>
                        styledComponentSheet.collectStyles(
                            materialUiSheets.collect(<App {...props} />),
                        ),
                });
            const initialProps = await Document.getInitialProps(ctx);
            return {
                ...initialProps,
                styles: [
                    <React.Fragment key="styles">
                        {initialProps.styles}
                        {materialUiSheets.getStyleElement()}
                        {styledComponentSheet.getStyleElement()}
                    </React.Fragment>,
                ],
            }
        } finally {
            styledComponentSheet.seal()
        }
    }

    render() {
        return (
            <Html lang="fa" dir={dir()}>
                <Head>
                    <link rel="stylesheet" href="/css/App.css"/>
                    {/*<link rel="stylesheet" href="/css/swiper.css"/>*/}
                    <link rel="stylesheet" href="/css/mt-icon.css"/>
                    <meta charSet="utf-8"/>
                    <link rel="apple-touch-icon" sizes="180x180" href="/drawable/icons/mt/apple-touch-icon.png"/>
                    <link rel="icon" type="image/png" sizes="32x32" href="/drawable/icons/mt/favicon-32x32.png"/>
                    <link rel="icon" type="image/png" sizes="16x16" href="/drawable/icons/mt/favicon-16x16.png"/>
                    <link rel="mask-icon" href="/drawable/icons/mt/safari-pinned-tab.svg" color="#ea4253"/>
                    <link rel="shortcut icon" href="/drawable/icons/mt/favicon.ico"/>
                    <link rel="manifest" href="/manifest.json"/>
                    <meta name="language" content="FA"/>
                    <meta name="copyright" content="mehrtakhfif"/>
                    <meta name="msapplication-config" content="/browserconfig.xml"/>
                    <meta name="msapplication-TileColor" content="#ffffff"/>
                    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png"/>
                    <meta name="theme-color" content={colors.primary.main}/>
                    <meta name="msapplication-navbutton-color" content={colors.primary.main}/>
                    <meta name="apple-mobile-web-app-status-bar-style" content={colors.primary.main}/>
                    <meta httpEquiv="Cache-Control"
                          content="public,max-age=1800,max-stale,stale-while-revalidate=86400,stale-if-error=259200"
                          rem="max-age=30minutes"/>
                    <meta name="keywords"
                          content="مهرتخفیف,مهرتخفیف,تخفیف واقعی,تخفیف ویژه,تخفیف گروهی,فروش ویژه,بیشترین تخفیف,تخفیف رشت,تخفیف گیلان,تخفیفی,تخفیف پت شاپ,تخفیف بوم گردی,تخفیف اقامتگاه,تخفیف صنایع دستی,تخفیف کودک,تخفیف غذا,تخفیف ورزش,غذا و نوشیدنی,گل,گلدان"/>
                    {/*<meta name="robots" content="noindex, nofollow"/>*/}
                    <meta name="author" content="GCorp Team"/>
                </Head>
                <body dir={dir()} style={{overflow: 'unset'}}>
                <noscript>لطفا جاوااسکریپت(javascript) مرورگر خود را روشن کنید.</noscript>
                <Main/>
                <NextScript/>
                <div
                    id={"root"}
                    style={{
                        display: "none"
                    }}>
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
                        <defs>
                            <filter id="svgWhite">
                                <feColorMatrix
                                    colorInterpolationFilters="sRGB"
                                    type="matrix"
                                    values="1     0     0     0     0
              0     1     0     0     0
              0     0     1     0     0
              0     0     0     1     0 "/>
                            </filter>
                            <filter id="svgBlack">
                                <feColorMatrix
                                    colorInterpolationFilters="sRGB"
                                    type="matrix"
                                    values="0     0     0     0     0
              0     0     0     0     0
              0     0     0     0     0
              0     0     0     1     0 "/>
                            </filter>
                            <filter id="svgBlue">
                                <feColorMatrix
                                    colorInterpolationFilters="sRGB"
                                    type="matrix"
                                    values="0 0 0 0 0
								0 0.50 0 0 0
								0 0 0.50 0 0
								0 0 0 1 0 "/>
                            </filter>
                            <filter id="svgRed">
                                <feColorMatrix
                                    colorInterpolationFilters="sRGB"
                                    type="matrix"
                                    values="1     0     0     0     0
              0     0     0     0     0
              0     0     0     0     0
              0     0     0     1     0 "/>
                            </filter>
                            <filter id="svgCyan">
                                <feColorMatrix
                                    colorInterpolationFilters="sRGB"
                                    type="matrix"
                                    values="0.15     0     0     0     0
              0     0.78     0     0     0
              0     0     0.85     0     0
              0     0     0     1     0 "/>
                            </filter>
                            <filter id="svgOrange">
                                <feColorMatrix
                                    colorInterpolationFilters="sRGB"
                                    type="matrix"
                                    values="0.98     0     0     0     0
              0     0.55     0     0     0
              0     0     0     0     0
              0     0     0     1     0 "/>
                            </filter>
                        </defs>
                    </svg>
                </div>
                </body>
            </Html>
        );
    }


}


MyDocument.getInitialProps = async ctx => {
    // Resolution order
    //
    // On the server:
    // 1. app.getInitialProps
    // 2. page.getInitialProps
    // 3. document.getInitialProps
    // 4. app.render
    // 5. page.render
    // 6. document.render
    //
    // On the server with error:
    // 1. document.getInitialProps
    // 2. app.render
    // 3. page.render
    // 4. document.render
    //
    // On the client
    // 1. app.getInitialProps
    // 2. page.getInitialProps
    // 3. app.render
    // 4. page.render

    // Render app and page and get the context of the page with collected side effects.
    const sheets = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;

    ctx.renderPage = () =>
        originalRenderPage({
            enhanceApp: App => props => sheets.collect(<App {...props} />),
        });

    const initialProps = await Document.getInitialProps(ctx);

    return {
        ...initialProps,
        // Styles fragment is rendered after the app and page rendering finish.
        styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
    };
};
