import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import {GA, lang, theme} from "../../repository";
import rout from "../../router";
import Skeleton from "@material-ui/lab/Skeleton";
import ControllerUser from "../../controller/ControllerUser";
import {cyan, grey} from "@material-ui/core/colors";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import Typography from "../../component/base/Typography";
import _ from 'lodash'
import {FiberManualRecordTwoTone, GetApp, Print} from "@material-ui/icons";
import {invoiceStatusColor} from "../../component/profile/AllOrder";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {Card, Tooltip} from "@material-ui/core";
import Link from "../../component/base/link/Link";
import TableContainer from "@material-ui/core/TableContainer";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import Img from "../../component/base/oldImg/Img";
import BaseButton from "../../component/base/button/BaseButton";
import {useRouter} from "next/router";
import useSWR, {mutate} from "swr";
import ComponentError from "../../component/base/ComponentError";
import {getPageTitle} from "../../headUtils";
import Head from "next/head";
import {bookingType} from "../../controller/type";
import JDate from "../../utils/JDate";
import BasePage from "../../component/base/BasePage";
import {DEBUG} from "../_app";

export default function Invoice({...props}) {
    const router = useRouter();
    const invoice_id = router.query[rout.User.Profile.Invoice.Single.Params.invoice_id];

    useEffect(() => {
        GA.initializePage({pageRout: invoice_id});
    }, []);

    const {data, error} = useSWR(`invoice-single-${invoice_id}`, () => {
        return ControllerUser.User.Profile.Orders.Invoice.get({invoice_id: invoice_id})
    });

    return (
        <BasePage
            name="invoice">
            <Head>
                <title>{getPageTitle(`MT-${invoice_id}`)}</title>
                {/*<description>نمایش فاکتور (`MT-${invoice_id}`)</description>*/}
            </Head>
            {error ?
                <ComponentError tryAgainFun={() => mutate(d[0])}/>
                : data ?
                    <Box width={1} mt={2} display={'flex'} flexDirection={'column'}>
                        {data.invoice &&
                        <Box pt={3} px={2}>
                            <BaseButton
                                variant={"outlined"}
                                onClick={() => {
                                    window.open(data.invoice)
                                }}
                                style={{
                                    borderColor: cyan[300]
                                }}>
                                <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                                    <Print/>
                                    <Typography p={1}>
                                        دریافت فاکتور
                                    </Typography>
                                </Box>
                            </BaseButton>
                        </Box>}
                        <Details data={data} mx={2} my={2}/>
                        <DigitalTable data={data} mx={2} my={2}/>
                        <ProductTable data={data} mx={2} my={2}/>
                    </Box>
                    :
                    <Skeleton variant={"rect"}
                              height={500}
                              style={{
                                  marginRight: theme.spacing(2),
                                  marginLeft: theme.spacing(2),
                              }}/>}
        </BasePage>
    )
}

//region Components

//region Details
const borderColor = grey[300];
const titleColor = grey[500];

function Details({data, ...props}) {
    const ad = data.address;
    const statusColor = invoiceStatusColor(data.status);


    return (
        <Box display={'flex'} flexWrap={'wrap'}
             component={Card}
             {...props}
             style={{
                 border: `1px solid ${borderColor}`,
                 ...UtilsStyle.borderRadius(5),
                 ...props.style,
             }}>
            {ad &&
            <>
                <DetailsItem
                    evv bBorder
                    flexDirection={'column'}
                    title={`${lang.get("transferee")}:`}
                    body={ad.name}/>
                <DetailsItem
                    bBorder
                    flexDirection={'column'}
                    title={`${lang.get("transferee_phone")}:`}
                    body={ad.phone}/>
                <DetailsItem
                    evv bBorder
                    flexDirection={'column'}
                    title={`${lang.get("transferee_address")}:`}
                    body={(
                        <Box display={'flex'} flexWrap='wrap'>
                            <Typography variant={'subtitle1'} fontWeight={500}>
                                {ad.state.label} ،
                            </Typography>
                            <Typography variant={'subtitle1'} fontWeight={500} pr={0.5}>
                                {ad.city.name} ،
                            </Typography>
                            <Typography variant={'subtitle1'} fontWeight={400} pr={0.5}>
                                {ad.address}
                            </Typography>
                        </Box>
                    )}/>
                <DetailsItem
                    bBorder
                    flexDirection={'column'}
                    title={`${lang.get("postal_code")}:`}
                    body={ad.postalCode}/>
            </>}
            <DetailsItem
                evv bBorder
                flexDirection={'column'}
                title={`${lang.get("number_of_products")}:`}
                body={data.productCount + " " + lang.get('product')}/>
            <DetailsItem
                bBorder
                flexDirection={'column'}
                title={`${lang.get("payment_status")}:`}
                body={(
                    <Box display={'flex'} flexWrap={'wrap'} alignItems={'center'}>
                        <FiberManualRecordTwoTone
                            style={{
                                fill: data.statusColor
                            }}/>
                        <Typography pr={0.5} variant={'subtitle1'} fontWeight={500}>
                            {data.statusLabel}
                        </Typography>
                    </Box>
                )}/>
            <DetailsItem
                evv
                bBorder
                flexDirection={'column'}
                title={`${lang.get("the_amount_payable")}:`}
                body={UtilsFormat.numberToMoney(data.amount) + " " + lang.get('toman')}/>
            {/*<DetailsItem*/}
            {/*    bBorder*/}
            {/*    flexDirection={'column'}*/}
            {/*    title={`${lang.get("total_amount")}:`}*/}
            {/*    body={UtilsFormat.numberToMoney(data.finalPrice) + " " + lang.get('toman')}/>*/}
            {
                (data.booking_type && data.booking_type.key) &&
                <React.Fragment>
                    <DetailsItem
                        evv
                        bBorder={data.booking_type.key === bookingType.datetime.key}
                        flexDirection={'column'}
                        title={`کد پیگیری:`}
                        body={`MT-${data.id}`}/>
                    {
                        data.booking_type.key === bookingType.datetime.key &&
                        <React.Fragment>
                            <DetailsItem
                                flexDirection={'column'}
                                title={`تاریخ ارسال:`}
                                body={(() => {
                                    const d = JDate.timeStampToJalali(data.start_date)
                                    const date = JDate.format(d, "jD jMMMM jYYYY")
                                    const hour = _.toInteger(JDate.format(d, "HH"))
                                    return (
                                        <React.Fragment>
                                            {hour + 1}:00
                                            {" تا "}
                                            {hour}:00
                                            <Box mx={1} style={{borderRight: `2px solid ${grey[500]}`}}/>
                                            {date}
                                        </React.Fragment>
                                    )
                                })()}/>
                            <DetailsItem
                                evv
                                flexDirection={'column'}
                                title={`کارت پستال:`}
                                body={(
                                    <Box display={'flex'} flexDirection={'column'}>
                                        <Typography variant={"body2"} fontWeight={400}>
                                            {data.details.cart_postal.sender}:
                                        </Typography>
                                        <Typography variant={"body2"} pt={0.5}>
                                            {data.details.cart_postal.text}
                                        </Typography>
                                    </Box>
                                )}/>
                        </React.Fragment>
                    }
                </React.Fragment>
            }
        </Box>
    )
}


function DetailsItem({title, body, evv, bBorder, ...props}) {
    return (
        <DenialsItemContainer bBorder={bBorder} evv={evv} flexDirection={'column'} justifyContent={'center'}>
            <Typography variant={'body2'} component={'span'} color={titleColor} fontWeight={500}>
                {title}
            </Typography>
            <Typography variant={'subtitle1'} component={'h6'} color={grey[800]} fontWeight={500} pt={1}>
                {body}
            </Typography>
        </DenialsItemContainer>
    )
}

function DenialsItemContainer({evv, bBorder, ...props}) {
    return (
        <Box display={'flex'} width={0.5}
             px={2}
             py={2}
             {...props}
             style={{
                 borderLeft: evv ? `1px solid ${borderColor}` : 0,
                 borderBottom: bBorder ? `1px solid ${borderColor}` : 0,
                 ...props.style,
             }}>
            {props.children}
        </Box>
    )
}

//endregion Details

//region Table
//region Digital

function DigitalTable({data, ...props}) {
    const d = data.products.services;

    return (
        !_.isEmpty(d) ?
            (
                <Box component={Card} display={'flex'} flexDirection={'column'} pt={2} {...props}>
                    <Typography pr={2} variant={'h6'} fontWeight={500} color={grey[800]} pb={3}>
                        {lang.get("digit_products_props")} :
                    </Typography>
                    <InvoiceTable data={d}/>
                </Box>
            )
            : <React.Fragment/>
    )
}

function ProductTable({data, ...props}) {
    const d = data.products.products;

    return (
        !_.isEmpty(d) ?
            (
                <Box component={Card} display={'flex'} flexDirection={'column'} pt={2} {...props}>
                    <Typography pr={2} variant={'h6'} fontWeight={500} color={grey[800]} pb={3}>
                        {lang.get("products")} :
                    </Typography>
                    <InvoiceTable data={d}/>
                </Box>
            )
            : <React.Fragment/>
    )
}

const useInvoiceTableStyles = makeStyles((theme) => ({
    digitalTable: {
        minWidth: 650,
    },
    digitalTableItem: {
        borderRight: `1px solid ${grey[400]}`,
        paddingRight: theme.spacing(1),
        paddingLeft: theme.spacing(1),
        paddingTop: theme.spacing(1.5),
        paddingBottom: theme.spacing(1.5),
        [theme.breakpoints.up('md')]: {
            paddingRight: theme.spacing(3),
            paddingLeft: theme.spacing(3),
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
        },
    },
    titleWidth: {
        maxWidth: 250
    }
}));

function InvoiceTable({data}) {
    const classes = useInvoiceTableStyles();

    return (
        <TableContainer>
            <Table className={classes.table} aria-label="orders"
                   style={{
                       overflow: 'scroll'
                   }}>
                <TableHead>
                    <TableRow>
                        <TableCell align="center"
                                   className={classes.tableItem}
                                   style={{...UtilsStyle.disableTextSelection()}}>#</TableCell>
                        <TableCell align="center"
                                   className={clsx(classes.tableItem, classes.titleWidth)}
                                   style={{
                                       maxWidth: 50,
                                   }}>{lang.get("product_name")}</TableCell>
                        <TableCell align="center"
                                   className={classes.tableItem}>{lang.get("count")}</TableCell>
                        <TableCell align="center"
                                   className={classes.tableItem}>{lang.get("price_unit")}</TableCell>
                        <TableCell align="center"
                                   className={classes.tableItem}>{lang.get("discount")} %</TableCell>
                        <TableCell align="center"
                                   className={classes.tableItem}>{lang.get("total_amount")}</TableCell>
                        <TableCell align="center"
                                   className={classes.tableItem}>{lang.get("operations")}</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((p, i) => {
                        const discount = p.finalPrice - p.discountPrice;
                        const storageLink = rout.Product.SingleV2.create(p?.product?.permalink);
                        return (
                            <TableRow key={i}>
                                <TableCell component="th" scope="row" align="center"
                                           className={classes.tableItem}
                                           style={{...UtilsStyle.disableTextSelection()}}>
                                    <Typography pr={0.5} variant={'subtitle1'}>
                                        {i + 1}
                                    </Typography>
                                </TableCell>
                                <TableCell component="th" scope="row" align="right"
                                           className={clsx(classes.tableItem, classes.titleWidth)}>
                                    <Box display={'flex'} alignItems={'center'}>
                                        <Link component={'figure'}
                                              href={storageLink}
                                              style={{
                                                  width: 120,
                                                  minWidth: 120,
                                                  margin: 0
                                              }}>
                                            <Img src={p.product.thumbnail}
                                                 alt={p.storage.invoiceTitle}
                                                 imgStyle={{
                                                     width: '100%',
                                                     overflow: 'hidden',
                                                     ...UtilsStyle.borderRadius(5)
                                                 }}/>
                                        </Link>
                                        <Box display={'flex'} flexDirection={'column'}>
                                            <Link href={storageLink}>
                                                <Typography pr={1.5} variant={'body2'} fontWeight={300}
                                                            color={grey[700]}
                                                            style={{
                                                                lineHeight: 2,
                                                                textAlign:"start"
                                                            }}>
                                                    {p.storage.invoiceTitle}
                                                </Typography>
                                            </Link>
                                            <Box display={'flex'} pt={0.6} alignItems={'center'}>
                                                <Typography pr={1.5} variant={'caption'} fontWeight={300}
                                                            color={grey[700]}
                                                            textStyle={{
                                                                ...UtilsStyle.disableTextSelection()
                                                            }}>
                                                    {lang.get("box")}:
                                                </Typography>
                                                <Typography pr={0.5} variant={'caption'} fontWeight={300}
                                                            color={grey[700]}>
                                                    {p.box}
                                                </Typography>
                                            </Box>
                                        </Box>
                                    </Box>
                                </TableCell>
                                <TableCell component="th" scope="row" align="center"
                                           className={clsx(classes.tableItem, classes.titleWidth)}>
                                    <Typography variant={'subtitle1'} fontWeight={500}
                                                justifyContent={"center"}
                                                color={grey[700]}>
                                        {p.count}
                                    </Typography>
                                </TableCell>
                                <TableCell component="th" scope="row" align="center"
                                           className={clsx(classes.tableItem, classes.titleWidth)}>
                                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                                        <Typography variant={'subtitle1'} fontWeight={300}
                                                    display={'flex'}
                                                    color={grey[700]}>
                                            {UtilsFormat.numberToMoney(p.unitPrice)}
                                        </Typography>
                                        <Typography mr={0.5} variant={'caption'} fontWeight={300}
                                                    color={grey[700]}>
                                            {lang.get('toman')}
                                        </Typography>
                                    </Box>
                                </TableCell>
                                <TableCell component="th" scope="row" align="center"
                                           className={clsx(classes.tableItem, classes.titleWidth)}>
                                    {discount ?
                                        <Box display={'flex'} alignItems={'center'}
                                             justifyContent={'center'}>
                                            <Typography variant={'subtitle1'} fontWeight={300}
                                                        display={'flex'}
                                                        dir={"ltr"}
                                                        color={grey[700]}
                                                        style={{
                                                            direction: 'ltr'
                                                        }}>
                                                {UtilsFormat.numberToMoney(Math.abs(discount))}
                                            </Typography>
                                            <Typography mr={0.5} variant={'caption'} fontWeight={300}
                                                        color={grey[700]}>
                                                {lang.get('toman')}
                                            </Typography>
                                        </Box> : null}
                                </TableCell>
                                <TableCell component="th" scope="row" align="center"
                                           className={clsx(classes.tableItem, classes.titleWidth)}>
                                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                                        <Typography variant={'subtitle1'} fontWeight={300}
                                                    display={'flex'}
                                                    color={grey[700]}>
                                            {UtilsFormat.numberToMoney(p.discountPrice)}
                                        </Typography>
                                        <Typography mr={0.5} variant={'caption'} fontWeight={300}
                                                    color={grey[700]}>
                                            {lang.get('toman')}
                                        </Typography>
                                    </Box>
                                </TableCell>
                                <TableCell component="th" scope="row" align="center"
                                           className={clsx(classes.tableItem, classes.titleWidth)}>
                                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                                        <Box display={'flex'} flexDirection={'column'}>
                                            {p.discount_file ?
                                                <Box display={'flex'} alignItems={'center'} justifyContent={'center'}
                                                     mb={2}>
                                                    <Link
                                                        target={"_target"}
                                                        color={grey[600]}
                                                        hoverColor={grey[700]}
                                                        href={p.discount_file}>
                                                        <Tooltip title={lang.get("show")} placement="top">
                                                            <BaseButton
                                                                variant={"outlined"}>
                                                                <GetApp style={{marginLeft: theme.spacing(1)}}/>
                                                                دانلود کد تخفیف
                                                            </BaseButton>
                                                        </Tooltip>
                                                    </Link>
                                                </Box> : null
                                            }
                                            {
                                                DEBUG&&
                                                <BaseButton
                                                    variant={"outlined"}
                                                    style={{
                                                        padding: 0
                                                    }}>
                                                    <Box display={'flex'} justifyConetnt={'center'}>
                                                        <Link
                                                            hasHover={false}
                                                            display={'flex'}
                                                            width={1}
                                                            color={grey[600]}
                                                            hoverColor={grey[700]}
                                                            href={rout.Product.SingleV2.as(p?.product?.permalink)}
                                                            linkStyle={{
                                                                width: '100%',
                                                                paddingTop: theme.spacing(0.5),
                                                                paddingBottom: theme.spacing(0.5),
                                                                paddingRight: theme.spacing(2),
                                                                paddingLeft: theme.spacing(2),
                                                            }}>
                                                            {lang.get("add_comment")}
                                                        </Link>
                                                    </Box>
                                                </BaseButton>
                                            }
                                        </Box>
                                    </Box>
                                </TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

//endregion Digital

//endregion Components

//endregion Components
