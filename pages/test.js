import React, {useState} from 'react';
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {Controller, Navigation, Thumbs} from 'swiper/core';
import {Box} from "material-ui-helper";
import BasePage from "../component/base/BasePage";
import Search from "../component/search/Search";
import DebugRout from "../lib/wrappedcomponent/DebugRout";

// install Swiper's Thumbs component
SwiperCore.use([Navigation, Thumbs, Controller]);

const images = [
    "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/media/11-30-43-13-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/media/11-30-36-74-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-07-31/media/11-30-14-27-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-11-17/media/22-39-33-66-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-11-17/media/22-39-24-14-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-11-17/media/22-39-28-95-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-11-17/media/22-39-40-91-has-ph.jpg",
    "https://api.mehrtakhfif.com/media/boxes/2/2020-11-17/media/22-40-10-62-has-ph.jpg",
]


const App = () => {


    return (
        <DebugRout>

        <BasePage
            name="test"
            setting={{
            header: {
                showLg: true,
            },
        }}>
            <Box p={10}>
                <Search/>
            </Box>
        </BasePage>
        </DebugRout>
    )


    // store thumbs swiper instance
    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const [controlledSwiper, setControlledSwiper] = useState(null);


    return (
        <Box flexDirection={'column'} center={true}>
            <Box display={'block'} width={1 / 2}>
                <Box>
                    <Swiper
                        id="main"
                        thumbs={{swiper: thumbsSwiper}}
                        tag="section"
                        wrapperTag="ul"
                        navigation
                        spaceBetween={0}
                        slidesPerView={1}
                        onInit={(swiper) => console.log('Swiper initialized!', swiper)}
                        onSlideChange={(swiper) => {
                            console.log('Slide index changed to: ', swiper.activeIndex);
                        }}
                        onReachEnd={() => console.log('Swiper end reached')}>
                        {images.map(it => (
                            <SwiperSlide key={it}>
                                <Box>
                                    <img src={it} alt={it} style={{width: "100%"}}/>
                                </Box>
                            </SwiperSlide>
                        ))}
                    </Swiper>
                </Box>
                <Box>
                    <Swiper
                        id="thumbs"
                        spaceBetween={5}
                        slidesPerView={3}
                        onSwiper={setThumbsSwiper}>
                        {images.map((it => (
                            <SwiperSlide
                                key={it} tag="li" style={{listStyle: 'none'}}>
                                <Box>
                                    <img src={it} alt={it} style={{width: '100%'}}/>
                                </Box>
                            </SwiperSlide>
                        )))}
                    </Swiper>
                </Box>
            </Box>
        </Box>
    )
}

export default App