import JsCookies from "js-cookie";
import {UtilsEncrypt} from "./utils/Utils";
import nextCookie from "next-cookies";
import jwt from "jsonwebtoken";
import _ from "lodash";
import {sitePrivateKeys} from "./sv";


function encode(key, value) {
    return jwt.sign(value, key + sitePrivateKeys.uniqueHSACode, {algorithm: 'HS256'});
}

function decode(key, value) {
    return jwt.verify(value, key + sitePrivateKeys.uniqueHSACode)
}

const cookies = {
    set: function ({key, value}) {
        try {
            return JsCookies.set(helperCookies.generateKey(key), helperCookies.generateValue(key, value));
        } catch (e) {
            return false;
        }
    },
    get: function ({key, ctx, defaultData}) {
        try {
            let val = undefined;
            if (ctx) {
                val = nextCookie(ctx)[helperCookies.generateKey(key)];
            }
            if (val === undefined)
                val = JsCookies.get(helperCookies.generateKey(key));
            const data = decode(key, val);
            if (data)
                return data
        } catch (e) {
        }
        return defaultData
    },
    getServer: function ({key,ctx, defaultData}) {
        try {
            let c =undefined;
            if (ctx){
                c =  nextCookie(ctx)[key];
            }else {
                c = JsCookies.get(key);
            }
            return _.split(c, ':', 1)[0]
        } catch (e) {
        }
        return defaultData
    },
    getJson: function ({key, ctx, defaultData}) {
        try {
            let val = undefined;
            if (ctx) {
                val = nextCookie(ctx)[helperCookies.generateKey(key)];
            }
            if (val === undefined) {
                val = JsCookies.getJSON(helperCookies.generateKey(key));
            }
            const data = (decode(key, val));
            if (data)
                return data
        } catch (e) {
        }
        return defaultData
    },
    remove: function ({key}) {
        try {
            return JsCookies.remove(helperCookies.generateKey(key));
        } catch (e) {
        }
    },
    removeAll: function () {
        try {
            Object.keys(JsCookies.get()).forEach(function (cookieName) {
                JsCookies.remove(cookieName);
            });
        } catch (e) {
        }
    },
};
export default cookies;

export const helperCookies = {
    generateKey: function (key) {
        return encode(sitePrivateKeys.uniqueCookieCode + key, key);
    },
    generateValue: function (key, value) {
        return UtilsEncrypt.HSA.encode(key, value);
    }
};


