import _ from 'lodash';
import {lang} from "./repository";

export const getTags = (newTags = '') => {
    const defTags = "مهرتخفیف,مهرتخفیف,تخفیف واقعی,تخفیف ویژه,تخفیف گروهی,فروش ویژه,بیشترین تخفیف,تخفیف رشت,تخفیف گیلان,تخفیفی";
    let addTags = ',';
    if (_.isArray(newTags)) {
        _.forEach(newTags, t => {
            addTags = addTags + t + ','
        })
    } else {
        addTags = newTags + ','
    }
    return (defTags + addTags).slice(0, -1)
};

export function getPageTitle(title) {
    return (
        (title ? title + " | " : "") + lang.get("mehr_takhfif")
    )
}


export function setPageTitle({title = "", withSiteName = true} = {}) {
    if (withSiteName) {
        title = getPageTitle(title)
    }
    document.title = title;
}
