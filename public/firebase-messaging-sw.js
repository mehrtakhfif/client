importScripts('https://www.gstatic.com/firebasejs/7.9.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.9.1/firebase-messaging.js');
if (!firebase.apps.length) {
    firebase.initializeApp({
        apiKey: "AIzaSyAPoyIJc-tp_fCPafgnOsW8FzrLJjs9cIs",
        authDomain: "mehrtakhfif-e5b93.firebaseapp.com",
        databaseURL: "https://mehrtakhfif-e5b93.firebaseio.com",
        projectId: "mehrtakhfif-e5b93",
        storageBucket: "mehrtakhfif-e5b93.appspot.com",
        messagingSenderId: "14720050507",
        appId: "1:14720050507:web:0b061137e84a94bb4534ce",
        measurementId: "G-Y188RXNMC9"
    });
    firebase.messaging();
//background notifications will be received here
    firebase.messaging().setBackgroundMessageHandler((payload) => console.log('payload', payload));
}
